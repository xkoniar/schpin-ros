#!/usr/bin/env python3

# Copyright 2021 Schpin
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


import rclpy
from rclpy.node import Node
import json
import sys
from schpin_msgs.srv import *

filename = sys.argv[1]
extra_raw = sys.argv[2:]
extra = dict([x.split('=') for x in extra_raw])

class LoadClient(Node):

    def __init__(self):
        super().__init__('load_client')
        self.cli = self.create_client(ControlCtl, '/control/ctl')
        while not self.cli.wait_for_service(timeout_sec=1.0):
            self.get_loger().info("Waiting for service /control/ctl")
        self.req = ControlCtl.Request()

    def send_request(self):
        with open(filename) as fd:
            data = fd.read()

        extras = []
        for key, val in extra:
            extras.append("{} = {};".format(key,val))

        self.req.data = "\n".join(extras) + "\n" + data
        self.future = self.cli.call_async(self.req)

def main():
    rclpy.init()

    client = LoadClient()
    client.send_request()

    while rclpy.ok():
        rclpy.spin_once(client)
        if client.future.done():
            try:
                response = client.future.result()
            except Exception as e:
                client.get_logger().info("Service fall failed %r" % (e,))
            else:
                if response.res == 2:
                    client.get_logger().error("Result: %r" % (response.error,))
                else:
                    client.get_logger().info("Result code: %d" % (response.res,))
            break
    client.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
