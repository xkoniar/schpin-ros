#!/usr/bin/python3

import os
import subprocess
import sys
import rclpy
from ament_index_python.packages import get_package_share_directory


control_directory = get_package_share_directory("schpin_control")

assert len(sys.argv) == 2

binary = "{}/stm32/{}.elf".format(control_directory, sys.argv[1])

assert os.path.exists(binary), binary

openocd_cmd = "openocd -f {}/stm32/stm32g4nucleo.cfg > /dev/null 2>&1 &".format(control_directory)
gdb_cmd = "arm-none-eabi-gdb -ex 'target extended localhost:3333' {}".format(binary)

print(openocd_cmd)
print(gdb_cmd)
os.system(openocd_cmd)
os.system(gdb_cmd)
