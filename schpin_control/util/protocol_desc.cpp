#include "shared/protocol.h"

#include <cxxabi.h>
#include <emlabcpp/protocol/json.h>
#include <iostream>
#include <schpin_lib/algorithm.h>
#include <set>

using namespace schpin;

nlohmann::json pretty_compose( nlohmann::json j )
{

        nlohmann::json tbl;

        std::function< void( const nlohmann::json& ) > descent = [&]( const nlohmann::json& ch ) {
                std::string name = ch["raw_name"].get< std::string >();
                if ( tbl.contains( name ) ) {
                        return;
                }
                tbl[name] = ch;
                if ( ch.contains( "sub_types" ) ) {
                        for ( const auto& item : ch["sub_types"] ) {
                                if ( item.contains( "sub_types" ) ) {
                                        descent( item );
                                }
                        }
                        tbl[name]["sub_types"] =
                            map_f_to_v( ch["sub_types"], [&]( const auto& item ) {
                                    if ( item.contains( "sub_types" ) ) {
                                            return item["raw_name"];
                                    }
                                    return item;
                            } );
                }
        };

        descent( j[0] );

        std::vector< nlohmann::json > res;
        std::set< std::string >       visited;

        std::function< void( const nlohmann::json& ) > dfs = [&]( const nlohmann::json& j ) {
                std::string name = j["raw_name"].get< std::string >();

                if ( visited.contains( name ) ) {
                        return;
                }

                visited.insert( name );

                if ( tbl[name].contains( "sub_types" ) ) {
                        for ( const auto& item : tbl[name]["sub_types"] ) {
                                if ( !item.is_string() ) {
                                        continue;
                                }
                                dfs( tbl[item.get< std::string >()] );
                        }
                }

                res.push_back( tbl[name] );
        };

        dfs( j[0] );

        return res;
}

void print_header( const nlohmann::json& j )
{
        std::cout << "   | " << j["name"].get< std::string >() << std::endl;
        std::cout << "   | size       | type          | comment " << std::endl;
        std::cout << "---|------------|---------------|---------" << std::endl;
}

void print_row( std::string prefix, const nlohmann::json& j, std::string t, std::string comment )
{
        int mins = j["min_size"].get< int >();
        int maxs = j["max_size"].get< int >();

        std::cout << " " << prefix << " | ";
        if ( mins != maxs ) {
                std::cout << mins << " - " << maxs;
        } else {
                std::cout << mins << "     ";
        }
        std::cout << "\t| " << t << "\t| " << comment << std::endl;
}

void print_reference( const nlohmann::json& l, std::string prefix, std::string key )
{
        for ( const auto& item : l ) {
                if ( item["raw_name"].get< std::string >() != key ) {
                        continue;
                }

                print_row( prefix, item, item["name"].get< std::string >(), "" );
        }
}

void print_composed( const nlohmann::json& l, const nlohmann::json& j, std::string prefix = "" )
{
        std::string t = j["type"].get< std::string >();

        if ( t == "tuple" ) {
                print_header( j );
                for ( const auto& item : j["sub_types"] ) {
                        if ( item.is_string() ) {
                                print_reference( l, "*", item.get< std::string >() );
                        } else {
                                print_composed( l, item, "*" );
                        }
                }
        } else if ( t == "group" ) {
                print_header( j );
                for ( const auto& item : j["sub_types"] ) {
                        if ( item.is_string() ) {
                                print_reference( l, "+", item.get< std::string >() );
                        } else {
                                print_composed( l, item, "+" );
                        }
                }
        } else if ( t == "variant" ) {
                print_header( j );
                print_composed( l, j["counter_type"], "-" );
                for ( const auto& item : j["sub_types"] ) {
                        if ( item.is_string() ) {
                                print_reference( l, "+", item.get< std::string >() );
                        } else {
                                print_composed( l, item, "+" );
                        }
                }
        } else if ( t == "array" ) {
                print_row(
                    prefix,
                    j,
                    j["sub_type"]["type"].get< std::string >() + "[" +
                        std::to_string( j["item_count"].get< int >() ) + "]",
                    "" );
        } else if ( t.starts_with( "uint" ) || t.starts_with( "int" ) ) {
                print_row( prefix, j, t, "" );
        } else if ( t == "error_record" ) {
                print_row(
                    prefix,
                    j["mark_type"],
                    j["mark_type"]["type"].get< std::string >(),
                    " as variant id" );
                print_composed( l, j["offset_type"], prefix );
        } else if ( t == "mark" ) {
                print_row( prefix, j, t + "  ", "error mark" );
        } else if ( t == "quantity" ) {
                print_row( prefix, j, j["sub_type"]["type"].get< std::string >(), "" );
        } else if ( t == "scaled" ) {
                print_row(
                    prefix,
                    j,
                    j["sub_type"]["type"].get< std::string >(),
                    " stored as value scaled by factor " + std::to_string( j["num"].get< int >() ) +
                        "/" + std::to_string( j["den"].get< int >() ) );
        } else if ( t == "bounded" ) {
                print_row(
                    prefix,
                    j,
                    j["name"].get< std::string >(),
                    " within " + std::to_string( j["min"].get< int >() ) + " - " +
                        std::to_string( j["max"].get< int >() ) + " range" );
        } else if ( t == "enum" ) {
                print_row(
                    prefix,
                    j,
                    j["sub_type"]["type"].get< std::string >(),
                    " with enum values: " +
                        em::joined(
                            j["values"].get< std::vector< std::string > >(), std::string{ "," } ) );
        } else if ( t == "tag" ) {
                const auto& jsub     = j["sub_type"];
                std::string cmt      = " with value " + std::to_string( j["value"].get< int >() );
                std::string sub_type = jsub["type"].get< std::string >();
                if ( sub_type == "enum" ) {
                        cmt += " labeled as " + j["enumerator"].get< std::string >() + " in " +
                               jsub["name"].get< std::string >();
                }
                print_row( prefix, j, jsub["sub_type"]["type"].get< std::string >(), cmt );
        } else {
                std::cout << j;
        }
}

int main( int argc, char* argv[] )
{
        std::string output_type;

        if ( argc == 1 ) {
                output_type = "def";
        } else if ( argc == 2 ) {
                output_type = argv[1];
        } else {
                std::cerr << "You have to specify either no argument or: def/json";
                return 1;
        }

        nlohmann::json fw_node{ em::protocol_decl< fw_node_protocol >{} };
        nlohmann::json node_fw{ em::protocol_decl< node_fw_protocol >{} };

        if ( output_type == "def" ) {
                auto j = pretty_compose( fw_node );
                for ( const auto& item : j ) {
                        print_composed( j, item );
                        std::cout << std::endl;
                        std::cout << std::endl;
                }
        } else if ( output_type == "json" ) {
                std::cout << fw_node << std::endl;
                std::cout << node_fw << std::endl;
        } else {
                std::cerr << "Unknown output type: " << output_type;
                return 1;
        }
        return 0;
}
