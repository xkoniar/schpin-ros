// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/lua_vm.h"

#include <chrono>
#include <schpin_lib/lua.h>
#include <schpin_lib/lua/repl.h>
#include <schpin_msgs/srv/lua_call.hpp>
#include <schpin_msgs/srv/lua_def.hpp>

using namespace schpin;
using namespace std::chrono_literals;

class console : public rclcpp::Node
{
        slua_repl                                              repl_;
        lua_State*                                             L_ptr_;
        rclcpp::Client< schpin_msgs::srv::LuaCall >::SharedPtr lua_call_;

        static int lua_callback( lua_State* l_ptr )
        {
                console* c_ptr =
                    reinterpret_cast< console* >( lua_touserdata( l_ptr, lua_upvalueindex( 1 ) ) );
                const char* name = luaL_checkstring( l_ptr, lua_upvalueindex( 2 ) );

                return c_ptr->handle_callback( name, l_ptr );
        }

        int handle_callback( const char* name, lua_State* l_ptr )
        {
                nlohmann::json args;
                for ( int i : em::range< int >( lua_gettop( l_ptr ) ) ) {
                        // TODO: make some proper handler for json in lua/def.h or json.h?
                        try {
                                args.push_back( slua_json_decode( l_ptr, i + 1 ) );
                        }
                        catch ( const nlohmann::json::exception& e ) {
                                SCHPIN_ERROR_LOG( "console", e.what() );
                        }
                }

                auto request  = std::make_shared< schpin_msgs::srv::LuaCall::Request >();
                request->name = name;
                request->args = args.dump();

                auto result = lua_call_->async_send_request( request );
                if ( rclcpp::spin_until_future_complete(
                         this->get_node_base_interface(), result ) !=
                     rclcpp::FutureReturnCode::SUCCESS ) {
                        SCHPIN_ERROR_LOG( "console", "Failed to call service" );
                        return 0;
                }

                auto response = result.get();

                if ( response->success == 0u ) {
                        SCHPIN_ERROR_LOG(
                            "console", "Remote service call failed: " << response->response );
                        return 0;
                }

                nlohmann::json res = response->response;
                slua_json_encode( l_ptr, res );

                // res.size() ?
                return 1;
        }

public:
        console()
          : Node( "console" )
          , lua_call_( this->create_client< schpin_msgs::srv::LuaCall >( "/control/lua/call" ) )
        {
                auto client = this->create_client< schpin_msgs::srv::LuaDef >( "/control/lua/def" );

                auto request = std::make_shared< schpin_msgs::srv::LuaDef::Request >();

                while ( !client->wait_for_service( 1s ) ) {
                        if ( !rclcpp::ok() ) {
                                SCHPIN_ERROR_LOG(
                                    "console", "Interrupted while waiting for service" );
                                std::abort();
                        }
                        SCHPIN_INFO_LOG( "console", "Service not available, waiting again..." );
                }

                auto result = client->async_send_request( request );

                if ( rclcpp::spin_until_future_complete(
                         this->get_node_base_interface(), result ) !=
                     rclcpp::FutureReturnCode::SUCCESS ) {
                        SCHPIN_ERROR_LOG( "console", "Failed call to service" );
                        std::abort();
                }

                nlohmann::json def = result.get()->definition;

                L_ptr_ = luaL_newstate();
                luaL_openlibs( L_ptr_ );

                em::for_each( def, [&]( nlohmann::json j ) {
                        if ( j["type"] != "callback" && j["type"] != "lua procedure" ) {
                                return;
                        }
                        lua_pushlightuserdata( L_ptr_, this );
                        lua_pushstring( L_ptr_, j["name"].get< std::string >().c_str() );
                        lua_pushcclosure( L_ptr_, lua_callback, 2 );
                        lua_setglobal( L_ptr_, j["name"].get< std::string >().c_str() );
                } );
        }

        void tick()
        {
                std::string msg = repl_.prompt();

                int res = luaL_dostring( L_ptr_, msg.c_str() );
                if ( res != 0 ) {
                        SCHPIN_ERROR_LOG( "console", lua_tostring( L_ptr_, -1 ) );
                        lua_pop( L_ptr_, 1 );
                }
        }
};

int main( int argc, char* argv[] )
{
        rclcpp::init( argc, argv );
        auto con = std::make_shared< console >();

        while ( true ) {
                con->tick();
                rclcpp::spin_some( con );
        }
}
