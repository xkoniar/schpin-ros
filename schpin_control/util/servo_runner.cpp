// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "test/servo_runner.h"

#include <rclcpp/rclcpp.hpp>
#include <schpin_lib/logging.h>

using namespace schpin;

// TODO:
//
//    make extension to protocol that allows json -> protocol serialization

// TODO:
//    make sleep function for tests
//    properly handle errors in run_file, now it loops infiniitely

em::either< std::vector< servo_id >, runner_error > find_dynmx_servos( serial_connection ser )
{
        // 254 is broadcast adress
        auto                    id = dynmx_servo_id::get< 254 >();
        std::vector< servo_id > found;

        SCHPIN_INFO_LOG( "servo_runner", "Finding dynamixels" );

        ser.write( make_master_dynmx_msg< DYNMX_PING >( id ) );

        bool repeat = true;
        while ( repeat ) {
                ser.read_with_sequencer< dynmx_sequencer< dynmx_master_message > >()
                    .bind_left( [&]( dynmx_master_message reply ) {
                            return dynmx_master_extract( reply ).convert_right( [&]( auto e ) {
                                    return E( runner_error ) << e;
                            } );
                    } )
                    .match(
                        [&]< typename T >(
                            std::tuple< dynmx_header, dynmx_servo_id, T, dynmx_crc > pack ) {
                                found.push_back( *std::get< dynmx_servo_id >( pack ) );
                        },
                        [&]( auto err ) {
                                repeat = false;
                                SCHPIN_ERROR_LOG( "servo_runner", err );
                        } );
        }
        if ( found.empty() ) {
                return E( runner_error ) << "Failed to find id";
        }
        SCHPIN_INFO_LOG( "servo_runner", "Found these ids: " << em::view{ found } );
        return found;
}

em::either< std::vector< servo_id >, runner_error >
find_servos( servo_type stype, serial_connection ser )
{
        switch ( stype ) {
                case DYNMX:
                        return find_dynmx_servos( ser );
                default:
                        return E( runner_error )
                               << "AUto find for this type of servo not implemented";
        }
}

int main( int argc, char* argv[] )
{

        rclcpp::init( argc, argv );

        auto node = rclcpp::Node::make_shared(
            "servo_runner",
            rclcpp::NodeOptions()
                .enable_rosout( true )
                .allow_undeclared_parameters( true )
                .automatically_declare_parameters_from_overrides( true ) );

        using tmp_type = em::either<
            std::tuple< servo_tester_config, std::optional< serial_connection > >,
            runner_error >;

        get_config< servo_tester_config >( *node, "" )
            .bind_left( [&]( const servo_tester_config& conf ) -> tmp_type {
                    if ( conf.opt_dev ) {
                            SCHPIN_INFO_LOG( "servo_runner", "Opening " << *conf.opt_dev );
                            return serial_connection::make( *conf.opt_dev )
                                .convert_left( [&]( serial_connection c ) {
                                        SCHPIN_INFO_LOG( "servo_runner", "Connection created" );
                                        return std::make_tuple( conf, std::make_optional( c ) );
                                } )
                                .construct_right< runner_error >();
                    }
                    return std::make_tuple( conf, std::optional< serial_connection >() );
            } )
            .bind_left(
                [&]( std::tuple< servo_tester_config, std::optional< serial_connection > > pack )
                    -> tmp_type {
                        auto& [con, opt_ser] = pack;

                        if ( con.opt_ids ) {
                                SCHPIN_INFO_LOG(
                                    "servo_runner",
                                    "Using provided ids: " << em::view{ *con.opt_ids } );
                                return pack;
                        }
                        if ( opt_ser ) {
                                return find_servos( con.type, *opt_ser )
                                    .convert_left( [&]( std::vector< servo_id > sids ) {
                                            con.opt_ids = std::move( sids );
                                            return pack;
                                    } );
                        }
                        return E( runner_error ) << "No servo ids";
                } )
            .match(
                [&]( std::tuple< servo_tester_config, std::optional< serial_connection > > pack ) {
                        auto [conf, opt_ser] = pack;
                        servo_runner runner{ conf, opt_ser };

                        if ( conf.opt_source ) {
                                runner.run_file( *conf.opt_source );
                                return;
                        }

                        while ( true ) {
                                runner.tick();
                        }
                },
                [&]( auto e ) {
                        std::cerr << e << std::endl;
                } );
}
