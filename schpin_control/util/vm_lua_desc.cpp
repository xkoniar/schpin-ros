// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/lua_vm/def.h"

#include <iostream>
#include <schpin_lib/lua/help.h>

// TODO: this is fugly
//
// - refactor the printing

using namespace schpin;

int main( int argc, char* argv[] )
{
        nlohmann::json res = slua_control_vm_def::def{};

        std::string output_type;

        if ( argc == 1 ) {
                output_type = "help";
        } else if ( argc == 2 ) {
                output_type = argv[1];
        } else {
                std::cerr << "You have to specificy either no argument, or output type: help, json";
                return 1;
        }

        if ( output_type == "help" ) {
                print_help( std::cout, res );
        } else if ( output_type == "json" ) {
                std::cout << res;
        } else {
                std::cerr << "Unknown output type: " << output_type;
                return 1;
        }
        return 0;
}
