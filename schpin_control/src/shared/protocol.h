// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/base.h"
#include "fw/config.h"
#include "fw/lewan/config.h"
#include "fw/lewan/protocol.h"

#include <emlabcpp/protocol/streams.h>
#include <schpin_lib_exp/scaled.h>

#pragma once

namespace schpin
{
enum messages_enum : uint8_t
{
        PING                = 1,
        PONG                = 2,
        GOTO                = 3,
        STOP                = 4,
        SYNC                = 5,
        SPIN                = 6,
        CMD_HASH            = 7,
        TELEM               = 8,
        POSITION            = 9,
        ERROR               = 10,
        ACTIVATE            = 11,
        TEMP                = 12,
        VOLTAGE             = 13,
        VELOCITY            = 14,
        ID                  = 15,
        SERVO               = 18,
        FINISHED            = 19,
        INPUT_RANGE         = 20,
        SERVO_RANGE         = 21,
        ANGLE_CHANGE_REPORT = 22,
        INPUT_BUFFER_SIZE   = 23,
        EVENT_BUFFER_SIZE   = 24,
        TASK                = 25,
        DEACTIVATE          = 26,
        CONFIG_SET          = 27,
        CONFIG_GET          = 28
};

enum error_protocol_enum : uint8_t
{
        SERVO_NOT_ACTIVE_E             = 1,
        INPUT_EVENT_BUFFER_FULL_E      = 2,
        SERVO_CAPACITY_USED_E          = 3,
        SERVO_MSG_UNKNOWN_E            = 4,
        INPUT_CONTROL_BUFFER_FULL_E    = 5,
        INPUT_CONTROL_INVALID_SIZE_E   = 6,
        INPUT_CONTROL_UNKNOWN_E        = 7,
        INPUT_INVALID_VALUE_E          = 8,
        SERVO_FEEDBACK_INVALID_SIZE_E  = 9,
        SERVO_FEEDBACK_UNKNOWN_E       = 10,
        SERVO_ID_MISSING_E             = 11,
        SERVO_UNDEFINED_POSITION_E     = 12,
        SERVO_FEEDBACK_INVALID_VALUE_E = 13,
        SERVO_NOT_RESPONDING           = 14,
        EVENT_BUFFER_NOT_EMPTY_E       = 15,
        CONFIG_E                       = 16,
        SERVO_ID_INVALID_E             = 17
};

// TODO: revision of errors
struct error_group
  : em::protocol_command_group<
        em::PROTOCOL_BIG_ENDIAN,
        em::protocol_command<
            SERVO_FEEDBACK_INVALID_VALUE_E >::with_args< lewan_message_enum, uint32_t >,
        em::protocol_command< INPUT_EVENT_BUFFER_FULL_E >,
        em::protocol_command< SERVO_NOT_ACTIVE_E >::with_args< servo_id >,
        em::protocol_command< SERVO_CAPACITY_USED_E >,
        em::protocol_command< SERVO_MSG_UNKNOWN_E >::with_args< lewan_master_value >,
        em::protocol_command< INPUT_CONTROL_BUFFER_FULL_E >,
        em::protocol_command< INPUT_CONTROL_INVALID_SIZE_E >,
        em::protocol_command< INPUT_CONTROL_UNKNOWN_E >,
        em::protocol_command< INPUT_INVALID_VALUE_E >::with_args< uint16_t >,
        em::protocol_command< SERVO_ID_MISSING_E >::with_args< servo_id >,
        em::protocol_command< EVENT_BUFFER_NOT_EMPTY_E >,
        em::protocol_command< CONFIG_E >::with_args< em::protocol_error_record > >
{
};

using error_message = typename error_group::message_type;

// TODO: rename 'master' to 'node'

struct servo_error_group
  : em::protocol_command_group<
        em::PROTOCOL_BIG_ENDIAN,
        em::protocol_command< SERVO_FEEDBACK_UNKNOWN_E >,
        em::protocol_command< INPUT_CONTROL_INVALID_SIZE_E >,
        em::protocol_command< SERVO_FEEDBACK_INVALID_SIZE_E >,
        em::protocol_command< INPUT_INVALID_VALUE_E >::with_args< messages_enum, uint32_t >,
        em::protocol_command< SERVO_UNDEFINED_POSITION_E >,
        em::protocol_command< CONFIG_E >,
        em::protocol_command< SERVO_ID_INVALID_E > >
{
};

using servo_error_message = typename servo_error_group::message_type;
using servo_error_variant = typename servo_error_group::value_type;

struct master_servo_group
  : em::protocol_command_group<
        em::PROTOCOL_BIG_ENDIAN,
        em::protocol_command< GOTO >::with_args< bangle, btime, end_condition >,
        em::protocol_command< SPIN >::with_args< bangular_velocity, btime >,
        em::protocol_command< POSITION >,
        em::protocol_command< TEMP >,
        em::protocol_command< VOLTAGE >,
        em::protocol_command< VELOCITY >,
        em::protocol_command< TASK >,
        em::protocol_command< ID >::with_args< servo_id >,
        em::protocol_command< CONFIG_SET >::with_args< config_addr, config_buffer >,
        em::protocol_command< CONFIG_GET >::with_args< config_addr > >
{
};

using master_servo_message = typename master_servo_group::message_type;
using master_servo_variant = typename master_servo_group::value_type;

struct servo_master_group
  : em::protocol_command_group<
        em::PROTOCOL_BIG_ENDIAN,
        em::protocol_command< POSITION >::with_args< bangle >,
        em::protocol_command< TEMP >::with_args< btemp >,
        em::protocol_command< VOLTAGE >::with_args< bvoltage >,
        em::protocol_command< VELOCITY >::with_args< bangular_velocity >,
        em::protocol_command< TASK >::with_args< servo_task_enum >,
        em::protocol_command< CONFIG_SET >::with_args< config_addr >,
        em::protocol_command< CONFIG_GET >::with_args< config_addr, config_buffer >,
        em::protocol_command< ERROR >::with_args< ctl_id, servo_error_group >,
        em::protocol_command< TELEM >::with_args< bangle, bangle, btime > >
{
};

using servo_master_message = typename servo_master_group::message_type;
using servo_master_variant = typename servo_master_group::value_type;

template < error_protocol_enum ID, typename... Args >
inline servo_master_variant make_servo_master_error( ctl_id cid, Args... args )
{
        return servo_master_group::make_val< ERROR >(
            cid, servo_error_group::make_val< ID >( args... ) );
}

struct node_fw_group
  : em::protocol_command_group<
        em::PROTOCOL_BIG_ENDIAN,
        em::protocol_command< PING >,
        em::protocol_command< STOP >,
        em::protocol_command< ACTIVATE >::with_args< servo_addr >,
        em::protocol_command< DEACTIVATE >::with_args< servo_id >,
        em::protocol_command< SYNC >::with_args< servo_flags >,
        em::protocol_command< SERVO >::with_args< servo_id, master_servo_group >,
        em::protocol_command< CONFIG_SET >::with_args< config_addr, config_buffer >,
        em::protocol_command< CONFIG_GET >::with_args< config_addr > >
{
};

using node_fw_group_message = typename node_fw_group::message_type;
using node_fw_group_value   = typename node_fw_group::value_type;

enum activated_enum : uint8_t
{
        ACTIVATED      = 0,
        ACTIVATING     = 1,
        WAS_ACTIVATING = 2,
        WAS_ACTIVATED  = 3,
        MISSING        = 4,
};

struct fw_node_group
  : em::protocol_command_group<
        em::PROTOCOL_BIG_ENDIAN,
        em::protocol_command< PONG >,
        // command< CMD_HASH >::with_args< commandHash >,
        em::protocol_command< ACTIVATE >::with_args< servo_id, activated_enum >,
        em::protocol_command< FINISHED >::with_args< ctl_id >,
        em::protocol_command< INPUT_BUFFER_SIZE >::with_args< uint16_t >,
        em::protocol_command< EVENT_BUFFER_SIZE >::with_args< uint16_t >,
        em::protocol_command< ERROR >::with_args< ctl_id, error_group >,
        em::protocol_command< SERVO >::with_args< servo_id, servo_master_group >,
        em::protocol_command< CONFIG_SET >::with_args< config_addr >,
        em::protocol_command< CONFIG_GET >::with_args< config_addr, config_buffer > >
{
};

using fw_node_group_message = typename fw_node_group::message_type;
using fw_node_group_value   = typename fw_node_group::value_type;

template < typename Subitem >
using control_protocol =
    em::protocol_tuple< em::PROTOCOL_LITTLE_ENDIAN, ctl_id, Subitem, checksum >;

struct fw_node_protocol : control_protocol< fw_node_group >
{
};
using fw_node_message = typename fw_node_protocol::message_type;
using fw_node_tuple   = typename fw_node_protocol::value_type;

struct node_fw_protocol : control_protocol< node_fw_group >
{
};
using node_fw_message = typename node_fw_protocol::message_type;
using node_fw_tuple   = typename node_fw_protocol::value_type;

node_fw_message                                        node_fw_serialize( const node_fw_tuple& );
em::either< node_fw_tuple, em::protocol_error_record > node_fw_extract( const node_fw_message& );

fw_node_message                                        fw_node_serialize( const fw_node_tuple& );
em::either< fw_node_tuple, em::protocol_error_record > fw_node_extract( const fw_node_message& );

}  // namespace schpin
