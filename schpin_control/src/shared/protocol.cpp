// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "shared/protocol.h"

#include <emlabcpp/protocol/handler.h>

namespace schpin
{
node_fw_message node_fw_serialize( const node_fw_tuple& val )
{
        return em::protocol_handler< node_fw_protocol >::serialize( val );
}
em::either< node_fw_tuple, em::protocol_error_record > node_fw_extract( const node_fw_message& m )
{
        return em::protocol_handler< node_fw_protocol >::extract( m );
}

fw_node_message fw_node_serialize( const fw_node_tuple& val )
{
        return em::protocol_handler< fw_node_protocol >::serialize( val );
}
em::either< fw_node_tuple, em::protocol_error_record > fw_node_extract( const fw_node_message& m )
{
        return em::protocol_handler< fw_node_protocol >::extract( m );
}
}  // namespace schpin
