// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cerrno>
#include <emlabcpp/iterators/convert.h>
#include <fcntl.h>
#include <filesystem>
#include <schpin_lib/error.h>
#include <termios.h>
#include <unistd.h>

#pragma once

namespace schpin
{

using serial_error = error< struct serial_error_tag >;

class serial_connection
{
        int fd_;

        serial_connection( int fd )
          : fd_( fd )
        {
        }

public:
        static em::either< serial_connection, serial_error >
        make( const std::filesystem::path& filename )
        {
                int fd = open( filename.c_str(), O_RDWR );

                if ( fd < 0 ) {
                        return {
                            E( serial_error ) << "Error " << errno << " when opening file "
                                              << filename << " :" << strerror( errno ) };
                }

                struct termios tty = {};

                if ( tcgetattr( fd, &tty ) != 0 ) {
                        return {
                            E( serial_error )
                            << "Error " << errno << " when getting attributes of console "
                            << filename << " :" << strerror( errno ) };
                }

                auto set = []( auto& flags, unsigned flag ) {
                        flags |= flag;
                };
                auto clr = []( auto& flags, unsigned flag ) {
                        flags &= ~flag;
                };

                clr( tty.c_cflag, PARENB );
                clr( tty.c_cflag, CSTOPB );
                set( tty.c_cflag, CS8 );
                clr( tty.c_cflag, CRTSCTS );
                set( tty.c_cflag, CREAD | CLOCAL );

                clr( tty.c_lflag, ICANON );
                clr( tty.c_lflag, ECHO );
                clr( tty.c_lflag, ECHOE );
                clr( tty.c_lflag, ECHONL );
                clr( tty.c_lflag, ISIG );
                clr( tty.c_iflag, IXON | IXOFF | IXANY );
                clr( tty.c_iflag, IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL );

                clr( tty.c_oflag, OPOST );
                clr( tty.c_oflag, ONLCR );

                tty.c_cc[VTIME] = 3;  // 2 deciseconds
                tty.c_cc[VMIN]  = 0;

                cfsetispeed( &tty, B115200 );
                cfsetospeed( &tty, B115200 );

                if ( tcsetattr( fd, TCSANOW, &tty ) != 0 ) {
                        return {
                            E( serial_error )
                            << "Error " << errno << " when settings attributes of console "
                            << filename << " :" << strerror( errno ) };
                }

                return serial_connection{ fd };
        }

        template < typename Message >
        void write( const Message& m )
        {
                auto res = ::write( fd_, m.begin(), m.size() );
                em::ignore( res );
        }

        em::either< std::vector< uint8_t >, serial_error > read( std::size_t n ) const
        {
                SCHPIN_ASSERT( n < 128 );
                std::array< uint8_t, 128 > buff;
                auto                       start = std::chrono::steady_clock::now();
                ssize_t                    m     = ::read( fd_, buff.begin(), n );
                auto                       end   = std::chrono::steady_clock::now();
                if ( m < 0 ) {
                        return E( serial_error )
                               << "Error " << errno
                               << " when reading the console:" << strerror( errno );
                }
                if ( m == 0 &&
                     std::chrono::duration_cast< std::chrono::milliseconds >( end - start )
                             .count() > 250 ) {
                        return E( serial_error ) << "Timeouted";
                }

                std::vector< uint8_t > res;

                std::copy_n( buff.begin(), m, std::back_inserter( res ) );

                return { res };
        }

        template < typename Sequencer >
        em::either< typename Sequencer::message_type, serial_error > read_with_sequencer()
        {
                using message_type = typename Sequencer::message_type;
                Sequencer seqe;

                std::optional< em::either< message_type, serial_error > > opt_res;
                std::size_t to_read = Sequencer::fixed_size;
                while ( !opt_res ) {
                        read( to_read ).match(
                            [&]( std::vector< uint8_t > data ) {
                                    seqe.load_data( em::view{ data } )
                                        .match(
                                            [&]( auto req ) {
                                                    to_read = req;
                                            },
                                            [&]( message_type m ) {
                                                    opt_res.emplace( m );
                                            } );
                            },
                            [&]( auto err ) {
                                    opt_res = err;
                            } );
                }
                return *opt_res;
        }
};

}  // namespace schpin
