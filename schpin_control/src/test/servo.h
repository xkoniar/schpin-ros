// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <emlabcpp/match.h>

namespace schpin
{

// TODO: this needs rewrite :)
// : split the functionality into separate classes
class sim_servo
{
        lewan_servo_id id_ = lewan_servo_id::get< 0 >();

        lewan_angle        goal_;
        lewan_time         goal_t_;
        lewan_angle_offset a_offset_;
        lewan_angle        from_limit_ = lewan_angle::get< 0 >();
        lewan_angle        to_limit_   = lewan_angle::get< 1000 >();

        lewan_voltage vfrom_limit_;
        lewan_voltage vto_limit_;
        lewan_voltage actual_v_;

        lewan_temp max_temp_;
        lewan_temp actual_temp_ = lewan_temp::get< 50 >();

        lewan_servo_mode mode_;

        lewan_angular_velocity velocity_;

        lewan_load      load_;
        lewan_led_mode  led_mode_;
        lewan_led_error led_error_;

        float max_velocity_ = 100.f;

        float goal_time_ = 0.f;

        float track_ = 0.f;

        float goal_angle_       = 0.f;
        float real_angle_       = 0.f;  // use degrees for simplicty
        float move_start_angle_ = 0;

        float angle_step_ = 0.24f;

        // 1500 total lewan_angle steps
        uint32_t last_time_       = 0;
        uint32_t move_start_time_ = 0;

        bool follow_goal_ = false;

        float mod( float v )
        {
                if ( v < 0 ) {
                        v += 360;
                }
                return static_cast< float >( std::fmod( v, 360 ) );
        }

        void setup_position_spin( lewan_angle a, lewan_time t )
        {
                goal_       = a;
                goal_angle_ = static_cast< float >( *goal_ ) * angle_step_;

                float min_time_f   = std::fabs( goal_angle_ - real_angle_ ) / max_velocity_;
                auto  opt_min_time = lewan_time::make( min_time_f * 1000 );
                goal_t_            = std::max( t, *opt_min_time );
                goal_time_         = static_cast< float >( *goal_t_ );

                track_ = goal_angle_ - real_angle_;
                if ( real_angle_ >= 300 ) {
                        float fix = 360 - real_angle_;
                        track_    = goal_angle_ + fix;
                }
        }

public:
        sim_servo( lewan_servo_id id, clk_time now, float a = 0 )
          : id_( id )
          , last_time_( *now )
        {
                manual_set_angle( a );
        }

        void tick( clk_time input_time )
        {
                uint32_t clock_time = *input_time;
                if ( mode_ == lewan_motor_mode ) {
                        float tdiff = static_cast< float >( clock_time - last_time_ ) / 1000.f;
                        if ( tdiff < 0.003f ) {  // it's better to give it at least 3 milliseconds
                                return;
                        }
                        float fvel = static_cast< float >( *velocity_ ) * angle_step_;
                        /*
                        SCHPIN_INFO_LOG(
                            "test.servo",
                            "motor-mode: id:" << id_ << " clocks " << clock_time << " - "
                                              << last_time_ << " = " << clock_time - last_time_
                                              << " tdiff: " << tdiff << " velocity: " << velocity_
                                              << " fvel: " << fvel << " fvel*tdiff " << fvel * tdiff
                                              << " real_angle_: " << real_angle_ );
                                              */
                        real_angle_ += fvel * tdiff;
                        real_angle_ = mod( real_angle_ );

                } else if ( follow_goal_ ) {  // POSITION MODE ACTIVE
                        if ( *goal_t_ == 0 ) {
                                follow_goal_ = false;
                                return;
                        }
                        float f =
                            static_cast< float >( clock_time - move_start_time_ ) / goal_time_;
                        f = std::min( f, 1.f );

                        float diff = std::fabs( goal_angle_ - real_angle_ );
                        if ( diff > 180 ) {
                                diff = 360 - diff;
                        }

                        if ( diff < 0.001f ) {
                                follow_goal_ = false;
                                real_angle_  = goal_angle_;
                                return;
                        }
                        /*
                        SCHPIN_INFO_LOG(
                            "test.servo",
                            "servo-mode: id: " << id_ << " clocks " << clock_time << " - "
                                               << move_start_time_ << " = "
                                               << clock_time - move_start_time_
                                               << " goal_t_:" << goal_time_ << " f: " << f
                                               << " move: " << goal_angle_ << " - " << real_angle_
                                               << " diff: " << diff << " track_: " << track_
                                               << " moved by: " << f * track_ );
                        */
                        real_angle_ = mod( move_start_angle_ + f * track_ );
                }

                last_time_ = clock_time;
        }

        float get_rad_angle() const
        {
                return real_angle_ * 3.141592f / 180.f;
        }

        float get_angle() const
        {
                return real_angle_;
        }

        void manual_set_angle( float a )
        {
                real_angle_ = static_cast< float >( std::fmod( a * 180.f / 3.141592f, 360 ) );
        }

        std::optional< lewan_master_message >
        on_cmd( clk_time input_time, lewan_servo_id sid, master_lewan_variant v )
        {
                tick( input_time );
                uint32_t                            clock_time = *input_time;
                std::optional< lewan_master_value > res;
                em::apply_on_match(
                    v,
                    [&]( em::tag< LEWAN_SERVO_MOVE_TIME_WRITE >, lewan_angle a, lewan_time t ) {
                            setup_position_spin( a, t );
                            move_start_time_  = clock_time;
                            move_start_angle_ = real_angle_;
                            follow_goal_      = true;
                    },
                    [&]( em::tag< LEWAN_SERVO_MOVE_TIME_READ > ) {
                            res = make_msg< LEWAN_SERVO_MOVE_TIME_READ >( sid, goal_, goal_t_ );
                    },
                    [&](
                        em::tag< LEWAN_SERVO_MOVE_TIME_WAIT_WRITE >, lewan_angle a, lewan_time t ) {
                            setup_position_spin( a, t );
                            follow_goal_ = false;
                    },
                    [&]( em::tag< LEWAN_SERVO_MOVE_TIME_WAIT_READ > ) {
                            res =
                                make_msg< LEWAN_SERVO_MOVE_TIME_WAIT_READ >( sid, goal_, goal_t_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_MOVE_START > ) {
                            move_start_time_  = clock_time;
                            move_start_angle_ = real_angle_;
                            follow_goal_      = true;
                    },
                    [&]( em::tag< LEWAN_SERVO_MOVE_STOP > ) {
                            follow_goal_ = false;
                    },
                    [&]( em::tag< LEWAN_SERVO_ID_WRITE >, lewan_servo_id sid ) {
                            id_ = sid;
                    },
                    [&]( em::tag< LEWAN_SERVO_ID_READ > ) {
                            res = make_msg< LEWAN_SERVO_ID_READ >( sid, sid );
                    },
                    [&]( em::tag< LEWAN_SERVO_ANGLE_OFFSET_ADJUST >, lewan_angle_offset off ) {
                            a_offset_ = off;
                    },
                    [&]( em::tag< LEWAN_SERVO_ANGLE_OFFSET_WRITE > ) {},
                    [&]( em::tag< LEWAN_SERVO_ANGLE_OFFSET_READ > ) {
                            res = make_msg< LEWAN_SERVO_ANGLE_OFFSET_READ >( sid, a_offset_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_ANGLE_LIMIT_WRITE >,
                         lewan_angle from,
                         lewan_angle to ) {
                            from_limit_ = from;
                            to_limit_   = to;
                    },
                    [&]( em::tag< LEWAN_SERVO_ANGLE_LIMIT_READ > ) {
                            res = make_msg< LEWAN_SERVO_ANGLE_LIMIT_READ >(
                                sid, from_limit_, to_limit_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_VIN_LIMIT_WRITE >,
                         lewan_voltage from,
                         lewan_voltage to ) {
                            vfrom_limit_ = from;
                            vto_limit_   = to;
                    },
                    [&]( em::tag< LEWAN_SERVO_VIN_LIMIT_READ > ) {
                            res = make_msg< LEWAN_SERVO_VIN_LIMIT_READ >(
                                sid, vfrom_limit_, vto_limit_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_TEMP_MAX_LIMIT_WRITE >, lewan_temp limit ) {
                            max_temp_ = limit;
                    },
                    [&]( em::tag< LEWAN_SERVO_TEMP_MAX_LIMIT_READ > ) {
                            res = make_msg< LEWAN_SERVO_TEMP_MAX_LIMIT_READ >( sid, max_temp_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_TEMP_READ > ) {
                            res = make_msg< LEWAN_SERVO_TEMP_READ >( sid, actual_temp_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_VIN_READ > ) {
                            res = make_msg< LEWAN_SERVO_VIN_READ >( sid, actual_v_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_POS_READ > ) {
                            uint32_t real_pos =
                                static_cast< uint32_t >( real_angle_ / angle_step_ );
                            auto opt_pos =
                                lewan_full_angle::make( static_cast< int16_t >( real_pos ) );
                            if ( opt_pos ) {
                                    res = make_msg< LEWAN_SERVO_POS_READ >( sid, *opt_pos );
                                    return;
                            }
                    },
                    [&]( em::tag< LEWAN_SERVO_OR_MOTOR_MODE_WRITE >,
                         lewan_servo_mode m,
                         uint8_t,
                         lewan_angular_velocity av ) {
                            follow_goal_ = false;
                            mode_        = m;
                            velocity_    = av;
                    },
                    [&]( em::tag< LEWAN_SERVO_OR_MOTOR_MODE_READ > ) {
                            res = make_msg< LEWAN_SERVO_OR_MOTOR_MODE_READ >(
                                sid, mode_, uint8_t{ 0 }, velocity_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_LOAD_OR_UNLOAD_WRITE >, lewan_load l ) {
                            load_ = l;
                    },
                    [&]( em::tag< LEWAN_SERVO_LOAD_OR_UNLOAD_READ > ) {
                            res = make_msg< LEWAN_SERVO_LOAD_OR_UNLOAD_READ >( sid, load_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_LED_CTRL_WRITE >, lewan_servo_mode lmode ) {
                            led_mode_ = lmode;
                    },
                    [&]( em::tag< LEWAN_SERVO_LED_CTRL_READ > ) {
                            res = make_msg< LEWAN_SERVO_LED_CTRL_READ >( sid, led_mode_ );
                    },
                    [&]( em::tag< LEWAN_SERVO_LED_ERROR_WRITE >, lewan_led_error le ) {
                            led_error_ = le;
                    },
                    [&]( em::tag< LEWAN_SERVO_LED_ERROR_READ > ) {
                            res = make_msg< LEWAN_SERVO_LED_ERROR_READ >( sid, led_error_ );
                    } );

                if ( !res ) {
                        return {};
                }
                return lewan_master_serialize( *res );
        }

        template < lewan_message_enum ID, typename... Args >
        lewan_master_value make_msg( lewan_servo_id sid, Args... args )
        {
                return lewan_master_protocol::make_val(
                    lewan_header_start_v,
                    lewan_header_start_v,
                    sid,
                    lewan_master_group::make_val< ID >( args... ),
                    lewan_checksum{} );
        }
};
}  // namespace schpin
