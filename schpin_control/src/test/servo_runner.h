#include "fw/dynmx/sequencer.h"
#include "fw/lewan/sequencer.h"
#include "fw/servo/interface.h"
#include "fw/util.h"
#include "node/json_ser.h"
#include "node/lua_vm/extra_ser.h"
#include "shared/serial.h"
#include "test/servo_runner/lua_def.h"

#include <schpin_lib/bucket_memory_resource.h>
#include <schpin_lib/lua/help.h>
#include <schpin_lib/lua/repl.h>
#include <schpin_lib/lua/util.h>
#include <schpin_lib/lua/vm.h>
#include <schpin_lib_exp/config.h>

#pragma once

namespace schpin
{

using runner_error = error< struct runner_error_tag >;

struct servo_tester_config
{
        servo_type                               type;
        std::optional< std::vector< servo_id > > opt_ids;
        std::optional< std::filesystem::path >   opt_dev;
        std::optional< std::filesystem::path >   opt_source;

        static auto ros_param_get()
        {
                return std::make_tuple(
                    ros_param< servo_type >( "stype", "Servo type" ),
                    ros_param< std::optional< std::vector< servo_id > > >( "sid", "Servo id" ),
                    ros_param< std::optional< std::filesystem::path > >(
                        "device", "Device to open for connection" ),
                    ros_param< std::optional< std::filesystem::path > >(
                        "script", "Script to execute instead of the interactive mode" ) );
        }
};

class servo_runner
{
        static constexpr std::size_t size = 32;

        using mem = bucket_memory_resource< size, servo_bucket_size >;

        mem                                memory_;
        std::optional< serial_connection > ser_;
        std::vector< servo_ptr >           servos_;
        slua_vm< slua_runner_def >         vm_;
        slua_repl                          repl_;

        struct visitor : servo_visitor
        {
                servo_runner& runner;

                visitor( servo_runner& srunner )
                  : runner( srunner )
                {
                }

                void to_control( servo_id, const servo_master_variant& v ) override;

                void on_activate( servo_id id ) override
                {
                        SCHPIN_INFO_LOG( "lewan_servo", "Got activation of: " << id );
                }

                void to_servo( s_ctl_id, reply, const master_lewan_message& ) override;

                void to_servo( s_ctl_id, reply, const master_dynmx_message& ) override;
        };

public:
        servo_runner(
            const servo_tester_config&         conf,
            std::optional< serial_connection > opt_ser = {} )
          : ser_( opt_ser )
        {
                SCHPIN_ASSERT( conf.opt_ids );
                for ( servo_id sid : *conf.opt_ids ) {
                        servos_.push_back(
                            make_servo( conf.type, sid, get_system_time(), &memory_ ) );
                }
                init_servos();
        }

        void init_servos()
        {
                SCHPIN_INFO_LOG( "servo_runner", "Initializing servos" );
                visitor vis{ *this };
                bool    finished = false;
                while ( !finished ) {
                        for ( auto& s : servos_ ) {
                                s->tick( get_system_time(), vis );
                        }

                        finished = em::none_of( servos_, [&]( const auto& p ) {
                                return p->is_activating();
                        } );
                }
                SCHPIN_INFO_LOG( "servo_runner", "Init done" );
        }

        void tick();

        void run_file( const std::filesystem::path& file );

        void handle_event( lua_State*, print_help_event );

        std::vector< servo_id > handle_event( lua_State*, get_ids_event );

        float handle_event( lua_State*, get_time_event )
        {
                return get_system_time().as_float();
        }

        void handle_event( lua_State*, quit_event e )
        {
                std::exit( e.res );
        }

        void handle_event( lua_State*, print_config_event );

        nlohmann::json handle_event( lua_State* lua_ptr, msg_event e );
};

}  // namespace schpin
