#include "node/lua_vm/def.h"

#pragma once

namespace schpin
{

struct get_ids_event
{
};

struct get_ids_bind
{
        static constexpr std::string_view name = "get_ids";
        static constexpr std::string_view desc = "gets ids of activated servos";

        inline static const auto arg = slua_struct< get_ids_event >();
        inline static const auto res = slua_result< std::vector< servo_id > >();
};

struct msg_event
{
        servo_id       sid;
        nlohmann::json j;
};

struct msg_bind
{

        static constexpr std::string_view name = "msg";
        static constexpr std::string_view desc = "creates message out of json";

        inline static const auto arg = slua_struct< msg_event >(
            lua_key_arg< servo_id >{ "id", "id of servo" },
            lua_key_arg< nlohmann::json >{ "json", "data" } );
        inline static const auto res = slua_result< nlohmann::json >{ "Response message" };
};

struct print_config_event
{
        servo_id sid;
};

struct print_config_bind
{
        static constexpr std::string_view name = "print_config";
        static constexpr std::string_view desc = "print_config information about servo";

        inline static const auto arg =
            slua_struct< print_config_event >( lua_key_arg< servo_id >{ "id", "id of servo" } );
        inline static const no_return_type res{};
};

struct quit_event
{
        int res;
};

struct quit_bind
{
        static constexpr std::string_view name = "quit";
        static constexpr std::string_view desc = "closes the app";

        inline static const auto arg =
            slua_struct< quit_event >( lua_key_arg< int >{ "res", "Result return id of the app" } );
        inline static const no_return_type res{};
};

struct print_help_event
{
};

struct print_help_bind
{
        static constexpr std::string_view name = "help";
        static constexpr std::string_view desc = "Prints binding help";

        inline static const auto           arg = slua_struct< print_help_event >();
        inline static const no_return_type res{};
};

struct get_time_event
{
};

struct get_time_bind
{
        static constexpr std::string_view name = "time";
        static constexpr std::string_view desc = "Returns time in seconds as float";

        inline static const auto arg = slua_struct< get_time_event >();
        inline static const auto res = slua_result< float >{ "The time" };
};

enum sr_lua_call_enum
{
        SR_CALL_ON_CMD
};

struct sr_on_cmd_bind
{
        static constexpr auto             call_id = CALL_ON_CMD;
        static constexpr std::string_view name    = "_on_cmd";
        static constexpr std::string_view desc =
            "Function is called each time msg is received from servo";

        // TODO: fix this...multiple args are needed
        inline static const auto arg = lua_arg< std::tuple< servo_id, servo_master_variant > >{
            "servo message split to string" };

        // TODO: the off by one indexing is just bad...
        static constexpr std::string_view code = R"EOF(
            if on_cmd~=nil then 
                on_cmd(arg[0], arg[1])
            end
        )EOF";
};

struct slua_runner_def
{
        using def = std::tuple<
            slua_callback< msg_bind >,
            slua_callback< get_ids_bind >,
            slua_callback< print_config_bind >,
            slua_callback< print_help_bind >,
            slua_callback< quit_bind >,
            slua_callback< get_time_bind >,
            slua_native_proc_call< sr_on_cmd_bind > >;
};

}  // namespace schpin
