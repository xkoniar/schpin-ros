#include "test/servo_runner.h"

#include <emlabcpp/protocol/streams.h>

using namespace schpin;

void servo_runner::visitor::to_servo( s_ctl_id msg_id, reply r, const master_lewan_message& m )
{
        SCHPIN_ASSERT( runner.ser_ );

        runner.ser_->write( m );
        if ( r == reply::NO ) {
                std::this_thread::sleep_for( std::chrono::milliseconds( 25 ) );
                return;
        }
        runner.ser_->read_with_sequencer< lewan_sequencer< lewan_master_message > >().match(
            [&]( lewan_master_message m ) {
                    lewan_master_extract( m ).match(
                        [&]( std::tuple<
                             lewan_header_start,
                             lewan_header_start,
                             lewan_servo_id,
                             lewan_master_variant,
                             lewan_checksum > pack ) {
                                auto     clk_time = get_system_time();
                                servo_id sid      = *std::get< lewan_servo_id >( pack );

                                auto iter = em::find_if( runner.servos_, [&]( const auto& s ) {
                                        return s->get_id() == sid;
                                } );
                                SCHPIN_ASSERT( iter != runner.servos_.end() );
                                ( *iter )->on_servo_receive(
                                    clk_time, msg_id, std::get< lewan_master_variant >( pack ) );
                        },
                        [&]( auto err ) {
                                SCHPIN_ERROR_LOG( "lewan_servo", "Lewan prot error: " << err );
                        } );
            },
            [&]( auto err ) {
                    SCHPIN_ERROR_LOG( "control", "Read error: " << err );
            } );
}

void servo_runner::visitor::to_servo( s_ctl_id msg_id, reply, const master_dynmx_message& m )
{
        SCHPIN_ASSERT( runner.ser_ );

        runner.ser_->write( m );

        runner.ser_->read_with_sequencer< dynmx_sequencer< dynmx_master_message > >()
            .convert_right( []( auto e ) {
                    return to_string( e );
            } )
            .bind_left( [&]( dynmx_master_message msg ) {
                    return dynmx_master_extract( msg ).convert_right( []( auto e ) {
                            return to_string( e );
                    } );
            } )
            .match(
                [&]( std::tuple< dynmx_header, dynmx_servo_id, dynmx_master_subvalue, dynmx_crc >
                         pack ) {
                        auto tpl = std::get< dynmx_master_subvalue >( pack );

                        servo_id sid = *std::get< dynmx_servo_id >( pack );

                        auto iter = em::find_if( runner.servos_, [&]( const auto& s ) {
                                return s->get_id() == sid;
                        } );
                        if ( iter == runner.servos_.end() ) {
                                SCHPIN_ERROR_LOG(
                                    "servo_runner", "Got reply for nonexistent servo: " << sid );
                                return;
                        }

                        ( *iter )->on_servo_receive(
                            get_system_time(), msg_id, servo_receive_variant{ tpl } );
                },
                [&]( auto err ) {
                        SCHPIN_ERROR_LOG( "servo_runner", err );
                } );
}

void servo_runner::visitor::to_control( servo_id sid, const servo_master_variant& v )
{
        runner.vm_
            .lua_func_call< SR_CALL_ON_CMD >(
                [&]( lua_State* lua_ptr, auto event ) {
                        return runner.handle_event( lua_ptr, event );
                },
                std::make_tuple( sid, v ) )
            .optionally_log();
}

void servo_runner::tick()
{
        visitor vis{ *this };
        for ( auto& s : servos_ ) {
                s->tick( get_system_time(), vis );
                s->poll( get_system_time(), 0, vis );
        }
        std::string msg = repl_.prompt();
        vm_.load( msg ).optionally_log();
        vm_.full_exec( [&]( lua_State* lua_state, auto event ) {
                   return handle_event( lua_state, event );
           } )
            .optionally_log();
}
void servo_runner::run_file( const std::filesystem::path& file )
{
        auto opt_e = vm_.load( file );
        if ( opt_e ) {
                opt_e.optionally_log();
                return;
        }
        bool finished = false;

        visitor vis{ *this };
        while ( !finished ) {
                for ( auto& s : servos_ ) {
                        s->tick( get_system_time(), vis );
                        s->poll( get_system_time(), 0, vis );
                }

                auto var = vm_.exec( [&]( lua_State* lua_ptr, auto event ) {
                        return handle_event( lua_ptr, event );
                } );
                em::match(
                    var,
                    [&]( lua_res res ) {
                            finished = res == lua_res::FINISHED;
                    },
                    [&]( const slua_error& err ) {
                            finished = true;
                            SCHPIN_ERROR_LOG( "runner", "Slua error: " << err );
                    } );
                std::this_thread::sleep_for( std::chrono::milliseconds{ 1 } );
        }
}
std::vector< servo_id > servo_runner::handle_event( lua_State*, get_ids_event )
{
        return map_f_to_v( servos_, [&]( const auto& s ) {
                return s->get_id();
        } );
}
void servo_runner::handle_event( lua_State*, print_help_event )
{
        nlohmann::json res = typename slua_runner_def::def{};
        print_help( std::cout, res );
}
void servo_runner::handle_event( lua_State*, print_config_event e )
{
        auto iter = em::find_if( servos_, [&]( const auto& s ) {
                return s->get_id() == e.sid;
        } );
        SCHPIN_ASSERT( iter != servos_.end() );
        ( *iter )->dump_info();
}
nlohmann::json servo_runner::handle_event( lua_State* lua_ptr, msg_event e )
{
        nlohmann::json res;

        master_servo_variant var;

        json_guard( [&]() {
                var = e.j.get< master_servo_variant >();
        } ).optionally( [&]( auto e ) {
                lua_error_stream( lua_ptr, "Json error: ", e );
        } );

        auto iter = em::find_if( servos_, [&]( const auto& s ) {
                return s->get_id() == e.sid;
        } );
        SCHPIN_ASSERT( iter != servos_.end() );
        ( *iter )
            ->on_servo_packet( ctl_id{ 0 }, var )
            .match(
                [&]( servo_events_variant var ) {
                        visitor vis{ *this };
                        ( *iter )->on_event( ctl_id{ 0 }, get_system_time(), var, vis );
                },
                [&]( servo_master_variant var ) {
                        json_guard( [&]() {
                                res = var;
                        } ).optionally( [&]( json_storage_error e ) {
                                lua_error_stream( lua_ptr, "Json error: ", e );
                        } );
                } );
        return res;
}
