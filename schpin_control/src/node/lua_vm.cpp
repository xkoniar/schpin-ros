// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/lua_vm.h"

#include "node/lua_vm/def.h"

#define LUA_USE_APICHECK 1

#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <schpin_lib/lua/pose.h>
#include <schpin_lib/lua/print.h>
#include <schpin_lib/lua/util.h>
#include <schpin_lib/lua/vm.h>

using namespace schpin;
struct lua_vm_event_handler
{
        const node_context& context;
        rclcpp::Node&       rcl_node;
        sm_feedback_f       fb_fun;

        const servo& lua_find_servo( lua_State* lua_ptr, std::string joint_name )
        {
                auto iter = em::find_if( context.sgroup.get_servos(), [&]( const servo& s ) {
                        return s->joint_name == joint_name;
                } );
                // TODO: drop lua error stream entirely
                if ( iter == context.sgroup.get_servos().end() ) {
                        lua_error_stream(
                            lua_ptr, "Failed to find servo for the joint", joint_name );
                }

                return *iter;
        }

        const leg_view&
        lua_find_leg( lua_State* lua_ptr, const robot_view& rview, std::string name )
        {
                const auto& legs = rview.get_legs();
                auto        iter = em::find_if( legs, [&]( const leg_view& s ) {  //
                        return s->get_name() == name;
                } );
                if ( iter == legs.end() ) {
                        lua_error_stream( lua_ptr, "Failed to find leg ", name );
                }
                return *iter;
        }

        // TODO: remove the default value for end condition
        opt_error< leg_model_error > move_leg_to(
            lua_State*              lua_ptr,
            const leg_view&         l,
            point< 3, robot_frame > target,
            std::size_t             steps,
            btime                   t,
            end_condition           end = ON_TIME )
        {
                return l.get_angles( target, steps )
                    .convert_left(
                        [&]( dof_array< em::angle > angles ) -> opt_error< leg_model_error > {
                                auto opt_error = l->check_angles_range( angles );
                                if ( opt_error ) {
                                        return opt_error;
                                }

                                SCHPIN_INFO_LOG(
                                    "lua_vm",
                                    "IK target " << l->get_name() << ": " << target
                                                 << " sol: " << l->get_tip_position( angles )
                                                 << " - " << angles );

                                SCHPIN_ASSERT( angles.size() == l->get_revolute_mapping().size() );
                                for ( auto [an, j_ptr] :
                                      em::zip( angles, l->get_revolute_mapping() ) ) {
                                        const servo& s = lua_find_servo( lua_ptr, j_ptr->name );
                                        fb_fun( s.make_goto_value(
                                            bangle::from_float( *an ), t, end ) );
                                }
                                return {};
                        } )
                    .convert_right( [&]( const ik_error& e ) -> opt_error< leg_model_error > {
                            return {
                                E( leg_model_error ).attach( e ) << "Failed to move leg due "
                                                                    "to IK errors" };
                    } )
                    .join();
        }

        std::size_t operator()( lua_State*, get_system_time_event )
        {
                return static_cast< std::size_t >( rcl_node.now().seconds() * 1000. );
        }

        dof_array< em::angle > operator()( lua_State* lua_ptr, const get_leg_state_event& e )
        {
                robot_view      rview{ context.robot, context.rstate };
                const leg_view& l = lua_find_leg( lua_ptr, rview, e.leg );
                return l.get_angles();
        }

        point< 3, robot_frame > operator()( lua_State* lua_ptr, const get_tip_pos_event& e )
        {
                robot_view      rview{ context.robot, context.rstate };
                const leg_view& l = lua_find_leg( lua_ptr, rview, e.leg );
                return l->get_tip_position( e.angles );
        }

        void operator()( lua_State* lua_ptr, const joint_goto_event& e )
        {
                for ( const std::string& jname : e.joints ) {
                        const servo& s = lua_find_servo( lua_ptr, jname );
                        fb_fun( s.make_goto_value( e.pos, e.time, e.end ) );
                }
        }
        void operator()( lua_State* lua_ptr, const joint_spin_event& e )
        {
                for ( const std::string& jname : e.joints ) {
                        const servo& s = lua_find_servo( lua_ptr, jname );
                        fb_fun( s.make_spin_value( e.vel, e.t ) );
                }
        }
        void operator()( lua_State* lua_ptr, const request_pos_event& e )
        {
                for ( const std::string& jname : e.joints ) {
                        const servo& s = lua_find_servo( lua_ptr, jname );
                        fb_fun( s.make_pos_value() );
                }
        }
        ctl_id operator()( lua_State* lua_ptr, const sync_event& e )
        {
                servo_flags flags;

                for ( const std::string& jname : e.joints ) {
                        const servo& s = lua_find_servo( lua_ptr, jname );
                        flags[s->id]   = true;
                }
                ctl_id ctl = fb_fun( node_fw_group::make_val< SYNC >( flags ) );
                SCHPIN_INFO_LOG( "lua", "sync id: " << ctl << " \n flags: " << flags );
                return ctl;
        }
        std::vector< std::string > operator()( lua_State* lua_ptr, get_joints_event e )
        {
                robot_view      rview{ context.robot, context.rstate };
                const leg_view& l = lua_find_leg( lua_ptr, rview, e.leg );

                return map_f_to_v( l->get_revolute_mapping(), [&]( const joint* j ) {
                        return j->name;
                } );
        }
        std::vector< std::string > operator()( lua_State*, get_legs_event )
        {
                return map_f_to_v( context.robot.get_legs(), [&]( const auto& leg ) {
                        return leg.get_name();
                } );
        }
        dof_array< bangle > operator()( lua_State* lua_ptr, get_neutral_angles_event e )
        {
                robot_view      rview{ context.robot, context.rstate };
                const leg_view& l = lua_find_leg( lua_ptr, rview, e.leg );
                return ( *l )->neutral_angles;
        }
        vec< 3 > operator()( lua_State* lua_ptr, get_neutral_dir_event e )
        {
                robot_view      rview{ context.robot, context.rstate };
                const leg_view& l = lua_find_leg( lua_ptr, rview, e.leg );
                return ( *l )->neutral_dir;
        }
        void operator()( lua_State* lua_ptr, const tip_goto_event& e )
        {
                robot_view              rview{ context.robot, context.rstate };
                const leg_view&         l = lua_find_leg( lua_ptr, rview, e.leg );
                point< 3, robot_frame > target =
                    inverse_transform< robot_frame >( e.target, context.rstate.body_pose );
                SCHPIN_INFO_LOG( "lua_vm", "Target: " << e.leg << " , " << target );
                auto opt_error = move_leg_to( lua_ptr, l, target, e.steps, e.time );
                if ( !opt_error ) {
                        return;
                }
                lua_error_stream(
                    lua_ptr,
                    simplified_error{
                        E( slua_error ).attach( *opt_error )
                        << " error when executing tip goto: " << e } );
        }
        void operator()( lua_State* lua_ptr, const relative_tip_goto_event& e )
        {
                robot_view      rview{ context.robot, context.rstate };
                const leg_view& l         = lua_find_leg( lua_ptr, rview, e.leg );
                auto            opt_error = move_leg_to( lua_ptr, l, e.target, e.steps, e.time );
                if ( !opt_error ) {
                        return;
                }
                lua_error_stream(
                    lua_ptr,
                    simplified_error{
                        E( slua_error ).attach( *opt_error )  //
                        << " error when executing relative tip goto: " << e } );
        }
        void operator()( lua_State* lua_ptr, const tip_move_event& e )
        {
                robot_view              rview{ context.robot, context.rstate };
                const leg_view&         l      = lua_find_leg( lua_ptr, rview, e.leg );
                point< 3, robot_frame > target = inverse_transform< robot_frame >(
                    transform( l.get_tip_position(), context.rstate.body_pose ) + e.dir,
                    context.rstate.body_pose );
                auto opt_error = move_leg_to( lua_ptr, l, target, e.steps, e.time );
                if ( !opt_error ) {
                        return;
                }
                lua_error_stream(
                    lua_ptr,
                    simplified_error{
                        E( slua_error ).attach( *opt_error )
                        << " error when executing tip move: " << e
                        << " with state: " << l.get_angles() } );
        }
        // TODO: ddduplication
        void operator()( lua_State* lua_ptr, const body_goto_event& e )
        {
                robot_view          rview{ context.robot, context.rstate };
                pose< world_frame > actual_bpose = context.rstate.body_pose;
                pose< world_frame > target_bpose = slua_get_real_pose( e.var, actual_bpose );

                for ( const leg_view& v : rview.get_legs() ) {
                        point< 3, world_frame > actual =
                            transform( v.get_tip_position(), actual_bpose );
                        point< 3, robot_frame > target =
                            inverse_transform< robot_frame >( actual, target_bpose );

                        auto opt_error = move_leg_to( lua_ptr, v, target, e.steps, e.time );
                        if ( !opt_error ) {
                                continue;
                        }
                        lua_error_stream(
                            lua_ptr,
                            simplified_error{
                                E( slua_error ).attach( *opt_error )
                                << " error when executing tip goto: " << e } );
                }
        }

        void operator()( lua_State* lua_ptr, const body_move_event& e )
        {
                robot_view          rview{ context.robot, context.rstate };
                pose< robot_frame > move_pose{ cast_to_point< robot_frame >( e.dir ), e.rot };
                for ( const leg_view& v : rview.get_legs() ) {
                        point< 3, robot_frame > target = v.get_tip_position();

                        target = inverse_transform< robot_frame >( target, move_pose );

                        auto opt_error = move_leg_to( lua_ptr, v, target, e.steps, e.time );
                        if ( !opt_error ) {
                                continue;
                        }
                        lua_error_stream(
                            lua_ptr,
                            simplified_error{
                                E( slua_error ).attach( *opt_error )
                                << " error when executing tip goto: " << e } );
                }
        }
        void operator()( lua_State*, const set_body_pose_event& e ) const
        {
                fb_fun( e );
        }
        void operator()( lua_State* lua_ptr, const lua_servo_config_event& e )
        {
                for ( const std::string& j : e.ids ) {
                        fb_fun( servo_config_event{ lua_find_servo( lua_ptr, j )->id, e.json } );
                }
        }
        void operator()( lua_State*, const slua_print_event& e )
        {
                SCHPIN_INFO_LOG( "lua_vm", e.msg );
        }
};

struct lua_vm_interactive::impl
{
        using vm_type = slua_vm< slua_control_vm_def >;

        vm_type vm;

        em::either< std::optional< nlohmann::json >, slua_error > exec_bind(
            const std::string&    name,
            const nlohmann::json& j,
            const node_context&   context,
            rclcpp::Node&         node,
            sm_feedback_f         fb_fun )
        {
                return vm.exec_bind( name, j, lua_vm_event_handler{ context, node, fb_fun } );
        }

        opt_error< slua_error > exec(
            const std::string&  code,
            const node_context& context,
            rclcpp::Node&       node,
            sm_feedback_f       fb_fun )
        {
                opt_error< slua_error > opt_res = vm.load( code );
                if ( opt_res ) {
                        return opt_res;
                }

                while ( true ) {
                        std::variant< lua_res, slua_error > exec_res =
                            vm.exec( lua_vm_event_handler{ context, node, fb_fun } );
                        if ( std::holds_alternative< slua_error >( exec_res ) ) {
                                return { std::get< slua_error >( exec_res ) };
                        }
                        if ( std::holds_alternative< lua_res >( exec_res ) &&
                             std::get< lua_res >( exec_res ) == lua_res::FINISHED ) {
                                break;
                        }
                }
                return {};
        }
};

lua_vm_interactive::lua_vm_interactive()
  : impl_ptr_{ std::make_unique< lua_vm_interactive::impl >() }
{
}

std::string lua_vm_interactive::get_json_def() const
{
        nlohmann::json res;
        em::for_each( typename slua_control_vm_def::def{}, [&]( auto item ) {
                res.push_back( item );
        } );
        return res.dump();
}

em::either< std::optional< nlohmann::json >, slua_error > lua_vm_interactive::exec_bind(
    const std::string&    name,
    const nlohmann::json& j,
    const node_context&   context,
    rclcpp::Node&         node,
    sm_feedback_f         fb_fun )
{
        return impl_ptr_->exec_bind( name, j, context, node, fb_fun );
}

opt_error< slua_error > lua_vm_interactive::exec(
    const std::string&  code,
    const node_context& context,
    rclcpp::Node&       node,
    sm_feedback_f       fb_fun )
{
        return impl_ptr_->exec( code, context, node, fb_fun );
}
lua_vm_interactive::~lua_vm_interactive() = default;

struct lua_vm_machine::impl
{
        using vm_type = slua_vm< slua_control_vm_def >;
        vm_type vm;

        opt_error< slua_error > load( const std::string& code )
        {
                return vm.load( code );
        }

        opt_error< slua_error > load( const std::filesystem::path& p )
        {
                return vm.load( p );
        }

        bool finished()
        {
                return vm.load_finished();
        }

        opt_error< slua_error > on_cmd(
            fw_node_group_value v,
            const node_context& context,
            rclcpp::Node&       node,
            sm_feedback_f       fb_fun )
        {
                return vm.lua_func_call< CALL_ON_CMD >(
                    lua_vm_event_handler{ context, node, fb_fun }, v );
        }

        opt_error< slua_error >
        tick( const node_context& context, rclcpp::Node& node, sm_feedback_f send_cb )
        {
                std::variant< lua_res, slua_error > exec_res =
                    vm.exec( lua_vm_event_handler{ context, node, send_cb } );
                if ( std::holds_alternative< slua_error >( exec_res ) ) {
                        return { std::get< slua_error >( exec_res ) };
                }
                return {};
        }
};

lua_vm_machine::lua_vm_machine( const std::string& code )
  : impl_ptr_{ std::make_unique< lua_vm_machine::impl >() }
{
        load( code ).optionally( []( const slua_error& e ) {
                SCHPIN_ERROR_LOG( "lua", simplified_error{ e } );
                SCHPIN_ASSERT( false );
        } );
}
lua_vm_machine::lua_vm_machine( const std::filesystem::path& p )
  : impl_ptr_{ std::make_unique< lua_vm_machine::impl >() }
{
        load( p ).optionally( []( const slua_error& e ) {
                SCHPIN_ERROR_LOG( "lua", simplified_error{ e } );
                SCHPIN_ASSERT( false );
        } );
}

bool lua_vm_machine::finished()
{
        return impl_ptr_->finished();
}

opt_error< slua_error > lua_vm_machine::load( const std::string& code )
{
        return impl_ptr_->load( code );
}

opt_error< slua_error > lua_vm_machine::load( const std::filesystem::path& p )
{
        return impl_ptr_->load( p );
}

opt_error< slua_error > lua_vm_machine::on_cmd(
    fw_node_group_value v,
    const node_context& context,
    rclcpp::Node&       node,
    sm_feedback_f       fb_fun )
{
        return impl_ptr_->on_cmd( v, context, node, fb_fun );
}

opt_error< slua_error >
lua_vm_machine::tick( const node_context& context, rclcpp::Node& node, sm_feedback_f send_cb )
{
        return impl_ptr_->tick( context, node, send_cb );
}

lua_vm_machine::~lua_vm_machine() = default;
