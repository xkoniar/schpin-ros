// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/context.h"
#include "node/robot/view.h"
#include "node/state_machines/base.h"

#include <experimental/propagate_const>
#include <schpin_lib/lua/base.h>

#pragma once

namespace schpin
{

class lua_vm_interactive
{
        struct impl;

        std::experimental::propagate_const< std::unique_ptr< impl > > impl_ptr_;

public:
        lua_vm_interactive();

        std::string get_json_def() const;

        em::either< std::optional< nlohmann::json >, slua_error > exec_bind(
            const std::string&,
            const nlohmann::json&,
            const node_context&,
            rclcpp::Node&,
            sm_feedback_f );

        opt_error< slua_error >
        exec( const std::string&, const node_context&, rclcpp::Node&, sm_feedback_f );

        ~lua_vm_interactive();
};

class lua_vm_machine
{
        struct impl;

        std::experimental::propagate_const< std::unique_ptr< impl > > impl_ptr_;

public:
        lua_vm_machine( const std::string& );
        lua_vm_machine( const std::filesystem::path& );
        opt_error< slua_error > load( const std::string& code );
        opt_error< slua_error > load( const std::filesystem::path& code );
        opt_error< slua_error >
        on_cmd( fw_node_group_value, const node_context&, rclcpp::Node&, sm_feedback_f );
        opt_error< slua_error >
             tick( const node_context& context, rclcpp::Node&, sm_feedback_f send_cb );
        bool finished();
        ~lua_vm_machine();
};

}  // namespace schpin
