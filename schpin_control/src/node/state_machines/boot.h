// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/state_machines/base.h"
#include "node/state_machines/interface.h"

#pragma once

namespace schpin
{

class boot_state_machine : state_machine_i
{
        std::vector< servo_id > to_finish_;
        std::vector< servo_id > to_activate_;
        std::vector< servo_id > finished_;

public:
        boot_state_machine( std::vector< servo_id > to_activate )
          : to_activate_( std::move( to_activate ) )
        {
        }

        opt_error< state_machine_error >
        on_cmd( fw_node_group_value v, const node_context&, rclcpp::Node&, sm_feedback_f send_cb )
        {
                return em::apply_on_match(
                    v,
                    [&]( em::tag< ACTIVATE >,
                         servo_id       id,
                         activated_enum a ) -> opt_error< state_machine_error > {
                            return on_activate_cmd( id, a, send_cb );
                    },
                    [&]( auto... ) -> opt_error< state_machine_error > {
                            return {};
                    } );
        }

        opt_error< state_machine_error >
        on_activate_cmd( servo_id id, activated_enum a, sm_feedback_f send_cb )
        {
                bool should_finish = em::find( to_finish_, id ) != to_finish_.end();
                bool is_finished   = em::find( finished_, id ) == finished_.end();
                if ( !( should_finish || is_finished ) ) {
                        deactivate( id, send_cb );
                        return {
                            E( state_machine_error )  //
                            << "Servo which should not activate, was "
                               "activating: "
                            << id << " is deactivated, msg: " << magic_enum::enum_name( a ) };
                }

                switch ( a ) {
                        case WAS_ACTIVATING:
                                SCHPIN_INFO_LOG(
                                    "boot",
                                    "Allready activating: " << id << " - restarting the process" );
                                to_activate_.push_back( id );
                                to_finish_.erase( em::find( to_finish_, id ) );
                                deactivate( id, send_cb );
                                return {};
                        case WAS_ACTIVATED:
                                SCHPIN_INFO_LOG(
                                    "boot",
                                    "Allready activated: " << id << " - restarting the process" );
                                to_activate_.push_back( id );
                                to_finish_.erase( em::find( to_finish_, id ) );
                                deactivate( id, send_cb );
                                return {};
                        case MISSING:
                                SCHPIN_INFO_LOG(
                                    "boot",
                                    "Servo " << id << " missing. Trying to activate again." );
                                to_activate_.push_back( id );
                                to_finish_.erase( em::find( to_finish_, id ) );
                                return {};
                        case ACTIVATING:
                                SCHPIN_INFO_LOG( "boot", "Started activation of: " << id );
                                return {};
                        case ACTIVATED:
                                SCHPIN_INFO_LOG( "boot", "Activated: " << id );
                                to_finish_.erase( em::find( to_finish_, id ) );
                                finished_.push_back( id );
                                return {};
                }
                return {};
        }

        opt_error< state_machine_error >
        tick( const node_context& context, rclcpp::Node&, sm_feedback_f send_cb )
        {
                if ( !to_activate_.empty() ) {
                        servo_id id = to_activate_.back();
                        SCHPIN_INFO_LOG( "boot", "Activating: " << id );
                        to_activate_.pop_back();
                        to_finish_.push_back( id );
                        auto servo_iter = context.sgroup.find_servo( id );
                        SCHPIN_ASSERT( servo_iter != context.sgroup.get_servos().end() );
                        activate( *servo_iter, send_cb );
                }
                return {};
        }

        bool finished() final
        {
                return to_activate_.empty() && to_finish_.empty();
        }

private:
        void activate( const servo& servo, sm_feedback_f send_cb )
        {
                send_cb(
                    node_fw_group::make_val< ACTIVATE >( servo_addr{ servo->stype, servo->id } ) );
        }
        void deactivate( servo_id id, sm_feedback_f send_cb )
        {
                send_cb( node_fw_group::make_val< DEACTIVATE >( id ) );
        }
};

}  // namespace schpin
