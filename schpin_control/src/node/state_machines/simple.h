// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/state_machines/base.h"
#include "node/state_machines/interface.h"

#pragma once

namespace schpin
{

class idle_state_machine : state_machine_i
{
public:
        opt_error< state_machine_error >
        on_cmd( fw_node_group_value, const node_context&, rclcpp::Node&, sm_feedback_f )
        {
                return {};
        }
        opt_error< state_machine_error > tick( const node_context&, rclcpp::Node&, sm_feedback_f )
        {
                return {};
        }
        bool finished() final
        {
                return true;
        }
};

class statelog_state_machine : state_machine_i
{
        bool fired_ = false;

public:
        inline static const std::string json_keyword = "statelog";

        opt_error< state_machine_error >
        on_cmd( fw_node_group_value, const node_context&, rclcpp::Node&, sm_feedback_f )
        {
                return {};
        }
        opt_error< state_machine_error >
        tick( const node_context& con, rclcpp::Node&, sm_feedback_f )
        {
                if ( fired_ ) {
                        return {};
                }
                SCHPIN_INFO_LOG( "statelog", con );
                fired_ = true;
                return {};
        }
        bool finished() final
        {
                return fired_;
        }
};

inline void from_json( const nlohmann::json&, statelog_state_machine& )
{
        // literally nothing to do here <3
}

}  // namespace schpin
