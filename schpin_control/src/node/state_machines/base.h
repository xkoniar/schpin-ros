// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/events.h"

#include <schpin_lib/error.h>

#pragma once

namespace schpin
{

using machine_feedback_var =
    std::variant< node_fw_group_value, set_body_pose_event, servo_config_event >;

using sm_feedback_f       = std::function< ctl_id( machine_feedback_var ) >;
using state_machine_error = error< struct state_machine_error_tag >;

template < typename... Machines >
auto make_machine( const nlohmann::json& j ) -> std::variant< Machines... >
{
        std::variant< Machines... > res;
        // TODO: make for_each_variant
        em::for_each( std::tuple< Machines... >{}, [&]< typename Machine >( Machine ) {
                if ( j["machine"] == Machine::json_keyword ) {
                        res = j["data"].get< Machine >();
                }
        } );

        return res;
}

template < typename... Machines >
auto make_machine( std::istream& input ) -> std::variant< Machines... >
{
        return safe_load_json( input, [&]( const nlohmann::json& j ) {
                return make_machine< Machines... >( j );
        } );
}

}  // namespace schpin
