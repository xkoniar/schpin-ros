// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/robot/model.h"
#include "node/state_machines/base.h"
#include "shared/protocol.h"

#pragma once

namespace schpin
{

struct state_machine_i
{
        virtual opt_error< state_machine_error >
        on_cmd( fw_node_group_value, const node_context&, rclcpp::Node&, sm_feedback_f ) = 0;
        virtual opt_error< state_machine_error >
                     tick( const node_context&, rclcpp::Node&, sm_feedback_f ) = 0;
        virtual bool finished()                                                = 0;
        virtual ~state_machine_i()                                             = default;
};

}  // namespace schpin
