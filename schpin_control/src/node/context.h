// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/msg_history.h"
#include "node/robot/model.h"
#include "node/robot/state.h"
#include "node/servos.h"

#pragma once

namespace schpin
{

struct node_context
{
        robot_model robot;
        servo_group sgroup;
        robot_state rstate;
        msg_history hist;

        node_context( robot_model m, servo_group g )
          : robot( std::move( m ) )
          , sgroup( std::move( g ) )
          , rstate( robot )
        {
        }
};

// TODO: fix this
inline std::ostream& operator<<( std::ostream& os, const node_context& con )
{
        return os << "robot: \n" << con.robot << "\n" << con.sgroup;
}

}  // namespace schpin
