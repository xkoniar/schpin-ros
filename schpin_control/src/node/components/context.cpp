// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/components/context.h"

#include <emlabcpp/match.h>
#include <emlabcpp/protocol/register_handler.h>

using namespace schpin;

using config_handler = em::protocol_register_handler< fw_config_map >;

context_component::context_component(
    rclcpp::Node&               node,
    robot_model                 rob,
    const node_hardware_config& c )
  : state_pub_( node.create_publisher< sensor_msgs::msg::JointState >( c.joint_state_topic, 100 ) )
  , context_( node_context{ std::move( rob ), servo_group{ c.servos } } )
  , br_( node )
{
        config_map_.set_val< INPUT_BUFFER_TELEM_PERIOD_MS >( clk_time{ 2000 } );
        config_map_.set_val< EVENT_BUFFER_TELEM_PERIOD_MS >( clk_time{ 2000 } );
}

void context_component::tick( const node_context&, rclcpp::Node& node, sm_feedback_f msg_cb )
{
        if ( config_updated_ ) {
                config_updated_ = false;
                em::protocol_for_each_register(
                    config_map_, [&]< auto key >( auto val ) {
                            // TODO: this is duplicated much :/
                            static_assert(
                                config_handler::max_size < std::tuple_size_v< config_buffer > );
                            auto          msg = config_handler::serialize< key >( val );
                            config_buffer buff{};
                            std::copy( msg.begin(), msg.end(), buff.begin() );

                            msg_cb( node_fw_group::make_val< CONFIG_SET >( key, buff ) );
                    } );
        }

        geometry_msgs::msg::TransformStamped msg;

        msg.header.stamp          = node.get_clock()->now();
        msg.header.frame_id       = "world";
        msg.child_frame_id        = "base_link";
        msg.transform.translation = vector_to_msg( context_.rstate.body_pose.position );
        msg.transform.rotation    = quaternion_to_msg( context_.rstate.body_pose.orientation );

        br_.sendTransform( msg );
}

void context_component::on_feedback(
    fw_node_group_value v,
    const node_context&,
    rclcpp::Node&,
    sm_feedback_f msg_cb )
{
        em::apply_on_match(
            v,
            [&]( em::tag< SERVO >, servo_id id, servo_master_variant var ) {
                    on_servo_feedback_cmd( id, var, msg_cb );
            },
            [&]( auto... ) {} );
}
