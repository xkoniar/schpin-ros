// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/config.h"
#include "node/context.h"
#include "node/state_machines/base.h"
#include "shared/protocol.h"

#include <chrono>
#include <deque>
#include <diagnostic_msgs/msg/diagnostic_array.hpp>
#include <rclcpp/rclcpp.hpp>
#include <schpin_lib_exp/stringify.h>

#pragma once

namespace schpin
{

class feedback_freq_analyzer
{
        struct dataset
        {
                std::deque< rclcpp::Time > timestamps{};
                rclcpp::Duration           dur{ std::chrono::seconds( 0 ) };
                float                      freq = 0.f;
        };

        std::unordered_map< messages_enum, std::array< dataset, 3 > > datasets_{};

public:
        feedback_freq_analyzer() = default;

        void init_dataset( messages_enum v )
        {
                datasets_[v][0].dur = rclcpp::Duration{ 1, 0 };
                datasets_[v][1].dur = rclcpp::Duration{ 5, 0 };
                datasets_[v][2].dur = rclcpp::Duration{ 15, 0 };
        }

        void tick( rclcpp::Time now )
        {
                for ( auto& [key, datas] : datasets_ ) {
                        for ( dataset& dat : datas ) {
                                while ( !dat.timestamps.empty() &&
                                        dat.timestamps.front() + dat.dur < now ) {
                                        dat.timestamps.pop_front();
                                }

                                dat.freq = static_cast< float >( dat.timestamps.size() ) /
                                           static_cast< float >( dat.dur.seconds() );
                        }
                }
        }

        void make_record( fw_node_group_value v, rclcpp::Time now )
        {
                messages_enum key =
                    em::apply_on_match( v, [&]< typename Tag >( Tag, auto... ) -> messages_enum {
                            auto val  = Tag::value;
                            auto iter = datasets_.find( val );
                            if ( iter == datasets_.end() ) {
                                    init_dataset( val );
                            }
                            return val;
                    } );
                for ( dataset& dat : datasets_[key] ) {
                        dat.timestamps.push_back( now );
                }
        }

        const auto& get_stats() const
        {
                return datasets_;
        }
};

class telemetry_component
{

        static constexpr std::array monitored_msgs_ = { POSITION, TEMP, VOLTAGE, VELOCITY, TASK };

        rclcpp::Publisher< diagnostic_msgs::msg::DiagnosticArray >::SharedPtr  diag_pub_;
        std::unordered_map< servo_id, diagnostic_msgs::msg::DiagnosticStatus > msgs_;
        rclcpp::Duration                                                       period_;
        rclcpp::Time                                                           last_;
        em::bounded< std::size_t, 0, monitored_msgs_.size() - 1 >              monitor_i_;
        feedback_freq_analyzer                                                 freq_analyzer_;
        uint16_t input_buffer_size_ = 0;
        uint16_t event_buffer_size_ = 0;

public:
        telemetry_component( rclcpp::Node& node, const node_hardware_config& c );

        void tick( const node_context&, rclcpp::Node&, sm_feedback_f msg_cb );

        void on_feedback(
            fw_node_group_value v,
            const node_context& context,
            rclcpp::Node&,
            sm_feedback_f );

private:
        template < messages_enum CMD, typename... Args >
        node_fw_group_value make_value( servo_id id, Args... args ) const
        {
                return { node_fw_group::make_val< SERVO >(
                    id, master_servo_group::make_val< CMD >( args... ) ) };
        }
        void on_servo_msg(
            servo_id,
            const node_context&,
            em::tag< CONFIG_GET >,
            config_addr,
            config_buffer )
        {
        }
        void on_servo_msg( servo_id, const node_context&, em::tag< CONFIG_SET >, config_addr )
        {
        }
        void on_servo_msg( servo_id sid, const node_context&, em::tag< POSITION >, bangle angle )
        {
                update_key( sid, "position", std::to_string( angle.as_float() ) );
        }
        void on_servo_msg( servo_id sid, const node_context&, em::tag< TEMP >, btemp temp )
        {
                update_key( sid, "temperature", std::to_string( temp.as_float() ) );
        }
        void on_servo_msg( servo_id sid, const node_context&, em::tag< VOLTAGE >, bvoltage v )
        {
                update_key( sid, "voltage", std::to_string( v.as_float() ) );
        }
        void
        on_servo_msg( servo_id sid, const node_context&, em::tag< VELOCITY >, bangular_velocity av )
        {
                update_key( sid, "velocity", std::to_string( av.as_float() ) );
        }
        void on_servo_msg( servo_id sid, const node_context&, em::tag< TASK >, servo_task_enum t )
        {
                update_key( sid, "task", std::string{ magic_enum::enum_name( t ) } );
        }
        void on_servo_msg(
            servo_id sid,
            const node_context&,
            em::tag< TELEM >,
            bangle present,
            bangle goal,
            btime  remains )
        {
                std::stringstream ss;
                ss << present << "," << goal << "," << remains;
                update_key( sid, "telem", ss.str() );
        }
        void on_servo_msg(
            servo_id            sid,
            const node_context& context,
            em::tag< ERROR >,
            ctl_id              cid,
            servo_error_variant var )
        {
                using namespace stringify;
                SCHPIN_ERROR_LOG( "telem", "error form servo " << sid << " : " << var );
                context.hist.try_log( cid );
        }

        void check_record( servo_id sid )
        {
                // this expects ref_msg to create the record if it is missing
                ref_msg( sid );
        }

        void update_key( servo_id sid, const std::string& key, const std::string& value );

        diagnostic_msgs::msg::DiagnosticStatus& ref_msg( servo_id sid );

        std::vector< diagnostic_msgs::msg::DiagnosticStatus > make_freq_msgs();

        diagnostic_msgs::msg::DiagnosticStatus
        make_buffer_size_msg( const std::string& name, uint16_t val );
};

}  // namespace schpin
