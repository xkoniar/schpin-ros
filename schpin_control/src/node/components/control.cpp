// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/components/control.h"

using namespace schpin;

control_component::control_component(
    rclcpp::Node&               node,
    const node_context&         context,
    const node_hardware_config& conf )
  : state_( boot_state_machine{ map_f_to_v(
        context.sgroup.get_servos(),
        []( const servo& s ) {
                return s->id;
        } ) } )
  , ctl_serv_( node.create_service< schpin_msgs::srv::ControlCtl >(
        "/control/ctl",
        std::bind(
            &control_component::on_ctl,
            this,
            std::placeholders::_1,
            std::placeholders::_2,
            std::placeholders::_3 ) ) )
  , conf_( conf )
{
}

bool control_component::on_ctl(
    std::shared_ptr< rmw_request_id_t >,
    std::shared_ptr< schpin_msgs::srv::ControlCtl::Request >  req,
    std::shared_ptr< schpin_msgs::srv::ControlCtl::Response > res )
{
        if ( !std::holds_alternative< idle_state_machine >( state_ ) ) {
                res->res = schpin_msgs::srv::ControlCtl::Response::BUSY;
                return true;
        }

        state_.emplace< lua_vm_machine >( req->data );

        return true;
}

void control_component::tick(
    const node_context& context,
    rclcpp::Node&       node,
    sm_feedback_f       fb_fun )
{
        std::visit(
            [&]( auto& state ) {
                    state.tick( context, node, fb_fun ).optionally( [&]( auto error ) {
                            SCHPIN_ERROR_LOG( "control", simplified_error{ error } );
                    } );
            },
            state_ );

        bool finished = std::visit(
            [&]( auto& state ) {
                    return state.finished();
            },
            state_ );

        if ( !finished ) {
                return;
        }

        em::match(
            state_,
            []( const idle_state_machine& ) {},
            [&]( const statelog_state_machine& ) {
                    state_.emplace< idle_state_machine >();
            },
            [&]( const boot_state_machine& ) {
                    if ( conf_.opt_script ) {
                            SCHPIN_INFO_LOG(
                                "control", "Executing loaded script: " << *conf_.opt_script );
                            state_.emplace< lua_vm_machine >( *conf_.opt_script );
                            conf_.opt_script.reset();
                    } else {
                            state_.emplace< statelog_state_machine >();
                    }
            },
            [&]( const lua_vm_machine& ) {
                    state_.emplace< idle_state_machine >();
            } );
}

void control_component::on_feedback(
    const fw_node_group_value& v,
    const node_context&        context,
    rclcpp::Node&              node,
    sm_feedback_f              fb_fun )
{
        std::visit(
            [&]( auto& state ) {
                    state.on_cmd( v, context, node, fb_fun ).optionally( [&]( auto e ) {
                            SCHPIN_ERROR_LOG( "control", simplified_error{ e } );
                    } );
            },
            state_ );
}
