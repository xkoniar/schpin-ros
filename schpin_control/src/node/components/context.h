// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/context.h"
#include "node/state_machines/base.h"

#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/joint_state.hpp>
#include <tf2_ros/transform_broadcaster.h>

#pragma once

namespace schpin
{

class context_component
{
        rclcpp::Publisher< sensor_msgs::msg::JointState >::SharedPtr state_pub_;
        node_context                                                 context_;
        fw_config_map                                                config_map_;
        bool                                                         config_updated_ = true;
        tf2_ros::TransformBroadcaster                                br_;

public:
        context_component( rclcpp::Node& node, robot_model rob, const node_hardware_config& c );

        const node_context& get_context()
        {
                return context_;
        }

        void record_msg_sent( ctl_id cid, node_fw_message m, rclcpp::Time now )
        {
                context_.hist.insert( cid, m, now );
        }

        void feedback_processed( ctl_id cid )
        {
                context_.hist.drop( cid );
        }

        void tick( const node_context&, rclcpp::Node&, sm_feedback_f msg_cb );

        void set_body_pose( const pose< world_frame >& p )
        {
                context_.rstate.body_pose = p;
        }

        void set_servo_config( servo_id id, const nlohmann::json& j )
        {
                context_.sgroup.set_servo_config( id, j );
        }

        void on_feedback(
            fw_node_group_value v,
            const node_context&,
            rclcpp::Node&,
            sm_feedback_f msg_cb );

private:
        template < typename UnaryFunction >
        void on_servo_feedback_cmd( servo_id id, servo_master_variant v, UnaryFunction msg_cb )
        {
                context_.sgroup.on_feedback(
                    id,
                    v,
                    em::matcher{
                        [&]( const servo& s, em::angle a ) {
                                bool has_updated = context_.rstate.update_joint_state(
                                    context_.robot, s->joint_name, a );

                                SCHPIN_ASSERT( has_updated );

                                sensor_msgs::msg::JointState msg;
                                msg.name.push_back( s->joint_name );
                                msg.position.push_back( static_cast< double >( *a ) );

                                state_pub_->publish( msg );
                        },
                        [&]( node_fw_group_value val ) {
                                msg_cb( val );
                        } } );
        }
};

}  // namespace schpin
