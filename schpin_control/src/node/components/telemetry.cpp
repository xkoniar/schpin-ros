// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/components/telemetry.h"

using namespace schpin;

telemetry_component::telemetry_component( rclcpp::Node& node, const node_hardware_config& c )
  : diag_pub_( node.create_publisher< diagnostic_msgs::msg::DiagnosticArray >( c.diag_topic, 100 ) )
  , period_( rclcpp::Duration::from_seconds( 1. / static_cast< double >( c.diag_freq ) ) )
  , last_( node.now() )
{
}

void telemetry_component::tick( const node_context&, rclcpp::Node& node, sm_feedback_f msg_cb )
{
        if ( node.now() < last_ + period_ ) {
                return;
        }

        freq_analyzer_.tick( node.now() );

        diagnostic_msgs::msg::DiagnosticArray msg;
        msg.header.stamp    = node.get_clock()->now();
        msg.header.frame_id = "base_link";
        msg.status          = map_f_to_v(
            msgs_, [&]( std::tuple< servo_id, diagnostic_msgs::msg::DiagnosticStatus > pack ) {
                    return std::get< 1 >( pack );
            } );
        for ( auto submsg : make_freq_msgs() ) {
                msg.status.push_back( std::move( submsg ) );
        }

        msg.status.push_back( make_buffer_size_msg( "input buffer", input_buffer_size_ ) );
        msg.status.push_back( make_buffer_size_msg( "event buffer", event_buffer_size_ ) );

        diag_pub_->publish( msg );

        em::select_index( monitor_i_, [&]< std::size_t i >() {
                for ( auto& pr : msgs_ ) {
                        msg_cb( make_value< monitored_msgs_[i] >( pr.first ) );
                }
        } );

        monitor_i_.rotate_right( 1u );

        last_ = node.now();
}

void telemetry_component::on_feedback(
    fw_node_group_value v,
    const node_context& context,
    rclcpp::Node&       node,
    sm_feedback_f )
{
        freq_analyzer_.make_record( v, node.now() );

        em::apply_on_match(
            v,
            [&]( em::tag< ACTIVATE >, servo_id id, activated_enum en ) {
                    if ( en == ACTIVATED || en == WAS_ACTIVATED ) {
                            check_record( id );
                    }
            },
            [&]( em::tag< INPUT_BUFFER_SIZE >, uint16_t val ) {
                    input_buffer_size_ = val;
            },
            [&]( em::tag< EVENT_BUFFER_SIZE >, uint16_t val ) {
                    event_buffer_size_ = val;
            },
            [&]( em::tag< ERROR >, ctl_id id, error_message e ) {
                    SCHPIN_ERROR_LOG( "telem", "incoming error:" << e );
                    context.hist.try_log( id );
            },
            [&]( em::tag< SERVO >, servo_id id, servo_master_variant var ) {
                    em::apply_on_match( var, [&]( auto... args ) {  //
                            on_servo_msg( id, context, args... );
                    } );
            },
            [&]( auto... ) {} );
}
void telemetry_component::update_key(
    servo_id           sid,
    const std::string& key,
    const std::string& value )
{
        check_record( sid );

        diagnostic_msgs::msg::KeyValue new_pair;
        new_pair.key   = key;
        new_pair.value = value;

        auto iter = em::find_if(
            msgs_[sid].values,
            [&]( const auto& kval ) {  //
                    return kval.key == key;
            } );
        if ( iter == msgs_[sid].values.end() ) {
                msgs_[sid].values.push_back( new_pair );
        } else {
                *iter = new_pair;
        }
}

diagnostic_msgs::msg::DiagnosticStatus& telemetry_component::ref_msg( servo_id sid )
{
        auto iter = msgs_.find( sid );
        if ( iter == msgs_.end() ) {
                auto& msg       = msgs_[sid];
                msg.level       = diagnostic_msgs::msg::DiagnosticStatus::OK;
                msg.name        = "Servomotor/" + std::to_string( sid );
                msg.message     = "Working correctly";
                msg.hardware_id = "SM" + std::to_string( sid );
                return msg;
        }
        return iter->second;
}

std::vector< diagnostic_msgs::msg::DiagnosticStatus > telemetry_component::make_freq_msgs()
{
        return map_f_to_v(
            freq_analyzer_.get_stats(),
            [&]< typename Datasets >( std::pair< const messages_enum, Datasets > pack ) {
                    auto [msg_id, datasets] = pack;
                    diagnostic_msgs::msg::DiagnosticStatus msg;
                    msg.level = diagnostic_msgs::msg::DiagnosticStatus::OK;
                    msg.name = "Feedback channel/" + std::string{ magic_enum::enum_name( msg_id ) };
                    msg.message     = "Working";
                    msg.hardware_id = "control";

                    for ( auto& dat : datasets ) {
                            diagnostic_msgs::msg::KeyValue pr;
                            pr.key   = "Window " + std::to_string( dat.dur.seconds() );
                            pr.value = std::to_string( dat.freq );
                            msg.values.push_back( pr );
                    }

                    return msg;
            } );
}

diagnostic_msgs::msg::DiagnosticStatus
telemetry_component::make_buffer_size_msg( const std::string& name, uint16_t val )
{
        diagnostic_msgs::msg::DiagnosticStatus msg;
        msg.level       = diagnostic_msgs::msg::DiagnosticStatus::OK;
        msg.name        = "Buffer size/" + name;
        msg.message     = "Working";
        msg.hardware_id = "control";

        diagnostic_msgs::msg::KeyValue pr;
        pr.key   = "size";
        pr.value = std::to_string( val );

        msg.values.push_back( pr );
        return msg;
}
