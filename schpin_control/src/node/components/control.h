// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/lua_vm.h"
#include "node/state_machines/boot.h"
#include "node/state_machines/simple.h"

#include <schpin_msgs/srv/control_ctl.hpp>

#pragma once

namespace schpin
{

class control_component
{
        using machine_variant = std::variant<
            idle_state_machine,
            boot_state_machine,
            lua_vm_machine,
            statelog_state_machine >;

        machine_variant                                            state_;
        rclcpp::Service< schpin_msgs::srv::ControlCtl >::SharedPtr ctl_serv_;
        node_hardware_config                                       conf_;

public:
        control_component(
            rclcpp::Node&       node,
            const node_context& context,
            const node_hardware_config& );

        bool on_ctl(
            std::shared_ptr< rmw_request_id_t >,
            std::shared_ptr< schpin_msgs::srv::ControlCtl::Request >  req,
            std::shared_ptr< schpin_msgs::srv::ControlCtl::Response > res );

        void tick( const node_context& context, rclcpp::Node&, sm_feedback_f fb_fun );

        void on_feedback(
            const fw_node_group_value& v,
            const node_context&        context,
            rclcpp::Node&              node,
            sm_feedback_f              fb_fun );
};

}  // namespace schpin
