// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "shared/protocol.h"

#include <schpin_lib/lua/ser.h>

#pragma once

namespace schpin
{

template < auto Val >
struct lua_serializer< em::tag< Val > >
{
        static void encode( lua_State* lua_ptr, em::tag< Val > )
        {
                lua_serializer< decltype( Val ) >::encode( lua_ptr, Val );
        }

        static std::string desc_name()
        {
                return std::string{ magic_enum::enum_name( Val ) };
        }

        static std::string desc_example()
        {
                return std::string{ magic_enum::enum_entries< decltype( Val ) >().front().second };
        }
};

template < em::protocol_message_derived T >
struct lua_serializer< T >
{
        static void encode( lua_State* lua_ptr, const T& msg )
        {
                std::stringstream ss;
                ss << msg;
                lua_serializer< std::string >::encode( lua_ptr, ss.str() );
        }

        static std::string desc_name()
        {
                return "Message of size " + to_string( T::max_size );
        }
};

template <>
struct lua_serializer< em::protocol_error_record >
{
        static void encode( lua_State* lua_ptr, const em::protocol_error_record& rec )
        {
                lua_serializer< typename em::protocol_mark::base_type >::encode(
                    lua_ptr, rec.mark );
                lua_serializer< std::size_t >::encode( lua_ptr, rec.offset );
        }

        static std::string desc_name()
        {
                return "Record of errro that happend in protocol lib";
        }
};

template <>
struct lua_serializer< servo_type >
{
        static void encode( lua_State* lua_ptr, servo_type stype )
        {
                lua_serializer< std::string >::encode(
                    lua_ptr, std::string{ magic_enum::enum_name( stype ) } );
        }

        static em::either< servo_type, slua_error > decode( lua_State* lua_ptr, int index )
        {
                return lua_serializer< std::string >::decode( lua_ptr, index )
                    .bind_left(
                        [&]( const std::string& val ) -> em::either< servo_type, slua_error > {
                                auto res = magic_enum::enum_cast< servo_type >( val );
                                if ( res.has_value() ) {
                                        return res.value();
                                }
                                return {
                                    E( slua_error )
                                    << "Value " << val << " is not valid option of servo type" };
                        } );
        }

        static std::string desc_name()
        {
                return "Type of servomotor";
        }

        static std::string desc_example()
        {
                return "LEWAN";
        }
};

}  // namespace schpin
