// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/events.h"
#include "node/lua_vm/extra_ser.h"

#include <schpin_lib/lua/pose.h>
#include <schpin_lib/lua/print.h>
#include <schpin_lib/lua/rosrequire.h>

#pragma once

namespace schpin
{

enum lua_call_enum
{
        CALL_ON_CMD
};

inline auto multi_joint_arg( const std::string& desc )
{
        return lua_multi_key_arg< std::string >{
            "joint",
            desc + "(either provide on joint, or list of "
                   "them)" };
}

struct lua_servo_config_bind
{
        static constexpr std::string_view name = "set_servo_config";
        static constexpr std::string_view desc = "sets configuration to servo";

        inline static const auto arg = slua_struct< lua_servo_config_event >(
            multi_joint_arg( "joints to be configured" ),
            lua_key_arg< nlohmann::json >{ "vars", "set of config for servo" } );
        inline static const no_return_type res{};
};

struct get_system_time_bind
{
        static constexpr std::string_view name = "get_system_time";
        static constexpr std::string_view desc = "Returns actual system time in milliseconds";

        inline static const auto arg = slua_struct_impl< get_system_time_event >();
        inline static const auto res = slua_result< std::size_t >();
};

struct get_leg_state_bind
{
        static constexpr std::string_view name = "get_leg_state";
        static constexpr std::string_view desc = "returns actual state of the leg";

        inline static const auto arg = slua_struct< get_leg_state_event >(
            lua_key_arg< std::string >{ "leg", "name of the requested leg" } );

        inline static const auto res = slua_result< dof_array< em::angle > >{ "angles representing "
                                                                              "state" };
};

struct get_tip_pos_bind
{
        static constexpr std::string_view name = "get_tip_pos";
        static constexpr std::string_view desc =
            "returns tip position of leg, when joints are in provided "
            "configuration";

        inline static const auto arg = slua_struct< get_tip_pos_event >(
            lua_key_arg< std::string >{ "leg", "name of the requested leg" },
            lua_key_arg< dof_array< em::angle > >{ "angles", "a set of angles for given leg" } );
        inline static const auto res =
            slua_result< point< 3, robot_frame > >{ "point relative to robot frame" };
};

struct joint_goto_bind
{
        static constexpr std::string_view name = "joint_goto";
        static constexpr std::string_view desc = "moves join to desired 'pos' in 'time'";
        inline static const auto          arg  = slua_struct< joint_goto_event >(
            multi_joint_arg( "joints to be moved" ),
            lua_key_arg< bangle >{ "pos", "position for the movement" },
            lua_key_arg< btime >{ "time", "time for the movement in ms" },
            lua_opt_key_arg< end_condition >{
                "end",
                "condition when the command should end",
                ON_TIME } );
        inline static const no_return_type res{};
};

struct joint_spin_bind
{
        static constexpr std::string_view name = "joint_spin";
        static constexpr std::string_view desc = "spins joint for 'time' with 'velocity'";

        inline static const auto arg = slua_struct< joint_spin_event >(
            multi_joint_arg( "joints to be spinned" ),
            lua_key_arg< bangular_velocity >{ "velocity", "velocity of the spin" },
            lua_key_arg< btime >{ "time", "how long does the spin take in ms" } );

        inline static const no_return_type res{};
};

struct request_pos_bind
{
        static constexpr std::string_view name = "request_pos";
        static constexpr std::string_view desc = "sends command for reading the position of joint";

        inline static const auto arg =
            slua_struct< request_pos_event >( multi_joint_arg( "joints that will be reported" ) );
        inline static const no_return_type res{};
};

struct sync_bind
{
        static constexpr std::string_view name = "sync";
        static constexpr std::string_view desc =
            "synchronizes execution of event-commands until all previous events are finished";

        inline static const auto arg =
            slua_struct< sync_event >( multi_joint_arg( "joints that should be synchronized" ) );
        inline static const auto res = slua_result< ctl_id >{ "id of the message" };
};

struct get_joints_bind
{
        static constexpr std::string_view name = "get_joints";
        static constexpr std::string_view desc = "returns list of active joints for leg";

        inline static const auto arg =
            slua_struct< get_joints_event >( lua_key_arg< std::string >{ "leg", "the leg" } );
        inline static const auto res =
            slua_result< std::vector< std::string > >{ "list of joints for leg" };
};

struct get_legs_bind
{
        static constexpr std::string_view name = "get_legs";
        static constexpr std::string_view desc = "returns list of active legs";

        inline static const auto arg = slua_struct< get_legs_event >();
        inline static const auto res = slua_result< std::vector< std::string > >{ "list of legs" };
};

struct get_neutral_angles_bind
{
        static constexpr std::string_view name = "get_neutral_angles";
        static constexpr std::string_view desc = "returns list of neutral angles for leg";

        inline static const auto arg = slua_struct< get_neutral_angles_event >(
            lua_key_arg< std::string >{ "leg", "the leg" } );
        inline static const auto res =
            slua_result< dof_array< bangle > >{ "list of angles for leg" };
};

struct get_neutral_dir_bind
{
        static constexpr std::string_view name = "get_neutral_dir";
        static constexpr std::string_view desc = "returns list of neutral direction for leg";

        inline static const auto arg = slua_struct< get_neutral_angles_event >(
            lua_key_arg< std::string >{ "leg", "the leg" } );
        inline static const auto res = slua_result< vec< 3 > >{ "neutral direction for leg" };
};

struct tip_goto_bind
{

        static constexpr std::string_view name = "tip_goto";
        static constexpr std::string_view desc =
            "Moves tip of the `leg` to position 'target' in 'time'. 'steps'represents number of iterations of the algorithm that solves the IK formula.";

        inline static const auto arg = slua_struct< tip_goto_event >(
            lua_key_arg< std::string >{ "leg", "leg to be moved" },
            lua_key_arg< btime >{ "time", "time of the movement" },
            lua_key_arg< point< 3, world_frame > >{
                "target",
                "target position of the tip of the leg" },
            lua_opt_key_arg< std::size_t >{
                "steps",
                "number of steps the IK solver",
                std::size_t{ 32 } } );
        inline static const no_return_type res{};
};

struct relative_tip_goto_bind
{
        static constexpr std::string_view name = "relative_tip_goto";

        static constexpr std::string_view desc =
            "Moves tip of the `leg` to position `target` in `time`. The target represents position relative to the positon of body";

        inline static const auto arg = slua_struct< relative_tip_goto_event >(
            lua_key_arg< std::string >{ "leg", "leg to be moved" },
            lua_key_arg< btime >{ "time", "time of the movement" },
            lua_key_arg< point< 3, robot_frame > >{
                "target",
                "target relative position of "
                "the tip of the leg" },
            lua_opt_key_arg< std::size_t >{
                "steps",
                "number of steps the IK solver",
                std::size_t{ 32 } } );
        inline static const no_return_type res{};
};

struct tip_move_bind
{
        static constexpr std::string_view name = "tip_move";
        static constexpr std::string_view desc =
            "Moves tip of the `leg` in 'dir' for 'time'. 'steps' represents number of iterations of the algorithm that solves the IK formula.";

        inline static const auto arg = slua_struct< tip_move_event >(
            lua_key_arg< std::string >{ "leg", "leg to be moved" },
            lua_key_arg< btime >{ "time", "time of the movement" },
            lua_key_arg< vec< 3 > >{ "dir", "direction of the movement as vector" },
            lua_opt_key_arg< std::size_t >{
                "steps",
                "numer of steps the IK solver",
                std::size_t{ 32 } } );
        inline static const no_return_type res{};
};

struct body_goto_bind
{
        static constexpr std::string_view name = "body_goto";
        static constexpr std::string_view desc =
            "Moves the body into a absolute coordinate specificed by pose in `time`.";

        inline static const auto arg = slua_struct< body_goto_event >(
            slua_pose_args{ "point", "orientation", "The desired pose" },
            lua_key_arg< btime >{ "time", "time of movement" },
            lua_opt_key_arg< std::size_t >{
                "steps",
                "number of steps the IK solver",
                std::size_t{ 32 } } );
        inline static const no_return_type res{};
};

struct body_move_bind
{

        static constexpr std::string_view name = "body_move";
        static constexpr std::string_view desc =
            "Moves body of the robot in direction 'dir' and rotates it by 'rot', by moving all legs in opposite direction, this is done in 'time'. 'steps' represents number of iterations of the algorithm that solves the IK formula.";

        inline static const auto arg = slua_struct< body_move_event >(
            lua_key_arg< vec< 3 > >{ "dir", "direction of body movement" },
            lua_key_arg< quaternion< robot_frame > >{ "rot", "rotation of the body" },
            lua_key_arg< btime >{ "time", "time of the movement" },
            lua_opt_key_arg< std::size_t >{
                "steps",
                "number of steps the IK solver",
                std::size_t{ 32 } } );
        inline static const no_return_type res{};
};

struct set_body_pose_bind
{
        static constexpr std::string_view name = "set_body_pose";
        static constexpr std::string_view desc =
            "Sets actual pose of the body of the robot relative to world frame";

        inline static const auto           arg = slua_struct< set_body_pose_event >( slua_pose_args(
            "point",
            "orientation",
            "The desired pose of the body in world frame" ) );
        inline static const no_return_type res{};
};

struct sync_cb_bind
{
        static constexpr std::string_view name = "sync_cb";
        static constexpr std::string_view desc = "Table of callbacks for messages";
        inline static const auto          var  = std::tuple{};
};

struct msg_cb_bind
{
        static constexpr std::string_view name = "msg_cb";
        static constexpr std::string_view desc = "Callbacks for messages";
        inline static const auto          var  = std::tuple{};
};

struct on_cmd_bind
{
        static constexpr auto             call_id = CALL_ON_CMD;
        static constexpr std::string_view name    = "on_cmd";
        static constexpr std::string_view desc =
            "Function is called each time msg is received from firmware";

        inline static const auto arg =
            lua_arg< fw_node_group_value >{ "firmware message split to string" };

        // msg_cb IS BIGASS HACK
        static constexpr std::string_view code = R"EOF(
          if msg_cb[arg[0]] ~= nil then
            msg_cb[arg[0]](arg)
          end
          if arg[0] ~= "FINISHED" then
            return
          end
          msg_id = arg[1]
          if sync_cb[msg_id] ~= nil then
            sync_cb[msg_id]()
            sync_cb[msg_id] = nil
          end
		    )EOF";
};

struct on_msg_bind
{
        static constexpr std::string_view name = "on_msg";
        static constexpr std::string_view desc =
            "executes callback when specific type of message arrives";

        inline static const auto arg = slua_native_args(
            lua_key_arg< std::string >{ "key", "id of message" },
            lua_key_arg< std::function< void() > >{ "cb", "callback" } );

        static constexpr std::string_view code = R"EOF(
            msg_cb[arg["key"]] = arg["cb"];
        )EOF";
};

struct on_sync_bind
{
        static constexpr std::string_view name = "on_sync";
        static constexpr std::string_view desc =
            "synces and executes callback when that sync finishes";

        inline static const auto arg = slua_native_args(
            lua_key_arg< std::vector< std::string > >{
                "joints",
                "Joints which should be "
                "waited for" },
            lua_key_arg< std::function< void() > >{ "callback", "callback to be executed" } );
        static constexpr std::string_view code = R"EOF(
	          msg_id = sync{joints=arg["joints"]}
	          sync_cb[msg_id] = arg["callback"]
		    )EOF";
};

struct block_yield_bind
{
        static constexpr std::string_view name = "block_yield";
        static constexpr std::string_view desc =
            "blocks execution until all joints finish their task";
        inline static const auto arg = slua_native_args( lua_key_arg< std::vector< std::string > >{
            "joints",
            "Joints which should be waited for" } );
        static constexpr std::string_view code = R"EOF(
          finished = 0
          on_sync{joints=arg["joints"], callback=function()
            finished = 1
          end}
          while finished == 0 do
            coroutine.yield()
          end
		    )EOF";
};

struct chain_bind
{
        static constexpr std::string_view name = "Chain";
        static constexpr std::string_view desc =
            "Chain converts a coroutine into a iterable event loop. This makes it possible"
            " to chain up complex movements into one thing.";

        struct new_bind
        {
                static constexpr std::string_view name = "new";
                static constexpr std::string_view desc =
                    "Creates new chain, argument is a function";

                inline static const auto arg =
                    lua_arg< std::function< void() > >{ "Coroutine to be sequenced." };

                static constexpr std::string_view code = R"EOF(
                    o = {fun = arg}
                    setmetatable(o, self)
                    self.__index = self
                    return o
                )EOF";
        };

        struct exec_bind
        {
                static constexpr std::string_view name = "exec";
                static constexpr std::string_view desc = "Executes the chain";

                inline static const auto arg = slua_native_args();

                static constexpr std::string_view code = R"EOF(
                    co = coroutine.create(self.fun)
                    while coroutine.resume(co) do
                        coroutine.yield()
                    end
               )EOF";
        };

        using methods = std::tuple< slua_native_proc< new_bind >, slua_native_proc< exec_bind > >;
};

struct slua_control_vm_def
{
        using def = std::tuple<
            slua_callback< slua_print_bind >,
            slua_rosrequire,
            slua_callback< lua_servo_config_bind >,
            slua_callback< get_system_time_bind >,
            slua_callback< get_leg_state_bind >,
            slua_callback< get_tip_pos_bind >,
            slua_callback< get_joints_bind >,
            slua_callback< get_legs_bind >,
            slua_callback< get_neutral_angles_bind >,
            slua_callback< joint_goto_bind >,
            slua_callback< joint_spin_bind >,
            slua_callback< request_pos_bind >,
            slua_callback< sync_bind >,
            slua_callback< tip_goto_bind >,
            slua_callback< relative_tip_goto_bind >,
            slua_callback< tip_move_bind >,
            slua_callback< body_goto_bind >,
            slua_callback< body_move_bind >,
            slua_callback< set_body_pose_bind >,
            slua_glob_var< sync_cb_bind >,
            slua_glob_var< msg_cb_bind >,
            slua_native_proc_call< on_cmd_bind >,
            slua_native_proc< on_msg_bind >,
            slua_native_proc< on_sync_bind >,
            slua_native_proc< block_yield_bind >,
            slua_native_class< chain_bind > >;
};

}  // namespace schpin
