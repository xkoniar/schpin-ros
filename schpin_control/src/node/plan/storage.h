// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <nlohmann/json.hpp>

#pragma once

namespace schpin
{

void to_json( nlohmann::json& j, const position_joint_task& t )
{
        j["pos"]  = t.goal;
        j["time"] = t.t;
}
void from_json( const nlohmann::json& j, position_joint_task& t )
{
        t.goal = em::angle{ j["pos"].get< float >() };
        t.t    = em::timeq{ j["time"].get< float >() };
}

void to_json( nlohmann::json& j, const velocity_joint_task& t )
{
        j["velocity"] = t.vel;
        j["time"]     = t.t;
}
void from_json( const nlohmann::json& j, velocity_joint_task& t )
{
        t.vel = em::angular_velocity{ j["velocity"].get< float >() };
        t.t   = em::timeq{ j["time"].get< float >() };
}

void to_json( nlohmann::json& j, const velocity_until_joint_task& t )
{
        j["velocity"] = t.vel;
        j["pos"]      = t.goal;
}
void from_json( const nlohmann::json& j, velocity_until_joint_task& t )
{
        t.vel  = em::angular_velocity{ j["velocity"].get< float >() };
        t.goal = em::angle{ j["pos"].get< float >() };
}

void to_json( nlohmann::json& j, const joint_tasks_variant& v )
{
        v << match(
            [&]( const position_joint_task& t ) {
                    j         = t;
                    j["type"] = "goto";
            },
            [&]( const velocity_joint_task& t ) {
                    j         = t;
                    j["type"] = "spin";
            },
            [&]( const velocity_until_joint_task& t ) {
                    j         = t;
                    j["type"] = "spin_until";
            } );
}
void from_json( const nlohmann::json& j, joint_tasks_variant& t )
{
        if ( j["type"] == "goto" ) {
                t = j.get< position_joint_task >();
        } else if ( j["type"] == "spin" ) {
                t = j.get< velocity_joint_task >();
        } else if ( j["type"] == "spin_until" ) {
                t = j.get< velocity_until_joint_task >();
        } else {
                // TODO: raise error
        }
}

void to_json( nlohmann::json& j, const joint_action& t )
{
        j["joint"] = t.joint;
        j["task"]  = t.task;
}
void from_json( const nlohmann::json& j, joint_action& t )
{
        t.joint = j["joint"].get< std::string >();
        t.task  = j["task"].get< joint_tasks_variant >();
}

void to_json( nlohmann::json& j, const action_pack_state_machine& p )
{
        j = p.get_actions();
}
void from_json( const nlohmann::json& j, action_pack_state_machine& p )
{
        p = action_pack_state_machine{ j.get< std::vector< joint_action > >() };
}

}  // namespace schpin
