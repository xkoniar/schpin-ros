// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/state_machines/action_pack.h"
#include "node/state_machines/simple.h"

#include <variant>

#pragma once

namespace schpin
{

struct plan_item
{
        std::string                                                       name;
        std::variant< action_pack_state_machine, statelog_state_machine > machine;
};

using plan = std::vector< plan_item >;

}  // namespace schpin
