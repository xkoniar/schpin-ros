// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/leg/model.h"

#include <vector>

#pragma once

namespace schpin
{

class robot_model
{
        std::vector< leg_model > legs_;

public:
        robot_model( robot_model&& ) = default;
        robot_model& operator=( robot_model&& ) = default;

        robot_model( const robot_model& ) = delete;
        robot_model& operator=( const robot_model& ) = delete;

        robot_model( std::vector< leg_model > legs )
          : legs_( std::move( legs ) )
        {
        }

        const std::vector< leg_model >& get_legs() const
        {
                return legs_;
        }
};

em::either< robot_model, load_error > load_robot_model(
    const urdf::Model&          model,
    const srdf_information&     srdf_info,
    const node_hardware_config& conf );

inline std::ostream& operator<<( std::ostream& os, const robot_model& r )
{
        os << "robot_model: " << std::endl;
        for ( const leg_model& l : r.get_legs() ) {
                os << l << std::endl;
        }
        return os;
}

}  // namespace schpin
