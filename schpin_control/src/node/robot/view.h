// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/leg/view.h"
#include "node/robot/model.h"
#include "node/robot/state.h"

#pragma once

namespace schpin
{

class robot_view
{

        std::vector< leg_view > legs_;

public:
        robot_view( const robot_model& m, const robot_state& s )
        {
                SCHPIN_ASSERT( m.get_legs().size() == s.legs.size() );
                legs_ = map_f_to_v( em::range( s.legs.size() ), [&]( std::size_t i ) {
                        return leg_view{ m.get_legs()[i], s.legs[i] };
                } );
        }

        const std::vector< leg_view >& get_legs() const
        {
                return legs_;
        }
};

}  // namespace schpin
