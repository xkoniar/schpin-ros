// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/leg/state.h"
#include "node/robot/model.h"

#pragma once

namespace schpin
{

struct robot_state
{
        std::vector< leg_state > legs;
        pose< world_frame >      body_pose;

        robot_state( const robot_model& m )
        {
                for ( const leg_model& l : m.get_legs() ) {
                        legs.push_back( leg_state{ l } );
                }
        }

        bool update_joint_state( const robot_model& m, const std::string& jname, em::angle a )
        {
                SCHPIN_ASSERT( legs.size() == m.get_legs().size() );
                return em::any_of( em::range( legs.size() ), [&]( std::size_t i ) {
                        return legs[i].update_joint_state( m.get_legs()[i], jname, a );
                } );
        }
};

inline std::ostream& operator<<( std::ostream& os, const robot_state& state )
{
        os << "body pose: " << state.body_pose << ", legs: \n";
        for ( const leg_state& lstate : state.legs ) {
                os << lstate << "\n";
        }
        return os;
}

}  // namespace schpin
