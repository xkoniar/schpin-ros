// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/robot/model.h"

using namespace schpin;

em::either< robot_model, load_error > schpin::load_robot_model(
    const urdf::Model&          model,
    const srdf_information&     srdf_info,
    const node_hardware_config& conf )
{
        auto eithers = load_leg_models( model, srdf_info, conf );

        partitioned_eithers< leg_model, load_error > parted =
            partition_eithers( std::move( eithers ) );

        if ( parted.right.empty() ) {
                return { robot_model{ std::move( parted.left ) } };
        }

        return E( load_error ).group_attach( parted.right ) << "Failed to load the robot_model";
}
