#include <nlohmann/json.hpp>
#include <schpin_lib/json/tuple_variant_serializer.h>

#pragma once

template <>
struct nlohmann::adl_serializer< schpin::master_servo_variant >
  : schpin::tuple_variant_serializer< schpin::master_servo_variant >
{
};

template <>
struct nlohmann::adl_serializer< schpin::servo_master_variant >
  : schpin::tuple_variant_serializer< schpin::servo_master_variant >
{
};

template <>
struct nlohmann::adl_serializer< schpin::servo_error_variant >
  : schpin::tuple_variant_serializer< schpin::servo_error_variant >
{
};

template < auto ID >
struct nlohmann::adl_serializer< emlabcpp::tag< ID > >
{
        static void to_json( nlohmann::json& j, emlabcpp::tag< ID > )
        {
                j = magic_enum::enum_name( ID );
        }
        static void from_json( const nlohmann::json& j, emlabcpp::tag< ID >& id )
        {

                auto opt_val = magic_enum::enum_cast< decltype( ID ) >( j.get< std::string >() );
                if ( !opt_val.has_value() ) {
                        throw nlohmann::json::parse_error::create( -1, 0, "Failed to parse ID" );
                }

                if ( opt_val.value() != ID ) {
                        throw nlohmann::json::parse_error::create( -1, 0, "ID does not match" );
                }

                id = emlabcpp::tag< ID >{};
        }
};
// TODO: this shall burn in hell
template < std::size_t N >
struct nlohmann::adl_serializer< em::protocol_message< N > >
{
        template < typename T >
        static void to_json( nlohmann::json& j, const T& msg )
        {
                for ( uint8_t b : msg ) {
                        j.push_back( b );
                }
        }

        template < typename T >
        static T from_json( const nlohmann::json& j )
        {
                if ( j.size() > T::max_size ) {
                        throw nlohmann::json::parse_error::create(
                            -1, 0, "message in json too big" );
                }
                em::static_vector< uint8_t, T::max_size > buffr;
                for ( uint8_t val : j ) {
                        buffr.push_back( val );
                }
                return *T::make( buffr );
        }
};
