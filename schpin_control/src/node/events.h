// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/base.h"
#include "node/robot/view.h"

#include <schpin_lib/geom/pose/serialization.h>
#include <schpin_lib/lua/base.h>

#pragma once

namespace schpin
{

struct get_system_time_event
{
};

struct get_leg_state_event
{
        std::string leg;
};

struct get_tip_pos_event
{
        std::string            leg;
        dof_array< em::angle > angles;
};

struct joint_goto_event
{
        std::vector< std::string > joints;
        bangle                     pos;
        btime                      time;
        end_condition              end;
};

struct joint_spin_event
{
        std::vector< std::string > joints;
        bangular_velocity          vel;
        btime                      t;
};

struct request_pos_event
{
        std::vector< std::string > joints;
};

struct sync_event
{
        std::vector< std::string > joints;
};

struct get_joints_event
{
        std::string leg;
};

struct get_legs_event
{
};

struct get_neutral_angles_event
{
        std::string leg;
};

struct get_neutral_dir_event
{
        std::string leg;
};

struct tip_goto_event
{
        std::string             leg;
        btime                   time;
        point< 3, world_frame > target;
        std::size_t             steps;
};
inline std::ostream& operator<<( std::ostream& os, const tip_goto_event& e )
{
        return os << "tip_goto: " << e.leg << "," << e.time << "," << e.target << "," << e.steps;
}

struct relative_tip_goto_event
{
        std::string             leg;
        btime                   time;
        point< 3, robot_frame > target;
        std::size_t             steps;
};
inline std::ostream& operator<<( std::ostream& os, const relative_tip_goto_event& e )
{
        return os << "relative_tip_goto: " << e.leg << "," << e.time << "," << e.target << ","
                  << e.steps;
}

struct tip_move_event
{
        std::string leg;
        btime       time;
        vec< 3 >    dir;
        std::size_t steps;
};
inline std::ostream& operator<<( std::ostream& os, const tip_move_event& e )
{
        return os << "tip_move: " << e.leg << "," << e.time << "," << e.dir << "," << e.steps;
}

struct body_goto_event
{
        slua_pose_input_var var;
        btime               time;
        std::size_t         steps;
};

inline std::ostream& operator<<( std::ostream& os, const body_goto_event& e )
{
        return os << "body_goto: " << e.var.index() << "," << e.time << "," << e.steps;
}

struct body_move_event
{
        vec< 3 >                  dir;
        quaternion< robot_frame > rot;
        btime                     time;
        std::size_t               steps;
};

inline std::ostream& operator<<( std::ostream& os, const body_move_event& e )
{
        return os << "body_move: " << e.dir << "," << e.rot << "," << e.time << "," << e.steps;
}

struct set_body_pose_event
{
        slua_pose_input_var i_var;
};

struct lua_servo_config_event
{
        std::vector< std::string > ids;
        nlohmann::json             json;
};

struct servo_config_event
{
        servo_id       id;
        nlohmann::json json;
};

}  // namespace schpin
