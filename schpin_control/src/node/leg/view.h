// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/leg/state.h"

#pragma once

namespace schpin
{

class leg_view
{
        const leg_model& model_;
        const leg_state& state_;

public:
        leg_view( const leg_model& m, const leg_state& s )
          : model_( m )
          , state_( s )
        {
        }

        const leg_model& operator*() const
        {
                return model_;
        }

        const leg_model* operator->() const
        {
                return &model_;
        }

        const dof_array< em::angle >& get_angles() const
        {
                return state_.angles;
        }

        point< 3, robot_frame > get_tip_position() const
        {
                return model_.get_tip_position( state_.angles );
        }

        em::either< dof_array< em::angle >, ik_error >
        get_angles( const point< 3, robot_frame >& target, std::size_t stop_steps = 32 ) const
        {
                return model_.get_angles( target, state_.angles, stop_steps );
        }
};

}  // namespace schpin
