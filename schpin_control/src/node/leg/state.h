// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/leg/model.h"

#pragma once

namespace schpin
{

struct leg_state
{
        dof_array< em::angle > angles;

        leg_state( const leg_model& model )
          : angles{ model.get_revolute_mapping().get_size_token() }
        {
        }

        bool update_joint_state( const leg_model& l, const std::string& jname, em::angle a )
        {
                return em::any_of(
                    em::enumerate( l.get_revolute_mapping() ),
                    [&]( std::tuple< std::size_t, const joint* > pack ) {
                            auto [i, j_ptr] = pack;
                            if ( j_ptr->name == jname ) {
                                    angles[i] = a;
                                    return true;
                            }
                            return false;
                    } );
        }
};

inline std::ostream& operator<<( std::ostream& os, const leg_state& state )
{
        return os << "leg angles: " << state.angles;
}

}  // namespace schpin
