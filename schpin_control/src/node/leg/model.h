// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/config.h"
#include "node/leg/ik.h"

#include <schpin_lib/model/leg.h>
#include <schpin_lib/model/leg/fk.h>
#include <schpin_lib/model/leg/storage.h>
#include <schpin_lib/srdf.h>

#pragma once

namespace schpin
{

using load_error = error< struct load_error_tag >;

sized_array< const joint*, dynamic_size > make_revolute_mapping( const joint& root );
pose< robot_frame >                       first_revolute_offset( const joint& root );

using leg_model_error = error< struct leg_model_error_tag >;

class leg_model
{

        std::string               name_;
        leg_config                conf_;
        std::unique_ptr< joint >  root_joint_ptr_;
        dof_array< const joint* > revolute_mapping_;
        fk_solver< dynamic_size > fk_;
        ik_solver                 ik_;

public:
        leg_model( std::string name, leg_config conf, joint jnt )
          : name_( std::move( name ) )
          , conf_( std::move( conf ) )
          , root_joint_ptr_( std::make_unique< joint >( std::move( jnt ) ) )
          , revolute_mapping_( make_revolute_mapping( *root_joint_ptr_ ) )
          , fk_( first_revolute_offset( *root_joint_ptr_ ), revolute_mapping_ )
          , ik_( first_revolute_offset( *root_joint_ptr_ ), revolute_mapping_ )
        {
        }

        const leg_config& operator*() const
        {
                return conf_;
        }

        const leg_config* operator->() const
        {
                return &conf_;
        }

        const std::string& get_name() const
        {
                return name_;
        }

        const dof_array< const joint* >& get_revolute_mapping() const
        {
                return revolute_mapping_;
        }

        opt_error< leg_model_error >
        check_angles_range( const dof_array< em::angle >& angles ) const
        {
                SCHPIN_ASSERT( angles.size() == revolute_mapping_.size() );
                std::vector< leg_model_error > tmp_errors = filter_optionals(
                    em::range( revolute_mapping_.size() ),
                    [&]( std::size_t i ) -> std::optional< leg_model_error > {
                            em::angle    alpha     = angles[i];
                            const joint* joint_ptr = revolute_mapping_[i];

                            SCHPIN_ASSERT(
                                std::holds_alternative< revolute_joint >( joint_ptr->jclass ) );

                            const auto& rev_joint = std::get< revolute_joint >( joint_ptr->jclass );

                            bool out_of_range =
                                alpha < rev_joint.min_angle || alpha > rev_joint.max_angle;

                            if ( out_of_range ) {
                                    return {
                                        E( leg_model_error )
                                        << "Angle " << alpha << " for joint " << joint_ptr->name
                                        << " is out of range: " << rev_joint.min_angle << " - "
                                        << rev_joint.max_angle };
                            }
                            return {};
                    } );

                if ( tmp_errors.empty() ) {
                        return {};
                }
                return {
                    E( leg_model_error ).group_attach( tmp_errors )
                    << " Problem with leg: " << name_ << " at angles: " << angles };
        }

        em::either< dof_array< em::angle >, ik_error > get_angles(
            const point< 3, robot_frame >& target,
            const dof_array< em::angle >&  origin,
            std::size_t                    stop_steps = 32 ) const
        {
                // TODO: ugly constant
                em::distance stop_error{ 0.0001f };
                return ik_.calculate_angles( fk_, origin, target, stop_error, stop_steps );
        }

        point< 3, robot_frame > get_tip_position( const dof_array< em::angle >& angles ) const
        {
                return fk_.get_tip_position( angles );
        }

        const joint& get_root_joint() const
        {
                return *root_joint_ptr_;
        }

        const ik_solver& get_ik() const
        {
                return ik_;
        }

        const fk_solver< dynamic_size >& get_fk() const
        {
                return fk_;
        }
};

std::vector< em::either< leg_model, load_error > >
load_leg_models( const urdf::Model&, const srdf_information&, const node_hardware_config& );

std::ostream& operator<<( std::ostream&, const leg_model& );

}  // namespace schpin
