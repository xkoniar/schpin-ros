#include "node/leg/model.h"

namespace schpin
{

sized_array< const joint*, dynamic_size > make_revolute_mapping( const joint& root )
{
        std::vector< const joint* > res;

        std::function< void( const joint& ) > f = [&]( const joint& joint ) {
                if ( std::holds_alternative< revolute_joint >( joint.jclass ) ) {
                        res.push_back( &joint );
                }
                if ( joint.sub_link.child_ptr ) {
                        f( *joint.sub_link.child_ptr );
                }
        };

        f( root );

        return sized_array< const joint*, dynamic_size >{ res };
}

pose< robot_frame > first_revolute_offset( const joint& root )
{
        if ( std::holds_alternative< revolute_joint >( root.jclass ) ) {
                return root.offset;
        }

        if ( root.sub_link.child_ptr ) {
                return transform( first_revolute_offset( *root.sub_link.child_ptr ), root.offset );
        }

        return pose< robot_frame >{};
}

std::vector< em::either< leg_model, load_error > > load_leg_models(
    const urdf::Model&          urdf_model,
    const srdf_information&     srdf_info,
    const node_hardware_config& conf )
{
        urdf::LinkConstSharedPtr model_root = urdf_model.getLink( srdf_info.body_link );

        return map_f_to_v(
            srdf_info.legs_info, [&]( srdf_leg linfo ) -> em::either< leg_model, load_error > {
                    auto root_ptr = urdf_model.getJoint( linfo.root_joint );
                    if ( !root_ptr ) {
                            return {
                                E( load_error ) << "Root joint " << linfo.root_joint
                                                << " is missing in the model. " };
                    }
                    return parse_joint_rec( urdf_model, root_ptr )
                        .convert_right( [&]( const urdf_parse_error& e ) {
                                return E( load_error ).attach( e ) << "Error during the parsing "
                                                                      "of legs for robot";
                        } )
                        .bind_left( [&]( joint root_joint ) -> em::either< leg_model, load_error > {
                                auto iter = conf.legs.find( linfo.name );
                                if ( iter == conf.legs.end() ) {
                                        return E( load_error )
                                               << "Failed to find config for leg " << linfo.name;
                                }
                                return leg_model{
                                    linfo.name, iter->second, std::move( root_joint ) };
                        } );
            } );
}

std::ostream& operator<<( std::ostream& os, const leg_model& l )
{
        return os << l.get_root_joint() << "\n"
                  << "ik:\n"
                  << l.get_ik();
}

}  // namespace schpin
