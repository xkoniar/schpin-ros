// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/geom/quaternion.h>
#include <schpin_lib/matrix.h>
#include <schpin_lib/model/leg.h>
#include <schpin_lib/model/leg/fk.h>

#pragma once

namespace schpin
{

using ik_error = error< struct ik_error_tag >;

struct ik_quat_pack
{
        quaternion< robot_frame > q;
        quaternion< robot_frame > q_deriv;
};

inline std::ostream& operator<<( std::ostream& os, const ik_quat_pack& pack )
{
        return os << "{" << pack.q << "," << pack.q_deriv << "}";
}

inline ik_quat_pack ik_get_axis_quat_deriv( const vec< 3 >& axis, const em::angle& alpha )
{
        // TODO: can we encode this property into type system somehow? :/
        SCHPIN_ASSERT( length_of( axis ) == em::length{ 1 } );

        float alpha_sin = std::sin( *alpha * 0.5f );
        float alpha_cos = std::cos( *alpha * 0.5f );

        ik_quat_pack res;

        // A. we calculate the actual quaternion, standart equation
        res.q = quaternion< robot_frame >{
            axis[0] * alpha_sin, axis[1] * alpha_sin, axis[2] * alpha_sin, alpha_cos };

        // B. we calculate it's derivation, dervation of [ cos(x/2), v*sin(x/2) ]
        // is [ -sin(x/2), v*cos(x/2) ]
        res.q_deriv = quaternion< robot_frame >{
            axis[0] * alpha_cos * 0.5f,
            axis[1] * alpha_cos * 0.5f,
            axis[2] * alpha_cos * 0.5f,
            -alpha_sin * 0.5f };

        return res;
}

class ik_solver
{
        template < typename T >
        using container = sized_array< T, dynamic_size >;

        dynamic_size::token                    size_token_;
        container< vec< 3 > >                  axis_;
        container< quaternion< robot_frame > > Q_;
        container< point< 3, robot_frame > >   P_;

public:
        using jacobian_matrix         = matrix< static_size< 3 >, dynamic_size >;
        using inverse_jacobian_matrix = matrix< dynamic_size, static_size< 3 > >;

        ik_solver(
            pose< robot_frame >              first_offset,
            const container< const joint* >& revolute_joints )
          : size_token_( revolute_joints.get_size_token() )
          , axis_( size_token_ )
          , Q_( size_token_ )
          , P_( dynamic_size::token{ size_token_.value + 1 } )
        {
                revolute_joints_dataset< dynamic_size > joint_cache{
                    first_offset, revolute_joints };

                axis_ = joint_cache.axis;
                for ( std::size_t i : em::range( size_token_.value ) ) {
                        Q_[i] = joint_cache.offsets[i].orientation;
                        P_[i] = joint_cache.offsets[i].position;
                }

                // P has one more item - point from the root of the last joint to the tip of the leg
                std::size_t  i = size_token_.value - 1;
                const joint* j = revolute_joints[i];

                std::function< point< 3, robot_frame >( const joint& ) > f = [&]( const joint& j ) {
                        if ( !j.sub_link.child_ptr ) {
                                return point< 3, robot_frame >{};
                        }
                        return transform(
                            f( *j.sub_link.child_ptr ), j.sub_link.child_ptr->offset );
                };

                point< 3, robot_frame > offset = f( *j );

                P_[i + 1] = offset;
        }

        jacobian_matrix ik_get_jacobian( const container< em::angle >& angles ) const
        {
                SCHPIN_ASSERT( angles.size() == size_token_.value );

                std::size_t size = size_token_.value;

                // step A. quaternions for  roration of joint around it's axis
                container< ik_quat_pack > w{ size_token_ };
                for ( std::size_t i : em::range( size ) ) {
                        w[i] = ik_get_axis_quat_deriv( axis_[i], angles[i] );
                }

                // step B. mid-relative positions of joints bases
                container< point< 3, robot_frame > > x{ size_token_ };
                x[size - 1] = P_[size];
                for ( std::size_t i : em::range( size - 1 ) ) {
                        std::size_t j = size - 1 - i;
                        x[j - 1] = P_[j] + cast_to_vec( rotate( rotate( x[j], w[j].q ), Q_[j] ) );
                }

                // step C. L
                container< quaternion< robot_frame > > l{ size_token_ };
                l[0] = Q_[0];
                for ( std::size_t i : em::range( std::size_t{ 1 }, size ) ) {
                        l[i] = l[i - 1] * w[i - 1].q * Q_[i];
                }

                // step D. R
                container< quaternion< robot_frame > > r{ size_token_ };
                r[0] = inverse( Q_[0] );
                for ( std::size_t i : em::range( std::size_t{ 1 }, size ) ) {
                        r[i] = inverse( Q_[i] ) * inverse( w[i - 1].q ) * r[i - 1];
                }

                // finish it!
                jacobian_matrix res{ static_size< 3 >::token{}, dynamic_size::token{ size } };
                for ( std::size_t i : em::range( size ) ) {
                        quaternion deriv_by_i =
                            l[i] *
                            ( w[i].q_deriv * x[i] * inverse( w[i].q ) +
                              w[i].q * x[i] * inverse( w[i].q_deriv ) ) *  // TODO: the usage of
                                                                           // ivnerse on the derive
                                                                           // is BAD IDEA
                            r[i];
                        for ( long j : em::range< long >( 3 ) ) {
                                res.at( j, static_cast< long >( i ) ) =
                                    deriv_by_i[static_cast< std::size_t >( j )];
                        }
                }
                return res;
        }

        container< em::angle > step(
            const fk_solver< dynamic_size >& fk,
            container< em::angle >           angles,
            point< 3, robot_frame >          target ) const
        {
                em::length max_e{ 0.01f };
                vec< 3 >   error = target - fk.get_tip_position( angles );
                if ( length_of( error ) > max_e ) {
                        error = *max_e * error / *length_of( error );
                }

                jacobian_matrix j = ik_get_jacobian( angles );

                auto angle_change = j.pseudo_inverse() * error;
                for ( std::size_t i : em::range( angles.size() ) ) {
                        angles[i] += em::angle{ angle_change[i] };
                }
                return angles;
        }

        em::either< container< em::angle >, ik_error > calculate_angles(
            const fk_solver< dynamic_size >& fk,
            container< em::angle >           angles,
            point< 3, robot_frame >          target,
            em::distance                     stop_error,
            std::size_t                      stop_steps ) const
        {

                std::size_t i = 0;
                while ( i < stop_steps ) {
                        point< 3, robot_frame > tip_pos = fk.get_tip_position( angles );
                        vec< 3 >                error   = target - tip_pos;
                        if ( length_of( error ) <= stop_error ) {
                                break;
                        }

                        angles = step( fk, angles, target );
                        i += 1;
                }

                if ( length_of( target - fk.get_tip_position( angles ) ) > stop_error ) {
                        return {
                            E( ik_error ) << "Failed to find IK solution, error too big after "
                                          << stop_steps << " steps, target: " << target
                                          << " pos: " << fk.get_tip_position( angles ) };
                }
                return { angles };
        }

        friend std::ostream& operator<<( std::ostream& os, const ik_solver& s )
        {
                return os << "axis: " << s.axis_ << "\nQ: " << s.Q_ << "\nP:" << s.P_;
        }
};

}  // namespace schpin
