// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/lewan/config.h"
#include "node/base.h"
#include "node/config.h"
#include "shared/protocol.h"

#include <emlabcpp/protocol/register_handler.h>
#include <magic_enum.hpp>
#include <schpin_lib_exp/protocol/register_map.h>

#pragma once

namespace schpin
{

class servo
{
        std::string       name_;
        node_servo_config conf_;
        bool      config_changed_ = true;  // default true is important - config set after init!
        em::angle state_;

public:
        servo( std::string name, const node_servo_config& c )
          : name_( std::move( name ) )
          , conf_( c )
        {
        }

        const node_servo_config& operator*() const
        {
                return conf_;
        }

        const node_servo_config* operator->() const
        {
                return &conf_;
        }

        const std::string& get_name() const
        {
                return name_;
        }

        void set_config( const nlohmann::json& j )
        {
                std::visit(
                    [&]( auto& conf ) {
                            json_guard( [&]() {
                                    fill_from_json( conf, j );
                            } ).optionally( [&]( json_storage_error e ) {
                                    SCHPIN_ERROR_LOG( "wololo", "Failed to set config: " << e );
                            } );
                            SCHPIN_INFO_LOG(
                                "wololo", "setting config to: " << nlohmann::json{ conf } );
                    },
                    conf_.cvar );
                config_changed_ = true;
        }

        node_fw_group_value make_pos_value() const
        {
                return make_value< POSITION >();
        }

        node_fw_group_value make_goto_value( bangle a, btime t, end_condition end ) const
        {
                return make_value< GOTO >( a, t, end );
        }

        node_fw_group_value make_spin_value( bangular_velocity av, btime t ) const
        {
                return make_value< SPIN >( av, t );
        }

        node_fw_group_value make_activate_value() const
        {
                // TODO: this actually may be an error
                return node_fw_group::make_val< ACTIVATE >( servo_addr{ LEWAN, conf_.id } );
        }

        template < typename Visitor >
        void on_feedback( servo_master_variant v, Visitor&& vis )
        {
                // this is not a good approach, the "boot" of servos is not well defined in APP,
                // there should be proper process for it -> servo is actiavted, than config is
                // loaded
                if ( config_changed_ ) {
                        config_changed_ = false;
                        upload_config( vis );
                }

                em::apply_on_match(
                    v,
                    [&]( em::tag< POSITION >, bangle a ) {
                            state_ = em::angle{ a.as_float() };
                            vis( *this, state_ );
                    },
                    [&]( auto... ) {} );
        }

private:
        template < typename Visitor >
        void upload_config( Visitor&& vis )
        {
                SCHPIN_INFO_LOG( "servo", "Uploading config to: " << conf_.id );
                std::visit(
                    [&]( const auto& cmap ) {
                            using config_handler =
                                em::protocol_register_handler< std::decay_t< decltype( cmap ) > >;
                            em::protocol_for_each_register(
                                cmap, [&]< auto key >( auto val ) {
                                        SCHPIN_INFO_LOG(
                                            "servo",
                                            "Setting config val: " << magic_enum::enum_name( key )
                                                                   << " " << val );

                                        static_assert(
                                            config_handler::max_size <
                                            std::tuple_size_v< config_buffer > );

                                        auto msg = config_handler::template serialize< key >( val );
                                        config_buffer buffer{};
                                        std::copy( msg.begin(), msg.end(), buffer.begin() );

                                        vis( make_value< CONFIG_SET >( key, buffer ) );
                                } );
                    },
                    conf_.cvar );
                // TODO: this may not be needed
                std::this_thread::sleep_for( std::chrono::milliseconds( 150 ) );
        }
        template < messages_enum CMD, typename... Args >
        node_fw_group_value make_value( Args... args ) const
        {
                return { node_fw_group::make_val< SERVO >(
                    conf_.id, master_servo_group::make_val< CMD >( args... ) ) };
        }
};

inline std::ostream& operator<<( std::ostream& os, const servo& s )
{
        os << s.get_name() << " - " << s->joint_name << " - " << s->id << "\n";
        std::visit(
            [&]( const auto& conf ) {
                    os << conf;
            },
            s->cvar );
        return os;
}

class servo_group
{
        std::vector< servo > servos_;
        template < typename Obj, typename UnaryPredicate >
        static auto find_servo( Obj& obj, UnaryPredicate&& f )
        {
                return em::find_if( obj.servos_, f );
        }

public:
        using iterator       = std::vector< servo >::iterator;
        using const_iterator = std::vector< servo >::const_iterator;

        servo_group( const std::map< std::string, node_servo_config >& configs )
          : servos_(
                map_f_to_v( configs, [&]( const std::pair< std::string, node_servo_config >& cp ) {
                        return servo{ cp.first, cp.second };
                } ) )
        {
        }

        const std::vector< servo >& get_servos() const
        {
                return servos_;
        }

        iterator find_servo( servo_id id )
        {
                return find_servo( *this, [&]( const auto& s ) {
                        return s->id == id;
                } );
        }

        const_iterator find_servo( servo_id id ) const
        {
                return find_servo( *this, [&]( const auto& s ) {
                        return s->id == id;
                } );
        }

        template < typename UnaryPredicate >
        iterator find_servo( UnaryPredicate&& f )
        {
                return find_servo( *this, f );
        }

        template < typename UnaryPredicate >
        const_iterator find_servo( UnaryPredicate&& f ) const
        {
                return find_servo( *this, f );
        }

        void set_servo_config( servo_id id, const nlohmann::json& j )
        {
                auto iter = find_servo( id );
                if ( iter == servos_.end() ) {
                        SCHPIN_ERROR_LOG(
                            "servos", "Got servo config command for non-existing servo: " << id );
                        return;
                }
                iter->set_config( j );
        }

        template < typename Visitor >
        void on_feedback( servo_id id, servo_master_variant v, Visitor&& vis )
        {
                auto iter = find_servo( id );
                if ( iter == servos_.end() ) {
                        SCHPIN_ERROR_LOG(
                            "servos", "Got servo command for non-existing servo: " << id );
                        return;
                }

                iter->on_feedback( v, vis );
        }
};

inline std::ostream& operator<<( std::ostream& os, const servo_group& g )
{
        os << "servo group: " << std::endl;
        for ( const servo& s : g.get_servos() ) {
                os << s << std::endl;
        }
        return os;
}

}  // namespace schpin
