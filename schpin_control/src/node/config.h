// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/base.h"
#include "fw/dynmx/config.h"
#include "fw/lewan/config.h"
#include "node/base.h"

#include <emlabcpp/physical_quantity.h>
#include <schpin_lib/geom/point.h>
#include <schpin_lib/geom/vec.h>
#include <schpin_lib_exp/config.h>

#pragma once

namespace schpin
{

template < typename T >
auto make_control_range_params( T servo_min, T servo_max )
{
        return std::make_tuple(
            ros_opt_param< bangle >(
                "from_angle", "angle at which movement starts", bangle::from_float( 0.f ) ),
            ros_opt_param< bangle >(
                "to_angle", "angle at which movement ends", bangle::from_float( 4.18879f ) ),
            ros_opt_param< T >( "from_servo", "servo angle at which movement starts", servo_min ),
            ros_opt_param< T >( "to_servo", "servo angle at which movement ends", servo_max ) );
}

template < typename T >
inline auto make_pid_param( const std::string& prefix, char term, T def )
{
        return ros_opt_param< T >(
            prefix + "_" + static_cast< char >( std::tolower( term ) ),
            static_cast< char >( std::toupper( term ) ) + " gain in " + prefix + "PID regulator",
            def );
}

template < typename T >
inline auto make_angle_change_param( T def )
{
        return ros_opt_param< T >(
            "angle_change",
            "change of angle that results in automatic position report, use servo units",
            def );
}

struct node_lewan_servo_config
{
        lewan_config_map cmap;

        template < typename... Ts >
        node_lewan_servo_config( Ts... items )
          : cmap( items... )
        {
        }

        static auto ros_param_get()
        {
                lewan_config_map def;

                return std::tuple_cat(
                    make_control_range_params(
                        def.get_val< LEWAN_SERVO_RANGE_LOW >(),
                        def.get_val< LEWAN_SERVO_RANGE_HIGH >() ),
                    std::make_tuple(
                        make_angle_change_param< lewan_angle >(
                            def.get_val< LEWAN_ANGLE_CHANGE_REPORT >() ),
                        ros_opt_param< lewan_angle >(
                            "stall_range", "???", def.get_val< LEWAN_ANGLE_STALL_RANGE >() ),
                        ros_opt_param< bool >(
                            "enable_telem", "Should telemetry be enable", false ),
                        make_pid_param( "goto", 'p', def.get_val< LEWAN_GOTO_P_GAIN >() ),
                        make_pid_param( "goto", 'i', def.get_val< LEWAN_GOTO_I_GAIN >() ),
                        make_pid_param( "goto", 'd', def.get_val< LEWAN_GOTO_D_GAIN >() ),
                        make_pid_param( "vel", 'p', def.get_val< LEWAN_VEL_P_GAIN >() ),
                        make_pid_param( "vel", 'i', def.get_val< LEWAN_VEL_I_GAIN >() ),
                        make_pid_param( "vel", 'd', def.get_val< LEWAN_VEL_D_GAIN >() ) ) );
        }
};

struct node_dynmx_servo_config
{
        dynmx_config_map cmap;

        template < typename... Ts >
        node_dynmx_servo_config( Ts... items )
          : cmap( items... )
        {
        }

        static auto ros_param_get()
        {
                dynmx_config_map def;

                return std::tuple_cat(
                    make_control_range_params(
                        def.get_val< DYNMX_SERVO_RANGE_LOW >(),
                        def.get_val< DYNMX_SERVO_RANGE_HIGH >() ),
                    std::make_tuple(
                        make_pid_param( "goto", 'p', def.get_val< DYNMX_POS_P_GAIN_C >() ),
                        make_pid_param( "goto", 'i', def.get_val< DYNMX_POS_I_GAIN_C >() ),
                        make_pid_param( "goto", 'd', def.get_val< DYNMX_POS_D_GAIN_C >() ),
                        make_pid_param( "vel", 'p', def.get_val< DYNMX_VEL_P_GAIN_C >() ),
                        make_pid_param( "vel", 'i', def.get_val< DYNMX_VEL_I_GAIN_C >() ) ) );
        }
};

using node_servo_config_var = std::variant< lewan_config_map, dynmx_config_map >;

struct node_servo_config
{
        servo_id              id;
        servo_type            stype;
        std::string           joint_name;
        node_servo_config_var cvar;
};

struct leg_config
{
        dof_array< bangle > neutral_angles;
        vec< 3 >            neutral_dir;

        static auto ros_param_get()
        {
                return std::make_tuple(
                    ros_param< dof_array< bangle > >(
                        "neutral_angles", "Angles of joints in neutral position" ),
                    ros_param< vec< 3 > >(
                        "neutral_dir", "Direction in which neutral movement should go" ) );
        }
};

struct node_hardware_config
{
        std::map< std::string, node_servo_config > servos;
        std::map< std::string, leg_config >        legs;
        std::string                                diag_topic;
        float                                      diag_freq;
        std::string                                joint_state_topic;
        std::string                                robot_description;
        std::string                                robot_semantics;
        std::optional< std::filesystem::path >     opt_script;

        static auto ros_param_get()
        {
                return std::make_tuple(
                    ros_param< std::map< std::string, node_servo_config > >(
                        "servos", "definitions of servomotors" ),
                    ros_param< std::map< std::string, leg_config > >(
                        "legs", "definitions of legs" ),
                    ros_param< std::string >( "diag_topic", "Diagnostics topic" ),
                    ros_param< float >( "diag_freq", "Diagnostic frequency" ),
                    ros_param< std::string >(
                        "joint_state", "Topic at which send updates of the joint positions." ),
                    ros_param< std::string >( "robot_description", "URDF description" ),
                    ros_param< std::string >( "robot_semantics", "SRDF descriptoon" ),
                    opt_ros_param_avoid_default< std::filesystem::path >(
                        "script", "Script to execute", std::filesystem::path{ "" } ) );
        }
};

template <>
struct ros_param_serializer< node_servo_config >
{

        static auto get_var( rclcpp::Node& node, servo_type stype, const std::string& prefix )
        {
                switch ( stype ) {
                        case LEWAN:
                        case LEWAN_TEST:
                                return ros_param_serializer< node_lewan_servo_config >::load(
                                           node, prefix )
                                    .convert_left(
                                        [&](
                                            node_lewan_servo_config cfg ) -> node_servo_config_var {
                                                return cfg.cmap;
                                        } );
                        case DYNMX:
                                return ros_param_serializer< node_dynmx_servo_config >::load(
                                           node, prefix )
                                    .convert_left(
                                        [&](
                                            node_dynmx_servo_config cfg ) -> node_servo_config_var {
                                                return cfg.cmap;
                                        } );
                }
                return em::either< node_servo_config_var, config_error >{
                    E( config_error ) << "Unknown servo type for config load" };
        }
        static em::either< node_servo_config, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return em::assemble_left_collect_right(
                           ros_param< servo_id >( "id", "id of the servo on its bus" )
                               .load( node, prefix ),
                           ros_param< servo_type >( "type", "type of the servomotor" )
                               .load( node, prefix ),
                           ros_param< std::string >(
                               "joint_name", "name of the joint that is used for this servo" )
                               .load( node, prefix ) )
                    .convert_right( [&]( const auto& errs ) {
                            return E( config_error ).group_attach( errs )
                                   << "Failed to get servo config header";
                    } )
                    .bind_left( [&]( std::tuple< servo_id, servo_type, std::string > pack ) {
                            auto [sid, stype, jname] = std::move( pack );
                            return get_var( node, stype, prefix )
                                .convert_left( [&]( node_servo_config_var cvar ) {
                                        return node_servo_config{ sid, stype, jname, cvar };
                                } );
                    } );
        }
};

}  // namespace schpin
