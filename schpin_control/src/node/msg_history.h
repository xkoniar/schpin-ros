// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "shared/protocol.h"

#include <deque>
#include <rclcpp/rclcpp.hpp>
#include <schpin_lib/logging.h>

#pragma once

namespace schpin
{
class msg_history
{
        struct msg_record
        {
                node_fw_message msg;
                ctl_id          id;
                rclcpp::Time    sent_at;
        };
        std::deque< msg_record > history_;

public:
        using const_iterator = typename std::deque< msg_record >::const_iterator;

        void insert( ctl_id cid, node_fw_message m, rclcpp::Time now )
        {
                history_.push_back( msg_record{ m, cid, now } );
        }

        const_iterator begin() const
        {
                return history_.begin();
        }

        const_iterator find( ctl_id id ) const
        {
                return em::find_if( history_, [&]( const msg_record& rec ) {
                        return rec.id == id;
                } );
        }

        void try_log( ctl_id id ) const
        {
                const_iterator iter = find( id );
                if ( iter == end() ) {
                        SCHPIN_INFO_LOG(
                            "msg_history",
                            "History record for msg with id " << id << " is missing" );
                } else {
                        SCHPIN_INFO_LOG(
                            "msg_history",
                            "Msg with id " << id << " was sent at " << iter->sent_at.seconds()
                                           << " and is:" << iter->msg );
                }
        }

        const_iterator end() const
        {
                return history_.end();
        }

        void drop( ctl_id cid )
        {
                auto iter = find( cid );
                if ( iter == end() ) {
                        return;
                }
                history_.erase( iter );
        }
};

}  // namespace schpin
