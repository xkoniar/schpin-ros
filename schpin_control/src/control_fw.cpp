// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/logging.h>
//
#include "fw/dynmx/sequencer.h"
#include "fw/lewan/sequencer.h"
#include "fw/reactor.h"
#include "fw/util.h"
#include "shared/serial.h"

#include <chrono>
#include <schpin_lib/util.h>
#include <schpin_lib_exp/config.h>
#include <schpin_msgs/msg/control_cmd.hpp>

using namespace std::chrono_literals;
using namespace schpin;

struct control_pc_config
{
        std::optional< std::filesystem::path > opt_device;
        std::optional< servo_type >            opt_servo_override;

        static auto ros_param_get()
        {
                return std::make_tuple(
                    opt_ros_param_avoid_default< std::filesystem::path >{
                        "dev",
                        "device to open for serial communication",
                        std::filesystem::path{ "" } },
                    ros_param< std::optional< servo_type > >{
                        "servo_override", "optionally specify to override servo" } );
        }
};

inline std::ostream& operator<<( std::ostream& os, const control_pc_config& conf )
{
        os << "Device to open: ";
        if ( conf.opt_device ) {
                os << *conf.opt_device;
        } else {
                os << "nothing";
        }
        os << "\n";
        // abstract this somehow or something?
        os << "Servo override: ";
        if ( conf.opt_servo_override ) {
                os << *conf.opt_servo_override;
        } else {
                os << "nothing";
        }
        os << "\n";
        return os;
}

class control_node
{

        control_pc_config                  config_;
        fw_reactor< 20 >                   reactor_;
        std::optional< serial_connection > opt_ser_;

        rclcpp::Publisher< schpin_msgs::msg::ControlCmd >::SharedPtr pub_;

        struct control_node_visitor : fw_reactor_visitor
        {
                ctl_id        id;
                control_node& cnode;

                control_node_visitor( ctl_id cid, control_node& n )
                  : id( cid )
                  , cnode( n )
                {
                }

                void to_control( const fw_node_message& m )
                {
                        schpin_msgs::msg::ControlCmd msg;
                        msg.data = map_f_to_v( m, []( uint8_t val ) {
                                return val;
                        } );
                        cnode.pub_->publish( msg );
                }

                void to_servo( s_ctl_id msg_id, reply r, const master_dynmx_message& m )
                {
                        SCHPIN_ASSERT( cnode.opt_ser_ );
                        cnode.opt_ser_->write( m );
                        if ( r == reply::YES ) {
                                cnode.receive_serial_msg< dynmx_sequencer< dynmx_master_message > >(
                                    msg_id, m );
                        }
                }
                void to_servo( s_ctl_id msg_id, reply r, const master_lewan_message& m )
                {
                        SCHPIN_ASSERT( cnode.opt_ser_ );
                        cnode.opt_ser_->write( m );
                        if ( r == reply::YES ) {
                                cnode.receive_serial_msg< lewan_sequencer< lewan_master_message > >(
                                    msg_id, m );
                        }
                }
        };

public:
        control_node( const control_node& ) = delete;
        control_node( control_node&& )      = delete;
        control_node& operator=( const control_node& ) = delete;
        control_node& operator=( control_node&& ) = delete;

        control_node(
            rclcpp::Node&                      rcnode,
            std::optional< serial_connection > ser,
            control_pc_config                  config )
          : config_( std::move( config ) )
          , reactor_( config.opt_servo_override )
          , opt_ser_( ser )
          , pub_( rcnode.create_publisher< schpin_msgs::msg::ControlCmd >(
                "/control/feedback",
                200 ) )
        {
                // TODO no delay subscription?
        }

        template < typename Sequencer, typename SourceMsg >
        void receive_serial_msg( s_ctl_id msg_id, const SourceMsg& m )
        {
                SCHPIN_ASSERT( opt_ser_ );
                control_node_visitor vis{ msg_id, *this };

                opt_ser_->read_with_sequencer< Sequencer >().match(
                    [&]( auto m ) {
                            reactor_.on_servo_receive( get_system_time(), msg_id, m, vis );
                    },
                    [&]( auto err ) {
                            reactor_.on_servo_not_replying( m, vis );
                            SCHPIN_ERROR_LOG( "control", err );
                    } );
        }

        void control_topic( schpin_msgs::msg::ControlCmd::SharedPtr msg )
        {
                auto opt_msg = node_fw_message::make( msg->data );
                if ( !opt_msg ) {
                        SCHPIN_ERROR_LOG(
                            "node", "Data that arrived from control topic are too long" );
                        return;
                }
                node_fw_extract( *opt_msg )
                    .match(
                        [&]( std::tuple< ctl_id, node_fw_group_value, checksum > pack ) {
                                auto [id, val, chcksm] = pack;
                                control_node_visitor vis{ id, *this };
                                reactor_.on_control_receive( id, val, vis );
                        },
                        [&]( auto e ) {  // TODO: print the error
                                SCHPIN_ERROR_LOG(
                                    "control_pc",
                                    "protocol parsin error: " << e
                                                              << " for message: " << *opt_msg );
                        } );
        }

        void tick( clk_time clock_time )
        {
                control_node_visitor vis{ ctl_id{ 0 }, *this };
                reactor_.tick( clock_time, vis );
        }

        ~control_node() = default;
};

class control_supervizor : public rclcpp::Node
{
        std::optional< control_node >                                   control_;
        rclcpp::TimerBase::SharedPtr                                    timer_;
        rclcpp::Subscription< schpin_msgs::msg::ControlCmd >::SharedPtr sub_;

public:
        control_supervizor()
          : Node(
                "control",
                rclcpp::NodeOptions()
                    .allow_undeclared_parameters( true )
                    .automatically_declare_parameters_from_overrides( true ) )
          , sub_( this->create_subscription< schpin_msgs::msg::ControlCmd >(
                "/control/control",
                200,
                std::bind( &control_supervizor::control_topic, this, std::placeholders::_1 ) ) )
        {
                get_config< control_pc_config >( *this )
                    .bind_left(
                        [&]( control_pc_config config )
                            -> em::either<
                                std::tuple< control_pc_config, std::optional< serial_connection > >,
                                serial_error > {
                                if ( !config.opt_device ) {
                                        return { std::make_tuple(
                                            config, std::optional< serial_connection >() ) };
                                }
                                return serial_connection::make( *config.opt_device )
                                    .convert_left( [&]( serial_connection port ) {
                                            SCHPIN_INFO_LOG(
                                                "node",
                                                "Created serial connection over "
                                                    << *config.opt_device );
                                            return std::make_tuple(
                                                config, std::make_optional( port ) );
                                    } );
                        } )
                    .match(
                        [&]( std::tuple< control_pc_config, std::optional< serial_connection > >
                                 pack ) {
                                auto [conf, port] = pack;

                                SCHPIN_INFO_LOG( "node", "Node initialized and running" );
                                SCHPIN_INFO_LOG( "node", "Config: " << conf );

                                control_.emplace( *this, port, std::move( conf ) );

                                timer_ = this->create_wall_timer(
                                    1ms, std::bind( &control_supervizor::tick, this ) );
                        },
                        [&]( auto e ) {
                                std::cerr << simplified_error{ e };
                                std::terminate();
                        } );
        }

        void control_topic( schpin_msgs::msg::ControlCmd::SharedPtr msg )
        {
                control_->control_topic( msg );
        }

        void tick()
        {
                control_->tick( get_system_time() );
        }
};

int main( int argc, char* argv[] )
{
        rclcpp::init( argc, argv );
        rclcpp::spin( std::make_shared< control_supervizor >() );
        rclcpp::shutdown();
        return 0;
}
