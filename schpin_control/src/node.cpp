// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/components/context.h"
#include "node/components/control.h"
#include "node/components/telemetry.h"
#include "node/load.h"
#include "node/servos.h"
#include "shared/protocol.h"

#include <chrono>
#include <emlabcpp/zip.h>
#include <schpin_lib/json.h>
#include <schpin_lib/lua/pose.h>
#include <schpin_msgs/msg/control_cmd.hpp>
#include <schpin_msgs/srv/control_ctl.hpp>
#include <schpin_msgs/srv/lua_call.hpp>
#include <schpin_msgs/srv/lua_def.hpp>
#include <std_msgs/msg/string.h>

using namespace schpin;
using namespace std::chrono_literals;

using lua_def  = schpin_msgs::srv::LuaDef;
using lua_call = schpin_msgs::srv::LuaCall;

class node
{

        node_hardware_config config_;

        rclcpp::Publisher< schpin_msgs::msg::ControlCmd >::SharedPtr control_pub_;

        lua_vm_interactive lua_vm_;

        context_component   context_comp_;
        control_component   ctl_comp_;
        telemetry_component telem_comp_;

        ctl_id msg_id_ = 0;

        struct feedback_handler
        {
                node&         node_;
                rclcpp::Node& rcl_node_;

                ctl_id operator()( const machine_feedback_var& var )
                {
                        return em::match(
                            var,
                            [&]( node_fw_group_value val ) -> ctl_id {
                                    return node_.transmit( val, rcl_node_ );
                            },
                            [&]( set_body_pose_event e ) -> ctl_id {
                                    node_.force_body_pose( e.i_var );
                                    return ctl_id{ 0 };
                            },
                            [&]( const servo_config_event& e ) -> ctl_id {
                                    node_.set_servo_config( e.id, e.json );
                                    return ctl_id{ 0 };
                            } );
                }
        };

public:
        node( const node& ) = delete;
        node( node&& )      = delete;
        node& operator=( const node& ) = delete;
        node& operator=( node&& ) = delete;

        node( rclcpp::Node& rcnode, node_hardware_config c, robot_model rob )
          : config_( std::move( c ) )
          , control_pub_(
                rcnode.create_publisher< schpin_msgs::msg::ControlCmd >( "/control/control", 200 ) )
          , context_comp_( rcnode, std::move( rob ), config_ )
          , ctl_comp_( rcnode, get_context(), config_ )
          , telem_comp_( rcnode, config_ )
        {
        }
        std::string get_json_def() const
        {
                return lua_vm_.get_json_def();
        }
        std::tuple< bool, std::string >
        on_lua_call( std::string name, nlohmann::json args, rclcpp::Node& rcl_node )
        {
                // TODO: feedback_hander/exec_bind are btoh taking a node as argument :/
                using res_type = std::tuple< bool, std::string >;
                return lua_vm_
                    .exec_bind(
                        name, args, get_context(), rcl_node, feedback_handler{ *this, rcl_node } )
                    .convert_left( [&]( std::optional< nlohmann::json > opt_j ) -> res_type {
                            std::string response;
                            if ( opt_j ) {
                                    response = opt_j->dump();
                            }
                            return { true, response };
                    } )
                    .convert_right( [&]( auto e ) -> res_type {
                            return { false, to_string( e ) };
                    } )
                    .join();
        }

        auto comps()
        {
                return std::tie( context_comp_, ctl_comp_, telem_comp_ );
        }

        const node_context& get_context()
        {
                return context_comp_.get_context();
        }

        void tick( rclcpp::Node& rcl_node )
        {
                em::for_each( comps(), [&]( auto& comp ) {
                        comp.tick( get_context(), rcl_node, feedback_handler{ *this, rcl_node } );
                } );
        }

        void force_body_pose( const slua_pose_input_var& ivar )
        {
                pose< world_frame > bpose =
                    slua_get_real_pose( ivar, get_context().rstate.body_pose );
                context_comp_.set_body_pose( bpose );
        }

        void set_servo_config( servo_id id, const nlohmann::json& j )
        {
                context_comp_.set_servo_config( id, j );
        }

        void on_feedback( fw_node_message msg, rclcpp::Node& node )
        {
                fw_node_extract( msg ).match(
                    [&]( std::tuple< ctl_id, fw_node_group_value, checksum > pack ) {
                            auto cid    = std::get< 0 >( pack );
                            auto subvar = std::get< 1 >( pack );
                            em::for_each( comps(), [&]( auto& comp ) {
                                    comp.on_feedback(
                                        subvar,
                                        get_context(),
                                        node,
                                        feedback_handler{ *this, node } );
                            } );
                            context_comp_.feedback_processed( cid );
                    },
                    [&]( auto e ) {
                            SCHPIN_ERROR_LOG( "node", e << " for " << msg );
                    } );
        }

        ctl_id transmit( node_fw_group_value val, rclcpp::Node& node )
        {
                ctl_id msg_id = msg_id_;
                msg_id_++;

                auto m = node_fw_serialize( node_fw_protocol::make_val( msg_id, val, checksum{} ) );

                schpin_msgs::msg::ControlCmd msg;
                msg.data = map_f_to_v( m, []( uint8_t val ) {
                        return val;
                } );
                control_pub_->publish( msg );
                context_comp_.record_msg_sent( msg_id, m, node.now() );

                return msg_id;
        }
};

class node_supervizor : public rclcpp::Node
{
        std::optional< node >        node_ptr_;
        rclcpp::TimerBase::SharedPtr timer_;

        rclcpp::Subscription< schpin_msgs::msg::ControlCmd >::SharedPtr control_sub_;

        rclcpp::Service< lua_def >::SharedPtr  lua_def_;
        rclcpp::Service< lua_call >::SharedPtr lua_call_;

public:
        node_supervizor()
          : Node(
                "node",
                rclcpp::NodeOptions()
                    .enable_rosout( true )
                    .allow_undeclared_parameters( true )
                    .automatically_declare_parameters_from_overrides( true ) )
          , control_sub_( this->create_subscription< schpin_msgs::msg::ControlCmd >(
                "/control/feedback",
                200,
                std::bind( &node_supervizor::on_feedback, this, std::placeholders::_1 ) ) )
          , lua_def_( this->create_service< lua_def >(
                "/control/lua/def",
                std::bind(
                    &node_supervizor::on_lua_def,
                    this,
                    std::placeholders::_1,
                    std::placeholders::_2,
                    std::placeholders::_3 ) ) )
          , lua_call_( this->create_service< lua_call >(
                "/control/lua/call",
                std::bind(
                    &node_supervizor::on_lua_call,
                    this,
                    std::placeholders::_1,
                    std::placeholders::_2,
                    std::placeholders::_3 ) ) )
        {
                load_node_data( *this ).match(
                    [&]( std::tuple< node_hardware_config, robot_model > pack ) {
                            node_ptr_.emplace(
                                *this,
                                std::move( std::get< node_hardware_config >( pack ) ),
                                std::move( std::get< robot_model >( pack ) ) );
                            SCHPIN_INFO_LOG( "control", "Control initialized and running" );

                            timer_ = this->create_wall_timer(
                                10ms, std::bind( &node_supervizor::tick, this ) );
                    },
                    [&]( const load_error& e ) {
                            std::cerr << simplified_error{ e };
                            std::terminate();
                    } );
        }

        void tick()
        {
                SCHPIN_ASSERT( node_ptr_ );
                node_ptr_->tick( *this );
        }

        void on_feedback( schpin_msgs::msg::ControlCmd::SharedPtr msg )
        {
                auto opt_msg = fw_node_message::make( msg->data );
                if ( !opt_msg ) {
                        SCHPIN_ERROR_LOG(
                            "node", "input msg is too long: " << em::view{ msg->data } );
                        return;
                }

                node_ptr_->on_feedback( *opt_msg, *this );
        }

        bool on_lua_def(
            std::shared_ptr< rmw_request_id_t >,
            std::shared_ptr< lua_def::Request >,
            std::shared_ptr< lua_def::Response > resp )
        {
                resp->definition = node_ptr_->get_json_def();
                return true;
        }

        bool on_lua_call(
            std::shared_ptr< rmw_request_id_t >,
            std::shared_ptr< lua_call::Request >  req,
            std::shared_ptr< lua_call::Response > resp )
        {
                nlohmann::json j = nlohmann::json::parse( req->args );
                SCHPIN_INFO_LOG( "node", "got call: " << req->name << " res: " << j );
                std::tie( resp->success, resp->response ) =
                    node_ptr_->on_lua_call( req->name, j, *this );
                return true;
        }
};

int main( int argc, char* argv[] )
{

        rclcpp::init( argc, argv );
        rclcpp::spin( std::make_shared< node_supervizor >() );
        rclcpp::shutdown();
        return 0;
}
