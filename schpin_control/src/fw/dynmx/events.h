#include "fw/base.h"
#include "fw/servo/events.h"

#pragma once

namespace schpin
{

struct dynmx_goto_event
{
        dynmx_angle   goal_pos;
        btime         t;
        end_condition end;
};

struct dynmx_spin_event
{
        dynmx_angular_velocity velocity;
        btime                  t;
};

using dynmx_servo_events_variant =
    std::variant< dynmx_goto_event, dynmx_spin_event, servo_config_set_event, servo_id_event >;

}  // namespace schpin
