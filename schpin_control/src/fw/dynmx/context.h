// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/base.h"
#include "fw/dynmx/protocol.h"
#include "fw/servo/visitor.h"

#include <emlabcpp/protocol/register_map.h>

#pragma once

namespace schpin
{

enum dynmx_reg_enum : uint16_t
{
        DYNMX_MODEL_NUMBER    = 0,
        DYNMX_MODEL_INFO      = 2,
        DYNMX_FW_VERSION      = 6,
        DYNMX_SERVO_ID        = 7,
        DYNMX_BAUD_RATE       = 8,
        DYNMX_RETURN_DELAY    = 9,
        DYNMX_DRIVE_MODE      = 10,
        DYNMX_OPER_MODE       = 11,
        DYNMX_SEC_ID          = 12,
        DYNMX_PROTO_TYPE      = 13,
        DYNMX_HOME_OFFSET     = 20,
        DYNMX_MOVING_TRESHOLD = 24,
        DYNMX_TEMP_LIMIT      = 31,
        DYNMX_MAX_VOLT        = 32,
        DYNMX_MIN_VOLT        = 34,
        DYNMX_PWM_LIMIT       = 36,
        DYNMX_VEL_LIMIT       = 44,
        DYNMX_MAX_POS         = 48,
        DYNMX_MIN_POS         = 52,
        DYNMX_SHUTDOWN        = 63,
        DYNMX_TORQUE_EN       = 64,
        DYNMX_LED_EN          = 65,
        DYNMX_STATUS_RETURN   = 68,
        DYNMX_REGISTERED_INST = 69,
        DYNMX_ERROR_STATUS    = 70,
        DYNMX_VEL_I_GAIN      = 76,
        DYNMX_VEL_P_GAIN      = 78,
        DYNMX_POS_D_GAIN      = 80,
        DYNMX_POS_I_GAIN      = 82,
        DYNMX_POS_P_GAIN      = 84,
        DYNMX_FF_SCND_GAIN    = 88,
        DYNMX_FF_FRST_GAIN    = 90,
        DYNMX_BUS_WATCHDOG    = 98,
        DYNMX_GOAL_PWM        = 100,
        DYNMX_GOAL_VEL        = 104,
        DYNMX_PROFILE_ACCEL   = 108,
        DYNMX_PROFILE_VEL     = 112,
        DYNMX_GOAL_POS        = 116,
        DYNMX_REALTIME_TICK   = 120,
        DYNMX_MOVING          = 122,
        DYNMX_MOVING_STATUS   = 123,
        DYNMX_PRESENT_PWM     = 124,
        DYNMX_PRESENT_LOAD    = 126,
        DYNMX_PRESENT_VEL     = 128,
        DYNMX_PRESENT_POS     = 132,
        DYNMX_VEL_TRAJECTORY  = 136,
        DYNMX_PSO_TRAJECTORY  = 140,
        DYNMX_PRESENT_VOLTAGE = 144,
        DYNMX_PRESENT_TEMP    = 146
};

struct dynmx_map : em::protocol_register_map<
                       em::PROTOCOL_LITTLE_ENDIAN,
                       em::protocol_reg< DYNMX_MODEL_NUMBER, dynmx_model_number >,
                       em::protocol_reg< DYNMX_MODEL_INFO, dynmx_model_info >,
                       em::protocol_reg< DYNMX_FW_VERSION, dynmx_fw_version >,
                       em::protocol_reg< DYNMX_SERVO_ID, dynmx_servo_id >,
                       em::protocol_reg< DYNMX_BAUD_RATE, dynmx_baud_rate >,
                       em::protocol_reg< DYNMX_RETURN_DELAY, dynmx_delay_time >,
                       em::protocol_reg< DYNMX_DRIVE_MODE, dynmx_drive_mode >,
                       em::protocol_reg< DYNMX_OPER_MODE, dynmx_oper_mode >,
                       em::protocol_reg< DYNMX_SEC_ID, dynmx_servo_id >,
                       em::protocol_reg< DYNMX_PROTO_TYPE, dynmx_proto_type >,
                       em::protocol_reg< DYNMX_HOME_OFFSET, dynmx_home_offset >,
                       em::protocol_reg< DYNMX_MOVING_TRESHOLD, dynmx_vel_limit >,
                       em::protocol_reg< DYNMX_TEMP_LIMIT, dynmx_temp >,
                       em::protocol_reg< DYNMX_MAX_VOLT, dynmx_voltage >,
                       em::protocol_reg< DYNMX_MIN_VOLT, dynmx_voltage >,
                       em::protocol_reg< DYNMX_PWM_LIMIT, dynmx_pwm_limit >,
                       em::protocol_reg< DYNMX_VEL_LIMIT, dynmx_vel_limit >,
                       em::protocol_reg< DYNMX_MAX_POS, dynmx_pos >,
                       em::protocol_reg< DYNMX_MIN_POS, dynmx_pos >,
                       em::protocol_reg< DYNMX_SHUTDOWN, dynmx_error >,
                       em::protocol_reg< DYNMX_TORQUE_EN, dynmx_bool >,
                       em::protocol_reg< DYNMX_LED_EN, dynmx_bool >,
                       em::protocol_reg< DYNMX_STATUS_RETURN, dynmx_status_level >,
                       em::protocol_reg< DYNMX_REGISTERED_INST, dynmx_bool >,
                       em::protocol_reg< DYNMX_ERROR_STATUS, dynmx_error >,
                       em::protocol_reg< DYNMX_VEL_I_GAIN, dynmx_pid_gain >,
                       em::protocol_reg< DYNMX_VEL_P_GAIN, dynmx_pid_gain >,
                       em::protocol_reg< DYNMX_POS_D_GAIN, dynmx_pid_gain >,
                       em::protocol_reg< DYNMX_POS_I_GAIN, dynmx_pid_gain >,
                       em::protocol_reg< DYNMX_POS_P_GAIN, dynmx_pid_gain >,
                       em::protocol_reg< DYNMX_FF_SCND_GAIN, dynmx_pid_gain >,
                       em::protocol_reg< DYNMX_FF_FRST_GAIN, dynmx_pid_gain >,
                       em::protocol_reg< DYNMX_BUS_WATCHDOG, dynmx_watchdog >,
                       em::protocol_reg< DYNMX_GOAL_PWM, dynmx_pwm >,
                       em::protocol_reg< DYNMX_GOAL_VEL, dynmx_vel >,
                       em::protocol_reg< DYNMX_PROFILE_ACCEL, dynmx_profile_accel >,
                       em::protocol_reg< DYNMX_PROFILE_VEL, dynmx_profile_vel >,
                       em::protocol_reg< DYNMX_GOAL_POS, dynmx_pos >,
                       em::protocol_reg< DYNMX_REALTIME_TICK, dynmx_time >,
                       em::protocol_reg< DYNMX_MOVING, dynmx_bool >,
                       em::protocol_reg< DYNMX_MOVING_STATUS, dynmx_status >,
                       em::protocol_reg< DYNMX_PRESENT_PWM, dynmx_pwm >,
                       em::protocol_reg< DYNMX_PRESENT_LOAD, dynmx_load >,
                       em::protocol_reg< DYNMX_PRESENT_VEL, dynmx_vel >,
                       em::protocol_reg< DYNMX_PRESENT_POS, dynmx_pos >,
                       em::protocol_reg< DYNMX_VEL_TRAJECTORY, dynmx_vel >,
                       em::protocol_reg< DYNMX_PSO_TRAJECTORY, dynmx_pos >,
                       em::protocol_reg< DYNMX_PRESENT_VOLTAGE, dynmx_voltage >,
                       em::protocol_reg< DYNMX_PRESENT_TEMP, dynmx_temp > >
{
};

class dynmx_context
{
        dynmx_map map_;
        s_ctl_id  msg_id_counter_ = 0;

public:
        explicit dynmx_context( dynmx_servo_id id )
        {
                map_.set_val< DYNMX_SERVO_ID >( id );
        }

        std::size_t full_poll( std::size_t i, servo_visitor& vis );

        dynmx_servo_id get_id() const
        {
                return map_.get_val< DYNMX_SERVO_ID >();
        }

        template < dynmx_reg_enum REG >
        void query_item( servo_visitor& vis )
        {
                dynmx_len len{ dynmx_map::reg_type< REG >::size };
                transmit_servo< DYNMX_READ >( REG, get_id(), vis, dynmx_addr{ REG }, len );
        }

        template < dynmx_reg_enum REG >
        dynmx_map::reg_value_type< REG > get_item()
        {
                return map_.get_val< REG >();
        }

        template < dynmx_reg_enum REG >
        void set_item( dynmx_map::reg_value_type< REG > val, servo_visitor& vis )
        {
                auto did = get_id();
                map_.set_val< REG >( val );
                transmit_servo< DYNMX_WRITE >(
                    REG, did, vis, dynmx_addr{ REG }, get_buffered_val( REG ) );
        }

        void on_receive( s_ctl_id sid, const dynmx_buffer& buff );

        void dump_info();

private:
        dynmx_buffer get_buffered_val( dynmx_reg_enum );

        template < dynmx_message_enum ID, typename... Args >
        void
        transmit_servo( dynmx_reg_enum reg, dynmx_servo_id did, servo_visitor& vis, Args... args )
        {
                vis.to_servo(
                    static_cast< s_ctl_id >( reg ),
                    reply::YES,
                    make_master_dynmx_msg< ID >( did, args... ) );
        }
};

}  // namespace schpin
