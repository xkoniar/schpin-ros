// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/dynmx/base.h"
#include "fw/dynmx/crc.h"

#include <emlabcpp/either.h>
#include <emlabcpp/protocol/command_group.h>
#include <emlabcpp/protocol/tuple.h>

#pragma once

namespace schpin
{

enum dynmx_message_enum : uint8_t
{
        DYNMX_PING          = 0x01,
        DYNMX_READ          = 0x02,
        DYNMX_WRITE         = 0x03,
        DYNMX_REG_WRITE     = 0x04,
        DYNMX_ACTION        = 0x05,
        DYNMX_FACTORY_RESET = 0x06,
        DYNMX_REBOOT        = 0x08,
        DYNMX_CLEAR         = 0x10,
        DYNMX_STATUS        = 0x55,
        DYNMX_SYNC_READ     = 0x82,
        DYNMX_SYNC_WRITE    = 0x83,
        DYNMX_BULK_READ     = 0x92,
        DYNMX_BULK_WRITE    = 0x93
};

struct master_dynmx_group
  : em::protocol_command_group<
        em::PROTOCOL_LITTLE_ENDIAN,
        em::protocol_command< DYNMX_PING >,
        em::protocol_command< DYNMX_READ >::with_args< dynmx_addr, dynmx_len >,
        em::protocol_command< DYNMX_WRITE >::with_args< dynmx_addr, dynmx_buffer >,
        em::protocol_command< DYNMX_REG_WRITE >::with_args< dynmx_addr, dynmx_buffer >,
        em::protocol_command< DYNMX_ACTION >,
        em::protocol_command< DYNMX_FACTORY_RESET >::with_args< dynmx_reset_flags >,
        em::protocol_command< DYNMX_REBOOT >
        // CLEAR
        // DYNMX_SYNC_READ
        // SYNC_WRITE
        // BULK_READ
        // BULK_WRITE
        >
{
};

using master_dynmx_submessage = master_dynmx_group::message_type;
using master_dynmx_variant    = master_dynmx_group::value_type;

using dynmx_status_const = em::bounded< uint8_t, DYNMX_STATUS, DYNMX_STATUS >;
static constexpr dynmx_status_const dynmx_status_const_v =
    dynmx_status_const::get< DYNMX_STATUS >();

using dynmx_master_subprotocol = em::protocol_tuple<
    em::PROTOCOL_LITTLE_ENDIAN,
    dynmx_status_const,
    dynmx_response_error,
    dynmx_buffer >;
using dynmx_master_submessage = dynmx_master_subprotocol::message_type;
using dynmx_master_subvalue   = dynmx_master_subprotocol::value_type;

template < typename T >
using dynmx_protocol = em::protocol_tuple<
    em::PROTOCOL_LITTLE_ENDIAN,
    dynmx_header,
    dynmx_servo_id,
    em::protocol_sized_buffer< em::protocol_offset< dynmx_packet_size, 2 >, T >,
    dynmx_crc >;

using dynmx_master_protocol = dynmx_protocol< dynmx_master_subprotocol >;
using dynmx_master_message  = dynmx_master_protocol::message_type;
using dynmx_master_value    = dynmx_master_protocol::value_type;

using master_dynmx_protocol = dynmx_protocol< master_dynmx_group >;
using master_dynmx_message  = master_dynmx_protocol::message_type;
using master_dynmx_value    = master_dynmx_protocol::value_type;

master_dynmx_message master_dynmx_serialize( const master_dynmx_value& );
em::either< master_dynmx_value, em::protocol_error_record >
                     master_dynmx_extract( const master_dynmx_message& );
dynmx_master_message dynmx_master_serialize( const dynmx_master_value& );
em::either< dynmx_master_value, em::protocol_error_record >
dynmx_master_extract( const dynmx_master_message& );

template < dynmx_message_enum ID, typename... Args >
inline master_dynmx_message make_master_dynmx_msg( dynmx_servo_id id, Args... args )
{
        master_dynmx_value val = master_dynmx_protocol::make_val(
            dynmx_header_v, id, master_dynmx_group::make_val< ID >( args... ), dynmx_crc{} );

        master_dynmx_message msg = master_dynmx_serialize( val );

        uint16_t crc =
            dynmx_update_crc( 0, msg.begin(), static_cast< uint16_t >( msg.size() - 2 ) );

        msg[msg.size() - 2] = static_cast< uint8_t >( crc & 0xff );
        msg[msg.size() - 1] = static_cast< uint8_t >( crc >> 8 );

        return msg;
}
}  // namespace schpin
