// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/base.h"
#include "fw/dynmx/base.h"

#include <emlabcpp/protocol/register_map.h>
#include <numbers>

#pragma once

namespace schpin
{

using dynmx_config_map_def = em::protocol_register_map<
    em::PROTOCOL_BIG_ENDIAN,
    em::protocol_reg< DYNMX_INPUT_RANGE_LOW, bangle >,
    em::protocol_reg< DYNMX_INPUT_RANGE_HIGH, bangle >,
    em::protocol_reg< DYNMX_SERVO_RANGE_LOW, dynmx_angle >,
    em::protocol_reg< DYNMX_SERVO_RANGE_HIGH, dynmx_angle >,
    em::protocol_reg< DYNMX_POS_P_GAIN_C, dynmx_pid_gain >,
    em::protocol_reg< DYNMX_POS_I_GAIN_C, dynmx_pid_gain >,
    em::protocol_reg< DYNMX_POS_D_GAIN_C, dynmx_pid_gain >,
    em::protocol_reg< DYNMX_VEL_P_GAIN_C, dynmx_pid_gain >,
    em::protocol_reg< DYNMX_VEL_I_GAIN_C, dynmx_pid_gain > >;

struct dynmx_config_map : dynmx_config_map_def
{
        using dynmx_config_map_def::dynmx_config_map_def;
        dynmx_config_map()
          : dynmx_config_map_def(
                bangle::from_float( 0.f ),
                bangle::from_float( 2 * std::numbers::pi_v< float > ),
                dynmx_angle::get< 0 >(),
                dynmx_angle::get< 4095 >(),
                dynmx_pid_gain::get< 640 >(),
                dynmx_pid_gain::get< 500 >(),
                dynmx_pid_gain::get< 2000 >(),
                dynmx_pid_gain::get< 100 >(),
                dynmx_pid_gain::get< 1800 >() )
        {
        }
};

}  // namespace schpin
