
#include <emlabcpp/protocol/sequencer.h>

#pragma once

namespace em = emlabcpp;

namespace schpin
{

template < typename Message >
struct dynmx_sequencer_def
{
        using message_type = Message;

        static constexpr std::array< uint8_t, 4 > prefix      = { 0xFF, 0xFF, 0xFD, 0x00 };
        static constexpr std::size_t              fixed_size  = 7;
        static constexpr std::size_t              buffer_size = Message::max_size * 2;

        static constexpr std::size_t get_size( const auto& bview )
        {
                auto val = static_cast< std::size_t >( ( bview[6] << 8 ) + bview[5] );
                return val + fixed_size;
        };
};

template < typename Message >
using dynmx_sequencer = em::protocol_sequencer< dynmx_sequencer_def< Message > >;

}  // namespace schpin
