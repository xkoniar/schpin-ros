// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/util.h"

#include <numbers>

#pragma once

namespace schpin
{

class dynmx_converter
{
        float in_from = 0.f;
        float in_to   = 2 * std::numbers::pi_v< float >;
        float se_from = 0.f;
        float se_to   = 4095.f;

public:
        dynmx_converter() = default;

        bangular_velocity se_to_in( dynmx_angular_velocity vel ) const
        {
                float mapped = map_val< float >(
                    static_cast< float >( *vel ), se_from, se_to, in_from, in_to );
                return bangular_velocity::from_float( mapped );
        }

        bvoltage se_to_in( dynmx_voltage volt ) const
        {
                return bvoltage::from_float( *volt / 10.f );
        }

        btemp se_to_in( dynmx_temp temp ) const
        {
                return btemp::from_float( *temp );
        }

        bangle se_to_in( dynmx_angle an ) const
        {
                float mapped =
                    map_val< float >( static_cast< float >( *an ), se_from, se_to, in_from, in_to );
                return bangle::from_float( mapped );
        }

        std::optional< dynmx_angular_velocity > in_to_se( bangular_velocity val ) const
        {
                float mapped = map_val< float >( val.as_float(), in_from, in_to, se_from, se_to );
                return dynmx_angular_velocity::make( mapped );
        }

        std::optional< dynmx_angle > in_to_se( bangle val ) const
        {
                float mapped = map_val< float >( val.as_float(), in_from, in_to, se_from, se_to );
                return dynmx_angle::make( mapped );
        }

        dynmx_converter( bangle i_from, bangle i_to, dynmx_angle s_from, dynmx_angle s_to )
          : in_from( i_from.as_float() )
          , in_to( i_to.as_float() )
          , se_from( static_cast< float >( *s_from ) )
          , se_to( static_cast< float >( *s_to ) )
        {
        }

        dynmx_converter( const dynmx_config_map& cmap )
          : dynmx_converter(
                cmap.get_val< DYNMX_INPUT_RANGE_LOW >(),
                cmap.get_val< DYNMX_INPUT_RANGE_HIGH >(),
                cmap.get_val< DYNMX_SERVO_RANGE_LOW >(),
                cmap.get_val< DYNMX_SERVO_RANGE_HIGH >() )
        {
        }
};

}  // namespace schpin
