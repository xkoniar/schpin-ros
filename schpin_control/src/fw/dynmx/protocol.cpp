
#include <emlabcpp/protocol/handler.h>
#include <fw/dynmx/protocol.h>

namespace schpin
{

master_dynmx_message master_dynmx_serialize( const master_dynmx_value& val )
{
        return em::protocol_handler< master_dynmx_protocol >::serialize( val );
}
em::either< master_dynmx_value, em::protocol_error_record >
master_dynmx_extract( const master_dynmx_message& msg )
{
        return em::protocol_handler< master_dynmx_protocol >::extract( msg );
}
dynmx_master_message dynmx_master_serialize( const dynmx_master_value& val )
{
        return em::protocol_handler< dynmx_master_protocol >::serialize( val );
}
em::either< dynmx_master_value, em::protocol_error_record >
dynmx_master_extract( const dynmx_master_message& msg )
{
        return em::protocol_handler< dynmx_master_protocol >::extract( msg );
}

}  // namespace schpin
