#include "fw/dynmx/servo.h"

#include "fw/servo/util.h"

#include <emlabcpp/iterators/convert.h>
#include <emlabcpp/match.h>
#include <emlabcpp/protocol/register_handler.h>
#include <schpin_lib/logging.h>

namespace schpin
{
using config_handler = em::protocol_register_handler< dynmx_config_map >;

servo_ptr make_dynmx_servo( dynmx_servo_id id, clk_time, std::pmr::memory_resource* mem )
{
        static_assert( servo_bucket_size >= sizeof( dynmx_servo ) );
        SCHPIN_ASSERT( mem != nullptr );
        void* target = mem->allocate( sizeof( dynmx_servo ), alignof( dynmx_servo ) );
        return { new ( target ) dynmx_servo{ id }, servo_deleter{ mem } };
}

void dynmx_servo::stop( servo_visitor& vis )
{
        // TODO: DO NOT DO THIS IF ACTIVATING?
        task_ = idle_task{};
        context_.set_item< DYNMX_TORQUE_EN >( dynmx_false, vis );
}

std::optional< servo_master_variant >
dynmx_servo::on_servo_receive( clk_time, s_ctl_id sid, const servo_receive_variant& var )
{
        catched_msg_ = true;

        const auto& tpl = std::get< dynmx_master_subvalue >( var );

        const dynmx_response_error& err = std::get< 1 >( tpl );
        if ( err != 0 ) {
                SCHPIN_ERROR_LOG( "runner", "Dynmx return error: " << std::bitset< 8 >{ err } );
                // TODO: error handling
                return {};
        }

        // TODO: error handling

        const auto& buff = std::get< dynmx_buffer >( tpl );
        if ( buff.size() == 0 ) {
                return {};
        }
        context_.on_receive( sid, buff );

        dynmx_angle pos = context_.get_item< DYNMX_PRESENT_POS >();

        // TODO: maybe we should write tests for reporting?
        // TODO: ugly ocnstant...
        if ( angle_dist( last_report_, pos ) < dynmx_angle::get< 12 >() ) {
                return {};
        }
        last_report_ = pos;
        return servo_master_group::make_val< POSITION >( converter_.se_to_in( pos ) );

        return {};
}

void dynmx_servo::tick( clk_time now, servo_visitor& vis )
{
        process_config( vis );
        em::match(
            task_,
            [&]( idle_task ) {},
            [&]( activate_task at ) {
                    at.i = context_.full_poll( at.i, vis );
                    if ( at.i == 0 && catched_msg_ ) {
                            vis.on_activate( *context_.get_id() );
                            config_i_ = 0;
                            task_     = idle_task{};
                    } else {
                            task_ = at;
                    }
            },
            [&]( goto_task gt ) {
                    // TODO: limit to new pos readings?
                    // TODO: implement after pos mechanics
                    clk_time t_time = std::clamp( now, gt.from_time, gt.to_time );
                    float    t      = ( t_time - gt.from_time ).as_float() /
                              ( gt.to_time - gt.from_time ).as_float();
                    float b      = bezier< 3 >( t, { 0.0f, 1.0f, 1.0f } );
                    float mapped = static_cast< float >( *gt.from_pos ) +
                                   ( static_cast< float >( *gt.to_pos ) -
                                     static_cast< float >( *gt.from_pos ) ) *
                                       b;

                    /*
                    SCHPIN_INFO_LOG(
                        "mapped",
                        "Mapped: " << mapped << " | " << gt.from_pos << "," << gt.to_pos <<
                    " | "
                                   << t_time << "," << gt.from_time << "," << gt.to_time <<
                    " | "
                                   << t << "," << b ); */
                    SCHPIN_INFO_LOG( "servo", "Goto: " << mapped );
                    context_.set_item< DYNMX_GOAL_POS >( *dynmx_angle::make( mapped ), vis );

                    if ( now < gt.to_time ) {
                            return;
                    }

                    if ( abs( static_cast< int >( *context_.get_item< DYNMX_PRESENT_POS >() ) -
                              static_cast< int >( *gt.to_pos ) ) > 15 ) {
                            return;
                    }

                    task_ = idle_task{};
            },
            [&]( spin_task ) {} );
}

void dynmx_servo::on_event(
    ctl_id                      cid,
    clk_time                    now,
    const servo_events_variant& svar,
    servo_visitor&              vis )
{
        // TODO: error handling
        const auto& var = std::get< dynmx_servo_events_variant >( svar );
        em::match(
            var,
            [&]( dynmx_goto_event e ) {
                    context_.set_item< DYNMX_TORQUE_EN >( dynmx_false, vis );
                    context_.set_item< DYNMX_OPER_MODE >( dynmx_position_control_mode, vis );
                    context_.set_item< DYNMX_TORQUE_EN >( dynmx_true, vis );

                    // context_.set_item< DYNMX_POS_I_GAIN >( dynmx_pid_gain::get< 500 >(),
                    // vis );

                    task_ = goto_task{
                        .from_pos  = context_.get_item< DYNMX_PRESENT_POS >(),
                        .to_pos    = e.goal_pos,
                        .from_time = now,
                        .to_time   = now + clk_time{ *e.t - 10 },
                        .end_cond  = e.end };
            },
            [&]( dynmx_spin_event ) {},
            [&]( servo_config_set_event e ) {
                    auto key       = static_cast< dynmx_config_enum >( e.addr );
                    auto opt_error = config_handler::insert( config_map_, key, e.buffer );
                    if ( opt_error ) {
                            vis.to_control_error< CONFIG_E >( cid, *context_.get_id() );
                    }
                    SCHPIN_INFO_LOG(
                        "servo",
                        "Setting key: " << int( key ) << " of servo " << context_.get_id()
                                        << " to value " << em::convert_view< int >( e.buffer ) );

                    // TODO: add tests that configuration change is handled properly by the code
                    // -> test that changes anything and watches for _propr_ changes
                    converter_ = dynmx_converter{ config_map_ };
            },
            [&]( servo_id_event e ) {
                    auto opt_did = dynmx_servo_id::make( e.sid );
                    if ( !opt_did ) {
                            vis.to_control_error< SERVO_ID_INVALID_E >( cid, *context_.get_id() );
                    }
                    context_.set_item< DYNMX_SERVO_ID >( *opt_did, vis );
            } );
}

void dynmx_servo::poll( clk_time, std::size_t i_offset, servo_visitor& vis )
{
        poll_i_       = ( poll_i_ + 1 ) % 13;
        std::size_t i = ( i_offset + poll_i_ ) % 13;
        switch ( i ) {
                case 4:
                        context_.query_item< DYNMX_PRESENT_VOLTAGE >( vis );
                        break;
                case 8:
                        context_.query_item< DYNMX_PRESENT_TEMP >( vis );
                        break;
                default:
                        context_.query_item< DYNMX_PRESENT_POS >( vis );
                        break;
        }
}
em::either< servo_events_variant, servo_master_variant >
dynmx_servo::on_servo_packet( ctl_id cid, const master_servo_variant& var )
{
        return em::apply_on_match(
            var,
            servo_goto_handler< dynmx_converter, dynmx_servo_events_variant, dynmx_goto_event >{
                cid, converter_ },
            servo_spin_handler< dynmx_converter, dynmx_servo_events_variant, dynmx_spin_event >{
                cid, converter_ },
            make_servo_query_handler< POSITION >(
                converter_, context_.get_item< DYNMX_PRESENT_POS >() ),
            make_servo_query_handler< TEMP >(
                converter_, context_.get_item< DYNMX_PRESENT_TEMP >() ),
            make_servo_query_handler< VOLTAGE >(
                converter_, context_.get_item< DYNMX_PRESENT_VOLTAGE >() ),
            make_servo_query_handler< VELOCITY >(
                converter_, context_.get_item< DYNMX_PRESENT_VEL >() ),
            [&]( em::tag< ID >, servo_id sid )
                -> em::either< servo_events_variant, servo_master_variant > {  // TODO:
                                                                               // duplicated
                    // TODO "set id event per servo!"
                    return servo_events_variant{
                        dynmx_servo_events_variant{ servo_id_event{ sid } } };
            },
            servo_task_query_handler{ task_ },
            config_msg_handler< config_handler, dynmx_servo_events_variant >{ config_map_ } );
}

void dynmx_servo::dump_info()
{
        context_.dump_info();
}

void dynmx_servo::process_config( servo_visitor& vis )
{
        if ( !config_i_ ) {
                return;
        }

        switch ( *config_i_ ) {
                case 0:
                        context_.set_item< DYNMX_POS_P_GAIN >(
                            config_map_.get_val< DYNMX_POS_P_GAIN_C >(), vis );
                        break;
                case 1:
                        context_.set_item< DYNMX_POS_I_GAIN >(
                            config_map_.get_val< DYNMX_POS_I_GAIN_C >(), vis );
                        break;
                case 2:
                        context_.set_item< DYNMX_POS_D_GAIN >(
                            config_map_.get_val< DYNMX_POS_D_GAIN_C >(), vis );
                        break;
                case 3:
                        context_.set_item< DYNMX_VEL_P_GAIN >(
                            config_map_.get_val< DYNMX_VEL_P_GAIN_C >(), vis );
                        break;
                case 4:
                        context_.set_item< DYNMX_VEL_I_GAIN >(
                            config_map_.get_val< DYNMX_VEL_I_GAIN_C >(), vis );
                        break;
                default:
                        config_i_.reset();
                        return;
        }
        *config_i_ += 1;
}

}  // namespace schpin
