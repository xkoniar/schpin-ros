#include "fw/dynmx/context.h"

#include <emlabcpp/protocol/register_handler.h>
#ifdef SCHPIN_USE_STREAMS
#include <iostream>
#endif

template < emlabcpp::protocol_endianess_enum Endianess >
struct emlabcpp::protocol_def< schpin::dynmx_error, Endianess >
{
        using value_type = typename emlabcpp::protocol_decl< schpin::dynmx_error >::value_type;
        static constexpr std::size_t max_size =
            emlabcpp::protocol_decl< schpin::dynmx_error >::max_size;
        using size_type = emlabcpp::bounded< std::size_t, max_size, max_size >;

        using sub_def = emlabcpp::protocol_def< std::bitset< 8 >, Endianess >;

        static constexpr size_type
        serialize_at( std::span< uint8_t, max_size > buffer, const value_type& e )
        {
                std::bitset< 8 > bset;
                bset[5] = e.overload;
                bset[4] = e.electrical_shock;
                bset[3] = e.motor_encoder;
                bset[2] = e.overheating;
                bset[0] = e.input_voltage;
                return sub_def::serialize_at( buffer, bset );
        }
        static constexpr auto deserialize( const bounded_view< const uint8_t*, size_type >& buffer )
            -> protocol_result< value_type >
        {
                auto [used, res] = sub_def::deserialize( buffer );
                if ( std::holds_alternative< const protocol_mark* >( res ) ) {
                        return { 0, std::get< const protocol_mark* >( res ) };
                }
                auto                bset = std::get< 0 >( res );
                schpin::dynmx_error de{};
                de.overload         = bset[5];
                de.electrical_shock = bset[4];
                de.motor_encoder    = bset[3];
                de.overheating      = bset[2];
                de.input_voltage    = bset[0];
                return { used, de };
        }
};

template < emlabcpp::protocol_endianess_enum Endianess >
struct emlabcpp::protocol_def< schpin::dynmx_status, Endianess >
{
        using value_type = typename emlabcpp::protocol_decl< schpin::dynmx_status >::value_type;
        static constexpr std::size_t max_size =
            emlabcpp::protocol_decl< schpin::dynmx_status >::max_size;

        using size_type = emlabcpp::bounded< std::size_t, max_size, max_size >;
        using sub_def   = emlabcpp::protocol_def< std::bitset< 8 >, Endianess >;

        static constexpr size_type
        serialize_at( std::span< uint8_t, max_size > buffer, const value_type& st )
        {
                std::bitset< 8 > bset;
                bset[5] = st.profile;
                bset[4] = st.profile >> 1;
                bset[3] = st.following;
                bset[1] = st.ongoing;
                bset[0] = st.arrived;
                return sub_def::serialize_at( buffer, bset );
        }

        static constexpr auto deserialize( const bounded_view< const uint8_t*, size_type >& buffer )
            -> protocol_result< value_type >
        {
                auto [used, res] = sub_def::deserialize( buffer );
                if ( std::holds_alternative< const protocol_mark* >( res ) ) {
                        return { 0, std::get< const protocol_mark* >( res ) };
                }
                auto                 bset = std::get< 0 >( res );
                schpin::dynmx_status st;
                st.profile =
                    static_cast< schpin::dynmx_status::profile_enum >( bset[4] << 1 & bset[5] );
                st.following = bset[3];
                st.ongoing   = bset[1];
                st.arrived   = bset[0];
                return { used, st };
        }
};

namespace schpin
{

using dynmx_handler = em::protocol_register_handler< dynmx_map >;

std::size_t dynmx_context::full_poll( std::size_t i, servo_visitor& vis )
{
        i                  = i % dynmx_map::registers_count;
        auto           mi  = *dynmx_map::register_index::make( i );
        dynmx_reg_enum key = dynmx_map::register_key( mi );
        dynmx_len      len{ dynmx_map::register_size( mi ) };
        transmit_servo< DYNMX_READ >( key, get_id(), vis, dynmx_addr{ key }, len );
        return ( i + 1 ) % dynmx_map::registers_count;
}

void dynmx_context::on_receive( s_ctl_id sid, const dynmx_buffer& buff )
{
        auto reg = static_cast< dynmx_reg_enum >( sid );

        auto opt_err = dynmx_handler::insert( map_, reg, buff );
        // TODO: add error handling
        em::ignore( opt_err );
}

void dynmx_context::dump_info()
{
#ifdef SCHPIN_USE_STREAMS
        std::cout << map_;
#endif
}

dynmx_buffer dynmx_context::get_buffered_val( dynmx_reg_enum reg )
{
        return dynmx_buffer{ dynmx_handler::select( map_, reg ) };
}

}  // namespace schpin
