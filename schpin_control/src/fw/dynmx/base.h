// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <emlabcpp/protocol/decl.h>
#include <emlabcpp/protocol/message.h>
#ifdef SCHPIN_USE_STREAMS
#include <magic_enum.hpp>
#endif

namespace em = emlabcpp;

#pragma once

namespace schpin
{

enum dynmx_config_enum : uint8_t
{
        DYNMX_INPUT_RANGE_LOW  = 0,
        DYNMX_INPUT_RANGE_HIGH = 1,
        DYNMX_SERVO_RANGE_LOW  = 2,
        DYNMX_SERVO_RANGE_HIGH = 3,
        DYNMX_POS_P_GAIN_C     = 4,
        DYNMX_POS_I_GAIN_C     = 5,
        DYNMX_POS_D_GAIN_C     = 6,
        DYNMX_VEL_P_GAIN_C     = 7,
        DYNMX_VEL_I_GAIN_C     = 8
};

struct dynmx_error
{
        // TODO make this enum
        bool overload;
        bool electrical_shock;
        bool motor_encoder;
        bool overheating;
        bool input_voltage;
};

struct dynmx_status
{
        enum profile_enum
        {
                TRAPEZOIADL_PROFILE = 0b11,
                TRIANGULAR_PROFILE  = 0b10,
                RECTANGULAR_PROFILE = 0b01,
                NO_PROFILE          = 0b00
        };

        profile_enum profile;
        bool         following;
        bool         ongoing;
        bool         arrived;
};

using dynmx_header                           = em::bounded< uint32_t, 0x00FDFFFF, 0x00FDFFFF >;
static constexpr dynmx_header dynmx_header_v = dynmx_header::get< 0x00FDFFFF >();
using dynmx_servo_id                         = em::bounded< uint8_t, 0, 254 >;
using dynmx_packet_size                      = uint16_t;
using dynmx_crc            = em::tagged_quantity< struct dynmx_crc_tag, uint16_t >;
using dynmx_response_error = uint8_t;  // maybe enum here
using dynmx_model_number   = em::tagged_quantity< struct dynmx_model_number_tag, uint16_t >;
using dynmx_model_info     = em::tagged_quantity< struct dynmx_model_info_tag, uint16_t >;
using dynmx_fw_version     = em::tagged_quantity< struct dynmx_fw_version_tag, uint8_t >;
using dynmx_baud_rate      = em::bounded< uint8_t, 0, 7 >;
using dynmx_delay_time     = em::bounded< uint8_t, 0, 254 >;
using dynmx_drive_mode     = em::bounded< uint8_t, 0, 5 >;
using dynmx_oper_mode      = em::bounded< uint8_t, 0, 16 >;
static constexpr auto dynmx_velocity_control_mode          = dynmx_oper_mode::get< 1 >();
static constexpr auto dynmx_position_control_mode          = dynmx_oper_mode::get< 3 >();
static constexpr auto dynmx_extended_position_control_mode = dynmx_oper_mode::get< 4 >();
static constexpr auto dynmx_pwm_control_mode               = dynmx_oper_mode::get< 16 >();
using dynmx_proto_type                                     = em::bounded< uint8_t, 1, 2 >;
using dynmx_home_offset           = em::bounded< int32_t, -1'044'479, 1'044'479 >;
using dynmx_vel_limit             = em::bounded< uint32_t, 0, 1'023 >;
using dynmx_voltage               = em::bounded< uint16_t, 60, 140 >;
using dynmx_temp                  = em::bounded< uint8_t, 0, 100 >;
using dynmx_pwm_limit             = em::bounded< uint16_t, 0, 885 >;
using dynmx_pos                   = em::bounded< uint32_t, 0, 4'095 >;
using dynmx_angle                 = dynmx_pos;
using dynmx_bool                  = em::bounded< uint8_t, 0, 1 >;
static constexpr auto dynmx_true  = dynmx_bool::get< 1 >();
static constexpr auto dynmx_false = dynmx_bool::get< 0 >();
using dynmx_status_level          = em::bounded< uint8_t, 0, 2 >;
using dynmx_pid_gain              = em::bounded< uint16_t, 0, 16'383 >;
using dynmx_watchdog              = em::bounded< int16_t, -1, 127 >;
using dynmx_pwm                   = em::bounded< int16_t, -885, 885 >;
using dynmx_vel                   = em::bounded< int32_t, -1'023, 1'023 >;
using dynmx_angular_velocity      = dynmx_vel;
using dynmx_profile_accel         = em::bounded< uint32_t, 0, 32'737 >;
using dynmx_profile_vel           = em::bounded< uint32_t, 0, 32'767 >;
using dynmx_time                  = em::bounded< uint16_t, 0, 32'767 >;
using dynmx_load                  = em::bounded< int16_t, -1'000, 1'000 >;
using dynmx_addr                  = em::tagged_quantity< struct dynmx_addr_tag, uint16_t >;
using dynmx_len                   = em::tagged_quantity< struct dynmx_len_tag, uint16_t >;
using dynmx_buffer                = em::protocol_sizeless_message< 16 >;
using dynmx_reset_flags =
    em::bounded< uint8_t, 0x01, 0x02 >;  // missing 0xFF option, maybe new type?

#ifdef SCHPIN_USE_STREAMS
inline std::ostream& operator<<( std::ostream& os, dynmx_error err )
{
        std::string s;
        if ( err.overload ) {
                os << "OVERLOAD";
                s = ",";
        }
        if ( err.electrical_shock ) {
                os << s << "ESHOCK";
                s = ",";
        }
        if ( err.motor_encoder ) {
                os << s << "M_ENCOD";
                s = ",";
        }
        if ( err.overheating ) {
                os << s << "HEAT";
                s = ",";
        }
        if ( err.input_voltage ) {
                os << s << "IVOT";
        }
        return os;
}
inline std::ostream& operator<<( std::ostream& os, dynmx_status st )
{
        os << magic_enum::enum_name( st.profile );
        if ( st.following ) {
                os << ",FOLLOWING";
        }
        if ( st.ongoing ) {
                os << ",ONGOING";
        }
        if ( st.arrived ) {
                os << ",ARRIVED";
        }
        return os;
}
#endif

}  // namespace schpin

template <>
struct emlabcpp::protocol_decl< schpin::dynmx_error >
{
        using value_type                      = schpin::dynmx_error;
        static constexpr std::size_t max_size = 1;
        static constexpr std::size_t min_size = max_size;
};

template <>
struct emlabcpp::protocol_decl< schpin::dynmx_status >
{
        using value_type                      = schpin::dynmx_status;
        static constexpr std::size_t max_size = 1;
        static constexpr std::size_t min_size = max_size;
};
