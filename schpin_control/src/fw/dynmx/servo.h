// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/dynmx/config.h"
#include "fw/dynmx/context.h"
#include "fw/dynmx/conv.h"
#include "fw/servo/base.h"
#include "fw/servo/interface.h"

#pragma once

namespace schpin
{

class dynmx_servo : public servo_interface
{
        struct idle_task
        {
                static constexpr servo_task_enum task_mark = TASK_IDLE;
        };

        struct activate_task
        {
                static constexpr servo_task_enum task_mark = TASK_ACTIVATE;
                std::size_t                      i;
        };

        struct goto_task
        {
                static constexpr servo_task_enum task_mark = TASK_GOTO;

                dynmx_angle from_pos;
                dynmx_angle to_pos;
                clk_time    from_time;
                clk_time    to_time;

                end_condition end_cond;
        };

        struct spin_task
        {
                static constexpr servo_task_enum task_mark = TASK_SPIN;
        };

        using task_variant = std::variant< idle_task, activate_task, goto_task, spin_task >;

        dynmx_config_map config_map_;
        dynmx_context    context_;
        dynmx_converter  converter_;
        task_variant     task_ = { activate_task{} };

        bool catched_msg_ = false;

        std::size_t                  poll_i_;
        std::optional< std::size_t > config_i_;
        dynmx_angle                  last_report_;

public:
        using receive_type   = dynmx_buffer;
        using events_variant = dynmx_servo_events_variant;

        explicit dynmx_servo( dynmx_servo_id id )
          : context_( id )
        {
        }

        servo_id get_id() const
        {
                return *context_.get_id();
        }

        bool has_task() const
        {
                return !std::holds_alternative< idle_task >( task_ );
        }

        bool is_activating() const
        {
                return std::holds_alternative< activate_task >( task_ );
        }

        void stop( servo_visitor& );

        std::optional< servo_master_variant >
        on_servo_receive( clk_time, s_ctl_id, const servo_receive_variant& );

        void tick( clk_time, servo_visitor& );

        void on_event( ctl_id, clk_time, const servo_events_variant&, servo_visitor& );

        void poll( clk_time, std::size_t i_offset, servo_visitor& vis );

        em::either< servo_events_variant, servo_master_variant >
        on_servo_packet( ctl_id cid, const master_servo_variant& var );

        void dump_info();

private:
        void process_config( servo_visitor& vis );
};
}  // namespace schpin
