#include "fw/dynmx/protocol.h"
#include "shared/protocol.h"

#pragma once

namespace schpin
{

class fw_reactor_visitor
{
        ctl_id cid_;

public:
        template < error_protocol_enum ID, typename... Args >
        void make_error_to_control( ctl_id cid, Args... args )
        {
                make_to_control< ERROR >( cid, error_group::make_val< ID >( args... ) );
        }

        template < messages_enum ID, typename... Args >
        void make_to_control( Args... args )
        {
                to_control( fw_node_group::make_val< ID >( args... ) );
        }

        void to_control( const fw_node_group_value& val )
        {
                to_control(
                    fw_node_serialize( fw_node_protocol::make_val( cid_, val, checksum{} ) ) );
        }

        virtual void to_control( const fw_node_message& )                     = 0;
        virtual void to_servo( s_ctl_id, reply, const master_dynmx_message& ) = 0;
        virtual void to_servo( s_ctl_id, reply, const master_lewan_message& ) = 0;
        virtual ~fw_reactor_visitor()                                         = default;
};

}  // namespace schpin
