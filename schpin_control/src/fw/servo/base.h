// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "shared/protocol.h"

#pragma once

namespace schpin
{

// TODO: rename this
template < typename T, typename A >
inline auto handle_in_to_se( const T& conv, ctl_id cid, messages_enum ID, A a_val )
    -> em::either< std::decay_t< decltype( *conv.in_to_se( a_val ) ) >, servo_master_variant >
{
        auto opt_a = conv.in_to_se( a_val );
        if ( !opt_a ) {
                return make_servo_master_error< INPUT_INVALID_VALUE_E >( cid, ID, 0u );
        }

        return *opt_a;
}

// TODO: rename this
template < typename T, typename A, typename B >
inline auto handle_in_to_se( const T& conv, ctl_id cid, messages_enum ID, A a_val, B b_val )
    -> em::either<
        std::tuple<
            std::decay_t< decltype( *conv.in_to_se( a_val ) ) >,
            std::decay_t< decltype( *conv.in_to_se( b_val ) ) > >,
        servo_master_variant >
{

        auto opt_a = conv.in_to_se( a_val );
        if ( !opt_a ) {
                return make_servo_master_error< INPUT_INVALID_VALUE_E >( cid, ID, 0u );
        }

        auto opt_b = conv.in_to_se( b_val );
        if ( !opt_b ) {
                return make_servo_master_error< INPUT_INVALID_VALUE_E >( cid, ID, 1u );
        }

        return std::make_tuple( *opt_a, *opt_b );
}

}  // namespace schpin
