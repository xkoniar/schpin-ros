#include "fw/base.h"
#include "fw/servo/interface.h"

#include <emlabcpp/visit.h>

#pragma once

namespace schpin
{

template < typename ConfigHandler, typename ServoEventsVariant >
struct config_msg_handler
{
        const typename ConfigHandler::map_type& config_map_;

        em::either< servo_events_variant, servo_master_variant >
        operator()( em::tag< CONFIG_GET >, config_addr addr ) const
        {
                static_assert( ConfigHandler::max_size < std::tuple_size_v< config_buffer > );
                auto msg = ConfigHandler::select(
                    config_map_, static_cast< typename ConfigHandler::key_type >( addr ) );
                config_buffer buff{};
                std::copy( msg.begin(), msg.end(), buff.begin() );
                return servo_master_group::make_val< CONFIG_GET >( addr, buff );
        }

        em::either< servo_events_variant, servo_master_variant >
        operator()( em::tag< CONFIG_SET >, config_addr addr, config_buffer buffr ) const
        {
                return servo_events_variant{
                    ServoEventsVariant{ servo_config_set_event{ addr, buffr } } };
        }
};

template < typename TaskVar >
struct servo_task_query_handler
{
        const TaskVar& task_;

        em::either< servo_events_variant, servo_master_variant > operator()( em::tag< TASK > ) const
        {
                return servo_master_group::make_val< TASK >( em::visit(
                    []< typename T >( T ) {
                            return T::task_mark;
                    },
                    task_ ) );
        }
};

template < messages_enum ID, typename Converter, typename T >
struct servo_query_handler
{
        const Converter& converter_;
        const T&         val_;

        em::either< servo_events_variant, servo_master_variant > operator()( em::tag< ID > ) const
        {
                return servo_master_group::make_val< ID >( converter_.se_to_in( val_ ) );
        }
};

template < messages_enum ID, typename Converter, typename T >
inline servo_query_handler< ID, Converter, T >
make_servo_query_handler( const Converter& conv, const T& val )
{
        return { conv, val };
}

template < typename Conventer, typename EventsVariant, typename Event >
struct servo_goto_handler
{
        ctl_id           cid_;
        const Conventer& converter_;

        em::either< servo_events_variant, servo_master_variant >
        operator()( em::tag< GOTO >, bangle goal, btime t, end_condition end ) const
        {
                return handle_in_to_se( converter_, cid_, GOTO, goal )
                    .convert_left( [&]( auto a ) -> EventsVariant {
                            return Event{ a, t, end };
                    } )
                    .template construct_left< servo_events_variant >();
        }
};
template < typename Conventer, typename EventsVariant, typename Event >
struct servo_spin_handler
{
        ctl_id           cid_;
        const Conventer& converter_;

        em::either< servo_events_variant, servo_master_variant >
        operator()( em::tag< SPIN >, bangular_velocity av, btime t ) const
        {
                return handle_in_to_se( converter_, cid_, SPIN, av )
                    .convert_left( [&]( auto vel ) -> EventsVariant {
                            return Event{ vel, t };
                    } )
                    .template construct_left< servo_events_variant >();
        }
};

}  // namespace schpin
