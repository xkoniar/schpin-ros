#include "fw/base.h"

#pragma once

namespace schpin
{
struct servo_config_set_event
{
        config_addr   addr;
        config_buffer buffer;
};
struct servo_id_event
{
        servo_id sid;
};
}  // namespace schpin
