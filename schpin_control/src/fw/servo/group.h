// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/static_vector.h"
#include "fw/servo/interface.h"

#include <schpin_lib/bucket_memory_resource.h>
#include <schpin_lib/logging.h>

#pragma once

namespace em = emlabcpp;

namespace schpin
{

template < std::size_t N >
class servo_group
{
        using mem       = bucket_memory_resource< N, servo_bucket_size >;
        using container = std::array< servo_ptr, N >;

        mem                         memory_;
        container                   servos_;
        std::size_t                 poll_i_        = 0;
        std::optional< servo_type > type_override_ = {};

        struct sgroup_visitor : servo_visitor
        {
                fw_reactor_visitor& m_vis;

                sgroup_visitor( fw_reactor_visitor& master_vis )
                  : m_vis( master_vis )
                {
                }

                void to_control( servo_id sid, const servo_master_variant& m )
                {
                        m_vis.to_control( fw_node_group::make_val< SERVO >( sid, m ) );
                }
                void on_activate( servo_id id )
                {
                        m_vis.to_control( fw_node_group::make_val< ACTIVATE >( id, ACTIVATED ) );
                }
                void to_servo( s_ctl_id scid, reply r, const master_lewan_message& m )
                {
                        m_vis.to_servo( scid, r, m );
                }
                void to_servo( s_ctl_id scid, reply r, const master_dynmx_message& m )
                {
                        m_vis.to_servo( scid, r, m );
                }
        };

public:
        servo_group( std::optional< servo_type > type_override = {} )
          : type_override_( type_override )
        {
        }

        using iterator = typename container::iterator;

        bool any_has_task() const
        {
                return any_has_task( servo_flags{}.set() );
        }
        bool any_has_task( servo_flags filter ) const
        {
                return em::any_of( servos_, [&]( const servo_ptr& s ) {
                        return s && filter[s->get_id()] && s->has_task();
                } );
        }

        void on_servo_receive(
            clk_time                    clock_time,
            s_ctl_id                    s_id,
            const lewan_master_message& m,
            fw_reactor_visitor&         vis )
        {
                lewan_master_extract( m ).match(
                    [&]( std::tuple<
                         lewan_header_start,
                         lewan_header_start,
                         lewan_servo_id,
                         lewan_master_variant,
                         lewan_checksum > pack ) {
                            auto id      = std::get< lewan_servo_id >( pack );
                            auto msg_var = std::get< lewan_master_variant >( pack );

                            auto iter = find_servo( *id );

                            if ( iter == servos_.end() ) {
                                    vis.make_error_to_control< SERVO_ID_MISSING_E >(
                                        ctl_id{ 0 }, *id );
                                    return;
                            }
                            auto opt_msg = iter->get()->on_servo_receive(
                                clock_time, s_id, servo_receive_variant{ msg_var } );
                            if ( opt_msg ) {
                                    vis.make_to_control< SERVO >( *id, *opt_msg );
                            }
                    },
                    [&]( auto e ) {  // TODO: add error handling
                            em::ignore( e );
                            SCHPIN_ERROR_LOG( "lewan.group", "Lewan error: " << e );
                    } );
        }

        // TODO: duplicity between lewan and dynmx message
        void on_servo_receive(
            clk_time                    clock_time,
            s_ctl_id                    s_id,
            const dynmx_master_message& m,
            fw_reactor_visitor&         vis )
        {
                dynmx_master_extract( m ).match(
                    [&](
                        std::tuple< dynmx_header, dynmx_servo_id, dynmx_master_subvalue, dynmx_crc >
                            pack ) {
                            auto id     = std::get< dynmx_servo_id >( pack );
                            auto subval = std::get< dynmx_master_subvalue >( pack );

                            auto iter = find_servo( *id );
                            if ( iter == servos_.end() ) {
                                    vis.make_error_to_control< SERVO_ID_MISSING_E >(
                                        ctl_id{ 0 }, *id );
                                    return;
                            }
                            auto opt_msg = iter->get()->on_servo_receive(
                                clock_time, s_id, servo_receive_variant{ subval } );
                            if ( opt_msg ) {
                                    vis.make_to_control< SERVO >( *id, *opt_msg );
                            }
                    },
                    [&]( auto e ) {  // TODO: add error handling
                            em::ignore( e );
                            SCHPIN_ERROR_LOG( "lewan.group", "Lewan error: " << e );
                    } );
        }

        // TODO: on_servo_not_replying also duplicated
        void on_servo_not_replying( const master_dynmx_message& m, fw_reactor_visitor& vis )
        {
                master_dynmx_extract( m ).match(
                    [&]( std::tuple< dynmx_header, dynmx_servo_id, master_dynmx_variant, dynmx_crc >
                             pack ) {
                            auto id   = std::get< dynmx_servo_id >( pack );
                            auto iter = find_servo( *id );

                            if ( iter->get()->is_activating() ) {
                                    iter->reset();

                                    vis.make_to_control< ACTIVATE >( *id, MISSING );
                            }
                    },
                    [&]( auto ) {  // TODO: add error handling

                    } );
        }

        void on_servo_not_replying( const master_lewan_message& m, fw_reactor_visitor& vis )
        {
                master_lewan_extract( m ).match(
                    [&]( std::tuple<
                         lewan_header_start,
                         lewan_header_start,
                         lewan_servo_id,
                         master_lewan_variant,
                         lewan_checksum > pack ) {
                            auto sid  = std::get< lewan_servo_id >( pack );
                            auto iter = find_servo( *sid );

                            if ( iter->get()->is_activating() ) {
                                    iter->reset();

                                    vis.make_to_control< ACTIVATE >( *sid, MISSING );
                            }
                    },

                    [&]( auto ) {  // TODO: add error handling

                    } );
        }

        void tick( clk_time clock_time, fw_reactor_visitor& vis )
        {
                for ( servo_ptr& s : servos_ ) {
                        if ( !s ) {
                                continue;
                        }
                        sgroup_visitor svis{ vis };
                        s->tick( clock_time, svis );
                }

                polling_tick( clock_time, vis );
        }

        void stop( fw_reactor_visitor& v )
        {
                for ( servo_ptr& s : servos_ ) {
                        sgroup_visitor svis{ v };
                        s->stop( svis );
                }
        }

        void activate( clk_time now, ctl_id cid, servo_addr saddr, fw_reactor_visitor& v )
        {
                auto [stype, sid] = saddr;
                SCHPIN_ERROR_LOG( "group", "Activating: " << sid );

                auto iter = find_servo( sid );
                if ( iter != servos_.end() ) {
                        v.make_to_control< ACTIVATE >(
                            sid, iter->get()->is_activating() ? WAS_ACTIVATING : WAS_ACTIVATED );
                        SCHPIN_ERROR_LOG( "group", "Allready got it" );
                        return;
                }

                bool is_full = em::all_of( servos_ );

                if ( is_full ) {
                        SCHPIN_ERROR_LOG( "group", "Is full" );
                        v.make_error_to_control< SERVO_CAPACITY_USED_E >( cid );
                        return;
                }

                if ( type_override_ ) {
                        stype = *type_override_;
                }

                for ( servo_ptr& spot : servos_ ) {
                        if ( !spot ) {
                                SCHPIN_ERROR_LOG(
                                    "group",
                                    "Placed new servo of type: "
                                        << magic_enum::enum_name( stype ) );
                                spot = make_servo( stype, sid, now, &memory_ );
                                break;
                        }
                }

                v.make_to_control< ACTIVATE >( sid, ACTIVATING );
        }

        void deactivate( ctl_id, servo_id sid, fw_reactor_visitor& vis )
        {
                SCHPIN_ERROR_LOG( "group", "Deactivating: " << sid );
                auto iter = find_servo( sid );
                if ( iter == servos_.end() ) {
                        vis.make_error_to_control< SERVO_ID_MISSING_E >( ctl_id{ 0 }, sid );
                        return;
                }
                sgroup_visitor svis{ vis };
                iter->get()->stop( svis );
                iter->reset();
        }

        void on_event( ctl_id id, clk_time clock_time, servo_event e, fw_reactor_visitor& vis )
        {
                auto iter = find_servo( e.id );
                SCHPIN_ASSERT( iter != servos_.end() );
                // TODO: check!

                sgroup_visitor svis{ vis };
                iter->get()->on_event( id, clock_time, e.sub_event, svis );
        }

        std::optional< servo_event > on_servo_packet(
            ctl_id               id,
            servo_id             sid,
            master_servo_variant subvar,
            fw_reactor_visitor&  vis )
        {
                auto iter = find_servo( sid );
                if ( iter == servos_.end() ) {
                        vis.make_error_to_control< SERVO_NOT_ACTIVE_E >( id, sid );
                        return {};
                }

                return iter->get()
                    ->on_servo_packet( id, subvar )
                    .convert_left( [&]( servo_events_variant var ) {
                            return std::make_optional< servo_event >( sid, var );
                    } )
                    .convert_right( [&]( servo_master_variant val ) {
                            vis.make_to_control< SERVO >( sid, val );
                            return std::optional< servo_event >{};
                    } )
                    .join();
        }

private:
        void polling_tick( clk_time ctime, fw_reactor_visitor& vis )
        {
                if ( em::none_of( servos_ ) ) {
                        return;
                }
                do {
                        poll_i_ = ( poll_i_ + 1 ) % servos_.size();
                } while ( !servos_[poll_i_] );

                sgroup_visitor svis{ vis };
                servos_[poll_i_]->poll( ctime, poll_i_, svis );
        }

        iterator find_servo( servo_id id )
        {
                return em::find_if( servos_, [&]( const servo_ptr& j ) {
                        return j && j->get_id() == id;
                } );
        }
};

}  // namespace schpin
