#include "fw/visitor.h"

#pragma once

namespace schpin
{

struct servo_visitor
{
        virtual void to_control( servo_id, const servo_master_variant& ) = 0;

        template < messages_enum ID, typename... Args >
        void to_control( servo_id sid, Args&&... args )
        {
                to_control( sid, servo_master_group::make_val< ID >( args... ) );
        }

        template < error_protocol_enum ID, typename... Args >
        void to_control_error( servo_id sid, ctl_id cid, Args&&... args )
        {
                to_control< ERROR >( sid, cid, servo_error_group::make_val< ID >( args... ) );
        }

        virtual void on_activate( servo_id )                                  = 0;
        virtual void to_servo( s_ctl_id, reply, const master_lewan_message& ) = 0;
        virtual void to_servo( s_ctl_id, reply, const master_dynmx_message& ) = 0;
        virtual ~servo_visitor()                                              = default;
};

}  // namespace schpin
