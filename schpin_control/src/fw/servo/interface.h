#include "fw/dynmx/events.h"
#include "fw/lewan/events.h"
#include "fw/servo/visitor.h"

#include <schpin_lib/bucket_memory_resource.h>

#pragma once

namespace schpin
{

static constexpr std::size_t servo_bucket_size = 512;

using servo_events_variant = std::variant< lewan_servo_events_variant, dynmx_servo_events_variant >;
using servo_receive_variant = std::variant< dynmx_master_subvalue, lewan_master_variant >;

class servo_interface
{
public:
        virtual servo_id get_id() const         = 0;
        virtual bool     has_task() const       = 0;
        virtual bool     is_activating() const  = 0;
        virtual void     stop( servo_visitor& ) = 0;
        virtual std::optional< servo_master_variant >
                     on_servo_receive( clk_time, s_ctl_id, const servo_receive_variant& )      = 0;
        virtual void tick( clk_time, servo_visitor& )                                          = 0;
        virtual void on_event( ctl_id, clk_time, const servo_events_variant&, servo_visitor& ) = 0;
        virtual void poll( clk_time, std::size_t, servo_visitor& )                             = 0;
        virtual em::either< servo_events_variant, servo_master_variant >
                     on_servo_packet( ctl_id id, const master_servo_variant& subcmd ) = 0;
        virtual void dump_info()                                                      = 0;

        virtual ~servo_interface() = default;
};

struct servo_event
{
        servo_id             id;
        servo_events_variant sub_event;
};

// TODO: the fact that this is parametric is problematic -> remove dependence of 'servo_ptr' on the
// param
using servo_deleter = bucket_memory_resource_deleter< servo_interface >;
using servo_ptr     = std::unique_ptr< servo_interface, servo_deleter >;

servo_ptr make_lewan_servo( lewan_servo_id id, clk_time, std::pmr::memory_resource* );
servo_ptr make_lewan_test_servo( lewan_servo_id id, clk_time, std::pmr::memory_resource* );
servo_ptr make_dynmx_servo( dynmx_servo_id id, clk_time, std::pmr::memory_resource* );

inline servo_ptr
make_servo( servo_type st, servo_id sid, clk_time now, std::pmr::memory_resource* mem )
{
        // TODO: error handling, including sub-makes - can throw alloc problem
        auto opt_lid = lewan_servo_id::make( sid );
        auto opt_did = dynmx_servo_id::make( sid );
        switch ( st ) {
                case LEWAN:
                        if ( !opt_lid ) {
                                return {};
                        }
                        return make_lewan_servo( *opt_lid, now, mem );
                case LEWAN_TEST:
                        if ( !opt_lid ) {
                                return {};
                        }
                        return make_lewan_test_servo( *opt_lid, now, mem );
                case DYNMX:
                        if ( !opt_did ) {
                                return {};
                        }
                        return make_dynmx_servo( *opt_did, now, mem );
        }
        return {};
}

}  // namespace schpin
