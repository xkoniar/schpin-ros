// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/base.h"
#include "fw/servo/group.h"
#include "fw/visitor.h"
#include "shared/protocol.h"

#include <array>
#include <emlabcpp/match.h>
#include <emlabcpp/protocol/register_handler.h>
#include <emlabcpp/static_circular_buffer.h>

#pragma once

namespace schpin
{

struct wait_event
{
        servo_flags flags;
};

struct activate_event
{
        servo_addr addr;
};

struct deactivate_event
{
        servo_id sid;
};

struct config_event
{
        config_addr   addr;
        config_buffer buffr;
};

template < unsigned ServoCount >
class fw_reactor
{
        static_assert(
            ServoCount <= servo_flags().size(),
            "number of servos is limited based on servo_flags type size" );
        using event_variant =
            std::variant< wait_event, servo_event, activate_event, deactivate_event, config_event >;
        using config_handler = em::protocol_register_handler< fw_config_map >;

        em::static_circular_buffer< std::pair< ctl_id, node_fw_group_value >, 198 > ictl_buffer_{};
        em::static_circular_buffer< std::pair< ctl_id, event_variant >, 198 >       event_buffer_{};
        servo_group< ServoCount >                                                   servo_group_;
        fw_config_map                                                               config_map_;

        clk_time last_input_bufer_telem_{};
        clk_time last_event_bufer_telem_{};

public:
        fw_reactor( std::optional< servo_type > servo_override = {} )
          : servo_group_( servo_override )
        {
        }

        void
        on_servo_receive( clk_time clock_time, s_ctl_id s_id, const auto& m, fw_reactor_visitor& v )
        {
                servo_group_.on_servo_receive( clock_time, s_id, m, v );
        }

        template < typename Message >
        void on_servo_not_replying( const Message& m, fw_reactor_visitor& v )
        {
                servo_group_.on_servo_not_replying( m, v );
        }

        void on_control_receive( ctl_id id, const node_fw_group_value& m, fw_reactor_visitor& v )
        {
                if ( ictl_buffer_.full() ) {
                        v.make_error_to_control< INPUT_CONTROL_BUFFER_FULL_E >( id );
                }
                ictl_buffer_.push_back( std::make_pair( id, m ) );
        }

        void tick( clk_time clock_time, fw_reactor_visitor& v )
        {
                for ( std::size_t i = 0; i < 2; ++i ) {
                        if ( !ictl_buffer_.empty() ) {
                                auto [id, var] = ictl_buffer_.pop_front();
                                on_packet( id, var, v );
                        }
                }

                handle_event( clock_time, v );

                if ( last_input_bufer_telem_ +
                         config_map_.get_val< INPUT_BUFFER_TELEM_PERIOD_MS >() <=
                     clock_time ) {
                        v.make_to_control< INPUT_BUFFER_SIZE >(
                            static_cast< uint16_t >( ictl_buffer_.size() ) );
                        last_input_bufer_telem_ = clock_time;
                }
                if ( last_input_bufer_telem_ +
                         config_map_.get_val< EVENT_BUFFER_TELEM_PERIOD_MS >() <=
                     clock_time ) {
                        v.make_to_control< EVENT_BUFFER_SIZE >(
                            static_cast< uint16_t >( event_buffer_.size() ) );
                        last_event_bufer_telem_ = clock_time;
                }

                servo_group_.tick( clock_time, v );
        }

        void handle_event( clk_time clock_time, fw_reactor_visitor& v )
        {
                servo_flags blocking_flags;  // flags where b[i] = 1 if 'i' servo is blocked by some
                                             // event in the queue
                blocking_flags.reset();
                auto iter =
                    em::find_if( event_buffer_, [&]( std::tuple< ctl_id, event_variant > pack ) {
                            const auto& ev = std::get< event_variant >( pack );
                            return em::match(
                                ev,
                                [&]( wait_event e ) {
                                        if ( ( e.flags & blocking_flags ).any() ) {
                                                // previous wait_event is blocking some servo
                                                // of this event, so this has to be skipped.
                                                blocking_flags |= e.flags;
                                                return false;
                                        }
                                        // now we know that none of the e.flags servos is
                                        // blocked by previous wait events
                                        if ( servo_group_.any_has_task( e.flags ) ) {
                                                // at least one of the servos is busy
                                                blocking_flags |= e.flags;
                                                return false;
                                        }
                                        // all servos are finished and there is no previous
                                        // blocking event, this is it!
                                        return true;
                                },
                                [&]( servo_event e ) {
                                        return !blocking_flags[e.id];
                                },
                                [&]( activate_event ) {
                                        return true;
                                },
                                [&]( deactivate_event ) {
                                        return true;
                                },
                                [&]( config_event ) {
                                        return true;
                                } );
                    } );
                if ( iter == event_buffer_.end() ) {
                        return;
                }
                ctl_id        id    = iter->first;
                event_variant event = iter->second;
                // move selected event to the front of reversed(buffer) -> end of the buffer
                while ( iter != event_buffer_.begin() ) {
                        using namespace std;
                        swap( *std::prev( iter ), *iter );
                        iter--;
                }
                event_buffer_.pop_front();

                em::match(
                    event,
                    [&]( wait_event ) {
                            v.make_to_control< FINISHED >( id );
                    },
                    [&]( servo_event e ) {
                            servo_group_.on_event( id, clock_time, e, v );
                    },
                    [&]( activate_event e ) {
                            servo_group_.activate( clock_time, id, e.addr, v );
                    },
                    [&]( deactivate_event e ) {
                            servo_group_.deactivate( id, e.sid, v );
                    },
                    [&]( config_event e ) {
                            auto opt_error = config_handler::insert(
                                config_map_, static_cast< fw_config_enum >( e.addr ), e.buffr );
                            if ( opt_error ) {
                                    v.make_error_to_control< CONFIG_E >( id, *opt_error );
                            }
                    } );
        }

        void on_packet( ctl_id id, node_fw_group_value var, fw_reactor_visitor& v )
        {
                em::apply_on_match(
                    var,
                    [&]( em::tag< PING > ) {
                            v.make_to_control< PONG >();
                    },
                    [&]( em::tag< STOP > ) {
                            servo_group_.stop( v );
                    },
                    [&]( em::tag< SYNC >, servo_flags flag ) {
                            add_event( id, wait_event{ flag }, v );
                    },
                    [&]( em::tag< SERVO >, servo_id sid, master_servo_variant subvar ) {
                            auto opt_event = servo_group_.on_servo_packet( id, sid, subvar, v );
                            if ( opt_event ) {
                                    add_event( id, *opt_event, v );
                            }
                    },
                    [&]( em::tag< ACTIVATE >, servo_addr sid ) {
                            add_event( id, activate_event{ sid }, v );
                    },
                    [&]( em::tag< DEACTIVATE >, servo_id sid ) {
                            add_event( id, deactivate_event{ sid }, v );
                    },
                    [&]( em::tag< CONFIG_SET >, config_addr addr, config_buffer bffr ) {
                            add_event( id, config_event{ addr, bffr }, v );
                    },
                    [&]( em::tag< CONFIG_GET >, config_addr addr ) {
                            static_assert(
                                config_handler::max_size < std::tuple_size_v< config_buffer > );
                            auto msg = config_handler::select(
                                config_map_, static_cast< fw_config_enum >( addr ) );
                            config_buffer buff{};
                            std::copy( msg.begin(), msg.end(), buff.begin() );

                            v.make_to_control< CONFIG_GET >( addr, buff );
                    } );
        }

private:
        void add_event( ctl_id id, event_variant e, fw_reactor_visitor& v )
        {
                if ( event_buffer_.full() ) {
                        v.make_error_to_control< INPUT_EVENT_BUFFER_FULL_E >( id );
                }
                event_buffer_.push_back( std::make_pair( id, e ) );
        }
};

}  // namespace schpin
