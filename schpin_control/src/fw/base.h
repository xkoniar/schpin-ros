// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/dynmx/base.h"
#include "fw/lewan/base.h"

#include <bitset>
#include <emlabcpp/protocol/message.h>
#include <schpin_lib_exp/scaled.h>
#include <variant>

#pragma once

namespace schpin
{

enum class reply
{
        YES,
        NO
};

enum servo_task_enum : uint8_t
{
        TASK_GOTO,
        TASK_SPIN,
        TASK_IDLE,
        TASK_ACTIVATE
};

struct full_buffer_tag
{
};

struct activate_tag
{
};

// TODO: this has to be strongly typed :/
using ctl_id        = uint16_t;
using s_ctl_id      = uint16_t;
using checksum      = uint16_t;
using config_addr   = uint16_t;
using config_buffer = std::array< uint8_t, 8 >;

using servo_flags = std::bitset< 256 >;
using clk_time    = scaled< struct clock_time_tag, uint32_t, std::milli >;
using pid_gain    = scaled< struct pid_gain_tag, uint32_t, std::micro >;

using bangle            = scaled< struct bangle_tag, int16_t, std::milli >;
using btime             = scaled< struct btime_tag, int16_t, std::milli >;
using bangular_velocity = scaled< struct bangular_velocity_tag, int16_t, std::milli >;
using btemp             = scaled< struct btemp_tag, uint16_t, std::milli >;
using bvoltage          = scaled< struct bvoltage_tag, uint16_t, std::milli >;
using bfreq             = scaled< struct bfreq_tag, uint16_t, std::milli >;

enum end_condition : uint8_t
{
        ON_TIME = 1,
        ON_POS  = 2
};

// TODO: this has to be strongly typed :/ and definetly not uint16_t
using servo_id = uint16_t;
enum servo_type : uint16_t
{
        LEWAN      = 0,
        LEWAN_TEST = 1,
        DYNMX      = 2,
};
using servo_addr = std::tuple< servo_type, servo_id >;

#ifdef SCHPIN_USE_STREAMS
inline std::ostream& operator<<( std::ostream& os, const servo_addr& addr )
{
        return os << magic_enum::enum_name( std::get< 0 >( addr ) ) << "," << std::get< 1 >( addr );
}
#endif

}  // namespace schpin
