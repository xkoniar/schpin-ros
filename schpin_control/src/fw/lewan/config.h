// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/lewan/base.h"

#include <emlabcpp/protocol/register_map.h>

#pragma once

namespace schpin
{

using lewan_config_map_def = em::protocol_register_map<
    em::PROTOCOL_BIG_ENDIAN,
    em::protocol_reg< LEWAN_INPUT_RANGE_LOW, bangle >,
    em::protocol_reg< LEWAN_INPUT_RANGE_HIGH, bangle >,
    em::protocol_reg< LEWAN_SERVO_RANGE_LOW, lewan_angle >,
    em::protocol_reg< LEWAN_SERVO_RANGE_HIGH, lewan_angle >,
    em::protocol_reg< LEWAN_ANGLE_CHANGE_REPORT, lewan_angle >,
    em::protocol_reg< LEWAN_ANGLE_STALL_RANGE, lewan_angle >,
    em::protocol_reg< LEWAN_TELEM, bool >,
    em::protocol_reg< LEWAN_GOTO_P_GAIN, pid_gain >,
    em::protocol_reg< LEWAN_GOTO_I_GAIN, pid_gain >,
    em::protocol_reg< LEWAN_GOTO_D_GAIN, pid_gain >,
    em::protocol_reg< LEWAN_VEL_P_GAIN, pid_gain >,
    em::protocol_reg< LEWAN_VEL_I_GAIN, pid_gain >,
    em::protocol_reg< LEWAN_VEL_D_GAIN, pid_gain > >;

struct lewan_config_map : lewan_config_map_def
{
        using lewan_config_map_def::lewan_config_map_def;
        lewan_config_map()
          : lewan_config_map_def(
                bangle::from_float( 0.f ),
                bangle::from_float( 4.18879f ),
                lewan_angle::get< 0 >(),
                lewan_angle::get< 1000 >(),
                lewan_angle::get< 5 >(),
                lewan_angle::get< 25 >(),
                false,
                pid_gain::from_float( 10.f ),
                pid_gain::from_float( 0.f ),
                pid_gain::from_float( 150.f ),
                pid_gain::from_float( 0.6f ),
                pid_gain::from_float( 0.005f ),
                pid_gain::from_float( 0.6f ) )
        {
        }
};

}  // namespace schpin
