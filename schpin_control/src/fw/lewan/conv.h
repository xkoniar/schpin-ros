// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/lewan/config.h"
#include "fw/util.h"

#pragma once

namespace schpin
{
class lewan_converter
{
        float in_from = 0.f;
        float in_to   = 0.f;
        float se_from = 0.f;
        float se_to   = 0.f;

public:
        lewan_converter() = default;

        lewan_converter( const lewan_config_map& conf )
          : in_from( conf.get_val< LEWAN_INPUT_RANGE_LOW >().as_float() )
          , in_to( conf.get_val< LEWAN_INPUT_RANGE_HIGH >().as_float() )
          , se_from( static_cast< float >( *conf.get_val< LEWAN_SERVO_RANGE_LOW >() ) )
          , se_to( static_cast< float >( *conf.get_val< LEWAN_SERVO_RANGE_HIGH >() ) )
        {
        }

        std::optional< lewan_angle > in_to_se( bangle a ) const
        {
                float mapped = map_val< float >( a.as_float(), in_from, in_to, se_from, se_to );
                return lewan_angle::make( mapped );
        }

        std::optional< lewan_time > in_to_se( btime t ) const
        {
                return lewan_time::make( t.as_float() * 1000.f );
        }

        std::optional< lewan_angular_velocity > in_to_se( bangular_velocity av ) const
        {
                float s_diff = se_to - se_from;
                float i_diff = in_to - in_from;
                float coeff  = std::fabs( s_diff ) / std::fabs( i_diff );

                float res = av.as_float() * coeff;

                if ( em::sign( s_diff ) != em::sign( i_diff ) ) {
                        res *= -1;
                }

                return lewan_angular_velocity::make( res );
        }

        bangle se_to_in( lewan_full_angle a ) const
        {
                float mapped =
                    map_val< float >( static_cast< float >( *a ), se_from, se_to, in_from, in_to );
                return bangle::from_float( mapped );
        }

        bangle se_to_in( lewan_angle a ) const
        {
                return se_to_in( lewan_full_angle{ a } );
        }

        btemp se_to_in( lewan_temp temp ) const
        {
                return btemp::from_float( *temp );
        }

        bvoltage se_to_in( lewan_voltage volt ) const
        {
                return bvoltage{ *volt };
        }

        bangular_velocity se_to_in( lewan_angular_velocity vel ) const
        {
                float mapped = map_val< float >(
                    static_cast< float >( *vel ), se_from, se_to, in_from, in_to );

                return bangular_velocity::from_float( mapped );
        }
};
}  // namespace schpin
