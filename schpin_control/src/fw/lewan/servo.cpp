#include "fw/lewan/servo.h"

#include "fw/lewan/tester.h"
#include "fw/servo/base.h"
#include "fw/servo/util.h"

#include <emlabcpp/match.h>
#include <emlabcpp/protocol/register_handler.h>

namespace schpin
{

using config_handler = em::protocol_register_handler< lewan_config_map >;

servo_ptr make_lewan_test_servo( lewan_servo_id id, clk_time now, std::pmr::memory_resource* mem )
{
        static_assert( servo_bucket_size >= sizeof( lewan_servo_tester ) );
        SCHPIN_ASSERT( mem != nullptr );
        void* target = mem->allocate( sizeof( lewan_servo_tester ), alignof( lewan_servo_tester ) );
        return { new ( target ) lewan_servo_tester{ id, now }, servo_deleter{ mem } };
}

servo_ptr make_lewan_servo( lewan_servo_id id, clk_time now, std::pmr::memory_resource* mem )
{
        static_assert( servo_bucket_size >= sizeof( lewan_servo ) );
        SCHPIN_ASSERT( mem != nullptr );
        void* target = mem->allocate( sizeof( lewan_servo ), alignof( lewan_servo ) );
        return { new ( target ) lewan_servo{ id, now }, servo_deleter{ mem } };
}

void lewan_servo::stop( servo_visitor& vis )
{
        // TODO: DO NOT DO THIS IF ACTIVATING?
        task_ = idle_task{};
        transmit_servo< LEWAN_SERVO_MOVE_STOP, reply::NO >( vis );
}

std::optional< servo_master_variant >
lewan_servo::on_servo_receive( clk_time clock_time, s_ctl_id, const servo_receive_variant& svar )
{
        const auto& var = std::get< lewan_master_variant >( svar );
        lewan_catcher_.check( var );
        context_.on_servo_receive( clock_time, var );

        using pos_tuple = std::tuple< em::tag< LEWAN_SERVO_POS_READ >, lewan_full_angle >;
        if ( !std::holds_alternative< pos_tuple >( var ) ) {
                return {};
        }

        new_pos_ = true;

        lewan_angle change = config_map_.get_val< LEWAN_ANGLE_CHANGE_REPORT >();
        if ( change == lewan_angle::get< 0 >() ) {
                return {};
        }

        if ( angle_dist( last_report_, context_->pos ) < *change ) {
                return {};
        }
        last_report_ = context_->pos;
        return servo_master_group::make_val< POSITION >( converter_.se_to_in( context_->pos ) );
}

void lewan_servo::tick( clk_time clock_time, servo_visitor& vis )
{
        em::match(
            task_,
            [&]( idle_task ) {},
            [&]( const activate_task& s ) {
                    activate_tick( s, vis );
            },
            [&]( goto_task t ) {
                    if ( !new_pos_ ) {
                            return;
                    }
                    // TODO: implement after pos mechanics
                    new_pos_ = false;

                    float goal_f  = static_cast< float >( *t.goal );
                    float start_f = static_cast< float >( *t.start );
                    float now_f   = static_cast< float >( *( clock_time - t.start_time_point ) );
                    float move_duration_f = static_cast< float >( *t.move_duration );

                    now_f = std::clamp( now_f, 0.f, move_duration_f );

                    float desired_pos =
                        em::map_range( now_f, 0.f, move_duration_f, start_f, goal_f );

                    float actual_f = static_cast< float >( *context_->pos );
                    desired_pos    = pos_pid_.update( clock_time, actual_f, desired_pos );

                    auto opt_an = lewan_angle::make( desired_pos );
                    SCHPIN_ASSERT( opt_an );

                    transmit_servo< LEWAN_SERVO_MOVE_TIME_WRITE, reply::NO >(
                        vis, *opt_an, lewan_time::get< 0 >() );

                    if ( config_map_.get_val< LEWAN_TELEM >() ) {
                            vis.to_control< TELEM >(
                                *id_,
                                converter_.se_to_in( context_->pos ),
                                converter_.se_to_in( *opt_an ),
                                btime{ now_f } );
                    }

                    if ( now_f < move_duration_f ) {
                            return;
                    }

                    if ( t.end_cond == ON_TIME ) {
                            task_ = idle_task{};
                    }

                    // random number - meh TODO
                    if ( t.end_cond == ON_POS &&
                         angle_dist( context_->pos, lewan_full_angle{ t.goal } ) < 15 ) {
                            task_ = idle_task{};
                    }
            },
            [&]( const spin_task& t ) {
                    if ( t.clock_end > clock_time ) {
                            return;
                    }
                    task_ = idle_task{};
            } );
}

void lewan_servo::on_event(
    ctl_id                      cid,
    clk_time                    clock_time,
    const servo_events_variant& svar,
    servo_visitor&              vis )
{
        // TODO: error handling
        const auto& var = std::get< lewan_servo_events_variant >( svar );

        em::match(
            var,
            [&]( lewan_goto_event e ) {
                    transmit_servo< LEWAN_SERVO_OR_MOTOR_MODE_WRITE, reply::NO >(
                        vis,
                        lewan_position_mode,
                        uint8_t{ 0 },
                        lewan_angular_velocity::get< 0 >() );

                    pos_pid_.reset( static_cast< float >( *context_->pos ) );

                    task_ = goto_task{
                        .start            = context_->pos,
                        .goal             = e.g,
                        .start_time_point = clock_time,
                        .move_duration    = e.t,
                        .end_cond         = e.end };
            },
            [&]( lewan_spin_event e ) {
                    task_ = spin_task{ .clock_end = clock_time + clk_time{ e.t } };
                    transmit_servo< LEWAN_SERVO_OR_MOTOR_MODE_WRITE, reply::NO >(
                        vis, lewan_motor_mode, uint8_t{ 0 }, e.av );
            },
            [&]( servo_config_set_event e ) {
                    auto opt_error = config_handler::insert(
                        config_map_, static_cast< lewan_config_enum >( e.addr ), e.buffer );
                    if ( opt_error ) {
                            vis.to_control_error< CONFIG_E >( *id_, cid );
                    }
                    converter_ = lewan_converter{ config_map_ };
            },
            [&]( servo_id_event e ) {
                    auto opt_lid = lewan_servo_id::make( e.sid );
                    if ( !opt_lid ) {
                            vis.to_control_error< SERVO_ID_INVALID_E >( cid, *id_ );
                    }
                    transmit_servo< LEWAN_SERVO_ID_WRITE, reply::NO >( vis, *opt_lid );
                    id_ = *opt_lid;
            } );
}

void lewan_servo::poll( clk_time, std::size_t i_offset, servo_visitor& vis )
{
        poll_i_       = ( poll_i_ + 1 ) % 13;
        std::size_t i = ( i_offset + poll_i_ ) % 13;
        switch ( i ) {
                case 4:
                        transmit_servo< LEWAN_SERVO_VIN_READ, reply::YES >( vis );
                        break;
                case 8:
                        transmit_servo< LEWAN_SERVO_TEMP_READ, reply::YES >( vis );
                        break;
                default:
                        transmit_servo< LEWAN_SERVO_POS_READ, reply::YES >( vis );
                        break;
        }
}

em::either< servo_events_variant, servo_master_variant >
lewan_servo::on_servo_packet( ctl_id id, const master_servo_variant& subcmd )
{
        return em::apply_on_match(
            subcmd,
            servo_goto_handler< lewan_converter, lewan_servo_events_variant, lewan_goto_event >{
                id, converter_ },
            servo_spin_handler< lewan_converter, lewan_servo_events_variant, lewan_spin_event >{
                id, converter_ },
            make_servo_query_handler< POSITION >( converter_, context_->pos ),
            make_servo_query_handler< TEMP >( converter_, context_->temperature ),
            make_servo_query_handler< VOLTAGE >( converter_, context_->voltage ),
            make_servo_query_handler< VELOCITY >( converter_, context_->velocity ),
            servo_task_query_handler{ task_ },
            [&]( em::tag< ID >, servo_id sid )
                -> em::either< servo_events_variant, servo_master_variant > {  // TODO:
                                                                               // duplicated
                    return servo_events_variant{
                        lewan_servo_events_variant{ servo_id_event{ sid } } };
            },
            config_msg_handler< config_handler, lewan_servo_events_variant >{ config_map_ } );
}

void lewan_servo::dump_info()
{
#ifdef SCHPIN_USE_STREAMS
        std::cout << config_map_;
#endif
}

void lewan_servo::activate_tick( activate_task s, servo_visitor& v )
{
        switch ( s.i ) {
                case 0:
                        em::select_index( s.msg_i, [&]< std::size_t i >() {
                                transmit_servo< activate_task::startup_msgs[i], reply::YES >( v );
                        } );
                        s.msg_i.rotate_right( 1u );
                        if ( *s.msg_i != 0 ) {
                                task_ = s;
                                return;
                        }
                        break;
                case 1:
                        lewan_catcher_.set_trap( LEWAN_SERVO_POS_READ );
                        transmit_servo< LEWAN_SERVO_POS_READ, reply::YES >( v );
                        break;
                case 2:
                        if ( !lewan_catcher_.catched() ) {
                                return;
                        }
                        break;
                default:
                        v.on_activate( *id_ );
                        task_ = idle_task{};
                        return;
        }

        s.i += 1;
        task_ = s;
}

}  // namespace schpin
