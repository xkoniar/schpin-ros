// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/lewan/servo.h"
#include "fw/servo/visitor.h"
#include "test/servo.h"

#pragma once

namespace schpin
{

class lewan_servo_tester : public servo_interface
{
        lewan_servo servo_;
        sim_servo   sim_;

        struct lewan_tester_visitor : servo_visitor
        {
                clk_time            ctime;
                servo_visitor&      master_vis;
                lewan_servo_tester& tester;

                lewan_tester_visitor( clk_time ct, servo_visitor& sv, lewan_servo_tester& ts )
                  : ctime( ct )
                  , master_vis( sv )
                  , tester( ts )
                {
                }

                void to_servo( s_ctl_id msg_id, reply, const master_lewan_message& m )
                {
                        master_lewan_extract( m ).match(
                            [&]( std::tuple<
                                 lewan_header_start,
                                 lewan_header_start,
                                 lewan_servo_id,
                                 master_lewan_variant,
                                 lewan_checksum > pack ) {
                                    tester.pass_to_sim(
                                        ctime,
                                        msg_id,
                                        std::get< lewan_servo_id >( pack ),
                                        std::get< master_lewan_variant >( pack ),
                                        master_vis );
                            },
                            [&]( auto ) {} );
                }

                void to_servo( s_ctl_id, reply, const master_dynmx_message& )
                {
                        SCHPIN_ASSERT( false );
                }

                void to_control( servo_id id, const servo_master_variant& m )
                {
                        master_vis.to_control( id, m );
                }

                void on_activate( servo_id id )
                {
                        master_vis.on_activate( id );
                }
        };

        void pass_to_sim(
            clk_time                    ctime,
            s_ctl_id                    s_id,
            lewan_servo_id              id,
            const master_lewan_variant& var,
            servo_visitor&              vis )
        {
                auto opt_msg = sim_.on_cmd( ctime, id, var );
                if ( !opt_msg ) {
                        return;
                }
                // TODO: this duplicates servogroup
                // maybe make ::extract uplift the message?
                lewan_master_extract( *opt_msg )
                    .match(
                        [&]( std::tuple<
                             lewan_header_start,
                             lewan_header_start,
                             lewan_servo_id,
                             lewan_master_variant,
                             lewan_checksum > pack ) {
                                auto opt_msg = servo_.on_servo_receive(
                                    ctime,
                                    s_id,
                                    servo_receive_variant{
                                        std::get< lewan_master_variant >( pack ) } );
                                if ( !opt_msg ) {
                                        return;
                                }
                                vis.to_control( *id, *opt_msg );
                        },
                        [&]( auto ) {} );
        }

public:
        using receive_type   = lewan_master_group;
        using events_variant = lewan_servo_events_variant;

        lewan_servo_tester( lewan_servo_id id, clk_time now )
          : servo_( id, now )
          , sim_( id, now, *id * 0.01f )
        {
        }

        servo_id get_id() const
        {
                return servo_.get_id();
        }

        bool has_task() const
        {
                return servo_.has_task();
        }

        bool is_activating() const
        {
                return servo_.is_activating();
        }

        void stop( servo_visitor& v )
        {
                // TODO: clk_time is problem - set properly
                lewan_tester_visitor vis{ clk_time{}, v, *this };
                servo_.stop( vis );
        }

        std::optional< servo_master_variant >
        on_servo_receive( clk_time, s_ctl_id, const servo_receive_variant& )
        {
                // TODO: when this is called it is error
                return {};
        }

        void tick( clk_time ctime, servo_visitor& v )
        {
                lewan_tester_visitor vis{ ctime, v, *this };
                servo_.tick( ctime, vis );
        }

        void
        on_event( ctl_id cid, clk_time ctime, const servo_events_variant& var, servo_visitor& v )
        {
                lewan_tester_visitor vis{ ctime, v, *this };
                servo_.on_event( cid, ctime, var, vis );
        }

        void poll( clk_time ctime, std::size_t offset_i, servo_visitor& v )
        {
                lewan_tester_visitor vis{ ctime, v, *this };
                servo_.poll( ctime, offset_i, vis );
        }

        em::either< servo_events_variant, servo_master_variant >
        on_servo_packet( ctl_id cid, const master_servo_variant& var )
        {
                return servo_.on_servo_packet( cid, var );
        }

        void dump_info()
        {
                servo_.dump_info();
        }
};
}  // namespace schpin
