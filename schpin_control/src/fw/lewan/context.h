// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/lewan/protocol.h"
#include "fw/util.h"
#include "fw/util/movement_hist.h"

#include <emlabcpp/match.h>
#include <schpin_lib/logging.h>

#pragma once

namespace schpin
{

struct lewan_servo_state
{
        lewan_full_angle       pos          = lewan_full_angle::get< 0 >();
        lewan_voltage          voltage      = lewan_voltage::get< 5000 >();
        lewan_angle_offset     angle_offset = lewan_angle_offset::get< 0 >();
        lewan_angle            min_angle    = lewan_angle::min();
        lewan_angle            max_angle    = lewan_angle::max();
        lewan_voltage          voltage_min  = lewan_voltage::min();
        lewan_voltage          voltage_max  = lewan_voltage::max();
        lewan_temp             temperature  = lewan_temp::min();
        lewan_temp             temp_limit   = lewan_temp::min();
        lewan_led_mode         led_mode     = lewan_no_load;
        lewan_led_error        led_error    = lewan_all_led;
        lewan_angular_velocity velocity     = lewan_angular_velocity::get< 0 >();
};

class lewan_context
{
        lewan_servo_state         state_;
        movement_history< 16, 5 > mov_his_;
        lewan_full_angle          last_angle_;
        clk_time                  last_reading_;
        float                     stackup_ = 0.f;

public:
        const lewan_servo_state& operator*() const
        {
                return state_;
        }

        const lewan_servo_state* operator->() const
        {
                return &state_;
        }

        auto get_hist_sum() const
        {
                return mov_his_.get_sum();
        }

        void on_servo_receive( clk_time clock_time, const lewan_master_variant& var )
        {
                em::apply_on_match(
                    var,
                    [&]( em::tag< LEWAN_SERVO_MOVE_TIME_READ >, lewan_angle, lewan_time ) {},
                    [&]( em::tag< LEWAN_SERVO_MOVE_TIME_WAIT_READ >, lewan_angle, lewan_time ) {},
                    [&]( em::tag< LEWAN_SERVO_ID_READ >, lewan_servo_id ) {},
                    [&]( em::tag< LEWAN_SERVO_ANGLE_OFFSET_READ >, lewan_angle_offset ao ) {
                            state_.angle_offset = ao;
                    },
                    [&]( em::tag< LEWAN_SERVO_ANGLE_LIMIT_READ >,
                         lewan_angle mina,
                         lewan_angle maxa ) {
                            state_.min_angle = mina;
                            state_.max_angle = maxa;
                    },
                    [&]( em::tag< LEWAN_SERVO_VIN_LIMIT_READ >,
                         lewan_voltage minv,
                         lewan_voltage maxv ) {
                            state_.voltage_min = minv;
                            state_.voltage_max = maxv;
                    },
                    [&]( em::tag< LEWAN_SERVO_TEMP_MAX_LIMIT_READ >, lewan_temp lim ) {
                            state_.temp_limit = lim;
                    },
                    [&]( em::tag< LEWAN_SERVO_TEMP_READ >, lewan_temp tmp ) {
                            state_.temperature = tmp;
                    },
                    [&]( em::tag< LEWAN_SERVO_VIN_READ >, lewan_voltage v ) {
                            state_.voltage = v;
                    },
                    [&]( em::tag< LEWAN_SERVO_POS_READ >, lewan_full_angle a ) {
                            mov_his_.take_reading( clock_time, a );

                            float achange = stackup_ + angle_change( last_angle_, a );
                            float tdiff =
                                static_cast< float >( *clock_time - *last_reading_ ) / 1000.f;
                            int16_t vel   = static_cast< int16_t >( achange / tdiff );
                            stackup_      = achange - vel * tdiff;
                            last_reading_ = clock_time;
                            last_angle_   = a;
                            auto opt_vel  = lewan_angular_velocity::make( vel );
                            if ( opt_vel ) {
                                    state_.velocity = *opt_vel;
                            } else {
                                    // SCHPIN_ASSERT( false );
                            }
                            state_.pos = a;
                    },
                    [&]( em::tag< LEWAN_SERVO_OR_MOTOR_MODE_READ >,
                         lewan_servo_mode,
                         uint8_t,
                         lewan_angular_velocity ) {},
                    [&]( em::tag< LEWAN_SERVO_LOAD_OR_UNLOAD_READ >, lewan_load ) {},
                    [&]( em::tag< LEWAN_SERVO_LED_CTRL_READ >, lewan_led_mode m ) {
                            state_.led_mode = m;
                    },
                    [&]( em::tag< LEWAN_SERVO_LED_ERROR_READ >, lewan_led_error e ) {
                            state_.led_error = e;
                    } );
        }
};

}  // namespace schpin
