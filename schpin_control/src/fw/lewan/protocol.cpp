
#include <emlabcpp/protocol/handler.h>
#include <fw/lewan/protocol.h>

namespace schpin
{

master_lewan_message master_lewan_serialize( const master_lewan_value& val )
{
        return em::protocol_handler< master_lewan_protocol >::serialize( val );
}
em::either< master_lewan_value, em::protocol_error_record >
master_lewan_extract( const master_lewan_message& msg )
{
        return em::protocol_handler< master_lewan_protocol >::extract( msg );
}
lewan_master_message lewan_master_serialize( const lewan_master_value& val )
{
        return em::protocol_handler< lewan_master_protocol >::serialize( val );
}
em::either< lewan_master_value, em::protocol_error_record >
lewan_master_extract( const lewan_master_message& msg )
{
        return em::protocol_handler< lewan_master_protocol >::extract( msg );
}

}  // namespace schpin
