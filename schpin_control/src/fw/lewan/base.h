// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cstdint>
#include <schpin_lib/bounded.h>

#pragma once

namespace em = emlabcpp;  // TODO: << track this and deal with it more properly

namespace schpin
{

enum lewan_config_enum : uint8_t
{
        LEWAN_INPUT_RANGE_LOW     = 0,
        LEWAN_INPUT_RANGE_HIGH    = 1,
        LEWAN_SERVO_RANGE_LOW     = 2,
        LEWAN_SERVO_RANGE_HIGH    = 3,
        LEWAN_ANGLE_CHANGE_REPORT = 4,
        LEWAN_ANGLE_STALL_RANGE   = 5,
        LEWAN_GOTO_P_GAIN         = 6,
        LEWAN_GOTO_I_GAIN         = 7,
        LEWAN_GOTO_D_GAIN         = 8,
        LEWAN_VEL_P_GAIN          = 9,
        LEWAN_VEL_I_GAIN          = 10,
        LEWAN_VEL_D_GAIN          = 11,
        LEWAN_TELEM               = 12
};

using lewan_angle                                     = em::bounded< uint16_t, 0, 1000 >;
using lewan_full_angle                                = em::bounded< int16_t, -500, 1500 >;
using lewan_time                                      = em::bounded< uint16_t, 0, 30000 >;  // ms
using lewan_servo_id                                  = em::bounded< uint8_t, 0, 253 >;
using lewan_angle_offset                              = em::bounded< int8_t, -125, 125 >;
using lewan_voltage                                   = em::bounded< uint16_t, 4500, 12000 >;
using lewan_temp                                      = em::bounded< uint8_t, 0, 100 >;
using lewan_servo_mode                                = em::bounded< uint8_t, 0, 1 >;
static constexpr lewan_servo_mode lewan_position_mode = lewan_servo_mode::get< 0 >();
static constexpr lewan_servo_mode lewan_motor_mode    = lewan_servo_mode::get< 1 >();
using lewan_load                                      = em::bounded< uint8_t, 0, 1 >;
static constexpr lewan_load lewan_no_load             = lewan_load::get< 0 >();
static constexpr lewan_load lewan_has_load            = lewan_load::get< 1 >();
using lewan_angular_velocity                          = em::bounded< int16_t, -1000, 1000 >;
using lewan_led_mode                                  = em::bounded< uint8_t, 0, 1 >;
using lewan_led_error                                 = em::bounded< uint8_t, 0, 7 >;
static constexpr lewan_led_error lewan_no_alarm_led   = lewan_led_error::get< 0 >();
static constexpr lewan_led_error lewan_over_temp_led  = lewan_led_error::get< 1 >();
static constexpr lewan_led_error lewan_over_volt_led  = lewan_led_error::get< 2 >();
static constexpr lewan_led_error lewan_over_temp_volt_led    = lewan_led_error::get< 3 >();
static constexpr lewan_led_error lewan_locked_rotor_led      = lewan_led_error::get< 4 >();
static constexpr lewan_led_error lewan_over_temp_stalled_led = lewan_led_error::get< 5 >();
static constexpr lewan_led_error lewan_over_volt_stalled_led = lewan_led_error::get< 6 >();
static constexpr lewan_led_error lewan_all_led               = lewan_led_error::get< 7 >();
using lewan_header_start                                     = em::bounded< uint8_t, 85, 85 >;
static constexpr lewan_header_start lewan_header_start_v     = lewan_header_start::get< 85 >();
using lewan_packet_size                                      = uint8_t;
using lewan_checksum                                         = uint8_t;

}  // namespace schpin
