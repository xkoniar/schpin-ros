// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/base.h"
#include "fw/lewan/config.h"
#include "fw/lewan/context.h"
#include "fw/lewan/conv.h"
#include "fw/servo/interface.h"
#include "fw/servo/util.h"

#include <emlabcpp/pid.h>
#include <schpin_lib/algorithm.h>

#pragma once

namespace schpin
{
class lewan_servo : public servo_interface
{
        struct activate_task
        {
                static constexpr servo_task_enum task_mark    = TASK_ACTIVATE;
                static constexpr std::array      startup_msgs = {
                    LEWAN_SERVO_ANGLE_OFFSET_READ,
                    LEWAN_SERVO_ANGLE_LIMIT_READ,
                    LEWAN_SERVO_VIN_LIMIT_READ,
                    LEWAN_SERVO_TEMP_MAX_LIMIT_READ,
                    LEWAN_SERVO_OR_MOTOR_MODE_READ,
                    LEWAN_SERVO_LED_CTRL_READ,
                    LEWAN_SERVO_POS_READ };
                em::bounded< std::size_t, 0, startup_msgs.size() - 1 > msg_i;
                std::size_t                                            i = 0;
        };

        struct goto_task
        {
                static constexpr servo_task_enum task_mark = TASK_GOTO;
                lewan_full_angle                 start;
                lewan_angle                      goal;
                clk_time                         start_time_point;
                btime                            move_duration;
                end_condition                    end_cond;
        };

        struct spin_task
        {
                static constexpr servo_task_enum task_mark = TASK_SPIN;
                clk_time                         clock_end;
        };

        struct idle_task
        {
                static constexpr servo_task_enum task_mark = TASK_IDLE;
        };

        using task_variant = std::variant< activate_task, goto_task, spin_task, idle_task >;

        lewan_servo_id                        id_;
        lewan_config_map                      config_map_;
        lewan_context                         context_;
        lewan_converter                       converter_;
        task_variant                          task_ = { activate_task{} };
        lewan_full_angle                      last_report_;
        message_catcher< lewan_master_group > lewan_catcher_;
        std::size_t                           poll_i_         = 0;
        s_ctl_id                              msg_id_counter_ = 0;

        bool new_pos_ = false;

        em::pid< clk_time > pos_pid_;

public:
        using receive_type   = lewan_master_variant;
        using events_variant = lewan_servo_events_variant;

        lewan_servo( lewan_servo_id id, clk_time now )
          : id_( id )
          , converter_( config_map_ )
          , pos_pid_(
                now,
                em::pid< clk_time >::config{
                    .p   = 0.25f,
                    .i   = 0.015f,
                    .d   = 0.f,
                    .min = 0.f,
                    .max = 1000.f } )
        {
        }

        servo_id get_id() const
        {
                return *id_;
        }

        bool has_task() const
        {
                return !std::holds_alternative< idle_task >( task_ );
        }

        bool is_activating() const
        {
                return std::holds_alternative< activate_task >( task_ );
        }

        const lewan_servo_state& get_state() const
        {
                return *context_;
        }

        void stop( servo_visitor& );

        std::optional< servo_master_variant >
        on_servo_receive( clk_time, s_ctl_id, const servo_receive_variant& );

        void tick( clk_time, servo_visitor& );

        void on_event( ctl_id, clk_time, const servo_events_variant&, servo_visitor& );

        void poll( clk_time, std::size_t, servo_visitor& );

        em::either< servo_events_variant, servo_master_variant >
        on_servo_packet( ctl_id id, const master_servo_variant& subcmd );

        void dump_info();

private:
        void activate_tick( activate_task s, servo_visitor& v );

        template < lewan_message_enum ID, reply Rpl, typename... Args >
        void transmit_servo( servo_visitor& v, Args... args )
        {
                msg_id_counter_++;
                v.to_servo( msg_id_counter_, Rpl, make_master_lewan_message< ID >( id_, args... ) );
        }
};

}  // namespace schpin
