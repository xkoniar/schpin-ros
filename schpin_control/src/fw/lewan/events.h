#include "fw/base.h"
#include "fw/servo/events.h"

#pragma once

namespace schpin
{

struct lewan_goto_event
{
        lewan_angle   g;
        btime         t;
        end_condition end;
};

struct lewan_spin_event
{
        lewan_angular_velocity av;
        btime                  t;
};

using lewan_servo_events_variant =
    std::variant< lewan_goto_event, lewan_spin_event, servo_config_set_event, servo_id_event >;

}  // namespace schpin
