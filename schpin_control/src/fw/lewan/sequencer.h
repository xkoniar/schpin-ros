
#include <emlabcpp/protocol/sequencer.h>

#pragma once

namespace em = emlabcpp;

namespace schpin
{

template < typename Message >
struct lewan_sequencer_def
{
        using message_type = Message;

        static constexpr std::array< uint8_t, 2 > prefix      = { 0x55, 0x55 };
        static constexpr std::size_t              fixed_size  = 4;
        static constexpr std::size_t              buffer_size = Message::max_size * 2;

        static constexpr std::size_t get_size( const auto& bview )
        {
                return bview[3] + 3;
        };
};

template < typename Message >
using lewan_sequencer = em::protocol_sequencer< lewan_sequencer_def< Message > >;

}  // namespace schpin
