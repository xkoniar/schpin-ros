// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/lewan/base.h"

// TODO: redesign including of the emlabcpp/protocol, propably protocl.h which includes what
// necessary?
#include <emlabcpp/algorithm.h>
#include <emlabcpp/either.h>
#include <emlabcpp/protocol/command_group.h>
#include <emlabcpp/protocol/tuple.h>
#include <emlabcpp/static_circular_buffer.h>

#pragma once

// TODO: this has to be gone too :)
namespace em = emlabcpp;

namespace schpin
{
enum lewan_message_enum : uint8_t
{
        LEWAN_SERVO_MOVE_TIME_WRITE      = 1,
        LEWAN_SERVO_MOVE_TIME_READ       = 2,
        LEWAN_SERVO_MOVE_TIME_WAIT_WRITE = 7,
        LEWAN_SERVO_MOVE_TIME_WAIT_READ  = 8,
        LEWAN_SERVO_MOVE_START           = 11,
        LEWAN_SERVO_MOVE_STOP            = 12,
        LEWAN_SERVO_ID_WRITE             = 13,
        LEWAN_SERVO_ID_READ              = 14,
        LEWAN_SERVO_ANGLE_OFFSET_ADJUST  = 17,
        LEWAN_SERVO_ANGLE_OFFSET_WRITE   = 18,
        LEWAN_SERVO_ANGLE_OFFSET_READ    = 19,
        LEWAN_SERVO_ANGLE_LIMIT_WRITE    = 20,
        LEWAN_SERVO_ANGLE_LIMIT_READ     = 21,
        LEWAN_SERVO_VIN_LIMIT_WRITE      = 22,
        LEWAN_SERVO_VIN_LIMIT_READ       = 23,
        LEWAN_SERVO_TEMP_MAX_LIMIT_WRITE = 24,
        LEWAN_SERVO_TEMP_MAX_LIMIT_READ  = 25,
        LEWAN_SERVO_TEMP_READ            = 26,
        LEWAN_SERVO_VIN_READ             = 27,
        LEWAN_SERVO_POS_READ             = 28,
        LEWAN_SERVO_OR_MOTOR_MODE_WRITE  = 29,
        LEWAN_SERVO_OR_MOTOR_MODE_READ   = 30,
        LEWAN_SERVO_LOAD_OR_UNLOAD_WRITE = 31,
        LEWAN_SERVO_LOAD_OR_UNLOAD_READ  = 32,
        LEWAN_SERVO_LED_CTRL_WRITE       = 33,
        LEWAN_SERVO_LED_CTRL_READ        = 34,
        LEWAN_SERVO_LED_ERROR_WRITE      = 35,
        LEWAN_SERVO_LED_ERROR_READ       = 36
};

struct master_lewan_group
  : em::protocol_command_group<
        em::PROTOCOL_LITTLE_ENDIAN,
        em::protocol_command< LEWAN_SERVO_MOVE_TIME_WRITE >::with_args< lewan_angle, lewan_time >,
        em::protocol_command< LEWAN_SERVO_MOVE_TIME_READ >,
        em::protocol_command<
            LEWAN_SERVO_MOVE_TIME_WAIT_WRITE >::with_args< lewan_angle, lewan_time >,
        em::protocol_command< LEWAN_SERVO_MOVE_TIME_WAIT_READ >,
        em::protocol_command< LEWAN_SERVO_MOVE_START >,
        em::protocol_command< LEWAN_SERVO_MOVE_STOP >,
        em::protocol_command< LEWAN_SERVO_ID_WRITE >::with_args< lewan_servo_id >,
        em::protocol_command< LEWAN_SERVO_ID_READ >,
        em::protocol_command< LEWAN_SERVO_ANGLE_OFFSET_ADJUST >::with_args< lewan_angle_offset >,
        em::protocol_command< LEWAN_SERVO_ANGLE_OFFSET_WRITE >,
        em::protocol_command< LEWAN_SERVO_ANGLE_OFFSET_READ >,
        em::protocol_command<
            LEWAN_SERVO_ANGLE_LIMIT_WRITE >::with_args< lewan_angle, lewan_angle >,
        em::protocol_command< LEWAN_SERVO_ANGLE_LIMIT_READ >,
        em::protocol_command<
            LEWAN_SERVO_VIN_LIMIT_WRITE >::with_args< lewan_voltage, lewan_voltage >,
        em::protocol_command< LEWAN_SERVO_VIN_LIMIT_READ >,
        em::protocol_command< LEWAN_SERVO_TEMP_MAX_LIMIT_WRITE >::with_args< lewan_temp >,
        em::protocol_command< LEWAN_SERVO_TEMP_MAX_LIMIT_READ >,
        em::protocol_command< LEWAN_SERVO_TEMP_READ >,
        em::protocol_command< LEWAN_SERVO_VIN_READ >,
        em::protocol_command< LEWAN_SERVO_POS_READ >,
        em::protocol_command< LEWAN_SERVO_OR_MOTOR_MODE_WRITE >::
            with_args< lewan_servo_mode, uint8_t, lewan_angular_velocity >,
        em::protocol_command< LEWAN_SERVO_OR_MOTOR_MODE_READ >,
        em::protocol_command< LEWAN_SERVO_LOAD_OR_UNLOAD_WRITE >::with_args< lewan_load >,
        em::protocol_command< LEWAN_SERVO_LOAD_OR_UNLOAD_READ >,
        em::protocol_command< LEWAN_SERVO_LED_CTRL_WRITE >::with_args< lewan_led_mode >,
        em::protocol_command< LEWAN_SERVO_LED_CTRL_READ >,
        em::protocol_command< LEWAN_SERVO_LED_ERROR_WRITE >::with_args< lewan_led_error >,
        em::protocol_command< LEWAN_SERVO_LED_ERROR_READ > >
{
};

using master_lewan_submessage = master_lewan_group::message_type;
using master_lewan_variant    = master_lewan_group::value_type;

struct lewan_master_group
  : em::protocol_command_group<
        em::PROTOCOL_LITTLE_ENDIAN,
        em::protocol_command< LEWAN_SERVO_MOVE_TIME_READ >::with_args< lewan_angle, lewan_time >,
        em::protocol_command<
            LEWAN_SERVO_MOVE_TIME_WAIT_READ >::with_args< lewan_angle, lewan_time >,
        em::protocol_command< LEWAN_SERVO_ID_READ >::with_args< lewan_servo_id >,
        em::protocol_command< LEWAN_SERVO_ANGLE_OFFSET_READ >::with_args< lewan_angle_offset >,
        em::protocol_command< LEWAN_SERVO_ANGLE_LIMIT_READ >::with_args< lewan_angle, lewan_angle >,
        em::protocol_command<
            LEWAN_SERVO_VIN_LIMIT_READ >::with_args< lewan_voltage, lewan_voltage >,
        em::protocol_command< LEWAN_SERVO_TEMP_MAX_LIMIT_READ >::with_args< lewan_temp >,
        em::protocol_command< LEWAN_SERVO_TEMP_READ >::with_args< lewan_temp >,
        em::protocol_command< LEWAN_SERVO_VIN_READ >::with_args< lewan_voltage >,
        em::protocol_command< LEWAN_SERVO_POS_READ >::with_args< lewan_full_angle >,
        em::protocol_command< LEWAN_SERVO_OR_MOTOR_MODE_READ >::
            with_args< lewan_servo_mode, uint8_t, lewan_angular_velocity >,
        em::protocol_command< LEWAN_SERVO_LOAD_OR_UNLOAD_READ >::with_args< lewan_load >,
        em::protocol_command< LEWAN_SERVO_LED_CTRL_READ >::with_args< lewan_led_mode >,
        em::protocol_command< LEWAN_SERVO_LED_ERROR_READ >::with_args< lewan_led_error > >
{
};

using lewan_master_submessage = lewan_master_group::message_type;
using lewan_master_variant    = lewan_master_group::value_type;

template < typename Msg >
inline uint8_t make_lewan_checksum( const Msg& msg )
{
        unsigned checksum =
            em::sum( em::view{ msg.begin() + 2, msg.end() - 1 }, em::convert_to< unsigned >{} );

        return static_cast< uint8_t >( ~( checksum & 0xff ) );
}

template < typename T >
using lewan_protocol_tuple = em::protocol_tuple<
    em::PROTOCOL_LITTLE_ENDIAN,
    lewan_header_start,
    lewan_header_start,
    lewan_servo_id,
    em::protocol_sized_buffer< em::protocol_offset< lewan_packet_size, 2 >, T >,
    lewan_checksum >;

using lewan_master_protocol = lewan_protocol_tuple< lewan_master_group >;
using lewan_master_value    = lewan_master_protocol::value_type;
using lewan_master_message  = lewan_master_protocol::message_type;

using master_lewan_protocol = lewan_protocol_tuple< master_lewan_group >;
using master_lewan_value    = master_lewan_protocol::value_type;
using master_lewan_message  = master_lewan_protocol::message_type;

master_lewan_message master_lewan_serialize( const master_lewan_value& );
em::either< master_lewan_value, em::protocol_error_record >
                     master_lewan_extract( const master_lewan_message& );
lewan_master_message lewan_master_serialize( const lewan_master_value& );
em::either< lewan_master_value, em::protocol_error_record >
lewan_master_extract( const lewan_master_message& );

template < lewan_message_enum ID, typename... Args >
inline master_lewan_message make_master_lewan_message( lewan_servo_id sid, const Args&... args )
{
        master_lewan_value val = master_lewan_protocol::make_val(
            lewan_header_start_v,
            lewan_header_start_v,
            sid,
            master_lewan_group::make_val< ID >( args... ),
            lewan_checksum{} );

        master_lewan_message msg = master_lewan_serialize( val );

        msg.back() = make_lewan_checksum( msg );

        return msg;
}

}  // namespace schpin
