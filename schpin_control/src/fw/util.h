// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/base.h"

#include <chrono>
#include <cstdint>

#pragma once

namespace schpin
{

inline clk_time get_system_time()
{

        using namespace std::chrono;  // provide custom milliseconds type
        milliseconds ms = duration_cast< milliseconds >( system_clock::now().time_since_epoch() );
        return clk_time{ static_cast< uint32_t >( ms.count() ) };
}

template < typename T >
inline T map_val_impl( T val, T i_from, T i_to, T o_from, T o_to )
{
        return o_from + ( o_to - o_from ) * ( val - i_from ) / ( i_to - i_from );
}

template < typename T, typename FromT, typename ToT >
inline T map_val( FromT val, FromT from, FromT to, ToT desired_from, ToT desired_to )
{
        return map_val_impl(
            static_cast< T >( val ),
            static_cast< T >( from ),
            static_cast< T >( to ),
            static_cast< T >( desired_from ),
            static_cast< T >( desired_to ) );
}

template < typename Bounded >
inline int16_t angle_change( Bounded from, Bounded to )
{
        // TODO: this is hacky as hell, shall be vaporized and rewritten
        int half    = static_cast< int >( Bounded::max_val / 2 );
        int diff    = static_cast< int >( *to > *from ? *to - *from : *from - *to );
        int moddiff = diff % static_cast< int >( Bounded::max_val );
        int res;
        if ( moddiff > half ) {
                res = static_cast< int >( Bounded::max_val ) - moddiff;
                if ( *to > *from ) {
                        res *= -1;
                }
        } else {
                res = moddiff;
                if ( *from > *to ) {
                        res *= -1;
                }
        }
        return static_cast< int16_t >( res );
}

template < typename Bounded >
inline int16_t angle_dist( Bounded a, Bounded b )
{
        return static_cast< int16_t >( abs( angle_change( a, b ) ) );
}

template < typename Group >
class message_catcher
{

        uint32_t id_      = 0;
        bool     catched_ = true;

public:
        template < typename Variant >
        void check( const Variant& var )
        {
                std::visit(
                    [&]( const auto& tpl ) {
                            using cmd_type = std::decay_t< decltype( std::get< 0 >( tpl ) ) >;
                            if ( cmd_type::value == id_ ) {
                                    catched_ = true;
                            }
                    },
                    var );
        }

        bool catched() const
        {
                return catched_;
        }

        void set_trap( uint32_t id )
        {
                id_      = id;
                catched_ = false;
        }
};

class property_timer
{
        uint32_t update_time_;
        uint32_t last_time_diff_;
        uint32_t last_update_ = 0;

public:
        property_timer( uint32_t update_time )
          : update_time_( update_time )
        {
        }

        uint32_t get_last_time_diff() const
        {
                return last_time_diff_;
        }

        // TODO: Function is not proper C++ term -> Callable
        template < typename Function >
        void repeadly( uint32_t clock_time, Function&& f )
        {
                if ( clock_time >= last_update_ + update_time_ ) {
                        f();
                        last_time_diff_ = clock_time - last_update_;
                        last_update_    = clock_time;
                }
        }
};

template < std::size_t N >
constexpr float bezier( float x, const std::array< float, N >& P )
{
        constexpr std::size_t n = N - 1;

        // generates binomial sequence at compile time
        constexpr auto binseq = []() {
                std::array< float, N > fac{};
                fac[0] = 1.0f;
                for ( std::size_t i = 1; i < N; i++ ) {
                        fac[i] = static_cast< float >( i ) * fac[i - 1];
                }
                std::array< float, N > bins{};
                for ( std::size_t i = 0; i < N; i++ ) {
                        bins[i] = fac[n] / ( fac[i] * fac[n - i] );
                }
                return bins;
        };
        constexpr std::array< float, N > bins = binseq();

        // calculate priority of each point
        std::array< float, N > prios;
        float                  j = 1;
        for ( std::size_t i = 0; i < N; ++i ) {
                prios[i] = j;
                j *= x;
        }
        j = 1;
        for ( std::size_t i = 0; i < N; ++i ) {
                prios[n - i] *= j;
                j *= 1 - x;
        }

        // tadaaah, get the result
        float res = 0.0f;
        for ( std::size_t i = 0; i < N; i++ ) {
                res += bins[i] * prios[i] * P[i];
        }
        return res;
}

}  // namespace schpin
