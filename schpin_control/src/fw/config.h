// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <emlabcpp/protocol/register_map.h>

#pragma once

namespace schpin
{
enum fw_config_enum : uint8_t
{
        INPUT_BUFFER_TELEM_PERIOD_MS = 0,
        EVENT_BUFFER_TELEM_PERIOD_MS = 1
};

using fw_config_map_def = em::protocol_register_map<
    em::PROTOCOL_BIG_ENDIAN,
    em::protocol_reg< INPUT_BUFFER_TELEM_PERIOD_MS, clk_time >,
    em::protocol_reg< EVENT_BUFFER_TELEM_PERIOD_MS, clk_time > >;

struct fw_config_map : fw_config_map_def
{
        fw_config_map()
          : fw_config_map_def( clk_time::from_float( 0.5f ), clk_time::from_float( 0.5f ) )
        {
        }
};

}  // namespace schpin
