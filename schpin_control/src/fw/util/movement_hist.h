// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace schpin
{

/* stores history of movement changes */
template < std::size_t BufferSize, std::size_t WindowSize >
class movement_history
{
        static constexpr clk_time slot_period = clk_time::from_float(
            static_cast< float >( WindowSize ) / static_cast< float >( BufferSize ) );

        em::static_circular_buffer< int16_t, BufferSize > buffer_;
        clk_time         last_slot_time_ = clk_time::from_float( 0.f );
        lewan_full_angle last_pos_       = lewan_full_angle::get< 0 >();

public:
        movement_history()
        {
                for ( std::size_t i = 0; i < buffer_.max_size(); ++i ) {
                        buffer_.push_back( 0u );
                }
        }

        void take_reading( clk_time t, lewan_full_angle pos )
        {
                while ( t > last_slot_time_ + slot_period ) {
                        buffer_.pop_front();
                        buffer_.push_back( 0u );
                        last_slot_time_ += slot_period;
                }
                buffer_.back() =
                    static_cast< int16_t >( buffer_.back() + angle_dist( last_pos_, pos ) );
                last_pos_ = pos;
        }

        int16_t get_last() const
        {
                return buffer_.back();
        }

        int32_t get_sum() const
        {
                return em::sum( buffer_ );
        }
};

}  // namespace schpin
