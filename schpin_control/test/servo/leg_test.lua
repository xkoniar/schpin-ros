require "math"

function on_cmd (sid, arg)
    print(sid..","..arg[1]..","..arg[2] ..",".. arg[3]) 
end

mov_t = 1.0
t = time()

msg{id=2, json={"CONFIG_SET",12,{1,0,0,0,0,0,0,0}}}
msg{id=3, json={"CONFIG_SET",12,{1,0,0,0,0,0,0,0}}}

pos2 = msg{id = 2, json = {"POSITION"}}[2]
pos3 = msg{id = 3, json = {"POSITION"}}[2]

for i, g in ipairs({{pos2+0.4,pos3+0.4},{pos2-0.4,pos3-0.4},{pos2,pos3}}) do

    t = time()
    msg {id = 2, json = {"GOTO", g[1], mov_t, g[2]}}
    msg {id = 3, json = {"GOTO", g[1], mov_t, g[2]}}
    while time() - t < mov_t do 
        pos2 = msg{id = 2, json = {"POSITION"}}[2]
        pos3 = msg{id = 3, json = {"POSITION"}}[2]
        coroutine.yield() 
    end
    
end

