require "math"

print_config {id = 1};

pi = 3.141592

c = pi

-- testing_poses = {4, 0, 6.2, 0, 3.,3.1,2.9,3.2,2.8,0}
testing_poses = {c, c - 0.2, c + 0.2, c, c + 0.4, c, c - 0.4, c}
    
msg{id = 1, json = {"GOTO", c, 4, c}}
pos = 0.0
while math.abs(pos-c) > 0.1 do
    coroutine.yield()
    pos = msg{id = 1, json = {"POSITION"}}[2]
end

for i, expected in ipairs(testing_poses) do
    mov_t = 1
    end_tol = 0.2
    after_t = 0.2
    after_tol = 0.1

    t = time()
    print(t, " sending goto ", expected)
    resp = msg {id = 1, json = {"GOTO", expected, mov_t, expected}}
    while time() - t < mov_t do
        coroutine.yield()
        pos = msg{id = 1, json = {"POSITION"}}[2]
    end
    pos = msg{id = 1, json = {"POSITION"}}[2]
    assert(math.abs(pos - expected) < end_tol,
           "GOTO is not within tolerable range at the end time, actual: " .. pos ..
               " expected: " .. expected .. " tolerance: " .. end_tol .. " time: " .. time())

    while time() - t < mov_t + after_t do
        coroutine.yield()
        pos = msg{id = 1, json = {"POSITION"}}[2]
    end

    pos = msg{id = 1, json = {"POSITION"}}[2]
    assert(math.abs(pos - expected) < after_tol,
           "GOTO didn't finished in desired pos, actual: " .. pos ..
               " expected: " .. expected .. " tolerance: " .. after_tol);
end
