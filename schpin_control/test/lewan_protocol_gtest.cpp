// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/lewan/protocol.h"
#include "fw/lewan/sequencer.h"

#include <emlabcpp/protocol/streams.h>
#include <emlabcpp/zip.h>
#include <gtest/gtest.h>
#include <schpin_lib/logging.h>
#include <schpin_lib/view.h>

using namespace schpin;

TEST( lewan_protocol, master_lewan )
{

        master_lewan_message msg =
            make_master_lewan_message< LEWAN_SERVO_POS_READ >( lewan_servo_id::get< 8 >() );
        master_lewan_message expected{
            std::array< uint8_t, 6 >{ 0x55, 0x55, 8, 3, LEWAN_SERVO_POS_READ, 216 } };

        EXPECT_EQ( msg, expected );

        master_lewan_extract( msg ).match(
            [&]( std::tuple<
                 lewan_header_start,
                 lewan_header_start,
                 lewan_servo_id,
                 master_lewan_variant,
                 lewan_checksum > pack ) {
                    auto id         = std::get< lewan_servo_id >( pack );
                    auto parsed_var = std::get< master_lewan_variant >( pack );
                    EXPECT_EQ( id, lewan_servo_id::get< 8 >() );

                    bool cmd_tuple =
                        std::holds_alternative< std::tuple< em::tag< LEWAN_SERVO_POS_READ > > >(
                            parsed_var );
                    EXPECT_TRUE( cmd_tuple );
            },
            [&]( auto e ) {
                    FAIL() << e;
            } );
}

TEST( lewan_protocol, lewan_master )
{
        auto val =
            lewan_master_group::make_val< LEWAN_SERVO_POS_READ >( lewan_angle::get< 500 >() );

        lewan_master_message msg = lewan_master_serialize( lewan_master_protocol::make_val(
            lewan_header_start_v,
            lewan_header_start_v,
            lewan_servo_id::get< 8 >(),
            val,
            lewan_checksum{ 225 } ) );
        lewan_master_message expected{
            std::array< uint8_t, 8 >{ 0x55, 0x55, 8, 5, LEWAN_SERVO_POS_READ, 244, 1, 225 } };

        EXPECT_EQ( msg, expected );

        lewan_master_extract( msg ).match(
            [&]( std::tuple<
                 lewan_header_start,
                 lewan_header_start,
                 lewan_servo_id,
                 lewan_master_variant,
                 lewan_checksum > pack ) {
                    EXPECT_EQ( std::get< lewan_servo_id >( pack ), lewan_servo_id::get< 8 >() );
            },
            [&]( auto e ) {
                    FAIL() << e;
            } );
}

TEST( lewan_protocol, full_range )
{
        // this is message setn by servo, not valid based on doc.
        lewan_master_message valid{
            std::array< uint8_t, 8 >{ 0x55, 0x55, 3, 5, 28, 103, 255, 117 } };

        lewan_master_extract( valid ).match(
            [&]( auto ) {},
            [&]( auto e ) {
                    FAIL() << e;
            } );
}

struct lewan_protocol_sequencer : public ::testing::Test
{
protected:
        using seqe = lewan_sequencer< lewan_master_message >;

        lewan_master_message       msg;
        em::view< const uint8_t* > mview;
        seqe                       seqen{};

        void SetUp() override
        {
                msg   = lewan_master_serialize( lewan_master_protocol::make_val(
                    lewan_header_start_v,
                    lewan_header_start_v,
                    lewan_servo_id::get< 1 >(),
                    lewan_master_group::make_val< LEWAN_SERVO_POS_READ >(
                        lewan_angle::get< 500 >() ),
                    lewan_checksum{} ) );
                mview = em::view{ msg };
                seqen = seqe{};
        }

        void test_sequence( std::vector< em::view< const uint8_t* > > steps )
        {
                em::either< std::size_t, lewan_master_message > ret_either{ 0u };
                EXPECT_FALSE( steps.empty() );
                for ( auto [i, sview] : em::enumerate( steps ) ) {
                        ret_either = seqen.load_data( sview );
                        if ( i != steps.size() - 1 ) {
                                ret_either.match(
                                    [&]( std::size_t ) {},
                                    [&]( lewan_master_message ) {
                                            FAIL() << "Message arrived too soon";
                                    } );
                        } else {
                                ret_either.match(
                                    [&]( std::size_t ) {
                                            FAIL()
                                                << "Read requested in a step where it should not be requested";
                                    },
                                    [&]( lewan_master_message msg2 ) {
                                            EXPECT_EQ( msg, msg2 );
                                    } );
                        }
                }
        }
};

TEST_F( lewan_protocol_sequencer, simple )
{
        test_sequence(
            { em::view{ mview.begin(), mview.begin() + 4 },
              em::view{ mview.begin() + 4, mview.end() } } );
}

TEST_F( lewan_protocol_sequencer, split_header )
{
        // we move the start of the packe by two bytes, and the start of the packet from actual
        // mview
        std::array< uint8_t, 4 > buffer_ = {
            0x34, 0x22, *lewan_header_start_v, *lewan_header_start_v };

        test_sequence(
            { em::view{ buffer_ },
              em::view{ mview.begin() + 2, mview.begin() + 4 },
              em::view{ mview.begin() + 4, mview.end() } } );
}

TEST_F( lewan_protocol_sequencer, border_split_header )
{
        // we move the start of the packe by three bytes
        std::array< uint8_t, 4 > buffer_ = { 0x34, 0x22, 0x11, *lewan_header_start_v };

        test_sequence(
            { em::view{ buffer_ },
              em::view{ mview.begin() + 1, mview.begin() + 4 },
              em::view{ mview.begin() + 4, mview.end() } } );
}

int main( int argc_in, char** argv_in )
{
        testing::InitGoogleTest( &argc_in, argv_in );

        return RUN_ALL_TESTS();
}
