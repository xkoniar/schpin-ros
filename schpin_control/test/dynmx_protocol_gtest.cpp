// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "fw/dynmx/protocol.h"
#include "fw/dynmx/sequencer.h"

#include <emlabcpp/iterators/convert.h>
#include <emlabcpp/protocol/streams.h>
#include <emlabcpp/zip.h>
#include <gtest/gtest.h>
#include <schpin_lib/view.h>

using namespace schpin;

TEST( dynamixel_protocol, master_dynmx_group )
{
        std::array< uint8_t, 4 > buffer{ 0x00, 0x02, 0x00, 0x00 };

        master_dynmx_message msg = make_master_dynmx_msg< DYNMX_WRITE >(
            dynmx_servo_id::get< 0 >(), dynmx_addr{ 0x0074 }, dynmx_buffer{ buffer } );

        master_dynmx_extract( msg ).match(
            [&](
                std::tuple< dynmx_header, dynmx_servo_id, master_dynmx_variant, dynmx_crc > pack ) {
                    using desired_alternative =
                        std::tuple< em::tag< DYNMX_WRITE >, dynmx_addr, dynmx_buffer >;
                    auto md_var = std::get< master_dynmx_variant >( pack );
                    bool holds_right_alternative =
                        std::holds_alternative< desired_alternative >( md_var );
                    EXPECT_TRUE( holds_right_alternative );
            },
            [&]( em::protocol_error_record rec ) {
                    FAIL() << rec << "\n" << msg;
            } );
}

TEST( dynamixel_protocol, master_dynmx )
{
        std::array< uint8_t, 4 > buffer{ 0x00, 0x02, 0x00, 0x00 };

        master_dynmx_message msg = make_master_dynmx_msg< DYNMX_WRITE >(
            dynmx_servo_id::get< 1 >(), dynmx_addr{ 0x0074 }, dynmx_buffer{ buffer } );

        master_dynmx_message expected{ std::array< uint8_t, 16 >{
            0xFF,
            0xFF,
            0xFD,
            0x00,
            0x01,
            0x09,
            0x00,
            0x03,
            0x74,
            0x00,
            0x00,
            0x02,
            0x00,
            0x00,
            0xCA,
            0x89 } };

        EXPECT_EQ( msg, expected ) << std::hex << em::convert_view< int >( msg ) << "\n"
                                   << convert_view< int >( expected );

        master_dynmx_extract( msg ).match(
            [&]( std::tuple< dynmx_header, dynmx_servo_id, master_dynmx_variant, dynmx_crc > ) {},
            [&]( em::protocol_error_record rec ) {
                    FAIL() << rec << "\n" << msg;
            } );
}

TEST( dynamixel_protocol, dynmx_master )
{
        dynmx_master_message msg{ std::array< uint8_t, 12 >{
            0xff, 0xff, 0xfd, 0x0, 0x1, 0x5, 0x0, 0x55, 0x0, 0x0, 0x53, 0x21 } };

        dynmx_master_extract( msg ).match(
            [&]( auto ) {},
            [&]( auto e ) {
                    FAIL() << e << "\n" << em::convert_view< int >( msg );
            } );
}

// TODO: duplication for same dynmx test...
struct dynmx_protocol_sequencer : public ::testing::Test
{
protected:
        using seqe = dynmx_sequencer< master_dynmx_message >;

        master_dynmx_message       msg;
        em::view< const uint8_t* > mview;
        seqe                       seqen{};

        void SetUp() override
        {
                msg   = make_master_dynmx_msg< DYNMX_PING >( dynmx_servo_id::get< 0 >() );
                mview = em::view{ msg };
                seqen = seqe{};
        }

        void test_sequence( std::vector< em::view< const uint8_t* > > steps )
        {
                em::either< std::size_t, master_dynmx_message > ret_either{ 0u };
                EXPECT_FALSE( steps.empty() );

                for ( auto [i, sview] : em::enumerate( steps ) ) {
                        ret_either = seqen.load_data( sview );
                        if ( i != steps.size() - 1 ) {
                                ret_either.match(
                                    [&]( std::size_t req ) {
                                            EXPECT_EQ( req, steps[i + 1].size() );
                                    },
                                    [&]( master_dynmx_message ) {
                                            FAIL() << "Message arrived too soon";
                                    } );
                        } else {
                                ret_either.match(
                                    [&]( std::size_t ) {
                                            FAIL()
                                                << "Read requested in a step where it should not be requested";
                                    },
                                    [&]( master_dynmx_message msg2 ) {
                                            EXPECT_EQ( msg, msg2 );
                                    } );
                        }
                }
        }
};

static constexpr std::size_t header_size = dynmx_sequencer< master_dynmx_message >::fixed_size;
static constexpr std::size_t header_start_size =
    dynmx_sequencer< master_dynmx_message >::prefix.size();

TEST_F( dynmx_protocol_sequencer, simple )
{
        test_sequence(
            { em::view{ mview.begin(), mview.begin() + header_size },
              em::view{ mview.begin() + header_size, mview.end() } } );
}

int main( int argc_in, char** argv_in )
{
        testing::InitGoogleTest( &argc_in, argv_in );

        return RUN_ALL_TESTS();
}
