// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "node/leg/ik.h"
#include "node/leg/model.h"

#include <gtest/gtest.h>
#include <schpin_lib/geom/point/rand.h>

using namespace schpin;

schpin::link make_link( std::string name, joint j )
{
        return schpin::link{
            std::move( name ),
            structure_model< robot_frame >{},
            std::make_unique< joint >( std::move( j ) ) };
}

schpin::link make_link( std::string name )
{
        return schpin::link{
            std::move( name ), structure_model< robot_frame >{}, std::unique_ptr< joint >() };
}

joint make_revolute_joint(
    const std::string&  name,
    vec< 3 >            axis,
    pose< robot_frame > offset,
    joint               child )
{
        return joint{
            name + "_joint",
            revolute_joint{
                axis,
                em::angle{ 0.f },
                em::angle{ 2 * 3.141592f },
                0,
                em::angular_velocity{ 1.f } },
            offset,
            make_link( name + "_link", std::move( child ) ) };
}

joint make_revolute_joint( const std::string& name, vec< 3 > axis, pose< robot_frame > offset )
{
        return joint{
            name + "_joint",
            revolute_joint{
                axis, em::angle{ 0.f }, em::angle{ 1.f }, 0, em::angular_velocity{ 1.f } },
            offset,
            make_link( name + "_link" ) };
}

joint make_fixed_joint( const std::string& name, pose< robot_frame > offset, joint child )
{
        return joint{
            name + "_joint",
            fixed_joint{},
            offset,
            make_link( name + "_link", std::move( child ) ) };
}
joint make_fixed_joint( const std::string& name, pose< robot_frame > offset )
{
        return joint{ name + "_link", fixed_joint{}, offset, make_link( name + "_link" ) };
}

using test_pose    = pose< robot_frame >;
using test_quat    = quaternion< robot_frame >;
using test_point   = point< 3, robot_frame >;
using jacob_matrix = typename ik_solver::jacobian_matrix;

joint get_simple_leg()
{
        return make_revolute_joint(
            "root",
            vec< 3 >{ 0, 0, 1 },
            test_pose{},
            make_fixed_joint( "tip_joint", test_pose{ test_point{ 10., 0., 0. } } ) );
}

joint get_3dof_leg()
{
        return make_revolute_joint(  //
            "coxa",
            vec< 3 >{ 0, 0, 1 },
            test_pose{ test_point{ 0.06, -0.052, 0. } },
            make_revolute_joint(  //
                "femur",
                vec< 3 >{ 0, -1, 0 },
                test_pose{ test_point{ 0.065, 0., 0. }, test_quat{ 0., 1., 0., 0. } },
                make_revolute_joint(  //
                    "tibia",
                    vec< 3 >{ 0, 1, 0 },
                    test_pose{
                        test_point{ 0.095, 0., 0.03 }, test_quat{ 0., 0.707107f, 0., -0.707107f } },
                    make_fixed_joint(  //
                        "tip",
                        test_pose{ test_point{ 0.11, 0., 0 } } ) ) ) );
}

TEST( IK, simple_jacob )
{
        joint      root = get_simple_leg();
        leg_config conf;

        leg_model model{ "simple_leg", conf, std::move( root ) };

        ik_solver solver{ pose< robot_frame >{}, model.get_revolute_mapping() };

        dof_array< em::angle > angles{ dynamic_size::token{ 1 } };
        angles[0]          = em::angle{ 0.f };
        auto         jacob = solver.ik_get_jacobian( angles );
        jacob_matrix expected{
            static_size< 3 >::token{}, dynamic_size::token{ 1 }, { 0., 10., 0. } };
        EXPECT_EQ( jacob, expected ) << "jacob:\n" << jacob << "\n expected:\n" << expected;
}

TEST( IK, 3DOF_jacob )
{
        joint      root = get_3dof_leg();
        leg_config conf;

        leg_model model{ "3dof_leg", conf, std::move( root ) };
        fk_solver fk{ pose< robot_frame >{}, model.get_revolute_mapping() };
        ik_solver solver{ pose< robot_frame >{}, model.get_revolute_mapping() };

        dof_array< em::angle > angles{
            em::angle{ 0.7f }, em::angle{ 3.141592f }, em::angle{ 3.141592f } };

        vec< 3 > angle_change{ 0.005f, 0.005f, 0.005f };

        auto jacob   = solver.ik_get_jacobian( angles );
        auto tip_pos = fk.get_tip_position( angles );
        for ( std::size_t i : em::range< std::size_t >( 3 ) ) {
                angles[i] += em::angle{ angle_change[i] };
        }
        auto moved_pos = fk.get_tip_position( angles );

        auto     jacob_estimate = jacob * angle_change;
        vec< 3 > jacob_estimate_v;
        for ( std::size_t i : em::range( 3u ) ) {
                jacob_estimate_v[i] = jacob_estimate[i];
        }

        bool are_eq = almost_equal( jacob_estimate_v, moved_pos - tip_pos, 0.00001f );
        EXPECT_TRUE( are_eq ) << "jacob:\n"
                              << jacob << "\njacob_estimate: " << jacob_estimate_v
                              << "\n real_change:" << moved_pos - tip_pos << "\n";

        // so, jacob tells us how tip pos changes based on change in angles
}

TEST( IK, 3DOF_rand )
{
        joint      root = get_3dof_leg();
        leg_config conf;

        leg_model model{ "3dof_leg", conf, std::move( root ) };
        fk_solver fk{ pose< robot_frame >{}, model.get_revolute_mapping() };
        ik_solver solver{ pose< robot_frame >{}, model.get_revolute_mapping() };
        point_generator< 3, robot_frame > gen;
        em::distance                      tolerance{ 0.005f };

        dof_array< em::angle > angles{
            em::angle{ 0.7f }, em::angle{ 3.1415f }, em::angle{ 3.1415f } };

        vec< 3 > center = cast_to_vec( fk.get_tip_position( angles ) );

        for ( std::size_t i : em::range< std::size_t >( 100 ) ) {
                test_point target = gen.get( vec< 3 >{ 0.005f, 0.005f, 0.005f } ) + center;

                solver.calculate_angles( fk, angles, target, tolerance, std::size_t{ 256 } )
                    .match(
                        [&]( auto angs ) {
                                angles = angs;
                        },
                        [&]( ik_error e ) {
                                FAIL() << e;
                        } );

                test_point tip_pos = fk.get_tip_position( angles );

                bool are_eq = almost_equal( tip_pos, target, *tolerance );
                ASSERT_TRUE( are_eq )
                    << "Failed in i: " << i << "\n angles: " << angles << "\n tip_pos:" << tip_pos
                    << "\n target:" << target << "\n center:" << center
                    << "\n distance:" << length_of( target - tip_pos );
        }
}

int main( int argc_in, char** argv_in )
{
        testing::InitGoogleTest( &argc_in, argv_in );

        return RUN_ALL_TESTS();
}
