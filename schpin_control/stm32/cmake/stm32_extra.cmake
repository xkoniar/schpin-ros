
function(add_stm32_executable)

  cmake_parse_arguments(A "" "TARGET" "FILES;LIBS" ${ARGN})

  add_executable(${A_TARGET} ${A_FILES})
  target_link_libraries(${A_TARGET} ${A_LIBS})
  target_link_options(
    ${A_TARGET} PRIVATE "-Wl,-Map=control.map,--cref"
    "-Wl,--print-memory-usage" "-funwind-tables" "-fasynchronous-unwind-tables")

  set(HEX_FILE ${PROJECT_BINARY_DIR}/${A_TARGET}.hex)
  set(BIN_FILE ${PROJECT_BINARY_DIR}/${A_TARGET}.bin)
  add_custom_command(
    TARGET ${A_TARGET}
    POST_BUILD
    COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${A_TARGET}> ${HEX_FILE}
    COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${A_TARGET}> ${BIN_FILE}
    COMMENT "Building ${HEX_FILE} \nBuilding ${BIN_FILE}")

endfunction()
