# Copyright 2021 Schpin
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.11)

include(FetchContent)

project(stm32_hal)
enable_language(C CXX ASM)

# STM32 HAL and CMSIS
string(TOLOWER ${MCU} MCU_LOWER)
string(TOLOWER ${MCU_FAMILY} MCU_FAMILY_LOWER)

FetchContent_Declare(
  hal
  GIT_REPOSITORY https://github.com/STMicroelectronics/STM32CubeG4.git
  GIT_TAG v1.3.0
  GIT_PROGRESS TRUE)

FetchContent_GetProperties(hal)

if(NOT hal_POPULATED)
  FetchContent_Populate(hal)
  # Turn config template into config
  file(GLOB_RECURSE cfgFile "${hal_SOURCE_DIR}/Drivers/*_conf_template.h")
  foreach(f ${cfgFile})
    string(REGEX REPLACE "_template\.h$" ".h" newF ${f})

    file(READ ${f} filedata)
    string(
      REGEX
      REPLACE "#define USE_HAL_UART_REGISTER_CALLBACKS       0U"
              "#define USE_HAL_UART_REGISTER_CALLBACKS       1U" filedata
              "${filedata}")
    file(WRITE ${newF} "${filedata}")
  endforeach(f)
endif()

set(STM_LIB ${hal_SOURCE_DIR}/Drivers)
set(HAL_PATH ${STM_LIB}/${MCU_FAMILY}_HAL_Driver)

add_library(
  startup OBJECT
  ${STM_LIB}/CMSIS/Device/ST/${MCU_FAMILY}/Source/Templates/gcc/startup_${MCU_LOWER}.s
)

add_library(
  CMSIS STATIC
  ${STM_LIB}/CMSIS/Device/ST/${MCU_FAMILY}/Source/Templates/system_${MCU_FAMILY_LOWER}.c
)
target_include_directories(
  CMSIS SYSTEM
  PUBLIC ${STM_LIB}/CMSIS/Include SYSTEM
  PUBLIC ${STM_LIB}/CMSIS/Device/ST/${MCU_FAMILY}/Include)

function(add_hallib libname)
  set(sources "")
  foreach(f ${ARGN})
    list(APPEND sources "${HAL_PATH}/Src/${MCU_FAMILY_LOWER}_${f}")
  endforeach(f)
  add_library(${libname} STATIC ${sources})
  target_include_directories(${libname} SYSTEM PUBLIC ${HAL_PATH}/Inc)
  target_link_libraries(${libname} PUBLIC CMSIS)
  target_compile_options(
    ${libname} PUBLIC -DUSE_FULL_LL_DRIVER -DUSE_HAL_DRIVER -D${MCU_FAMILY} -Og
                      -g)
endfunction()

add_hallib(
  HAL
  hal.c
  hal_rcc.c
  hal_rcc_ex.c
  hal_cortex.c
  hal_uart.c
  hal_uart_ex.c
  hal_gpio.c
  hal_fdcan.c
  hal_i2c.c
  hal_i2c_ex.c
  hal_dma.c
  hal_pwr_ex.c)
add_hallib(HAL_CRC hal_crc.c hal_crc_ex.c)

add_library(stm32_hal STATIC $<TARGET_OBJECTS:startup>)
target_compile_options(stm32_hal PUBLIC -DUSE_FULL_LL_DRIVER -DUSE_HAL_DRIVER
                                        -D${MCU_FAMILY} -Os)
target_link_libraries(stm32_hal PUBLIC HAL HAL_CRC)
