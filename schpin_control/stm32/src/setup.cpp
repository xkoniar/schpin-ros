#include "setup.h"

namespace schpin
{

void setup_oscillators()
{
        RCC_OscInitTypeDef osc_struct{};

        osc_struct.OscillatorType      = RCC_OSCILLATORTYPE_HSI;
        osc_struct.HSIState            = RCC_HSI_ON;
        osc_struct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
        osc_struct.PLL.PLLState        = RCC_PLL_ON;
        osc_struct.PLL.PLLSource       = RCC_PLLSOURCE_HSI;
        osc_struct.PLL.PLLM            = RCC_PLLM_DIV4;
        osc_struct.PLL.PLLN            = 85;
        osc_struct.PLL.PLLP            = RCC_PLLP_DIV2;
        osc_struct.PLL.PLLQ            = RCC_PLLQ_DIV2;
        osc_struct.PLL.PLLR            = RCC_PLLR_DIV2;

        if ( HAL_RCC_OscConfig( &osc_struct ) != HAL_OK ) {
                std::abort();
        }
}

void setup_clock()
{
        RCC_ClkInitTypeDef clk_struct{};

        clk_struct.ClockType =
            RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
        clk_struct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
        clk_struct.AHBCLKDivider  = RCC_SYSCLK_DIV1;
        clk_struct.APB1CLKDivider = RCC_HCLK_DIV1;
        clk_struct.APB2CLKDivider = RCC_HCLK_DIV2;

        if ( HAL_RCC_ClockConfig( &clk_struct, FLASH_LATENCY_4 ) != HAL_OK ) {
                std::abort();
        }
}

void setup_periph_clock()
{
        RCC_PeriphCLKInitTypeDef clk_struct{};

        clk_struct.PeriphClockSelection =
            RCC_PERIPHCLK_USART1 | RCC_PERIPHCLK_USART2 | RCC_PERIPHCLK_I2C1 | RCC_PERIPHCLK_FDCAN;
        clk_struct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
        clk_struct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
        clk_struct.I2c1ClockSelection   = RCC_I2C1CLKSOURCE_PCLK1;

        if ( HAL_RCCEx_PeriphCLKConfig( &clk_struct ) != HAL_OK ) {
                std::abort();
        }
}

void setup_gpio()
{
        __HAL_RCC_GPIOA_CLK_ENABLE();
        __HAL_RCC_GPIOB_CLK_ENABLE();

        HAL_GPIO_WritePin( GPIOB, GPIO_PIN_8, GPIO_PIN_RESET );

        GPIO_InitTypeDef gpio_struct;

        gpio_struct.Pin   = GPIO_PIN_8;
        gpio_struct.Mode  = GPIO_MODE_OUTPUT_PP;
        gpio_struct.Pull  = GPIO_NOPULL;
        gpio_struct.Speed = GPIO_SPEED_FREQ_LOW;

        HAL_GPIO_Init( GPIOB, &gpio_struct );
}

void setup_fdcan( FDCAN_HandleTypeDef& h )
{
        h.Instance                  = FDCAN1;
        h.Init.ClockDivider         = FDCAN_CLOCK_DIV1;
        h.Init.FrameFormat          = FDCAN_FRAME_CLASSIC;
        h.Init.Mode                 = FDCAN_MODE_NORMAL;
        h.Init.AutoRetransmission   = DISABLE;
        h.Init.TransmitPause        = DISABLE;
        h.Init.ProtocolException    = DISABLE;
        h.Init.NominalPrescaler     = 1;
        h.Init.NominalSyncJumpWidth = 1;
        h.Init.NominalTimeSeg1      = 2;
        h.Init.NominalTimeSeg2      = 2;
        h.Init.DataPrescaler        = 1;
        h.Init.DataSyncJumpWidth    = 1;
        h.Init.DataTimeSeg1         = 1;
        h.Init.DataTimeSeg2         = 1;
        h.Init.StdFiltersNbr        = 0;
        h.Init.ExtFiltersNbr        = 0;
        h.Init.TxFifoQueueMode      = FDCAN_TX_FIFO_OPERATION;

        if ( HAL_FDCAN_Init( &h ) != HAL_OK ) {
                std::abort();
        }
}

void setup_i2c( I2C_HandleTypeDef& h )
{
        h.Instance              = I2C1;
        h.Init.Timing           = 0x30A0A7FB;
        h.Init.OwnAddress1      = 0;
        h.Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
        h.Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
        h.Init.OwnAddress2      = 0;
        h.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
        h.Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
        h.Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;

        if ( HAL_I2C_Init( &h ) != HAL_OK ) {
                std::abort();
        }
        if ( HAL_I2CEx_ConfigAnalogFilter( &h, I2C_ANALOGFILTER_ENABLE ) != HAL_OK ) {
                std::abort();
        }
        if ( HAL_I2CEx_ConfigDigitalFilter( &h, 0 ) != HAL_OK ) {
                std::abort();
        }
}

void prepare_half_duplex_uart( UART_HandleTypeDef& h )
{
        h.Instance                    = USART1;
        h.Init.BaudRate               = 57600;
        h.Init.WordLength             = UART_WORDLENGTH_8B;
        h.Init.StopBits               = UART_STOPBITS_1;
        h.Init.Parity                 = UART_PARITY_NONE;
        h.Init.Mode                   = UART_MODE_TX_RX;
        h.Init.HwFlowCtl              = UART_HWCONTROL_NONE;
        h.Init.OverSampling           = UART_OVERSAMPLING_16;
        h.Init.OneBitSampling         = UART_ONE_BIT_SAMPLE_DISABLE;
        h.Init.ClockPrescaler         = UART_PRESCALER_DIV1;
        h.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
}

void initialize_half_duplex_uart( UART_HandleTypeDef& h )
{
        if ( HAL_HalfDuplex_Init( &h ) != HAL_OK ) {
                std::abort();
        }
        if ( HAL_UARTEx_SetTxFifoThreshold( &h, UART_TXFIFO_THRESHOLD_1_8 ) != HAL_OK ) {
                std::abort();
        }
        if ( HAL_UARTEx_SetRxFifoThreshold( &h, UART_RXFIFO_THRESHOLD_1_8 ) != HAL_OK ) {
                std::abort();
        }
        if ( HAL_UARTEx_DisableFifoMode( &h ) != HAL_OK ) {
                std::abort();
        }
}

void setup_lewan_uart( UART_HandleTypeDef& h )
{
        prepare_half_duplex_uart( h );

        h.Init.BaudRate = 115200;

        initialize_half_duplex_uart( h );
}

void setup_dynmx_uart( UART_HandleTypeDef& h )
{
        prepare_half_duplex_uart( h );

        h.Init.BaudRate = 57600;

        initialize_half_duplex_uart( h );
}

void setup_com_uart( UART_HandleTypeDef& h )
{
        h.Instance                    = USART2;
        h.Init.BaudRate               = 115200;
        h.Init.WordLength             = UART_WORDLENGTH_8B;
        h.Init.StopBits               = UART_STOPBITS_1;
        h.Init.Parity                 = UART_PARITY_NONE;
        h.Init.Mode                   = UART_MODE_TX_RX;
        h.Init.HwFlowCtl              = UART_HWCONTROL_NONE;
        h.Init.OverSampling           = UART_OVERSAMPLING_16;
        h.Init.OneBitSampling         = UART_ONE_BIT_SAMPLE_DISABLE;
        h.Init.ClockPrescaler         = UART_PRESCALER_DIV1;
        h.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;

        if ( HAL_UART_Init( &h ) != HAL_OK ) {
                std::abort();
        }
        if ( HAL_UARTEx_SetTxFifoThreshold( &h, UART_TXFIFO_THRESHOLD_1_8 ) != HAL_OK ) {
                std::abort();
        }
        if ( HAL_UARTEx_SetRxFifoThreshold( &h, UART_RXFIFO_THRESHOLD_1_8 ) != HAL_OK ) {
                std::abort();
        }
        if ( HAL_UARTEx_DisableFifoMode( &h ) != HAL_OK ) {
                std::abort();
        }
}

void setup_system( perif_handlers& h, servo_type stype )
{
        HAL_Init();

        setup_oscillators();
        setup_clock();
        setup_periph_clock();

        setup_gpio();

        switch ( stype ) {
                case LEWAN:
                case LEWAN_TEST:
                        setup_lewan_uart( h.bus_uart );
                        break;
                case DYNMX:
                        setup_dynmx_uart( h.bus_uart );
                        break;
        }
        setup_fdcan( h.can );
        setup_i2c( h.i2c );
        setup_com_uart( h.com_uart );
}

}  // namespace schpin
