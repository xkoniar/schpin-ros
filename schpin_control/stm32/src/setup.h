#include "util.h"

#include <cstdlib>
#include <cstring>
#include <stm32g4xx_hal.h>

#pragma once

namespace schpin
{

struct perif_handlers
{
        FDCAN_HandleTypeDef can{};
        I2C_HandleTypeDef   i2c{};
        UART_HandleTypeDef  bus_uart{};
        UART_HandleTypeDef  com_uart{};
};

void setup_system( perif_handlers& h, servo_type stype );

}  // namespace schpin
