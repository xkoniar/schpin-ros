
#include "fw/dynmx/protocol.h"
#include "fw/dynmx/sequencer.h"
#include "fw/lewan/protocol.h"
#include "fw/lewan/sequencer.h"
#include "it.h"
#include "main.h"
#include "setup.h"
#include "stm32g4xx_hal.h"

using namespace schpin;

#ifndef BRIDGE_TYPE
#error You have to define bridge type
#endif

namespace lewan_bridge
{
using servo_master_sequencer      = lewan_sequencer< lewan_master_message >;
using master_servo_sequencer      = lewan_sequencer< master_lewan_message >;
static constexpr servo_type STYPE = LEWAN;
}  // namespace lewan_bridge
namespace dynmx_bridge
{
using servo_master_sequencer      = dynmx_sequencer< dynmx_master_message >;
using master_servo_sequencer      = dynmx_sequencer< master_dynmx_message >;
static constexpr servo_type STYPE = DYNMX;
}  // namespace dynmx_bridge

using namespace BRIDGE_TYPE;  // specify proper namespace for bridge

perif_handlers HANDLERS;

using servo_master_message = typename servo_master_sequencer::message_type;
servo_master_sequencer                                SM_SEQ;
std::array< uint8_t, servo_master_message::max_size > SM_BUFF;
servo_master_message                                  SM_OUT;

using master_servo_message = typename master_servo_sequencer::message_type;
master_servo_sequencer                                MS_SEQ;
std::array< uint8_t, master_servo_message::max_size > MS_BUFF;
master_servo_message                                  MS_OUT;

void __gnu_cxx::__verbose_terminate_handler()
{
        std::abort();
}

void start_cycle()
{
        HAL_Checker <<= HAL_UART_Receive_IT(
            &HANDLERS.com_uart,
            MS_BUFF.data(),
            static_cast< uint16_t >( master_servo_sequencer::fixed_size ) );
}

int main()
{
        setup_system( HANDLERS, STYPE );

        HAL_UART_RegisterCallback(
            &HANDLERS.bus_uart, HAL_UART_TX_COMPLETE_CB_ID, []( UART_HandleTypeDef* ) {
                    HAL_Checker <<= HAL_HalfDuplex_EnableReceiver( &HANDLERS.bus_uart );
                    HAL_Checker <<= HAL_UART_Receive_IT(
                        &HANDLERS.bus_uart,
                        SM_BUFF.data(),
                        static_cast< uint16_t >( servo_master_sequencer::fixed_size ) );
            } );
        HAL_UART_RegisterCallback(
            &HANDLERS.bus_uart, HAL_UART_RX_COMPLETE_CB_ID, []( UART_HandleTypeDef* u ) {
                    SM_SEQ.load_data( em::view_n( SM_BUFF.begin(), u->RxXferSize ) )
                        .match(
                            [&]( std::size_t req ) {
                                    HAL_Checker <<= HAL_UART_Receive_IT(
                                        &HANDLERS.bus_uart,
                                        SM_BUFF.data(),
                                        static_cast< uint16_t >( req ) );
                            },
                            [&]( const servo_master_message& m ) {
                                    SM_OUT = m;
                                    HAL_Checker <<= HAL_UART_Transmit_IT(
                                        &HANDLERS.com_uart,
                                        SM_OUT.begin(),
                                        static_cast< uint16_t >( SM_OUT.size() ) );
                            } );
            } );
        HAL_UART_RegisterCallback(
            &HANDLERS.bus_uart, HAL_UART_ERROR_CB_ID, []( UART_HandleTypeDef* ) {
                    std::abort();
            } );

        HAL_UART_RegisterCallback(
            &HANDLERS.com_uart, HAL_UART_TX_COMPLETE_CB_ID, []( UART_HandleTypeDef* ) {} );
        HAL_UART_RegisterCallback(
            &HANDLERS.com_uart, HAL_UART_RX_COMPLETE_CB_ID, []( UART_HandleTypeDef* u ) {
                    MS_SEQ.load_data( em::view_n( MS_BUFF.begin(), u->RxXferSize ) )
                        .match(
                            [&]( std::size_t req ) {
                                    HAL_Checker <<= HAL_UART_Receive_IT(
                                        &HANDLERS.com_uart,
                                        MS_BUFF.data(),
                                        static_cast< uint16_t >( req ) );
                            },
                            [&]( const master_servo_message& m ) {
                                    start_cycle();
                                    MS_OUT = m;
                                    if ( HAL_UART_GetState( &HANDLERS.bus_uart ) !=
                                         HAL_UART_STATE_READY ) {
                                            HAL_Checker <<= HAL_UART_Abort( &HANDLERS.bus_uart );
                                    }
                                    HAL_Checker <<=
                                        HAL_HalfDuplex_EnableTransmitter( &HANDLERS.bus_uart );
                                    HAL_Checker <<= HAL_UART_Transmit_IT(
                                        &HANDLERS.bus_uart,
                                        MS_OUT.begin(),
                                        static_cast< uint16_t >( MS_OUT.size() ) );
                            } );
            } );
        HAL_UART_RegisterCallback(
            &HANDLERS.com_uart, HAL_UART_ERROR_CB_ID, []( UART_HandleTypeDef* ) {
                    std::abort();
            } );

        start_cycle();
        while ( true ) {
                asm( "nop" );
        }
}
