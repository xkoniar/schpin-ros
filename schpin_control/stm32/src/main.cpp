// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "main.h"

#include "fw/reactor.h"
#include "it.h"
#include "stm32g4xx_hal.h"
#include "util.h"

using namespace schpin;

using reactor = fw_reactor< 20 >;

perif_handlers HANDLERS;
reactor        REACTOR;

struct stm32_commands : fw_reactor_visitor
{

        void to_control( const fw_node_message& msg )
        {
                HAL_UART_Transmit(
                    &HANDLERS.com_uart,
                    const_cast< uint8_t* >( msg.begin() ),
                    static_cast< uint16_t >( msg.size() ),
                    200 );
        }
        void to_servo( s_ctl_id, reply, const master_dynmx_message& )
        {
        }
        void to_servo( s_ctl_id msg_id, reply r, const master_lewan_message& m )
        {
                HAL_UART_Transmit(
                    &HANDLERS.bus_uart,
                    const_cast< uint8_t* >( m.begin() ),
                    static_cast< uint16_t >( m.size() ),
                    200 );
                if ( r == reply::NO ) {
                        return;
                }

                std::array< uint8_t, 10 > data;

                HAL_UART_Receive(
                    &HANDLERS.bus_uart,
                    const_cast< uint8_t* >( data.begin() ),
                    static_cast< uint16_t >( data.size() ),
                    200 );

                REACTOR.on_servo_receive(
                    get_systime(), msg_id, lewan_master_message{ data }, *this );
        }
};

void* operator new( std::size_t )
{
        std::abort();
}
void* operator new( unsigned int, std::align_val_t )
{
        std::abort();
}
void operator delete( void* )
{
        std::abort();
}
void operator delete( void*, std::size_t )
{
        std::abort();
}

void __gnu_cxx::__verbose_terminate_handler()
{
        std::abort();
}

int main()
{
        setup_system( HANDLERS, LEWAN );
        stm32_commands vis{};
        while ( 1 ) {
                REACTOR.tick( get_systime(), vis );
        }
}
