#include "fw/base.h"

#include <stm32g4xx_hal.h>

#pragma once

namespace schpin
{

struct HAL_Checker_Impl
{
};

static constexpr HAL_Checker_Impl HAL_Checker;

inline void operator<<=( const HAL_Checker_Impl&, const HAL_StatusTypeDef& v )
{
        if ( v != HAL_OK ) {
                std::abort();
        }
}

template < typename T >
inline void zero_struct( T* val )
{
        memset( val, 0, sizeof( T ) );
}

inline clk_time get_systime()
{
        return clk_time{ HAL_GetTick() };
}

}  // namespace schpin
