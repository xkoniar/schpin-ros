#include "it.h"

#include "main.h"

/**
 * @brief This function handles Non maskable interrupt.
 */
extern "C" void NMI_Handler( void )
{
}

extern "C" void WWDG_IRQHandler( void )
{
        while ( 1 ) {
                asm( "nop" );
        }
}

extern "C" void ReportHardFault( uint32_t* hard_fault_values, uint32_t exc );

/**
 * @brief This function handles Hard fault interrupt.
 */
extern "C" void HardFault_Handler( void )
{
        __asm volatile( "TST    LR, #0b0100;      "
                        "ITE    EQ;               "
                        "MRSEQ  R0, MSP;          "
                        "MRSNE  R0, PSP;          "
                        "MOV    R1, LR;           "
                        "B      ReportHardFault;  " );
}

extern "C" void ReportHardFault( uint32_t* stack_frame, uint32_t exc )
{
        uint32_t r0   = stack_frame[0];
        uint32_t r1   = stack_frame[1];
        uint32_t r2   = stack_frame[2];
        uint32_t r3   = stack_frame[3];
        uint32_t r12  = stack_frame[4];
        uint32_t lr   = stack_frame[5];
        uint32_t pc   = stack_frame[6];
        uint32_t psr  = stack_frame[7];
        uint32_t hfsr = SCB->HFSR;
        uint32_t cfsr = SCB->CFSR;
        uint32_t mmar = SCB->MMFAR;
        uint32_t bfar = SCB->BFAR;
        uint32_t afsr = SCB->AFSR;

        printf( "\n!!!Hard Fault detected!!!\n" );

        printf( "\nStack frame:\n" );
        printf( "R0 :        0x%08lX\n", r0 );
        printf( "R1 :        0x%08lX\n", r1 );
        printf( "R2 :        0x%08lX\n", r2 );
        printf( "R3 :        0x%08lX\n", r3 );
        printf( "R12:        0x%08lX\n", r12 );
        printf( "LR :        0x%08lX\n", lr );
        printf( "PC :        0x%08lX\n", pc );
        printf( "PSR:        0x%08lX\n", psr );

        printf( "\nFault status:\n" );
        printf( "HFSR:       0x%08lX\n", hfsr );
        printf( "CFSR:       0x%08lX\n", cfsr );
        printf( "MMAR:       0x%08lX\n", mmar );
        printf( "BFAR:       0x%08lX\n", bfar );
        printf( "AFSR:       0x%08lX\n", afsr );

        printf( "\nOther:\n" );
        printf( "EXC_RETURN: 0x%08lX\n", exc );

        /* Breakpoint. */
        __asm volatile( "BKPT #0" );

        /* Infinite loop to stop the execution. */
        while ( 1 ) {
                asm( "nop" );
        }
}

/**
 * @brief This function handles Memory management fault.
 */
extern "C" void MemManage_Handler( void )
{
        while ( 1 ) {
                asm( "nop" );
        }
}

/**
 * @brief This function handles Prefetch fault, memory access fault.
 */
extern "C" void BusFault_Handler( void )
{
        while ( 1 ) {
                asm( "nop" );
        }
}

/**
 * @brief This function handles Undefined instruction or illegal state.
 */
extern "C" void UsageFault_Handler( void )
{
        while ( 1 ) {
                asm( "nop" );
        }
}

/**
 * @brief This function handles System service call via SWI instruction.
 */
extern "C" void SVC_Handler( void )
{
}

/**
 * @brief This function handles Debug monitor.
 */
extern "C" void DebugMon_Handler( void )
{
}

/**
 * @brief This function handles Pendable request for system service.
 */
extern "C" void PendSV_Handler( void )
{
}

/**
 * @brief This function handles System tick timer.
 */
extern "C" void SysTick_Handler( void )
{
        HAL_IncTick();
}

/**
 * @brief This function handles ADC1 and ADC2 global interrupt.
 */
extern "C" void ADC1_2_IRQHandler( void )
{
}

/**
 * @brief This function handles FDCAN1 interrupt 0.
 */
extern "C" void FDCAN1_IT0_IRQHandler( void )
{
        // HAL_FDCAN_IRQHandler( &hfdcan1 );
}

/**
 * @brief This function handles FDCAN1 interrupt 1.
 */
extern "C" void FDCAN1_IT1_IRQHandler( void )
{
        // HAL_FDCAN_IRQHandler( &hfdcan1 );
}

/**
 * @brief This function handles SPI1 global interrupt.
 */
extern "C" void SPI1_IRQHandler( void )
{
        // HAL_SPI_IRQHandler( &hspi1 );
}

/**
 * @brief This function handles USART1 global interrupt / USART1 wake-up interrupt through EXTI
 * line 25.
 */
extern "C" void USART1_IRQHandler( void )
{
        HAL_UART_IRQHandler( &HANDLERS.bus_uart );
}

/**
 * @brief This function handles USART2 global interrupt / USART2 wake-up interrupt through EXTI
 * line 26.
 */
extern "C" void USART2_IRQHandler( void )
{
        HAL_UART_IRQHandler( &HANDLERS.com_uart );
}

/**
 * @brief This function handles I2C3 event interrupt / I2C3 wake-up interrupt through EXTI line 27.
 */
extern "C" void I2C3_EV_IRQHandler( void )
{
        HAL_I2C_EV_IRQHandler( &HANDLERS.i2c );
}

/**
 * @brief This function handles I2C3 error interrupt.
 */
extern "C" void I2C3_ER_IRQHandler( void )
{
        HAL_I2C_ER_IRQHandler( &HANDLERS.i2c );
}
