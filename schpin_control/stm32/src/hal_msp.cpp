/**
 ******************************************************************************
 * @file         stm32g4xx_hal_msp.c
 * @brief        This file provides code for the MSP Initialization
 *               and de-Initialization codes.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */

#include "main.h"
#include "util.h"

using namespace schpin;

extern "C" void HAL_MspInit( void )
{

        __HAL_RCC_SYSCFG_CLK_ENABLE();
        __HAL_RCC_PWR_CLK_ENABLE();

        /** Disable the internal Pull-Up in Dead Battery pins of UCPD peripheral
         */
        HAL_PWREx_DisableUCPDDeadBattery();
}

/**
 * @brief FDCAN MSP Initialization
 * This function configures the hardware resources used in this example
 * @param hfdcan: FDCAN handle pointer
 * @retval None
 */
extern "C" void HAL_FDCAN_MspInit( FDCAN_HandleTypeDef* hfdcan )
{
        GPIO_InitTypeDef GPIO_InitStruct;
        zero_struct( &GPIO_InitStruct );

        if ( hfdcan->Instance == FDCAN1 ) {
                /* Peripheral clock enable */
                __HAL_RCC_FDCAN_CLK_ENABLE();

                __HAL_RCC_GPIOA_CLK_ENABLE();
                /**FDCAN1 GPIO Configuration
                PA11     ------> FDCAN1_RX
                PA12     ------> FDCAN1_TX
                */
                GPIO_InitStruct.Pin       = GPIO_PIN_11 | GPIO_PIN_12;
                GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
                GPIO_InitStruct.Pull      = GPIO_NOPULL;
                GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_LOW;
                GPIO_InitStruct.Alternate = GPIO_AF9_FDCAN1;
                HAL_GPIO_Init( GPIOA, &GPIO_InitStruct );
        }
}

/**
 * @brief FDCAN MSP De-Initialization
 * This function freeze the hardware resources used in this example
 * @param hfdcan: FDCAN handle pointer
 * @retval None
 */
extern "C" void HAL_FDCAN_MspDeInit( FDCAN_HandleTypeDef* hfdcan )
{
        if ( hfdcan->Instance == FDCAN1 ) {
                /* Peripheral clock disable */
                __HAL_RCC_FDCAN_CLK_DISABLE();

                /**FDCAN1 GPIO Configuration
                PA11     ------> FDCAN1_RX
                PA12     ------> FDCAN1_TX
                */
                HAL_GPIO_DeInit( GPIOA, GPIO_PIN_11 | GPIO_PIN_12 );
        }
}

/**
 * @brief I2C MSP Initialization
 * This function configures the hardware resources used in this example
 * @param hi2c: I2C handle pointer
 * @retval None
 */
extern "C" void HAL_I2C_MspInit( I2C_HandleTypeDef* hi2c )
{
        GPIO_InitTypeDef GPIO_InitStruct;
        zero_struct( &GPIO_InitStruct );

        if ( hi2c->Instance == I2C1 ) {
                __HAL_RCC_GPIOA_CLK_ENABLE();
                __HAL_RCC_GPIOB_CLK_ENABLE();
                /**I2C1 GPIO Configuration
                PA15     ------> I2C1_SCL
                PB7     ------> I2C1_SDA
                */
                GPIO_InitStruct.Pin       = GPIO_PIN_15;
                GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
                GPIO_InitStruct.Pull      = GPIO_PULLUP;
                GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_LOW;
                GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
                HAL_GPIO_Init( GPIOA, &GPIO_InitStruct );

                GPIO_InitStruct.Pin       = GPIO_PIN_7;
                GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
                GPIO_InitStruct.Pull      = GPIO_PULLUP;
                GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_LOW;
                GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
                HAL_GPIO_Init( GPIOB, &GPIO_InitStruct );

                /* Peripheral clock enable */
                __HAL_RCC_I2C1_CLK_ENABLE();
                /* I2C1 interrupt Init */
                HAL_NVIC_SetPriority( I2C1_EV_IRQn, 0, 0 );
                HAL_NVIC_EnableIRQ( I2C1_EV_IRQn );
                HAL_NVIC_SetPriority( I2C1_ER_IRQn, 0, 0 );
                HAL_NVIC_EnableIRQ( I2C1_ER_IRQn );
        }
}

/**
 * @brief I2C MSP De-Initialization
 * This function freeze the hardware resources used in this example
 * @param hi2c: I2C handle pointer
 * @retval None
 */
extern "C" void HAL_I2C_MspDeInit( I2C_HandleTypeDef* hi2c )
{
        if ( hi2c->Instance == I2C1 ) {
                /* Peripheral clock disable */
                __HAL_RCC_I2C1_CLK_DISABLE();

                /**I2C1 GPIO Configuration
                PA15     ------> I2C1_SCL
                PB7     ------> I2C1_SDA
                */
                HAL_GPIO_DeInit( GPIOA, GPIO_PIN_15 );

                HAL_GPIO_DeInit( GPIOB, GPIO_PIN_7 );

                /* I2C1 interrupt DeInit */
                HAL_NVIC_DisableIRQ( I2C1_EV_IRQn );
                HAL_NVIC_DisableIRQ( I2C1_ER_IRQn );
        }
}

/**
 * @brief SPI MSP Initialization
 * This function configures the hardware resources used in this example
 * @param hspi: SPI handle pointer
 * @retval None
 */
extern "C" void HAL_SPI_MspInit( SPI_HandleTypeDef* hspi )
{
        GPIO_InitTypeDef GPIO_InitStruct;
        zero_struct( &GPIO_InitStruct );

        if ( hspi->Instance == SPI1 ) {
                /* Peripheral clock enable */
                __HAL_RCC_SPI1_CLK_ENABLE();

                __HAL_RCC_GPIOA_CLK_ENABLE();
                /**SPI1 GPIO Configuration
                PA5     ------> SPI1_SCK
                PA6     ------> SPI1_MISO
                PA7     ------> SPI1_MOSI
                */
                GPIO_InitStruct.Pin       = GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
                GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
                GPIO_InitStruct.Pull      = GPIO_NOPULL;
                GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_LOW;
                GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
                HAL_GPIO_Init( GPIOA, &GPIO_InitStruct );
        }
}

/**
 * @brief SPI MSP De-Initialization
 * This function freeze the hardware resources used in this example
 * @param hspi: SPI handle pointer
 * @retval None
 */
extern "C" void HAL_SPI_MspDeInit( SPI_HandleTypeDef* hspi )
{
        if ( hspi->Instance == SPI1 ) {
                /* Peripheral clock disable */
                __HAL_RCC_SPI1_CLK_DISABLE();

                /**SPI1 GPIO Configuration
                PA5     ------> SPI1_SCK
                PA6     ------> SPI1_MISO
                PA7     ------> SPI1_MOSI
                */
                HAL_GPIO_DeInit( GPIOA, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7 );
        }
}

/**
 * @brief UART MSP Initialization
 * This function configures the hardware resources used in this example
 * @param huart: UART handle pointer
 * @retval None
 */
extern "C" void HAL_UART_MspInit( UART_HandleTypeDef* huart )
{
        GPIO_InitTypeDef GPIO_InitStruct;
        zero_struct( &GPIO_InitStruct );

        if ( huart->Instance == USART1 ) {
                /* Peripheral clock enable */
                __HAL_RCC_USART1_CLK_ENABLE();

                __HAL_RCC_GPIOA_CLK_ENABLE();
                /**USART1 GPIO Configuration
                PA9     ------> USART1_TX
                */
                GPIO_InitStruct.Pin       = GPIO_PIN_9;
                GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
                GPIO_InitStruct.Pull      = GPIO_NOPULL;
                GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_LOW;
                GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
                HAL_GPIO_Init( GPIOA, &GPIO_InitStruct );

                /* USART1 interrupt Init */
                HAL_NVIC_SetPriority( USART1_IRQn, 0, 0 );
                HAL_NVIC_EnableIRQ( USART1_IRQn );
        } else if ( huart->Instance == USART2 ) {
                /* Peripheral clock enable */
                __HAL_RCC_USART2_CLK_ENABLE();

                __HAL_RCC_GPIOA_CLK_ENABLE();
                /**USART2 GPIO Configuration
                PA2     ------> USART2_TX
                PA3     ------> USART2_RX
                */
                GPIO_InitStruct.Pin       = GPIO_PIN_2 | GPIO_PIN_3;
                GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
                GPIO_InitStruct.Pull      = GPIO_NOPULL;
                GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_LOW;
                GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
                HAL_GPIO_Init( GPIOA, &GPIO_InitStruct );

                /* USART2 interrupt Init */
                HAL_NVIC_SetPriority( USART2_IRQn, 0, 0 );
                HAL_NVIC_EnableIRQ( USART2_IRQn );
        }
}

/**
 * @brief UART MSP De-Initialization
 * This function freeze the hardware resources used in this example
 * @param huart: UART handle pointer
 * @retval None
 */
extern "C" void HAL_UART_MspDeInit( UART_HandleTypeDef* huart )
{
        if ( huart->Instance == USART1 ) {
                /* Peripheral clock disable */
                __HAL_RCC_USART1_CLK_DISABLE();

                /**USART1 GPIO Configuration
                PA9     ------> USART1_TX
                */
                HAL_GPIO_DeInit( GPIOA, GPIO_PIN_9 );

                /* USART1 interrupt DeInit */
                HAL_NVIC_DisableIRQ( USART1_IRQn );
        } else if ( huart->Instance == USART2 ) {
                /* Peripheral clock disable */
                __HAL_RCC_USART2_CLK_DISABLE();

                /**USART2 GPIO Configuration
                PA2     ------> USART2_TX
                PA3     ------> USART2_RX
                */
                HAL_GPIO_DeInit( GPIOA, GPIO_PIN_2 | GPIO_PIN_3 );

                /* USART2 interrupt DeInit */
                HAL_NVIC_DisableIRQ( USART2_IRQn );
        }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
