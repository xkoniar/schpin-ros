#pragma once

extern "C" void DebugMon_Handler( void );
extern "C" void PendSV_Handler( void );
extern "C" void SysTick_Handler( void );
extern "C" void ADC1_2_IRQHandler( void );
extern "C" void FDCAN1_IT0_IRQHandler( void );
extern "C" void FDCAN1_IT1_IRQHandler( void );
extern "C" void SPI1_IRQHandler( void );
extern "C" void USART1_IRQHandler( void );
extern "C" void USART2_IRQHandler( void );
extern "C" void I2C3_EV_IRQHandler( void );
extern "C" void I2C3_ER_IRQHandler( void );
