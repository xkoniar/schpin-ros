import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument, IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution

def generate_launch_description():
    robot_package = os.environ['SCHPIN_PACKAGE']

    robot_urdf_filename = os.path.join(get_package_share_directory(robot_package), 'urdf/robot.urdf')
    with open(robot_urdf_filename, 'r') as fd:
        robot_urdf = fd.read()
    robot_srdf_filename = os.path.join(get_package_share_directory(robot_package), 'srdf/robot.srdf')
    with open(robot_srdf_filename, 'r') as fd:
        robot_srdf = fd.read()

    params = os.path.join(get_package_share_directory(robot_package), "config/control.yml")

    return LaunchDescription([
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([get_package_share_directory("schpin_lib"), "/launch/shared.py"]),
            launch_arguments={"urdf": robot_urdf}.items()
        ),
        DeclareLaunchArgument('dev', default_value=""),
        Node(
            package='schpin_control',
            executable='control_fw',
            name='control_fw',
            output='both',
            parameters=[{"dev":LaunchConfiguration('dev')}]
        ),
        DeclareLaunchArgument('script', default_value=""),
        Node(
            package='schpin_control',
            executable='control_node',
            name='control_node',
            output='both',
            ##prefix=['kitty -e gdb --args'],
            parameters=[params,{"robot_description": robot_urdf,
                "robot_semantics": robot_srdf,
                "script": LaunchConfiguration('script')
            }]
        )
    ])
