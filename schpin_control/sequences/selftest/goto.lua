require "schpin_control.sequences.koke"

for i, l in ipairs(legs) do
    print("Self testing leg ",l)

    alfa = l.."/alfa"
    beta = l.."/beta"
    gama = l.."/gama"

    print("Alfa test of ", alfa)
    for i, val in ipairs({pi/4, pi/3, pi/4}) do
        print("step ", i, " pos ", val)
        joint_goto {joint=alfa, pos=val, time=1}
        block_yield{joints={alfa}}
        assert(almost_equal(get_leg_state{leg=l}[1], val, 0.08), alfa .. " joint state is " .. get_leg_state{leg=l}[1] .. " instead of " .. val )
    end

    print("Beta test of ", beta)
    for i, val in ipairs({pi, pi - 0.2, pi + 0.2, pi}) do
        print("step ", i, " pos ", val)
        joint_goto {joint=beta, pos=val, time=1}
        block_yield{joints={beta}}
        assert(almost_equal(get_leg_state{leg=l}[2], val, 0.08), beta .. " joint state is " .. get_leg_state{leg=l}[2] .. " instead of " .. val )
    end

    print("Gama test of ", gama)
    for i, val in ipairs({pi, pi - 0.2, pi + 0.2, pi}) do
        print("step ", i, " pos ", val)
        joint_goto {joint=gama, pos=val, time=1}
        block_yield{joints={gama}}
        assert(almost_equal(get_leg_state{leg=l}[3], val, 0.08), gama .. " joint state is " .. get_leg_state{leg=l}[2] .. " instead of " .. val )
    end
end


