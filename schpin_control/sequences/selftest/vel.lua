require "schpin_control.sequences.koke"

for i, l in ipairs(legs) do

    alfa = l.."/alfa"
    beta = l.."/beta"
    gama = l.."/gama"

    for i, val in ipairs({0.5,-0.5}) do
        joint_spin {joint=alfa, velocity=val, time=1.}
        block_yield {joints={alfa}}
    end
    
    for i, val in ipairs({0.5,-0.5}) do
        joint_spin {joint=beta, velocity=val, time=1.}
        block_yield {joints={beta}}
    end
    
    for i, val in ipairs({0.5,-0.5}) do
        joint_spin {joint=gama, velocity=val, time=1.}
        block_yield {joints={gama}}
    end
end
