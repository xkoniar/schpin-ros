require "schpin_control.sequences.util"


set_servo_config{
    joints=get_joints{leg=get_legs()[1]},
    vars={LEWAN_TELEM = true}
}
-- initial setup
set_body_pose {
    point = {0., 0., 0.},
    orientation = {{1., 0., 0}, 0}
}

legs = get_legs()
all = {}
for i, leg in ipairs(legs) do
    all = merge(all, get_joints{leg=leg})
end

print("Setting to neutral")
for i, leg in ipairs(legs) do
    neutral_angles = get_neutral_angles{leg=leg}
    for j, joint in ipairs(get_joints{leg=leg}) do
        print(joint," ", neutral_angles[j])
        joint_goto {joint = joint, pos = neutral_angles[j], time = 2.}
    end
end
block_yield {joints = all}

-- store tips coords at start
tips = get_leg_tips(legs)
for i, leg in pairs(legs) do

    print("Leg " .. leg .. " tip: ", tips[leg][1],tips[leg][2], tips[leg][3])
end

-- number of steps
n = 2000
-- scale for the sin/cos operations
f = 16
-- step of movement on x/y plane
pos_step = 0.01
-- angle change around direction
angle_step = 0.05

print("Starting the scan")
for i = 1, n do

    -- direction of orientation
    dir = {0., 1., math.sin(i / f)}

    set_body_pose {
        point = {
            math.cos(i / f) * pos_step,
            math.sin(i / f) * pos_step, 0.
        },
        orientation = {dir, math.cos(i / f) * angle_step}
    }

    for i, leg in pairs(legs) do
        tip_goto {
            leg = leg,
            time = 0.01,
            target = tips[leg],
            steps = 64
        }
    end

    block_yield {joints = all}

end
