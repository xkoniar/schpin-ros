log_file = io.open("./pid.csv", "w")

on_msg {
    key = "SERVO",
    cb = function(msg)
        sid = msg[1]
        submsg = msg[2]
        subid = submsg[0]
        if subid ~= "TELEM" then return end
        log_file:write(0, ",", sid, ",", submsg[1], ",",
                       submsg[2], ",", submsg[3], "\n")
    end
}

