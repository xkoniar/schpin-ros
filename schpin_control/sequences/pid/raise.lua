require "schpin_control.sequences.koke"
require "schpin_control.sequences.pid.log"

set_servo_config {
    joint = all,
    vars = {
        goto_p_gain = 1.5,
        goto_i_gain = 0.,
        goto_d_gain = 0.05,
        vel_p_gain = 0.2,
        vel_i_gain = 0.02,
        vel_d_gain = 0.02
    }
}

from = 0.1
target = pi / 4

joint_goto {joints = betas, pos = from, time = 1.0}
block_yield {joints = betas}

joint_goto {joints = betas, pos = target, time = 2.0}
block_yield {joints = betas}

