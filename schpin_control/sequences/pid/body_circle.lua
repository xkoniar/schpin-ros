require "schpin_control.sequences.koke"
require "schpin_control.sequences.pid.log"

-- initial setup
set_body_pose {
    point = {0., 0., 0.},
    orientation = {{1., 0., 0}, 0}
}
joint_goto {joints = alfas, pos = neutral_angles[1], time = 2.}
joint_goto {joints = betas, pos = neutral_angles[2], time = 2.}
joint_goto {joints = gamas, pos = neutral_angles[2], time = 2.}
block_yield {joints = all}

-- store tips coords at start
tips = get_leg_tips(legs)

-- number of steps
n = 2000
-- scale for the sin/cos operations
f = 128
-- step of movement on x/y plane
pos_step = 0.02
-- angle change around direction
angle_step = 0.15

-- Movement is coded by forcing body position and moving all tips of legs into the original world coordinates
for i = 1, n do

    set_body_pose {
        point = {
            math.cos(i / f) * pos_step,
            math.sin(i / f) * pos_step, 0.
        },
        orientation = {{0., 0., 1.}, 0.}
    }

    for i, leg in pairs(legs) do
        tip_goto {
            leg = leg,
            time = 0.002,
            target = tips[leg],
            steps = 64
        }
    end

    block_yield {joints = all}

end
