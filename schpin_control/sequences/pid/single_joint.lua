require "schpin_control.sequences.koke"
require "schpin_control.sequences.pid.log"

set_servo_config {
    joint = "leg/fl/alfa",
    vars = {
        goto_p_gain = 1.,
        goto_i_gain = 0.,
        goto_d_gain = 0.005,
        vel_p_gain = 0.,
        vel_i_gain = 0.05,
        vel_d_gain = 0.
    }
}

joint_goto {joint = "leg/fl/alfa", pos = 0., time = 1.0}
