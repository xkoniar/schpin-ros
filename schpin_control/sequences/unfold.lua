require "schpin_control.sequences.koke"
require "schpin_control.sequences.util"

Chain:new(function()

    print("enforce 0 on alfas")
    joint_goto {joints = alfas, pos = 0., time = 1.}
    block_yield {joints = alfas}

    print("partly lift betas and move gamas to zero")
    joint_goto {joints = betas, pos = 0.785398, time = 3}
    block_yield {joints = betas}

    joint_spin_until {joints = gamas, pos = 0.1, velocity = 0.2}
    block_yield {joints = gamas}

    joint_goto {joints = alfas, pos = 0.2, time = 2.}
    block_yield {joints = alfas}

    print("straightn the legs")
    joint_goto {joints = betas, pos = 1.5708, time = 3}
    joint_goto {joints = gamas, pos = 1.5708, time = 3}
    block_yield {joints = merge(betas, gamas)}

    print("straightn the legs")
    joint_goto {joints = betas, pos = 1.5708 + 0.78398, time = 3}
    joint_goto {joints = gamas, pos = 1.5708 + 1.5708, time = 3}
    block_yield {joints = merge(betas, gamas)}
    block_yield {joints = betas}

end):exec()

-- store tips coords at start
tips = get_leg_tips(legs)

function sleek_body_move(a, b)
    steps = 32
    for i = 1, steps do
        set_body_pose {
            point = interp(a, b, i / steps),
            orientation = {{0, 0, 1}, 0}
        }
        for i, leg in pairs(legs) do
            tip_goto {
                leg = leg,
                time = 0.002,
                target = tips[leg],
                steps = 64
            }
        end
        block_yield {joints = all}
    end
end

midpose = {0, 0, 0}
bpose = midpose

-- unfold each leg, at start move the robot apart from the legs direction, unfold the leg, move body part
for i, key in ipairs(legs) do
    b_step = 0.015
    newpose = {-dirs[i][1] * b_step, -dirs[i][2] * 0.01, 0}
    sleek_body_move(bpose, newpose)
    bpose = newpose

    tip_move {leg = legs[i], dir = {0, 0, 0.05}, time = 2.}
    block_yield {joints = {alfas[i], betas[i], gamas[i]}}

    state = get_leg_state {leg = legs[i]}
    pos = get_tip_pos {leg = legs[i], angles = state}
    desired_pos = get_tip_pos {
        leg = legs[i],
        angles = neutral_angles
    }

    tip_goto {
        leg = legs[i],
        target = {
            desired_pos[1] + dirs[i][1] * b_step, desired_pos[2],
            pos[3]
        },
        time = 2.
    }
    block_yield {joints = {alfas[i], betas[i], gamas[i]}}

    tip_move {leg = legs[i], dir = {0, 0, -0.05}, time = 2.}
    block_yield {joints = {alfas[i], betas[i], gamas[i]}}

    tips[key] = get_tip_pos {
        leg = key,
        angles = get_leg_state {leg = key}
    }
end

sleek_body_move(bpose, midpose)
