pi = math.pi

function clamp(a, b, c)
    if a > c then return c end
    if a < b then return b end
    return a
end

function almost_equal(a,b,eps)
    return math.abs(a-b) < eps
end

function merge(a, b)
    res = {}
    for k, v in pairs(b) do res[k] = v end
    for k, v in pairs(a) do res[#b + k] = v end
    return res
end

function get_leg_tips(legs)
    tips = {}
    for i, leg in pairs(legs) do
        tips[leg] = get_tip_pos {
            leg = leg,
            angles = get_leg_state {leg = leg}
        }
    end
    return tips
end

function interp(a, b, f)
    return {
        a[1] + (b[1] - a[1]) * f, a[2] + (b[2] - a[2]) * f,
        a[3] + (b[3] - a[3]) * f
    }
end
