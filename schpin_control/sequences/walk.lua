function update_pose_on_block_yield(jnt, point, quat)
    block_yield {joints = jnt}
    set_body_pose {point = point, orientation = quat}
end

set_body_pose {
    point = {0.27, 0, 0.15},
    orientation = {0, 0, 0, 1}
}
joint_goto {joint = "leg/rr/alfa", time = 0.25, pos = 0.75125}
joint_goto {joint = "leg/rr/beta", time = 0.25, pos = 2.91394}
joint_goto {joint = "leg/rr/gama", time = 0.25, pos = 3.09606}
joint_goto {joint = "leg/rl/alfa", time = 0.25, pos = 0.75125}
joint_goto {joint = "leg/rl/beta", time = 0.25, pos = 2.91394}
joint_goto {joint = "leg/rl/gama", time = 0.25, pos = 3.09606}
joint_goto {joint = "leg/fr/alfa", time = 0.25, pos = 0.682955}
joint_goto {joint = "leg/fr/beta", time = 0.25, pos = 2.91394}
joint_goto {joint = "leg/fr/gama", time = 0.25, pos = 2.91394}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
}, {0.27, 0, 0.15}, {0, 0, 0, 1})
print(
    "pose from: (0.270000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.270000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.614659
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.73182
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.546364
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {joint = "leg/fl/gama", time = 0.364243, pos = 2.5497}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.478068
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.36758
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.409773
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.18546
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.341477
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.00333
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.341477
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.00333
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 2.500000,
    target = {-0.17, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 2.500000,
    target = {-0.17, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 2.500000,
    target = {0.18, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.27, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 2.500000,
    target = {-0.17, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 2.500000,
    target = {-0.17, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 2.500000,
    target = {0.18, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.27, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.270000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.270000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 0.819546
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.27818
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 0.887841
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 0.956137
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.64243
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.136591,
    pos = 1.02443
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.136591,
    pos = 1.09273
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.136591,
    pos = 1.16102
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.136591,
    pos = 1.22932
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.136591,
    pos = 1.29761
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.136591,
    pos = 1.36591
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 1.43421
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 1.43421
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
relative_tip_goto {
    leg = "leg/rl",
    time = 2.500000,
    target = {-0.17, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 2.500000,
    target = {0.18, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 2.500000,
    target = {0.28, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.27, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 2.500000,
    target = {-0.17, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 2.500000,
    target = {0.18, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 2.500000,
    target = {0.28, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.27, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.270000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.270000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.614659
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fr/gama",
    time = 0.364243,
    pos = 2.73182
}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.546364
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {joint = "leg/fr/gama", time = 0.364243, pos = 2.5497}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.478068
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fr/gama",
    time = 0.364243,
    pos = 2.36758
}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.409773
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fr/gama",
    time = 0.364243,
    pos = 2.18546
}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.341477
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fr/gama",
    time = 0.364243,
    pos = 2.00333
}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.341477
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fr/gama",
    time = 0.364243,
    pos = 2.00333
}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 2.500000,
    target = {-0.07, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 2.500000,
    target = {-0.17, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 2.500000,
    target = {0.28, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.27, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 2.500000,
    target = {-0.07, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 2.500000,
    target = {-0.17, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 2.500000,
    target = {0.28, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.27, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.270000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.298750,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.341477
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.00333
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.341477
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.18546
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.409773
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.36758
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.478068
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {joint = "leg/fl/gama", time = 0.364243, pos = 2.5497}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.546364
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.73182
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.546364
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 2.73182
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.07, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.17, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.28, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.27, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0719167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.171917, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.278083, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.271917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0738333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.173833, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.276167, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.273833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.07575, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.17575, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.27425, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.27575, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0776667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.177667, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.272333, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.277667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0795833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.179583, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.270417, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.279583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0815, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.1815, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.2685, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.2815, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0834167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.183417, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.266583, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.283417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0853333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.185333, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.264667, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.285333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.08725, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.18725, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.26275, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.28725, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0891667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.189167, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.260833, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.289167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0910833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.191083, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.258917, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.291083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.093, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.193, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.257, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.293, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0949167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.194917, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.255083, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.294917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.0968333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.196833, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.253167, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.296833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.331956,
    target = {-0.09875, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.331956,
    target = {-0.19875, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.331956,
    target = {0.25125, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.29875, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.298750,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.327500,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.682955
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.09606
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.75125
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.27818
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.819546
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.887841
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.956137
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.956137
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.09875, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.19875, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.25125, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.29875, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.100667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.200667, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.249333, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.300667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.102583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.202583, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.247417, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.302583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.1045, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.2045, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.2455, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.3045, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.106417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.206417, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.243583, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.306417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.108333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.208333, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.241667, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.308333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.11025, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.21025, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.23975, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.31025, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.112167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.212167, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.237833, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.312167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.114083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.214083, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.235917, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.314083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.116, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.216, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.234, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.316, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.117917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.217917, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.232083, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.317917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.119833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.219833, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.230167, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.319833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.12175, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.22175, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.22825, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.32175, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.123667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.223667, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.226333, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.323667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.125583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.225583, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.224417, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.325583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317662,
    target = {-0.1275, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317662,
    target = {-0.2275, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.317662,
    target = {0.2225, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.3275, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.327500,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.356250,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/rl/alfa",
    time = 0.364243,
    pos = 0.614659
}
joint_goto {
    joint = "leg/rl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {joint = "leg/rl/gama", time = 0.364243, pos = 2.5497}
sync {joints = {"leg/rl/alfa", "leg/rl/beta", "leg/rl/gama"}}
joint_goto {
    joint = "leg/rl/alfa",
    time = 0.364243,
    pos = 0.682955
}
joint_goto {
    joint = "leg/rl/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/rl/gama",
    time = 0.364243,
    pos = 2.73182
}
sync {joints = {"leg/rl/alfa", "leg/rl/beta", "leg/rl/gama"}}
joint_goto {
    joint = "leg/rl/alfa",
    time = 0.364243,
    pos = 0.75125
}
joint_goto {
    joint = "leg/rl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rl/gama",
    time = 0.364243,
    pos = 2.91394
}
sync {joints = {"leg/rl/alfa", "leg/rl/beta", "leg/rl/gama"}}
joint_goto {
    joint = "leg/rl/alfa",
    time = 0.364243,
    pos = 0.75125
}
joint_goto {
    joint = "leg/rl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rl/gama",
    time = 0.364243,
    pos = 2.91394
}
sync {joints = {"leg/rl/alfa", "leg/rl/beta", "leg/rl/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.1275, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.2225, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.1225, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.3275, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.129417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.220583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.120583, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.329417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.131333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.218667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.118667, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.331333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.13325, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.21675, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.11675, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.33325, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.135167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.214833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.114833, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.335167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.137083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.212917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.112917, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.337083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.139, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.211, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.111, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.339, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.140917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.209083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.109083, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.340917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.142833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.207167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.107167, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.342833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.14475, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.20525, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.10525, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.34475, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.146667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.203333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.103333, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.346667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.148583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.201417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.101417, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.348583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.1505, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.1995, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.0995, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.3505, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.152417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.197583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.0975833, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.352417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.154333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.195667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.0956666, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.354333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308379,
    target = {-0.15625, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308379,
    target = {0.19375, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308379,
    target = {0.09375, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.35625, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.356250,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.385000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 1.16102
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 1.09273
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.136591,
    pos = 1.02443
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.136591,
    pos = 0.956137
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.887841
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.819546
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.27818
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.819546
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.27818
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.15625, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.15625, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.19375, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.35625, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.158167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.158167, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.191833, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.358167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.160083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.160083, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.189917, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.360083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.162, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.162, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.188, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.362, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.163917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.163917, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.186083, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.363917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.165833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.165833, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.184167, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.365833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.16775, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.16775, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.18225, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.36775, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.169667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.169667, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.180333, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.369667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.171583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.171583, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.178417, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.371583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.1735, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.1735, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.1765, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.3735, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.175417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.175417, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.174583, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.375417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.177333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.177333, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.172667, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.377333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.17925, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.17925, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.17075, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.37925, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.181167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.181167, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.168833, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.381167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.183083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.183083, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.166917, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.383083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.273359,
    target = {-0.185, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.273359,
    target = {-0.185, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.273359,
    target = {0.165, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.385, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.385000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.413750,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 0.75125
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.09606
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 0.819546
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.27818
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 0.887841
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 0.956137
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.64243
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 1.02443
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
joint_goto {
    joint = "leg/rr/alfa",
    time = 0.364243,
    pos = 1.02443
}
joint_goto {
    joint = "leg/rr/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/rr/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/rr/alfa", "leg/rr/beta", "leg/rr/gama"}}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.185, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.165, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.165, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.385, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.186917, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.163083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.163083, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.386917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.188833, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.161167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.161167, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.388833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.19075, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.15925, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.15925, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.39075, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.192667, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.157333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.157333, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.392667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.194583, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.155417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.155417, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.394583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.1965, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.1535, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.1535, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.3965, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.198417, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.151583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.151583, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.398417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.200333, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.149667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.149667, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.400333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.20225, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.14775, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.14775, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.40225, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.204167, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.145833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.145833, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.404167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.206083, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.143917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.143917, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.406083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.208, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.142, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.142, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.408, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.209917, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.140083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.140083, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.409917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.211833, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.138167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.138167, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.411833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rl",
    time = 0.267730,
    target = {-0.21375, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.267730,
    target = {0.13625, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.267730,
    target = {0.13625, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.41375, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.413750,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.442500,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.819546
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fr/gama",
    time = 0.364243,
    pos = 3.09606
}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.75125
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/fr/gama",
    time = 0.364243,
    pos = 2.91394
}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.682955
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/fr/gama",
    time = 0.364243,
    pos = 2.73182
}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.614659
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {joint = "leg/fr/gama", time = 0.364243, pos = 2.5497}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
joint_goto {
    joint = "leg/fr/alfa",
    time = 0.364243,
    pos = 0.614659
}
joint_goto {
    joint = "leg/fr/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {joint = "leg/fr/gama", time = 0.364243, pos = 2.5497}
sync {joints = {"leg/fr/alfa", "leg/fr/beta", "leg/fr/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.11375, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.21375, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.13625, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.41375, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.115667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.215667, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.134333, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.415667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.117583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.217583, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.132417, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.417583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.1195, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.2195, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.1305, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.4195, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.121417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.221417, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.128583, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.421417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.123333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.223333, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.126667, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.423333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.12525, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.22525, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.12475, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.42525, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.127167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.227167, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.122833, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.427167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.129083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.229083, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.120917, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.429083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.131, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.231, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.119, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.431, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.132917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.232917, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.117083, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.432917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.134833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.234833, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.115167, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.434833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.13675, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.23675, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.11325, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.43675, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.138667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.238667, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.111333, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.438667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.140583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.240583, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.109417, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.440583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.317794,
    target = {-0.1425, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.317794,
    target = {-0.2425, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.317794,
    target = {0.1075, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.4425, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.442500,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.471250,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/rl/alfa",
    time = 0.364243,
    pos = 0.546364
}
joint_goto {
    joint = "leg/rl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {joint = "leg/rl/gama", time = 0.364243, pos = 2.5497}
sync {joints = {"leg/rl/alfa", "leg/rl/beta", "leg/rl/gama"}}
joint_goto {
    joint = "leg/rl/alfa",
    time = 0.364243,
    pos = 0.614659
}
joint_goto {
    joint = "leg/rl/beta",
    time = 0.364243,
    pos = 3.27818
}
joint_goto {
    joint = "leg/rl/gama",
    time = 0.364243,
    pos = 2.73182
}
sync {joints = {"leg/rl/alfa", "leg/rl/beta", "leg/rl/gama"}}
joint_goto {
    joint = "leg/rl/alfa",
    time = 0.364243,
    pos = 0.682955
}
joint_goto {
    joint = "leg/rl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rl/gama",
    time = 0.364243,
    pos = 2.91394
}
sync {joints = {"leg/rl/alfa", "leg/rl/beta", "leg/rl/gama"}}
joint_goto {
    joint = "leg/rl/alfa",
    time = 0.364243,
    pos = 0.682955
}
joint_goto {
    joint = "leg/rl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/rl/gama",
    time = 0.364243,
    pos = 2.91394
}
sync {joints = {"leg/rl/alfa", "leg/rl/beta", "leg/rl/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.1425, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.2075, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.1075, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.4425, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.144417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.205583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.105583, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.444417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.146333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.203667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.103667, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.446333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.14825, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.20175, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.10175, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.44825, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.150167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.199833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0998333, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.450167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.152083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.197917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0979167, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.452083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.154, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.196, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.096, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.454, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.155917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.194083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0940833, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.455917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.157833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.192167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0921667, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.457833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.15975, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.19025, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.09025, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.45975, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.161667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.188333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0883333, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.461667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.163583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.186417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0864167, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.463583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.1655, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.1845, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0845, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.4655, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.167417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.182583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0825833, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.467417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.169333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.180667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.0806667, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.469333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.308405,
    target = {-0.17125, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.308405,
    target = {0.17875, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fl",
    time = 0.308405,
    target = {0.07875, 0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/fr/alfa",
    "leg/fr/beta", "leg/fr/gama", "leg/fl/alfa", "leg/fl/beta",
    "leg/fl/gama"
}, {0.47125, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
print(
    "pose from: (0.471250,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
print(
    "pose to: (0.500000,0.000000,0.150000)-[0.000000,0.000000,0.000000,1.000000]")
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 1.29761
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 1.22932
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.136591,
    pos = 1.16102
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.136591,
    pos = 1.09273
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.136591,
    pos = 1.02443
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.136591,
    pos = 0.956137
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.136591,
    pos = 3.09606
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.136591,
    pos = 3.64243
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.887841
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
joint_goto {
    joint = "leg/fl/alfa",
    time = 0.364243,
    pos = 0.887841
}
joint_goto {
    joint = "leg/fl/beta",
    time = 0.364243,
    pos = 2.91394
}
joint_goto {
    joint = "leg/fl/gama",
    time = 0.364243,
    pos = 3.46031
}
sync {joints = {"leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"}}
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.17125, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.17125, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.17875, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.47125, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.173167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.173167, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.176833, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.473167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.175083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.175083, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.174917, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.475083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.177, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.177, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.173, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.477, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.178917, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.178917, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.171083, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.478917, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.180833, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.180833, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.169167, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.480833, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.18275, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.18275, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.16725, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.48275, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.184667, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.184667, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.165333, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.484667, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.186583, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.186583, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.163417, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.486583, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.1885, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.1885, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.1615, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.4885, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.190417, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.190417, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.159583, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.490417, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.192333, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.192333, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.157667, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.492333, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.19425, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.19425, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.15575, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.49425, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.196167, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.196167, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.153833, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.496167, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.198083, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.198083, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.151917, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.498083, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
relative_tip_goto {
    leg = "leg/rr",
    time = 0.275326,
    target = {-0.2, -0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/rl",
    time = 0.275326,
    target = {-0.2, 0.15, -0.1},
    steps = 32
}
relative_tip_goto {
    leg = "leg/fr",
    time = 0.275326,
    target = {0.15, -0.15, -0.1},
    steps = 32
}
update_pose_on_block_yield({
    "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama", "leg/rl/alfa",
    "leg/rl/beta", "leg/rl/gama", "leg/fr/alfa", "leg/fr/beta",
    "leg/fr/gama"
}, {0.5, 0, 0.15}, {0, 0, 0, 1})
coroutine.yield()
sync {
    joints = {
        "leg/rr/alfa", "leg/rr/beta", "leg/rr/gama",
        "leg/rl/alfa", "leg/rl/beta", "leg/rl/gama",
        "leg/fr/alfa", "leg/fr/beta", "leg/fr/gama",
        "leg/fl/alfa", "leg/fl/beta", "leg/fl/gama"
    }
}
