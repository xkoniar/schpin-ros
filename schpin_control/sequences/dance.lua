
legs = get_legs();


for i, leg in ipairs(legs) do
    neutral_angles = get_neutral_angles{leg = leg};
    joint_goto {joints = leg.."/alfa", pos = neutral_angles[1], time = 1.}
    joint_goto {joints = leg.."/beta", pos = neutral_angles[2], time = 1.}
    joint_goto {joints = leg.."/gama", pos = neutral_angles[3], time = 1.}
end
block_yield {joints = all}

-- movement on z axis
for i = 1, 3 do
    for i, s in pairs({0.05, -0.05}) do
        body_move {
            dir = {0, 0, s},
            rot = {{0, 0, 1}, 0},
            time = 0.05
        }
        block_yield {joints = all}
    end
end

-- movement on y axis
body_move {dir = {0, 0.05, 0}, rot = {{0, 0, 1}, 0}, time = 0.2}
block_yield {joints = all}
for i = 1, 3 do
    body_move {
        dir = {0, -0.05, 0},
        rot = {{0, 0, 1}, 0},
        time = 0.05
    }
    block_yield {joints = all}

    body_move {
        dir = {0, 0.05, 0},
        rot = {{0, 0, 1}, 0},
        time = 0.05
    }
    block_yield {joints = all}
end
body_move {dir = {0, -0.05, 0}, rot = {{0, 0, 1}, 0}, time = 0.2}
block_yield {joints = all}
