import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution

def generate_launch_description():
    robot_package = os.environ['SCHPIN_PACKAGE']

    robot_urdf_filename = os.path.join(get_package_share_directory(robot_package), 'urdf/robot.urdf')
    with open(robot_urdf_filename, 'r') as fd:
        robot_urdf = fd.read()
    robot_srdf_filename = os.path.join(get_package_share_directory(robot_package), 'srdf/robot.srdf')
    with open(robot_srdf_filename, 'r') as fd:
        robot_srdf = fd.read()

    return LaunchDescription([
        DeclareLaunchArgument('script', default_value=None),
        Node(
            package='schpin_motion',
            executable='reactor',
            name='reactor',
            output='both',
            #prefix=['kitty -e cgdb --args'],
            parameters=[{"robot_description": robot_urdf,
                "robot_semantics": robot_srdf,
                "lua_boot_script": LaunchConfiguration('script')
            }]
        )
    ])
