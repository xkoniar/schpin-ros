// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib_exp/event_reactor.h"

#include <schpin_lib/lua/pose.h>

#pragma once

namespace schpin
{
struct set_current_pose_event
{
        static constexpr auto priority = event_priority::DEFAULT;
        slua_pose_input_var   user_input_var;

        set_current_pose_event( slua_pose_input_var var )
          : user_input_var( var )
        {
        }

        set_current_pose_event(
            std::optional< point< 3, world_frame > >   opt_point,
            std::optional< quaternion< world_frame > > opt_quat )
        {
                if ( opt_quat && opt_point ) {
                        user_input_var =
                            slua_pose_input_var{ pose< world_frame >{ *opt_point, *opt_quat } };
                } else if ( opt_quat ) {
                        user_input_var = slua_pose_input_var{ *opt_quat };
                } else if ( opt_point ) {
                        user_input_var = slua_pose_input_var{ *opt_point };
                }
        }
};
struct set_goal_pose_event
{
        static constexpr auto priority = event_priority::DEFAULT;
        slua_pose_input_var   user_input_var;

        set_goal_pose_event( slua_pose_input_var var )
          : user_input_var( var )
        {
        }

        set_goal_pose_event(
            std::optional< point< 3, world_frame > >   opt_point,
            std::optional< quaternion< world_frame > > opt_quat )
        {
                if ( opt_quat && opt_point ) {
                        user_input_var =
                            slua_pose_input_var{ pose< world_frame >{ *opt_point, *opt_quat } };
                } else if ( opt_quat ) {
                        user_input_var = slua_pose_input_var{ *opt_quat };
                } else if ( opt_point ) {
                        user_input_var = slua_pose_input_var{ *opt_point };
                }
        }
};

struct find_body_path_event
{
        static constexpr auto priority   = event_priority::DEFAULT;
        std::size_t           step_limit = 64;  // random constant much
};

struct find_gait_path_event
{
        static constexpr auto priority   = event_priority::DEFAULT;
        std::size_t           step_limit = 64;  // random constant much
};

struct quit_event
{
        static constexpr auto priority = event_priority::LOW;
};

struct export_event
{
        static constexpr auto priority = event_priority::LOW;
        std::string           filename;
};
struct export_path_event
{
        static constexpr auto priority = event_priority::LOW;
        std::string           filename;
};
struct plan_event
{
        static constexpr auto priority = event_priority::LOW;
};

struct make_until_path_event
{
        static constexpr auto priority = event_priority::DEFAULT;
};
struct load_environment_event
{
        static constexpr auto priority = event_priority::DEFAULT;
        std::string           filename;
};
struct show_leg_graph_event
{
        static constexpr auto     priority = event_priority::LOW;
        std::optional< unsigned > index;
};
struct show_workarea_event
{
        static constexpr auto     priority = event_priority::LOW;
        std::optional< unsigned > index;
};
struct show_body_collision_event
{
        static constexpr auto priority = event_priority::LOW;
};
struct print_model_event
{
        static constexpr auto priority = event_priority::LOW;
};
struct print_pose_cache_event
{
        static constexpr auto priority = event_priority::LOW;
};
using robot_print_level_event =
    reactor_event_group< struct RobotPrintLevelTag, print_model_event, print_pose_cache_event >;
struct show_environment_event
{
        static constexpr auto priority = event_priority::LOW;
};
struct clear_event
{
        static constexpr auto priority = event_priority::LOW;
};

}  // namespace schpin
