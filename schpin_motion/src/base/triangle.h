// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "base/physics.h"
#include "schpin_lib/geom/point.h"
#include "schpin_lib/geom/pose.h"
#include "schpin_lib/geom/pose/serialization.h"
#include "schpin_lib/geom/simplex.h"
#include "schpin_lib/geom/triangle.h"

#include <schpin_lib/octree/visitors.h>

#pragma once

namespace schpin
{

template < std::size_t N, typename TagT >
triangle< N, TagT > scale( const triangle< N, TagT >& t, float scale )
{
        auto                              middle = center_of( t );
        std::array< point< N, TagT >, 3 > tmp    = {};

        for ( std::size_t i : em::range( 3u ) ) {
                tmp[i] = middle + ( t[i] - middle ) * scale;
        }

        return triangle< N, TagT >{ tmp };
}

template < std::size_t N, typename TagT >
class sat_triangle_collision_query
{
public:
        using tag                      = TagT;
        using point_iterator           = const point< N, tag >*;
        using separation_axis_iterator = const vec< N >*;
        using item_type                = triangle< N, tag >;

private:
        std::array< vec< N >, 3 > intersection_axis_{};
        std::array< vec< N >, 1 > separation_axis_{};
        const triangle< N, tag >& triangle_;

        static_assert( N == 3, "TriangleCollisionQuery is working only for N = 3" );

public:
        explicit sat_triangle_collision_query( const triangle< N, tag >& tri )
          : triangle_( tri )
        {
                vec< N > side_ab = tri[0] - tri[1];
                vec< N > side_ac = tri[0] - tri[2];
                vec< N > normal  = normalized( cross_product( side_ab, side_ac ) );

                separation_axis_[0]   = normal;
                intersection_axis_[0] = normalized( cross_product( normal, side_ab ) );
                intersection_axis_[1] = normalized( cross_product( normal, tri[2] - tri[1] ) );
                intersection_axis_[2] = normalized( cross_product( normal, side_ac ) );
        }
        const triangle< N, tag >& operator*() const
        {
                return triangle_;
        }
        const auto& points() const
        {
                return triangle_;
        }
        const auto& separation_axes() const
        {
                return separation_axis_;
        }
        const auto& intersection_axes() const
        {
                return intersection_axis_;
        }
};

template < typename TagT >
class bs_triangle_collision_query
{
public:
        using tag       = TagT;
        using item_type = triangle< 3, tag >;
        using sat_type  = sat_triangle_collision_query< 3, tag >;

private:
        point< 3, tag >    sphere_center_;
        em::radius         sphere_radius_;
        triangle< 3, tag > triangle_;

public:
        explicit bs_triangle_collision_query( const triangle< 3, tag >& tri )
          : sphere_radius_( 0 )
          , triangle_( tri )
        {
                sphere_center_ = get_triangle_sphere_center( triangle_ );
                bool is_nan    = em::any_of( sphere_center_, [&]( float f ) {
                        return std::isnan( f );
                } );
                if ( is_nan ) {
                        sphere_center_ =
                            cast_to_point< tag >( em::avg( triangle_, [&]( const auto& p ) {
                                    return cast_to_vec( p );
                            } ) );
                        sphere_radius_ = em::max_elem( triangle_, [&]( const auto& p ) {  //
                                return distance_of( sphere_center_, p );
                        } );
                } else {
                        sphere_radius_ = distance_of( sphere_center_, triangle_[0] );
                }
        }

        const point< 3, tag >& min_sphere_position() const
        {
                return sphere_center_;
        }

        em::radius min_sphere_radius() const
        {
                return sphere_radius_;
        }

        sat_triangle_collision_query< 3, tag > sat() const
        {
                return sat_triangle_collision_query< 3, tag >{ triangle_ };
        }

        bool is_leaf() const
        {
                return true;
        }

        float descent_priority() const
        {
                return *sphere_radius_;
        }

        em::view< bs_triangle_collision_query* > children() const
        {
                return { nullptr, nullptr };
        }
};

}  // namespace schpin
