// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/line.h"

#pragma once

namespace schpin
{

// query of Line for the collision detection system
template < std::size_t N, typename TagT >
class sat_line_collision_query
{

public:
        using tag                      = TagT;
        using axes_container           = std::vector< vec< N > >;
        using item_type                = line< N, tag >;
        using point_iterator           = const point< N, tag >*;
        using separation_axis_iterator = typename axes_container::const_iterator;

private:
        const line< N, tag > line_;
        const axes_container axes_;

public:
        explicit sat_line_collision_query( line< N, tag > line, std::vector< vec< N > > axes = {} )
          : line_( std::move( line ) )
          , axes_( std::move( axes ) )
        {
        }

        const auto& points() const
        {
                return line_;
        }

        const auto& separation_axes() const
        {
                return axes_;
        }

        em::view< const vec< N >* > intersection_axes() const
        {
                return { nullptr, nullptr };
        }
};

}  // namespace schpin
