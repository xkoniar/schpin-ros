// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/generics.h"
#include "graph/generics/edge.h"
#include "graph/generics/vertex.h"
#include "leg/base.h"
#include "leg/graph/base.h"

#pragma once

namespace schpin
{
struct leg_edge_content
{
        leg_price price;
        leg_price get_price() const
        {
                return price;
        }
};
using leg_edge =
    generic_edge< leg_edge_id, leg_edge_content, leg_price, leg_vertex_id, leg_edge_offset >;
}  // namespace schpin

template <>
struct std::hash< schpin::leg_edge_content >
{
        std::size_t operator()( const schpin::leg_edge_content& content )
        {
                return std::hash< schpin::leg_price >()( content.price );
        }
};

namespace schpin
{
template < unsigned N >
struct leg_vertex_content
{
        discrete_angles< N > angles;
};

template < unsigned N >
using leg_vertex = generic_vertex< leg_vertex_id, leg_vertex_content< N >, leg_edge >;

template < unsigned N >
using leg_graph = generic_graph< leg_vertex< N >, leg_edge >;

}  // namespace schpin

template < unsigned N >
struct std::hash< schpin::leg_vertex_content< N > >
{
        std::size_t operator()( const schpin::leg_vertex_content< N >& content )
        {
                return std::hash< schpin::discrete_angles< N > >()( content.angles );
        }
};

template <>
struct nlohmann::adl_serializer< schpin::leg_edge_content >
{
        static void to_json( nlohmann::json& j, const schpin::leg_edge_content& content )
        {
                j["price"] = content.get_price();
        }

        static schpin::leg_edge_content from_json( const nlohmann::json& j )
        {
                return { j.at( "price" ).get< schpin::leg_price >() };
        }
};

template < unsigned N >
struct nlohmann::adl_serializer< schpin::leg_vertex_content< N > >
{
        static void to_json( nlohmann::json& j, const schpin::leg_vertex_content< N >& cont )
        {
                j["angles"] = cont.angles;
        }

        static schpin::leg_vertex_content< N > from_json( const nlohmann::json& j )
        {
                return { j.at( "angles" ).get< schpin::discrete_angles< N > >() };
        }
};
