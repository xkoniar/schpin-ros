// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg/graph.h"
#include "leg/graph/gen.h"
#include "leg/model.h"
#include "schpin_lib/math.h"

#include <schpin_lib/geom/point/collision.h>
#include <schpin_lib/octree/occupancy.h>
#include <schpin_lib/octree/tree.h>

#pragma once

namespace schpin
{

using workarea_oc_tree = oc_tree< occupancy_content >;

template < unsigned N, typename AnglesToPointFunction >
inline workarea_oc_tree
create_workarea( AnglesToPointFunction angle_converison_f, em::length edge_length )
{
        using visitor = oc_collision_visitor<
            oc_collision_checker<
                sat_point_collision_query< 3, oc_frame >,
                bs_point_collision_query< 3, oc_frame >,
                oc_node< occupancy_content > >,
            1 >;

        constexpr std::size_t max_steps = 64;

        std::array< unsigned, N > steps =
            em::map_f_to_a< N >( em::range( N ), [&]( std::size_t ) -> unsigned {
                    return max_steps;
            } );

        workarea_oc_tree res( edge_length, occupancy_content( OC_MIN_OCCUPANCY ) );

        std::size_t count = 0;
        generate_leg_vertexes< 0, N >(
            discrete_angles< N >{}, steps, [&]( discrete_angles< N > angles ) {
                    SCHPIN_INFO_LOG( "leg.workarea", "Workarea vertex count: " << count );
                    point< 3, robot_frame > p = angle_converison_f( angles, max_steps );
                    std::array< point< 3, oc_frame >, 1 > ps{ tag_cast< oc_frame >( p ) };
                    res.expand( ps[0] );
                    visitor vis( ps, occupancy_token::FULL );

                    res.modify( vis );
                    count++;
            } );

        return res;
}

}  // namespace schpin
