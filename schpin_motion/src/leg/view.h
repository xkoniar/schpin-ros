// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "environment.h"
#include "leg.h"

#pragma once

namespace schpin
{

// TODO(squirrel): way a fay for this to be temporal - maybe forbid assigment operator?
template < unsigned N >
class leg_view
{

        const leg< N >&           leg_;
        const pose< world_frame > body_offset_;

public:
        leg_view( const leg< N >& leg, const pose< world_frame >& body_offset )
          : leg_( leg )
          , body_offset_( body_offset )
        {
        }
        const pose< world_frame >& get_body_offset() const
        {
                return body_offset_;
        }

        leg_vertex_id
        find_closest_vertex( const point< 3, world_frame >& position, leg_vertex_id lid ) const
        {
                return find_closest_vertex( position, lid, []( auto ) {
                        return true;
                } );
        }

        template < typename FilterFunction >
        leg_vertex_id find_closest_vertex(
            const point< 3, world_frame >& position,
            leg_vertex_id                  lid,
            FilterFunction&&               fun ) const
        {
                return graph_find_closest_vertex(
                    position,
                    lid,
                    leg_.get_graph(),
                    [&]( const leg_vertex_id& id ) -> point< 3, world_frame > {
                            return generate_tip_position( id );
                    },
                    std::forward< FilterFunction >( fun ) );
        }

        std::array< em::angle, N > get_configuration( leg_vertex_id lid ) const
        {
                return leg_.get_model().get_real_angles(
                    leg_.get_graph().get_vertex( lid )->angles );
        }

        point< 3, world_frame > generate_tip_position( leg_vertex_id lid ) const
        {
                const auto& vertex = leg_.get_graph().get_vertex( lid );
                return tip_position( vertex->angles );
        }

        template < typename Angles >
        point< 3, world_frame > tip_position( const Angles& angles ) const
        {
                return transform( leg_.tip_position( angles ), body_offset_ );
        }

        template < typename Angles, typename BinaryFunction >
        void for_each_joint( const Angles& angles, BinaryFunction f ) const
        {
                pose< world_frame > global_offset =
                    transform( leg_.get_origin_offset(), body_offset_ );
                return leg_.get_model().for_each_joint(
                    angles, [&]( const joint& j, const pose< robot_frame >& offset ) {
                            f( j, transform( offset, global_offset ) );
                    } );
        }

        bool workarea_reaches( const point< 3, world_frame >& p ) const
        {
                bs_point_collision_query   point_query{ p };
                bs_oc_node_collision_query w_query{ leg_.get_workarea().root_view(), body_offset_ };
                return bs_trees_collides(
                    w_query, point_query, []( const auto& leg_query, const auto& ) {
                            return occupancy_treshold_predicate{}( *leg_query.node_view() );
                    } );
        }

        template < typename Angles >
        bool collides( const Angles& angles, const environment& env ) const
        {

                auto env_query = env.get_query();

                bool collides = false;
                for_each_joint( angles, [&]( const joint& j, const pose< world_frame >& offset ) {
                        bs_structure_model_collision_query< world_frame, robot_frame > leg_query{
                            j.sub_link.model, offset };
                        collides |= bs_trees_collides(
                            env_query, leg_query, [&]( const auto& env_sub_query, const auto& ) {
                                    return env_occupancy_treshold_predicate< (
                                        ENV_MAX_OCCUPANCY / 2 ) >{}( *env_sub_query.node_view() );
                            } );
                } );

                return collides;
        }

        template < typename Angles >
        bool near_surface( const Angles& angles, const environment& env ) const
        {
                auto env_query = env.get_query();

                bool near_surface = false;
                for_each_joint( angles, [&]( const joint& j, const pose< world_frame >& offset ) {
                        bs_structure_model_collision_query< world_frame, robot_frame > leg_query{
                            j.sub_link.model, offset };
                        near_surface |= bs_trees_collides(
                            env_query, leg_query, [&]( const auto& env_sub_query, const auto& ) {
                                    return is_surface_predicate{}( *env_sub_query.node_view() );
                            } );
                } );
                return near_surface;
        }

        const leg< N >& operator*() const
        {
                return leg_;
        }

        const leg< N >* operator->() const
        {
                return &leg_;
        }
};

extern template class leg_view< 3 >;

template < unsigned N >
inline bs_oc_node_collision_query< const oc_node< occupancy_content >, world_frame >
get_workarea_query( const leg_view< N >& leg_view )
{
        return bs_oc_node_collision_query{
            leg_view->get_workarea().root_view(), leg_view.get_body_offset() };
}

}  // namespace schpin
