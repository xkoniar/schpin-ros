// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/iterators/access.h"
#include "leg/base.h"
#include "schpin_lib/model/leg.h"
#include "schpin_lib/model/leg/fk.h"
#include "schpin_lib/model/leg/hash.h"
#include "schpin_lib/model/leg/revolute_joint_angle_cache.h"
#include "schpin_lib/model/leg/storage.h"
#include "smodel.h"

#include <utility>

#pragma once

namespace schpin
{

template < unsigned N >
inline std::array< const joint*, N > make_mapping( const joint& root )
{
        std::array< const joint*, N > res;
        std::size_t                   i = 0;

        std::function< void( const joint& ) > f = [&]( const joint& joint ) {
                if ( std::holds_alternative< revolute_joint >( joint.jclass ) ) {
                        SCHPIN_ASSERT( i < N );
                        res[i] = &joint;
                        ++i;
                }
                if ( joint.sub_link.child_ptr ) {
                        f( *joint.sub_link.child_ptr );
                }
        };

        f( root );

        SCHPIN_ASSERT( i == N );

        return res;
}

template < unsigned N >
class leg_model
{
        static constexpr unsigned n = N;

private:
        pose< robot_frame >             origin_offset_;
        std::unique_ptr< joint >        root_joint_ptr_;
        color                           color_;
        std::array< const joint*, N >   mapping_;
        revolute_joint_angle_cache< N > a_cache_;
        fk_solver< static_size< N > >   fk_;

public:
        explicit leg_model( pose< robot_frame > origin_offset, joint root_joint )
          : origin_offset_( origin_offset )
          , root_joint_ptr_( std::make_unique< joint >( std::move( root_joint ) ) )
          , mapping_( make_mapping< N >( *root_joint_ptr_ ) )
          , a_cache_( mapping_ )
          , fk_( origin_offset_, mapping_ )
        {
        }

        leg_model< N > copy() const
        {
                return leg_model< N >{ origin_offset_, root_joint_ptr_->copy() };
        }

        const std::array< const joint*, N >& get_mapping() const
        {
                return mapping_;
        }

        const joint& get_root_joint() const
        {
                return *root_joint_ptr_;
        }

        const color& get_color() const
        {
                return color_;
        }

        const pose< robot_frame >& get_origin_offset() const
        {
                return origin_offset_;
        }

        constexpr std::array< em::angle, N >
        get_real_angles( const discrete_angles< N >& angles ) const
        {
                return a_cache_.discrete_to_angles( angles );
        }

        constexpr point< 3, robot_frame > tip_position( const discrete_angles< N >& angles ) const
        {
                return tip_position( get_real_angles( angles ) );
        }
        point< 3, robot_frame > tip_position( const std::array< em::angle, N >& angles ) const
        {
                return fk_.get_tip_position( angles );
        }

        template < typename BinaryFunction >
        void for_each_joint( const discrete_angles< N >& angles, BinaryFunction f ) const
        {
                for_each_joint( get_real_angles( angles ), f );
        }

        template < typename BinaryFunction >
        void for_each_joint( const std::array< em::angle, N >& angles, BinaryFunction f ) const
        {
                fk_.for_each_joint( angles, f );
        }

        std::array< em::angle, N > neutral_configuration() const
        {
                // TODO: this has to be configured
                return { em::angle{ 0.785f }, em::angle{ 3.1415f }, em::angle{ 3.1415f } };
        }
};

em::either< leg_model< 3 >, urdf_parse_error > parse_leg_model(
    const urdf::Model&               model,
    const urdf::LinkConstSharedPtr&  model_root,
    const urdf::JointConstSharedPtr& root_joint );

}  // namespace schpin

template < unsigned N >
struct std::hash< schpin::leg_model< N > >
{
        std::size_t operator()( const schpin::leg_model< N >& model ) const
        {
                return std::hash< schpin::pose< schpin::robot_frame > >()(
                           model.get_origin_offset() ) ^
                       std::hash< schpin::joint >()( model.get_root_joint() );
        }
};

template < unsigned N >
struct nlohmann::adl_serializer< schpin::leg_model< N > >
{
        static void to_json( nlohmann::json& j, const schpin::leg_model< N >& model )
        {
                j["root_joint"]    = model.get_root_joint();
                j["origin_offset"] = model.get_origin_offset();
        }

        static schpin::leg_model< N > from_json( const nlohmann::json& j )
        {
                return schpin::leg_model< N >{
                    j.at( "origin_offset" ).get< schpin::pose< schpin::robot_frame > >(),
                    j.at( "root_joint" ).get< schpin::joint >() };
        }
};
