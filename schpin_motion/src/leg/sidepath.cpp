// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg/sidepath.h"

namespace schpin
{

template std::vector< env_oc_node_view >
collect_eligible_surface< 3 >( const leg_view< 3 >& leg_view, const environment& env );

template bool is_sidepath_colliding_vertex< 3 >(
    const leg< 3 >&             leg,
    const sidepath_layer_graph& g,
    sidepath_layer_vertex_id    vid,
    const environment&          env );
}  // namespace schpin
