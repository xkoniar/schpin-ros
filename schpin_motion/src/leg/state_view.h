// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg.h"

#pragma once

namespace schpin
{

template < unsigned N >
struct leg_state
{
        std::array< em::angle, N > configuration;
};

template < unsigned N >
inline std::ostream& operator<<( std::ostream& os, const leg_state< N >& rs )
{
        return os << em::view( rs.configuration );
}

template < unsigned N >
class leg_state_view
{

        const leg< N >&         leg_;
        const leg_state< N >&   state_;
        point< 3, robot_frame > tip_position_;

public:
        leg_state_view( const leg< N >& leg, const leg_state< N >& state )
          : leg_( leg )
          , state_( state )
        {
                tip_position_ = leg.tip_position( state.configuration );
        }
        const leg< N >& operator*() const
        {
                return leg_;
        }
        const leg< N >* operator->() const
        {
                return &leg_;
        }

        const point< 3, robot_frame >& get_tip_position() const
        {
                return tip_position_;
        }

        bool is_standing() const
        {
                return true;  // TODO: implement
        }
};

}  // namespace schpin
