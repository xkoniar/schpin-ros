// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg.h"
#include "leg/data.h"
#include "leg/graph/gen.h"

#include <filesystem>
#include <utility>

#pragma once

namespace schpin
{

template < unsigned N >
leg_data< N > generate_leg_data( const std::string& name, leg_model< N >&& model )
{
        SCHPIN_INFO_LOG( "leg", "Generating dataset for: " << name );
        leg_graph< N > graph = generate_leg_graph< leg_graph< N > >( model );

        SCHPIN_INFO_LOG( "leg", "LegGraph created, size: " << graph.size() );

        auto leg_bound_cube =
            dimensional_min_max_elem( graph.get_vertexes(), [&]( const auto& vertex ) {
                    return model.tip_position( vertex->angles );
            } );
        SCHPIN_INFO_LOG( "leg", "graph area: " << leg_bound_cube );

        auto angle_conversion = [&]( const discrete_angles< N >& dangles, std::size_t max_step ) {
                auto angles = em::map_f_to_a< N >( em::range( N ), [&]( std::size_t i ) {
                        const joint* j = model.get_mapping()[i];

                        return em::match(
                            j->jclass,
                            [&]( const revolute_joint& rj ) -> em::angle {
                                    return rj.min_angle + ( rj.max_angle - rj.min_angle ) *
                                                              float( dangles[i] ) /
                                                              float( max_step - 1 );
                            },
                            [&]( const fixed_joint& ) -> em::angle {
                                    SCHPIN_ASSERT( false );
                                    return em::angle{ 0 };
                            } );
                } );
                return model.tip_position( angles );
        };

        workarea_oc_tree workarea = create_workarea< N >( angle_conversion, em::length( 0.04f ) );
        SCHPIN_INFO_LOG( "leg", "Workarea created: " << workarea.bare_root().recursive_count() );

        return leg_data< N >{ name, std::move( model ), std::move( graph ), std::move( workarea ) };
}

using leg_boot_error = error< struct leg_boot_error_tag >;

template < unsigned N >
em::either< leg_data< N >, leg_boot_error > load_leg_data(
    const std::filesystem::path& prefix,
    const std::string&           name,
    leg_model< N >&& /*model*/ )
{
        SCHPIN_INFO_LOG( "leg", "Loading generated data for: " << name );
        std::ifstream in_stream{ prefix / "leg.json" };
        return safe_load_json< leg_data< N > >( in_stream )
            .convert_right( [&]( const json_storage_error& e ) {
                    return E( leg_boot_error ).attach( e )
                           << "Failed to load leg " << name << " data";
            } );
}

template < unsigned N >
opt_error< leg_boot_error >
save_leg_data( const std::filesystem::path& prefix, const leg_data< N >& data )
{
        SCHPIN_INFO_LOG( "leg", "Saving generated data for: " << data.name );
        std::ofstream out_stream{ prefix / "leg.json" };
        return safe_store_json( out_stream, data ).convert( [&]( const json_storage_error& e ) {
                return E( leg_boot_error ).attach( e ) << "Failed to save leg data to json error";
        } );
}

template < unsigned N >
em::either< leg_data< N >, leg_boot_error > load_or_generate_leg_data(
    const std::filesystem::path&     cache_prefix,
    const std::string&               name,
    const urdf::Model&               model,
    const urdf::LinkConstSharedPtr&  model_root,
    const urdf::JointConstSharedPtr& root_joint )
{
        // TODO(squirrel): hash is not saved
        std::filesystem::path prefix{ cache_prefix / name };

        std::error_code err_code;
        if ( !std::filesystem::exists( prefix ) && !create_directories( prefix, err_code ) ) {
                return {
                    E( leg_boot_error )
                    << "Cache folder " << prefix << " does not exists, and failed to create."
                    << err_code.message() };
        }

        return parse_leg_model( model, model_root, root_joint )
            .convert_right( [&]( urdf_parse_error&& error ) {
                    return E( leg_boot_error ).attach( error )
                           << "Failed to load leg data due to URDF errors in leg: " << name;
            } )
            .bind_left(
                [&]( leg_model< N >&& leg_mod ) -> em::either< leg_data< N >, leg_boot_error > {
                        std::ifstream hash_file{ prefix / "hash" };
                        if ( hash_file ) {
                                std::string stored_hash;
                                hash_file >> stored_hash;

                                if ( stored_hash ==
                                     std::to_string( std::hash< leg_model< N > >()( leg_mod ) ) ) {
                                        return {
                                            load_leg_data( prefix, name, std::move( leg_mod ) ) };
                                }
                        }

                        std::ofstream hash_out{ prefix / "hash" };
                        hash_out << std::hash< leg_model< N > >()( leg_mod );

                        leg_data< N > l_data = generate_leg_data( name, std::move( leg_mod ) );
                        opt_error< leg_boot_error > err = save_leg_data( prefix, l_data );

                        if ( err ) {
                                return { *err };
                        }
                        return { std::move( l_data ) };
                } );
}

inline std::vector< em::either< leg< 3 >, leg_boot_error > > map_srdf_roots_to_legs(
    const std::filesystem::path& cache_prefix,
    const urdf::Model&           model,
    const srdf_information&      srdf_info )
{

        urdf::LinkConstSharedPtr model_root = model.getLink( srdf_info.body_link );
        SCHPIN_ASSERT( model_root );
        return map_f_to_v(
            em::enumerate( srdf_info.legs_info ),
            [&]( std::tuple< leg_id, srdf_leg > pack ) -> em::either< leg< 3 >, leg_boot_error > {
                    auto [id, linfo] = pack;

                    auto root_ptr = model.getJoint( linfo.root_joint );
                    if ( !root_ptr ) {
                            return {
                                E( leg_boot_error ) << "Root joint " << linfo.root_joint
                                                    << " is missing in the "
                                                       "model." };
                    }
                    return load_or_generate_leg_data< 3 >(
                               cache_prefix, linfo.name, model, model_root, root_ptr )
                        .convert_left( [&]( leg_data< 3 >&& data ) {
                                leg< 3 > leg( id, std::move( data ) );
                                ++id;
                                return leg;
                        } );
            } );
}

inline em::either< leg_container< leg< 3 > >, leg_boot_error > load_legs(
    const std::filesystem::path& cache_prefix,
    const urdf::Model&           model,
    const srdf_information&      srdf_info )
{

        auto legs_eithers = map_srdf_roots_to_legs( cache_prefix, model, srdf_info );

        partitioned_eithers< leg< 3 >, leg_boot_error > partitioned =
            partition_eithers( std::move( legs_eithers ) );

        if ( !partitioned.right.empty() ) {
                return {
                    E( leg_boot_error ).group_attach( partitioned.right )  //
                    << "Failed to load legs:" };
        }
        leg_container< leg< 3 > > legs = map_f_to_l( std::move( partitioned.left ) );
        return { std::move( legs ) };
}

}  // namespace schpin
