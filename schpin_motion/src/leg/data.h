// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/astar.h"
#include "graph/astar/storage.h"
#include "graph/dstar.h"
#include "graph/dstar/storage.h"
#include "leg/graph.h"
#include "leg/model.h"
#include "leg/workarea.h"
#include "util/heaps.h"

#include <schpin_lib/octree/json.h>

#pragma once

namespace schpin
{

template < typename Vertex >
class leg_heuristics
{
public:
        leg_price operator()( const Vertex&, const Vertex& )
        {
                return leg_price{ 0.f };
        }

        bool minimal()
        {
                return true;
        }

        void decrease()
        {
        }
};

template < unsigned N, template < class > class HeapT >
struct leg_d_star_config
{
        using queue_item         = dstar_queue_item< leg_vertex_id, leg_price >;
        using heap               = HeapT< queue_item >;
        using heap_handle        = typename heap::handle_type;
        using graph              = leg_graph< N >;
        using heuristic_function = leg_heuristics< typename graph::vertex >;
};

template < unsigned N, template < class > class HeapT >
struct leg_a_star_config
{
        using queue_item         = a_star_queue_item< leg_vertex_id, leg_price >;
        using heap               = HeapT< queue_item >;
        using heap_handle        = typename heap::handle_type;
        using graph              = leg_graph< N >;
        using heuristic_function = leg_heuristics< typename graph::vertex >;
};

template < unsigned N >
struct leg_data
{

        // TODO(squirrel): I have bad feeling about consistency here...
        std::string      name;
        leg_model< N >   model;
        leg_graph< N >   graph;
        workarea_oc_tree workarea;

        leg_data< N > copy() const
        {
                return leg_data< N >{ name, model.copy(), graph, workarea.copy() };
        }
};

}  // namespace schpin

template < unsigned N >
struct nlohmann::adl_serializer< schpin::leg_data< N > >
{
        static void to_json( nlohmann::json& j, const schpin::leg_data< N >& data )
        {
                j["name"]     = data.name;
                j["model"]    = data.model;
                j["graph"]    = data.graph;
                j["workarea"] = data.workarea;
        }

        static schpin::leg_data< N > from_json( const nlohmann::json& j )
        {
                using namespace schpin;
                return leg_data< N >{
                    j.at( "name" ).get< std::string >(),
                    j.at( "model" ).get< leg_model< N > >(),
                    j.at( "graph" ).get< leg_graph< N > >(),
                    j.at( "workarea" ).get< workarea_oc_tree >() };
        }
};

template < unsigned N >
struct std::hash< schpin::leg_data< N > >
{
        std::size_t operator()( const schpin::leg_data< N >& data )
        {
                return std::hash< std::string >()( data.name ) ^
                       std::hash< schpin::leg_model< N > >()( data.model ) ^
                       std::hash< schpin::leg_graph< N > >()( data.graph ) ^
                       std::hash< schpin::workarea_oc_tree >()( data.workarea );
        }
};
