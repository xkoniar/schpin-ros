// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "collission_interfaces.h"
#include "leg.h"
#include "schpin_lib_exp/rviz.h"

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>

#pragma once

namespace schpin
{

inline visualization_msgs::Marker
createLegGraphMarker( const std::string& ns, unsigned id, const std::string& frame )
{
        visualization_msgs::Marker res = create_marker( ns, id, frame );

        res.type = visualization_msgs::Marker::POINTS;

        res.scale.x = 0.001;
        res.scale.y = 0.001;
        res.scale.z = 0.001;

        res.color.r = 1.0f;
        res.color.g = 0.0f;
        res.color.b = 0.0f;
        res.color.a = 1.0;

        return res;
}

template < unsigned N >
visualization_msgs::Marker createLegGraphVisualization( const Leg< N >& leg )
{
        visualization_msgs::Marker res = createLegGraphMarker( leg.getName(), 1, leg.getFrame() );
        for ( const auto& vertex : leg.getGraph() ) {
                point< 3, LegBaseFrame > p =
                    leg.getModel().tipPosition( vertex.getDiscreteAngles() );

                geometry_msgs::Point coord = point_to_msg( p );
                res.points.push_back( coord );
        }
        return res;
}

}  // namespace schpin
