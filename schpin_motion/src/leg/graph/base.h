// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/generics/edge.h"
#include "schpin_lib/identificator.h"

#include <emlabcpp/quantity.h>
#include <nlohmann/json.hpp>

#pragma once

namespace schpin
{
using leg_price       = em::timeq;
using leg_vertex_id   = identificator< struct LegVertexIDTag, 32 >;
using leg_edge_offset = identificator< struct LegEdgeOffsetTag, 32 >;
using leg_edge_id     = generic_edge_id< leg_vertex_id, leg_edge_offset >;

}  // namespace schpin
