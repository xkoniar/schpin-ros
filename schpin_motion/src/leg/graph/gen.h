// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg/base.h"
#include "leg/graph.h"
#include "leg/model.h"

#pragma once

namespace schpin
{
template < unsigned I, unsigned N, typename OutputFunction >
void generate_leg_vertexes(
    discrete_angles< N >             angles,
    const std::array< unsigned, N >& steps,
    OutputFunction                   out_f )
{
        if constexpr ( I == N ) {
                out_f( angles );
        } else {
                for ( unsigned step : em::range( steps[I] ) ) {
                        angles[I] = static_cast< uint8_t >( step );
                        generate_leg_vertexes< I + 1, N >( angles, steps, out_f );
                }
        }
}

template < typename Graph, unsigned N >
inline Graph generate_leg_graph( const std::array< unsigned, N > steps )
{

        using vertex         = typename Graph::vertex;
        using vertex_content = typename vertex::content_type;
        Graph result;

        generate_leg_vertexes< 0, N >(
            discrete_angles< N >{}, steps, [&]( discrete_angles< N > angles ) {
                    result.emplace_vertex( vertex_content{ angles } );
            } );

        for ( const vertex& v : result.get_vertexes() ) {
                for ( const vertex& other : result.get_vertexes() ) {
                        unsigned max_dist =
                            em::max_elem( v->angles - other->angles, []( unsigned diff ) {  //
                                    return diff;
                            } );

                        if ( max_dist == 1 ) {
                                result.add_edge(
                                    v.get_id(),
                                    other.get_id(),
                                    leg_edge_content{ leg_price( 0 ) } );
                        }
                }
        }

        return result;
}

template < typename Graph, unsigned N >
leg_price eval_edge_price( const leg_edge& e, const Graph& g, const leg_model< N >& model )
{
        const auto& source = g.get_vertex( e.get_source_id() );
        const auto& target = g.get_vertex( e.get_target_id() );

        std::array< em::angle, N > source_angles = model.get_real_angles( source->angles );
        std::array< em::angle, N > target_angles = model.get_real_angles( target->angles );

        return em::max_elem( em::range( N ), [&]( std::size_t i ) -> em::timeq {
                auto diff = abs( source_angles[i] - target_angles[i] );
                return em::match(
                    model.get_mapping()[i]->jclass,
                    [&]( const revolute_joint& rj ) -> em::timeq {
                            return diff / rj.max_velocity;
                    },
                    [&]( const fixed_joint& ) -> em::timeq {
                            return em::timeq{ 0 };
                    } );
        } );
}

template < typename Graph, unsigned N >
inline Graph generate_leg_graph( const leg_model< N >& model )
{
        auto steps = em::map_f_to_a( model.get_mapping(), []( const joint* j ) {
                return em::match(
                    j->jclass,
                    [&]( const revolute_joint& rj ) -> unsigned {
                            return rj.state_space_steps;
                    },
                    [&]( const fixed_joint& ) -> unsigned {
                            return 0;
                    } );
        } );

        Graph g = generate_leg_graph< Graph, N >( steps );

        for ( const auto& vertex : g.get_vertexes() ) {
                for ( const auto& edge : vertex.get_edges() ) {
                        auto& edge_content = g.ref_edge_content( edge.get_id() );
                        edge_content.price = eval_edge_price( edge, g, model );
                }
        }
        return g;
}

}  // namespace schpin
