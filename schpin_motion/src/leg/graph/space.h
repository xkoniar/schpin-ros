// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg/graph.h"
#include "schpin_lib/geom/simplex.h"
#include "schpin_lib/util.h"

#pragma once

namespace schpin
{

template < std::size_t K, typename Vertex >
bool covers( const simplex< leg_vertex_id, K >& simplex, const Vertex& vertex )
{
        return em::all_of( simplex, [&]( const leg_vertex_id& id ) {
                return em::any_of( vertex.get_edges(), [&]( const leg_edge& edge ) {  //
                        return edge.get_target_id() == id;
                } );
        } );
}

template < std::size_t K, std::size_t N, typename Graph >
std::vector< leg_vertex_id > find_promotable_vertexes(
    const simplex< leg_vertex_id, K >&  actual,
    const std::vector< leg_vertex_id >& neighbour_ids,
    const Graph&                        g )
{
        std::vector< leg_vertex_id > res;

        for ( leg_vertex_id id : neighbour_ids ) {
                if ( !covers( actual, g.get_vertex( id ) ) ) {
                        continue;
                }
                if ( em::none_of( actual, [&]( leg_vertex_id pres_id ) {
                             return pres_id == id;
                     } ) ) {
                        res.push_back( id );
                }
        }
        return res;
}

template < std::size_t K, std::size_t N, typename Graph >
struct simplex_search
{
        static std::vector< simplex< leg_vertex_id, N > > find_simplexes(
            const simplex< leg_vertex_id, K >&  actual,
            const std::vector< leg_vertex_id >& neighbour_ids,
            const Graph&                        g )
        {
                std::vector< simplex< leg_vertex_id, N > > res;
                static_assert( K < N );
                std::vector< leg_vertex_id > promotables =
                    find_promotable_vertexes< K, N, Graph >( actual, neighbour_ids, g );

                for ( leg_vertex_id id : promotables ) {
                        simplex< leg_vertex_id, K + 1 > sim{ actual, id };

                        std::vector< simplex< leg_vertex_id, N > > tmp =
                            simplex_search< K + 1, N, Graph >::find_simplexes(
                                sim, neighbour_ids, g );
                        res.insert( res.end(), tmp.begin(), tmp.end() );
                }
                return res;
        }
};

template < std::size_t N, typename Graph >
struct simplex_search< N, N, Graph >
{
        static std::vector< simplex< leg_vertex_id, N > > find_simplexes(
            const simplex< leg_vertex_id, N >& actual,
            const std::vector< leg_vertex_id >& /*neighbour_ids*/,
            const Graph& /*g*/ )
        {
                std::vector< simplex< leg_vertex_id, N > > res;
                res.push_back( actual );
                return res;
        }
};

inline std::array< vec< 3 >, 4 >
get_separation_axes_simplex_collision( std::array< point< 3, no_frame >, 4 >& points )
{
        return std::array< vec< 3 >, 4 >{
            normal_of( triangle< 3, no_frame >( points[0], points[1], points[2] ) ),
            normal_of( triangle< 3, no_frame >( points[0], points[1], points[3] ) ),
            normal_of( triangle< 3, no_frame >( points[0], points[2], points[3] ) ),
            normal_of( triangle< 3, no_frame >( points[1], points[2], points[3] ) ) };
}
inline std::array< vec< 2 >, 3 >
get_separation_axes_simplex_collision( std::array< point< 2, no_frame >, 3 >& points )
{
        return std::array< vec< 2 >, 3 >{
            normal_of( points[0] - points[1] ),
            normal_of( points[1] - points[2] ),
            normal_of( points[2] - points[0] ) };
}

template < std::size_t N >
class simplex_graph_collision_query
{

        static constexpr std::size_t t_count = N + 1;

        std::array< point< N, no_frame >, N + 1 > points_;
        std::array< vec< N >, t_count >           intersection_axes_;

        static_assert(
            N == 3 || N == 2,
            " For this to work, we have to assert that separation axis theorem works "
            "for n-dimensional "
            "simplexes "
            "and we know how to get a relevant axes for the algo... that should be "
            "solved later and for now "
            "deal "
            "with 3 dimensions only " );

public:
        using tag                      = no_frame;
        using point_iterator           = const point< N, no_frame >*;
        using separation_axis_iterator = const vec< N >*;

        template < typename Graph >
        simplex_graph_collision_query( const Graph& g, const simplex< leg_vertex_id, N >& simplex )
          : points_()
          , intersection_axes_()
        {
                for ( std::size_t i : em::range( N + 1 ) ) {
                        discrete_angles< N > angles = g.get_vertex( simplex[i] )->angles;

                        points_[i] =
                            point< N, no_frame >{ em::map_f_to_a( angles, []( uint8_t val ) {  //
                                    return float( val );
                            } ) };
                }

                intersection_axes_ = get_separation_axes_simplex_collision( points_ );
        }

        const auto& points() const
        {
                return points_;
        }

        const auto& intersection_axes() const
        {
                return intersection_axes_;
        }

        em::view< separation_axis_iterator > separation_axes() const
        {
                return { nullptr, nullptr };
        }
};

template < typename Graph, std::size_t N >
std::vector< simplex< leg_vertex_id, N > > autogenerate_simplexes( const Graph& g )
{
        using vertex = typename Graph::vertex;

        std::vector< simplex< leg_vertex_id, N > > res;

        for ( const vertex& ver : g.get_vertexes() ) {
                auto neighbours = map_f_to_v( ver.get_edges(), [&]( const leg_edge& edge ) {  //
                        return edge.get_target_id();
                } );

                simplex< leg_vertex_id, 0 > actual{ ver.get_id() };

                std::vector< simplex< leg_vertex_id, N > > tmp =
                    simplex_search< 0, N, Graph >::find_simplexes( actual, neighbours, g );

                for ( const simplex< leg_vertex_id, N >& sim : tmp ) {
                        bool in_collision =
                            em::any_of( res, [&]( const simplex< leg_vertex_id, N >& accepted ) {
                                    simplex_graph_collision_query< N > acc_q( g, accepted );
                                    simplex_graph_collision_query< N > sim_q( g, sim );
                                    return sat_collides( acc_q, sim_q );
                            } );

                        if ( in_collision ) {
                                continue;
                        }

                        res.push_back( sim );
                }
        }

        std::sort( res.begin(), res.end() );
        auto iter = std::unique( res.begin(), res.end() );

        res.erase( iter, res.end() );

        return res;
}

}  // namespace schpin
