// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg/data.h"
#include "leg/view.h"

#pragma once

namespace schpin
{

struct leg_timing_record
{

        std::vector< std::chrono::duration< double > > collides{};
        std::vector< std::chrono::duration< double > > near_surface{};
        std::size_t                                    path_length;

        static auto now()
        {
                return std::chrono::system_clock::now();
        }
};

using leg_planner_error = error< struct leg_planner_error_tag >;

template < unsigned N >
class leg_planner
{
public:
        using graph_search = d_star< leg_d_star_config< N, binary_heap > >;

        enum col_state
        {
                UNCHECKED,
                COLLIDES,
                NEAR_COLLISION,
                NOT_COLLIDING
        };

private:
        using state_cont = container< col_state, leg_vertex_id, std::vector >;

        graph_search sg_;
        state_cont   col_states_;

public:
        explicit leg_planner( const leg_graph< N >& g )
          : sg_( g )
          , col_states_( g.get_vertexes().size() )
        {
        }

        leg_vertex_id find_closest_vertex(
            const leg_view< N >&           lview,
            const point< 3, world_frame >& position,
            leg_vertex_id                  lid,
            const environment&             env )
        {
                return lview.find_closest_vertex( position, lid, [&]( leg_vertex_id lid ) {
                        auto [vstate, mb_col, mb_surf] = make_vertex_check( lview, lid, env );
                        em::ignore( mb_col );
                        em::ignore( mb_surf );
                        return vstate != COLLIDES;
                } );
        }

        struct check_res
        {
                col_state                                        state;
                std::optional< std::chrono::duration< double > > mb_collides_tm{};
                std::optional< std::chrono::duration< double > > mb_surface_tm{};
        };

        check_res
        make_vertex_check( const leg_view< N >& lview, leg_vertex_id vid, const environment& env )
        {
                const leg_graph< N >& g = lview->get_graph();
                if ( col_states_[vid] != UNCHECKED ) {
                        return { .state = col_states_[vid] };
                }
                auto cstart   = leg_timing_record::now();
                bool collides = lview.collides( g.get_vertex( vid )->angles, env );
                auto col_time = leg_timing_record::now() - cstart;
                if ( collides ) {
                        col_states_[vid] = COLLIDES;
                        return { .state = COLLIDES, .mb_collides_tm = col_time };
                }
                auto sstart       = leg_timing_record::now();
                bool near_surface = lview.near_surface( g.get_vertex( vid )->angles, env );
                auto sur_time     = leg_timing_record::now() - sstart;
                if ( near_surface ) {
                        col_states_[vid] = NEAR_COLLISION;
                } else {
                        col_states_[vid] = NOT_COLLIDING;
                }
                return {
                    .state          = col_states_[vid],
                    .mb_collides_tm = col_time,
                    .mb_surface_tm  = sur_time };
        }

        std::pair<
            em::either< chain< leg_edge_id, leg_vertex_id >, leg_planner_error >,
            leg_timing_record >
        get_path(
            const leg_view< N >& leg_view,
            leg_vertex_id        from,
            leg_vertex_id        to,
            const environment&   env )
        {
                leg_timing_record     timings{};
                const leg_graph< N >& g = leg_view->get_graph();
                sg_.set_task( g, from, to );
                chain< leg_edge_id, leg_vertex_id > p;
                chain< leg_edge_id, leg_vertex_id > prev;
                do {
                        sg_.compute_shortest_path( g, std::numeric_limits< std::size_t >::max() );
                        prev             = p;
                        p                = sg_.get_path( g );
                        bool is_bad_path = em::any_of( p.get_pins(), [&]( leg_vertex_id vid ) {
                                return sg_.get_extra_price( g, vid ) ==
                                       std::numeric_limits< leg_price >::max();
                        } );

                        if ( is_bad_path ) {
                                return {
                                    { E( leg_planner_error )
                                      << "Failed to plan movement for "
                                         "leg from: "
                                      << from << " to: " << to
                                      << "; max price on path: " << em::view{ p.get_pins() } },
                                    timings };
                        }
                        // THIS may cycle
                        for ( leg_vertex_id vid : p.get_pins() ) {
                                auto [vstate, mb_col, mb_sur] =
                                    make_vertex_check( leg_view, vid, env );
                                if ( mb_col ) {
                                        timings.collides.push_back( *mb_col );
                                }
                                if ( mb_sur ) {
                                        timings.near_surface.push_back( *mb_sur );
                                }
                                if ( vstate == COLLIDES ) {
                                        sg_.set_extra_price(
                                            g, vid, std::numeric_limits< leg_price >::max() );
                                }
                                if ( vstate == NEAR_COLLISION ) {
                                        // TODO: random constnat much, but have to keep in sync with
                                        // 'leg_heuristics'
                                        sg_.set_extra_price( g, vid, leg_price{ 100 } );
                                }
                        }
                } while ( prev != p );
                timings.path_length = p.size();
                return { { p }, timings };
        }
};

extern template class leg_planner< 3 >;

}  // namespace schpin

template < unsigned N >
struct nlohmann::adl_serializer< schpin::leg_planner< N > >
{
        static void to_json( nlohmann::json& j, const schpin::leg_planner< N >& data )
        {
                j["graph"] = data.getGraph();
        }

        static schpin::leg_planner< N > from_json( const nlohmann::json& j )
        {
                return schpin::leg_planner< N >{ j.at( "graph" ).get< schpin::leg_graph< N > >() };
        }
};

template < unsigned N >
struct std::hash< schpin::leg_planner< N > >
{
        std::size_t operator()( const schpin::leg_planner< N >& planner )
        {
                // TODO maybe hash the graph search too?
                return std::hash< schpin::leg_graph< N > >()( planner.getGraph() );
        }
};
