// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/iterators/access.h"
#include "environment.h"
#include "graph/astar.h"
#include "leg.h"
#include "leg/sidepath/graph.h"
#include "leg/view.h"
#include "robot.h"
#include "robot/state_view.h"
#include "schpin_lib/geom/point.h"
#include "schpin_lib_exp/rviz.h"
#include "util/heaps.h"

#include <vector>

#pragma once

namespace schpin
{
using sidepath = std::vector< point< 3, world_frame > >;

template < unsigned N >
std::vector< env_oc_node_view >
collect_eligible_surface( const leg_view< N >& leg_view, const environment& env )
{
        std::vector< env_oc_node_view > node_views;

        auto opt_env_box =
            env.get_tree().root_view().recursive_matched_area( is_surface_predicate{} );
        auto opt_workarea_box = leg_view->get_workarea().root_view().recursive_matched_area(
            occupancy_treshold_predicate{} );
        SCHPIN_ASSERT( opt_env_box )
        SCHPIN_ASSERT( opt_workarea_box );

        bs_find_collision_nodes(
            get_workarea_query( leg_view ),
            env.get_query(),
            occupancy_collides_surface_predicate{},
            [&]( const auto&, const auto& env_query ) {  // collect function of colliding leafs
                    node_views.push_back( env_query.node_view() );
            } );

        return node_views;
}

extern template std::vector< env_oc_node_view >
collect_eligible_surface< 3 >( const leg_view< 3 >& leg_view, const environment& env );

// detects that there exists vertex (filtered by 'filter' function) that does not collide with the
// environment
template < typename LView, typename UnaryFunction >
bool leg_collisionless_config_exists(
    const LView&       leg_view,
    const environment& env,
    UnaryFunction      filter )
{
        return em::any_of( leg_view->get_graph().get_vertexes(), [&]( const auto& leg_vertex ) {
                if ( !filter( leg_vertex ) ) {
                        return false;
                }

                return !leg_view.collides( leg_vertex->angles, env );
        } );
}

struct side_path_layer_a_star_config
{
        using queue_item         = a_star_queue_item< sidepath_layer_vertex_id, em::distance >;
        using heap               = pairing_heap< queue_item >;
        using heap_handle        = typename heap::handle_type;
        using graph              = sidepath_layer_graph;
        using heuristic_function = zero_graph_heuristics< em::distance >;
};

using sidepath_layer_search = a_star< side_path_layer_a_star_config >;

struct sidepath_failure_report
{
        leg_id                     lid;
        std::vector< std::size_t > empty_body_poses;
};

inline std::ostream& operator<<( std::ostream& os, const sidepath_failure_report& rep )
{
        return os << "Leg " << rep.lid << ", empty body poses on body_path indexes: "
                  << em::view{ rep.empty_body_poses };
}
template < unsigned N >
inline bool is_sidepath_colliding_vertex(
    const leg< N >&             leg,
    const sidepath_layer_graph& g,
    sidepath_layer_vertex_id    vid,
    const environment&          env )
{
        auto& layer_vertex = g.get_vertex( vid );
        if ( layer_vertex->collide_check ) {
                return false;
        }

        leg_view l_view( leg, layer_vertex->body_pose );

        bool has_collisionless_config =
            leg_collisionless_config_exists( l_view, env, [&]( const auto& leg_vertex ) {
                    return layer_vertex->env_node->overlaps(
                        tag_cast< oc_frame >( l_view.tip_position( leg_vertex->angles ) ) );
            } );

        return !has_collisionless_config;
}

extern template bool is_sidepath_colliding_vertex< 3 >(
    const leg< 3 >&             leg,
    const sidepath_layer_graph& g,
    sidepath_layer_vertex_id    vid,
    const environment&          env );

inline std::vector< point< 3, world_frame > > sidepath_grap_path_to_segments(
    const chain< sidepath_layer_edge_id, sidepath_layer_vertex_id >& sidepath,
    const sidepath_layer_graph&                                      g )
{
        std::vector< point< 3, world_frame > > res;
        for ( const auto& vertex_id : sidepath.get_pins() ) {
                const auto& layer_vertex = g.get_vertex( vertex_id );
                if ( !layer_vertex->env_node ) {
                        continue;
                }
                res.push_back( layer_vertex->position );
        }
        return res;
}

class leg_sidepath_generator
{
        sidepath_layer_graph  g_;
        sidepath_layer_search search_;
        leg_id                lid_;

public:
        leg_sidepath_generator(
            leg_id                   lid,
            sidepath_layer_graph     g,
            sidepath_layer_vertex_id from,
            sidepath_layer_vertex_id to )
          : g_( std::move( g ) )
          , search_( g_ )
          , lid_( lid )
        {
                search_.set_task( g_, from, to );
        }

        leg_id get_leg_id() const
        {
                return lid_;
        }

        const sidepath_layer_graph& get_graph() const
        {
                return g_;
        }

        void remove_vertex( sidepath_layer_vertex_id vid )
        {
                g_.remove_vertex( vid, search_.make_graph_visitor() );
        }

        // TODO: this smells, for this to work correctly, the _correct_ leg has to be passed :/
        template < unsigned N >
        std::vector< point< 3, world_frame > >
        find_sidepath( const leg< N >& leg, const environment& env )
        {

                while ( !g_.empty() ) {
                        search_.compute_shortest_path(
                            g_, std::numeric_limits< std::size_t >::max() );
                        auto sidepath = search_.get_path( g_ );
                        // searches for first colliding vertex
                        auto iter =
                            em::find_if( sidepath.get_pins(), [&]( const auto& sidepath_vid ) {
                                    bool is_colliding_vertex =
                                        is_sidepath_colliding_vertex( leg, g_, sidepath_vid, env );
                                    if ( is_colliding_vertex ) {
                                            return true;
                                    }
                                    g_.ref_content( sidepath_vid ).collide_check = true;
                                    return false;
                            } );
                        // if none, we are save!
                        if ( iter == sidepath.get_pins().end() ) {
                                return sidepath_grap_path_to_segments( sidepath, g_ );
                        }
                        remove_vertex( *iter );
                }
                return std::vector< point< 3, world_frame > >{};
        }
};

template < typename Poses, unsigned N >
em::either< leg_sidepath_generator, sidepath_failure_report > create_sidepath_generator(
    const Poses&                   body_poses,
    const leg< N >&                leg,
    const point< 3, world_frame >& actual_tip_pos,
    const environment&             env )
{

        sidepath_layer_graph layer_graph;

        sidepath_layer_vertex_id last_eid =
            layer_graph.emplace_vertex( sidepath_layer_vertex_content{} ).get_id();
        sidepath_layer_vertex_id first_eid =
            layer_graph.emplace_vertex( sidepath_layer_vertex_content( actual_tip_pos ) ).get_id();

        std::vector< sidepath_layer_vertex_id > prev_layer = { first_eid };
        std::vector< sidepath_layer_vertex_id > actual_layer;

        sidepath_failure_report report;
        report.lid = leg.get_id();

        // function to connect all vertexes from layer to 'actual_id' vertex
        auto connect_layer_f = [&]( const sidepath_layer_vertex_id                 actual_id,
                                    const std::vector< sidepath_layer_vertex_id >& layer ) {
                const auto&                    actual     = layer_graph.get_vertex( actual_id );
                const point< 3, world_frame >& actual_pos = actual->position;
                for ( sidepath_layer_vertex_id prev_id : layer ) {
                        const auto&  prev  = layer_graph.get_vertex( prev_id );
                        em::distance dist  = distance_of( prev->position, actual_pos );
                        em::distance price = dist + actual->price;
                        layer_graph.add_edge(
                            prev_id, actual_id, sidepath_layer_edge_content{ price } );
                }
        };

        // generate all layers and interconnect them
        for ( const auto& [i, body_pose] : em::enumerate( body_poses ) ) {
                leg_view< 3 >           leg_view{ leg, body_pose };
                point< 3, world_frame > ideal_tip_pos =
                    leg_view.tip_position( leg_view->get_model().neutral_configuration() );

                std::vector< env_oc_node_view > env_nodes =
                    collect_eligible_surface( leg_view, env );

                for ( const env_oc_node_view& node : env_nodes ) {
                        em::distance price_bias = distance_of(
                            node.center_position(), tag_cast< oc_frame >( ideal_tip_pos ) );
                        sidepath_layer_vertex_content vertex_content(
                            std::make_shared< env_oc_node_view >( node ),
                            tag_cast< world_frame >( node.center_position() ),
                            price_bias,
                            body_pose );
                        sidepath_layer_vertex_id actual_id =
                            layer_graph.emplace_vertex( vertex_content ).get_id();

                        connect_layer_f( actual_id, prev_layer );
                        actual_layer.push_back( actual_id );
                }

                if ( actual_layer.empty() ) {
                        report.empty_body_poses.push_back( i );
                }

                prev_layer = actual_layer;
                actual_layer.clear();
        }

        if ( !report.empty_body_poses.empty() ) {
                return { report };
        }

        // connect last layer to final vertex
        for ( sidepath_layer_vertex_id vid : prev_layer ) {
                layer_graph.add_edge(
                    vid, last_eid, sidepath_layer_edge_content{ em::distance( 0. ) } );
        }

        return { leg_sidepath_generator{ leg.get_id(), layer_graph, first_eid, last_eid } };
}

template < typename Container >
inline visualization_msgs::msg::MarkerArray create_sidepath_visualization(
    const Container&   points,
    const std::string& frame,
    const std::string& ns,
    color              color )
{
        auto msg = create_marker( ns, 0, frame );

        msg.type = visualization_msgs::msg::Marker::LINE_STRIP;

        msg.color.r = color.r;
        msg.color.g = color.g;
        msg.color.b = color.b;

        msg.scale.x = 0.004;

        msg.points = em::map_f< std::vector< geometry_msgs::msg::Point > >(
            points, [&]( const auto& point ) {  //
                    return point_to_msg( point );
            } );

        visualization_msgs::msg::MarkerArray res;
        res.markers.push_back( msg );
        return res;
}
}  // namespace schpin
