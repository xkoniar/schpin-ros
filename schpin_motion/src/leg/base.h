// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/iterators/numeric.h"
#include "emlabcpp/zip.h"
#include "schpin_lib/algorithm.h"
#include "schpin_lib/identificator.h"
#include "schpin_lib_exp/sized_array.h"

#include <array>
#include <ostream>
#include <utility>

#pragma once

namespace schpin
{

using leg_id = uint8_t;

template < typename T >
using leg_container = sized_array< T, static_size< 4 > >;

using leg_size_token = leg_container< int >::token;

template < typename T >
auto get_leg_ids( const leg_container< T >& l )
{
        return em::range< leg_id >( static_cast< leg_id >( l.size() ) );
}

template <
    typename Container,
    typename Function = std::identity,
    typename T        = std::decay_t< em::mapped_t< Container, Function > > >
leg_container< T > map_f_to_l( Container&& cont, Function&& f = std::identity{} )
{
        return map_f_to_sz< static_size< 4 > >(
            std::forward< Container >( cont ), std::forward< Function >( f ) );
}

template < typename T >
constexpr bool operator<( const leg_container< T >& lh, const leg_container< T >& rh )
{
        return std::lexicographical_compare( lh.begin(), lh.end(), rh.begin(), rh.end() );
}

}  // namespace schpin

template < typename T >
struct std::hash< schpin::leg_container< T > >
{
        std::size_t operator()( const schpin::leg_container< T >& cont ) const
        {
                std::size_t res = 0;
                for ( const T& item : cont ) {
                        res ^= std::hash< T >()( item );
                }
                return res;
        }
};

namespace schpin
{

template < std::size_t N >
using discrete_angles = std::array< uint8_t, N >;

template < std::size_t N >
inline std::array< uint8_t, N >
operator-( const discrete_angles< N >& lh, const discrete_angles< N >& rh )
{

        std::array< uint8_t, N > res;
        for ( std::size_t i : em::range( N ) ) {
                res[i] = uint8_t( lh[i] > rh[i] ? lh[i] - rh[i] : rh[i] - lh[i] );
        }
        return res;
}
}  // namespace schpin

template < std::size_t N >
struct std::hash< schpin::discrete_angles< N > >
{
        std::size_t operator()( const schpin::discrete_angles< N >& angles )
        {
                std::size_t res = 0;
                emlabcpp::for_each( em::range( N ), [&]( std::size_t i ) {
                        std::bitset< 32 > val = angles[i];
                        res ^= schpin::rotated( val, i * 8 ).to_ulong();
                } );
                return res;
        }
};
