// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg/model.h"

#include "schpin_lib/model/leg/storage.h"

namespace schpin
{

em::either< leg_model< 3 >, urdf_parse_error > parse_leg_model(
    const urdf::Model&               model,
    const urdf::LinkConstSharedPtr&  model_root,
    const urdf::JointConstSharedPtr& root_joint )
{
        return parse_joint_rec( model, root_joint )
            .bind_left( [&]( joint j ) -> em::either< leg_model< 3 >, urdf_parse_error > {
                    if ( count_revolute_joints( j ) != 3 ) {
                            return {
                                E( urdf_parse_error ) << "Leg " << root_joint->name << " has  "
                                                      << count_revolute_joints( j )
                                                      << " revolute joints, but only exactly 3 "
                                                         "are supported. " };
                    }
                    return urdf_tree_offset( model, model_root, root_joint )
                        .convert_left( [&]( const pose< robot_frame >& offset ) {
                                return leg_model< 3 >{ offset, std::move( j ) };
                        } );
            } );
}

}  // namespace schpin
