// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/interface.h"

#include <utility>

#pragma once

namespace schpin
{

using sidepath_layer_vertex_id   = identificator< struct SidepathLayerVertexIDTag, 32 >;
using sidepath_layer_edge_offset = identificator< struct SidepathLayerEdgeOffsetTag, 32 >;
using sidepath_layer_edge_id =
    generic_edge_id< sidepath_layer_vertex_id, sidepath_layer_edge_offset >;

struct sidepath_layer_edge_content
{
        em::distance price;

public:
        em::distance get_price() const
        {
                return price;
        }
};

using sidepath_layer_edge = generic_edge<
    sidepath_layer_edge_id,
    sidepath_layer_edge_content,
    em::distance,
    sidepath_layer_vertex_id,
    sidepath_layer_edge_offset >;

struct sidepath_layer_vertex_content
{
        pose< world_frame >                 body_pose{};
        std::shared_ptr< env_oc_node_view > env_node{};
        point< 3, world_frame >             position{};
        em::distance                        price{};
        bool                                collide_check = false;

        sidepath_layer_vertex_content(
            std::shared_ptr< env_oc_node_view > env_node,
            point< 3, world_frame >             position,
            em::distance                        price,
            pose< world_frame >                 pose )
          : body_pose( pose )
          , env_node( std::move( env_node ) )
          , position( position )
          , price( price )
        {
        }

        explicit sidepath_layer_vertex_content(
            point< 3, world_frame > position = point< 3, world_frame >{} )
          : position( position )
          , collide_check( true )
        {
        }
};

using sidepath_layer_vertex =
    generic_vertex< sidepath_layer_vertex_id, sidepath_layer_vertex_content, sidepath_layer_edge >;
using sidepath_layer_graph = generic_graph< sidepath_layer_vertex, sidepath_layer_edge >;
}  // namespace schpin
