// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/pose.h"

#include <interactive_markers/interactive_marker_server.hpp>
#include <tf2_ros/transform_broadcaster.h>
#include <utility>

#pragma once

namespace schpin
{

visualization_msgs::msg::InteractiveMarker
make_pose_marker( const std::string& name, std::string frame )
{
        visualization_msgs::msg::InteractiveMarker int_marker;
        int_marker.header.frame_id = std::move( frame );
        int_marker.name            = name + "_marker";
        int_marker.description     = name + " marker";
        int_marker.scale           = 0.1f;

        visualization_msgs::msg::Marker box_marker;
        box_marker.frame_locked = true;
        box_marker.type         = visualization_msgs::msg::Marker::SPHERE;
        box_marker.scale.x      = 0.05;
        box_marker.scale.y      = 0.05;
        box_marker.scale.z      = 0.05;
        box_marker.color.r      = 0.5;
        box_marker.color.g      = 0.5;
        box_marker.color.b      = 0.5;
        box_marker.color.a      = 0.5;

        visualization_msgs::msg::InteractiveMarkerControl box_control;
        box_control.always_visible = true;
        box_control.markers.push_back( box_marker );

        int_marker.controls.push_back( box_control );

        visualization_msgs::msg::InteractiveMarkerControl control;
        control.always_visible = true;

        control.orientation.w    = 1;
        control.orientation.x    = 1;
        control.orientation.y    = 0;
        control.orientation.z    = 0;
        control.name             = name + "_move_x";
        control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::MOVE_AXIS;
        int_marker.controls.push_back( control );
        control.name             = name + "_rotate_x";
        control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::ROTATE_AXIS;
        int_marker.controls.push_back( control );

        control.orientation.w    = 1;
        control.orientation.x    = 0;
        control.orientation.y    = 1;
        control.orientation.z    = 0;
        control.name             = name + "_move_y";
        control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::MOVE_AXIS;
        int_marker.controls.push_back( control );
        control.name             = name + "_rotate_y";
        control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::ROTATE_AXIS;
        int_marker.controls.push_back( control );

        control.orientation.w    = 1;
        control.orientation.x    = 0;
        control.orientation.y    = 0;
        control.orientation.z    = 1;
        control.name             = name + "_move_z";
        control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::MOVE_AXIS;
        int_marker.controls.push_back( control );
        control.name             = name + "_rotate_z";
        control.interaction_mode = visualization_msgs::msg::InteractiveMarkerControl::ROTATE_AXIS;
        int_marker.controls.push_back( control );

        return int_marker;
}

class interactive_pose
{
        std::unique_ptr< interactive_markers::InteractiveMarkerServer > server_ptr_;
        std::string                                                     name_;
        pose< world_frame >                                             pose_;
        std::string                                                     frame_;
        tf2_ros::TransformBroadcaster                                   br_;

public:
        interactive_pose( interactive_pose&& other ) noexcept
          : server_ptr_( std::move( other.server_ptr_ ) )
          , name_( std::move( other.name_ ) )
          , pose_( other.pose_ )
          , frame_( std::move( other.frame_ ) )
          , br_( std::move( other.br_ ) )
        {
        }

        interactive_pose( rclcpp::Node& rclnode, const std::string& name, std::string frame )
          : server_ptr_(
                std::make_unique< interactive_markers::InteractiveMarkerServer >( name, &rclnode ) )
          , name_( name )
          , frame_( std::move( frame ) )
          , br_( rclnode )
        {
                show();
                set_tf();
        }

        const pose< world_frame >& get_pose() const
        {
                return pose_;
        }

        void set_pose( const pose< world_frame >& p )
        {
                pose_ = p;
                set_tf();
        }

private:
        void show()
        {
                auto marker = make_pose_marker( name_, "world" );
                server_ptr_->insert(
                    marker,
                    [this](
                        std::shared_ptr< const visualization_msgs::msg::InteractiveMarkerFeedback >
                            feedback ) {
                            this->on_feedback( feedback );
                    } );
                server_ptr_->applyChanges();
        }

        void on_feedback(
            std::shared_ptr< const visualization_msgs::msg::InteractiveMarkerFeedback >& feedback )
        {

                pose_ = msg_to_pose< world_frame >( feedback->pose );
                set_tf();
        }
        void set_tf()
        {
                geometry_msgs::msg::TransformStamped transform_stamped;

                // TODO: is this needed?
                //                transform_stamped.header.stamp          = ros::Time::now();
                transform_stamped.header.frame_id       = "world";
                transform_stamped.child_frame_id        = frame_;
                transform_stamped.transform.translation = vector_to_msg( pose_.position );
                transform_stamped.transform.rotation    = quaternion_to_msg( pose_.orientation );
                br_.sendTransform( transform_stamped );
        }
};

}  // namespace schpin
