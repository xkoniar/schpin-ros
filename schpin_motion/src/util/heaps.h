// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <boost/heap/binomial_heap.hpp>
#include <boost/heap/d_ary_heap.hpp>
#include <boost/heap/fibonacci_heap.hpp>
#include <boost/heap/pairing_heap.hpp>
#include <boost/heap/skew_heap.hpp>

#pragma once

namespace schpin
{
template < typename QueueItem >
using binary_heap = boost::heap::d_ary_heap<
    QueueItem,
    boost::heap::mutable_< true >,
    boost::heap::arity< 2 >,
    boost::heap::compare< std::greater< QueueItem > > >;

template < typename QueueItem >
using d_ary_heap_4 = boost::heap::d_ary_heap<
    QueueItem,
    boost::heap::mutable_< true >,
    boost::heap::arity< 4 >,
    boost::heap::compare< std::greater< QueueItem > > >;

template < typename QueueItem >
using d_ary_heap_8 = boost::heap::d_ary_heap<
    QueueItem,
    boost::heap::mutable_< true >,
    boost::heap::arity< 8 >,
    boost::heap::compare< std::greater< QueueItem > > >;

template < typename QueueItem >
using binom_heap =
    boost::heap::binomial_heap< QueueItem, boost::heap::compare< std::greater< QueueItem > > >;

template < typename QueueItem >
using fibonacci_heap =
    boost::heap::fibonacci_heap< QueueItem, boost::heap::compare< std::greater< QueueItem > > >;

template < typename QueueItem >
using pairing_heap =
    boost::heap::pairing_heap< QueueItem, boost::heap::compare< std::greater< QueueItem > > >;

template < typename QueueItem >
using skew_heap =
    boost::heap::skew_heap< QueueItem, boost::heap::compare< std::greater< QueueItem > > >;

}  // namespace schpin
