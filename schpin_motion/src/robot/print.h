// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "robot.h"

#pragma once

namespace schpin
{

void print_robot_model( const robot& rob )
{
        for ( const leg< 3 >& pleg : rob.get_legs() ) {
                SCHPIN_INFO_LOG( "robot.print", "Printing model for leg " << pleg.get_id() );

                auto& root = pleg.get_model().get_root_joint();

                std::function< void( const joint& j, std::string ) > f =
                    [&]( const joint& j, const std::string& prefix ) {
                            SCHPIN_INFO_LOG(
                                "robot.print",
                                prefix << "| " << j.name << " - " << j.offset << " - "
                                       << j.sub_link.name );
                            if ( std::holds_alternative< revolute_joint >( j.jclass ) ) {
                                    const auto& rj = std::get< revolute_joint >( j.jclass );
                                    SCHPIN_INFO_LOG(
                                        "robot.print",
                                        prefix << "+ " << rj.axis << " - (" << rj.min_angle << "-"
                                               << rj.max_angle << ") - " << rj.state_space_steps
                                               << " - " << rj.max_velocity );
                            } else {
                                    SCHPIN_INFO_LOG( "robot.print", prefix << "+ fixed" );
                            }
                            if ( j.sub_link.child_ptr ) {
                                    f( *j.sub_link.child_ptr, prefix + "  " );
                            }
                    };
                f( root, "" );
        }
}

}  // namespace schpin
