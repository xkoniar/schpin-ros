// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "robot/storage.h"

#include "leg/storage.h"

namespace schpin
{
em::either< robot, robot_error > load_robot(
    const std::filesystem::path& cache_prefix,
    const urdf::Model&           model,
    const srdf_information&      srdf_info )
{
        return assemble_left_collect_right(  //
                   load_legs( cache_prefix, model, srdf_info )
                       .convert_right( [&]( leg_boot_error&& e ) -> robot_error {
                               return E( robot_error ).attach( e ) << "Errors occured during the "
                                                                      "load of leg";
                       } ),
                   load_body( model, srdf_info.body_link )
                       .convert_right( [&]( body_boot_error&& e ) -> robot_error {
                               return E( robot_error ).attach( e ) << "Errors occured during the "
                                                                      "load of body";
                       } ) )
            .convert_left( [&]( std::tuple< leg_container< leg< 3 > >, body > pack ) {
                    auto [legs, b] = std::move( pack );
                    return robot( std::move( b ), std::move( legs ) );
            } )
            .convert_right( [&]( auto&& errors ) -> robot_error {
                    return E( robot_error ).group_attach( errors ) << "Failed to load the robot";
            } );
}
}  // namespace schpin
