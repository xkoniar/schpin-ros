// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "robot.h"
#include "schpin_lib/srdf.h"

#pragma once

namespace schpin
{

em::either< robot, robot_error > load_robot(
    const std::filesystem::path& cache_prefix,
    const urdf::Model&           model,
    const srdf_information&      srdf_info );

inline em::either< robot, robot_error > load_robot_ros( const reactor_config& cfg )
{
        urdf::Model model;
        model.initString( cfg.robot_description );

        return load_srdf_info( cfg.robot_semantics )
            .convert_right( [&]( const xml_storage_error& e ) {
                    return E( robot_error ).attach( e ) << "Failed to load robot model, when "
                                                           "loading the srdf "
                                                           "definition";
            } )
            .bind_left( [&]( const srdf_information& srdf_info ) {
                    return load_robot( cfg.cache_prefix, model, srdf_info );
            } );
}
}  // namespace schpin
