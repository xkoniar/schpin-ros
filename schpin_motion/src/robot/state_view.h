// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "body/state_view.h"
#include "leg/state_view.h"

#pragma once

namespace schpin
{

struct robot_state
{
        body_state                      body;
        leg_container< leg_state< 3 > > legs;

        robot_state( leg_size_token tok )
          : legs( tok )
        {
        }

        robot_state( body_state b, leg_container< leg_state< 3 > > l )
          : body( b )
          , legs( l )
        {
        }
};

inline std::ostream& operator<<( std::ostream& os, const robot_state& rs )
{
        return os << "body: " << rs.body << " legs: \n" << em::view( rs.legs );
}

class robot_state_view
{
        const robot&                         robot_;
        body_state_view                      body_;
        leg_container< leg_state_view< 3 > > legs_;

        static leg_container< leg_state_view< 3 > >
        map_legs( const robot& robot, const robot_state& state )
        {
                return map_f_to_l( get_leg_ids( state.legs ), [&]( leg_id id ) {
                        return leg_state_view< 3 >{ robot.get_legs()[id], state.legs[id] };
                } );
        }

public:
        robot_state_view( const robot& robot, const robot_state& state )
          : robot_( robot )
          , body_( robot.get_body(), state.body )
          , legs_( map_legs( robot, state ) )
        {
        }

        const robot& operator*() const
        {
                return robot_;
        }
        const robot* operator->() const
        {
                return &robot_;
        }

        const leg_container< leg_state_view< 3 > >& get_legs() const
        {
                return legs_;
        }

        const body_state_view& get_body() const
        {
                return body_;
        }

        const pose< world_frame >& get_pose() const
        {
                return body_.get_pose();
        }

        point< 3, world_frame > get_tip_position( leg_id lid ) const
        {
                return transform( legs_[lid].get_tip_position(), body_.get_pose() );
        }
};

}  // namespace schpin
