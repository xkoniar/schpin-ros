// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <filesystem>
#include <schpin_lib_exp/config.h>
#include <string>

#pragma once

namespace schpin
{
struct reactor_config
{
        std::string                            robot_description{};
        std::string                            robot_semantics{};
        std::filesystem::path                  cache_prefix = "~/.schpin/cache";
        std::optional< std::filesystem::path > lua_boot_script{};

        static auto ros_param_get()
        {
                return std::make_tuple(
                    ros_param< std::string >(
                        "robot_description", "URDF description of the robot" ),
                    ros_param< std::string >(
                        "robot_semantics", "Semantic description of the robot" ),
                    ros_opt_param< std::filesystem::path >(
                        "cache_path",
                        "path to cache for robot termporary data",
                        "~/.schpin/cache" ),
                    ros_param< std::optional< std::filesystem::path > >(
                        "lua_boot_script",
                        "lua script to be executed at start of the motion planner" ) );
        }
};
}  // namespace schpin
