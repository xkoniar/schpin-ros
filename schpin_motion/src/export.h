// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "plan.h"
#include "robot.h"

#pragma once

namespace schpin
{

class lua_code_gen
{
        std::vector< std::string > lines_;

public:
        std::vector< std::string > generate_lines( const movement_plan& p, const robot& rob )
        {
                SCHPIN_INFO_LOG( "export", "Exported plan: " );
                for ( const auto& step : p ) {
                        SCHPIN_INFO_LOG( "export", step );
                }

                lines_ = std::vector< std::string >{};

                auto all_joints_names = map_f_to_v( rob.get_legs(), [&]( const auto& leg ) {
                        auto leg_names = leg.get_joint_names();
                        return to_string( em::view{ leg_names } );
                } );

                if ( !p.empty() ) {
                        set_body_pose( p.front().from_pose );
                        generate_start_state( p.front(), rob );
                        update_pose_on_block( all_joints_names, p.front().from_pose );
                }
                for ( const auto& step : p ) {
                        generate_plan_step( step, rob );
                        sync( all_joints_names );
                }

                return lines_;
        }

private:
        template < unsigned N >
        void generate_start_state( const plan_step< N >& step, const robot& robot )
        {
                for ( const auto& [var, leg] : em::zip( step.legs, robot.get_legs() ) ) {
                        std::vector< std::string > joint_names = leg.get_joint_names();
                        em::match(
                            var,
                            [&]( leg_plan_move< N > ) {},
                            [&]( const leg_plan_contact< N >& contact ) {  //
                                    for ( auto [i, angle] :
                                          em::enumerate( contact.approx_angles ) ) {
                                            joint_goto( joint_names[i], 0.25f, *angle );
                                    }
                            } );
                }
        }

        template < unsigned N >
        void generate_leg_move( const leg_plan_move< N >& move, const leg< N >& leg )
        {
                std::vector< std::string > joint_names = leg.get_joint_names();

                for ( auto ch_view : move.path ) {
                        for ( auto [i, angle] : em::enumerate( ch_view.target_pin ) ) {
                                joint_goto( joint_names[i], *ch_view->link, *angle );
                        }
                        sync( joint_names );
                }
        }

        template < unsigned N >
        void generate_plan_step( const plan_step< N >& step, const robot& robot )
        {
                print( "pose from: ", step.from_pose );
                print( "pose to: ", step.to_pose );

                std::vector< std::pair< leg_id, point< 3, world_frame > > > contact_points;
                for ( std::size_t i : em::range( step.legs.size() ) ) {
                        const auto& leg = robot.get_legs()[i];
                        em::match(
                            step.legs[i],
                            [&]( const leg_plan_move< N >& move ) {
                                    generate_leg_move( move, leg );
                            },
                            [&]( const leg_plan_contact< N >& contact ) {
                                    contact_points.emplace_back(
                                        leg.get_id(), contact.contact_point );
                            } );
                }

                auto contact_joints = map_f_to_v(
                    contact_points, [&]( std::tuple< leg_id, point< 3, world_frame > > pack ) {
                            leg_id id    = std::get< leg_id >( pack );
                            auto   names = robot.get_legs()[id].get_joint_names();
                            return to_string( em::view{ names } );
                    } );

                auto body_path = lineary_interpolate_path< world_frame >(
                    { step.from_pose, step.to_pose }, em::distance{ 0.002f }, em::angle{ 0.1f } );

                SCHPIN_INFO_LOG( "export", "Body path interpolated: " << em::view{ body_path } );

                for ( pose< world_frame > b_pose : body_path ) {
                        for ( auto [lid, contact_point] : contact_points ) {
                                const auto& leg = robot.get_legs()[lid];
                                auto        target_point =
                                    inverse_transform< robot_frame >( contact_point, b_pose );
                                auto t = *step.time / static_cast< float >( body_path.size() - 1 );
                                relative_tip_goto( leg.get_name(), t, target_point );
                        }
                        update_pose_on_block( contact_joints, b_pose );
                        yield();
                }
        }

        template < typename... Args >
        void print( Args... args )
        {
                lines_.push_back( "print(\"" + ( to_string( args ) + ... ) + "\")" );
        }

        void yield()
        {
                lines_.emplace_back( "coroutine.yield()" );
        }

        void set_body_pose( const pose< world_frame >& p )
        {
                std::string line = "set_body_pose{point=" + point_to_string( p.position ) +
                                   ",orientation=" + quat_to_string( p.orientation ) + "}";
                lines_.push_back( line );
        }

        void update_pose_on_block(
            const std::vector< std::string >& joints,
            const pose< world_frame >&        pos )
        {
                std::string line = "update_pose_on_block({" + to_string( em::view{ joints } ) +
                                   "}," + point_to_string( pos.position ) + "," +
                                   quat_to_string( pos.orientation ) + ")";
                lines_.push_back( line );
        }

        void sync( const std::vector< std::string >& joints )
        {
                std::string line = "sync{joints={" + to_string( em::view{ joints } ) + "}}";
                lines_.push_back( line );
        }

        void joint_goto( const std::string& name, float move_time, float pos )
        {
                std::string line = "joint_goto{joint=" + name + ",time=" + to_string( move_time ) +
                                   ",pos=" + to_string( pos ) + "}";
                lines_.push_back( line );
        }

        void tip_goto(
            const std::string&             leg_name,
            float                          move_time,
            const point< 3, world_frame >& target )
        {

                std::string line = "tip_goto{leg=\"" + leg_name +
                                   "\",time=" + std::to_string( move_time ) +
                                   ",target=" + point_to_string( target ) + ",steps=32}";
                lines_.push_back( line );
        }
        void relative_tip_goto(
            const std::string&             leg_name,
            float                          move_time,
            const point< 3, robot_frame >& target )
        {

                std::string line = "relative_tip_goto{leg=\"" + leg_name +
                                   "\",time=" + std::to_string( move_time ) +
                                   ",target=" + point_to_string( target ) + ",steps=32}";
                lines_.push_back( line );
        }

        template < typename Tag >
        std::string point_to_string( const point< 3, Tag >& p )
        {
                return "{" + to_string( p[0] ) + "," + to_string( p[1] ) + "," + to_string( p[2] ) +
                       "}";
        }

        std::string quat_to_string( const quaternion< world_frame >& q )
        {
                return "{" + to_string( q[0] ) + "," + to_string( q[1] ) + "," + to_string( q[2] ) +
                       "," + to_string( q[3] ) + "}";
        }
};

inline void export_plan( const movement_plan& p, const robot& robot, const std::string& filename )
{
        std::ofstream of( filename );
        of << R"EOF(

function update_pose_on_block(jnt, point, quat)
    block_yield {joints = jnt}
    set_body_pose{point=point, orientation=quat}
end

)EOF";

        lua_code_gen code_gen{};

        for ( const std::string& line : code_gen.generate_lines( p, robot ) ) {
                of << line << "\n";
        }
}

}  // namespace schpin
