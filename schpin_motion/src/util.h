// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/either.h"
#include "schpin_lib/util.h"
#include "schpin_lib/view.h"

#include <algorithm>
#include <chrono>
#include <emlabcpp/algorithm.h>
#include <emlabcpp/iterators/numeric.h>
#include <iterator>
#include <utility>

#pragma once

namespace schpin
{

template < typename Container, typename UnaryFunction >
constexpr void for_nth_iterator( unsigned n, const Container& cont, UnaryFunction&& f )
{
        auto begin = cont.begin();
        for ( ; begin != cont.end();
              begin += std::min< int64_t >( n, std::distance( begin, cont.end() ) ) ) {
                f( begin );
        }
}

template < typename Iterator >
using batch = em::view< Iterator >;

template < std::size_t N, typename Container, typename Iterator = em::iterator_of_t< Container > >
inline std::vector< batch< Iterator > > make_batches( Container& cont )
{
        std::vector< batch< Iterator > > res;
        auto                             work_view = em::view{ cont };
        while ( work_view.size() > N ) {
                res.push_back( batch< Iterator >{ work_view.begin(), work_view.begin() + N } );
                work_view = em::view( work_view.begin() + N, work_view.end() );
        }
        if ( !work_view.empty() ) {
                res.push_back( batch< Iterator >{ work_view.begin(), work_view.end() } );
        }
        return res;
}

template < typename Function >
int64_t measure_time( Function f )
{
        std::chrono::high_resolution_clock::time_point t_point =
            std::chrono::high_resolution_clock::now();
        f();
        return std::chrono::duration_cast< std::chrono::nanoseconds >(
                   std::chrono::high_resolution_clock::now() - t_point )
            .count();
}
}  // namespace schpin
