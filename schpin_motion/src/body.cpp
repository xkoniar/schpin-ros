// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "body.h"

#include "util.h"

#include <schpin_lib/octree/visitors.h>

namespace schpin
{

oc_tree< occupancy_content > create_body_occupancy_tree(
    const std::vector< triangle< 3, robot_frame > >& surface,
    em::length                                       edge_length )
{
        oc_tree< occupancy_content > res( edge_length, occupancy_content{ OC_MIN_OCCUPANCY } );

        for ( const triangle< 3, robot_frame >& t : surface ) {
                res.expand( tag_cast< oc_frame >( t ) );
        }

        using visitor = oc_collision_visitor<
            oc_collision_checker<
                sat_triangle_collision_query< 3, oc_frame >,
                bs_triangle_collision_query< oc_frame >,
                oc_node< occupancy_content > >,
            32 >;

        for ( const auto& batch : make_batches< 32 >( surface ) ) {
                std::vector< triangle< 3, oc_frame > > transformed_batch =
                    map_f_to_v( batch, [&]( const triangle< 3, robot_frame >& t ) {
                            return tag_cast< oc_frame >( t );
                    } );
                visitor vis{ transformed_batch, occupancy_token::FULL };
                res.modify( vis, vis.default_info() );
        }

        return res;
}
em::either< body, body_boot_error >
load_body( const urdf::Model& model, const std::string& link_name )
{
        urdf::LinkConstSharedPtr link = model.getLink( link_name );

        if ( !link ) {
                return { E( body_boot_error ) << "Failed to load the body link;" };
        }

        return extract_model< robot_frame >( *link )
            .convert_left( [&]( structure_model< robot_frame > smodel ) {
                    auto bound_cube = dimensional_min_max_elem(
                        em::range( smodel.get_surface().size() * 3 ), [&]( std::size_t i ) {
                                return smodel.get_surface()[i / 3][i % 3];
                        } );
                    SCHPIN_INFO_LOG( "body", "Body surface bounding cube: " << bound_cube );
                    return body( std::move( smodel ), em::length( 0.05f ) );
            } )
            .convert_right( [&]( const urdf_parse_error& error ) {
                    return E( body_boot_error ).attach( error ) << "Body not loaded due to error "
                                                                   "in urdf "
                                                                   "parsing";
            } );
}
}  // namespace schpin
