// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/generics/edge.h"
#include "schpin_lib/identificator.h"

#pragma once

namespace schpin
{
using body_vertex_id   = identificator< struct BodyVertexIDTag, 32 >;
using body_edge_offset = identificator< struct BodyEdgeOffsetTag, 32 >;
using body_price       = float;
using body_edge_id     = generic_edge_id< body_vertex_id, body_edge_offset >;

}  // namespace schpin
