// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"
#include "schpin_lib/geom/quaternion.h"

#include <random>

namespace schpin
{
class body_generator
{
        std::mt19937                            gen_{};
        std::normal_distribution< float >       normal_d_{ 0, 1 };
        std::uniform_real_distribution< float > uniform_d_{ *( -em::pi ), *em::pi };

public:
        pose< world_frame >
        get_random_body_coord( const pose< world_frame >& from, const pose< world_frame >& to )
        {

                // TODO: to debug the maze: gnerate just random points, without collision detection!
                pose< world_frame > center = lin_interp( from, to, 0.5f );

                em::length l = distance_of( center, to ).dist;

                vec< 3 > offset{
                    normal_d_( gen_ ) * l / 1.f, normal_d_( gen_ ) * l / 1.f, em::length{ 0.f } };

                point< 3, world_frame > new_coord = center.position + offset;

                em::distance a = distance_of( new_coord, to.position );
                em::distance b = distance_of( new_coord, from.position );

                float r = *b / ( *a + *b );

                return pose< world_frame >{
                    new_coord, slerp( from.orientation, to.orientation, r ) };
        }
};
}  // namespace schpin
