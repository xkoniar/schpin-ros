// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "body.h"

#pragma once

namespace schpin
{

struct body_state
{
        pose< world_frame > pos;
};

inline std::ostream& operator<<( std::ostream& os, const body_state& bs )
{
        return os << bs.pos;
}

class body_state_view
{

        const body_state state_;
        const body&      body_;

public:
        body_state_view( const body& body, const body_state& state )
          : state_( state )
          , body_( body )
        {
        }

        const body& get_body() const
        {
                return body_;
        }

        const pose< world_frame >& get_pose() const
        {
                return state_.pos;
        }

        bs_oc_node_collision_query< const oc_node< occupancy_content >, world_frame >
        get_query() const
        {
                return bs_oc_node_collision_query<
                    const oc_node< occupancy_content >,
                    world_frame >{ body_.get_collision_tree().root_view(), state_.pos };
        }
};

}  // namespace schpin
