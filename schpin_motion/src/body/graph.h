// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "body/base.h"
#include "graph/generics.h"
#include "graph/generics/edge.h"
#include "graph/generics/vertex.h"
#include "schpin_lib/geom/pose.h"
#include "schpin_lib/geom/pose/serialization.h"

#pragma once

namespace schpin
{

struct body_edge_content
{
        body_price price;
        body_price get_price() const
        {
                return price;
        }
};
using body_edge =
    generic_edge< body_edge_id, body_edge_content, body_price, body_vertex_id, body_edge_offset >;

inline void to_json( nlohmann::json& j, const body_edge_content& content )
{
        j = content.price;
}

inline void from_json( const nlohmann::json& j, body_edge_content& content )
{
        content.price = j.get< body_price >();
}

struct body_vertex_content
{
        pose< world_frame > pos;
};

inline void to_json( nlohmann::json& j, const body_vertex_content& content )
{
        j = content.pos;
}

inline void from_json( const nlohmann::json& j, body_vertex_content& content )
{
        content.pos = j.get< pose< world_frame > >();
}

using body_vertex = generic_vertex< body_vertex_id, body_vertex_content, body_edge >;
using body_graph  = generic_graph< body_vertex, body_edge >;
}  // namespace schpin
