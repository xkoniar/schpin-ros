// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/dstar.h"
#include "leg/view.h"
#include "planner/body.h"
#include "planner/gait.h"
#include "robot.h"

#pragma once

namespace schpin
{

// TODO: check all Error definitions!
using planner_error = error< struct planner_errorTag >;

class planner
{

        planner_context plan_context_;
        body_planner<>  body_planner_;
        gait_planner    gait_planner_;
        robot_state     robot_state_;

public:
        explicit planner( planner_context plan_context, leg_size_token tok )
          : plan_context_( std::move( plan_context ) )
          , body_planner_( plan_context_ )
          , gait_planner_( tok )
          , robot_state_( tok )
        {
                robot_state_.legs =
                    map_f_to_l( plan_context_.rob.get_legs(), [&]( const leg< 3 >& l ) {
                            return leg_state< 3 >{ l.get_model().neutral_configuration() };
                    } );
        }

        template < typename Event >
        opt_error< planner_error > body_event( Event&& e )
        {
                return body_planner_
                    .event(
                        std::forward< Event >( e ),
                        plan_context_,
                        body_planner_lambda_visitor{
                            [this]( const chain< body_edge_id, body_vertex_id >& path ) {
                                    return this->on_path( path );
                            } } )
                    .convert( [&]( const body_planner_error& e ) {
                            return E( planner_error ).attach( e ) << "Body planner failed to "
                                                                     "process an event";
                    } );
        }

        template < typename Event >
        auto gait_event( Event&& e )
        {
                return gait_planner_.event( std::forward< Event >( e ), plan_context_ );
        }

        opt_error< planner_error > set_environment( environment env )
        {
                gait_planner_.clear();
                plan_context_.env = std::move( env );
                return body_event( environment_changed_event{} );
        }

        const gait_planner& get_gait_planner() const
        {
                return gait_planner_;
        }

        opt_error< body_path_failure_event >
        on_path( const chain< body_edge_id, body_vertex_id >& path )
        {
                auto body_path = map_f_to_v( path.get_pins(), [&]( body_vertex_id bid ) {  //
                        return gait_body_pose{
                            body_planner_.get_body_pose( bid ),
                            bid,
                            map_f_to_l( get_robot().get_legs(), [&]( const leg< 3 >& leg ) {
                                    return leg_planner< 3 >{ leg.get_graph() };
                            } ) };
                } );

                return gait_planner_
                    .reset(
                        robot_state_view{ get_robot(), robot_state_ }, body_path, plan_context_ )
                    .convert( [&]( const gait_reset_failure_report& r ) {
                            std::set< body_vertex_id > res;
                            for ( const gait_body_pose& gpose : r.failed_poses ) {
                                    res.insert( gpose.bid );
                            }

                            body_path_failure_event e;
                            e.vertexes = std::vector< body_vertex_id >{ res.begin(), res.end() };
                            return e;
                    } );
        }

        opt_error< planner_error > set_goal( const pose< world_frame >& p )
        {
                return body_planner_.set_goal( p, plan_context_ )
                    .convert_left( [&]( body_vertex_id bid ) {
                            plan_context_.goal = std::make_optional< pose< world_frame > >(
                                body_planner_.get_body_pose( bid ) );
                            return opt_error< planner_error >{};
                    } )
                    .convert_right(
                        []( const body_planner_error& e ) -> opt_error< planner_error > {
                                return {
                                    E( planner_error ).attach( e ) << "Failed to set goal for "
                                                                      "planner, because of "
                                                                      "body "
                                                                      "planner error" };
                        } )
                    .join();
        }

        robot_state get_robot_state() const
        {
                return robot_state_;
        }

        opt_error< planner_error > set_robot_state( robot_state rstate )
        {
                return body_planner_.set_current( rstate.body.pos, plan_context_ )
                    .convert_left( [&]( body_vertex_id ) {
                            robot_state_          = rstate;
                            plan_context_.current = rstate.body.pos;
                            return opt_error< planner_error >{};
                    } )
                    .convert_right(
                        [&]( const body_planner_error& e ) -> opt_error< planner_error > {
                                return {
                                    E( planner_error ).attach( e ) << "Failed to change "
                                                                      "current robot state "
                                                                      "due "
                                                                      "to "
                                                                      "body "
                                                                      "planner error" };
                        } )
                    .join();
        }

        const environment& get_environment() const
        {
                return plan_context_.env;
        }

        const robot& get_robot() const
        {
                return plan_context_.rob;
        }

        movement_plan get_plan()
        {
                return gait_planner_.get_plan( plan_context_ );
        }

        chain< gait_price, gait_state_view > get_path_view()
        {
                return gait_planner_.get_path_view( plan_context_ );
        }

        const auto& get_body_planner() const
        {
                return body_planner_;
        }
};
}  // namespace schpin
