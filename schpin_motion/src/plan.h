// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg.h"
#include "schpin_lib/chain.h"
#include "schpin_lib/geom/point.h"

#pragma once

namespace schpin
{
// TODO: dof_array should be usedh ere instead of std::array
template < unsigned N >
struct leg_plan_move
{
        chain< em::timeq, std::array< em::angle, N > > path;
};

template < unsigned N >
struct leg_plan_contact
{
        point< 3, world_frame >    contact_point;
        std::array< em::angle, N > approx_angles;
};

template < unsigned N >
using leg_plan_var = std::variant< leg_plan_move< N >, leg_plan_contact< N > >;

template < unsigned N >
struct plan_step
{
        pose< world_frame >                from_pose;
        pose< world_frame >                to_pose;
        leg_container< leg_plan_var< N > > legs;
        em::timeq                          time;
};

using movement_plan = std::vector< plan_step< 3 > >;

inline bool is_plan_valid( const movement_plan& p )
{
        return em::all_of( p, [&]( const plan_step< 3 >& pstep ) {
                return em::all_of( pstep.legs, [&]( const leg_plan_var< 3 >& lvar ) {
                        return em::match(
                            lvar,
                            [&]( const leg_plan_move< 3 >& leg_step ) {
                                    bool non_empty_path = leg_step.path.empty();
                                    bool time_is_below_step =
                                        em::sum( leg_step.path.get_links() ) < pstep.time;

                                    return non_empty_path && time_is_below_step;
                            },
                            [&]( const leg_plan_contact< 3 >& ) {
                                    return true;
                            } );
                } );
        } );
}

template < unsigned N >
inline std::ostream& operator<<( std::ostream& os, const leg_plan_contact< N >& lpc )
{
        return os << "contact at: " << lpc.contact_point << " (" << em::view{ lpc.approx_angles }
                  << ")";
}

template < unsigned N >
inline std::ostream& operator<<( std::ostream& os, const leg_plan_move< N >& move )
{
        if ( move.path.empty() ) {
                return os << "path empty";
        }
        os << "path: " << em::view{ move.path.get_pins().front() };
        for ( auto chview : move.path ) {
                os << " -(" << chview.link << ")-> " << em::view{ chview.target_pin };
        }

        return os;
}

template < unsigned N >
inline std::ostream& operator<<( std::ostream& os, const plan_step< N >& step )
{
        os << "Plan step: " << step.from_pose << " - " << step.to_pose << "\n";
        for ( const auto& [id, leg_var] : em::enumerate( step.legs ) ) {
                os << id << ":";
                std::visit(
                    [&]( const auto& item ) {
                            os << item;
                    },
                    leg_var );
                os << "\n";
        }
        return os;
}

}  // namespace schpin
