// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "body.h"
#include "config.h"
#include "leg.h"

#include <utility>

#pragma once

namespace schpin
{

struct robot_error_tag
{
};

using robot_error = error< robot_error_tag >;

class robot
{

        body                      body_;
        leg_container< leg< 3 > > legs_;

public:
        robot( body body, leg_container< leg< 3 > > legs )
          : body_( std::move( body ) )
          , legs_( std::move( legs ) )
        {
        }

        robot copy() const
        {
                return robot( body_.copy(), legs_.copy() );
        }

        const body& get_body() const
        {
                return body_;
        }

        const leg_container< leg< 3 > >& get_legs() const
        {
                return legs_;
        }

        // radius used when calculationg price of rotation in pose distance
        em::radius get_price_radius() const
        {
                // TODO: THIS SHALL NOT BE HARDCODED
                return em::radius{ 0.1f };
        }
};
template < typename Tag >
em::distance pose_change_price( const robot& rob, const pose< Tag >& from, const pose< Tag >& to )
{
        pose_distance dist = distance_of( from, to );
        return dist.dist + em::distance{ *dist.angle_dist * rob.get_price_radius() };
}

}  // namespace schpin

template <>
struct nlohmann::adl_serializer< schpin::robot >
{
        static void to_json( nlohmann::json& j, const schpin::robot& robot )
        {
                j["legs"] = robot.get_legs();
                j["body"] = robot.get_body();
        }

        static schpin::robot from_json( const nlohmann::json& j )
        {
                return schpin::robot(
                    j.at( "body" ).get< schpin::body >(),
                    j.at( "legs" ).get< schpin::leg_container< schpin::leg< 3 > > >() );
        }
};

template <>
struct std::hash< schpin::robot >
{
        std::size_t operator()( const schpin::robot& robot )
        {
                return std::hash< schpin::body >()( robot.get_body() ) ^
                       std::hash< schpin::leg_container< schpin::leg< 3 > > >()( robot.get_legs() );
        }
};
