// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "events.h"
#include "planner/base.h"

#include <experimental/propagate_const>
#include <schpin_lib/lua/base.h>
#include <variant>

#pragma once

namespace schpin
{

using lua_event_var = std::variant<
    load_environment_event,
    set_goal_pose_event,
    set_current_pose_event,
    plan_event,
    clear_event,
    export_event,
    show_body_collision_event,
    show_leg_graph_event,
    show_workarea_event,
    print_model_event,
    print_pose_cache_event,
    make_vertexes_event,
    find_body_path_event,
    make_until_path_event,
    export_path_event,
    quit_event >;

class lua_vm_eventmaker
{
        struct impl;
        std::experimental::propagate_const< std::unique_ptr< impl > > impl_ptr_;

public:
        lua_vm_eventmaker();

        bool                              is_finished();
        opt_error< slua_error >           load_file( std::filesystem::path );
        em::either< lua_res, slua_error > tick( std::function< void( lua_event_var ) > );

        ~lua_vm_eventmaker();
};

}  // namespace schpin
