// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "planner/gait.h"

namespace schpin
{
partitioned_eithers< leg_sidepath_generator, sidepath_failure_report > make_sidepath_generators(
    const robot_state_view& robot,
    const gait_body_path&   body_path,
    const planner_context&  context )
{
        return partition_eithers(
            map_f_to_v( robot.get_legs(), [&]( const leg_state_view< 3 >& leg_view ) {
                    SCHPIN_INFO_LOG(
                        "planner.gait",
                        "Generating sidepath for leg, body_path has " << body_path.size()
                                                                      << " steps " );
                    return create_sidepath_generator(
                        em::access_view(
                            body_path,
                            [&]( auto& gpose ) -> const pose< world_frame >& {  //
                                    return gpose.pos;
                            } ),
                        *leg_view,
                        robot.get_tip_position( leg_view->get_id() ),
                        context.env );
            } ) );
}
}  // namespace schpin
