// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "base/line/collission.h"
#include "emlabcpp/iterators/access.h"
#include "events.h"
#include "gait/graph.h"
#include "gait/sql.h"
#include "gait/view.h"
#include "graph/util.h"
#include "leg/sidepath.h"
#include "plan.h"
#include "planner/base.h"
#include "robot/state_view.h"

#include <execution>
#include <schpin_lib/geom/line.h>

#pragma once

namespace schpin
{

struct gait_planner_config
{
        em::distance       dist_step;
        em::angle          ang_step;
        float              max_variance;
        std::size_t        max_moving;
        std::vector< int > sidepath_offsets;
        em::timeq          bodyless_timestep;
};

using gait_planner_error = error< struct gait_planner_error_tag >;

struct reset_chain_event
{
        robot_state_view robot;
        gait_body_path   body_path;
};

inline std::size_t
closest_point_on_sidepath( const sidepath& sidepath, const point< 3, world_frame >& tip_position )
{
        return *min_iterator(
            em::range( sidepath.size() ),
            [&]( std::size_t i ) {  //
                    return distance_of( sidepath[i], tip_position );
            } );
}

struct gait_predecessor
{
        gait_state              state;
        em::timeq               t;
        gait_vertex_status_enum status;
};

template < typename PlanFunction >
inline em::either< gait_leg_state, leg_planner_error > retract_leg_gait_state(
    const gait_leg_view& gview,
    em::timeq            movement_time,
    PlanFunction         plan_path_f,
    leg_vertex_id        source_vertex )
{
        if ( !gview.is_leg_moving() || source_vertex == gview.get_vertex_id() ) {
                // assert movement_time is enough for standing leg to adjust itself
                return gait_leg_state{
                    .leg_config = source_vertex,
                    .sidepath_i = gview.get_sidepath_index(),
                    .is_moving  = false };
        }

        // shortest path from start of gait movement to actual state
        return plan_path_f( source_vertex, gview.get_vertex_id() )
            .convert_left( [&]( const chain< leg_edge_id, leg_vertex_id >& leg_path ) {
                    em::timeq move_sum{ 0.f };
                    // rev is path from actual to 'start' leg config
                    auto rev = em::reversed( leg_path.get_links() );

                    // find first edge on the path, that depletes time limit (movement_time)
                    auto iter = em::find_if( rev, [&]( const leg_edge_id& eid ) {
                            auto p = gview->get_graph().get_edge( eid ).get_price();
                            if ( move_sum + p > movement_time ) {
                                    return true;
                            }
                            move_sum += p;
                            return false;
                    } );

                    if ( iter == rev.end() ) {  // not depleted => conversion
                            return gait_leg_state{
                                .leg_config = source_vertex,
                                .sidepath_i = gview.get_sidepath_index(),
                                .is_moving  = false };
                    }
                    // depleted => move to depleting vertex
                    return gait_leg_state{
                        .leg_config = gview->get_graph().get_edge( *iter ).get_target_id(),
                        .sidepath_i = gview.get_sidepath_index(),
                        .is_moving  = true };
            } );
}

inline em::timeq approximate_gait_movement_time(
    const gait_state_view&     previous_state,
    const gait_state_view&     actual_state,
    const robot&               robot,
    const gait_planner_config& conf )
{

        SCHPIN_ASSERT( em::equal(
            get_leg_ids( previous_state.get_legs() ), get_leg_ids( actual_state.get_legs() ) ) );

        return em::max_elem( get_leg_ids( previous_state.get_legs() ), [&]( leg_id id ) {
                gait_leg_view prev_lview   = previous_state.get_legs()[id];
                gait_leg_view actual_lview = actual_state.get_legs()[id];

                if ( prev_lview.is_leg_moving() || actual_lview.is_leg_moving() ) {
                        return em::timeq{ 0. };
                }

                const point< 3, world_frame >& sidepath_point = prev_lview.get_sidepath_point();

                // TODO: deeply think about this
                SCHPIN_ASSERT( sidepath_point == actual_lview.get_sidepath_point() );

                std::size_t steps = std::size_t{
                    pose_change_price(
                        robot,
                        previous_state.get_body_pose().pos,
                        actual_state.get_body_pose().pos ) /
                    conf.dist_step };

                steps = std::max( steps, std::size_t{ 1 } );
                std::vector< point< 3, robot_frame > > movement_path =
                    map_f_to_v( em::range( steps + 1 ), [&]( std::size_t i ) {
                            pose< world_frame > b_pose = lin_interp(
                                previous_state.get_body_pose().pos,
                                actual_state.get_body_pose().pos,
                                float( i ) / float( steps ) );
                            return inverse_transform< robot_frame >( sidepath_point, b_pose );
                    } );
                auto res = prev_lview->approximate_movement_price(
                    movement_path, prev_lview.get_vertex_id() );
                return res;
        } );
}

inline bool is_stable( const gait_state_view& state )
{
        line< 3, world_frame > line = {
            state.get_body_pose().pos.position,
            state.get_body_pose().pos.position + vec< 3 >{ 0, 0, -1000.f } };
        // TODO: meh constant on line :)

        sat_line_collision_query line_q{ line };

        std::vector< point< 3, world_frame > > stand_points = filter_optionals(
            state.get_legs(),
            [&]( const gait_leg_view& gview ) -> std::optional< point< 3, world_frame > > {
                    if ( gview.is_leg_moving() ) {
                            return {};
                    }
                    return { gview.get_sidepath_point() };
            } );

        std::vector< triangle< 3, world_frame > > stable_triangles;

        for ( std::size_t i : em::range( stand_points.size() - 2 ) ) {
                for ( std::size_t j : em::range( i + 1, stand_points.size() - 1 ) ) {
                        for ( std::size_t k : em::range( j + 1, stand_points.size() ) ) {
                                triangle< 3, world_frame > base_t{
                                    stand_points[i], stand_points[j], stand_points[k] };

                                stable_triangles.push_back( base_t );
                                stable_triangles.push_back( scale( base_t, 1.05f ) );
                        }
                }
        }

        return em::any_of( stable_triangles, [&]( const triangle< 3, world_frame >& t ) {
                sat_triangle_collision_query triangle_q{ t };

                bool is_colliding = sat_collides( line_q, triangle_q );

                return is_colliding;
        } );
}

inline bool gait_state_is_collisionless(
    const gait_state&                state,
    const gait_body_path&            body_path,
    const leg_container< sidepath >& sidepaths,
    const robot&                     robot,
    const environment&               env )
{
        gait_state_view gview{ state, body_path, robot.get_legs(), sidepaths };

        return em::none_of( gview.get_legs(), [&]( const gait_leg_view& lview ) {
                auto config = lview.get_configuration();

                bool is_colliding = lview.get_leg_view().collides( config, env );

                if ( !is_colliding ) {
                        return false;
                }
                SCHPIN_INFO_LOG(
                    "leg.planner",
                    "leg " << int( lview->get_id() ) << " is in collision, with config "
                           << em::view{ config } << " with tip at " << lview.get_tip_position()
                           << " which is_moving=" << lview.is_leg_moving() << " and vertex_id is "
                           << lview.get_vertex_id() );
                return true;
        } );
}

inline std::vector< gait_predecessor > gait_predecessors(
    const std::vector< gait_state >& raw_states,
    const gait_state&                center,
    const gait_body_path&            body_path,
    const leg_container< sidepath >& sidepaths,
    const robot&                     robot,
    const gait_planner_config&       conf )
{

        gait_state_view center_view{ center, body_path, robot.get_legs(), sidepaths };

        return map_f_to_v( raw_states, [&]( const gait_state& state ) {
                gait_state_view  state_view{ state, body_path, robot.get_legs(), sidepaths };
                gait_predecessor res{ state, em::timeq{ 0.f }, FRESH };

                if ( state_view.count_moving_legs() > conf.max_moving ) {
                        res.status = LEG_COUNT_ERROR;
                        return res;
                }

                bool cant_reach_sidepath =
                    em::any_of( em::range( state_view.get_legs().size() ), [&]( std::size_t i ) {
                            const auto& flview = state_view.get_legs()[i];
                            const auto& tlview = center_view.get_legs()[i];
                            if ( !flview.reaches_sidepath() ) {
                                    return true;
                            }
                            if ( !tlview.get_leg_view().workarea_reaches(
                                     flview.get_sidepath_point() ) ) {
                                    return true;
                            }
                            return false;
                    } );
                if ( cant_reach_sidepath ) {
                        res.status = LEG_UNREACHABLE_ERROR;
                        return res;
                }

                if ( !is_stable( state_view ) ) {
                        res.status = UNSTABLE_ERROR;
                        return res;
                }

                auto var = em::variance( state_view.get_legs(), [&]( const auto& lview ) -> float {
                        return static_cast< float >( lview.get_sidepath_index() );
                } );

                if ( var > conf.max_variance ) {
                        res.status = VARIANCE_HIGH_ERROR;
                        return res;
                }

                em::timeq movement_time{ conf.bodyless_timestep };

                if ( state_view.get_body_pose() != center_view.get_body_pose() ) {
                        movement_time =
                            approximate_gait_movement_time( state_view, center_view, robot, conf );
                }

                res.t = movement_time;
                SCHPIN_ASSERT( res.t != em::timeq{ 0 } );

                return res;
        } );
}

// TODO; logging in entire gait planenr _is_ improtatn for cases that fail for some reason
// we actually need something like "if assert fails, rerun again with extensive logging"

struct gait_reset_failure_report
{
        std::vector< sidepath_failure_report > sidepath_reports;
        std::set< gait_body_pose >             failed_poses;
};

partitioned_eithers< leg_sidepath_generator, sidepath_failure_report > make_sidepath_generators(
    const robot_state_view& robot,
    const gait_body_path&   body_path,
    const planner_context&  context );

inline gait_state approach_gait_state_legs( const gait_state& goal, gait_state actual )
{
        SCHPIN_INFO_LOG( "planner.gait", "Approaching " << actual << " to " << goal );
        SCHPIN_ASSERT( goal.body_pose_i == actual.body_pose_i );
        auto ids  = get_leg_ids( goal.legs );
        auto iter = em::find_if( ids, [&]( leg_id id ) {  //
                std::size_t ai = actual.legs[id].sidepath_i;
                std::size_t gi = goal.legs[id].sidepath_i;

                return ai != gi &&
                       ( ai > gi ? ai - gi : gi - ai ) < 3;  // TODO: random constant much
        } );

        if ( iter != ids.end() ) {
                actual.legs[*iter] = gait_leg_state{
                    .leg_config = actual.legs[*iter].leg_config,
                    .sidepath_i = goal.legs[*iter].sidepath_i,
                    .is_moving  = true };
        }

        return actual;
}

inline bool gait_try_direct_connection( const gait_state& goal, const gait_state& actual )
{
        if ( actual.body_pose_i != goal.body_pose_i ) {
                return false;
        }

        return em::all_of( get_leg_ids( goal.legs ), [&]( leg_id id ) {  //
                return goal.legs[id].sidepath_i == actual.legs[id].sidepath_i;
        } );
}

struct gait_find_path_report
{
        std::size_t remaining_steps;
};

class gait_planner
{
        using gait_d_star = d_star< gait_d_star_config >;

        gait_body_path            body_poses_;
        gait_graph                g_;
        gait_d_star               search_;
        leg_container< sidepath > sidepaths_;
        gait_planner_config       conf_ = {
            .dist_step         = em::distance{ 0.01f },
            .ang_step          = em::angle{ 0.05f },
            .max_variance      = 99999999.f,
            .max_moving        = 1,
            .sidepath_offsets  = { -3 },
            .bodyless_timestep = em::timeq{ 0.25f } };
        gait_db db_con_;

public:
        gait_planner( leg_size_token tok )
          : search_( g_ )
          , sidepaths_( tok )
        {
        }

        const auto& get_graph_search() const
        {
                return search_;
        }

        const leg_container< sidepath >& get_sidepaths() const
        {
                return sidepaths_;
        }

        void clear()
        {
                body_poses_.clear();
                g_.clear();
                search_.clear();  // TODO: does this work?
                sidepaths_ = leg_container< sidepath >{ sidepaths_.get_size_token() };
        }

        opt_error< gait_planner_error >
        event( const reset_chain_event& e, const planner_context& context )
        {
                return reset( e.robot, e.body_path, context )
                    .convert( [&]( const gait_reset_failure_report& r ) {
                            std::stringstream ss;

                            for ( const gait_body_pose& gpose : r.failed_poses ) {
                                    ss << gpose.pos << "\n";
                            }

                            auto e = E( gait_planner_error ) << "Failed to reset gait planner, "
                                                                "planning failed on poses ";
                            e.attach( ss.str() );
                            e.attach( "Sidepath reports: " );
                            e.group_attach( r.sidepath_reports );
                            return e;
                    } );
        }

        opt_error< gait_reset_failure_report > reset(
            robot_state_view       robot,
            const gait_body_path&  body_path,
            const planner_context& context )
        {
                SCHPIN_INFO_LOG( "planner.gait", "Reseting the gait planner" );
                clear();
                body_poses_ =
                    lineary_interpolate_path( body_path, conf_.dist_step, conf_.ang_step );

                for ( gait_body_pose& pose : body_poses_ ) {
                        pose.leg_planners = map_f_to_l(
                            robot.get_legs(), [&]( const leg_state_view< 3 >& leg_view ) {  //
                                    return leg_planner< 3 >{ leg_view->get_graph() };
                            } );
                }

                auto generator_eithers = make_sidepath_generators( robot, body_poses_, context );
                SCHPIN_INFO_LOG( "planner.gait", "Sidepath generators regenerated" );
                if ( !generator_eithers.right.empty() ) {
                        SCHPIN_ERROR_LOG(
                            "planner.gait", "Failed to generate all sidepaths, reportin error" );
                        gait_reset_failure_report report{
                            generator_eithers.right, std::set< gait_body_pose >{} };
                        for ( const sidepath_failure_report& r : generator_eithers.right ) {
                                for ( std::size_t i : r.empty_body_poses ) {
                                        report.failed_poses.insert( body_poses_[i] );
                                }
                        }
                        return report;
                }

                for ( const auto& leg : robot.get_legs() ) {
                        SCHPIN_INFO_LOG(
                            "planner.gait",
                            "Leg "
                                << int( leg->get_id() ) << " neutral at: "
                                << leg->tip_position( leg->get_model().neutral_configuration() ) );
                }

                sidepaths_ = map_f_to_l( generator_eithers.left, []( auto ) {
                        return sidepath{};
                } );

                std::for_each(
                    std::execution::par,
                    std::begin( generator_eithers.left ),
                    std::end( generator_eithers.left ),
                    [&]( leg_sidepath_generator& gen ) {
                            sidepath p = gen.find_sidepath(
                                *robot.get_legs()[gen.get_leg_id()], context.env );

                            SCHPIN_INFO_LOG(
                                "planner.gait",
                                "Sidepath for leg " << int( gen.get_leg_id() )
                                                    << "is: " << em::view{ p } );
                            auto res = lineary_interpolate_path( p, conf_.dist_step );
                            sidepaths_[gen.get_leg_id()] = std::move( res );
                    } );

                gait_vertex_content goal_content{
                    .state  = goal_pose_to_gait_state( context.rob.get_legs(), context.env ),
                    .status = FRESH };
                SCHPIN_ASSERT( state_is_collisionless( goal_content.state, context ) );
                gait_vertex_id goal_id =
                    g_.emplace_vertex( goal_content, search_.make_graph_visitor() ).get_id();

                gait_vertex_content current_content{
                    .state = current_state_to_gait_state( robot, context.env ), .status = FRESH };
                SCHPIN_INFO_LOG( "planner.gait", "current state: " << current_content.state );
                SCHPIN_ASSERT( state_is_collisionless( current_content.state, context ) );
                gait_vertex_id current_id =
                    g_.emplace_vertex( current_content, search_.make_graph_visitor() ).get_id();

                search_.set_task( g_, current_id, goal_id );
                SCHPIN_INFO_LOG( "planner.gait", "Gait planner reseted" );

                db_con_.store_gait_run( body_poses_, sidepaths_ );
                db_con_.store_vertex_creation(
                    current_id, get_gait_view( current_id, context.rob ), FRESH );
                db_con_.store_vertex_creation(
                    goal_id, get_gait_view( goal_id, context.rob ), FRESH );
                return {};
        }

        gait_find_path_report event( find_gait_path_event e, const planner_context& context )
        {
                std::size_t steps = search_.compute_shortest_path(
                    g_,
                    e.step_limit,
                    make_graph_process_visitor< gait_graph >(
                        [&]( const gait_vertex& gv ) -> bool {  // is vertex ready?
                                return gv->status != FRESH;
                        },
                        [&]( gait_vertex_id gid ) {  //
                                initialize_vertex( gid, context );
                        } ) );
                if ( steps != e.step_limit ) {
                        SCHPIN_INFO_LOG(
                            "planner.gait",
                            "Finished generating plan for gait " << g_.size() << " vertexes, "
                                                                 << search_.get_search_state() );
                } else {
                        SCHPIN_INFO_LOG(
                            "planner.gait",
                            "Gait search progress : " << g_.size() << " vertices " );
                }
                gait_find_path_report report{};
                report.remaining_steps = steps;
                return report;
        }

        inline gait_state
        goal_pose_to_gait_state( const leg_container< leg< 3 > >& legs, const environment& env )
        {
                SCHPIN_ASSERT( !body_poses_.empty() );
                std::size_t body_i = body_poses_.size() - 1;
                return {
                    body_i,  //
                    map_f_to_l( get_leg_ids( sidepaths_ ), [&]( leg_id id ) {
                            // SCHPIN_ASSERT( legs.contains( id ) );
                            // SCHPIN_ASSERT( sidepaths.contains( id ) );

                            leg_view          view{ legs[id], body_poses_.back().pos };
                            leg_planner< 3 >& planner = body_poses_[body_i].leg_planners[id];

                            gait_leg_state res;
                            // That is not a good idea TODO
                            res.sidepath_i             = sidepaths_[id].size() - 1;
                            leg_vertex_id start_vertex = view->get_neutral_vertex();
                            res.leg_config             = planner.find_closest_vertex(
                                view, sidepaths_[id][res.sidepath_i], start_vertex, env );
                            res.is_moving = false;
                            return res;
                    } ) };
        }

        inline gait_state
        current_state_to_gait_state( const robot_state_view& robot, const environment& env )
        {
                // TODO: print actual state to the log, so we know how the planning starts
                std::size_t body_i = *min_iterator(
                    em::range( body_poses_.size() ),
                    [&]( std::size_t i ) {  //
                            return pose_change_price(
                                *robot, body_poses_[i].pos, robot.get_pose() );
                    } );
                return {
                    body_i,                                                    //
                    map_f_to_l( get_leg_ids( sidepaths_ ), [&]( leg_id id ) {  //
                            // SCHPIN_ASSERT( robot.get_legs().contains( id ) );
                            const leg_state_view< 3 >& view = robot.get_legs()[id];
                            leg_planner< 3 >& planner       = body_poses_[body_i].leg_planners[id];

                            SCHPIN_ASSERT( view->get_id() == id );
                            SCHPIN_ASSERT( view.is_standing() );
                            // TODO: still ugly...

                            gait_leg_state stand_leg;
                            stand_leg.sidepath_i = closest_point_on_sidepath(
                                sidepaths_[id],
                                robot.get_tip_position( id ) );  // TODO: trigger error in case
                                                                 // the distance is too big!

                            leg_view      l_view{ *view, body_poses_[body_i].pos };
                            leg_vertex_id start_vertex = view->get_neutral_vertex();
                            stand_leg.leg_config       = planner.find_closest_vertex(
                                l_view, sidepaths_[id][stand_leg.sidepath_i], start_vertex, env );

                            stand_leg.is_moving = !view.is_standing();
                            return stand_leg;
                    } ) };
        }

        inline std::vector< gait_state >
        candidate_raw_states( const gait_state& actual, const robot& robot, const environment& env )
        {
                gait_state                new_state_base = actual;
                std::vector< gait_state > res;

                if ( new_state_base.body_pose_i > 0 ) {
                        new_state_base.body_pose_i--;
                }
                res.push_back( new_state_base );  // Without leg change source movement change

                for ( leg_id id : get_leg_ids( actual.legs ) ) {
                        gait_leg_view gview{
                            leg_view< 3 >{
                                robot.get_legs()[id], body_poses_[new_state_base.body_pose_i].pos },
                            new_state_base.legs[id],
                            sidepaths_[id] };
                        leg_planner< 3 >& planner =
                            body_poses_[new_state_base.body_pose_i].leg_planners[id];

                        for ( int offset : conf_.sidepath_offsets ) {
                                offset = std::clamp(
                                    int( gview.get_sidepath_index() ) + offset,
                                    0,
                                    int( sidepaths_[id].size() - 1 ) );

                                gait_leg_state lstate;

                                lstate.sidepath_i = std::size_t( offset );
                                lstate.leg_config = planner.find_closest_vertex(
                                    gview.get_leg_view(),
                                    sidepaths_[id][lstate.sidepath_i],
                                    gview.get_vertex_id(),
                                    env );
                                lstate.is_moving = true;

                                gait_state new_state = new_state_base;
                                new_state.legs[id]   = lstate;
                                res.push_back( new_state );

                                // TODO: HACK
                                new_state.body_pose_i = actual.body_pose_i;
                                res.push_back( new_state );
                        }
                }
                std::sort( res.begin(), res.end() );
                auto last = std::unique( res.begin(), res.end() );
                res.erase( last, res.end() );
                return res;
        }

        // TODO: in case of rewrite, we need some way to enable detailed LOGing for specific cases
        void initialize_vertex( gait_vertex_id gid, const planner_context& context )
        {
                timing_record rec{ .start = timing_record::now() };

                SCHPIN_ASSERT( search_.get_task() );
                const auto& vertex    = g_.get_vertex( gid );
                const auto& current_v = g_.get_vertex( search_.get_task()->current_id );
                // A. generate list of neighbours
                std::vector< gait_state > raw_states =
                    candidate_raw_states( vertex->state, context.rob, context.env );
                if ( vertex->state.body_pose_i == 0 ) {
                        // TODO: may be duplicate
                        raw_states.push_back(
                            approach_gait_state_legs( current_v->state, vertex->state ) );
                }
                if ( gait_try_direct_connection( current_v->state, vertex->state ) ) {
                        raw_states.push_back( current_v->state );
                }
                std::vector< gait_predecessor > prede = gait_predecessors(
                    raw_states, vertex->state, body_poses_, sidepaths_, context.rob, conf_ );

                rec.generation_end = timing_record::now();
                // B. retraction
                for ( gait_predecessor& pred : prede ) {
                        // SCHPIN_ASSERT( state_is_collisionless( pred.state, context ) );

                        if ( pred.status == FRESH ) {
                                SCHPIN_ASSERT( pred.t != em::timeq{ 0 } );
                                // maybe process only FRESH vertices?
                        }

                        if ( pred.status != FRESH ) {
                                continue;
                        }

                        gait_state_view state_view{
                            pred.state, body_poses_, context.rob.get_legs(), sidepaths_ };

                        auto leg_ids = get_leg_ids( pred.state.legs );
                        auto eithers = map_f_to_v(
                            leg_ids,
                            []( leg_id ) -> em::either< gait_leg_state, leg_planner_error > {
                                    return { E( leg_planner_error ) };
                            } );

                        std::mutex mut;

                        std::transform(
                            std::execution::par,
                            std::begin( leg_ids ),
                            std::end( leg_ids ),
                            std::begin( eithers ),
                            [&]( leg_id id ) -> em::either< gait_leg_state, leg_planner_error > {
                                    const auto&       lview = state_view.get_legs()[id];
                                    leg_planner< 3 >& planner =
                                        body_poses_[pred.state.body_pose_i].leg_planners[id];

                                    auto plan_path_f = [&]( leg_vertex_id from, leg_vertex_id to ) {
                                            auto [p, timings] = planner.get_path(
                                                lview.get_leg_view(), from, to, context.env );
                                            std::lock_guard g{ mut };
                                            rec.legs.push_back( timings );
                                            return p;
                                    };

                                    leg_vertex_id source_vertex = planner.find_closest_vertex(
                                        lview.get_leg_view(),
                                        lview.get_sidepath_point(),
                                        lview.get_vertex_id(),
                                        context.env );
                                    return retract_leg_gait_state(
                                        state_view.get_legs()[id],
                                        pred.t,
                                        plan_path_f,
                                        source_vertex );
                            } );
                        auto parted = partition_eithers( eithers );
                        SCHPIN_ASSERT_MSG( parted.right.empty(), em::view{ parted.right } );
                        if ( parted.right.empty() ) {
                                pred.state.legs = map_f_to_l( parted.left, [&]( auto item ) {
                                        return item;
                                } );
                        }
                        // SCHPIN_ASSERT( state_is_collisionless( pred.state, context ) );
                }

                auto cmp = []( const gait_predecessor& a, const gait_predecessor& b ) -> bool {
                        return std::tie( a.status, a.state ) < std::tie( b.status, b.state );
                };
                std::sort( prede.begin(), prede.end(), cmp );
                auto iter = std::unique(
                    prede.begin(),
                    prede.end(),
                    [&]( const gait_predecessor& a, const gait_predecessor& b ) {
                            return a.state == b.state;
                    } );
                prede.erase( iter, prede.end() );

                rec.retraction_end = timing_record::now();

                std::vector< gait_edge > edges;
                // C. find existing, create the rest
                for ( const gait_predecessor& pred : prede ) {
                        auto vertexes = g_.get_vertexes();
                        auto iter     = em::find_if( vertexes, [&]( const gait_vertex& v ) {  //
                                return v->state == pred.state;
                        } );
                        gait_vertex_id v;
                        if ( iter == vertexes.end() ) {
                                gait_vertex_content new_c{
                                    .state = pred.state, .status = pred.status };

                                v = g_.emplace_vertex( new_c, search_.make_graph_visitor() )
                                        .get_id();
                                db_con_.store_vertex_creation(
                                    v, get_gait_view( v, context.rob ), pred.status );
                        } else {
                                v = iter->get_id();
                        }

                        if ( pred.status == FRESH ) {
                                SCHPIN_ASSERT( pred.t != em::timeq{ 0 } );
                        }

                        gait_price  p{ *pred.t };
                        const auto& edge = g_.add_edge(
                            v, gid, gait_edge_content{ p }, search_.make_graph_visitor() );
                        edges.push_back( edge );
                }
                db_con_.store_vertex_edges( edges );
                // D. finished

                // TODO: at this point, we have to acces via 'gid' and not vertex.get_id(), the
                // vertex reference may not have be valid anymore
                g_.ref_content( gid ).status = EXPANDED;

                rec.end = timing_record::now();

                db_con_.store_timing( rec );
        }

        const gait_graph& get_graph() const
        {
                return g_;
        }

        chain< gait_edge_id, gait_vertex_id > get_path() const
        {
                return search_.get_path( g_ );
        }

        chain< gait_price, gait_state_view > get_path_view( const planner_context& con ) const
        {
                return map_f_to_ch(
                    get_path(),
                    [&]( gait_edge_id eid ) -> gait_price {
                            return g_.get_edge( eid ).get_price();
                    },
                    [&]( gait_vertex_id vid ) -> gait_state_view {
                            return get_gait_view( vid, con.rob );
                    } );
        }

        leg_plan_var< 3 > get_leg_plan_step(
            std::size_t            body_pose_i,
            const gait_leg_view&   from_leg,
            const gait_leg_view&   to_leg,
            const planner_context& con )
        {
                if ( from_leg.get_sidepath_index() == to_leg.get_sidepath_index() &&
                     !to_leg.is_leg_moving() ) {
                        return { leg_plan_contact< 3 >{
                            to_leg.get_sidepath_point(), to_leg.get_configuration() } };
                }

                leg_planner< 3 >& planner =
                    body_poses_[body_pose_i].leg_planners[from_leg->get_id()];

                chain< leg_edge_id, leg_vertex_id > leg_path;
                auto [p, timing] = planner.get_path(
                    from_leg.get_leg_view(),
                    from_leg.get_vertex_id(),
                    to_leg.get_vertex_id(),
                    con.env );
                em::ignore( timing );
                p.match(
                    [&]( const auto& p ) {  //
                            leg_path = p;
                    },
                    [&]( const leg_planner_error& e ) {
                            SCHPIN_ERROR_LOG(
                                "planner.gait",
                                "Failed to plan leg path in "
                                "plan generation, this "
                                "should not happen: "
                                    << e );
                    } );
                SCHPIN_INFO_LOG(
                    "planner.gait",
                    " from: " << from_leg.get_vertex_id() << " to: " << to_leg.get_vertex_id()
                              << " leg path: " << leg_path );
                SCHPIN_ASSERT( !leg_path.empty() );

                leg_plan_move< 3 > move;
                for ( auto pview : leg_path ) {
                        move.path.push_back(
                            from_leg.get_leg_view().get_configuration( pview->source_pin ) );
                }

                move.path.push_back(
                    to_leg.get_leg_view().get_configuration( leg_path.get_pins().back() ) );

                for ( std::size_t i : em::range( move.path.size() ) ) {
                        auto p = from_leg->get_graph().get_edge( leg_path[i]->link ).get_price();

                        move.path[i]->link += p;
                }

                return { move };
        }

        movement_plan get_plan( const planner_context& con )
        {
                auto p = get_path();
                SCHPIN_INFO_LOG( "planner.gait", "Converting path into plan, " << p );
                std::vector< plan_step< 3 > > steps = map_f_to_v( p, [&]( auto p_view ) {
                        gait_state_view from_view = get_gait_view( p_view.source_pin, con.rob );
                        gait_state_view to_view   = get_gait_view( p_view.target_pin, con.rob );
                        plan_step< 3 >  res{
                            from_view.get_body_pose().pos,
                            to_view.get_body_pose().pos,
                            map_f_to_l(
                                get_leg_ids( to_view.get_legs() ),
                                [&]( leg_id lid ) {
                                        return get_leg_plan_step(
                                            from_view.get_state().body_pose_i,
                                            from_view.get_legs()[lid],
                                            to_view.get_legs()[lid],
                                            con );
                                } ),
                            g_.get_edge( p_view->link )->get_price() };
                        return res;
                } );

                return { steps };
        }

        gait_state_view get_gait_view( gait_vertex_id gid, const robot& robot ) const
        {
                const auto& vertex = g_.get_vertex( gid );
                return gait_state_view{ vertex->state, body_poses_, robot.get_legs(), sidepaths_ };
        }

        bool state_is_collisionless( const gait_state& state, const planner_context& context )
        {
                return gait_state_is_collisionless(
                    state, body_poses_, sidepaths_, context.rob, context.env );
        }
};

}  // namespace schpin
