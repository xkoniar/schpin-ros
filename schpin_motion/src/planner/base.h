// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "body/base.h"
#include "environment.h"
#include "robot.h"
#include "schpin_lib_exp/event_reactor.h"

#pragma once

namespace schpin
{

struct body_path_failure_event
{
        std::vector< body_vertex_id > vertexes;
        std::vector< body_edge_id >   edges;
};
struct make_vertexes_event
{
        static constexpr auto     priority = event_priority::DEFAULT;
        unsigned                  count;
        std::optional< unsigned > opt_verbosity{};
};

struct planner_context
{
        pose< world_frame >                  current;
        std::optional< pose< world_frame > > goal;
        environment                          env;
        robot                                rob;

        explicit planner_context( robot robot )
          : rob( std::move( robot ) )
        {
        }

        planner_context(
            pose< world_frame >                  current,
            std::optional< pose< world_frame > > goal,
            environment                          env,
            robot                                robot )
          : current( current )
          , goal( goal )
          , env( std::move( env ) )
          , rob( std::move( robot ) )
        {
        }
};
}  // namespace schpin

template <>
struct std::hash< schpin::planner_context >
{
        std::size_t operator()( const schpin::planner_context& context )
        {
                return std::hash< schpin::pose< schpin::world_frame > >()( context.current ) ^
                       std::hash< std::optional< schpin::pose< schpin::world_frame > > >()(
                           context.goal ) ^
                       std::hash< schpin::environment >()( context.env ) ^
                       std::hash< schpin::robot >()( context.rob );
        }
};
