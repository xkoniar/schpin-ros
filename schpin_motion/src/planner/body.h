// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "body/graph.h"
#include "body/rand.h"
#include "body/state_view.h"
#include "environment.h"
#include "events.h"
#include "graph/astar.h"
#include "leg/view.h"
#include "planner/base.h"
#include "robot.h"

#pragma once

namespace schpin
{

struct body_planner_error_tag
{
};

using body_planner_error = error< body_planner_error_tag >;

// usefull for tests
struct valid_path_res
{
        const bool                is_valid;
        const pose< world_frame > failed_pose;
        enum
        {
                BODY,
                LEG
        } failure_source;

        explicit operator bool() const
        {
                return is_valid;
        }
};

inline std::ostream& operator<<( std::ostream& os, const valid_path_res res )
{
        if ( res.is_valid ) {
                os << "no failure";
        } else {
                os << "failed at pose: " << res.failed_pose << "\n"
                   << "failure source: "
                   << ( res.failure_source == valid_path_res::BODY ? "body" : "leg" );
        }

        return os;
}

namespace detail
{
        inline bool body_collides_with_environment_impl(
            const body&                body,
            const pose< world_frame >& offset,
            const environment&         env )
        {
                body_state_view body_view{ body, body_state{ offset } };
                return bs_trees_collides(
                    body_view.get_query(),
                    env.get_query(),
                    occupancy_collides_env_predicate< ENV_MAX_OCCUPANCY / 2 >{} );
        }

        inline bool
        leg_collides_with_environment_impl( const leg_view< 3 >& leg_view, const environment& env )
        {
                // TODO: either this should be part of LegView or none such thing should be
                auto env_query = env.get_query();
                return bs_trees_collides(
                    get_workarea_query( leg_view ),
                    env_query,
                    occupancy_collides_surface_predicate{} );
        }
}  // namespace detail

template < bool EnableExport = true >  // using bool is bad, enum would be
                                       // better
struct body_planner_functions
{

        bool body_collides_with_environment(
            const body&                body,
            const pose< world_frame >& offset,
            const environment&         env )
        {
                return detail::body_collides_with_environment_impl( body, offset, env );
        }

        bool all_legs_touches_with_environment(
            const leg_container< leg< 3 > >& legs,
            const pose< world_frame >&       offset,
            const environment&               env )
        {

                return em::all_of( legs, [&]( const leg< 3 >& leg ) -> bool {
                        auto leg_v = leg_view< 3 >{ leg, offset };
                        return detail::leg_collides_with_environment_impl( leg_v, env );
                } );
        }

        valid_path_res is_valid_path(
            const pose< world_frame >& source_pose,
            const pose< world_frame >& target_pose,
            const robot&               robot,
            const environment&         env )
        {

                // distance dist_step = min(
                // robot.get_body().getCollisionTree().getRootContext().edge_length,
                //                          robot.get_legs()[0].getWorkarea().getRootContext().edge_length
                //                          );
                auto dist_step = em::distance( 0.01f );

                // TODO(squirrel):
                // the angle could be calculated by getting radius of smalles sphere for entire
                // robot strechted at maximum the angle is than angle between two points, which are
                // 'radius' from [0,0,0] and 'dist_step' from each other
                auto angle_step = em::angle( 0.1f );

                pose_distance actual_dist = distance_of( source_pose, target_pose );

                std::size_t steps_c = steps( actual_dist, dist_step, angle_step );

                steps_c = std::max( steps_c, std::size_t( 2 ) );

                std::size_t maxstep = steps_c + 1;
                auto        offset  = *em::find_if( em::range( maxstep ), [&]( std::size_t i ) {
                        float               factor = float( i ) / float( steps_c );
                        pose< world_frame > actual_offset =
                            lin_interp( source_pose, target_pose, factor );

                        return body_collides_with_environment(
                            robot.get_body(), actual_offset, env );
                } );

                if ( offset != maxstep ) {
                        return valid_path_res{
                            false,
                            lin_interp(
                                source_pose, target_pose, float( offset ) / float( steps_c ) ),
                            valid_path_res::BODY };
                }

                // find problematic step for legs
                offset = *em::find_if( em::range( maxstep ), [&]( std::size_t i ) {
                        float               factor = float( i ) / float( steps_c );
                        pose< world_frame > actual_offset =
                            lin_interp( source_pose, target_pose, factor );

                        return !all_legs_touches_with_environment(
                            robot.get_legs(), actual_offset, env );
                } );

                return valid_path_res{
                    offset == maxstep,
                    lin_interp( source_pose, target_pose, float( offset ) / float( steps_c ) ),
                    valid_path_res::LEG };
        }

        std::vector< std::pair< em::distance, body_vertex_id > > get_neighbours_distances(
            const pose< world_frame >& new_pose,
            const robot&               robot,
            const body_graph&          graph )
        {

                auto distances = map_f_to_v(
                    graph.get_vertexes(),
                    [&]( const auto& vertex ) -> std::pair< em::distance, body_vertex_id > {  //
                            pose_distance actual_dist = distance_of( vertex->pos, new_pose );
                            return std::make_pair(
                                actual_dist.dist +
                                    ( *actual_dist.angle_dist ) * robot.get_price_radius(),
                                vertex.get_id() );
                    } );

                std::sort(
                    distances.begin(), distances.end(), [&]( const auto& lh, const auto& rh ) {  //
                            return lh.first < rh.first;
                    } );

                return distances;
        }

        std::vector< body_vertex_id > filter_candidate_neighbours(
            const pose< world_frame >&                                      new_pose,
            const body_graph&                                               graph,
            const std::vector< std::pair< em::distance, body_vertex_id > >& distances,
            const robot&                                                    robot,
            const environment&                                              env,
            std::size_t                                                     count )
        {

                std::vector< body_vertex_id > res{};

                // ugly way to do while :)
                __attribute__( ( unused ) ) auto item = em::find_if(
                    distances,
                    [&]( const std::pair< em::distance, body_vertex_id >& pair ) -> bool {
                            const auto& vertex = graph.get_vertex( pair.second );
                            if ( is_valid_path( new_pose, vertex->pos, robot, env ) ) {
                                    res.emplace_back( vertex.get_id() );
                            }

                            return res.size() == count;
                    } );

                return res;
        }
};
struct body_a_star_config
{
        using queue_item         = a_star_queue_item< body_vertex_id, body_price >;
        using heap               = pairing_heap< queue_item >;
        using heap_handle        = typename heap::handle_type;
        using graph              = body_graph;
        using heuristic_function = zero_graph_heuristics< body_price >;
};

template < typename OnPathFunction >
class body_planner_lambda_visitor
{
        OnPathFunction on_path_f_;

public:
        explicit body_planner_lambda_visitor( OnPathFunction on_path_f )
          : on_path_f_( on_path_f )
        {
        }

        opt_error< body_path_failure_event > has_plan( chain< body_edge_id, body_vertex_id > chain )
        {
                return on_path_f_( std::move( chain ) );
        }
};

struct environment_changed_event
{
};

enum class body_planner_errors
{
        POSE_CLOSE_NEIGHBOUR,
        POSE_NO_NEIGHBOURS,
        POSE_BODY_COLLIDES,
        POSE_LEGS_NO_COLLIDES,
};

inline std::ostream& operator<<( std::ostream& os, body_planner_errors e )
{
        switch ( e ) {
                case body_planner_errors::POSE_CLOSE_NEIGHBOUR:
                        return os << "pose too close to neighbour";
                case body_planner_errors::POSE_NO_NEIGHBOURS:
                        return os << "there are no neighbours near provided pose";
                case body_planner_errors::POSE_BODY_COLLIDES:
                        return os << "robot body is in collision at provided pose";
                case body_planner_errors::POSE_LEGS_NO_COLLIDES:
                        return os << "robot legs do not touch the environment";
        }
        return os;
}

template < bool EnableProfilingExport = true >
class body_planner
{

private:
        body_graph                                      g_;
        a_star< body_a_star_config >                    search_;
        body_generator                                  gen_{};
        body_planner_functions< EnableProfilingExport > func_{};

        enum vertex_creation_config
        {
                REQUIRES_NEIGHBOURS,
                NEIGHBOURLESS
        };

public:
        explicit body_planner( const planner_context& plan_context )
          : search_( g_ )
        {
                // this does not check collision, fix! TODO
                body_vertex_id current = g_.emplace_vertex(
                                               body_vertex_content{ plan_context.current },
                                               search_.make_graph_visitor() )
                                             .get_id();
                if ( plan_context.goal ) {
                        body_vertex_id goal = g_.emplace_vertex(
                                                    body_vertex_content{ *plan_context.goal },
                                                    search_.make_graph_visitor() )
                                                  .get_id();
                        search_.set_task( g_, current, goal );
                }
        }

        bool has_plan() const
        {
                return search_.get_search_state() == graph_search_state::FOUND_PATH;
        }

        em::either< body_vertex_id, body_planner_error >
        set_goal( pose< world_frame > goal_pose, const planner_context& context )
        {
                // Search first, check for collsion not present... TODO
                return create_vertex< NEIGHBOURLESS >( goal_pose, context )
                    .convert_left( [&]( body_vertex_id bid ) {
                            body_vertex_id current = bid;
                            if ( search_.get_task() ) {
                                    current = search_.get_task()->current_id;
                            }
                            search_.set_task( g_, current, bid );
                            return bid;
                    } )
                    .convert_right( [&]( body_planner_errors e ) -> body_planner_error {
                            return E( body_planner_error ).attach( e ) << "Failed to create vertex "
                                                                          "for goal pose, "
                                                                          "reason: ";
                    } );
        }

        em::either< body_vertex_id, body_planner_error >
        set_current( pose< world_frame > pose, const planner_context& context )
        {
                return create_vertex< NEIGHBOURLESS >( pose, context )
                    .convert_left( [&]( body_vertex_id bid ) {
                            body_vertex_id goal = bid;
                            if ( search_.get_task() ) {
                                    goal = search_.get_task()->goal_id;
                            }
                            search_.set_task( g_, bid, goal );
                            return bid;
                    } )
                    .convert_right( [&]( body_planner_errors e ) -> body_planner_error {
                            return E( body_planner_error ).attach( e ) << "Failed to create vertex "
                                                                          "for current pose";
                    } );
        }

        template < typename Visitor >
        opt_error< body_planner_error >
        event( environment_changed_event, const planner_context& context, Visitor )
        {
                using opt_e = opt_error< body_planner_error >;
                search_.clear();

                return create_vertex< NEIGHBOURLESS >( context.current, context )
                    .convert_left( [&]( body_vertex_id current_id ) -> opt_e {
                            if ( !context.goal ) {
                                    return {};
                            }
                            return create_vertex< NEIGHBOURLESS >( *context.goal, context )
                                .convert_left( [&]( body_vertex_id goal_id ) -> opt_e {
                                        search_.set_task( g_, current_id, goal_id );
                                        return {};
                                } )
                                .convert_right( [&]( body_planner_errors e ) -> opt_e {  //
                                        return {
                                            E( body_planner_error ).attach( e ) << "Failed to "
                                                                                   "initialize"
                                                                                   " "
                                                                                   "goal "
                                                                                   "pose" };
                                } )
                                .join();
                    } )
                    .convert_right( [&]( body_planner_errors e ) -> opt_e {  //
                            return {
                                E( body_planner_error ).attach( e ) << "Failed to initialze "
                                                                       "current pose" };
                    } )
                    .join();
        }

        template < typename Visitor >
        opt_error< body_planner_error >
        event( make_vertexes_event e, const planner_context& context, Visitor )
        {
                if ( !context.goal ) {
                        if ( e.opt_verbosity ) {
                                SCHPIN_INFO_LOG(
                                    "planner.body", "Not making vertexes, goal is missing" );
                        }
                        return {};
                }

                while ( e.count > 0 ) {
                        pose< world_frame > offset =
                            gen_.get_random_body_coord( context.current, *context.goal );

                        create_vertex< NEIGHBOURLESS >( offset, context )
                            .match(
                                [&]( body_vertex_id ) {  //
                                        e.count -= 1;
                                },
                                [&]( body_planner_errors err ) {
                                        if ( e.opt_verbosity ) {
                                                SCHPIN_INFO_LOG( "planner.body", err );
                                        }
                                } );
                }
                return {};
        }

        template < typename Visitor >
        opt_error< body_planner_error >
        event( find_body_path_event, const planner_context&, Visitor visitor )
        {
                // replan path
                auto limit = std::numeric_limits< std::size_t >::max();
                limit -= search_.compute_shortest_path( g_, limit );

                if ( !has_plan() ) {
                        return { E( body_planner_error ) << "Failed to find path for body" };
                }
                opt_error< body_path_failure_event > error = visitor.has_plan( get_path() );

                return error.convert( [&]( const body_path_failure_event& e ) {
                        for ( body_edge_id eid : e.edges ) {
                                g_.remove_edge( eid, search_.make_graph_visitor() );
                        }
                        for ( body_vertex_id bid : e.vertexes ) {
                                g_.remove_vertex( bid, search_.make_graph_visitor() );
                        }

                        return E( body_planner_error ) << "Failed to plan body path, had to remove "
                                                          "vertexes from found "
                                                          "path";
                } );
        }

        chain< body_edge_id, body_vertex_id > get_path() const
        {
                return search_.get_path( g_ );
        }

        const body_graph& get_graph() const
        {
                return g_;
        }

        const pose< world_frame >& get_body_pose( body_vertex_id vid ) const
        {
                return get_graph().get_vertex( vid )->pos;
        }

private:
        template < vertex_creation_config RequiresNeighbours >
        em::either< body_vertex_id, body_planner_errors >
        create_vertex( pose< world_frame > offset, const planner_context& context )
        {

                const environment& env   = context.env;
                const robot&       robot = context.rob;
                // check collision with environment
                if ( func_.body_collides_with_environment( robot.get_body(), offset, env ) ) {
                        return { body_planner_errors::POSE_BODY_COLLIDES };
                }
                if ( !func_.all_legs_touches_with_environment( robot.get_legs(), offset, env ) ) {
                        return { body_planner_errors::POSE_LEGS_NO_COLLIDES };
                }

                std::vector< std::pair< em::distance, body_vertex_id > > distances =
                    func_.get_neighbours_distances( offset, robot, get_graph() );

                // check for closest neighbour
                if ( !distances.empty() && distances.front().first < em::distance( 0.001f ) ) {
                        return { distances.front().second };
                }

                // try to attach to existing
                std::vector< body_vertex_id > nbrs = func_.filter_candidate_neighbours(
                    offset, get_graph(), distances, robot, env, 8 );  // TODO: what the number
                                                                      // means? :)
                if ( RequiresNeighbours == REQUIRES_NEIGHBOURS && nbrs.empty() ) {
                        return { body_planner_errors::POSE_NO_NEIGHBOURS };
                }

                // create graph node, attach it
                const auto& new_vertex = g_.emplace_vertex(
                    body_vertex_content{ offset }, search_.make_graph_visitor() );

                auto id_to_pose_f = [&]( body_vertex_id id ) -> const pose< world_frame >& {  //
                        return get_graph().get_vertex( id )->pos;
                };

                auto calc_edge_price_f = [&]( body_vertex_id from, body_vertex_id to ) {
                        pose_distance dist =
                            distance_of( id_to_pose_f( from ), id_to_pose_f( to ) );
                        body_price p =
                            *dist.dist + ( *dist.angle_dist ) * ( *robot.get_price_radius() );
                        return body_edge_content{ p };
                };

                for ( body_vertex_id nb : nbrs ) {
                        g_.add_edge(
                            new_vertex.get_id(),
                            nb,
                            calc_edge_price_f( new_vertex.get_id(), nb ),
                            search_.make_graph_visitor() );
                        g_.add_edge(
                            nb,
                            new_vertex.get_id(),
                            calc_edge_price_f( nb, new_vertex.get_id() ),
                            search_.make_graph_visitor() );
                }

                return { new_vertex.get_id() };
        }
};  // namespace schpin

}  // namespace schpin
