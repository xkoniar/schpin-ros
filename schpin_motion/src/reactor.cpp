// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "reactor.h"

#include "export.h"
#include "gait/rviz.h"
#include "graph/util/rviz.h"
#include "lua_vm.h"
#include "octree/rviz.h"
#include "planner.h"
#include "robot/print.h"
#include "robot/storage.h"
#include "util/interactive_pose.h"

using namespace schpin;
using namespace std::chrono_literals;

struct lua_tick_event
{
        static constexpr auto priority = event_priority::LOW;
};

class planner_reactor
{
        planner planner_;

        event_reactor<
            load_environment_event,
            show_environment_event,
            set_goal_pose_event,
            set_current_pose_event,
            clear_event,
            plan_event,
            export_event,
            export_path_event,
            show_body_collision_event,
            show_leg_graph_event,
            show_workarea_event,
            print_model_event,
            print_pose_cache_event,
            make_vertexes_event,
            find_body_path_event,
            find_gait_path_event,
            make_until_path_event,
            quit_event,
            lua_tick_event >
            reactor_{};

        marker_manager marker_manager_;

        lua_vm_eventmaker lua_vm_;

        interactive_pose goal_marker_;
        interactive_pose current_marker_;

        bool is_running_ = true;

public:
        explicit planner_reactor(
            rclcpp::Node&   rclnode,
            planner_context context,
            leg_size_token  tok )
          : planner_( std::move( context ), tok )
          , marker_manager_( rclnode )
          , goal_marker_( rclnode, "goal", "goal" )
          , current_marker_( rclnode, "current", "body" )
        {
        }

        void load_lua_script( const std::filesystem::path& p )
        {
                SCHPIN_INFO_LOG( "reactor", "Loading lua script: " << p );
                lua_vm_.load_file( p ).optionally_log();
                reactor_.insert( lua_tick_event{} );
        }

        void tick()
        {
                auto opt_event = reactor_.pop();
                if ( opt_event ) {
                        em::match( *opt_event, [&]( auto e ) {
                                event( e );
                        } );
                }
        }

private:
        void event( const quit_event& )
        {
                is_running_ = false;
        }
        void event( const lua_tick_event& )
        {
                lua_vm_
                    .tick( [&]( const lua_event_var& var ) {
                            em::match( var, [&]( auto event ) {
                                    reactor_.insert( event );
                            } );
                    } )
                    .match(
                        [&]( lua_res r ) {
                                if ( r != lua_res::YIELDED ) {
                                        return;
                                }
                                reactor_.insert( lua_tick_event{} );
                        },
                        []( const slua_error& err ) {
                                SCHPIN_ERROR_LOG( "reactor", err );
                        } );
        }
        void event( const load_environment_event& e )
        {
                SCHPIN_INFO_LOG( "reactor", "Loading environment from file: " << e.filename );
                std::ifstream env_data{ e.filename };
                safe_load_json< environment >( env_data )
                    .match(
                        [&]( environment&& env ) {
                                planner_.set_environment( std::move( env ) ).optionally_log();
                        },
                        [&]( const json_storage_error& e ) {
                                SCHPIN_ERROR_LOG(
                                    "reactor",
                                    "Failed to load environment due:\n"
                                        << e );
                        } );
        }
        void event( const show_environment_event& )
        {
                SCHPIN_INFO_LOG( "reactor", "Loading environment into rviz " );
                auto msg = create_oc_visualization(
                    "environment",
                    planner_.get_environment().get_tree(),
                    "world",
                    1,
                    env_occupancy_treshold_predicate< ENV_MAX_OCCUPANCY / 4 >{} );

                marker_manager_.publish( msg );

                msg = create_oc_visualization(
                    "surface",
                    planner_.get_environment().get_tree(),
                    "world",
                    0.5,
                    is_surface_predicate{} );
                marker_manager_.publish( msg );
        }
        void event( const set_goal_pose_event& e )
        {
                pose< world_frame > pos = em::match(
                    e.user_input_var,
                    [&]( point< 3, world_frame > p ) {
                            return pose< world_frame >{ p, goal_marker_.get_pose().orientation };
                    },
                    [&]( quaternion< world_frame > q ) {
                            return pose< world_frame >{ goal_marker_.get_pose().position, q };
                    },
                    [&]( pose< world_frame > p ) {
                            return p;
                    } );
                SCHPIN_INFO_LOG( "reactor", "Setting goal pose to: " << pos );
                goal_marker_.set_pose( pos );
                planner_.set_goal( pos ).optionally_log();
        }
        void event( const set_current_pose_event& e )
        {
                pose< world_frame > pos = em::match(
                    e.user_input_var,
                    [&]( point< 3, world_frame > p ) {
                            return pose< world_frame >{ p, goal_marker_.get_pose().orientation };
                    },
                    [&]( quaternion< world_frame > q ) {
                            return pose< world_frame >{ goal_marker_.get_pose().position, q };
                    },
                    [&]( pose< world_frame > p ) {
                            return p;
                    } );
                current_marker_.set_pose( pos );

                robot_state rstate = planner_.get_robot_state();
                rstate.body.pos    = pos;
                SCHPIN_INFO_LOG(
                    "reactor", "Setting current pose to: " << pos << " as state: " << rstate );
                planner_.set_robot_state( rstate ).optionally_log();
        }
        void event( const clear_event& )
        {
                marker_manager_.clear();
        }

        void event( const show_body_collision_event& )
        {
                SCHPIN_INFO_LOG( "reactor", "Sending body collision model to rviz" );
                const auto& body = planner_.get_robot().get_body();
                auto        msg  = create_oc_visualization(
                    "body_collision",
                    body.get_collision_tree(),
                    body.get_frame(),
                    0.5,
                    occupancy_treshold_predicate{} );
                marker_manager_.publish( msg );
        }

        void event( const show_leg_graph_event& e )
        {
                for ( std::size_t i : em::range( planner_.get_robot().get_legs().size() ) ) {
                        const leg< 3 >& vleg = planner_.get_robot().get_legs()[i];
                        if ( e.index && i != *e.index ) {
                                return;
                        }
                        SCHPIN_INFO_LOG( "reactor", "Sending leg " << i << " graph to rviz" );
                        auto msg = create_graph_visualization(
                            vleg.get_graph(),
                            vleg.get_frame(),
                            [&]( const auto& vertex ) -> point< 3, robot_frame > {
                                    return vleg.get_model().tip_position( vertex->angles );
                            } );
                        marker_manager_.publish( msg );
                }
        }

        void event( const show_workarea_event& e )
        {
                for ( std::size_t i : em::range( planner_.get_robot().get_legs().size() ) ) {
                        const leg< 3 >& vleg = planner_.get_robot().get_legs()[i];
                        if ( e.index && i != *e.index ) {
                                return;
                        }
                        SCHPIN_INFO_LOG( "reactor", "Sending leg " << i << " workarea to rviz" );
                        auto msg = create_oc_visualization(
                            vleg.get_frame() + "-workarea",
                            vleg.get_workarea(),
                            vleg.get_frame(),
                            0.5,
                            occupancy_treshold_predicate{} );
                        marker_manager_.publish( msg );
                }
        }

        void event( const print_model_event& )
        {  //
                print_robot_model( planner_.get_robot() );
        }
        void event( const print_pose_cache_event& )
        {
                for ( const leg< 3 >& pleg : planner_.get_robot().get_legs() ) {
                        SCHPIN_INFO_LOG(
                            "reactor", "Printing pose cache for leg " << pleg.get_id() );
                        // TODO: implement this
                }
        }
        void show_sidepaths()
        {
                const auto& sidepaths = planner_.get_gait_planner().get_sidepaths();
                const auto& legs      = planner_.get_robot().get_legs();
                for ( leg_id id : get_leg_ids( sidepaths ) ) {
                        auto msg = create_sidepath_visualization(
                            sidepaths[id],
                            "world",
                            legs[id].get_frame(),
                            legs[id].get_model().get_color() );

                        marker_manager_.publish( msg );
                }
        }
        void show_body_graph()
        {
                auto msg = create_graph_visualization(
                    planner_.get_body_planner().get_graph(),
                    "world",
                    [&]( const auto& vertex ) -> point< 3, world_frame > {
                            return vertex->pos.position;
                    } );
                marker_manager_.publish( msg );
        }
        void event( const export_event& e )
        {
                export_plan( planner_.get_plan(), planner_.get_robot(), e.filename );
        }
        void event( const export_path_event& e )
        {
                chain< gait_price, gait_state_view > p = planner_.get_path_view();

                std::ofstream out_file{ e.filename };

                if ( out_file.fail() ) {
                        SCHPIN_ERROR_LOG(
                            "reactor", "Failed to open file for export: " << strerror( errno ) );
                        return;
                }

                auto state_to_json = [&]( const gait_state_view& gview ) {
                        nlohmann::json j;
                        j["body"] = gview.get_body_pose().pos;
                        j["legs"] =
                            map_f_to_v( gview.get_legs(), [&]( const gait_leg_view& glview ) {
                                    nlohmann::json j;
                                    j["name"]           = glview->get_name();
                                    j["config"]         = glview.get_configuration();
                                    j["tip"]            = glview.get_tip_position();
                                    j["is_moving"]      = glview.is_leg_moving();
                                    j["sidepath_point"] = glview.get_sidepath_point();
                                    return j;
                            } );
                        return j;
                };

                safe_store_json( out_file, [&] {
                        nlohmann::json res;

                        for ( auto p_view : p ) {
                                res.push_back( state_to_json( p_view.source_pin ) );
                                res.push_back( p_view.link );
                        }
                        if ( !p.empty() ) {
                                res.push_back( state_to_json( p.get_pins().back() ) );
                        }
                        return res;
                } ).optionally_log();
        }
        void event( const plan_event& )
        {
                SCHPIN_INFO_LOG( "reactor", "Executed planning" );
                reactor_.insert( make_until_path_event{} );
                reactor_.insert( find_gait_path_event{} );
        }
        void event( make_vertexes_event e )
        {
                SCHPIN_INFO_LOG( "reactor", "Making new vertexes" );
                if ( e.count > 1 ) {
                        make_vertexes_event new_event{ 1, e.opt_verbosity };
                        planner_.body_event( new_event ).optionally_log();
                        e.count--;
                        reactor_.insert( e );
                }
                show_body_graph();
        }

        void event( find_body_path_event e )
        {
                e.step_limit   = std::numeric_limits< std::size_t >::max();
                auto opt_error = planner_.body_event( e );
                opt_error.optionally_log();

                if ( opt_error ) {
                        SCHPIN_INFO_LOG( "reactor", "body planner failed to replan, bailing out" );
                        return;
                }

                chain< body_edge_id, body_vertex_id > g_path =
                    planner_.get_body_planner().get_path();

                auto msg = create_chain_visualization(
                    g_path,
                    "world",
                    "body_path",
                    [&]( body_vertex_id vid ) -> point< 3, world_frame > {
                            return planner_.get_body_planner()
                                .get_graph()
                                .get_vertex( vid )
                                ->pos.position;
                    } );

                SCHPIN_INFO_LOG(
                    "reactor", "sending path visualization to viz, path size: " << g_path.size() );
                marker_manager_.publish( msg );
                show_sidepaths();
        }

        void event( const make_until_path_event& e )
        {
                if ( planner_.get_body_planner().has_plan() ) {
                        show_body_graph();
                        show_sidepaths();
                        return;
                }
                auto opt_error = planner_.body_event( make_vertexes_event{ 2 } );
                opt_error.optionally_log();
                if ( opt_error ) {
                        return;
                }
                opt_error = planner_.body_event( find_body_path_event{} );
                opt_error.optionally_log();
                if ( opt_error ) {
                        return;
                }
                reactor_.insert( e );
        }

        void event( const find_gait_path_event& e )
        {
                auto report = planner_.gait_event( e );

                if ( report.remaining_steps == e.step_limit ) {
                        reactor_.insert( e );
                        return;
                }

                auto msg =
                    create_gait_visualization( planner_.get_gait_planner(), planner_.get_robot() );
                marker_manager_.publish( msg );
        }
};

class reactor_supervizor : public rclcpp::Node
{
        std::optional< planner_reactor > reactor_;
        rclcpp::TimerBase::SharedPtr     timer_;

public:
        reactor_supervizor()
          : Node(
                "motion_reactor",
                rclcpp::NodeOptions()
                    .enable_rosout( true )
                    .allow_undeclared_parameters( true )
                    .automatically_declare_parameters_from_overrides( true ) )
        {
                // we use static config so this is just ...
                leg_size_token tok{};

                get_config< reactor_config >( *this ).match(
                    [&]( reactor_config cfg ) {
                            load_robot_ros( cfg ).match(
                                [&]( robot&& robot ) {
                                        planner_context context{ std::move( robot ) };
                                        context.goal = std::optional< pose< world_frame > >();

                                        reactor_.emplace( *this, std::move( context ), tok );
                                        SCHPIN_INFO_LOG( "planner", "Reactor started" );
                                        if ( cfg.lua_boot_script ) {
                                                reactor_->load_lua_script( *cfg.lua_boot_script );
                                        }

                                        timer_ = this->create_wall_timer( 10ms, [this] {
                                                tick();
                                        } );
                                },
                                [&]( const robot_error& e ) {  //
                                        SCHPIN_ERROR_LOG(
                                            "reactor", "Failed to load robot due to: " << e );
                                } );
                    },
                    [&]( const config_error& e ) {
                            SCHPIN_ERROR_LOG( "reactor", "Failed to load config: " << e );
                    } );
        }

        void tick()
        {
                reactor_->tick();
        }
};

int main( int argc, char* argv[] )
{
        rclcpp::init( argc, argv );
        rclcpp::spin( std::make_shared< reactor_supervizor >() );
        rclcpp::shutdown();
        return 0;
}
