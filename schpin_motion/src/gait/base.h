// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "body/base.h"
#include "leg/planner.h"
#include "schpin_lib/geom/pose/serialization.h"
#include "schpin_lib/identificator.h"

#include <emlabcpp/physical_quantity.h>

#pragma once

namespace schpin
{
using gait_price       = em::timeq;
using gait_edge_offset = identificator< struct gait_edge_offset_tag, 32 >;
using gait_vertex_id   = identificator< struct gait_vertex_id_tag, 32 >;
using gait_edge_id     = generic_edge_id< gait_vertex_id, gait_edge_offset >;

struct gait_body_pose
{
        pose< world_frame >               pos;
        body_vertex_id                    bid;
        leg_container< leg_planner< 3 > > leg_planners;

        gait_body_pose(
            pose< world_frame >               pose,
            body_vertex_id                    bid,
            leg_container< leg_planner< 3 > > planners )
          : pos( pose )
          , bid( bid )
          , leg_planners( std::move( planners ) )
        {
        }

        gait_body_pose(
            pose< world_frame >              pose,
            body_vertex_id                   bid,
            const leg_container< leg< 3 > >& legs )
          : gait_body_pose( pose, bid, map_f_to_l( legs, [&]( const leg< 3 >& leg ) {
                                    return leg_planner< 3 >{ leg.get_graph() };
                            } ) )
        {
        }
};

constexpr bool operator==( const gait_body_pose& lh, const gait_body_pose& rh )
{
        return lh.pos == rh.pos && lh.bid == rh.bid;
}

constexpr bool operator!=( const gait_body_pose& lh, const gait_body_pose& rh )
{
        return !( lh == rh );
}

inline bool operator<( const gait_body_pose& lh, const gait_body_pose& rh )
{
        return std::tie( lh.bid, lh.pos ) < std::tie( rh.bid, rh.pos );
}

inline std::ostream& operator<<( std::ostream& os, const gait_body_pose& pos )
{
        return os << pos.pos << "-" << pos.bid;
}

using gait_body_path = std::vector< gait_body_pose >;

inline gait_body_path
lineary_interpolate_path( const gait_body_path& ipath, em::distance d_step, em::angle a_step )
{
        gait_body_path res;
        if ( ipath.empty() ) {
                return res;
        }
        for ( std::size_t i : em::range( ipath.size() - 1 ) ) {
                const pose< world_frame >& from = ipath[i].pos;
                const pose< world_frame >& to   = ipath[i + 1].pos;

                std::size_t seg_steps = steps( distance_of( to, from ), d_step, a_step );
                if ( seg_steps == 0 ) {
                        seg_steps = 1;
                }

                for ( std::size_t j : em::range( seg_steps ) ) {
                        res.push_back( gait_body_pose{
                            lin_interp( from, to, float( j ) / float( seg_steps ) ),
                            ipath[i].bid,
                            ipath[i].leg_planners } );
                }
        }
        res.push_back( ipath.back() );
        return res;
}

}  // namespace schpin
