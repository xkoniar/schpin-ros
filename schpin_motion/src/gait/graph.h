// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gait/vertex.h"

#pragma once

namespace schpin
{

class gait_graph_heuristics
{
public:
        constexpr bool minimal() const
        {
                return true;
        }

        void decrease()
        {
        }

        void clear()
        {
        }

        template < typename Vertex >
        gait_price operator()( const Vertex& from, const Vertex& to ) const
        {

                gait_price res =
                    gait_price{ std::abs(
                        float( from->state.body_pose_i ) - float( to->state.body_pose_i ) ) } +
                    gait_price{ em::sum( get_leg_ids( from->state.legs ), [&]( leg_id lid ) {  //
                            return std::abs(
                                float( from->state.legs[lid].sidepath_i ) -
                                float( to->state.legs[lid].sidepath_i ) );
                    } ) };

                res *= em::variance( to->state.legs, [&]( const auto& leg ) {
                        return static_cast< float >( leg.sidepath_i );
                } );

                return res;
        }
};

struct gait_d_star_config
{
        using queue_item         = dstar_queue_item< gait_vertex_id, gait_price >;
        using heap               = pairing_heap< queue_item >;
        using heap_handle        = typename heap::handle_type;
        using graph              = gait_graph;
        using heuristic_function = gait_graph_heuristics;
};

}  // namespace schpin
