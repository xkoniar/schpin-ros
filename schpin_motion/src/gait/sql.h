// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gait/vertex.h"
#include "gait/view.h"

#include <magic_enum.hpp>

#pragma once
namespace schpin
{

struct timing_record
{
        std::chrono::time_point< std::chrono::system_clock > start{};
        std::chrono::time_point< std::chrono::system_clock > generation_end{};
        std::chrono::time_point< std::chrono::system_clock > retraction_end{};
        std::chrono::time_point< std::chrono::system_clock > end{};
        std::vector< leg_timing_record >                     legs{};

        static auto now()
        {
                return std::chrono::system_clock::now();
        }
};

class gait_db
{
        std::filesystem::path out_filename_ = "/home/veverak/Projects/Schpin/debug/motion_db.json";
        std::ofstream         out_;

        gait_body_path bpath_;

public:
        gait_db()
        {
                out_.open( out_filename_, std::ios_base::out | std::ios_base::trunc );
        }

        void store_gait_run( const gait_body_path&, const leg_container< sidepath >& )
        {
        }

        void store_vertex_creation(
            gait_vertex_id          gid,
            const gait_state_view&  gview,
            gait_vertex_status_enum status_en )
        {

                nlohmann::json res;
                res["gid"]    = gid;
                res["pose"]   = gview.get_body_pose().pos;
                res["legs"]   = map_f_to_v( gview.get_legs(), [&]( const gait_leg_view& glview ) {
                        return nlohmann::json{
                            { "conf", glview.get_configuration() },
                            { "tip", glview.get_tip_position() },
                            { "sp_point", glview.get_sidepath_point() },
                            { "sp_i", glview.get_sidepath_index() } };
                } );
                res["status"] = magic_enum::enum_name( status_en );

                out_ << nlohmann::json{ { "type", "vertex" }, { "data", res } } << "\n";
        }

        void store_timing( const timing_record& rec )
        {

                nlohmann::json res;
                auto           setv = [&]( const std::string& key, auto val ) {
                        std::chrono::duration< double > tmp = val.time_since_epoch();
                        res[key]                            = tmp.count();
                };

                setv( "start", rec.start );
                setv( "generation_end", rec.generation_end );
                setv( "retraction_end", rec.retraction_end );
                setv( "end", rec.end );

                for ( const leg_timing_record& rec : rec.legs ) {
                        nlohmann::json tmp;
                        tmp["collides"]     = map_f_to_v( rec.collides, [&]( auto dur ) {
                                return dur.count();
                        } );
                        tmp["near_surface"] = map_f_to_v( rec.near_surface, [&]( auto dur ) {
                                return dur.count();
                        } );
                        tmp["path_length"]  = rec.path_length;
                        res["legs"].push_back( tmp );
                }

                out_ << nlohmann::json{ { "type", "timing" }, { "data", res } } << "\n";
        };

        void store_vertex_edges( const std::vector< gait_edge >& edges )
        {
                for ( const gait_edge& e : edges ) {
                        out_ << nlohmann::json{ { "type", "edge" }, { "data", e } } << "\n";
                }
        }
};

}  // namespace schpin
