// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gait/vertex.h"
#include "gait/view.h"
#include "planner/gait.h"
#include "schpin_lib/chain/rviz.h"

#pragma once

namespace schpin
{

inline visualization_msgs::msg::MarkerArray
create_gait_visualization( const gait_planner& planner, const robot& robot )
{
        auto p = planner.get_path();

        visualization_msgs::msg::MarkerArray res;

        auto bodyarray = create_chain_visualization(
            p, "world", "gait_movement_body", [&]( gait_vertex_id gid ) {
                    gait_state_view v = planner.get_gait_view( gid, robot );
                    return v.get_body_pose().pos.position;
            } );

        res.markers.insert( res.markers.end(), bodyarray.markers.begin(), bodyarray.markers.end() );

        for ( leg_id id : get_leg_ids( robot.get_legs() ) ) {
                auto legarray = create_chain_visualization(
                    p, "world", "gait_leg_path_" + std::to_string( id ), [&]( gait_vertex_id gid ) {
                            gait_state_view v = planner.get_gait_view( gid, robot );

                            return v.get_legs()[id].get_tip_position();
                    } );
                res.markers.insert(
                    res.markers.end(), legarray.markers.begin(), legarray.markers.end() );
        }

        return res;
}

inline visualization_msgs::msg::MarkerArray create_gait_predecessors_visualization(
    const gait_planner& planner,
    gait_vertex_id      gid,
    const robot&        robot,
    const environment&  env )
{

        std::map< leg_id, std::set< std::pair< leg_vertex_id, leg_vertex_id > > > movements;

        const gait_graph& g = planner.get_graph();

        const auto& actual = g.get_vertex( gid )->state;

        for ( gait_edge_id eid : g.get_vertex( gid ).get_reverse_edges() ) {
                const auto&     edge   = g.get_edge( eid );
                const auto&     source = g.get_vertex( edge.get_source_id() )->state;
                gait_state_view view   = planner.get_gait_view( edge.get_source_id(), robot );
                for ( leg_id id : get_leg_ids( source.legs ) ) {
                        movements[id].emplace(
                            view.get_legs()[id].find_closest_vertex_to_sidepath(),
                            actual.legs[id].leg_config );
                }
        }

        visualization_msgs::msg::MarkerArray res;
        gait_state_view                      gview = planner.get_gait_view( gid, robot );
        std::size_t                          i     = 0;
        for ( const auto& [id, paths] : movements ) {
                const leg< 3 >&      leg = robot.get_legs()[id];
                leg_planner< 3 >     planner{ leg.get_graph() };
                const gait_leg_view& lgview = gview.get_legs()[id];

                for ( auto [from, to] : paths ) {
                        auto [p, timing] = planner.get_path( lgview.get_leg_view(), from, to, env );
                        p.match(
                            [&]( const auto& p ) {
                                    auto legarray = create_chain_visualization(
                                        p,
                                        "world",
                                        "gait_leg_move_" + std::to_string( i ),
                                        [&]( leg_vertex_id lid ) {
                                                return lgview.get_leg_view().generate_tip_position(
                                                    lid );
                                        } );

                                    res.markers.insert(
                                        res.markers.end(),
                                        legarray.markers.begin(),
                                        legarray.markers.end() );
                            },
                            [&]( const auto& ) {  // error
                            } );
                        i++;
                }
        }
        return res;
}

}  // namespace schpin
