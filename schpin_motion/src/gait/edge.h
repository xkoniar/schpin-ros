// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gait/base.h"
#include "graph/generics.h"
#include "graph/generics/edge.h"

#pragma once

namespace schpin
{
struct gait_edge_content
{
        gait_price price_;

public:
        gait_price get_price() const
        {
                return price_;
        }
};
using gait_edge =
    generic_edge< gait_edge_id, gait_edge_content, gait_price, gait_vertex_id, gait_edge_offset >;

inline void to_json( nlohmann::json j, const gait_edge_content& content )
{
        j["price"] = content.get_price();
}
}  // namespace schpin
