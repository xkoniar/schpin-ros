// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gait/base.h"
#include "gait/edge.h"
#include "graph/generics/vertex.h"
#include "leg/base.h"
#include "leg/graph/base.h"
#include "schpin_lib/geom/pose.h"
#include "schpin_lib/util.h"

#include <utility>
#include <variant>

#pragma once

namespace schpin
{

struct gait_leg_state
{
        leg_vertex_id leg_config;
        std::size_t   sidepath_i;
        bool          is_moving;
};

using gait_leg_container = leg_container< gait_leg_state >;

struct gait_state
{
        std::size_t        body_pose_i;
        gait_leg_container legs;
};

enum gait_vertex_status_enum
{
        FRESH = 0,
        EXPANDED,
        LEG_COUNT_ERROR,
        LEG_UNREACHABLE_ERROR,
        UNSTABLE_ERROR,
        VARIANCE_HIGH_ERROR
};

struct gait_vertex_content
{
        gait_state              state;
        gait_vertex_status_enum status;
};

using gait_vertex = generic_vertex< gait_vertex_id, gait_vertex_content, gait_edge >;
using gait_graph  = generic_graph< gait_vertex, gait_edge >;

inline std::ostream& operator<<( std::ostream& os, const gait_leg_state& gls )
{
        return os << "(" << ( gls.is_moving ? "m" : "s" ) << "," << gls.leg_config << ","
                  << gls.sidepath_i << ")";
}

constexpr bool operator==( const gait_leg_state& lh, const gait_leg_state& rh )
{
        return lh.is_moving == rh.is_moving && lh.leg_config == rh.leg_config &&
               lh.sidepath_i == rh.sidepath_i;
}
constexpr bool operator!=( const gait_leg_state& lh, const gait_leg_state& rh )
{
        return !( lh == rh );
}

constexpr bool operator<( const gait_leg_state& lh, const gait_leg_state& rh )
{
        return std::tie( lh.leg_config, lh.sidepath_i, lh.is_moving ) <
               std::tie( rh.leg_config, rh.sidepath_i, rh.is_moving );
}

inline std::ostream& operator<<( std::ostream& os, const gait_state& gs )
{
        return os << "gait_state: " << gs.body_pose_i << " [" << em::view{ gs.legs } << "]";
}

constexpr bool operator==( const gait_state& lh, const gait_state& rh )
{
        return lh.body_pose_i == rh.body_pose_i && lh.legs == rh.legs;
}

constexpr bool operator!=( const gait_state& lh, const gait_state& rh )
{
        return !( lh == rh );
}

constexpr bool operator<( const gait_state& lh, const gait_state& rh )
{
        return std::tie( lh.body_pose_i, lh.legs ) < std::tie( rh.body_pose_i, rh.legs );
}

}  // namespace schpin
