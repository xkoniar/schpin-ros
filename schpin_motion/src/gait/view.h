// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "gait/vertex.h"
#include "leg/sidepath.h"

#include <utility>

#pragma once

namespace schpin
{

class gait_leg_view
{
        leg_view< 3 >   view_;
        gait_leg_state  state_;
        const sidepath& sidepath_;

public:
        gait_leg_view( leg_view< 3 > view, gait_leg_state state, const sidepath& sidepath )
          : view_( std::move( view ) )
          , state_( state )
          , sidepath_( sidepath )
        {
        }

        bool reaches_sidepath() const
        {
                // TODO: maybe this hsould be separate function? same as workarea_reaches?
                return view_.workarea_reaches( get_sidepath_point() );
        }

        std::array< em::angle, 3 > get_configuration() const
        {
                return view_.get_configuration( state_.leg_config );
        }

        const leg_view< 3 >& get_leg_view() const
        {
                return view_;
        }

        const gait_leg_state& get_state() const
        {
                return state_;
        }

        leg_vertex_id find_closest_vertex_to_point( const point< 3, world_frame >& p ) const
        {
                return view_.find_closest_vertex( p, state_.leg_config );
        }

        leg_vertex_id find_closest_vertex_to_sidepath() const
        {
                return find_closest_vertex_to_point( get_sidepath_point() );
        }

        point< 3, world_frame > get_tip_position() const
        {
                return view_.generate_tip_position( state_.leg_config );
        }

        bool is_leg_moving() const
        {
                return state_.is_moving;
        }

        leg_vertex_id get_vertex_id() const
        {
                return state_.leg_config;
        }

        const point< 3, world_frame >& get_sidepath_point() const
        {
                return sidepath_[state_.sidepath_i];
        }

        std::size_t get_sidepath_index() const
        {
                return state_.sidepath_i;
        }

        const leg< 3 >& operator*() const
        {
                return *view_;
        }

        const leg< 3 >* operator->() const
        {
                return &*view_;
        }
};

inline leg_container< gait_leg_view > make_gait_leg_views(
    const pose< world_frame >&       body_pose,
    const leg_container< leg< 3 > >& legs,
    const gait_leg_container&        gait_legs,
    const leg_container< sidepath >& sidepaths )
{
        return map_f_to_l( get_leg_ids( legs ), [&]( leg_id id ) {
                return gait_leg_view{
                    leg_view< 3 >{ legs[id], body_pose }, gait_legs[id], sidepaths[id] };
        } );
}

class gait_state_view
{
        gait_state                     state_;
        const gait_body_path&          body_poses_;
        leg_container< gait_leg_view > legs_;

public:
        gait_state_view(
            gait_state                       state,
            const gait_body_path&            body_poses,
            const leg_container< leg< 3 > >& legs,
            const leg_container< sidepath >& sidepaths )
          : state_( state )
          , body_poses_( body_poses )
          , legs_( make_gait_leg_views( get_body_pose().pos, legs, state_.legs, sidepaths ) )
        {
        }

        const gait_state& get_state() const
        {
                return state_;
        }

        const leg_container< gait_leg_view >& get_legs() const
        {
                return legs_;
        }

        const gait_body_pose& get_body_pose() const
        {
                return body_poses_[state_.body_pose_i];
        }

        std::size_t count_moving_legs() const
        {
                return em::count( legs_, [&]( const gait_leg_view& gview ) {  //
                        return gview.is_leg_moving();
                } );
        }
};

}  // namespace schpin
