// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "lua_vm.h"

#include <schpin_lib/lua/print.h>
#include <schpin_lib/lua/vm.h>

using namespace schpin;

struct load_environment_bind
{
        static constexpr std::string_view name = "load_environment";
        static constexpr std::string_view desc = "Loads the model of the environment";

        inline static const auto arg = slua_struct< load_environment_event >(
            lua_key_arg< std::string >{ "filename", "path to the environment file" } );
        inline static const no_return_type res{};
};
struct set_goal_bind
{
        static constexpr std::string_view name = "set_goal";
        static constexpr std::string_view desc = "Sets the goal positions for the planner";

        inline static const auto arg = slua_struct< set_goal_pose_event >( slua_pose_args(
            "point",
            "orientation",
            "Sets the target pose for movement, specify either point, orientation "
            "or "
            "both" ) );

        inline static const no_return_type res{};
};
struct set_current_bind
{
        static constexpr std::string_view name = "set_current";
        static constexpr std::string_view desc =
            "Sets the current position of the robot in the environment manually";

        inline static const auto arg = slua_struct< set_current_pose_event >( slua_pose_args(
            "point",
            "orientation",
            "Sets the current pose for movement, specify either point, orientation "
            "or "
            "both" ) );
        inline static const no_return_type res{};
};
struct plan_bind
{
        static constexpr std::string_view name = "plan";
        static constexpr std::string_view desc = "Starts the planning process";

        inline static const auto           arg = slua_struct< plan_event >();
        inline static const no_return_type res{};
};
struct clear_bind
{
        static constexpr std::string_view name = "clear";
        static constexpr std::string_view desc = "Clears the data";

        inline static const auto           arg = slua_struct< clear_event >();
        inline static const no_return_type res{};
};
struct export_bind
{
        static constexpr std::string_view name = "export";
        static constexpr std::string_view desc =
            "Exports the movement plan to .lua file as appropiate commands";

        inline static const auto arg = slua_struct< export_event >(
            lua_key_arg< std::string >{ "filename", "Path to the exported file" } );
        inline static const no_return_type res{};
};
struct show_body_collision_bind
{
        static constexpr std::string_view name = "show_body_collision";
        static constexpr std::string_view desc = "Shows collisio nstructure used for body";

        inline static const auto           arg = slua_struct< show_body_collision_event >();
        inline static const no_return_type res{};
};
struct show_leg_graph_bind
{
        static constexpr std::string_view name = "show_leg_graph";
        static constexpr std::string_view desc = "Shows movement graph of an leg";
        inline static const auto          arg  = slua_struct< show_leg_graph_event >(
            lua_opt_key_arg< int >{ "index", "index of an leg", -1 } );

        inline static const no_return_type res{};
};
struct show_workarea_bind
{
        static constexpr std::string_view name = "show_workarea";
        static constexpr std::string_view desc = "Shows workarea of leg";

        inline static const auto arg = slua_struct< show_workarea_event >(
            lua_opt_key_arg< int >{ "index", "index of an leg", -1 } );

        inline static const no_return_type res{};
};
struct print_robot_model_bind
{
        static constexpr std::string_view name = "print_robot_model";
        static constexpr std::string_view desc = "Prints various information about the robot";

        inline static const auto arg = slua_struct< print_model_event >();

        inline static const no_return_type res{};
};
struct print_pose_cache_bind
{
        static constexpr std::string_view name = "print_pose_cache";
        static constexpr std::string_view desc = "Prints cache of pose informations";

        inline static const auto arg = slua_struct< print_pose_cache_event >();

        inline static const no_return_type res{};
};
struct make_body_vertexes_bind
{
        static constexpr std::string_view name = "make_body_vertexes";
        static constexpr std::string_view desc = "Create vertexes in graph for body movement";

        inline static const auto arg = slua_struct< make_vertexes_event >(
            lua_key_arg< unsigned >{ "count", "Number of vertexes" },
            lua_opt_key_arg< unsigned >{ "v", "Verbosity level", 0 } );

        inline static const no_return_type res{};
};
struct find_body_path_bind
{
        static constexpr std::string_view name = "find_body_path";
        static constexpr std::string_view desc = "Finds path in existing body graph";

        inline static const auto arg = slua_struct< find_body_path_event >();

        inline static const no_return_type res{};
};
struct make_body_until_path_bind
{
        static constexpr std::string_view name = "make_body_until_path";
        static constexpr std::string_view desc = "Creates vertexes until path is found";

        inline static const auto arg = slua_struct< make_until_path_event >();

        inline static const no_return_type res{};
};
struct export_path_bind
{
        static constexpr std::string_view name = "export_path";
        static constexpr std::string_view desc =
            "Exports the movement path to .lua file as appropiate commands";

        inline static const auto arg = slua_struct< export_path_event >(  //
            lua_key_arg< std::string >{ "filename", "Path to the exported file" } );

        inline static const no_return_type res{};
};
struct quit_bind
{
        static constexpr std::string_view name = "quit";
        static constexpr std::string_view desc = "Stops the motion planner";

        inline static const auto arg = slua_struct< quit_event >();

        inline static const no_return_type res{};
};

struct slua_motion_vm_def
{
        using def = std::tuple<
            slua_callback< load_environment_bind >,
            slua_callback< set_goal_bind >,
            slua_callback< set_current_bind >,
            slua_callback< plan_bind >,
            slua_callback< clear_bind >,
            slua_callback< export_bind >,
            slua_callback< show_body_collision_bind >,
            slua_callback< show_leg_graph_bind >,
            slua_callback< show_workarea_bind >,
            slua_callback< print_robot_model_bind >,
            slua_callback< print_pose_cache_bind >,
            slua_callback< make_body_vertexes_bind >,
            slua_callback< find_body_path_bind >,
            slua_callback< make_body_until_path_bind >,
            slua_callback< export_path_bind >,
            slua_callback< quit_bind > >;
};

struct lua_vm_eventmaker::impl
{
        slua_vm< slua_motion_vm_def > vm;

        opt_error< slua_error > load_file( std::filesystem::path p )
        {
                return vm.load( p );
        }

        bool is_finished()
        {
                return vm.load_finished();
        }

        em::either< lua_res, slua_error > tick( std::function< void( lua_event_var ) > event_cb )
        {
                return em::match(
                    vm.exec( [&]( lua_State*, auto var ) {
                            event_cb( var );
                    } ),
                    [&]( const auto& item ) {
                            return em::either< lua_res, slua_error >{ item };
                    } );
        }
};

lua_vm_eventmaker::lua_vm_eventmaker()
  : impl_ptr_{ std::make_unique< lua_vm_eventmaker::impl >() }
{
}

bool lua_vm_eventmaker::is_finished()
{
        return impl_ptr_->is_finished();
}

opt_error< slua_error > lua_vm_eventmaker::load_file( std::filesystem::path p )
{
        return impl_ptr_->load_file( p );
}

em::either< lua_res, slua_error >
lua_vm_eventmaker::tick( std::function< void( lua_event_var ) > fun )
{
        return impl_ptr_->tick( fun );
}

lua_vm_eventmaker::~lua_vm_eventmaker() = default;
