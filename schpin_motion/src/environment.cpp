// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "environment.h"

#include "base/triangle.h"
#include "util.h"

#include <schpin_lib/octree/visitors.h>
#include <schpin_lib/parser.h>

namespace schpin
{

class environment_surface_visitor
{
        const env_oc_tree& tree_;

public:
        using info_type    = uint8_t;
        using content_type = environment_oc_tree_content;
        using node_type    = const oc_node< content_type >;

        environment_surface_visitor( env_oc_tree& tree )
          : tree_( tree )
        {
        }

        uint8_t default_info() const
        {
                return 0;
        }

        constexpr std::pair< oc_descent_control, info_type >
        on_node( const env_oc_node_view& /*unused*/, info_type ) const
        {
                return { oc_descent_control::FOLLOW_CHILDREN, 0 };
        }

        constexpr std::pair< oc_leaf_control, info_type >
        on_locked_leaf( const env_oc_node_view& /*unused*/, info_type ) const
        {
                return { oc_leaf_control::EXPAND_CHILDREN, 0 };
        }

        void on_leaf(
            const env_oc_node_view& node,
            info_type,
            environment_oc_tree_content& content ) const
        {

                // verify that actual node holds 'air'
                if ( ( *node )->occ() >= ( ENV_MIN_OCCUPANCY + ENV_PRUNNING_DIFF ) ) {
                        return;
                }

                point< 3, oc_frame > desired_pos =
                    node.center_position() + vec< 3 >{ 0, 0, -node.edge_length() };

                auto opt_below_node = tree_.root_view().find_overlapping_leaf( desired_pos );

                if ( !opt_below_node ) {
                        return;
                }

                const env_oc_node_view& below_node = *opt_below_node;

                // verify that node below holds ground
                if ( ( *below_node )->occ() < ENV_MAX_OCCUPANCY - ENV_PRUNNING_DIFF ) {
                        return;
                }

                content.set_surface( true );
        }
};

em::either< environment, env_error > build_environment( const env_config& config )
{
        SCHPIN_INFO_LOG( "env", "Building environment" );
        SCHPIN_INFO_LOG( "env", "Default occ " << config.default_occ );
        SCHPIN_INFO_LOG( "env", "Min edge length " << config.min_edge_length );
        for ( const std::filesystem::path& p : config.paths ) {
                SCHPIN_INFO_LOG( "env", "Path " << p );
        }

        using visitor = oc_collision_visitor<
            oc_collision_checker<
                sat_triangle_collision_query< 3, oc_frame >,
                bs_triangle_collision_query< oc_frame >,
                oc_node< environment_oc_tree_content > >,
            16 >;

        std::vector< parse_error > errors;
        env_oc_tree                tree{
            config.min_edge_length, environment_oc_tree_content( config.default_occ, false ) };

        std::size_t c = 0;

        auto handle_triangles_f = [&](
                                      const std::vector< triangle< 3, world_frame > >& triangles ) {
                auto bound_cube = dimensional_min_max_elem(
                    em::range( triangles.size() * 3 ), [&]( std::size_t i ) {
                            return triangles[i / 3][i % 3];
                    } );
                SCHPIN_INFO_LOG( "env", "Triangle area: " << bound_cube );
                for ( const auto& batch : make_batches< 32 >( triangles ) ) {
                        std::vector< triangle< 3, oc_frame > > transformed_batch =
                            map_f_to_v( batch, [&]( const triangle< 3, world_frame >& t ) {
                                    auto res = tag_cast< oc_frame >( t );
                                    tree.expand( res );
                                    return res;
                            } );
                        c += transformed_batch.size();
                        SCHPIN_INFO_LOG(
                            "env", "Triangle processed count: " << c << "/" << triangles.size() );
                        visitor v{ transformed_batch, occupancy_token::FULL };
                        tree.modify( v );
                }
        };

        for ( const std::filesystem::path& p : config.paths ) {
                parse_triangles< world_frame >( p ).match(
                    handle_triangles_f,
                    [&]( const parse_error& error ) {  //
                            errors.push_back( error );
                    } );
        }

        if ( !errors.empty() ) {
                return {
                    E( env_error ).group_attach( errors )
                    << "Failed to build environment due to errors in parsing" };
        }

        environment_surface_visitor surface_visitor{ tree };
        tree.modify( surface_visitor );
        SCHPIN_INFO_LOG( "env", "Environment size: " << tree.root_view() );
        auto opt_space = tree.root_view().recursive_matched_area( [&]( const auto& pnode ) {
                return ( *pnode )->occ() > ENV_MAX_OCCUPANCY / 2;
        } );
        if ( opt_space ) {
                SCHPIN_INFO_LOG( "env", "Environment used space: " << *opt_space );
        }

        return { environment( std::move( tree ) ) };
}

}  // namespace schpin
