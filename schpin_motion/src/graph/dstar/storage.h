// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <nlohmann/json.hpp>

#pragma once

template < typename DStarConfig >
struct nlohmann::adl_serializer< schpin::d_star< DStarConfig > >
{

        static void to_json( nlohmann::json& j, const schpin::d_star< DStarConfig >& dstar )
        {
                j["graph"] = *dstar;
        }
        static schpin::d_star< DStarConfig > from_json( const nlohmann::json& j )
        {
                using graph              = typename DStarConfig::graph;
                using heuristic_function = typename DStarConfig::heuristic_function;
                return schpin::d_star< DStarConfig >(
                    j.at( "graph" ).get< graph >(), heuristic_function() );
        }
};
