// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/interface.h"
#include "schpin_lib/identificator_container.h"
#include "schpin_lib/view.h"

#pragma once

namespace schpin
{

template < typename VertexID, typename EdgeOffset >
struct generic_edge_id
{
        VertexID   vertex_id;
        EdgeOffset offset;
};

template < typename VertexID, typename EdgeOffset >
inline std::ostream&
operator<<( std::ostream& os, const generic_edge_id< VertexID, EdgeOffset >& id )
{
        return os << id.vertex_id << "|" << id.offset;
}

template < typename VertexID, typename EdgeOffset >
constexpr bool operator==(
    const generic_edge_id< VertexID, EdgeOffset >& lh,
    const generic_edge_id< VertexID, EdgeOffset >& rh )
{
        return lh.vertex_id == rh.vertex_id && lh.offset == rh.offset;
}

template < typename VertexID, typename EdgeOffset >
constexpr bool operator!=(
    const generic_edge_id< VertexID, EdgeOffset >& lh,
    const generic_edge_id< VertexID, EdgeOffset >& rh )
{
        return !( lh == rh );
}

template < typename VertexID, typename EdgeOffset >
inline void to_json( nlohmann::json& j, const generic_edge_id< VertexID, EdgeOffset >& eid )
{
        j["vid"]    = eid.vertex_id;
        j["offset"] = eid.offset;
}

template < typename VertexID, typename EdgeOffset >
inline void from_json( const nlohmann::json& j, generic_edge_id< VertexID, EdgeOffset >& eid )
{
        eid.vertex_id = j.at( "vid" ).get< VertexID >();
        eid.offset    = j.at( "offset" ).get< EdgeOffset >();
}

template < typename EdgeID, typename ContentT, typename PriceT, typename VertexID, typename OffsetT >
class generic_edge : i_graph_edge< ContentT, PriceT, VertexID >
{
        EdgeID   id_;
        ContentT content_;
        VertexID to_;

public:
        using price_type   = PriceT;
        using id_type      = EdgeID;
        using content_type = ContentT;
        using offset_type  = OffsetT;

        generic_edge() = default;
        generic_edge( EdgeID id, content_type content, VertexID vid )
          : id_( id )
          , content_( std::move( content ) )
          , to_( vid )
        {
        }

        content_type& operator*() final
        {
                return content_;
        }
        content_type* operator->() final
        {
                return &content_;
        }

        void set_id( EdgeID eid )
        {
                id_ = eid;
        }

        const content_type& operator*() const final
        {
                return content_;
        }
        const content_type* operator->() const final
        {
                return &content_;
        }
        price_type get_price() const final
        {
                return content_.get_price();
        }

        EdgeID get_id() const
        {
                return id_;
        }

        VertexID get_source_id() const final
        {
                return id_.vertex_id;
        }

        VertexID get_target_id() const final
        {
                return to_;
        }
};
}  // namespace schpin

template < typename EdgeID, typename ContentT, typename PriceT, typename VertexID, typename OffsetT >
struct nlohmann::adl_serializer<
    schpin::generic_edge< EdgeID, ContentT, PriceT, VertexID, OffsetT > >
{
        using edge_t = schpin::generic_edge< EdgeID, ContentT, PriceT, VertexID, OffsetT >;
        static void to_json( nlohmann::json& j, const edge_t& edge )
        {
                j["id"]      = edge.get_id();
                j["content"] = *edge;
                j["target"]  = edge.get_target_id();
        }

        static edge_t from_json( const nlohmann::json& j )
        {
                return edge_t(
                    j.at( "id" ).get< EdgeID >(),
                    j.at( "content" ).get< ContentT >(),
                    j.at( "target" ).get< VertexID >() );
        }
};

template < typename VertexID, typename EdgeOffset >
struct std::hash< schpin::generic_edge_id< VertexID, EdgeOffset > >
{
        std::size_t operator()( const schpin::generic_edge_id< VertexID, EdgeOffset >& eid ) const
        {
                return ( std::hash< VertexID >()( eid.vertex_id ) << sizeof( std::size_t ) / 2 ) +
                       std::hash< EdgeOffset >()( eid.offset );
        }
};

template < typename EdgeID, typename ContentT, typename PriceT, typename VertexID, typename OffsetT >
struct std::hash< schpin::generic_edge< EdgeID, ContentT, PriceT, VertexID, OffsetT > >
{
        std::size_t
        operator()( const schpin::generic_edge< EdgeID, ContentT, PriceT, VertexID, OffsetT >& e )
        {
                return std::hash< EdgeID >()( e.get_id() ) ^ std::hash< ContentT >()( *e ) ^
                       std::hash< VertexID >()( e.get_target_id() );
        }
};
