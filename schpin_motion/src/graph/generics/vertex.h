// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/interface.h"
#include "schpin_lib/identificator_container.h"
#include "schpin_lib/view.h"

#pragma once

namespace schpin
{

template < typename VertexID, typename VertexContent, typename Edge >
class generic_vertex : i_graph_vertex< VertexID, VertexContent, Edge >
{
public:
        using id_type           = VertexID;
        using content_type      = VertexContent;
        using edge_offset_type  = typename Edge::offset_type;
        using edge_id_type      = typename Edge::id_type;
        using edge_content_type = typename Edge::content_type;

private:
        using edge_container         = container< Edge, edge_offset_type, std::vector >;
        using reverse_edge_container = container< edge_id_type, edge_offset_type, std::vector >;
        using const_edge_iterator    = typename edge_container::const_iterator;
        using reverse_const_edge_iterator = typename reverse_edge_container::const_iterator;

        edge_container         edges_{};
        reverse_edge_container reverse_edges_{};
        VertexID               id_;
        VertexContent          content_;

public:
        generic_vertex( VertexID id, VertexContent content )
          : id_( std::move( id ) )
          , content_( std::move( content ) )
        {
        }

        generic_vertex( const generic_vertex& )     = default;
        generic_vertex( generic_vertex&& ) noexcept = default;
        generic_vertex& operator=( const generic_vertex& ) = default;
        generic_vertex& operator=( generic_vertex&& ) noexcept = default;

        ~generic_vertex() override = default;

        void set_id( VertexID vid )
        {
                id_ = vid;
        }

        const Edge& get_edge( edge_offset_type eid ) const final
        {
                return edges_[eid];
        }
        em::view< const_edge_iterator > get_edges() const
        {
                return em::view{ edges_ };
        }
        em::view< reverse_const_edge_iterator > get_reverse_edges() const
        {
                return em::view{ reverse_edges_ };
        }
        edge_id_type add_edge( edge_content_type&& content, VertexID target ) final
        {
                edge_id_type eid{ id_, edges_.size() };
                edges_.emplace_back( eid, std::move( content ), target );
                return eid;
        }
        void add_reverse_edge( edge_id_type eid ) final
        {
                reverse_edges_.emplace_back( eid );
        }
        VertexID get_id() const final
        {
                return id_;
        }

        const VertexContent& operator*() const final
        {
                return content_;
        }
        const VertexContent* operator->() const final
        {
                return &content_;
        }
        VertexContent& operator*() final
        {
                return content_;
        }
        VertexContent* operator->() final
        {
                return &content_;
        }
        edge_content_type& ref_edge_content( edge_offset_type eoffset ) final
        {
                return *edges_[eoffset];
        }
        void swap_edges( edge_offset_type lh, edge_offset_type rh )
        {
                using std::swap;
                swap( edges_[lh], edges_[rh] );
                edges_[lh].set_id( edge_id_type{ id_, lh } );
                edges_[rh].set_id( edge_id_type{ id_, rh } );
        }

        void remove_last_edge()
        {
                edges_.pop_back();
        }

        void remove_reverse_edge( edge_id_type eid )
        {
                auto iter = em::find( reverse_edges_, eid );
                SCHPIN_ASSERT( iter != reverse_edges_.end() );
                reverse_edges_.erase( iter );
        }

private:
        generic_vertex(
            VertexID                 id,
            VertexContent&&          content,
            edge_container&&         edges,
            reverse_edge_container&& reverse_edges )
          : edges_( edges )
          , reverse_edges_( reverse_edges )
          , id_( id )
          , content_( content )
        {
        }

        friend nlohmann::adl_serializer< generic_vertex >;
};

template < typename VertexID, typename VertexContent, typename Edge >
bool operator==(
    const generic_vertex< VertexID, VertexContent, Edge >& lh,
    const generic_vertex< VertexID, VertexContent, Edge >& rh )
{
        return ( lh.get_id() == rh.get_id() );
}
template < typename VertexID, typename VertexContent, typename Edge >
bool operator!=(
    const generic_vertex< VertexID, VertexContent, Edge >& lh,
    const generic_vertex< VertexID, VertexContent, Edge >& rh )
{
        return !( lh == rh );
}

}  // namespace schpin

template < typename VertexID, typename VertexContent, typename Edge >
struct nlohmann::adl_serializer< schpin::generic_vertex< VertexID, VertexContent, Edge > >
{
        using vertex = schpin::generic_vertex< VertexID, VertexContent, Edge >;
        static void to_json( nlohmann::json& json, const vertex& v )
        {
                const auto& content = *v;

                json = nlohmann::json{
                    { "id", v.get_id() },
                    { "content", content },
                    { "edges", v.get_edges() },
                    { "reverse_edges", v.get_reverse_edges() } };
        }

        static vertex from_json( const nlohmann::json& j )
        {
                using edge_container         = typename vertex::edge_container;
                using reverse_edge_container = typename vertex::reverse_edge_container;

                return vertex(
                    j.at( "id" ).get< VertexID >(),
                    j.at( "content" ).get< VertexContent >(),
                    j.at( "edges" ).get< edge_container >(),
                    j.at( "reverse_edges" ).get< reverse_edge_container >() );
        }
};

template < typename VertexID, typename VertexContent, typename Edge >
struct std::hash< schpin::generic_vertex< VertexID, VertexContent, Edge > >
{
        std::size_t operator()( const schpin::generic_vertex< VertexID, VertexContent, Edge >& v )
        {
                std::size_t res =
                    std::hash< VertexContent >()( *v ) ^ std::hash< VertexID >()( v.get_id() );
                emlabcpp::for_each( v.get_edges(), [&]( const Edge& e ) {  //
                        res ^= std::hash< Edge >()( e );
                } );

                return res;
        }
};
