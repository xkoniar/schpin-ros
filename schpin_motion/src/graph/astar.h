// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/base.h"
#include "graph/util.h"
#include "graph/visitor.h"
#include "schpin_lib/chain.h"
#include "schpin_lib/identificator_container.h"
#include "schpin_lib/logging.h"

#include <optional>

#pragma once

namespace schpin
{

template < typename VertexID, typename Price >
struct a_star_queue_item
{
        Price    p;
        VertexID id;

        a_star_queue_item( Price p, VertexID id )
          : p( p )
          , id( id )
        {
        }
};

template < typename VertexID, typename Price >
bool operator<(
    const a_star_queue_item< VertexID, Price >& lh,
    const a_star_queue_item< VertexID, Price >& rh )
{
        return lh.p < rh.p;
}
template < typename VertexID, typename Price >
bool operator>(
    const a_star_queue_item< VertexID, Price >& lh,
    const a_star_queue_item< VertexID, Price >& rh )
{
        return lh.p > rh.p;
}

template < typename Price, typename HeapHandle, typename EdgeID >
struct a_star_vertex_info
{
        Price                       g;
        std::optional< HeapHandle > handle;
        bool                        seen;
        EdgeID                      parent_edge;
};

template < typename AStarConfig >
class a_star
{
public:
        using graph               = typename AStarConfig::graph;
        using heap                = typename AStarConfig::heap;
        using heuristic_function  = typename AStarConfig::heuristic_function;
        using vertex              = typename graph::vertex;
        using vertex_id_type      = typename vertex::id_type;
        using vertex_content_type = typename vertex::content_type;
        using edge                = typename graph::edge;
        using edge_id_type        = typename edge::id_type;
        using queue_item          = typename AStarConfig::queue_item;
        using price_type          = typename graph::price_type;
        using heap_handle         = typename AStarConfig::heap_handle;
        using vertex_info         = a_star_vertex_info< price_type, heap_handle, edge_id_type >;
        using info_container      = container< vertex_info, vertex_id_type, std::vector >;

        static_assert( std::is_base_of< i_graph< vertex, edge >, graph >::value );

private:
        info_container                              vertex_info_;
        heap                                        heap_;
        std::optional< graph_search_task< graph > > task_;
        graph_search_state                          search_state_;
        heuristic_function                          heuristic_function_;

public:
        a_star( const graph& g, heuristic_function hf = {} )
          : vertex_info_( g.size() )
          , search_state_( graph_search_state::NOT_FINISHED )
          , heuristic_function_( hf )
        {
                invalidate_search();
        }

        const info_container& get_vertexes_informations() const
        {
                return vertex_info_;
        }

        void clear()
        {
                task_.reset();
                invalidate_search();
                heuristic_function_.clear();
        }

        const std::optional< graph_search_task< graph > >& get_task() const
        {
                return task_;
        }

        graph_search_state get_search_state() const
        {
                return search_state_;
        }

        void set_task( const graph&, vertex_id_type current, vertex_id_type goal )
        {
                task_ = graph_search_task< graph >{ current, goal };
                invalidate_search();
        }

        void clear_task()
        {
                task_.reset();
                invalidate_search();
        }
        auto make_graph_visitor()
        {
                return graph_visitor< graph >::make(
                    on_edge_change_callback( [&]( const graph&, const auto&... ) {  //
                            invalidate_search();
                    } ),
                    before_swap_vertex_callback(
                        [&]( const graph&, const vertex& lh, const vertex& rh ) {  //
                                using namespace std;
                                swap( vertex_info_[lh.get_id()], vertex_info_[rh.get_id()] );
                                invalidate_search();
                        } ),
                    before_pop_vertex_callback( [&]( const graph&, const vertex& ) {  //
                            vertex_info_.pop_back();
                            invalidate_search();
                    } ),
                    after_append_vertex_callback( [&]( const graph&, const vertex& ) {  //
                            vertex_info_.emplace_back();
                            invalidate_search();
                    } ) );
        }

        chain< edge_id_type, vertex_id_type > get_path( const graph& graph ) const
        {

                chain< edge_id_type, vertex_id_type > res;

                if ( !task_ ) {
                        return res;
                }

                if ( search_state_ != graph_search_state::FOUND_PATH ) {
                        return res;
                }

                const vertex* actual = &graph.get_vertex( task_->goal_id );

                res.push_back( actual->get_id() );

                while ( actual->get_id() != task_->current_id ) {

                        const edge& e = graph.get_edge( parent_edge( actual->get_id() ) );

                        vertex_id_type prev_id = e.get_source_id();
                        actual                 = &graph.get_vertex( prev_id );

                        res.push_back( actual->get_id() );
                        res.back()->link = e.get_id();
                }

                res.reverse();

                return res;
                // TODO: method same as in dstar
        }
        std::size_t compute_shortest_path( const graph& g, std::size_t step_limit )
        {
                return compute_shortest_path(
                    g,
                    step_limit,
                    make_graph_process_visitor< graph >(
                        []( auto ) {
                                return true;
                        },
                        []( auto ) {} ) );
        }
        template < typename Visitor >
        std::size_t compute_shortest_path( const graph& g, std::size_t step_limit, Visitor visitor )
        {
                if ( !task_ ) {
                        return 0;
                }

                return *em::find_if( em::range( step_limit ), [&]( std::size_t ) {
                        check_path_existence();
                        if ( !has_candidate_queue_item() ) {
                                return true;
                        }
                        process_queue_item( g, visitor );
                        return false;
                } );
        }

private:
        template < typename Visitor >
        void process_queue_item( const graph& gr, Visitor visitor )
        {
                SCHPIN_ASSERT( task_ );
                vertex_id_type actual_id = heap_.top().id;
                const vertex&  actual    = gr.get_vertex( actual_id );

                if ( !visitor.vertex_ready( actual ) ) {
                        visitor.initialize( actual_id );
                }

                heap_.pop();
                opt_handle( actual_id ).reset();
                seen( actual_id ) = true;

                for ( const edge& e : actual.get_edges() ) {
                        vertex_id_type target = e.get_target_id();
                        if ( seen( target ) ) {
                                continue;
                        }
                        price_type candidate_g = g( actual_id ) + e.get_price();

                        if ( candidate_g >= g( target ) ) {
                                continue;
                        }

                        parent_edge( target ) = e.get_id();
                        g( target )           = candidate_g;

                        if ( !opt_handle( target ) ) {
                                opt_handle( target ) = std::make_optional(
                                    heap_.push( queue_item{ candidate_g, target } ) );
                        } else {
                                heap_.update(
                                    *opt_handle( target ), queue_item( candidate_g, target ) );
                        }
                }
        }

        void check_path_existence()
        {
                SCHPIN_ASSERT( task_ );
                vertex_id_type goal = task_->goal_id;
                if ( g( goal ) == std::numeric_limits< price_type >::max() ) {
                        search_state_ = graph_search_state::PATH_MISSING;
                } else {
                        search_state_ = graph_search_state::FOUND_PATH;
                }
        }
        bool has_candidate_queue_item()
        {
                SCHPIN_ASSERT( task_ );
                vertex_id_type goal = task_->goal_id;
                if ( heap_.empty() ) {
                        return false;
                }

                return heap_.top().p <= g( goal );
        }
        vertex_info& info( vertex_id_type id )
        {
                return vertex_info_[id];
        }
        const vertex_info& info( vertex_id_type id ) const
        {
                return vertex_info_[id];
        }
        bool& seen( vertex_id_type id )
        {
                return info( id ).seen;
        }
        price_type& g( vertex_id_type id )
        {
                return info( id ).g;
        }
        std::optional< heap_handle >& opt_handle( vertex_id_type id )
        {
                return info( id ).handle;
        }
        std::optional< heap_handle >& opt_handle( const vertex& v )
        {
                return info( v.get_id() ).handle;
        }
        edge_id_type& parent_edge( vertex_id_type id )
        {
                return info( id ).parent_edge;
        }
        const edge_id_type& parent_edge( vertex_id_type id ) const
        {
                return info( id ).parent_edge;
        }
        void invalidate_search()
        {
                heap_.clear();
                for ( vertex_info& info : vertex_info_ ) {
                        info.g    = std::numeric_limits< price_type >::max();
                        info.seen = false;
                        info.handle.reset();
                }
                if ( task_ ) {
                        g( task_->current_id )          = price_type( 0. );
                        opt_handle( task_->current_id ) = std::make_optional(
                            heap_.push( queue_item{ price_type( 0. ), task_->current_id } ) );
                }
                search_state_ = graph_search_state::NOT_FINISHED;
        }
};

}  // namespace schpin

template < typename AStarConfig >
struct std::hash< schpin::a_star< AStarConfig > >
{
        std::size_t operator()( const schpin::a_star< AStarConfig >& astar )
        {
                using graph = typename schpin::a_star< AStarConfig >::Graph;
                return std::hash< graph >()( *astar ) ^
                       std::hash< std::optional< schpin::graph_search_task< graph > > >()(
                           astar.get_task() );
        }
};
