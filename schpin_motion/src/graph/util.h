// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/interface.h"
#include "schpin_lib/geom/point.h"
#include "schpin_lib_exp/algorithm.h"

#pragma once

namespace schpin
{

template <
    typename Tag,
    typename Graph,
    typename VertexToPointFunction,
    typename FilterFunction,
    typename VertexID = typename Graph::vertex_id_type >
auto graph_find_closest_vertex(
    const point< 3, Tag >&  target_point,
    VertexID                actual_id,
    const Graph&            graph,
    VertexToPointFunction&& v_to_p_f,
    FilterFunction&&        filter_f ) -> typename Graph::vertex_id_type
{
        using vertex = typename Graph::vertex;
        using edge   = typename Graph::edge;

        em::distance actual_distance{ 1. };
        em::distance best_nb_distance{ 0. };
        VertexID     next_id = actual_id;
        while ( actual_distance > best_nb_distance &&
                !em::almost_equal( actual_distance, best_nb_distance, em::default_epsilon * 4 ) ) {

                actual_id            = next_id;
                const vertex& actual = graph.get_vertex( actual_id );

                if ( actual.get_edges().empty() ) {
                        break;
                }

                auto closest_successor_edge_iter =
                    min_iterator( actual.get_edges(), [&]( const edge& e ) {
                            if ( !filter_f( e.get_target_id() ) ) {
                                    return std::numeric_limits< em::distance >::max();
                            }
                            return distance_of( v_to_p_f( e.get_target_id() ), target_point );
                    } );

                const vertex& best_nb =
                    graph.get_vertex( closest_successor_edge_iter->get_target_id() );

                if ( !filter_f( best_nb.get_id() ) ) {
                        break;
                }

                actual_distance  = distance_of( v_to_p_f( actual.get_id() ), target_point );
                best_nb_distance = distance_of( v_to_p_f( best_nb.get_id() ), target_point );

                next_id = best_nb.get_id();
        }

        return actual_id;
}

template <
    typename Tag,
    typename Graph,
    typename VertexToPointFunction,
    typename VertexID = typename Graph::vertex_id_type >
auto graph_find_closest_vertex(
    const point< 3, Tag >&  target_point,
    VertexID                actual_id,
    const Graph&            graph,
    VertexToPointFunction&& v_to_p_f ) -> typename Graph::vertex_id_type
{
        return graph_find_closest_vertex(
            target_point,
            actual_id,
            graph,
            std::forward< VertexToPointFunction >( v_to_p_f ),
            [&]( auto ) {
                    return true;
            } );
}

template < typename Graph, typename ReadyFunction, typename InitializeFunction >
class lambda_graph_process_visitor : i_graph_process_visitor< Graph >
{
        ReadyFunction      ready_f_;
        InitializeFunction initialize_f_;

public:
        lambda_graph_process_visitor( ReadyFunction ready_f, InitializeFunction initialize_f )
          : ready_f_( ready_f )
          , initialize_f_( initialize_f )
        {
        }

        using vertex    = typename Graph::vertex;
        using vertex_id = typename Graph::vertex_id_type;

        bool vertex_ready( const vertex& v ) final
        {
                return ready_f_( v );
        }
        void initialize( vertex_id vid ) final
        {
                initialize_f_( vid );
        };
};

template < typename Graph, typename ReadyFunction, typename InitializeFunction >
lambda_graph_process_visitor< Graph, ReadyFunction, InitializeFunction >
make_graph_process_visitor( ReadyFunction rf, InitializeFunction inif )
{
        return { rf, inif };
}

}  // namespace schpin
