// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Copyright 2017 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */
#include "graph/util/backtrack.h"

#include <algorithm>
#include <boost/heap/pairing_heap.hpp>
#include <list>
#include <map>
#include <memory>
#include <queue>
#include <ros/assert.h>
#include <utility>

#pragma once

namespace schpin
{

/**
 * Implementation of dijkstra algorithm using handle mechanism. There is no internal loop that
 * iterates entire graph, instead, API provides 'Handle' class as 'step reference'. Each advance of
 * handle to next handle, either advances in history or makes one iteration of dijsktra. Handle also
 * serves as way to get discovered vertex at that step, edge used and getting shortest path from
 * start to discovered vertex.
 */
template < typename Graph >
class DijkstraAlgorithm
{
public:
        typedef typename Graph::Vertex    Vertex;
        typedef typename Graph::Edge      Edge;
        typedef typename Graph::VertexID  VertexID;
        typedef Backtrack< Vertex, Edge > Backtrack;

        struct Handle
        {
                typedef typename std::list< Vertex >::iterator Iterator;

                bool operator==( const Handle& other ) const
                {
                        return iterator == other.iterator;
                }

        private:
                Iterator iterator;
                friend DijkstraAlgorithm;
        };

        DijkstraAlgorithm( Graph& graph, Vertex start )
          : graph_( graph )
          , queue_()
          , backtrack_( start )
          , open_()
        {

                enqueue( start, 0. );
                advance();
        }

        Handle nextVertex( Handle handle )
        {
                if ( std::next( handle.iterator ) == backtrack_.end() && !queue_.empty() ) {
                        advance();
                };

                handle.iterator = std::next( handle.iterator );
                return handle;
        }

        Handle prevVertex( Handle handle )
        {
                handle.iterator = std::prev( handle.iterator );
                return handle;
        };

        Handle begin()
        {
                Handle h;
                h.iterator = backtrack_.begin();

                return h;
        };

        Handle edgeBegin()
        {
                return next( begin() );
        };

        Handle end()
        {
                Handle h;
                h.iterator = backtrack_.end();
                return h;
        };

        Vertex& refVertex( Handle handle )
        {
                return *handle.iterator;
        };

        Edge& refDiscoveryEdge( Handle handle )
        {
                return *backtrack_.getEdgeIter( handle.iterator );
        };

        Backtrack singleTrack( Handle handle )
        {
                return backtrack_.singleTrack( handle.iterator );
        };

private:
        // Queue stores iterators to container of 'open' vertexes.
        struct QueueItem;

        typedef std::map< VertexID, QueueItem > OpenStorage;
        typedef typename OpenStorage::iterator  OpenStorageIterator;

        struct QueueCompare
        {
                inline bool
                operator()( const OpenStorageIterator& lh, const OpenStorageIterator& rh ) const;
        };

        typedef boost::heap::
            pairing_heap< OpenStorageIterator, boost::heap::compare< QueueCompare > >
                Queue;

        typedef typename Queue::handle_type QueueHandle;

        /* structure to store information into priority queue */
        struct QueueItem
        {
                Vertex                  vertex;
                double                  priority;
                QueueHandle             handle;
                std::shared_ptr< Edge > discovery_edge;

                QueueItem( Vertex ver, double prio )
                  : vertex( ver )
                  , priority( prio )
                  , handle()
                  , discovery_edge( nullptr )
                {
                }
        };

        std::reference_wrapper< Graph > graph_;
        Queue                           queue_;
        Backtrack                       backtrack_;
        OpenStorage                     open_;

        // set of methods to ease work with iterators ----------------------------

        const Vertex& getVertex( const OpenStorageIterator& iter ) const
        {
                return iter->second.vertex;
        };

        double& refPriority( OpenStorageIterator& iter ) const
        {
                return iter->second.priority;
        };

        QueueItem& refQueueItem( OpenStorageIterator& iter ) const
        {
                return iter->second;
        };

        QueueHandle& refQueueHandle( OpenStorageIterator& iter ) const
        {
                return iter->second.handle;
        };

        void setDiscoveryEdge( OpenStorageIterator& iter, Edge& edge ) const
        {
                iter->second.discovery_edge = std::make_shared< Edge >( edge );
        };

        // -----------------------------------------------------------------------

        void enqueue( Vertex target, double priority )
        {
                QueueItem item( target, priority );
                enqueue( item );
        }

        void enqueue( Vertex target, double priority, Edge edge )
        {
                QueueItem item( target, priority );
                item.discovery_edge = std::make_shared< Edge >( edge );
                enqueue( item );
        };

        void enqueue( QueueItem item )
        {
                ROS_ASSERT( open_.find( item.vertex.getID() ) == open_.end() );

                std::pair< OpenStorageIterator, bool > res =
                    open_.insert( std::pair< VertexID, QueueItem >( item.vertex.getID(), item ) );

                ROS_ASSERT( res.second );

                res.first->second.handle = queue_.push( res.first );
        }

        void exploreEdge( Edge& edge, double base_priority )
        {

                Vertex target   = graph_.get().getVertex( edge.getTargetID() );
                double priority = base_priority + edge.getPrice();

                OpenStorageIterator iter = open_.find( target.getID() );

                // item is discovered first time ---------------------------------------

                if ( iter == open_.end() ) {
                        enqueue( target, priority, edge );
                        return;
                }

                // item was discovered -------------------------------------------------

                ROS_ASSERT( getVertex( iter ) == target );

                if ( refPriority( iter ) <= priority )  // new priority is lower, ignore
                {
                        return;
                };

                // this assert checks if item we are going to update exists in the queue, without
                // using handle, which may not be valid
                ROS_ASSERT(
                    std::find_if(
                        queue_.begin(), queue_.end(), [&]( const OpenStorageIterator& item ) {
                                return getVertex( item ) == getVertex( iter );
                        } ) != queue_.end() );

                refPriority( iter ) = priority;
                setDiscoveryEdge( iter, edge );

                queue_.decrease( refQueueHandle( iter ) );
                return;
        }

        void advance()
        {
                QueueItem item = queue_.top()->second;

                if ( item.discovery_edge ) {
                        backtrack_.push( item.vertex, *item.discovery_edge );
                }
                queue_.pop();

                std::for_each(
                    graph_.get().outgoingEdgeBegin( item.vertex ),
                    graph_.get().outgoingEdgeEnd( item.vertex ),
                    [&]( Edge edge ) {
                            this->exploreEdge( edge, item.priority );
                    } );
        }
};

template < typename Graph >
bool DijkstraAlgorithm< Graph >::QueueCompare::operator()(
    const OpenStorageIterator& lh,
    const OpenStorageIterator& rh ) const
{
        return lh->second.priority > rh->second.priority;
}
}  // namespace schpin
