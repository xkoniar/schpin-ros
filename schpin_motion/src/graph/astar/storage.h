// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <nlohmann/json.hpp>

#pragma once

template < typename AStarConfig >
struct nlohmann::adl_serializer< schpin::a_star< AStarConfig > >
{

        static void to_json( nlohmann::json& j, const schpin::a_star< AStarConfig >& astar )
        {
                j["graph"] = *astar;
        }
        static schpin::a_star< AStarConfig > from_json( const nlohmann::json& j )
        {
                using graph              = typename AStarConfig::Graph;
                using heuristic_function = typename AStarConfig::heuristic_function;
                return schpin::a_star< AStarConfig >(
                    j.at( "graph" ).get< graph >(), heuristic_function() );
        }
};
