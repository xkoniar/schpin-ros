// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <functional>
#include <ostream>

#pragma once

namespace schpin
{

template < typename Graph >
struct graph_search_task
{
        using vertex_id_type = typename Graph::vertex_id_type;
        using price_type     = typename Graph::price_type;
        vertex_id_type current_id;
        vertex_id_type goal_id;
};

enum class graph_search_state
{
        FOUND_PATH,
        NOT_FINISHED,
        PATH_MISSING
};

inline std::ostream& operator<<( std::ostream& os, graph_search_state state )
{
        switch ( state ) {
                case graph_search_state::FOUND_PATH:
                        return os << "found path";
                case graph_search_state::NOT_FINISHED:
                        return os << "not finished";
                case graph_search_state::PATH_MISSING:
                        return os << "path missng";
        }
        return os;
}

}  // namespace schpin

template < typename Graph >
struct std::hash< schpin::graph_search_task< Graph > >
{
        std::size_t operator()( const schpin::graph_search_task< Graph >& g )
        {
                using vertex_id = typename Graph::VertexID;
                return std::hash< vertex_id >()( g.current_id ) ^
                       std::hash< vertex_id >()( g.goal_id );
        }
};
