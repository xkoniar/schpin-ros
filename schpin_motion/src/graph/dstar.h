// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Copyright 2017 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */

#include "graph/base.h"
#include "graph/interface.h"
#include "graph/util.h"
#include "graph/visitor.h"
#include "schpin_lib/chain.h"
#include "schpin_lib/identificator_container.h"
#include "schpin_lib/util.h"

#include <algorithm>
#include <array>
#include <utility>
#include <vector>

#pragma once

namespace schpin
{

/*
 * QueueItem is used in priority queues
 */
template < typename VertexID, typename Price >
struct dstar_queue_item
{
        std::array< Price, 2 > keys;
        VertexID               vid;

        dstar_queue_item( std::array< Price, 2 > keys, VertexID vid )
          : keys( keys )
          , vid( vid )
        {
        }
};

template < typename VertexID, typename Price >
constexpr bool operator<(
    const dstar_queue_item< VertexID, Price >& lh,
    const dstar_queue_item< VertexID, Price >& rh )
{
        return lh.keys < rh.keys;
}

template < typename VertexID, typename Price >
constexpr bool operator>(
    const dstar_queue_item< VertexID, Price >& lh,
    const dstar_queue_item< VertexID, Price >& rh )
{
        return lh.keys > rh.keys;
}

template < typename Price, typename HeapHandle >
struct d_star_vertex_info
{
        Price            g;
        Price            rhs;
        Price            extra_price;
        std::bitset< 8 > flags;
        HeapHandle       handle;
};

template < typename DStarConfig >
class d_star
{
public:
        using graph              = typename DStarConfig::graph;
        using vertex             = typename graph::vertex;
        using edge               = typename graph::edge;
        using heap               = typename DStarConfig::heap;
        using queue_item         = typename DStarConfig::queue_item;
        using heuristic_function = typename DStarConfig::heuristic_function;
        using vertex_id          = typename vertex::id_type;
        using edge_id            = typename edge::id_type;
        using price              = typename graph::price_type;
        using heap_handle        = typename DStarConfig::heap_handle;
        using vertex_info        = d_star_vertex_info< price, heap_handle >;
        using info_container     = container< vertex_info, vertex_id, std::vector >;

        static_assert( std::is_base_of< i_graph< vertex, edge >, graph >::value );

        // there are 8 bits in dstar content
        enum flags
        {
                OPEN = 0
        };

private:
        info_container                              vertex_info_;
        heap                                        open_;
        graph_search_state                          search_state_;
        heuristic_function                          heuristic_function_;
        std::optional< graph_search_task< graph > > task_;
        price                                       k_{ 0.f };

public:
        d_star( const graph& g, heuristic_function hf = heuristic_function{} )
          : search_state_( graph_search_state::NOT_FINISHED )
          , heuristic_function_( hf )
        {
                for ( std::size_t i : em::range( *g.size() ) ) {
                        em::ignore( i );
                        vertex_info vi;
                        vi.g           = std::numeric_limits< price >::max();
                        vi.rhs         = std::numeric_limits< price >::max();
                        vi.extra_price = price{ 0 };
                        vertex_info_.emplace_back( vi );
                }
        }

        const info_container& get_vertexes_informations() const
        {
                return vertex_info_;
        }

        void clear()
        {
                // TODO: clear vertex_info
                open_.clear();
                search_state_ = graph_search_state::NOT_FINISHED;
                heuristic_function_.clear();
                task_.reset();
        }

        const std::optional< graph_search_task< graph > >& get_task() const
        {
                return task_;
        }

        graph_search_state get_search_state() const
        {
                return search_state_;
        }

        void set_task( const graph& gr, vertex_id current, vertex_id goal )
        {
                if ( task_ ) {
                        g( task_->goal_id )   = std::numeric_limits< price >::max();
                        rhs( task_->goal_id ) = calculate_rhs( gr, task_->goal_id );

                        update_vertex( gr, task_->goal_id );

                        k_ += hf( gr, task_->current_id, current );
                }
                g( current )   = std::numeric_limits< price >::max();
                rhs( current ) = std::numeric_limits< price >::max();
                // NOTE: the order of setting current/goal is relevant for the case when current ==
                // goal
                g( goal )   = std::numeric_limits< price >::max();
                rhs( goal ) = price( 0 );
                task_       = graph_search_task< graph >{ current, goal };
                update_vertex( gr, goal );
                update_vertex( gr, current );
                search_state_ = graph_search_state::NOT_FINISHED;
        }

        auto make_graph_visitor()
        {
                return graph_visitor< graph >::make(
                    after_append_edge_callback( [&]( const graph& g, const edge& e ) {  //
                            update_vertex( g, e.get_source_id() );
                    } ),
                    after_pop_edge_callback( [&]( const graph& g, const edge& e ) {  //
                            update_vertex( g, e.get_source_id() );
                    } ),
                    after_append_vertex_callback( [&]( const graph& g, const vertex& v ) {  //
                            vertex_info_.emplace_back();
                            vertex_info vi;
                            vi.g                     = std::numeric_limits< price >::max();
                            vi.rhs                   = std::numeric_limits< price >::max();
                            vi.extra_price           = price{ 0 };
                            vertex_info_[v.get_id()] = vi;

                            update_vertex( g, v.get_id() );
                    } ),
                    before_pop_vertex_callback( [&]( const graph&, const vertex& v ) {  //
                            SCHPIN_ASSERT( v.get_edges().empty() );
                            if ( flags( v.get_id() )[OPEN] ) {
                                    open_.erase( handle( v.get_id() ) );
                            }
                    } ) );
        }

        void set_extra_price( const graph& graph, vertex_id id, price p )
        {
                extra_price( id ) = p;
                update_vertex( graph, id );
        }
        price get_extra_price( const graph&, vertex_id id ) const
        {
                return extra_price( id );
        }

        chain< edge_id, vertex_id > get_path( const graph& graph ) const
        {

                chain< edge_id, vertex_id > res;

                if ( !task_ ) {
                        return res;
                }

                if ( search_state_ != graph_search_state::FOUND_PATH ) {
                        return res;
                }

                const vertex* actual = &graph.get_vertex( task_->current_id );

                res.push_back( actual->get_id() );

                while ( actual->get_id() != task_->goal_id ) {

                        auto iter = min_iterator(
                            actual->get_edges(),
                            [&]( const edge& e ) {  //
                                    return g( e.get_target_id() ) + e.get_price();
                            } );

                        SCHPIN_ASSERT( iter != actual->get_edges().end() );

                        actual = &graph.get_vertex( iter->get_target_id() );

                        res.push_back( actual->get_id() );
                        res.back().link = iter->get_id();
                }

                return res;
        }

        std::size_t compute_shortest_path( const graph& g, std::size_t step_limit )
        {
                return compute_shortest_path(
                    g,
                    step_limit,
                    make_graph_process_visitor< graph >(
                        []( auto ) {
                                return true;
                        },
                        []( auto ) {} ) );
        }

        template < typename Visitor >
        std::size_t
        compute_shortest_path( const graph& gr, std::size_t step_limit, Visitor visitor )
        {
                if ( !task_ ) {
                        return 0;
                }
                static_assert( std::is_base_of_v< i_graph_process_visitor< graph >, Visitor > );

                vertex_id current = task_->current_id;

                return *em::find_if( em::range( step_limit ), [&]( std::size_t ) {  //
                        bool has_candidate_item =
                            !open_.empty() && ( open_.top().keys < keys( gr, current ) ||
                                                rhs( current ) != g( current ) );
                        if ( !has_candidate_item ) {
                                if ( g( task_->current_id ) !=
                                     std::numeric_limits< price >::max() ) {
                                        search_state_ = graph_search_state::FOUND_PATH;
                                } else {
                                        search_state_ = graph_search_state::PATH_MISSING;
                                }
                                return true;
                        }
                        if ( !is_top_ready_for_processing( gr, visitor ) ) {
                                return false;
                        }

                        vertex_id vertex = open_.top().vid;

                        pop_open( vertex );

                        if ( g( vertex ) >= rhs( vertex ) ) {
                                g( vertex ) = rhs( vertex );
                        } else {
                                g( vertex ) = std::numeric_limits< price >::max();
                                update_vertex( gr, vertex );
                        }
                        for ( const edge_id& eid : gr.get_vertex( vertex ).get_reverse_edges() ) {
                                update_vertex( gr, gr.get_edge( eid ).get_source_id() );
                        }
                        return false;
                } );
        }

private:
        price calculate_rhs( const graph& gr, const vertex_id& vid ) const
        {
                const auto& edges = gr.get_vertex( vid ).get_edges();

                if ( edges.empty() ) {
                        return std::numeric_limits< price >::max();
                }

                return em::min_elem( edges, [&]( const edge& e ) {  //
                        return e.get_price() + g( e.get_target_id() );
                } );
        }

        std::array< price, 2 > keys( const graph& gr, vertex_id vertex )
        {
                SCHPIN_ASSERT( task_ );
                if ( g( vertex ) > rhs( vertex ) ) {
                        return {
                            rhs( vertex ) + hf( gr, task_->current_id, vertex ) + k_,
                            rhs( vertex ) };
                }
                return { g( vertex ) + hf( gr, task_->current_id, vertex ) + k_, g( vertex ) };
        }

        template < typename Visitor >
        bool is_top_ready_for_processing( const graph& gr, Visitor visitor )
        {
                SCHPIN_ASSERT( task_ );
                vertex_id     actual_vid = open_.top().vid;
                const vertex& actual     = gr.get_vertex( actual_vid );

                if ( !visitor.vertex_ready( actual ) ) {
                        visitor.initialize( actual_vid );
                        return false;
                }
                return true;
        }

        void update_vertex( const graph& gr, vertex_id id )
        {
                if ( id != task_->goal_id ) {
                        rhs( id ) = calculate_rhs( gr, id ) + extra_price( id );
                }

                // if rhs/g price are inconsistent, update the vertex
                if ( g( id ) != rhs( id ) ) {
                        update_open( gr, id );
                }
        }

        void update_open( const graph& gr, vertex_id id )
        {
                if ( flags( id )[OPEN] ) {
                        open_.update( handle( id ), queue_item{ keys( gr, id ), id } );
                } else {
                        flags( id )[OPEN] = 1;
                        handle( id )      = open_.push( queue_item{ keys( gr, id ), id } );
                }
        }

        void pop_open( vertex_id id )
        {
                flags( id )[OPEN] = 0;
                open_.pop();
        }

        vertex_info& info( vertex_id id )
        {
                return vertex_info_[id];
        }
        const vertex_info& info( vertex_id id ) const
        {
                return vertex_info_[id];
        }

        heap_handle& handle( vertex_id id )
        {
                return info( id ).handle;
        }

        std::bitset< 8 >& flags( vertex_id id )
        {
                return info( id ).flags;
        }

        const std::bitset< 8 >& flags( vertex_id id ) const
        {
                return info( id ).flags;
        }
        price& extra_price( vertex_id id )
        {
                return info( id ).extra_price;
        }
        const price& extra_price( vertex_id id ) const
        {
                return info( id ).extra_price;
        }
        price& g( vertex_id id )
        {
                return info( id ).g;
        }
        const price& g( vertex_id id ) const
        {
                return info( id ).g;
        }
        price& rhs( vertex_id id )
        {
                return info( id ).rhs;
        }
        const price& rhs( vertex_id id ) const
        {
                return info( id ).rhs;
        }

        price hf( const graph& g, vertex_id from, vertex_id to )
        {
                return heuristic_function_( g.get_vertex( from ), g.get_vertex( to ) );
        }
};

}  // namespace schpin

template < typename Config >
struct std::hash< schpin::d_star< Config > >
{
        std::size_t operator()( const schpin::d_star< Config >& adstar )
        {
                using graph          = typename schpin::d_star< Config >::graph;
                using info_container = typename schpin::d_star< Config >::info_container;
                // TODO: maybe avoiding the state is bad thing?
                return std::hash< info_container >()( adstar.get_vertexes_informations() ) ^
                       std::hash< std::optional< schpin::graph_search_task< graph > > >()(
                           adstar.get_task() );
        }
};
