// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib_exp/visitor.h"

#pragma once

namespace schpin
{

struct AfterAppendVertexTag;
struct AfterAppendEdgeTag;
struct AfterPopEdgeTag;
struct BeforeSwapVertexTag;
struct BeforeSwapEdgeTag;
struct BeforePopVertexTag;
struct BeforePopEdgeTag;

template < typename Function >
visitor_callback< Function, AfterAppendVertexTag > after_append_vertex_callback( Function f )
{
        return { f };
}
template < typename Function >
visitor_callback< Function, AfterAppendEdgeTag > after_append_edge_callback( Function f )
{
        return { f };
}
template < typename Function >
visitor_callback< Function, AfterPopEdgeTag > after_pop_edge_callback( Function f )
{
        return { f };
}
template < typename Function >
visitor_callback< Function, BeforeSwapVertexTag > before_swap_vertex_callback( Function f )
{
        return { f };
}
template < typename Function >
visitor_callback< Function, BeforeSwapEdgeTag > before_swap_edge_callback( Function f )
{
        return { f };
}
template < typename Function >
visitor_callback< Function, BeforePopVertexTag > before_pop_vertex_callback( Function f )
{
        return { f };
}
template < typename Function >
visitor_callback< Function, BeforePopEdgeTag > before_pop_edge_callback( Function f )
{
        return { f };
}

template < typename Function >
visitor_callback< Function, AfterAppendEdgeTag, BeforeSwapEdgeTag, BeforePopEdgeTag >
on_edge_change_callback( Function f )
{
        return { f };
}

template < typename Graph, typename... Callbacks >
class graph_visitor
{
        visitor_driver< Callbacks... > driver_;

        using vertex = typename Graph::vertex;
        using edge   = typename Graph::edge;

public:
        template < typename... NewCallbacks >
        static graph_visitor< Graph, NewCallbacks... > make( NewCallbacks... cbs )
        {
                return { std::move( cbs )... };
        }

        graph_visitor( Callbacks... cbs )
          : driver_( std::move( cbs )... )
        {
        }

        void after_append_vertex( const Graph& g, const vertex& v )
        {
                driver_.template call< AfterAppendVertexTag >( g, v );
        }

        void after_append_edge( const Graph& g, const edge& e )
        {
                driver_.template call< AfterAppendEdgeTag >( g, e );
        }

        void after_pop_edge( const Graph& g, const edge& e )
        {
                driver_.template call< AfterPopEdgeTag >( g, e );
        }

        void before_swap_vertex( const Graph& g, const vertex& lh, const vertex& rh )
        {
                driver_.template call< BeforeSwapVertexTag >( g, lh, rh );
        }

        void before_swap_edge( const Graph& g, const edge& lh, const edge& rh )
        {
                driver_.template call< BeforeSwapEdgeTag >( g, lh, rh );
        }

        void before_pop_vertex( const Graph& g, const vertex& v )
        {
                driver_.template call< BeforePopVertexTag >( g, v );
        }

        void before_pop_edge( const Graph& g, const edge& e )
        {
                driver_.template call< BeforePopEdgeTag >( g, e );
        }
};

}  // namespace schpin
