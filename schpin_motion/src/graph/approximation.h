// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/algorithm.h"
#include "schpin_lib/geom/point.h"
#include "schpin_lib/logging.h"

#include <queue>
#include <set>
#include <unordered_set>

#pragma once

namespace schpin
{
template <
    typename Price,
    typename Tag,
    typename Container,
    typename Graph,
    typename VertexToPointFunction >
Price approximate_edge_price(
    const Container&                     movement_path,
    const typename Graph::vertex_id_type start_v,
    const Graph&                         g,
    VertexToPointFunction                v_to_p_f )
{
        using vertex_id = typename Graph::vertex_id_type;
        using edge      = typename Graph::edge;

        SCHPIN_ASSERT( !g.empty() );

        if ( movement_path.size() < 2 ) {
                return Price{ 0.f };
        }

        // B. find closes vertex to from/to called V
        auto vid = graph_find_closest_vertex( movement_path.front(), start_v, g, v_to_p_f );

        const point< 3, Tag >& from = movement_path[0];
        const point< 3, Tag >& to   = movement_path[1];
        // TODO: this is shiet
        point< 3, Tag > center =
            cast_to_point< Tag >( cast_to_vec( to ) + cast_to_vec( from ) ) / 2;

        float p_density    = 0;
        float priority_sum = 0;

        // C. explore 'N' closest vertexes
        static constexpr std::size_t n = 8;

        std::unordered_set< vertex_id > seen;
        seen.reserve( 3 * n );

        std::queue< vertex_id > to_see;
        seen.insert( vid );
        to_see.push( vid );

        // run BFS and add insert into queue new vertexes until 3*N was seen.
        // This is random guess for effective algorithm
        while ( !to_see.empty() ) {
                vertex_id actual = to_see.front();
                to_see.pop();

                point< 3, Tag > edge_from = v_to_p_f( actual );

                for ( const edge& e : g.get_vertex( actual ).get_edges() ) {
                        point< 3, Tag > edge_to = v_to_p_f( e.get_target_id() );
                        vec< 3 >        v       = edge_to - edge_from;
                        em::angle       alpha   = vec_angle( v, to - from );
                        // TODO: meh
                        point< 3, Tag > edge_center =
                            cast_to_point< Tag >(
                                cast_to_vec( edge_from ) + cast_to_vec( edge_to ) ) /
                            2;
                        float priority =
                            *distance_of( edge_center, center ) * std::abs( float( cos( alpha ) ) );
                        float density = *e.get_price() / *length_of( v );
                        p_density += priority * density;
                        priority_sum += priority;
                        vertex_id target = e.get_target_id();
                        if ( seen.find( target ) != seen.end() ) {
                                continue;
                        }
                        seen.insert( target );

                        if ( seen.size() < n ) {
                                to_see.push( target );
                        }
                }
        }

        if ( priority_sum != 0.f ) {
                p_density /= priority_sum;
        }

        SCHPIN_ASSERT( p_density >= 0.f );

        // F. return price for this segment
        return Price{ *distance_of( to, from ) * p_density } +
               approximate_edge_price< Price, Tag >( em::tail( movement_path ), vid, g, v_to_p_f );
}

}  // namespace schpin
