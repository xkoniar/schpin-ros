// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/interface.h"
#include "graph/visitor.h"
#include "schpin_lib/algorithm.h"
#include "schpin_lib/identificator_container.h"
#include "schpin_lib/view.h"
#include "schpin_lib_exp/visitor.h"

#pragma once

namespace schpin
{
template < typename Price >
class zero_graph_heuristics
{
public:
        constexpr bool minimal() const
        {
                return true;
        }

        void decrease()
        {
        }

        void clear()
        {
        }

        template < typename T >
        Price operator()( const T&, const T& )
        {
                return Price{};
        }
};

template < typename VertexT, typename EdgeT >
class generic_graph : i_graph< VertexT, EdgeT >
{
public:
        using vertex                = VertexT;
        using edge                  = EdgeT;
        using price_type            = typename edge::price_type;
        using edge_content_type     = typename edge::content_type;
        using edge_id_type          = typename edge::id_type;
        using vertex_id_type        = typename vertex::id_type;
        using vertex_content_type   = typename vertex::content_type;
        using vertex_container      = container< vertex, vertex_id_type, std::vector >;
        using const_vertex_iterator = typename vertex_container::const_iterator;
        using dummy_visitor         = graph_visitor< generic_graph< vertex, edge > >;

private:
        vertex_container vertexes_;

public:
        explicit generic_graph()
          : vertexes_()
        {
        }

        explicit generic_graph( std::vector< vertex > vertexes )
          : vertexes_( std::move( vertexes ) )
        {
        }

        constexpr bool empty() const
        {
                return vertexes_.empty();
        }

        void clear()
        {
                vertexes_.clear();
        }

        vertex_id_type size() const
        {
                return vertexes_.size();
        }

        template < typename Visitor = dummy_visitor >
        const vertex& emplace_vertex( vertex_content_type vcontent, Visitor vis = {} )
        {
                vertexes_.emplace_back( vertex_id_type{ vertexes_.size() }, std::move( vcontent ) );

                vis.after_append_vertex( *this, vertexes_.back() );

                return vertexes_.back();
        }
        em::view< const_vertex_iterator > get_vertexes() const
        {
                return em::view{ vertexes_ };
        }

        const vertex& get_vertex( const vertex_id_type& id ) const final
        {
                SCHPIN_ASSERT( id < size() );
                return vertexes_[id];
        }

        const edge& get_edge( const edge_id_type& eid ) const final
        {
                return get_vertex( eid.vertex_id ).get_edge( eid.offset );
        }

        vertex_content_type& ref_content( const vertex_id_type& id ) final
        {
                return *vertexes_[id];
        }

        auto& ref_edge_content( const edge_id_type& eid )
        {
                return vertexes_[eid.vertex_id].ref_edge_content( eid.offset );
        }

        template < typename Visitor = dummy_visitor >
        void clear_out_edges( vertex_id_type vid, Visitor vis = {} )
        {
                auto edges = em::reversed( vertexes_[vid].get_edges() );
                for ( const edge& e : edges ) {
                        remove_edge( e.get_id(), vis );
                }
        }

        template < typename Visitor = dummy_visitor >
        void clear_in_edges( vertex_id_type vid, Visitor vis = {} )
        {
                auto edge_ids = em::reversed( vertexes_[vid].get_reverse_edges() );
                for ( edge_id_type eid : edge_ids ) {
                        remove_edge( eid, vis );
                }
        }

        template < typename Visitor = dummy_visitor >
        void remove_vertex( vertex_id_type vid, Visitor vis = {} )
        {

                clear_out_edges( vid, vis );
                clear_in_edges( vid, vis );

                swap_vertexes( vid, vertexes_.back().get_id(), vis );

                vis.before_pop_vertex( *this, vertexes_.back() );
                vertexes_.erase( std::prev( vertexes_.end() ) );
        }

        template < typename Visitor = dummy_visitor >
        void swap_vertexes( vertex_id_type lid, vertex_id_type rid, Visitor vis = {} )
        {
                // TODO: this is a slow approach, but relatively safe
                auto& lh = vertexes_[lid];
                auto& rh = vertexes_[rid];

                auto edges = merge(
                    merge(
                        map_f_to_v(
                            lh.get_reverse_edges(),
                            [&]( edge_id_type eid ) -> edge {  //
                                    return get_edge( eid );
                            } ),
                        lh.get_edges() ),
                    merge(
                        map_f_to_v(
                            rh.get_reverse_edges(),
                            [&]( edge_id_type eid ) -> edge {  //
                                    return get_edge( eid );
                            } ),
                        rh.get_edges() ) );

                clear_out_edges( lid, vis );
                clear_in_edges( lid, vis );
                clear_out_edges( rid, vis );
                clear_in_edges( rid, vis );

                vis.before_swap_vertex( *this, get_vertex( lid ), get_vertex( rid ) );

                lh.set_id( rid );
                rh.set_id( lid );
                std::swap( lh, rh );

                for ( const edge& e : edges ) {
                        vertex_id_type from = e.get_source_id();
                        vertex_id_type to   = e.get_target_id();
                        if ( from == lid ) {
                                from = rid;
                        } else if ( from == rid ) {
                                from = lid;
                        }

                        if ( to == lid ) {
                                to = rid;
                        } else if ( to == rid ) {
                                to = lid;
                        }
                        add_edge( from, to, *e, vis );
                }
        }

        template < typename Visitor = dummy_visitor >
        void remove_edge( edge_id_type eid, Visitor vis = {} )
        {

                vertex& source = vertexes_[eid.vertex_id];

                vis.before_swap_edge( *this, get_edge( eid ), source.get_edges().back() );

                edge actual_edge = source.get_edges()[*eid.offset];
                edge last_edge   = source.get_edges().back();

                vertex& last_edge_target = vertexes_[last_edge.get_target_id()];
                last_edge_target.remove_reverse_edge( last_edge.get_id() );
                last_edge_target.add_reverse_edge( eid );

                source.swap_edges( eid.offset, last_edge.get_id().offset );

                vis.before_pop_edge( *this, source.get_edges().back() );

                vertexes_[actual_edge.get_target_id()].remove_reverse_edge( eid );
                last_edge = source.get_edges().back();
                source.remove_last_edge();

                vis.after_pop_edge( *this, last_edge );
        }

        template < typename Visitor = dummy_visitor >
        const edge& add_edge(
            vertex_id_type    from,
            vertex_id_type    to,
            edge_content_type content,
            Visitor           vis = {} )
        {
                auto eid = vertexes_[from].add_edge( std::move( content ), to );
                vertexes_[to].add_reverse_edge( eid );
                vis.after_append_edge( *this, get_edge( eid ) );
                return get_edge( eid );
        }

        template < typename UnaryFunction >
        void modify_all( UnaryFunction f )
        {
                for ( vertex& v : vertexes_ ) {
                        f( *v );
                }
        }

        template < typename Visitor = dummy_visitor >
        void purge_all_edges( vertex_id_type vid, Visitor vis = {} )
        {
                const vertex& v       = get_vertex( vid );
                auto          out_ids = map_f_to_v(
                    v.get_edges(),
                    [&]( const edge& e ) {  //
                            return e.get_id();
                    } );

                for ( edge_id_type eid : em::reversed( out_ids ) ) {
                        remove_edge( eid, vis );
                        // ok, bucket removal would be more efficient :)
                }

                std::vector< edge_id_type > ingoing_ids{
                    v.get_reverse_edges().begin(), v.get_reverse_edges().end() };

                for ( edge_id_type eid : ingoing_ids ) {
                        remove_edge( eid, vis );
                }
        }
};
}  // namespace schpin

template < typename Vertex, typename Edge >
struct std::hash< schpin::generic_graph< Vertex, Edge > >
{
        std::size_t operator()( const schpin::generic_graph< Vertex, Edge >& g )
        {
                std::size_t res = 0;
                for ( const Vertex& v : g.get_vertexes() ) {
                        res ^= std::hash< Vertex >()( v );
                }
                return res;
        }
};

template < typename Vertex, typename Edge >
struct nlohmann::adl_serializer< schpin::generic_graph< Vertex, Edge > >
{
        static void
        to_json( nlohmann::json& json, const schpin::generic_graph< Vertex, Edge >& graph )
        {
                json = nlohmann::json{ { "vertexes", graph.get_vertexes() } };
        }
        static auto from_json( const nlohmann::json& j )
        {
                return schpin::generic_graph< Vertex, Edge >{
                    j.at( "vertexes" ).get< std::vector< Vertex > >() };
        }
};
