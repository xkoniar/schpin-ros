// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <type_traits>

#pragma once

namespace schpin
{

template < typename GraphType >
class i_graph_path_visitor  // NOLINT(cppcoreguidelines-special-member-functions)
{
public:
        using edge           = typename GraphType::edge;
        using vertex         = typename GraphType::vertex_;
        using vertex_content = typename GraphType::vertex_content_type;

        virtual void visit_edge( const edge& edge )                                = 0;
        virtual void visit_vertex( const vertex& vertex, vertex_content& content ) = 0;
        virtual ~i_graph_path_visitor()                                            = default;
};

template < typename GraphType >
class i_graph_process_visitor  // NOLINT(cppcoreguidelines-special-member-functions)
{
public:
        using vertex                               = typename GraphType::vertex;
        using vertex_id                            = typename GraphType::vertex_id_type;
        virtual bool vertex_ready( const vertex& ) = 0;
        virtual void initialize( vertex_id vid )   = 0;
        virtual ~i_graph_process_visitor()         = default;
};

template < typename Content, typename Price, typename VertexID >
class i_graph_edge  // NOLINT(cppcoreguidelines-special-member-functions)
{
public:
        virtual const Content& operator*() const     = 0;
        virtual const Content* operator->() const    = 0;
        virtual Content&       operator*()           = 0;
        virtual Content*       operator->()          = 0;
        virtual Price          get_price() const     = 0;
        virtual VertexID       get_source_id() const = 0;
        virtual VertexID       get_target_id() const = 0;
        virtual ~i_graph_edge()                      = default;
};

template < typename VertexID, typename VertexContent, typename Edge >
class i_graph_vertex  // NOLINT(cppcoreguidelines-special-member-functions)
{
public:
        using edge_content = typename Edge::content_type;
        using edge_offset  = typename Edge::offset_type;
        using edge_id      = typename Edge::id_type;
        //  using EdgeIterator        = decltype( std::declval< Vertex >().getEdges().begin() );
        //  using ReverseEdgeIterator = decltype( std::declval< Vertex >().getReverseEdges().begin()
        //  );

        //  virtual em::view< EdgeIterator >        getEdges() const                        = 0;
        // virtual em::view< ReverseEdgeIterator > getReverseEdges() const                 = 0;
        virtual const Edge&          get_edge( edge_offset ) const           = 0;
        virtual void                 add_reverse_edge( edge_id )             = 0;
        virtual const VertexContent& operator*() const                       = 0;
        virtual VertexContent&       operator*()                             = 0;
        virtual const VertexContent* operator->() const                      = 0;
        virtual VertexContent*       operator->()                            = 0;
        virtual VertexID             get_id() const                          = 0;
        virtual edge_id              add_edge( edge_content&&, VertexID id ) = 0;
        virtual edge_content&        ref_edge_content( edge_offset )         = 0;
        virtual ~i_graph_vertex()                                            = default;
};

template < typename Vertex, typename Edge >
class i_graph  // NOLINT(cppcoreguidelines-special-member-functions)
{
public:
        using edge_id        = typename Edge::id_type;
        using edge_content   = typename Edge::content_type;
        using vertex_id      = typename Vertex::id_type;
        using vertex_content = typename Vertex::content_type;
        using price          = typename Edge::price_type;

        static_assert(
            std::is_base_of< i_graph_vertex< vertex_id, vertex_content, Edge >, Vertex >::value );
        static_assert(
            std::is_base_of< i_graph_edge< edge_content, price, vertex_id >, Edge >::value );

        virtual vertex_content& ref_content( const vertex_id& id )   = 0;
        virtual const Edge&     get_edge( const edge_id& ) const     = 0;
        virtual const Vertex&   get_vertex( const vertex_id& ) const = 0;
        //  virtual void addEdge( const VertexID source, const VertexID target, std::function<
        //  EdgeContent( VertexID, VertexID
        //  )>>);
        // virtual void modifyEdge( const edge_id &, std::function<void(EdgeContent&)> ) = 0;
        // virtual const Vertex & emplaceVertex( args ... );
        virtual ~i_graph() = default;
};

}  // namespace schpin
