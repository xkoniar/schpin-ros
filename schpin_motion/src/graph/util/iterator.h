// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Copyright 2017 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */
#include "./backtrack.h"

#include <memory>

#pragma once

namespace schpin
{

/*
 * Simply base class for Vertex/Edge iterator, uses handle mechanics to iterate over graph using
 * given algorithm
 * @tparam Iterator actually used iterator, CRTP pattern
 */
template < typename Iterator, typename Algorithm, typename ValueType >
class GraphIterator
{
public:
        typedef typename Algorithm::Handle Handle;
        typedef typename Algorithm::Vertex Vertex;
        typedef typename Algorithm::Edge   Edge;

        typedef ptrdiff_t                 difference_type;
        typedef ValueType                 value_type;
        typedef value_type&               reference;
        typedef value_type*               pointer;
        typedef std::forward_iterator_tag iterator_category;

        GraphIterator( std::shared_ptr< Algorithm >& algorithm, Handle handle )
          : algorithm_( algorithm )
          , handle_( handle )
        {
        }

        virtual reference operator*() = 0;
        virtual void      advance()   = 0;
        virtual void      retire()    = 0;

        Iterator& operator++()
        {
                advance();
                return *static_cast< Iterator* >( this );
        }

        Iterator& operator--()
        {
                retire();
                return *static_cast< Iterator* >( this );
        }

        Backtrack< Vertex, Edge > singleTrack()
        {
                return algorithm_->singleTrack( handle_ );
        };

        bool operator==( const Iterator& other ) const
        {
                return handle_ == other.handle_;
        }

        /*
         * Follows methods that are just different call of methods above
         */

        Iterator operator++( int )
        {
                Iterator copy( *this );
                this->   operator++();
                return copy;
        }

        Iterator operator--( int )
        {
                Iterator copy( *this );
                this->   operator--();
                return copy;
        }

        pointer operator->()
        {
                return &( this->operator*() );
        }

        bool operator!=( const Iterator& other ) const
        {
                return !( *this == other );
        }

protected:
        Algorithm& refAlgorithm() const
        {
                return *algorithm_;
        }
        const Handle& getHandle() const
        {
                return handle_;
        }

        void setHandle( Handle handle )
        {
                handle_ = handle;
        }

private:
        std::shared_ptr< Algorithm > algorithm_;
        Handle                       handle_;
};

template < typename Algorithm >
class VertexIterator
  : public GraphIterator< VertexIterator< Algorithm >, Algorithm, typename Algorithm::Vertex >
{
public:
        typedef typename Algorithm::Vertex value_type;
        using GraphIterator< VertexIterator< Algorithm >, Algorithm, typename Algorithm::Vertex >::
            GraphIterator;

        void advance()
        {
                this->setHandle( this->refAlgorithm().nextVertex( this->getHandle() ) );
        }

        void retire()
        {
                this->setHandle( this->refAlgorithm().prevVertex( this->getHandle() ) );
        }

        value_type& operator*()
        {
                return this->refAlgorithm().refVertex( this->getHandle() );
        }
};

template < typename Algorithm >
class EdgeIterator
  : public GraphIterator< EdgeIterator< Algorithm >, Algorithm, typename Algorithm::Edge >
{
public:
        typedef typename Algorithm::Edge value_type;
        using GraphIterator< EdgeIterator< Algorithm >, Algorithm, typename Algorithm::Edge >::
            GraphIterator;

        void advance()
        {
                this->setHandle( this->refAlgorithm().nextEdge( this->getHandle() ) );
        }

        void retire()
        {
                this->setHandle( this->refAlgorithm().prevEdge( this->getHandle() ) );
        }

        value_type& operator*()
        {
                return this->refAlgorithm().refDiscoveryEdge( this->getHandle() );
        }
};
}  // namespace schpin
