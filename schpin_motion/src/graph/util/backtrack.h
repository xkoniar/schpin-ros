// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Copyright 2017 by Jan Koniarik <jan.koniarik@gmail.com>
 *
 * This file is part of Schpin.
 *
 * Schpin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Schpin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Schpin.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @license LPGL-3.0+ <https://www.gnu.org/licenses/lgpl.html>
 */
#include <algorithm>
#include <list>
#include <ros/assert.h>
#include <vector>

#pragma once

namespace schpin
{
namespace graph
{
        /** utillity class for storing information about graph traversal
         *
         * Should contain storage for vertexes and edges in order they were traversed.
         * However that is not enforced, only checked with asserts.
         *
         * Expects that there is one start vertex, which was explored as first and no edge was used
         * for it's discovery. For each vertex that follows, there should exist Edge that was used
         * for it's discovery.
         *
         * This means that for 'n' vertexes, there are exactly 'n-1' edges.
         * For vertex on position 'i' (for 'i > 0'), there exists edge that was used for it's
         * discovery on position 'i-1'.
         *
         * This class should be used only for traversal history of single graph component,
         * asserts checks that source of inserted edge is present in the history.
         *
         * Note: vertexes are stored in std::list, bevare of execution time consequences.
         *
         */
        template < typename Vertex, typename Edge >
        class Backtrack
        {
        public:
                typedef typename std::list< Vertex >::iterator VertexIterator;
                typedef typename std::vector< Edge >::iterator EdgeIterator;

                Backtrack( Vertex start )
                  : vertexes_()
                  , edges_()
                {
                        vertexes_.push_back( start );
                }

                VertexIterator begin()
                {
                        return vertexes_.begin();
                };

                VertexIterator end()
                {
                        return vertexes_.end();
                };

                std::size_t size()
                {
                        return vertexes_.size();
                };

                EdgeIterator edgeBegin()
                {
                        return edges_.begin();
                };

                EdgeIterator edgeEnd()
                {
                        return edges_.end();
                };

                std::size_t edgeSize()
                {
                        return edges_.size();
                };

                void push( Vertex vertex, Edge discovery_edge )
                {
                        ROS_ASSERT( findSource( discovery_edge ) != vertexes_.end() );
                        vertexes_.push_back( vertex );
                        edges_.push_back( discovery_edge );
                }

                /**
                 * Extracts traversal 'track' from start of this backtrack up to 'goal_iter'
                 */
                Backtrack singleTrack( VertexIterator goal_iter )
                {
                        ROS_ASSERT( goal_iter != vertexes_.end() );

                        // for 'sane' algorithm, we traverse from the end of extracted path
                        // (goal_iter) to it's beginning (vertexes.front()), everything found is
                        // inserted into vertexes/edges vectors and than re-inserted into final
                        // backtrack in reverse order.
                        std::vector< Vertex > vertexes;
                        std::vector< Edge >   edges;

                        VertexIterator vertex_iter = goal_iter;

                        while ( vertex_iter != vertexes_.begin() ) {

                                EdgeIterator edge_iter = getEdgeIter( vertex_iter );

                                vertexes.push_back( *vertex_iter );
                                edges.push_back( *edge_iter );

                                vertex_iter = findSource( *edge_iter );
                        }

                        Backtrack res{ vertexes_.front() };

                        while ( !vertexes.empty() ) {
                                res.push( vertexes.back(), edges.back() );
                                vertexes.pop_back();
                                edges.pop_back();
                        }

                        return res;
                };

                EdgeIterator getEdgeIter( VertexIterator vertex_iter )
                {
                        std::ptrdiff_t edge_index =
                            std::distance( vertexes_.begin(), vertex_iter ) - 1;

                        ROS_ASSERT( edge_index >= 0 );

                        return edges_.begin() + edge_index;
                }

        private:
                // std::list used, because we can't afford invalidated iterators during insertion of
                // new vertexes, iterators are used as handles in some algorithms
                std::list< Vertex > vertexes_;
                std::vector< Edge > edges_;

                VertexIterator findSource( Edge edge )
                {
                        // SLOOOOOOW
                        return std::find_if(
                            vertexes_.begin(), vertexes_.end(), [&]( Vertex& vertex ) {
                                    return vertex.getID() == edge.getSourceID();
                            } );
                }
        };
}  // namespace graph
}  // namespace schpin
