// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"
#include "schpin_lib_exp/rviz.h"

#pragma once

namespace schpin
{

inline visualization_msgs::msg::Marker create_vertex_marker( unsigned id, const std::string& frame )
{
        visualization_msgs::msg::Marker res = create_marker( "vertex" + frame, id, frame );

        res.type = visualization_msgs::msg::Marker::SPHERE_LIST;

        res.scale.x = 0.002;
        res.scale.y = 0.002;
        res.scale.z = 0.002;

        res.color.r = 1.f;
        res.color.g = 0.f;
        res.color.b = 0.f;

        return res;
}
inline visualization_msgs::msg::Marker create_edge_marker( unsigned id, const std::string& frame )
{
        visualization_msgs::msg::Marker res = create_marker( "edge" + frame, id, frame );

        res.type = visualization_msgs::msg::Marker::LINE_LIST;

        res.scale.x = 0.001;

        res.color.r = 1.f;
        res.color.g = 0.f;
        res.color.b = 0.f;
        res.color.a = 0.5f;

        return res;
}

template < typename Graph, typename UnaryFunction >
visualization_msgs::msg::MarkerArray create_graph_visualization(
    const Graph&       g,
    const std::string& frame,
    UnaryFunction      vertex_to_point )
{
        visualization_msgs::msg::Marker vertex_msg = create_vertex_marker( 0, frame );
        visualization_msgs::msg::Marker edge_msg   = create_edge_marker( 0, frame );

        vertex_msg.points = map_f_to_v( g.get_vertexes(), [&]( const auto& vertex ) {
                auto point = vertex_to_point( vertex );
                for ( const auto& edge : vertex.get_edges() ) {
                        edge_msg.points.push_back( point_to_msg( point ) );
                        const auto& target_vertex = g.get_vertex( edge.get_target_id() );
                        edge_msg.points.push_back(
                            point_to_msg( vertex_to_point( target_vertex ) ) );
                }
                return point_to_msg( point );
        } );

        visualization_msgs::msg::MarkerArray res;
        res.markers.push_back( vertex_msg );
        res.markers.push_back( edge_msg );
        return res;
}
}  // namespace schpin
