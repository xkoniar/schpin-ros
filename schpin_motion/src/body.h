// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "smodel.h"

#include <schpin_lib/octree/occupancy.h>
#include <schpin_lib/octree/tree.h>
#include <utility>

#pragma once

namespace schpin
{

oc_tree< occupancy_content > create_body_occupancy_tree(
    const std::vector< triangle< 3, robot_frame > >& surface,
    em::length                                       edge_length );

class body
{
        structure_model< robot_frame > model_;
        oc_tree< occupancy_content >   collision_tree_;
        em::length                     oc_edge_length_;

        explicit body(
            structure_model< robot_frame > model,
            em::length                     oc_edge_length,
            oc_tree< occupancy_content >&& tree )
          : model_( std::move( model ) )
          , collision_tree_( std::move( tree ) )
          , oc_edge_length_( oc_edge_length )
        {
        }

public:
        explicit body( structure_model< robot_frame > model, em::length oc_edge_length )
          : model_( std::move( model ) )
          , collision_tree_( oc_edge_length, occupancy_content{ OC_MIN_OCCUPANCY } )
          , oc_edge_length_( oc_edge_length )
        {
                collision_tree_ =
                    create_body_occupancy_tree( model_.get_surface(), oc_edge_length );
        }

        body copy() const
        {
                return body{ model_, oc_edge_length_, collision_tree_.copy() };
        }

        em::length get_oc_edge_length() const
        {
                return oc_edge_length_;
        }

        std::string get_frame() const
        {
                return std::string{ "body" };
        }

        const structure_model< robot_frame >& get_model() const
        {
                return model_;
        }

        const oc_tree< occupancy_content >& get_collision_tree() const
        {
                return collision_tree_;
        }
};

using body_boot_error = error< struct BodyBootTag >;

em::either< body, body_boot_error >
load_body( const urdf::Model& model, const std::string& link_name );

}  // namespace schpin

template <>
struct nlohmann::adl_serializer< schpin::body >
{
        static void to_json( nlohmann::json& j, const schpin::body& body )
        {
                j["model"]          = body.get_model();
                j["oc_edge_length"] = body.get_oc_edge_length();
        }

        static schpin::body from_json( const nlohmann::json& j )
        {
                return schpin::body(
                    j.at( "model" ).get< schpin::structure_model< schpin::robot_frame > >(),
                    j.at( "oc_edge_length" ).get< emlabcpp::length >() );
        }
};

template <>
struct std::hash< schpin::body >
{
        std::size_t operator()( const schpin::body& body ) const
        {
                return std::hash< schpin::structure_model< schpin::robot_frame > >()(
                           body.get_model() ) ^
                       std::hash< emlabcpp::length >()( body.get_oc_edge_length() );
        }
};
