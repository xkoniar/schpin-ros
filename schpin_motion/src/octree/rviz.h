// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib_exp/rviz.h"

#include <queue>
#include <schpin_lib/octree/tree.h>

#pragma once

namespace schpin
{

inline visualization_msgs::msg::Marker create_oc_marker(
    const std::string& name,
    unsigned           id,
    em::length         scale,
    const std::string& frame,
    float              alpha )
{
        visualization_msgs::msg::Marker res = create_marker( name, id, frame );

        res.frame_locked = true;

        res.type = visualization_msgs::msg::Marker::CUBE_LIST;

        res.action = visualization_msgs::msg::Marker::ADD;

        res.scale.x = double( *scale );
        res.scale.y = double( *scale );
        res.scale.z = double( *scale );

        res.color.r = 0.5f;
        res.color.g = 0.5f;
        res.color.b = 0.5f;
        res.color.a = alpha;

        return res;
}

template < typename Content, typename UnaryFunction >
inline visualization_msgs::msg::MarkerArray create_oc_visualization(
    const std::string&        name,
    const oc_tree< Content >& tree,
    const std::string&        frame,
    float                     alpha,
    UnaryFunction             filter_f )
{
        using node_view                           = typename oc_tree< Content >::const_node_view;
        const node_view                      root = tree.root_view();
        std::queue< node_view >              que;
        visualization_msgs::msg::MarkerArray res;

        que.push( root );

        while ( !que.empty() ) {
                node_view node = que.front();
                que.pop();

                while ( res.markers.size() <= node.depth() ) {
                        res.markers.push_back( create_oc_marker(
                            name, node.depth(), node.edge_length(), frame, alpha ) );
                }
                if ( node.is_leaf() && filter_f( *node ) ) {
                        geometry_msgs::msg::Point coord = point_to_msg( node.center_position() );
                        res.markers[node.depth()].points.push_back( coord );
                }
                if ( !node.is_leaf() ) {
                        for ( node_view child : node.generate_children() ) {
                                que.push( child );
                        }
                }
        }

        return res;
}

}  // namespace schpin
