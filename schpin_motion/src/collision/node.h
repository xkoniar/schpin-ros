// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "collission_interfaces.h"
#include "schpin_lib/util.h"

#pragma once

namespace schpin
{

template < typename Query >
class simple_collision_tree_node
  : i_collision_item< typename Query::point_iterator, typename Query::separation_axis_iterator >
{
public:
        using separation_axis_iterator = typename Query::separation_axis_iterator;
        using point_iterator           = typename Query::point_iterator;

private:
        static_assert( std::is_base_of_v<
                       i_collision_item< point_iterator, separation_axis_iterator >,
                       Query > );

        const Query query_;

public:
        explicit simple_collision_tree_node( const Query& query )
          : query_( query )
        {
        }
        em::view< separation_axis_iterator > get_separation_axes() const final
        {
                return query_.get_separation_axes();
        }

        em::view< point_iterator > get_points() const final
        {
                return query_.getPoints();
        }

        em::view< separation_axis_iterator > get_intersection_axes() const final
        {
                return query_.get_intersection_axes();
        }

        bool is_leaf() const
        {
                return true;
        }

        em::view< const simple_collision_tree_node* > get_children() const
        {
                return { nullptr, nullptr };
        }
};

template < typename Query >
class fast_collision_tree_node : i_fast_collision_item< typename Query::Full, Query::N >
{
        const Query query_;

public:
        static constexpr unsigned n = Query::N;
        using full                  = typename Query::Full;
        using tag                   = typename Query::Tag;

        explicit fast_collision_tree_node( const Query& query )
          : query_( query )
        {
        }

        point< n, tag > get_min_sphere_position() const final
        {
                return query_.get_min_sphere_position();
        }

        em::radius get_min_sphere_radius() const final
        {
                return query_.get_min_sphere_radius();
        }

        bool is_leaf() const
        {
                return true;
        }

        em::view< const fast_collision_tree_node* > get_children() const
        {
                return { nullptr, nullptr };
        }

        full get_full() const final
        {
                return query_.getFull();
        }
};

}  // namespace schpin
