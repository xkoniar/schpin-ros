// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "base/line/collission.h"
#include "collision/node.h"
#include "octree/proxy_node.h"

#pragma once

namespace schpin
{

template < typename NodeContent, typename RealTag, typename NodeTag >
constexpr collision_dive_direction collision_pick_divce_direction(
    const oc_node_full_collision_query< NodeContent, RealTag, NodeTag >& /*node*/,
    const simple_collision_tree_node< line_collision_query< 3, RealTag > >& /*line*/ )
{
        return collision_dive_direction::LEFT;
}

}  // namespace schpin
