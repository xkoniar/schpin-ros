// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "base/triangle.h"
#include "schpin_lib/either.h"

#include <schpin_lib/model/structure.h>
#include <set>
#include <string>
#include <utility>
#include <vector>

#pragma once

namespace schpin
{

using vec_vector_iterator = typename std::vector< vec< 3 > >::const_iterator;
template < typename Tag >
using point_set_iterator = typename std::set< point< 3, Tag > >::const_iterator;

template < typename TagT >
class sat_structure_model_collision_query
{
public:
        using tag                      = TagT;
        using point_iterator           = point_set_iterator< tag >;
        using separation_axis_iterator = vec_vector_iterator;

private:
        std::set< point< 3, tag > > points_;
        std::vector< vec< 3 > >     normals_;

public:
        template < typename ModelTag >
        explicit sat_structure_model_collision_query(
            const structure_model< ModelTag >& model,
            pose< tag >                        offset )
        {
                for ( const triangle< 3, ModelTag >& tri : model.get_convex_hull() ) {
                        triangle< 3, tag > t = transform( tri, offset );

                        for ( const point< 3, tag >& p : t ) {
                                points_.insert( p );
                        }

                        normals_.push_back( normal_of( t ) );
                }
        }

        const auto& points() const
        {
                return points_;
        }

        const auto& separation_axes() const
        {
                return normals_;
        }

        em::view< separation_axis_iterator > intersection_axes() const
        {
                return { normals_.end(), normals_.end() };
        }
};

template < typename RealTag, typename ModelTag >
class bs_structure_model_collision_query
{
public:
        using tag      = RealTag;
        using sat_type = sat_structure_model_collision_query< RealTag >;

private:
        const structure_model< ModelTag >& model_;
        point< 3, RealTag >                center_;
        pose< RealTag >                    offset_;

public:
        bs_structure_model_collision_query(
            const structure_model< ModelTag >& model,
            pose< RealTag >                    offset )
          : model_( model )
          , center_( transform( model_.get_center(), offset_ ) )
          , offset_( offset )
        {
        }
        const point< 3, RealTag >& min_sphere_position() const
        {
                return center_;
        }
        em::radius min_sphere_radius() const
        {
                return model_.get_minball_radius();
        }
        sat_structure_model_collision_query< RealTag > sat() const
        {
                return sat_structure_model_collision_query< RealTag >{ model_, offset_ };
        }

        float descent_priority() const
        {
                return *model_.get_minball_radius();
        }

        bool is_leaf() const
        {
                return false;
        }

        auto children() const
        {
                // TODO: this rather be lazy?
                return map_f_to_v(
                    model_.get_convex_hull(), [&]( const triangle< 3, ModelTag >& t ) {
                            return bs_triangle_collision_query{ transform( t, offset_ ) };
                    } );
        }
};

}  // namespace schpin
