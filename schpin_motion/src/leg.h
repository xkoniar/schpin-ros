// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/approximation.h"
#include "leg/data.h"

#include <cmath>

#pragma once

namespace schpin
{

template < unsigned N >
class leg
{
        leg_data< N > data_;
        leg_id        id_;

public:
        explicit leg( leg_id id, leg_data< N >&& data )
          : data_( std::move( data ) )
          , id_( id )
        {
        }

        leg copy() const
        {
                return leg( id_, data_.copy() );
        }

        pose< robot_frame > get_origin_offset() const
        {
                return data_.model.get_origin_offset();
        }

        leg_id get_id() const
        {
                return id_;
        }

        const workarea_oc_tree& get_workarea() const
        {
                return data_.workarea;
        }

        const leg_model< N >& get_model() const
        {
                return data_.model;
        }

        std::string get_name() const
        {
                return data_.name;
        }

        std::string get_frame() const
        {
                return data_.model.get_root_joint().sub_link.name;
        }

        const leg_data< N >& get_leg_data() const
        {
                return data_;
        }

        std::vector< std::string > get_joint_names() const
        {
                return map_f_to_v( get_model().get_mapping(), [&]( const joint* j_ptr ) {  //
                        return '"' + j_ptr->name + '"';
                } );
        }

        template < typename T >
        point< 3, robot_frame > tip_position( const T& angles ) const
        {
                return data_.model.tip_position( angles );
        }

        template < typename ValidationFunction >
        chain< leg_edge_id, leg_vertex_id >
        get_leg_path( leg_vertex_id from, leg_vertex_id to, ValidationFunction f )
        {
                return data_.planner.get_path( from, to, f );
        }

        const auto& get_graph() const
        {
                return data_.graph;
        }

        leg_vertex_id get_neutral_vertex() const
        {
                auto neutral_angles = get_model().neutral_configuration();
                auto iter =
                    min_iterator( data_.graph.get_vertexes(), [&]( const leg_vertex< N >& v ) {
                            auto actual_angles = get_model().get_real_angles( v->angles );
                            return em::sum( em::range( N ), [&]( std::size_t i ) {
                                    return std::pow( *actual_angles[i] - *neutral_angles[i], 2 );
                            } );
                    } );

                return iter->get_id();
        }

        em::timeq approximate_movement_price(
            const std::vector< point< 3, robot_frame > >& movement_path,
            leg_vertex_id                                 hint ) const
        {
                return approximate_edge_price< em::timeq, robot_frame >(
                    em::view{ movement_path },
                    hint,
                    data_.graph,
                    [&]( leg_vertex_id vid ) {  //
                            return tip_position( data_.graph.get_vertex( vid )->angles );
                    } );
        }
};

}  // namespace schpin

template < unsigned N >
struct nlohmann::adl_serializer< schpin::leg< N > >
{
        static void to_json( nlohmann::json& j, const schpin::leg< N >& leg )
        {
                j["id"]   = leg.get_id();
                j["data"] = leg.get_leg_data();
        }

        static schpin::leg< N > from_json( const nlohmann::json& j )
        {
                return schpin::leg< N >{
                    j.at( "id" ).get< schpin::leg_id >(),
                    j.at( "data" ).get< schpin::leg_data< N > >() };
        }
};

template < unsigned N >
struct std::hash< schpin::leg< N > >
{
        std::size_t operator()( const schpin::leg< N >& leg ) const
        {
                return std::hash< schpin::leg_data< N > >()( leg.get_leg_data() ) ^
                       std::hash< schpin::leg_id >()( leg.get_id() );
        }
};
