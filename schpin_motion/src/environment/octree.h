// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/logging.h>
#include <schpin_lib/octree/node_view.h>
#include <schpin_lib/octree/occupancy.h>

#pragma once

namespace schpin
{
static constexpr uint16_t ENV_MAX_OCCUPANCY = 65535;
static constexpr uint16_t ENV_MIN_OCCUPANCY = 0;
static constexpr uint16_t ENV_PRUNNING_DIFF = 1024;  // #random

class environment_oc_tree_content
{
        uint16_t occupancy_  = ENV_MAX_OCCUPANCY / 2;
        bool     is_surface_ = false;

public:
        using token = occupancy_token;

        environment_oc_tree_content() = default;

        environment_oc_tree_content( uint16_t occupancy, bool is_surface )
          : occupancy_( occupancy )
          , is_surface_( is_surface )
        {
        }

        uint16_t occ() const
        {
                return occupancy_;
        }

        void set_surface( bool val )
        {
                is_surface_ = val;
        }

        constexpr bool is_surface() const
        {
                return is_surface_;
        }

        std::size_t memory_usage() const
        {
                return sizeof( environment_oc_tree_content );
        }

        bool prunnable( const oc_node< environment_oc_tree_content >& node ) const
        {
                bool prunnable_occupancy = ( ENV_MAX_OCCUPANCY - occupancy_ ) < ENV_PRUNNING_DIFF ||
                                           ( occupancy_ - ENV_MIN_OCCUPANCY ) < ENV_PRUNNING_DIFF;

                bool prunnable_surface = em::none_of( node, [&]( const auto& child ) {
                        return child->is_surface();
                } );

                return prunnable_surface && prunnable_occupancy;
        }

        void on_child_update( const oc_node< environment_oc_tree_content >& node )
        {
                SCHPIN_ASSERT( node.size() != 0 );
                unsigned val =
                    em::sum( node, [&]( const oc_node< environment_oc_tree_content >& child ) {
                            return child->occ();
                    } );

                occupancy_ = uint16_t( val / node.size() );
        }

        void set_prunned( const oc_node< environment_oc_tree_content >& )
        {
                if ( occupancy_ > ( ENV_MAX_OCCUPANCY + ENV_MIN_OCCUPANCY ) / 2 ) {
                        occupancy_ = ENV_MAX_OCCUPANCY;
                } else {
                        occupancy_ = ENV_MIN_OCCUPANCY;
                }
        }

        void update( const occupancy_token& token )
        {
                switch ( token ) {
                        case occupancy_token::FULL:
                                occupancy_ = ENV_MAX_OCCUPANCY;
                                return;
                }
        }
};

using env_oc_node      = oc_node< environment_oc_tree_content >;
using env_oc_node_view = oc_node_view< const env_oc_node >;

struct is_surface_predicate
{
        constexpr bool operator()( const env_oc_node& node )
        {
                return node->is_surface();
        }

        constexpr bool operator()( const oc_node_view< const env_oc_node >& node )
        {
                return this->operator()( *node );
        }
};

template < uint16_t Treshold >
struct env_occupancy_treshold_predicate
{
        constexpr bool operator()( const env_oc_node& node )
        {
                return node->occ() > Treshold;
        }
        constexpr bool operator()( const oc_node_view< const env_oc_node >& node )
        {
                return this->operator()( *node );
        }
};

struct occupancy_collides_surface_predicate
{
        template < typename LhQuery, typename RhQuery >
        bool operator()( const LhQuery& lh, const RhQuery& rh )
        {
                bool is_occupied = occupancy_treshold_predicate{}( lh.node_view() );
                bool is_surface  = is_surface_predicate{}( rh.node_view() );

                return is_occupied && is_surface;
        }
};

template < uint16_t Treshold >
struct occupancy_collides_env_predicate
{
        template < typename LhQuery, typename RhQuery >
        constexpr bool operator()( const LhQuery& lh, const RhQuery& rh )
        {
                return occupancy_treshold_predicate{}( lh.node_view() ) &&
                       env_occupancy_treshold_predicate< Treshold >{}( rh.node_view() );
        }
};

inline bool
operator==( const environment_oc_tree_content& lh, const environment_oc_tree_content& rh )
{
        return lh.occ() == rh.occ() && lh.is_surface() == rh.is_surface();
}
inline bool
operator!=( const environment_oc_tree_content& lh, const environment_oc_tree_content& rh )
{
        return !( lh == rh );
}
inline void from_json( const nlohmann::json& j, environment_oc_tree_content& c )
{
        c = environment_oc_tree_content{
            j.at( "occ" ).get< uint16_t >(), j.at( "is_surface" ).get< bool >() };
}
inline void to_json( nlohmann::json& j, const environment_oc_tree_content& c )
{
        j["occ"]        = c.occ();
        j["is_surface"] = c.is_surface();
}
}  // namespace schpin

template <>
struct std::hash< schpin::environment_oc_tree_content >
{
        std::size_t operator()( const schpin::environment_oc_tree_content& content ) const
        {
                return std::size_t( content.occ() ) ^ std::size_t( content.is_surface() );
        }
};
