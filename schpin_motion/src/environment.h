// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "environment/octree.h"
#include "schpin_lib/either.h"
#include "schpin_lib/error.h"

#include <filesystem>
#include <nlohmann/json.hpp>
#include <schpin_lib/octree/json.h>
#include <schpin_lib/octree/query.h>
#include <schpin_lib/octree/tree.h>

#pragma once

namespace schpin
{

using env_oc_tree = oc_tree< environment_oc_tree_content >;

class environment
{
        env_oc_tree tree_;

public:
        using query =
            bs_oc_node_collision_query< const oc_node< environment_oc_tree_content >, world_frame >;

        environment() = default;

        explicit environment( env_oc_tree&& tree )
          : tree_( std::move( tree ) )
        {
        }

        environment copy() const
        {
                return environment( tree_.copy() );
        }

        const env_oc_tree& get_tree() const
        {
                return tree_;
        }

        query get_query() const
        {
                return query{ tree_.root_view(), pose< world_frame >{} };
        }
};

inline std::ostream& operator<<( std::ostream& os, const environment& /*env*/ )
{
        return os;
}

using env_error = error< struct env_errorTag >;

struct env_config
{
        uint16_t                             default_occ;
        em::length                           min_edge_length;
        std::vector< std::filesystem::path > paths;
};

em::either< environment, env_error > build_environment( const env_config& config );

}  // namespace schpin

template <>
struct nlohmann::adl_serializer< schpin::environment >
{
        static void to_json( nlohmann::json& j, const schpin::environment& env )
        {
                j["tree"] = env.get_tree();
        }

        static schpin::environment from_json( const nlohmann::json& j )
        {
                return schpin::environment( j.at( "tree" ).get< schpin::env_oc_tree >() );
        }
};

// TODO: THIS IS BUG< SOLVE!
template <>
struct nlohmann::adl_serializer< const schpin::environment >
{
        static void to_json( nlohmann::json& j, const schpin::environment& env )
        {
                j["tree"] = env.get_tree();
        }

        static schpin::environment from_json( const nlohmann::json& j )
        {
                return schpin::environment( j.at( "tree" ).get< schpin::env_oc_tree >() );
        }
};

template <>
struct std::hash< schpin::environment >
{
        std::size_t operator()( const schpin::environment& env )
        {
                return std::hash< schpin::env_oc_tree >()( env.get_tree() );
        }
};
