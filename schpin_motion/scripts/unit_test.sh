#!/bin/bash

# Execute the tests
OUTPUT="$(timeout 60 ${@:2})"
TEST_RESULT=$?

# log the output if success
if [ $TEST_RESULT -eq 0 ]; then
   echo $OUTPUT > $1
fi

# print output anyway
echo "$OUTPUT"

# exit without the result of the tests, this is _necessary_ for other stuff to work correctly!
#exit $TEST_RESULT
exit 0
