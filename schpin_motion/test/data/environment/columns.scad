
a = 2;
t = 0.1;
d = 0.1;
h = 0.5;
step = 0.5;

translate([0, 0, -t/2])
    cube([a,a,t],center=true);

for(x=[-a/2:step:a/2], y=[-a/2:step:a/2])
    translate([x,y,0])
    cylinder(d=d, h=h);