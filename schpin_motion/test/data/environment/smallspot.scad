
a = 0.1; // tile size
t = 0.2;

x = [-1:1];
y = [-1:8];

w = 0.5;

translate([0, 0, -t/2])
cube([w,w,t], center=true);
translate([0, 2, -t/2])
cube([w,w,t], center=true);

for(i=x, j=y)
    translate([i*a*2,j*a*2,-t/2])
    cube([a,a,t], center=true);