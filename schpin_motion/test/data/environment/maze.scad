// 16 x 16 maze
fudge  = 0.01;
width  = 10.0;
length = 10.0;
height = 1.0;
floor_thickness = 0.01;
wall_thickness  = 0.125;
wall_height     = height - floor_thickness;

module wall_vert(x, y1, y2)
{
     translate([x, y1, floor_thickness - fudge]) cube([wall_thickness, (y2 - y1) + wall_thickness, wall_height + fudge], center=false);
}

module wall_horz(x1, x2, y)
{
     translate([x1, y, floor_thickness - fudge]) cube([(x2 - x1) + wall_thickness, wall_thickness, wall_height + fudge], center=false);
}

translate([-width / 2, -length / 2, 0]) scale([width / (wall_thickness + 2 * 16), length / (wall_thickness + 2 * 16), 1]) union()
{
     cube([wall_thickness + 2 * 16, wall_thickness + 2 * 16, floor_thickness], center=false);
     wall_horz(0, 2, 0);
     wall_vert(0, 0, 2);
     wall_vert(0, 2, 4);
     wall_vert(0, 4, 6);
     wall_vert(0, 6, 8);
     wall_vert(0, 8, 10);
     wall_vert(0, 10, 12);
     wall_horz(0, 2, 12);
     wall_vert(0, 12, 14);
     wall_vert(0, 14, 16);
     wall_vert(0, 16, 18);
     wall_horz(0, 2, 18);
     wall_vert(0, 18, 20);
     wall_vert(0, 20, 22);
     wall_vert(0, 22, 24);
     wall_vert(0, 24, 26);
     wall_vert(0, 26, 28);
     wall_vert(0, 28, 30);
     wall_vert(0, 30, 32);
     wall_horz(0, 2, 32);
     wall_horz(2, 4, 0);
     wall_vert(2, 2, 4);
     wall_horz(2, 4, 4);
     wall_vert(2, 4, 6);
     wall_horz(2, 4, 6);
     wall_vert(2, 6, 8);
     wall_vert(2, 8, 10);
     wall_horz(2, 4, 12);
     wall_vert(2, 14, 16);
     wall_horz(2, 4, 16);
     wall_vert(2, 18, 20);
     wall_vert(2, 20, 22);
     wall_horz(2, 4, 22);
     wall_horz(2, 4, 24);
     wall_vert(2, 24, 26);
     wall_vert(2, 26, 28);
     wall_horz(2, 4, 28);
     wall_vert(2, 28, 30);
     wall_horz(2, 4, 32);
     wall_horz(4, 6, 0);
     wall_vert(4, 0, 2);
     wall_horz(4, 6, 4);
     wall_horz(4, 6, 6);
     wall_horz(4, 6, 8);
     wall_vert(4, 8, 10);
     wall_vert(4, 10, 12);
     wall_vert(4, 12, 14);
     wall_horz(4, 6, 16);
     wall_vert(4, 16, 18);
     wall_vert(4, 18, 20);
     wall_horz(4, 6, 22);
     wall_horz(4, 6, 24);
     wall_horz(4, 6, 26);
     wall_horz(4, 6, 30);
     wall_vert(4, 30, 32);
     wall_horz(4, 6, 32);
     wall_horz(6, 8, 0);
     wall_horz(6, 8, 2);
     wall_vert(6, 2, 4);
     wall_horz(6, 8, 6);
     wall_vert(6, 8, 10);
     wall_vert(6, 10, 12);
     wall_horz(6, 8, 14);
     wall_vert(6, 14, 16);
     wall_vert(6, 18, 20);
     wall_vert(6, 20, 22);
     wall_horz(6, 8, 24);
     wall_vert(6, 26, 28);
     wall_vert(6, 28, 30);
     wall_horz(6, 8, 32);
     wall_horz(8, 10, 0);
     wall_vert(8, 2, 4);
     wall_horz(8, 10, 4);
     wall_horz(8, 10, 6);
     wall_horz(8, 10, 8);
     wall_vert(8, 8, 10);
     wall_vert(8, 10, 12);
     wall_vert(8, 12, 14);
     wall_vert(8, 14, 16);
     wall_vert(8, 16, 18);
     wall_horz(8, 10, 18);
     wall_vert(8, 18, 20);
     wall_horz(8, 10, 20);
     wall_vert(8, 20, 22);
     wall_vert(8, 22, 24);
     wall_horz(8, 10, 26);
     wall_vert(8, 26, 28);
     wall_vert(8, 28, 30);
     wall_horz(8, 10, 30);
     wall_horz(8, 10, 32);
     wall_horz(10, 12, 0);
     wall_vert(10, 0, 2);
     wall_horz(10, 12, 4);
     wall_horz(10, 12, 8);
     wall_horz(10, 12, 10);
     wall_vert(10, 10, 12);
     wall_vert(10, 12, 14);
     wall_horz(10, 12, 14);
     wall_vert(10, 14, 16);
     wall_horz(10, 12, 18);
     wall_horz(10, 12, 20);
     wall_vert(10, 22, 24);
     wall_vert(10, 24, 26);
     wall_horz(10, 12, 26);
     wall_horz(10, 12, 28);
     wall_vert(10, 30, 32);
     wall_horz(10, 12, 32);
     wall_horz(12, 14, 0);
     wall_horz(12, 14, 2);
     wall_vert(12, 2, 4);
     wall_vert(12, 6, 8);
     wall_horz(12, 14, 8);
     wall_vert(12, 10, 12);
     wall_horz(12, 14, 14);
     wall_horz(12, 14, 16);
     wall_vert(12, 16, 18);
     wall_horz(12, 14, 22);
     wall_vert(12, 22, 24);
     wall_vert(12, 24, 26);
     wall_horz(12, 14, 26);
     wall_vert(12, 28, 30);
     wall_horz(12, 14, 30);
     wall_horz(12, 14, 32);
     wall_horz(14, 16, 0);
     wall_vert(14, 2, 4);
     wall_vert(14, 4, 6);
     wall_horz(14, 16, 6);
     wall_horz(14, 16, 8);
     wall_vert(14, 8, 10);
     wall_vert(14, 10, 12);
     wall_horz(14, 16, 14);
     wall_vert(14, 16, 18);
     wall_horz(14, 16, 18);
     wall_horz(14, 16, 20);
     wall_vert(14, 20, 22);
     wall_horz(14, 16, 22);
     wall_horz(14, 16, 24);
     wall_vert(14, 26, 28);
     wall_horz(14, 16, 30);
     wall_horz(14, 16, 32);
     wall_horz(16, 18, 0);
     wall_horz(16, 18, 2);
     wall_vert(16, 2, 4);
     wall_horz(16, 18, 4);
     wall_vert(16, 6, 8);
     wall_horz(16, 18, 8);
     wall_vert(16, 10, 12);
     wall_vert(16, 12, 14);
     wall_horz(16, 18, 14);
     wall_vert(16, 14, 16);
     wall_horz(16, 18, 16);
     wall_horz(16, 18, 18);
     wall_horz(16, 18, 22);
     wall_horz(16, 18, 24);
     wall_vert(16, 24, 26);
     wall_vert(16, 26, 28);
     wall_horz(16, 18, 28);
     wall_vert(16, 28, 30);
     wall_horz(18, 20, 0);
     wall_horz(18, 20, 2);
     wall_horz(18, 20, 4);
     wall_vert(18, 4, 6);
     wall_horz(18, 20, 8);
     wall_vert(18, 8, 10);
     wall_vert(18, 10, 12);
     wall_horz(18, 20, 14);
     wall_horz(18, 20, 16);
     wall_horz(18, 20, 18);
     wall_vert(18, 18, 20);
     wall_vert(18, 20, 22);
     wall_horz(18, 20, 24);
     wall_horz(18, 20, 26);
     wall_vert(18, 26, 28);
     wall_horz(18, 20, 30);
     wall_horz(18, 20, 32);
     wall_horz(20, 22, 0);
     wall_horz(20, 22, 2);
     wall_horz(20, 22, 4);
     wall_vert(20, 6, 8);
     wall_horz(20, 22, 10);
     wall_vert(20, 10, 12);
     wall_vert(20, 12, 14);
     wall_horz(20, 22, 16);
     wall_horz(20, 22, 20);
     wall_vert(20, 20, 22);
     wall_vert(20, 22, 24);
     wall_horz(20, 22, 26);
     wall_horz(20, 22, 28);
     wall_vert(20, 28, 30);
     wall_horz(20, 22, 32);
     wall_horz(22, 24, 0);
     wall_vert(22, 4, 6);
     wall_vert(22, 6, 8);
     wall_vert(22, 8, 10);
     wall_horz(22, 24, 12);
     wall_vert(22, 12, 14);
     wall_horz(22, 24, 14);
     wall_vert(22, 16, 18);
     wall_vert(22, 18, 20);
     wall_horz(22, 24, 22);
     wall_vert(22, 22, 24);
     wall_vert(22, 24, 26);
     wall_horz(22, 24, 28);
     wall_vert(22, 28, 30);
     wall_vert(22, 30, 32);
     wall_horz(22, 24, 32);
     wall_vert(24, 0, 2);
     wall_vert(24, 2, 4);
     wall_horz(24, 26, 4);
     wall_vert(24, 4, 6);
     wall_vert(24, 6, 8);
     wall_vert(24, 8, 10);
     wall_horz(24, 26, 10);
     wall_vert(24, 10, 12);
     wall_vert(24, 14, 16);
     wall_vert(24, 16, 18);
     wall_vert(24, 18, 20);
     wall_horz(24, 26, 20);
     wall_horz(24, 26, 22);
     wall_vert(24, 24, 26);
     wall_vert(24, 26, 28);
     wall_horz(24, 26, 30);
     wall_horz(24, 26, 32);
     wall_horz(26, 28, 0);
     wall_vert(26, 0, 2);
     wall_horz(26, 28, 4);
     wall_vert(26, 6, 8);
     wall_horz(26, 28, 8);
     wall_vert(26, 10, 12);
     wall_horz(26, 28, 12);
     wall_horz(26, 28, 14);
     wall_vert(26, 14, 16);
     wall_horz(26, 28, 18);
     wall_vert(26, 18, 20);
     wall_horz(26, 28, 22);
     wall_vert(26, 22, 24);
     wall_vert(26, 24, 26);
     wall_horz(26, 28, 26);
     wall_horz(26, 28, 28);
     wall_vert(26, 28, 30);
     wall_horz(26, 28, 32);
     wall_horz(28, 30, 0);
     wall_vert(28, 2, 4);
     wall_horz(28, 30, 4);
     wall_vert(28, 4, 6);
     wall_horz(28, 30, 8);
     wall_vert(28, 8, 10);
     wall_horz(28, 30, 10);
     wall_horz(28, 30, 12);
     wall_vert(28, 14, 16);
     wall_vert(28, 16, 18);
     wall_vert(28, 20, 22);
     wall_horz(28, 30, 22);
     wall_horz(28, 30, 24);
     wall_vert(28, 26, 28);
     wall_horz(28, 30, 28);
     wall_horz(28, 30, 30);
     wall_horz(28, 30, 32);
     wall_horz(30, 32, 0);
     wall_vert(30, 0, 2);
     wall_vert(32, 0, 2);
     wall_vert(32, 2, 4);
     wall_horz(30, 32, 6);
     wall_vert(32, 4, 6);
     wall_vert(30, 6, 8);
     wall_vert(32, 6, 8);
     wall_vert(32, 8, 10);
     wall_vert(32, 10, 12);
     wall_vert(30, 12, 14);
     wall_vert(32, 12, 14);
     wall_vert(30, 14, 16);
     wall_vert(32, 14, 16);
     wall_vert(30, 16, 18);
     wall_vert(32, 16, 18);
     wall_vert(30, 18, 20);
     wall_vert(32, 18, 20);
     wall_vert(30, 20, 22);
     wall_vert(32, 20, 22);
     wall_horz(30, 32, 24);
     wall_vert(32, 22, 24);
     wall_vert(30, 24, 26);
     wall_vert(32, 24, 26);
     wall_vert(32, 26, 28);
     wall_horz(30, 32, 30);
     wall_vert(32, 28, 30);
     wall_horz(30, 32, 32);
     wall_vert(32, 30, 32);
}
