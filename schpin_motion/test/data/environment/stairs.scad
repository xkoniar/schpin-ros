stair_h = 0.05;
stair_depth = 0.05;
c = 9;
w = 0.5;
t = 0.2;

top_platform_z = 0.5;
top_platform_y = 1;


translate([0, 0, -t/2])
cube([w,w,t], center=true);


translate([0, top_platform_y, top_platform_z-t/2])
cube([w,w,t], center=true);


for(i = [0:c])
translate([0,w/2 + i*stair_depth + stair_depth/2,i*stair_h + stair_h/2])
cube([w,stair_depth, stair_h], center=true);