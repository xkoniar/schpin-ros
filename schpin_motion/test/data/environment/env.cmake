# Copyright 2021 Schpin
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

function(add_environment_files target)
  foreach(scad_file ${ARGN})
    set(scad_file
        "${CMAKE_CURRENT_SOURCE_DIR}/test/data/environment/${scad_file}")
    string(REGEX REPLACE "\\.scad$" ".stl" stl_file ${scad_file})
    string(REGEX REPLACE "\\.scad$" "_octree.json" json_file ${scad_file})
    add_custom_command(
      OUTPUT "${stl_file}"
      COMMAND "openscad" ARGS -o ${stl_file} ${scad_file}
      COMMENT "Generatin stl enviroment file ${stl_file} with scad"
      DEPENDS ${scad_file}
      VERBATIM)

    add_custom_command(
      OUTPUT "${json_file}"
      COMMAND "env_to_json" ARGS ${stl_file} 0.05 ${json_file}
      COMMENT "Converting stltl environment file to octree"
      DEPENDS "${stl_file}" env_to_json
      VERBATIM)

    list(APPEND json_files ${json_file})

  endforeach()
  add_custom_target(${target} DEPENDS ${json_files})
endfunction()

add_environment_files(basic_env_files column.scad columns.scad plane.scad
                      smallspot.scad stairs.scad)

add_environment_files(adv_env_files maze.scad)

add_dependencies(adv_env_files basic_env_files)
