
a = 4;
t = 0.1;
d = 0.1;
h = 0.5;

translate([0, 0, -t/2])
    cube([a,a,t],center=true);

cylinder(d=d, h=h);
