// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/approximation.h"
#include "graph/util.h"
#include "schpin_test/leg_test_util.h"

#include <gtest/gtest.h>
#include <random>

using namespace schpin;

template < typename Graph, typename VertexToPointFunction >
leg_vertex_id graph_find_closest_vertex(
    const point< 3, world_frame >& target_point,
    const Graph&                   g,
    VertexToPointFunction&&        v_to_p_f )
{
        return min_iterator(
                   g.get_vertexes(),
                   [&]( const auto& vertex ) {
                           return distance_of( v_to_p_f( vertex.get_id() ), target_point );
                   } )
            ->get_id();
}

template < typename Graph, typename VertexToPointFunction >
point< 3, world_frame > select_random_point( const Graph& g, VertexToPointFunction&& v_to_p_f )
{

        std::random_device                                         dev;
        std::mt19937                                               rng( dev() );
        std::uniform_int_distribution< std::mt19937::result_type > dist( 0, *g.size() - 1 );

        std::size_t n = 8;

        return cast_to_point< world_frame >( em::avg( em::range( n ), [&]( std::size_t ) {
                leg_vertex_id id{ dist( rng ) };
                return cast_to_vec( v_to_p_f( id ) );
        } ) );
}

TEST( Graph, findClosestVertex )
{
        leg_graph< 3 > g = gen_leg_test_graph< 3 >( 4 );

        auto vertex_to_point = [&]( leg_vertex_id vertex_id ) {
                const auto& vertex = g.get_vertex( vertex_id );
                return point< 3, world_frame >{
                    vertex->angles[0], vertex->angles[1], vertex->angles[2] };
        };
        std::random_device dev;
        std::mt19937       rng( dev() );

        for ( std::size_t i : em::range( 1000u ) ) {
                em::ignore( i );
                std::uniform_int_distribution< std::mt19937::result_type > dist( 0, *g.size() - 1 );

                leg_vertex_id           start_vertex{ dist( rng ) };
                point< 3, world_frame > target = select_random_point( g, vertex_to_point );

                auto res = graph_find_closest_vertex( target, start_vertex, g, vertex_to_point );
                auto res_dist = distance_of( vertex_to_point( res ), target );

                auto naive_res  = graph_find_closest_vertex( target, g, vertex_to_point );
                auto naive_dist = distance_of( vertex_to_point( res ), target );

                EXPECT_FLOAT_EQ( *res_dist, *naive_dist )  //
                    << "target: " << target << "\n"
                    << "found: " << vertex_to_point( res ) << " => " << res_dist << "\n"
                    << "naive: " << vertex_to_point( naive_res ) << " =>  " << naive_dist
                    << std::endl;
        }
}

TEST( Graph, approximation )
{
        leg_graph< 2 > g = gen_leg_test_graph< 2 >( 4 );  // 4x4 square grid

        std::vector< point< 3, world_frame > > base_path;
        base_path.emplace_back( 0.f, 0.f, 0.f );
        base_path.emplace_back( 3.f, 3.f, 0.f );
        auto vertex_to_point = [&]( auto vertex_id ) {
                const auto& vertex = g.get_vertex( vertex_id );
                return point< 3, world_frame >{ vertex->angles[0], vertex->angles[1], 0.f };
        };
        for ( const auto& vertex : g.get_vertexes() ) {
                for ( const auto& edge : vertex.get_edges() ) {
                        g.ref_edge_content( edge.get_id() ).price = leg_price{ *distance_of(
                            vertex_to_point( edge.get_source_id() ),
                            vertex_to_point( edge.get_target_id() ) ) };
                }
        }

        auto mpath = lineary_interpolate_path( base_path, em::distance{ 1.f } );

        EXPECT_GT( mpath.size(), std::size_t{ 4 } );

        leg_price p = approximate_edge_price< leg_price, world_frame >(
            mpath, leg_vertex_id{ *g.size() / 2 }, g, vertex_to_point );

        ASSERT_EQ( p, leg_price{ sqrt( 2.f ) * 3.f } );
}
