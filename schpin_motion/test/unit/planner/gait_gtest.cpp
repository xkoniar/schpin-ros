// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "planner/gait.h"
#include "robot/print.h"
#include "schpin_test/test_environment.h"

#include <gtest/gtest.h>

using namespace schpin;

extern test_data_archive ARCHIVE;

class gait_planner_test : public ::testing::Test
{
public:
        leg_container< sidepath > SIDEPATHS{ leg_size_token{} };
        gait_body_path            BODY_PATH;
        std::array< int, 4 >      OFFSETS{ { 1, -1, -2, -4 } };
        gait_state                CENTER_GAIT_STATE{ 0, gait_leg_container{ leg_size_token{} } };
        em::distance              D_STEP{ 0.005f };
        planner_context           CONTEXT{ ARCHIVE.get_robot( "koke" ).copy() };
        gait_planner              PLANNER{ leg_size_token{} };

        void SetUp()
        {
                float        z_offset = 0.2f;
                const robot& rob      = ARCHIVE.get_robot( "koke" );
                print_robot_model( rob );
                SIDEPATHS = map_f_to_l( rob.get_legs(), [&]( const auto& leg ) {
                        auto config  = leg.get_model().neutral_configuration();
                        auto tip_pos = leg.tip_position( config );
                        SCHPIN_INFO_LOG(
                            "gait",
                            "tip pos for leg: " << leg.get_name() << " - " << tip_pos << " - "
                                                << em::view( config ) << "\n"
                                                << "origin offset: " << leg.get_origin_offset() );
                        sidepath s;
                        s.emplace_back( -0.5f + tip_pos[0], tip_pos[1], tip_pos[2] + z_offset );
                        s.emplace_back( tip_pos[0], tip_pos[1], tip_pos[2] + z_offset );
                        s.emplace_back( 0.5f + tip_pos[0], tip_pos[1], tip_pos[2] + z_offset );
                        return lineary_interpolate_path( s, D_STEP );
                } );

                gait_body_path base;
                base.emplace_back(
                    pose{ point< 3, world_frame >{ -0.5f, 0.f, z_offset } },
                    body_vertex_id{ 0 },
                    rob.get_legs() );
                base.emplace_back(
                    pose{ point< 3, world_frame >{ 0.f, 0.f, z_offset } },
                    body_vertex_id{ 1 },
                    rob.get_legs() );
                base.emplace_back(
                    pose{ point< 3, world_frame >{ 0.5f, 0.f, z_offset } },
                    body_vertex_id{ 2 },
                    rob.get_legs() );
                BODY_PATH = lineary_interpolate_path( base, D_STEP, em::angle{ 0.1f } );

                CENTER_GAIT_STATE.body_pose_i = BODY_PATH.size() / 2;
                CENTER_GAIT_STATE.legs = map_f_to_l( rob.get_legs(), [&]( const leg< 3 >& l ) {
                        leg_view view{ l, BODY_PATH[CENTER_GAIT_STATE.body_pose_i].pos };

                        gait_leg_state st;
                        st.is_moving  = false;
                        st.sidepath_i = SIDEPATHS[l.get_id()].size() / 2;
                        st.leg_config = view.find_closest_vertex(
                            SIDEPATHS[l.get_id()][st.sidepath_i], l.get_neutral_vertex() );
                        return st;
                } );

                SCHPIN_INFO_LOG(
                    "gait_tests", "bdy: " << BODY_PATH[CENTER_GAIT_STATE.body_pose_i] );
                for ( leg_id id : get_leg_ids( rob.get_legs() ) ) {
                        SCHPIN_INFO_LOG(
                            "gait_tests",
                            "leg: " << rob.get_legs()[id].get_name() << " - "
                                    << SIDEPATHS[id][CENTER_GAIT_STATE.legs[id].sidepath_i] );
                }
                CONTEXT = planner_context{
                    this->BODY_PATH.front().pos,
                    std::make_optional( this->BODY_PATH.at( 5 ).pos ),
                    ARCHIVE.get_env( "plane" ).copy(),
                    ARCHIVE.get_robot( "koke" ).copy() };
        }
};

/*
TEST_F( gait_planner_test, rawStates )
{
        const robot& rob = ARCHIVE.get_robot( "koke" );

        std::vector< gait_state > raw_states = PLANNER.candidate_raw_states(
            this->OFFSETS, this->CENTER_GAIT_STATE, this->BODY_PATH, this->SIDEPATHS, rob );

        EXPECT_EQ( raw_states.size(),
                   1 + this->CENTER_GAIT_STATE.legs.size() * this->OFFSETS.size() )
            << em::view{ raw_states };
}
TEST_F( gait_planner_test, predecessors )
{
        robot        rob               = ARCHIVE.get_robot( "koke" ).copy();
        environment  env               = ARCHIVE.get_env( "plane" ).copy();
        em::distance price_approx_step = this->D_STEP / 2.f;

        std::vector< gait_state > raw_states = PLANNER.candidate_raw_states(
            this->OFFSETS, this->CENTER_GAIT_STATE, this->BODY_PATH, this->SIDEPATHS, rob );
        std::vector< gait_predecessor > preds = gait_predecessors( raw_states,
                                                                   this->CENTER_GAIT_STATE,
                                                                   this->BODY_PATH,
                                                                   this->SIDEPATHS,
                                                                   rob,
                                                                   price_approx_step );

        EXPECT_EQ( preds.size(), 1 + this->CENTER_GAIT_STATE.legs.size() * this->OFFSETS.size() );

        em::timeq min_time = em::min_elem( preds, [&]( const gait_predecessor& p ) {  //
                return p.t;
        } );

        EXPECT_NE( min_time, em::timeq{ 0.f } );
}

TEST_F( gait_planner_test, planner )
{

        robot_state robot_state{
            body_state{ this->CONTEXT.current },
            map_f_to_l( this->CONTEXT.rob.get_legs(), [&]( const leg< 3 >& l ) {
                    return leg_state< 3 >{ l.get_model().neutral_configuration() };
            } ) };

        auto              path_view = em::view_n( this->BODY_PATH.begin(), 5 );
        reset_chain_event reset_event{ robot_state_view{ this->CONTEXT.rob, robot_state },
                                       gait_body_path{ path_view.begin(), path_view.end() } };
        PLANNER.event( reset_event, this->CONTEXT )
            .optionally( [&]( const gait_planner_error& e ) {  //
                    FAIL() << e;
            } );
        find_path_event e;
        e.step_limit = std::numeric_limits< std::size_t >::max();
        PLANNER.event( e, this->CONTEXT );
}
*/
