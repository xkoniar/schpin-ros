// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "planner/body.h"
#include "schpin_test/test_environment.h"

#include <gtest/gtest.h>
#include <tuple>

using namespace schpin;

extern test_data_archive ARCHIVE;

class BodyPlannerTest : public ::testing::Test
{
public:
        float               h = 0.07f;
        pose< world_frame > NEUTRAL_POSE{ point< 3, world_frame >{ 0, 0, h }, neutral_quat };
        pose< world_frame > SHIFTED_POSE{ point< 3, world_frame >{ 1, 0, h }, neutral_quat };
        pose< world_frame > CORNER_POSE{ point< 3, world_frame >{ 1, -1, h }, neutral_quat };
        body_planner_functions< false > FUN;

        void check_path(
            bool                       should_be_valid,
            const pose< world_frame >& from,
            const pose< world_frame >& to )
        {
                const environment& env   = ARCHIVE.get_env( "column" );
                const robot&       robot = ARCHIVE.get_robot( "koke" );

                valid_path_res is_valid = FUN.is_valid_path( from, to, robot, env );
                EXPECT_EQ( should_be_valid, bool( is_valid ) ) << "from          : " << from << "\n"
                                                               << "to            : " << to << "\n"
                                                               << is_valid << std::endl;
        }
};

TEST_F( BodyPlannerTest, body_collides_with_environment )
{
        const environment& env   = ARCHIVE.get_env( "column" );
        const robot&       robot = ARCHIVE.get_robot( "koke" );

        EXPECT_TRUE( this->FUN.body_collides_with_environment(
            robot.get_body(), this->NEUTRAL_POSE, env ) );  // fails on collision in both modes
        EXPECT_FALSE(
            this->FUN.body_collides_with_environment( robot.get_body(), this->SHIFTED_POSE, env ) );
        EXPECT_FALSE(
            this->FUN.body_collides_with_environment( robot.get_body(), this->CORNER_POSE, env ) );
}

TEST_F( BodyPlannerTest, all_legs_touches_with_environment )
{
        const environment& env   = ARCHIVE.get_env( "column" );
        const robot&       robot = ARCHIVE.get_robot( "koke" );

        EXPECT_TRUE( this->FUN.all_legs_touches_with_environment(
            robot.get_legs(), this->CORNER_POSE, env ) );
        EXPECT_TRUE( this->FUN.all_legs_touches_with_environment(
            robot.get_legs(), this->NEUTRAL_POSE, env ) );
        EXPECT_TRUE( this->FUN.all_legs_touches_with_environment(
            robot.get_legs(), this->SHIFTED_POSE, env ) );
}

TEST_F( BodyPlannerTest, isValidPath )
{
        std::vector< std::tuple< bool, pose< world_frame >, pose< world_frame > > > tests{
            { false, this->NEUTRAL_POSE, this->SHIFTED_POSE },
            { false,
              this->NEUTRAL_POSE,
              pose{ point< 3, world_frame >{ 1, 1, this->h }, neutral_quat } },
            { false,
              this->NEUTRAL_POSE,
              pose{ point< 3, world_frame >{ -5, 0, this->h }, neutral_quat } },
            { true, this->SHIFTED_POSE, this->CORNER_POSE },
            { true,
              this->CORNER_POSE,
              pose{ point< 3, world_frame >{ 0.3, 0.3, this->h }, neutral_quat } } };

        for ( auto [should_be_valid, from, to] : tests ) {
                this->check_path( should_be_valid, from, to );
                this->check_path( should_be_valid, to, from );
        }
}

TEST_F( BodyPlannerTest, simple )
{
        const environment& env   = ARCHIVE.get_env( "column" );
        const robot&       robot = ARCHIVE.get_robot( "koke" );

        planner_context context{
            this->SHIFTED_POSE, std::make_optional( this->CORNER_POSE ), env.copy(), robot.copy() };

        body_planner< false > planner{ context };

        body_planner_lambda_visitor visitor{ []( const auto& ) {
                return opt_error< body_path_failure_event >{};
        } };

        std::size_t max_steps = 50;
        std::size_t step      = *em::find_if( em::range( max_steps ), [&]( std::size_t ) -> bool {
                planner.event( make_vertexes_event{ 1 }, context, visitor ).optionally( []( auto ) {
                } );
                planner.event( find_body_path_event{}, context, visitor ).optionally( []( auto ) {
                } );

                return planner.has_plan();
        } );

        EXPECT_NE( max_steps, step );
}
