// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg.h"
#include "leg/graph/gen.h"
#include "leg/graph/space.h"
#include "schpin_test/leg_test_util.h"

#include <gtest/gtest.h>

using namespace schpin;

TEST( Collision, SimplexTrianglesGraph )
{
        leg_graph< 2 > graph = gen_leg_test_graph< 2 >( 3 );

        simplex< leg_vertex_id, 2 > s1{
            leg_vertex_id{ 1 }, leg_vertex_id{ 3 }, leg_vertex_id{ 4 } };
        simplex< leg_vertex_id, 2 > s2{
            leg_vertex_id{ 1 }, leg_vertex_id{ 2 }, leg_vertex_id{ 4 } };

        simplex_graph_collision_query< 2 > q1( graph, s1 );
        simplex_graph_collision_query< 2 > q2( graph, s2 );

        EXPECT_FALSE( sat_collides( q1, q2 ) )
            << draw_simplexes( graph, std::vector< simplex< leg_vertex_id, 2 > >{ s1, s2 } );
}
