// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_test/main.h"

#include "rclcpp/rclcpp.hpp"
#include "schpin_test/test_environment.h"

#include <gtest/gtest.h>
#include <schpin_lib/util.h>

namespace schpin
{

std::map< std::string, std::string > ARGS;

}  // namespace schpin

using namespace schpin;

test_data_archive ARCHIVE;

em::either< std::map< std::string, std::string >, test_error >
parse_args( int argc_in, char** argv_in )
{
        std::map< std::string, std::string > res;

        // skip the binary name
        argc_in -= 1;
        argv_in = std::next( argv_in );

        if ( argc_in % 2 != 0 ) {
                return {
                    E( test_error ) << "Failed to parse test arguments, arg count: " << argc_in };
        }

        for ( std::size_t i : em::range( std::size_t( argc_in / 2 ) ) ) {
                std::string key = argv_in[2 * i];
                std::string val = argv_in[2 * i + 1];

                res[key] = val;
        }

        if ( res.size() != std::size_t( argc_in / 2 ) ) {
                return { E( test_error ) << "Duplicate keys in arguments" };
        }

        bool keys_ok = em::all_of( res, [&]( const auto& map_pair ) {
                return map_pair.first.find( "--" ) == 0;
        } );
        if ( !keys_ok ) {
                return { E( test_error ) << "Failed to parse keys" };
        }

        return { res };
}

int main( int argc_in, char** argv_in )
{
        testing::InitGoogleTest( &argc_in, argv_in );

        auto arg_either = parse_args( argc_in, argv_in );

        arg_either.match(
            [&]( std::map< std::string, std::string > args ) {
                    ARGS = std::move( args );
            },
            [&]( const test_error& e ) {
                    std::cerr << e;
            } );

        if ( !arg_either.is_left() ) {
                return -1;
        }

        ARCHIVE.preload_environment( "plane" );
        ARCHIVE.preload_environment( "column" );
        ARCHIVE.preload_robot( "koke" );

        return RUN_ALL_TESTS();
}
