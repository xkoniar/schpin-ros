// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg/sidepath.h"
#include "schpin_test/test_environment.h"

#include <gtest/gtest.h>

using namespace schpin;

extern test_data_archive ARCHIVE;

struct is_surface_predicate_counter
{
        std::size_t checked_counter = 0;
        std::size_t is_surface      = 0;

        constexpr bool operator()( const oc_node< environment_oc_tree_content >& node )
        {
                checked_counter += 1;
                bool res = node->is_surface();
                if ( res ) {
                        is_surface += 1;
                }
                return res;
        }

        constexpr bool
        operator()( const oc_node_view< const oc_node< environment_oc_tree_content > >& node )
        {
                return this->operator()( *node );
        }
};

TEST( Sidepath, eligible_surface_collection )
{
        const robot&       robot = ARCHIVE.get_robot( "koke" ).copy();
        const environment& env   = ARCHIVE.get_env( "plane" ).copy();
        const leg< 3 >&    leg   = robot.get_legs()[leg_id{ 0 }];

        is_surface_predicate_counter pred;
        auto opt_env_box = env.get_tree().root_view().recursive_matched_area( pred );
        auto opt_workarea_box =
            leg.get_workarea().root_view().recursive_matched_area( occupancy_treshold_predicate{} );

        EXPECT_TRUE( opt_env_box )
            << "counters: " << pred.checked_counter << "/" << pred.is_surface << "\n"
            << "root: " << env.get_tree().root_view();
        EXPECT_TRUE( opt_workarea_box );
}

TEST( Sidepath, createGenerator )
{
        const robot&       robot = ARCHIVE.get_robot( "koke" ).copy();
        const environment& env   = ARCHIVE.get_env( "plane" ).copy();
        const leg< 3 >&    leg   = robot.get_legs()[leg_id{ 0 }];

        std::vector< pose< world_frame > > base_path;
        base_path.emplace_back( pose{ point< 3, world_frame >{ -0.1f, 0.f, 0.2f } } );
        base_path.emplace_back( pose{ point< 3, world_frame >{ 0.1f, 0.f, 0.2f } } );

        const std::vector< pose< world_frame > > body_poses =
            lineary_interpolate_path( base_path, em::distance{ 0.05f }, em::angle{ 0.1f } );

        const point< 3, world_frame >& tip_position =
            transform( leg.tip_position( leg.get_model().neutral_configuration() ), body_poses[0] );

        create_sidepath_generator( body_poses, leg, tip_position, env )
            .match(
                [&]( leg_sidepath_generator gen ) {                               //
                        EXPECT_GT( *gen.get_graph().size(), body_poses.size() );  // rough estimate
                        auto p = gen.find_sidepath( leg, env );
                        EXPECT_GT( p.size(), body_poses.size() / 2 );
                        EXPECT_LT( p.size(), body_poses.size() * 2 );
                },
                [&]( const sidepath_failure_report& rep ) {
                        FAIL() << rep;
                } );
}
