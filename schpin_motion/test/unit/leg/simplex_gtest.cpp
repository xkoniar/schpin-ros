// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg.h"
#include "leg/graph/gen.h"
#include "leg/graph/space.h"
#include "schpin_test/leg_test_util.h"

#include <gtest/gtest.h>

using namespace schpin;

// NOTE: test disabled because functionality is not needed

TEST( Simplex, simple )
{
        leg_graph< 2 > graph = gen_leg_test_graph< 2 >( 2 );

        std::vector< simplex< leg_vertex_id, 2 > > storage =
            autogenerate_simplexes< leg_graph< 2 >, 2 >( graph );

        //        ASSERT_EQ( storage.size(), std::size_t{2} ) << draw_simplexes( graph, storage ) <<
        //        std::endl << view(storage);
        //      ASSERT_NE( storage[0], storage[1] );
}

TEST( Simplex, twoByTwo )
{
        leg_graph< 2 > graph = gen_leg_test_graph< 2 >( 3 );

        std::vector< simplex< leg_vertex_id, 2 > > storage =
            autogenerate_simplexes< leg_graph< 2 >, 2 >( graph );

        //    ASSERT_EQ( storage.size(), std::size_t{8} ) << draw_simplexes( graph, storage ) <<
        //    std::endl << view(storage);
}

TEST( Simplex, lShape )
{
        leg_graph< 2 > graph = gen_leg_test_graph< 2 >( 3 );

        auto node_iter =
            em::find_if( graph.get_vertexes(), []( const leg_vertex< 2 >& node ) -> bool {
                    return node->angles[0] == 0 && node->angles[1] == 0;
            } );

        graph.purge_all_edges( node_iter->get_id() );

        std::vector< simplex< leg_vertex_id, 2 > > storage =
            autogenerate_simplexes< leg_graph< 2 >, 2 >( graph );

        //  ASSERT_EQ( storage.size(), std::size_t{7} ) << draw_simplexes( graph, storage );
}

TEST( Simplex, oShape )
{
        leg_graph< 2 > graph = gen_leg_test_graph< 2 >( 5 );

        auto node_iter = em::find_if( graph.get_vertexes(), []( const leg_vertex< 2 >& node ) {
                return node->angles[0] == 2 && node->angles[1] == 2;
        } );

        graph.purge_all_edges( node_iter->get_id() );

        std::vector< simplex< leg_vertex_id, 2 > > storage =
            autogenerate_simplexes< leg_graph< 2 >, 2 >( graph );

        // ASSERT_EQ( storage.size(), std::size_t{28} ) << draw_simplexes( graph, storage );
}
