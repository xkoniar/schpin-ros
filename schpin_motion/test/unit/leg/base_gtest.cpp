// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg/base.h"

#include <gtest/gtest.h>

using namespace schpin;

TEST( LegBaseTest, discreteAnglesJson )
{
        using angles = discrete_angles< 2 >;

        angles data( { 1, 2 } );

        ASSERT_EQ( nlohmann::json( data ).get< angles >(), data );
}
