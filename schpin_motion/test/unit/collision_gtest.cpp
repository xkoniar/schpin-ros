// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "base/triangle.h"
#include "schpin_test/test_environment.h"

#include <gtest/gtest.h>
#include <schpin_lib/octree/occupancy.h>
#include <schpin_lib/octree/tree.h>

using namespace schpin;

static const test_data_archive ARCHIVE{ { "column", "columns" } };

TEST( Collision, SimplexTriangles )
{
        triangle< 3, oc_frame > t1{
            point< 3, oc_frame >( 0, 1, 0 ),
            point< 3, oc_frame >( 0, 0, 0 ),
            point< 3, oc_frame >( 1, 0, 0 ) };
        triangle< 3, oc_frame > t2{
            point< 3, oc_frame >( 0, 0, 0 ),
            point< 3, oc_frame >( 1, 1, 0 ),
            point< 3, oc_frame >( 1, 0, 0 ) };
        triangle< 3, oc_frame > t3{
            point< 3, oc_frame >( 0, 1, 0 ),
            point< 3, oc_frame >( 1, 1, 0 ),
            point< 3, oc_frame >( 1, 0, 0 ) };
        triangle< 3, oc_frame > t4{
            point< 3, oc_frame >( 10, 1, 0 ),
            point< 3, oc_frame >( 11, 1, 0 ),
            point< 3, oc_frame >( 11, 0, 0 ) };

        sat_triangle_collision_query< 3, oc_frame > q1( t1 );
        sat_triangle_collision_query< 3, oc_frame > q2( t2 );
        sat_triangle_collision_query< 3, oc_frame > q3( t3 );
        sat_triangle_collision_query< 3, oc_frame > q4( t4 );

        EXPECT_TRUE( sat_collides( q1, q2 ) );
        EXPECT_TRUE( sat_collides( q2, q3 ) );
        EXPECT_FALSE( sat_collides( q1, q3 ) );
        EXPECT_FALSE( sat_collides( q3, q4 ) );
}

TEST( Collision, SimplexTriangles2 )
{
        triangle< 3, oc_frame > t1{
            point< 3, oc_frame >( 1, 0, 0 ),
            point< 3, oc_frame >( 0, 1, 0 ),
            point< 3, oc_frame >( 1, 1, 0 ) };
        triangle< 3, oc_frame > t2{
            point< 3, oc_frame >( 1, 0, 0 ),
            point< 3, oc_frame >( 2, 0, 0 ),
            point< 3, oc_frame >( 1, 1, 0 ) };

        sat_triangle_collision_query< 3, oc_frame > q1( t1 );
        sat_triangle_collision_query< 3, oc_frame > q2( t2 );

        EXPECT_FALSE( sat_collides( q1, q2 ) );
}

using tree_type = oc_tree< environment_oc_tree_content >;

TEST( Collision, TriangleOctreeNode )
{
        triangle< 3, oc_frame > t1{
            point< 3, oc_frame >( -2, -2, -0.099999 ),
            point< 3, oc_frame >( -2, 2, 0 ),
            point< 3, oc_frame >( -2, 2, -0.099999 ) };

        tree_type tree{
            oc_node< environment_oc_tree_content >{},
            oc_node_context{ 0, 0, em::length{ 0.05f }, point< 3, oc_frame >{ -0.7f, 3.1f, 0 } },
            environment_oc_tree_content{} };

        sat_triangle_collision_query< 3, oc_frame > q1{ t1 };
        sat_oc_node_collision_query                 q2{ tree.root_view() };

        EXPECT_FALSE( sat_collides( q1, q2 ) );
}

// TODO: ugly lambdas! fix this
// TODO: maybe give access in uqery to nodes? or content?
auto NODES_COLLIDES_F = []( const auto& lh_node_query, const auto& rh_node_query ) {
        return ( *lh_node_query.node_view() )->occ() > ENV_MIN_OCCUPANCY / 2 &&
               ( *rh_node_query.node_view() )->occ() > ENV_MAX_OCCUPANCY / 2;
};
const environment_oc_tree_content tree_content{ ENV_MAX_OCCUPANCY, false };

TEST( TreeCollision, selfCollision )
{
        tree_type tree{ em::length( 1 ), tree_content };

        bool has_collided = bs_trees_collides(
            bs_oc_node_collision_query( tree.root_view() ),
            bs_oc_node_collision_query( tree.root_view() ),
            NODES_COLLIDES_F );
        EXPECT_TRUE( has_collided ) << "same tree at same place should collide";

        pose offset{ point< 3, oc_frame >( 10, 0, 0 ), tag_cast< oc_frame >( neutral_quat ) };

        has_collided = bs_trees_collides(
            bs_oc_node_collision_query( tree.root_view(), offset ),
            bs_oc_node_collision_query( tree.root_view() ),
            NODES_COLLIDES_F );
        EXPECT_FALSE( has_collided ) << "same tree moved by size bigger than itself should not "
                                        "collide";
}

std::vector< point< 3, oc_frame > > TREE_POINTS{
    point< 3, oc_frame >{ 0, 0, 0 },
    point< 3, oc_frame >{ 1, 0, 0 },
    point< 3, oc_frame >{ 0, 1, 0 },
    point< 3, oc_frame >{ 0, 0, 1 } };

TEST( TreeCollision, selfCollisionComplex )
{
        tree_type tree{ em::length{ 1.f }, tree_content };

        for ( const point< 3, oc_frame >& p : TREE_POINTS ) {
                tree.expand( p );
        }

        bool has_collided = bs_trees_collides(
            bs_oc_node_collision_query( tree.root_view() ),
            bs_oc_node_collision_query( tree.root_view() ),
            NODES_COLLIDES_F );
        EXPECT_TRUE( has_collided ) << "same tree at same place should collide";

        pose offset_big{ point< 3, oc_frame >( 10, 0, 0 ), tag_cast< oc_frame >( neutral_quat ) };

        has_collided = bs_trees_collides(
            bs_oc_node_collision_query( tree.root_view(), offset_big ),
            bs_oc_node_collision_query( tree.root_view() ),
            NODES_COLLIDES_F );
        EXPECT_FALSE( has_collided ) << "tree moved by size bigger than his should not collide";

        pose offset_small{
            point< 3, oc_frame >( 2.f - 0.001f, 0, 0 ), tag_cast< oc_frame >( neutral_quat ) };

        has_collided = bs_trees_collides(
            bs_oc_node_collision_query( tree.root_view(), offset_small ),
            bs_oc_node_collision_query( tree.root_view() ),
            NODES_COLLIDES_F );
        EXPECT_TRUE( has_collided ) << "tree moved by size smaller than his should collide";
}

TEST( Environment, simpleCollision )
{
        tree_type tree{ em::length( 1 ), tree_content };

        const environment& env = ARCHIVE.get_env( "column" );

        bool has_collided = bs_trees_collides(
            bs_oc_node_collision_query( tree.root_view() ),
            bs_oc_node_collision_query( env.get_tree().root_view() ),
            NODES_COLLIDES_F );
        EXPECT_TRUE( has_collided );
}

TEST( Environment, complexCollision )
{
        const environment& env1 = ARCHIVE.get_env( "column" );
        const environment& env2 = ARCHIVE.get_env( "columns" );

        bool has_collided = bs_trees_collides(
            bs_oc_node_collision_query( env2.get_tree().root_view() ),
            bs_oc_node_collision_query( env1.get_tree().root_view() ),
            NODES_COLLIDES_F );
        EXPECT_TRUE( has_collided );
}
