// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "base/triangle.h"

#include <gtest/gtest.h>

using namespace schpin;

void test_triangle_sphere_center( const triangle< 3, world_frame >& triangle )
{

        point< 3, world_frame > center = get_triangle_sphere_center( triangle );

        em::distance scale = distance_of( center, triangle[0] );

        ASSERT_NEAR(
            *distance_of( center, triangle[0] ),
            *distance_of( center, triangle[1] ),
            double( *scale ) * 1e-6 );
        ASSERT_NEAR(
            *distance_of( center, triangle[0] ),
            *distance_of( center, triangle[2] ),
            double( *scale ) * 1e-6 );
}

TEST( TriangleSphereCenter, base )
{
        triangle< 3, world_frame > triangle{
            point< 3, world_frame >( 0, 0, 0 ),
            point< 3, world_frame >( 1, 0, 0 ),
            point< 3, world_frame >( 0, 1, 0 ) };
        test_triangle_sphere_center( triangle );
}

TEST( TriangleSphereCenter, base2 )
{
        triangle< 3, world_frame > triangle{
            point< 3, world_frame >( -500, 200, -300 ),
            point< 3, world_frame >( 6, 66, 666 ),
            point< 3, world_frame >( 10, 10, 10 ) };
        test_triangle_sphere_center( triangle );
}

TEST( Triangle, collisionQuery )
{
        triangle< 3, world_frame > t1{
            point< 3, world_frame >( 0, 1, 0 ),
            point< 3, world_frame >( 0, 0, 0 ),
            point< 3, world_frame >( 1, 0, 0 ) };
        sat_triangle_collision_query< 3, world_frame > q1( t1 );

        vec< 3 > normal = q1.separation_axes().front();

        ASSERT_EQ( dot( normal, cast_to_vec( t1[0] ) ), 0.f ) << normal;
        for ( const vec< 3 >& v : q1.intersection_axes() ) {
                ASSERT_EQ( dot( v, normal ), 0.f ) << v;
                ASSERT_TRUE( em::any_of( t1, [&]( const point< 3, world_frame >& t_point ) {  //
                        return dot( v, cast_to_vec( t_point ) ) == 0;
                } ) );
        }
}
