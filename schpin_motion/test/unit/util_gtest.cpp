// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "util.h"

#include <gtest/gtest.h>

using namespace schpin;

TEST( Util, assemble_left_collect_right )
{
        em::either< std::tuple< int, unsigned >, em::static_vector< float, 2 > > either_val =
            assemble_left_collect_right(
                em::either< int, float >( 0.1f ), em::either< unsigned, float >( 0.1f ) );

        ASSERT_FALSE( either_val.is_left() );

        either_val = assemble_left_collect_right(
            em::either< int, float >( 1 ), em::either< unsigned, float >( 0.1f ) );

        ASSERT_FALSE( either_val.is_left() );

        either_val = assemble_left_collect_right(
            em::either< int, float >( 1 ), em::either< unsigned, float >( unsigned( 1 ) ) );

        ASSERT_TRUE( either_val.is_left() );
}

TEST( Util, partition_eithers )
{
        auto partitioned =
            partition_eithers< int, float >( { { 1 }, { 2 }, { 15 }, { 0.1f }, { -0.1f } } );

        ASSERT_EQ( partitioned.left.size(), std::size_t( 3 ) );
        ASSERT_EQ( partitioned.right.size(), std::size_t( 2 ) );
}
