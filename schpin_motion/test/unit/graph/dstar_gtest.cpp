// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "graph/astar.h"
#include "graph/dstar.h"
#include "graph/generics.h"
#include "graph/generics/edge.h"
#include "graph/generics/vertex.h"
#include "schpin_lib_exp/algorithm.h"
#include "schpin_test/test_environment.h"
#include "util/heaps.h"

#include <filesystem>
#include <fstream>
#include <gtest/gtest.h>
#include <random>
#include <schpin_lib/identificator.h>

using namespace schpin;

using test_vertex_id   = identificator< struct test_vertex_id_tag, 32 >;
using test_edge_offset = identificator< struct test_edge_offset_tag, 32 >;
using test_edge_id     = generic_edge_id< test_vertex_id, test_edge_offset >;

struct test_edge_content
{
        float price;
        float get_price() const
        {
                return price;
        }
};
using test_edge =
    generic_edge< test_edge_id, test_edge_content, float, test_vertex_id, test_edge_offset >;
struct test_vertex_content
{
};
using test_vertex = generic_vertex< test_vertex_id, test_vertex_content, test_edge >;
using test_graph  = generic_graph< test_vertex, test_edge >;

struct test_d_star_config
{
        using queue_item         = dstar_queue_item< test_vertex_id, float >;
        using heap               = binary_heap< queue_item >;
        using heap_handle        = typename heap::handle_type;
        using graph              = test_graph;
        using heuristic_function = zero_graph_heuristics< float >;
};

struct test_a_star_config
{
        using queue_item         = a_star_queue_item< test_vertex_id, float >;
        using heap               = binary_heap< queue_item >;
        using heap_handle        = typename heap::handle_type;
        using graph              = test_graph;
        using heuristic_function = zero_graph_heuristics< float >;
};

struct test_graph_case
{
        test_graph                                graph;
        std::filesystem::path                     file;
        std::size_t                               path_count;
        std::uniform_int_distribution< unsigned > uniform_d;
};

template < typename AddEdgeFunction >
inline void parse_test_graph_file( const std::filesystem::path& filename, AddEdgeFunction f )
{
        std::ifstream file{ filename };

        if ( !file ) {
                std::cerr << "Failed to open file: " << filename;
                std::exit( 1 );
        }

        std::string line;
        while ( std::getline( file, line ) ) {
                if ( line[0] == '#' ) {
                        continue;
                }

                std::stringstream ss{ line };
                unsigned          from;
                unsigned          to;

                ss >> from >> to;

                f( from, to );
        }
}

inline std::vector< std::filesystem::path > scan_test_graph_filenames()
{
        std::filesystem::path prefix = get_data_prefix();

        std::vector< std::filesystem::path > res;
        for ( const std::filesystem::path& p :
              std::filesystem::directory_iterator{ prefix / "graph" } ) {

                if ( p.extension() == ".txt" ) {
                        res.push_back( p );
                }
        }

        return res;
}
std::vector< test_graph_case > load_graphs()
{
        std::normal_distribution< float > normal_d{ 0, 1 };
        auto                              paths = scan_test_graph_filenames();
        std::mt19937                      gen;

        return map_f_to_v( paths, [&]( const std::filesystem::path& p ) {
                test_graph_case test_case;
                test_case.file = p;
                parse_test_graph_file( p, [&]( unsigned fromu, unsigned tou ) {
                        test_vertex_id from{ fromu };
                        test_vertex_id to{ tou };
                        while ( test_case.graph.size() <= from || test_case.graph.size() <= to ) {
                                test_case.graph.emplace_vertex( test_vertex_content{} );
                        }
                        float price = std::abs( normal_d( gen ) );
                        test_case.graph.add_edge( from, to, test_edge_content{ price } );
                } );

                test_case.uniform_d = std::uniform_int_distribution< unsigned >{
                    0, unsigned{ *test_case.graph.size() - 1 } };
                test_case.path_count = std::min( *test_case.graph.size() * 3, unsigned{ 100 } );

                return test_case;
        } );
}

class DStarTest : public ::testing::TestWithParam< test_graph_case >
{
public:
        std::mt19937 GEN;

        void eval_searchers(
            const test_graph&             graph,
            a_star< test_a_star_config >& astar,
            d_star< test_d_star_config >& dstar,
            unsigned                      from,
            unsigned                      to )
        {

                astar.set_task( graph, from, to );
                astar.compute_shortest_path( graph, 3 * ( *graph.size() ) );

                dstar.set_task( graph, from, to );
                dstar.compute_shortest_path( graph, 3 * ( *graph.size() ) );

                EXPECT_EQ( astar.get_search_state(), dstar.get_search_state() )
                    << "Task: " << from << " ~~> " << to << "\n"
                    << "A* state: " << astar.get_search_state() << "\n"
                    << "D* state: " << dstar.get_search_state() << std::endl;

                EXPECT_EQ( astar.get_path( graph ), dstar.get_path( graph ) )
                    << "Task: " << from << " ~~> " << to << "\n"
                    << "A* path: " << astar.get_path( graph ) << "\n"
                    << "D* path: " << dstar.get_path( graph ) << std::endl;
        }
};

TEST_P( DStarTest, randomPaths )
{
        test_graph_case              test_case = GetParam();
        a_star< test_a_star_config > astar{ test_case.graph };

        for ( std::size_t i : em::range( test_case.path_count ) ) {
                em::ignore( i );
                unsigned                     from = test_case.uniform_d( this->GEN );
                unsigned                     to   = test_case.uniform_d( this->GEN );
                d_star< test_d_star_config > dstar{ test_case.graph };
                eval_searchers( test_case.graph, astar, dstar, from, to );
        }
}

TEST_P( DStarTest, randomPathsReplanned )
{
        test_graph_case              test_case = GetParam();
        a_star< test_a_star_config > astar{ test_case.graph };
        d_star< test_d_star_config > dstar{ test_case.graph };

        for ( std::size_t i : em::range( test_case.path_count ) ) {
                em::ignore( i );
                unsigned from = test_case.uniform_d( this->GEN );
                unsigned to   = test_case.uniform_d( this->GEN );
                eval_searchers( test_case.graph, astar, dstar, from, to );
        }
}

INSTANTIATE_TEST_CASE_P(
    DStarGroup,
    DStarTest,
    testing::ValuesIn( load_graphs() ),
    []( const testing::TestParamInfo< DStarTest::ParamType >& info ) {
            std::stringstream      ss;
            const test_graph_case& c = info.param;
            ss << c.file.stem().string() << "_" << c.path_count;
            return ss.str();
    } );
