// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "environment.h"

#include <schpin_lib/json.h>

using namespace schpin;

int main( int argc, char* argv[] )
{
        if ( argc != 4 ) {
                SCHPIN_WARN_LOG( "env_to_util", "Bad number of arguments" );
        }

        std::string input_filename{ argv[1] };
        double      edge_length = std::stod( argv[2] );
        std::string output_filename{ argv[3] };

        env_config config;

        config.default_occ     = ENV_MIN_OCCUPANCY;
        config.min_edge_length = em::length( float( edge_length ) );
        config.paths.emplace_back( input_filename );

        build_environment( config ).match(
            [&]( environment&& env ) {
                    std::ofstream output{ output_filename };
                    safe_store_json( output, env ).optionally( []( const json_storage_error& e ) {
                            SCHPIN_WARN_LOG( "env_to_util", e );
                    } );
            },
            [&]( const env_error& e ) {
                    SCHPIN_WARN_LOG( "env_to_util", e );
            } );

        return 0;
}
