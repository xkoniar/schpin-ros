# Copyright 2021 Schpin
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

add_executable(export_robot test/util/export_robot.cpp)
motion_setup(export_robot)
target_link_libraries(export_robot PUBLIC schpin_motion)
target_compile_options(export_robot PRIVATE -O0 -g)

add_executable(env_to_json test/util/env_to_json.cpp)
motion_setup(env_to_json)
target_link_libraries(env_to_json PUBLIC schpin_motion)
target_compile_options(env_to_json PRIVATE -O3)
