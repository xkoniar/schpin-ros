// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "robot.h"
#include "robot/storage.h"

using namespace schpin;

//  TODO: fields are duplicated, make "fields.h" for config in schpin_lib with general fields used
// everywhere

struct export_config
{
        std::string           robot_description = "";
        std::string           robot_semantics   = "";
        std::filesystem::path cache_prefix      = "~/.schpin/cache";
        std::filesystem::path output_filename   = "";
        static auto           ros_param_get()
        {
                return std::make_tuple(
                    ros_param< std::string >(
                        "robot_description", "URDF description of the robot" ),
                    ros_param< std::string >(
                        "robot_semantics", "Semantic description of the robot" ),
                    ros_opt_param< std::filesystem::path >(
                        "cache_path",
                        "path to cache for robot termporary data",
                        "~/.schpin/cache" ),
                    ros_param< std::filesystem::path >(
                        "output_filename", "path to output file" ) );
        }
};

int main( int argc, char* argv[] )
{
        rclcpp::init( argc, argv );
        auto node = rclcpp::Node::make_shared( "export_robot" );

        get_config< export_config >( *node, "" )
            .match(
                [&]( export_config conf ) {
                        urdf::Model model;
                        model.initString( conf.robot_description );

                        load_srdf_info( conf.robot_semantics )
                            .convert_right( [&]( const xml_storage_error& e ) {
                                    return E( robot_error ).attach( e )
                                           << "Failed to load srdf for robot";
                            } )
                            .bind_left( [&]( const srdf_information& srdf_info ) {
                                    return load_robot( conf.cache_prefix, model, srdf_info );
                            } )
                            .match(
                                [&]( robot&& robot ) {
                                        std::ofstream out_stream{ conf.output_filename };
                                        SCHPIN_INFO_LOG(
                                            "export_robot", "Writing to " << conf.output_filename );
                                        if ( !out_stream ) {
                                                SCHPIN_WARN_LOG(
                                                    "export_robot",
                                                    "Failed to open file: " << strerror( errno ) );
                                                return;
                                        }
                                        safe_store_json( out_stream, robot )
                                            .optionally( [&]( const json_storage_error& e ) {
                                                    SCHPIN_WARN_LOG( "export_robot", e );
                                            } );
                                },
                                []( const robot_error& e ) {
                                        SCHPIN_WARN_LOG( "export_robot", e );
                                } );
                },
                [&]( auto err ) {
                        SCHPIN_ERROR_LOG( "export_robot", err );
                } );
        rclcpp::spin_some( node );

        return 0;
}
