// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_test/test_environment.h>

namespace schpin
{

std::filesystem::path get_data_prefix()
{
        std::filesystem::path prefix{ "./" };
        if ( ARGS.find( "--data-prefix" ) != ARGS.end() ) {
                prefix = ARGS["--data-prefix"];
        }
        return prefix;
}

em::either< environment, test_error > load_test_env( std::filesystem::path env_file )
{
        std::ifstream env_data{ get_data_prefix() / env_file };
        return safe_load_json< environment >( env_data )
            .convert_right( [&]( const json_storage_error& e ) -> test_error {
                    return E( test_error ).attach( e ) << "Failed to load test env." << env_file;
            } );
}

em::either< robot, test_error > load_test_robot( std::filesystem::path robot_file )
{
        std::ifstream robot_data{ get_data_prefix() / robot_file };
        return safe_load_json< robot >( robot_data )
            .convert_right( [&]( const json_storage_error& e ) -> test_error {
                    return E( test_error ).attach( e )
                           << "Failed to load test robot: " << robot_file;
            } );
}

}  // namespace schpin
