// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace schpin
{

class Prompt;

struct Choice
{
        std::string                            key;
        std::function< Prompt( std::string ) > cb;

        Choice( std::string key, std::function< Prompt( std::string ) > cb )
          : key( key )
          , cb( cb )
        {
        }
};

class Prompt
{
        const std::string     msg_;
        std::vector< Choice > choices_;

public:
        Prompt( std::string msg, std::vector< Choice > choices )
          : msg_( msg )
          , choices_( choices )
        {
        }

        std::optional< Prompt > ask()
        {
        }
};

void runPrompt( Prompt p )
{
        std::optional< Prompt > q = p.ask();
        while ( q ) {
                q = std::move( q->ask() );
        }
}

}  // namespace schpin
