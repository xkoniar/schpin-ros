// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "environment.h"
#include "robot.h"
#include "schpin_test/main.h"

#include <gtest/gtest.h>

namespace schpin
{

std::filesystem::path                 get_data_prefix();
em::either< environment, test_error > load_test_env( std::filesystem::path env_file );
em::either< robot, test_error >       load_test_robot( std::filesystem::path robot_file );

struct test_data_archive
{
        std::map< std::string, environment > env_;
        std::map< std::string, robot >       robot_;

public:
        test_data_archive(
            const std::vector< std::string >& envs   = {},
            const std::vector< std::string >& robots = {} )
        {
                for ( const std::string& name : envs ) {
                        preload_environment( name );
                }
                for ( const std::string& name : robots ) {
                        preload_robot( name );
                }
        }

        void preload_environment( std::string name )
        {
                if ( env_.find( name ) != env_.end() ) {
                        return;
                }
                load_test_env( "environment/" + name + "_octree.json" )
                    .match(
                        [&]( environment&& env ) {
                                env_.emplace( name, std::move( env ) );
                        },
                        [&]( const test_error& e ) {
                                FAIL() << e;
                        } );
        }
        void preload_robot( std::string name )
        {
                if ( robot_.find( name ) != robot_.end() ) {
                        return;
                }
                load_test_robot( "robot/" + name + ".json" )
                    .match(
                        [&]( robot&& robot ) {
                                robot_.emplace( name, std::move( robot ) );
                        },
                        [&]( const test_error& e ) {
                                FAIL() << e;
                        } );
        }

        const environment& get_env( const std::string& p ) const
        {
                auto iter = env_.find( p );
                if ( iter == env_.end() ) {
                        std::cerr << "required environment " << p << " for test is not loaded";
                        std::exit( 1 );
                }
                return iter->second;
        }

        const robot& get_robot( const std::string& p ) const
        {
                auto iter = robot_.find( p );
                if ( iter == robot_.end() ) {
                        std::cerr << "required robot " << p << " for test is not loaded";
                        std::exit( 1 );
                }
                return iter->second;
        }
};  // namespace schpin

}  // namespace schpin
