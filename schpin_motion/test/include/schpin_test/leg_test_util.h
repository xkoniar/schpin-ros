// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "leg.h"

#pragma once

using namespace schpin;

template < unsigned N >
inline leg_graph< N > gen_leg_test_graph( unsigned step )
{
        std::array< unsigned, N > steps;
        for ( unsigned& arr_step : steps ) {
                arr_step = step;
        }

        return generate_leg_graph< leg_graph< N >, N >( steps );
}

template < unsigned N >
inline void remove_ingoing_edges( leg_graph< N >& graph, const leg_vertex< N >& node )
{
        for ( leg_vertex< N >& subnode : graph.get_vertexes() ) {
                forEachIter( subnode.get_edges(), [&]( auto iter ) {
                        if ( iter->get_target_id() == node.get_id() ) {
                                subnode.remove_edge( iter );
                        }
                } );
        }
}

inline std::string draw_simplexes(
    const leg_graph< 2 >&                             graph,
    const std::vector< simplex< leg_vertex_id, 2 > >& storage )
{
        constexpr unsigned                                   scale = 2;
        std::map< unsigned, std::map< unsigned, unsigned > > area;

        for ( const simplex< leg_vertex_id, 2 >& simplex : storage ) {
                auto angles = em::map_f< std::vector< discrete_angles< 2 > > >(
                    simplex, [&]( leg_vertex_id nid ) {
                            return graph.get_vertex( nid )->angles;  //
                    } );

                for ( const discrete_angles< 2 > a : angles ) {
                        area[scale * a[0]][scale * a[1]] += 1;  //
                }

                area[unsigned( angles[0][0] + angles[1][0] )]
                    [unsigned( angles[0][1] + angles[1][1] )] += 1;
                area[unsigned( angles[1][0] + angles[2][0] )]
                    [unsigned( angles[1][1] + angles[2][1] )] += 1;
                area[unsigned( angles[2][0] + angles[0][0] )]
                    [unsigned( angles[2][1] + angles[0][1] )] += 1;
        }

        unsigned max_y = em::max_elem(
            area, [&]( std::pair< const unsigned&, const std::map< unsigned, unsigned >& > pack ) {
                    return pack.second.rbegin()->first;
            } );

        std::stringstream ss{};

        for ( std::size_t x : em::range( area.rbegin()->first + 1 ) ) {
                for ( std::size_t y : em::range( max_y + 1 ) ) {
                        unsigned val = area[unsigned( x )][unsigned( y )];

                        if ( y % 2 == 0 && x % 2 == 0 ) {
                                ss << "\033[4m";
                        } else {
                                ss << "\033[0m";
                        }
                        if ( val == 0 ) {
                                ss << "    ";
                        } else if ( val < 10 ) {
                                ss << " 0" << val << " ";
                        } else {
                                ss << " " << val << " ";
                        }
                }
                ss << std::endl;
                ss << std::endl;
        }
        return ss.str();
}
