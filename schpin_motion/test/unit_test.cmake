# Copyright 2021 Schpin
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

if(BUILD_TESTING)

  find_package(ament_cmake_gtest REQUIRED)
  find_package(ament_lint_auto REQUIRED)
  ament_lint_auto_find_test_dependencies()

  add_library(schpin_test_lib STATIC test/lib/test_environment.cpp)

  motion_setup(schpin_test_lib)
  target_include_directories(schpin_test_lib PUBLIC test/include/ src/)
  target_compile_options(schpin_test_lib PRIVATE -O3 -g)

  file(
    GLOB_RECURSE gtest_cpp_files
    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    "*_gtest.cpp")

  foreach(gtest_file ${gtest_cpp_files})
    string(REGEX REPLACE ".cpp" "" TEST_NAME ${gtest_file})
    string(REGEX REPLACE "/" "-" TEST_NAME ${TEST_NAME})

    schpin_add_gtest(${TEST_NAME} ${gtest_file} test/unit/main.cpp
                     WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/test/data/)

    target_link_libraries(${TEST_NAME} PUBLIC schpin_test_lib stdc++fs)
    motion_setup(${TEST_NAME})
    target_include_directories(${TEST_NAME} PRIVATE test/include/)
    target_compile_options(${TEST_NAME} PRIVATE -O0 -g)
  endforeach()

endif()
