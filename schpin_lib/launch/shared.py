
import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration


def generate_launch_description():

    return LaunchDescription([
        DeclareLaunchArgument('urdf'),
        Node(
            package='joint_state_publisher',
            executable='joint_state_publisher',
            output='both',
            parameters=[{
                "source_list" : ["/control/joint_states"]
            }]
        ),
        Node(package='robot_state_publisher',
             executable='robot_state_publisher',
            output='both',
             parameters=[{"robot_description": LaunchConfiguration('urdf')}]),
        Node(package='diagnostic_aggregator',
             executable='aggregator_node',
             output='both',
             parameters=[{
                "pub_rate": 1.,
                "base_path": '',
                "analyzers": {
                    "servomotors":{
                        "type":"diagnostic_aggregator/GenericAnalyzer",
                        "path": "Servomotors",
                        "startswith": ["Servomotor"]
                    }
                }
             }])
    ])
