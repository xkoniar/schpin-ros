cmake_minimum_required(VERSION 3.7.2)

project(schpin_lib)

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(schpin_msgs REQUIRED)
find_package(Boost REQUIRED)
find_package(Eigen3 REQUIRED NO_MODULE)
find_package(nlohmann_json REQUIRED)
find_package(geometry_msgs REQUIRED)

include(cmake/lua.cmake)
include(cmake/extra.cmake)

add_library(schpin_lib STATIC src/octree.cpp)
schpin_compile_options(schpin_lib)
target_compile_options(schpin_lib PRIVATE -DEMLABCPP_USE_STREAMS -DSCHPIN_USE_STREAMS -DEMLABCPP_USE_MAGIC_ENUM -DSCHPIN_USE_NLOHMANN_JSON -DEMLABCPP_USE_NLOHMANN_JSON -DEMLABCPP_USE_STREAMS)
target_link_libraries(schpin_lib PUBLIC readline assimp lua tbb)
target_include_directories(schpin_lib PUBLIC include/ emlabcpp/include/)
target_include_directories(schpin_lib SYSTEM PUBLIC third_party/include/)
ament_target_dependencies(schpin_lib PUBLIC Eigen3 nlohmann_json
                          geometry_msgs)

if(BUILD_TESTING)

  file(
    GLOB_RECURSE gtest_cpp_files
    RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
    "*_gtest.cpp")

  find_package(ament_cmake_gtest REQUIRED)
  find_package(ament_lint_auto REQUIRED)
  ament_lint_auto_find_test_dependencies()

  foreach(file ${gtest_cpp_files})
    string(REPLACE ".cpp" "" test_target_name "schpin_lib_test_${file}")
    string(REPLACE "/" "" test_target_name "${test_target_name}")

    schpin_add_gtest(${test_target_name} ${file} test/main.cpp)
    schpin_compile_options(${test_target_name})
    target_compile_options(${test_target_name} PRIVATE -g -O0
      -DSCHPIN_USE_STREAMS -DEMLABCPP_USE_STREAMS)
    target_link_libraries(${test_target_name} PUBLIC schpin_lib)
    target_include_directories(${test_target_name} PRIVATE test/include/)
    ament_target_dependencies(${test_target_name} PUBLIC rclcpp)
  endforeach()
endif()

install(
  DIRECTORY include/ emlabcpp/include/ third_party/include/
            ${lua_repo_SOURCE_DIR}/
  DESTINATION include/
  FILES_MATCHING
  PATTERN "*.h"
  PATTERN "*.hpp")

install(
  TARGETS schpin_lib lua
  LIBRARY DESTINATION lib/
  ARCHIVE DESTINATION lib/
  RUNTIME DESTINATION bin/
  INCLUDES
  DESTINATION include/)

install(DIRECTORY launch DESTINATION share/${PROJECT_NAME}/)

ament_export_include_directories(include)
ament_export_dependencies(assimp Eigen3 nlohmann_json urdf)
ament_export_libraries(lua)
ament_package(CONFIG_EXTRAS cmake/extra.cmake)
