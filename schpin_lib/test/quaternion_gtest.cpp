// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/quaternion.h"

#include <gtest/gtest.h>

using namespace schpin;

TEST( Quaternion, global_constants )
{
        quaternion< world_frame > v{ 0, 0, 0, 1.f };
        EXPECT_EQ( neutral_quat, v );
}

TEST( Quaternion, rotation )
{
        point< 3, world_frame > p{ 1.f, 0.f, 0.f };

        auto res_p = rotate( p, quaternion< world_frame >( y_axis, em::pi / 4.f ) );
        point< 3, world_frame > desired{ 0.70710678118f, 0, -0.70710678118f };
        EXPECT_TRUE( almost_equal( res_p, desired ) );
}
