// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/chain.h"

#include <gtest/gtest.h>
#include <numeric>

using namespace schpin;

TEST( chain, iterator )
{
        chain_link_container< float > links( 10 );
        std::iota( links.begin(), links.end(), 0.f );
        chain_pin_container< int > pins( 11 );
        std::iota( pins.begin(), pins.end(), 0 );

        chain_iterator begin{ links.begin(), pins.begin() };
        chain_iterator end{ links.end(), pins.end() };

        std::for_each( begin, end, [&]( const chain_view< float, int >& view ) {
                EXPECT_EQ( float( view.source_pin ), view.link );
                EXPECT_EQ( float( view.target_pin ), view.link + 1 );
        } );

        ASSERT_EQ( std::size_t( std::distance( begin, end ) ), links.size() );

        auto iter = std::find_if( begin, end, []( const chain_view< float, int >& ) {
                return false;
        } );
        ASSERT_EQ( iter, end );
}

TEST( chain, chainPushBack )
{
        chain< float, int > chain;

        chain.push_back( 0 );

        ASSERT_EQ( chain.size(), std::size_t( 0 ) );

        chain.push_back( 1 );

        ASSERT_EQ( chain.size(), std::size_t( 1 ) );
}

TEST( chain, chainInsert )
{
        chain< float, int > chain;

        std::vector< int > pins( 10 );
        std::iota( pins.begin(), pins.end(), 0 );

        auto iter = chain.insert( chain.end(), pins );

        ASSERT_EQ( iter, chain.begin() );
        ASSERT_EQ( chain.size(), std::size_t( 9 ) );

        iter = chain.insert( chain.end(), 10 );

        ASSERT_EQ( iter, std::prev( chain.end() ) );
        ASSERT_EQ( chain.size(), std::size_t( 10 ) );
}
