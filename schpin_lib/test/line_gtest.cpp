// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/line.h"
#include "schpin_test/point_test_util.h"

#include <gtest/gtest.h>

TYPED_TEST( PointTest, line_point_distance )
{
        static constexpr unsigned n = TypeParam::value;
        line< n, world_frame >    test_line{ this->zero_p_, this->unit_p_ };

        ASSERT_NEAR( *distance_of( test_line, this->unit_p_ ), *em::distance( 0. ), 1e-6 );
        ASSERT_NEAR( *distance_of( test_line, this->zero_p_ ), *em::distance( 0. ), 1e-6 );
        ASSERT_NEAR( *distance_of( test_line, -this->unit_p_ ), *length_of( this->unit_p_ ), 1e-6 );
}

TYPED_TEST( PointTest, perpendicular )
{
        static constexpr unsigned n = TypeParam::value;
        point< 2, world_frame >   p{ { n, 0 } };
        point< 2, world_frame >   zero_p{ { 0, 0 } };
        point< 2, world_frame >   unit_p{ { n, n } };

        em::distance diag_dist{ float( sqrt( 2 * n * n ) / 2 ) };

        line< 2, world_frame > test_line{ zero_p, unit_p };
        ASSERT_NEAR( *distance_of( test_line, p ), *diag_dist, 1e-6 );

        line< 2, world_frame > reversed_line{ unit_p, zero_p };
        ASSERT_NEAR( *distance_of( reversed_line, p ), *diag_dist, 1e-6 );

        line< 2, world_frame > diag_line{
            point< 2, world_frame >{ { 0, n } }, point< 2, world_frame >{ { n, 0 } } };
        ASSERT_NEAR( *distance_of( diag_line, zero_p ), *diag_dist, 1e-6 );
        ASSERT_NEAR( *distance_of( diag_line, unit_p ), *diag_dist, 1e-6 );
}
