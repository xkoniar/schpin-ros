// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <emlabcpp/match.h>
#include <gtest/gtest.h>
#include <schpin_lib/lua.h>

using namespace schpin;

struct empty_event
{
};

struct one_arg_event
{
        std::string arg;
};

struct multiarg_event
{
        std::string arg;
        float       val;
};

struct return_event
{
        std::string arg;
};

struct return_response
{
        std::string var;
        float       fvar;
};

struct optarg_event
{
        std::optional< std::string > opt_str;
};

struct array_event
{
        std::array< float, 10 > data;
};

struct empty_bind
{
        static constexpr std::string_view name = "empty";
        static constexpr std::string_view desc = "Command without argument";

        inline static const auto           arg = slua_struct< empty_event >();
        inline static const no_return_type res{};
};

struct onecmd_bind
{
        static constexpr std::string_view name = "onecmd";
        static constexpr std::string_view desc = "Command with one argument";

        inline static const auto arg =
            slua_struct< one_arg_event >( lua_key_arg< std::string >{ "first", "one key arg" } );
        inline static const no_return_type res{};
};

struct multicmd_bind
{
        static constexpr std::string_view name = "multicmd";
        static constexpr std::string_view desc = "Command with multiple arguments";

        inline static const auto arg = slua_struct< multiarg_event >(
            lua_key_arg< std::string >{
                "first",
                "First argument that is a "
                "string!" },
            lua_key_arg< float >{ "second", "Second argument!" } );
        inline static const no_return_type res{};
};

struct return_bind
{
        static constexpr std::string_view name = "returncmd";
        static constexpr std::string_view desc = "Command that returns value";

        inline static const auto arg =
            slua_struct< return_event >( lua_key_arg< std::string >{ "arg", "Single argument!" } );
        inline static const auto res = slua_result< std::tuple< std::string, std::string > >{
            "Result of function is result!" };
};
struct array_bind
{
        static constexpr std::string_view name = "arrayproc";
        static constexpr std::string_view desc = "Procedure that has struct with an array";

        inline static const auto arg = slua_struct< array_event >(
            lua_key_arg< std::array< float, 10 > >{ "data", "Array of data" } );
        inline static const no_return_type res{};
};
struct opt_bind
{
        static constexpr std::string_view name = "opt_arg";
        static constexpr std::string_view desc = "Lua call with opt arg";

        inline static const auto           arg = slua_struct< optarg_event >(  //
            lua_opt_key_arg< std::string >{ "opt_str", "optional string as argument", "defval" } );
        inline static const no_return_type res{};
};

struct slua_test_vm_def
{
        using def = std::tuple<
            slua_callback< slua_print_bind >,
            slua_callback< empty_bind >,
            slua_callback< onecmd_bind >,
            slua_callback< multicmd_bind >,
            slua_callback< return_bind >,
            slua_callback< array_bind >,
            slua_callback< opt_bind > >;
};

TEST( lua, basic )
{
        using vm_type = slua_vm< slua_test_vm_def >;
        vm_type vm;

        std::string code = "print(\"wolololo\", 42);";
        vm.load( code ).optionally( [&]( slua_error e ) {
                FAIL() << e;
        } );

        auto event_matcher = em::matcher{
            [&]( lua_State*, slua_print_event e ) {
                    EXPECT_EQ( e.msg, "wolololo\t42" );
            },
            [&]( lua_State*, empty_event ) {
                    FAIL();
            },
            [&]( lua_State*, optarg_event ) {
                    FAIL();
            },
            [&]( lua_State*, array_event ) {
                    FAIL();
            },
            [&]( lua_State*, one_arg_event ) {
                    FAIL();
            },
            [&]( lua_State*, multiarg_event ) {
                    FAIL();
            },
            [&]( lua_State*, return_event ) -> std::tuple< std::string, std::string > {
                    return std::tuple< std::string, std::string >{ "1234", "5678" };
            } };

        std::variant< lua_res, slua_error > res = vm.exec( event_matcher );

        em::match(
            res,
            [&]( lua_res ) {},
            [&]( slua_error e ) {
                    FAIL() << e;
            } );
}
