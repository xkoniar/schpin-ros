// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"

#include <gtest/gtest.h>

#pragma once

using namespace schpin;

template < typename IC >
class PointTest : public ::testing::Test
{
public:
        static constexpr unsigned n = IC::value;

        void SetUp() override
        {
                zero_p_    = point< n, world_frame >::make_filled_with( 0.f );
                unit_p_    = point< n, world_frame >::make_filled_with( 1.f );
                dim_p_     = point< n, world_frame >::make_filled_with( float( n ) );
                neg_dim_p_ = point< n, world_frame >::make_filled_with( -float( n ) );
        }

        point< n, world_frame > zero_p_;
        point< n, world_frame > unit_p_;
        point< n, world_frame > dim_p_;
        point< n, world_frame > neg_dim_p_;
};

using test_types = ::testing::Types<
    std::integral_constant< unsigned, 2 >,
    std::integral_constant< unsigned, 3 >,
    std::integral_constant< unsigned, 4 >,
    std::integral_constant< unsigned, 8 > >;

TYPED_TEST_CASE( PointTest, test_types );
