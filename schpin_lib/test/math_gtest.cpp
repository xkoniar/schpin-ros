// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/math.h"

#include <gtest/gtest.h>

using namespace schpin;

TEST( Determinant, fourtofour )
{

        std::array< float, 16 > data{ 1, 0, 2, -1, 3, 0, 0, 5, 2, 1, 4, -3, 1, 0, 5, 0 };

        ASSERT_EQ( determinant< 4 >( data ), 30.f );
}

TEST( Determinant, threetothree )
{

        std::array< float, 9 > data{ 1, 2, 1, 3, 0, 4, 8, 4, 10 };

        ASSERT_EQ( determinant< 3 >( data ), 0.f );
}

TEST( Determinant, twototwo )
{

        std::array< float, 4 > data{ -6, 8, 0, 5 };

        ASSERT_EQ( determinant< 2 >( data ), -30.f );
}
