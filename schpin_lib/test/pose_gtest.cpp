// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/pose.h"
#include "schpin_lib/geom/pose/serialization.h"

#include <gtest/gtest.h>

using namespace schpin;

template < typename Tag >
float sum_distance( pose_distance< Tag > pd )
{
        return *pd.dist + *pd.angle_dist;
}

TEST( Pose, distance )
{
        pose< world_frame > neutral;
        pose< world_frame > pos_offseted{ point< 3, world_frame >( 10, 0, 0 ) };
        pose< world_frame > angle_offseted{ quaternion< world_frame >( z_axis, em::angle( 1. ) ) };

        ASSERT_NEAR( sum_distance( distance_of( neutral, pos_offseted ) ), 10., 1e-6 );
        ASSERT_NEAR( sum_distance( distance_of( neutral, angle_offseted ) ), 1., 1e-6 );
}

TEST( Pose, interp )
{
        pose< world_frame > neutral;
        pose< world_frame > pos_offseted{ point< 3, world_frame >( 10, 0, 0 ) };
        pose< world_frame > angle_offseted{ quaternion< world_frame >( z_axis, em::angle( 1. ) ) };

        pose< world_frame > offseted{
            point< 3, world_frame >( 10, 0, 0 ),
            quaternion< world_frame >( z_axis, em::angle( 1. ) ) };

        pose< world_frame > interpolated = lin_interp( neutral, pos_offseted, 0.5 );
        pose< world_frame > interpolated_exp{ point< 3, world_frame >( 5, 0, 0 ) };

        ASSERT_TRUE( almost_equal( interpolated, interpolated_exp, em::default_epsilon * 2 ) )
            << interpolated << "\n"
            << interpolated_exp;

        interpolated = lin_interp( neutral, angle_offseted, 0.5 );
        interpolated_exp =
            pose< world_frame >{ quaternion< world_frame >( z_axis, em::angle{ 0.5 } ) };

        ASSERT_TRUE( almost_equal( interpolated, interpolated_exp, em::default_epsilon * 2 ) )
            << interpolated << "\n"
            << interpolated_exp;

        pose< world_frame > half_offseted{
            point< 3, world_frame >( 5, 0, 0 ),
            quaternion< world_frame >( z_axis, em::angle( 0.5 ) ) };
        ASSERT_TRUE( almost_equal(
            lin_interp( neutral, offseted, 0.5 ), half_offseted, em::default_epsilon * 2 ) );
}
