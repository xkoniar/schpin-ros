#include "schpin_lib/bucket_memory_resource.h"

#include <gtest/gtest.h>

using namespace schpin;

using test_mem = bucket_memory_resource< 16, 126 >;
using test_map = std::pmr::map< std::string, std::string >;

TEST( TestMem, map_insert )
{
        using test_alloc = typename test_map::allocator_type;

        test_mem   memory;
        test_alloc alloc{ &memory };
        test_map   map{ alloc };

        for ( std::size_t i : em::range( test_mem::bucket_count ) ) {
                map.insert( { std::to_string( i ), "WOLOLO" } );
        }

        std::pair< std::string, std::string > last{ "last", "last" };
        EXPECT_THROW( map.insert( last ), std::bad_alloc );
}

TEST( TestMem, map_random )
{
        using test_alloc = typename test_map::allocator_type;

        test_mem   memory;
        test_alloc alloc{ &memory };
        test_map   map{ alloc };

        for ( std::size_t i : em::range( 999u ) ) {
                ASSERT_EQ( map.size(), memory.used() );
                if ( map.size() == test_mem::bucket_count || std::rand() % 2 == 0 ) {
                        if ( map.size() != 0 ) {
                                map.erase( map.begin() );
                        }
                } else {
                        map.insert( { std::to_string( i ), "wololo" } );
                }
        }
}
