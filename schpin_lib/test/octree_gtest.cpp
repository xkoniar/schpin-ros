// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <gtest/gtest.h>
#include <schpin_lib/geom/point/collision.h>
#include <schpin_lib/octree/occupancy.h>
#include <schpin_lib/octree/tree.h>

using namespace schpin;

using test_point = point< 3, oc_frame >;

TEST( OcTree, basePos )
{
        std::vector< std::tuple< uint8_t, int, int, int > > pos = {
            { 0, -1, -1, -1 },
            { 1, 1, -1, -1 },
            { 2, -1, 1, -1 },
            { 3, 1, 1, -1 },
            { 4, -1, -1, 1 },
            { 5, 1, -1, 1 },
            { 6, -1, 1, 1 },
            { 7, 1, 1, 1 },
        };

        for ( auto [a, x, y, z] : pos ) {
                vec< 3 > calculated = get_oc_child_direction( a );
                vec< 3 > expected{ x, y, z };
                EXPECT_EQ( calculated, expected );
        }
}

TEST( OcTree, contextScaling )
{
        oc_node_context default_context{
            0, 66, em::length{ 1.f }, point< 3, oc_frame >{ 1.f, 0.f, 1.f } };

        auto downscaled = map_f_to_v( em::range( 8u ), [&]( std::size_t i ) {
                return oc_lower_context( default_context, static_cast< oc_child_index >( i ) );
        } );

        std::vector< oc_node_context > sorted{ downscaled.begin(), downscaled.end() };
        std::sort( sorted.begin(), sorted.end() );
        bool is_unique = std::unique( sorted.begin(), sorted.end() ) == sorted.end();

        EXPECT_TRUE( is_unique ) << em::view{ downscaled };

        for ( std::size_t i : em::range( 8u ) ) {
                auto upscaled =
                    oc_upper_context( downscaled[i], static_cast< oc_child_index >( i ) );
                EXPECT_EQ( upscaled, default_context );
        }
}

template < typename Node, typename CondFunction, typename CollectFunction >
void collect_impl( const Node& node, CondFunction cond_f, CollectFunction collect_f )
{
        if ( cond_f( node ) ) {
                collect_f( node );
        }
        if ( node.is_leaf() ) {
                return;
        }
        for ( auto&& child : node.generate_children() ) {
                collect_impl( child, cond_f, collect_f );
        }
}

template < typename Node, typename CondFunction >
std::vector< Node > collect( const Node& node, CondFunction cond_f )
{
        std::vector< Node > res;
        collect_impl( node, cond_f, [&]( const Node& node ) {  //
                res.push_back( node );
        } );
        return res;
}

template < typename NodeContentT >
class oc_tree_typed_test : public ::testing::Test
{
public:
        using node_content = NodeContentT;
        // TODO: I think the visitor definitons are more or less same everyhwere: grep them and
        // check them and simplify them
        using test_visitor = oc_collision_visitor<
            oc_collision_checker<
                sat_point_collision_query< 3, oc_frame >,
                bs_point_collision_query< 3, oc_frame >,
                oc_node< occupancy_content > >,
            1 >;
        using test_oc_tree = oc_tree< occupancy_content >;
};

// TODO: add another type of content?
using oc_tree_types = ::testing::Types< occupancy_content >;

TYPED_TEST_CASE( oc_tree_typed_test, oc_tree_types );

TYPED_TEST( oc_tree_typed_test, insertion )
{
        using test_oc_tree = typename oc_tree_typed_test< TypeParam >::test_oc_tree;
        using test_visitor = typename oc_tree_typed_test< TypeParam >::test_visitor;
        occupancy_content node_content{ occupancy{ 0.f } };
        em::length        min_edge_length{ 0.1f };

        test_oc_tree tree{ min_edge_length, node_content };

        test_point center{ 0.f, 0.f, 0.f };

        EXPECT_TRUE( tree.root_view().overlaps( center ) );

        test_point distant{ 1.f, 1.f, 1.f };

        EXPECT_FALSE( tree.root_view().overlaps( distant ) );

        tree.expand( distant );
        test_visitor v{ std::vector{ distant }, occupancy_token::FULL };
        tree.modify( v );

        EXPECT_TRUE( tree.root_view().overlaps( distant ) );

        auto occupied_nodes = collect( tree.root_view(), [&]( const auto& node ) {
                return ( *node )->occ() > OC_MAX_OCCUPANCY / 2.f;
        } );

        EXPECT_EQ( occupied_nodes.size(), std::size_t{ 1 } );
        auto marked_ptr = occupied_nodes.front();
        EXPECT_TRUE( marked_ptr.overlaps( distant ) );
        distant += vec< 3 >{ marked_ptr.edge_length(), 0.f, 0.f };

        tree.expand( distant );
        test_visitor v2{ std::vector{ distant }, occupancy_token::FULL };
        tree.modify( v2 );

        EXPECT_TRUE( tree.root_view().overlaps( distant ) );

        occupied_nodes = collect( tree.root_view(), [&]( const auto& node ) {  //
                return ( *node )->occ() > OC_MAX_OCCUPANCY / 2.f;
        } );

        EXPECT_EQ( occupied_nodes.size(), std::size_t{ 2 } );
}

template < typename Content >
bool has_equal_structure( const oc_node< Content >& lh, const oc_node< Content >& rh )
{
        if ( lh.is_leaf() && rh.is_leaf() ) {
                return *lh == *rh;
        }
        if ( lh.is_leaf() || rh.is_leaf() ) {
                return false;
        }
        return em::all_of( em::range( lh.size() ), [&]( std::size_t i ) {
                return has_equal_structure( lh[i], rh[i] );
        } );
}

TEST( OcTree, complexInsertion )
{
        using test_oc_tree = typename oc_tree_typed_test< occupancy_content >::test_oc_tree;
        using node         = typename test_oc_tree::node;
        using test_content = typename test_oc_tree::node_content;

        test_content empty{ occupancy{ 0.f } };
        test_content full{ occupancy{ 1.f } };
        auto         mn = [&]( auto&&... items ) {
                std::array< node, 8 > arr{ std::move( items )... };
                return std::make_unique< std::array< node, 8 > >( std::move( arr ) );
        };

        node bottom{
            empty,
            mn( node{ empty },
                node{ full },
                node{ empty },
                node{ full },
                node{ empty },
                node{ full },
                node{ empty },
                node{ full } ),
        };
        node root{
            empty,
            mn( bottom.copy(),
                bottom.copy(),
                bottom.copy(),
                bottom.copy(),
                bottom.copy(),
                bottom.copy(),
                bottom.copy(),
                bottom.copy() ),
        };

        test_oc_tree source_tree{
            std::move( root ),
            oc_node_context{ 0, 2, em::length{ 1.f }, point< 3, oc_frame >{} },
            empty };

        auto occupied_nodes = collect( source_tree.root_view(), [&]( const auto& node ) {
                return ( *node )->occ() > OC_MAX_OCCUPANCY / 2.f;
        } );

        using visitor = oc_collision_visitor<
            oc_collision_checker<
                sat_point_collision_query< 3, oc_frame >,
                bs_point_collision_query< 3, oc_frame >,
                oc_node< occupancy_content > >,
            1 >;

        test_oc_tree made_tree{
            node{ empty },
            oc_node_context{ 0, 0, em::length{ 1.f }, point< 3, oc_frame >{} },
            empty };

        for ( const auto& proxy_node : occupied_nodes ) {
                visitor v{ std::vector{ proxy_node.center_position() }, occupancy_token::FULL };
                made_tree.expand( proxy_node.center_position() );
                made_tree.modify( v );
        }

        EXPECT_TRUE( has_equal_structure( made_tree.bare_root(), source_tree.bare_root() ) )
            << "made tree: " << made_tree.root_view() << "\n"
            << "source tre: " << source_tree.root_view();
}
