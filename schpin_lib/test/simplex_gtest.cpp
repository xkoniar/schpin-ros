// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point/serialization.h"
#include "schpin_lib/geom/simplex.h"

#include <gtest/gtest.h>

using namespace schpin;

TEST( SimplexTest, volume2d )
{
        std::array< point< 2, world_frame >, 3 > data{
            point< 2, world_frame >( 0, 1 ),
            point< 2, world_frame >( 1, 0 ),
            point< 2, world_frame >( 0, 0 ) };

        simplex< point< 2, world_frame >, 2 > sim( data );

        ASSERT_EQ( volume_of( sim ), 0.5f );
}
TEST( SimplexTest, volume3d )
{
        std::array< point< 3, world_frame >, 4 > data{
            point< 3, world_frame >( 0, 0, 1 ),
            point< 3, world_frame >( 1, 0, 0 ),
            point< 3, world_frame >( 0, 0, 0 ),
            point< 3, world_frame >{ 0, 1, 0 } };

        simplex< point< 3, world_frame >, 3 > sim( data );

        ASSERT_NEAR( volume_of( sim ), 0.166667f, 0.00001f );
}

TEST( SimplexTest, json )
{
        std::array< point< 3, world_frame >, 4 > data{
            point< 3, world_frame >( 0, 0, 1 ),
            point< 3, world_frame >( 1, 0, 0 ),
            point< 3, world_frame >( 0, 0, 0 ),
            point< 3, world_frame >{ 0, 1, 0 } };

        using simplex_type = simplex< point< 3, world_frame >, 3 >;

        simplex_type sim( data );

        nlohmann::json simplex_j = nlohmann::json( sim );
        ASSERT_EQ( simplex_j.get< simplex_type >(), sim ) << simplex_j;
}
