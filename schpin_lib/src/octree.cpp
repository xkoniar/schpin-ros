#include "schpin_lib/octree/node_context.h"

namespace schpin
{
std::ostream& operator<<( std::ostream& os, const oc_node_context& cont )
{
        os << "Depth: " << cont.depth << " MaxDepth: " << cont.max_depth
           << " Edgelength: " << cont.edge_length << " Position: " << cont.center_position;
        return os;
}

}  // namespace schpin
