
#include <emlabcpp/protocol/register_map.h>
#include <nlohmann/json.hpp>

#pragma once

namespace emlabcpp
{

template < protocol_endianess_enum Endianess, typename... Regs >
inline void
fill_from_json( em::protocol_register_map< Endianess, Regs... >& m, const nlohmann::json& j )
{
        using map = em::protocol_register_map< Endianess, Regs... >;
        protocol_for_each_register(
            m, [&]< auto key >( const auto& ) {
                    using T = typename map::reg_value_type< key >;
                    std::string name{ magic_enum::enum_name( key ) };
                    if ( !j.contains( name ) ) {
                            return;
                    }
                    m.template set_val< key >( j[name].get< T >() );
            } );
}

template < protocol_endianess_enum Endianess, typename... Regs >
inline void from_json( const nlohmann::json& j, em::protocol_register_map< Endianess, Regs... >& m )
{
        using map = em::protocol_register_map< Endianess, Regs... >;
        protocol_for_each_register(
            m, [&]< auto key >( const auto& ) {
                    using T = typename map::reg_value_type< key >;
                    std::string name{ magic_enum::enum_name( key ) };
                    m.template set_val< key >( j[name].get< T >() );
            } );
}

template < protocol_endianess_enum Endianess, typename... Regs >
inline void to_json( nlohmann::json& j, const em::protocol_register_map< Endianess, Regs... >& m )
{
        // TODO: copy pasta festival from above
        using map = em::protocol_register_map< Endianess, Regs... >;
        protocol_for_each_register(
            m, [&]< auto key >( const auto& val ) {
                    std::string name{ magic_enum::enum_name( key ) };
                    j[name] = val;
            } );
}
}  // namespace emlabcpp
