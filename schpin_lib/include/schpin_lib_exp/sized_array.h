// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <array>
#include <schpin_lib/geom/vec.h>
#include <type_traits>
#include <vector>

#pragma once

namespace schpin
{

template < std::size_t N >
struct static_size
{
        struct token
        {
                const std::size_t value = N;
        };

        static constexpr std::size_t value = N;
};

struct dynamic_size
{
        struct token
        {
                std::size_t value;
        };

        static constexpr std::size_t value = 0;
};

template < typename T, typename SizeType >
class sized_array
{
public:
        static constexpr bool is_dynamic = std::is_same_v< SizeType, dynamic_size >;
        using container =
            std::conditional_t< is_dynamic, std::vector< T >, std::array< T, SizeType::value > >;
        using size_type = SizeType;
        using token     = typename SizeType::token;

        using value_type      = T;
        using reference       = T&;
        using const_reference = const T&;

        using iterator       = typename container::iterator;
        using const_iterator = typename container::const_iterator;

private:
        container data_;

public:
        sized_array() = default;

        sized_array( token t )
        {
                em::ignore( t );
                if constexpr ( is_dynamic ) {
                        data_ = container( t.value );
                }
        }

        sized_array( container d )
          : data_( std::move( d ) )
        {
        }

        sized_array( std::initializer_list< T > data )
          : data_( data )
        {
        }

        sized_array( const sized_array& )     = default;
        sized_array( sized_array&& ) noexcept = default;

        sized_array& operator=( const sized_array& ) = default;
        sized_array& operator=( sized_array&& ) noexcept = default;

        sized_array copy() const
        {
                if constexpr ( is_dynamic ) {
                        return { map_f_to_v( data_, []( const T& item ) {
                                return item.copy();
                        } ) };
                } else {
                        return { em::map_f_to_a( data_, []( const T& item ) {
                                return item.copy();
                        } ) };
                }
        }

        token get_size_token() const
        {
                if constexpr ( is_dynamic ) {
                        return token{ data_.size() };
                } else {
                        return token{};
                }
        }

        iterator begin()
        {
                return data_.begin();
        }

        const_iterator begin() const
        {
                return data_.begin();
        }

        iterator end()
        {
                return data_.end();
        }

        const_iterator end() const
        {
                return data_.end();
        }

        T& operator[]( std::size_t i )
        {
                return data_[i];
        }

        const T& operator[]( std::size_t i ) const
        {
                return data_[i];
        }

        constexpr std::size_t size() const
        {
                return data_.size();
        }
};

template <
    typename SizeType,
    typename Container,
    typename Function,
    typename T = std::decay_t< em::mapped_t< Container, Function > > >
inline sized_array< T, SizeType > map_f_to_sz( Container&& cont, Function&& f )
{
        if constexpr ( sized_array< T, SizeType >::is_dynamic ) {
                return sized_array< T, SizeType >{ map_f_to_v(
                    std::forward< Container >( cont ), std::forward< Function >( f ) ) };
        } else {
                return sized_array< T, SizeType >{ em::map_f_to_a< SizeType::value >(
                    std::forward< Container >( cont ), std::forward< Function >( f ) ) };
        }
}

template < typename T, typename SizeType >
inline constexpr bool
operator==( const sized_array< T, SizeType >& lh, const sized_array< T, SizeType >& rh )
{
        return em::equal( lh, rh );
}

template < typename T, typename SizeType >
inline std::ostream& operator<<( std::ostream& os, const sized_array< T, SizeType >& arr )
{
        return os << em::view{ arr };
}

}  // namespace schpin

template < typename T, typename SizeType >
struct nlohmann::adl_serializer< schpin::sized_array< T, SizeType > >
{
        static void to_json( nlohmann::json& j, const schpin::sized_array< T, SizeType >& cont )
        {
                j = nlohmann::json( emlabcpp::view{ cont } );
        }

        template < std::size_t N, std::size_t... Is >
        static std::array< T, N >
        extract_array( const nlohmann::json& j, std::index_sequence< Is... > )
        {
                auto f = []( const nlohmann::json& jj, std::size_t i ) {
                        return jj[i].get< T >();
                };
                return std::array< T, N >{ f( j, Is )... };
        }

        static schpin::sized_array< T, SizeType > from_json( const nlohmann::json& j )
        {
                using sa = schpin::sized_array< T, SizeType >;
                if constexpr ( sa::is_dynamic ) {
                        return sa( j.get< std::vector< T > >() );
                } else {
                        static constexpr std::size_t n = sa::size_type::value;
                        return sa( extract_array< n >( j, std::make_index_sequence< n >() ) );
                }
        }
};
