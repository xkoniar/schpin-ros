// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/iterator.h"

#pragma once

namespace em = emlabcpp;

namespace schpin
{
template < typename, typename >
class convert_iterator;
}

template < typename T, typename Iterator >
struct std::iterator_traits< schpin::convert_iterator< T, Iterator > >
{
        using value_type        = T;
        using difference_type   = std::ptrdiff_t;
        using pointer           = value_type;
        using const_pointer     = value_type;
        using reference         = value_type;
        using iterator_category = std::random_access_iterator_tag;
};

namespace schpin
{

template < typename T, typename Iterator >
class convert_iterator : public em::generic_iterator< convert_iterator< T, Iterator > >
{
        Iterator iter_;

public:
        explicit convert_iterator( Iterator iter )
          : iter_( std::move( iter ) )
        {
        }

        T operator*()
        {
                return T{ *iter_ };
        }
        T operator*() const
        {
                return T{ *iter_ };
        }

        convert_iterator& operator+=( std::ptrdiff_t offset )
        {
                std::advance( iter_, offset );
                return *this;
        }

        convert_iterator& operator-=( std::ptrdiff_t offset )
        {
                std::advance( iter_, -offset );
                return *this;
        }

        bool operator<( const convert_iterator& other ) const
        {
                return iter_ < other.iter_;
        }

        bool operator==( const convert_iterator& other ) const
        {
                return iter_ == other.iter_;
        }

        std::ptrdiff_t operator-( const convert_iterator& other )
        {
                return iter_ - other.iter_;
        }
};

template < typename T, typename Container, typename Iterator = em::iterator_of_t< Container > >
em::view< convert_iterator< T, Iterator > > convert_view( Container&& cont )
{
        return em::view{
            convert_iterator< T, Iterator >{ cont.begin() },  //
            convert_iterator< T, Iterator >{ cont.end() } };
}

}  // namespace schpin
