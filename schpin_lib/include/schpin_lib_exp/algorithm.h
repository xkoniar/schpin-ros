// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/algorithm.h"

#pragma once

namespace schpin
{

template < typename ConditionFunction, typename Function >
constexpr void until( ConditionFunction&& cond, Function&& f )
{
        while ( !cond() ) {
                f();
        }
}

// Expects the function f(x) to return std::optional<T>, applies f(x) to each item of container
// 'cont' and returns first optional that holds some item, empty optional if one is not present.
template <
    typename Container,
    typename UnaryFunction,
    typename T = typename em::mapped_t< Container, UnaryFunction >::value_type >
[[nodiscard]] constexpr std::optional< T >
first_optional( Container&& cont, UnaryFunction&& f = std::identity() )
{
        std::optional< T >               res;
        __attribute__( ( unused ) ) auto item = em::find_if( cont, [&]( const auto& item ) {
                res = f( item );
                return bool( res );
        } );
        return res;
}

template <
    typename Container,
    typename UnaryFunction,
    typename T = typename em::mapped_t< Container, UnaryFunction >::value_type >
[[nodiscard]] inline std::vector< T >
filter_optionals( Container&& cont, UnaryFunction&& f = std::identity() )
{
        std::vector< T > res;
        em::for_each( std::forward< Container >( cont ), [&]< typename Item >( Item&& item ) {
                auto opt = f( std::forward< Item >( item ) );

                if ( opt ) {
                        res.push_back( std::move( *opt ) );
                }
        } );
        return res;
}

// Returns true if value 'value' can be find in 'cont' withn an usage of binary search
template < typename Container, typename T >
[[nodiscard]] constexpr bool binary_search( const Container& cont, const T& value )
{
        return std::binary_search( cont.begin(), cont.end(), value );
}

// Takes type T which is convertible to std::string_view, splits the string by any of delimitors in
// 'delims' and returns std::vector containing string_views to splitted parts
// TODO: note that copied from https://www.bfilipek.com/2018/07/string-view-perf-followup.html
template < typename T >
[[nodiscard]] inline std::vector< std::string_view >
split( T&& line, std::string_view delims = " " )
{
        std::string_view                strv{ std::forward< T >( line ) };
        std::vector< std::string_view > output;
        size_t                          first = 0;

        while ( first < strv.size() ) {
                const auto second = strv.find_first_of( delims, first );

                if ( first != second ) {
                        output.emplace_back( strv.substr( first, second - first ) );
                }

                if ( second == std::string_view::npos ) {

                        break;
                }

                first = second + 1;
        }
        return output;
}

// TODO: this needs more love
template < typename T, typename UnaryFunction >
inline void cartesian_product( const std::vector< std::vector< T > >& data, UnaryFunction&& f )
{
        using item_iterator                     = typename std::vector< T >::const_iterator;
        std::vector< item_iterator > iterators  = map_f_to_v( data, &std::begin );
        std::vector< T& >            item_stack = map_f_to_v( iterators, [&]( auto iter ) {
                return *iter;
        } );

        std::size_t i = data.size();

        while ( i != 0 || iterators[0] != data[0].begin() ) {
                f( item_stack );

                i = data.size();
                do {
                        --i;
                        if ( iterators[i] != data[i].end() ) {
                                ++iterators[i];
                                item_stack[i] = *iterators[i];
                                break;
                        }
                        iterators[i] = data[i].begin();
                        iterators[i] = *iterators[i];

                } while ( i != 0 );
        }
}

template < typename Container, typename UnaryFunction = std::identity >
[[nodiscard]] constexpr em::iterator_of_t< const Container& >
min_iterator( const Container& cont, UnaryFunction&& f = std::identity() )
{
        using value_type = decltype( f( *cont.begin() ) );

        auto       beg      = cont.begin();
        auto       end      = cont.end();
        value_type res_val  = std::numeric_limits< value_type >::max();
        auto       res_iter = cont.begin();

        for ( ; beg != end; ++beg ) {
                value_type val = f( *beg );
                if ( val < res_val ) {
                        res_val  = val;
                        res_iter = beg;
                }
        }
        return res_iter;
}

}  // namespace schpin
