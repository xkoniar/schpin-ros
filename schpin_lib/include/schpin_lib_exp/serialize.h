// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib_exp/static_view.h"

#include <array>

#pragma once

namespace schpin
{

enum class endianess_enum
{
        BIG,
        LOW
};

template < typename >
inline constexpr endianess_enum serialization_endianess = endianess_enum::LOW;

template < typename T >
struct serializer
{
        static_assert(
            std::is_trivial_v< T >,
            "Default serializer is able to serialize only trivial types" );
        static constexpr std::size_t size = sizeof( T );

        template < endianess_enum Endianess = serialization_endianess< T >, typename Iterator >
        static constexpr void serialize( static_view< size, Iterator > place, T item )
        {
                // todo: fix asserts

                // static_assert(
                //    std::is_same_v<
                //        std::decay_t< typename std::iterator_traits< Iterator >::value_type >,
                //        char > );
                if constexpr ( Endianess == endianess_enum::LOW ) {
                        for ( std::size_t i = 0; i < size; ++i ) {
                                place[size - 1 - i] = static_cast< uint8_t >( item & 0xFF );
                                item                = static_cast< T >( item >> 8 );
                        }
                } else {
                        for ( std::size_t i = 0; i < size; ++i ) {
                                place[i] = static_cast< uint8_t >( item & 0xFF );
                                item     = static_cast< T >( item >> 8 );
                        }
                }
        }

        template < endianess_enum Endianess = serialization_endianess< T >, typename Iterator >
        static T deserialize( static_view< size, Iterator > place )
        {
                T res{};
                // static_assert(
                //    std::is_same_v<
                //        std::decay_t< typename std::iterator_traits< Iterator >::value_type >,
                //        char > );
                auto iter =
                    Endianess == endianess_enum::LOW ? place.begin() : std::prev( place.end() );
                for ( std::size_t i = 0; i < size; ++i ) {
                        res = static_cast< T >( res << 8 );
                        res = static_cast< T >( res | *iter );
                        if ( Endianess == endianess_enum::LOW ) {
                                iter++;
                        } else {
                                iter--;
                        }
                }
                return res;
        }
};

template < typename T, std::size_t N >
struct serializer< std::array< T, N > >
{
        static constexpr std::size_t size = serializer< T >::size * N;

        template < endianess_enum Endianess = serialization_endianess< T > >
        static constexpr void
        serialize( static_view< size, uint8_t* > place, std::array< T, N > data )
        {
                auto parts = place.template split< N >();
                for ( std::size_t i = 0; i < N; ++i ) {
                        serializer< T >::template serialize< Endianess >( parts[i], data[i] );
                }
        }

        template < endianess_enum Endianess = serialization_endianess< T > >
        static std::array< T, N > deserialize( static_view< size, const uint8_t* > place )
        {
                std::array< T, N > data;
                auto               parts = place.template split< N >();
                for ( std::size_t i = 0; i < N; ++i ) {
                        data[i] = serializer< T >::template deserialize< Endianess >( parts[i] );
                }
                return data;
        }
};

template < std::size_t N, typename... Ts >
constexpr std::array< uint8_t, N > serialize_to( Ts... items )
{
        std::array< uint8_t, N > res = {};
        auto                     places_tpl =
            make_static_view( res ).template segmentize< serializer< Ts >::size... >();

        auto f = [&]( auto place, auto item ) {
                using ser = serializer< decltype( item ) >;

                ser::serialize( place, item );
        };

        std::apply(
            [&]( auto... places ) {
                    ( f( places, items ), ... );
            },
            places_tpl );

        return res;
}

template < typename... Ts, std::size_t N = ( serializer< Ts >::size + ... + 0 ) >
constexpr std::array< uint8_t, N > serialize( Ts... items )
{
        return serialize_to< N >( items... );
}

}  // namespace schpin
