// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <array>
#include <optional>
#include <tuple>

#pragma once

namespace schpin
{
template < std::size_t, typename >
class static_view;

template < std::size_t N, typename Iterator >
class static_view
{
        Iterator begin_ = {};

        constexpr explicit static_view( Iterator begin )
          : begin_( std::move( begin ) )
        {
        }

        constexpr static_view() = default;

        template < typename T, std::size_t M >
        friend class std::array;

public:
        template < typename OtherIter >
        constexpr static_view( static_view< N, OtherIter > other )
          : begin_( other.begin_ )
        {
        }

        template < std::size_t M, std::enable_if_t< M >= N >* = nullptr >
        constexpr static_view( static_view< M, Iterator > other )
          : begin_( other.begin_ )
        {
        }

        Iterator begin()
        {
                return begin_;
        }

        Iterator end()
        {
                return begin_ + N;
        }

        constexpr auto& operator[]( std::size_t i )
        {
                return *( begin_ + i );
        }

        template < std::size_t... Seg >
        constexpr std::tuple< static_view< Seg, Iterator >... > segmentize() const
        {
                return segmentize< Seg... >( std::make_index_sequence< sizeof...( Seg ) >() );
        }

        template < std::size_t Size >
        constexpr std::array< static_view< N / Size, Iterator >, Size > split() const
        {
                std::array< static_view< N / Size, Iterator >, Size > res = {};
                for ( std::size_t i = 0; i < N; ++i ) {
                        res[i] = static_view< N / Size, Iterator >( begin_ + i * N / Size );
                }
                return res;
        }

        template < typename T >
        static constexpr static_view< N, T* > make_static_view( std::array< T, N >& data )
        {
                return static_view< N, T* >{ data.begin() };
        }

        template < typename Container >
        static std::optional< static_view< N, Iterator > > try_static_view( Container&& cont )
        {
                if ( cont.size() < N ) {
                        return {};
                }
                return { static_view< N, Iterator >{ cont.begin() } };
        }

        static std::optional< static_view< N, Iterator > >
        try_static_view( Iterator begin, Iterator end )
        {
                if ( std::distance( begin, end ) < static_cast< long int >( N ) ) {
                        return {};
                }
                return { static_view< N, Iterator >{ begin } };
        }

private:
        template < std::size_t M >
        static constexpr auto make_indexes( std::array< std::size_t, M > segs )
        {
                std::array< std::size_t, M > res{};
                for ( std::size_t i = 1; i < M; i++ ) {
                        res[i] = segs[i - 1] + res[i - 1];
                }
                return res;
        }

        template < std::size_t... Seg, std::size_t... Is >
        constexpr auto segmentize( std::index_sequence< Is... > ) const
        {
                auto indexes =
                    make_indexes( std::array< std::size_t, sizeof...( Seg ) >( { Seg... } ) );

                (void) ( indexes );

                return std::make_tuple( static_view< Seg, Iterator >( begin_ + indexes[Is] )... );
        }

        template < std::size_t OtherSize, typename OtherIter >
        friend class static_view;

        template < typename Container >
        friend std::optional< static_view< N, Iterator > > try_static_view( Container&& cont );
};

template < std::size_t N, typename T >
constexpr static_view< N, T* > make_static_view( std::array< T, N >& data )
{
        return static_view< N, T* >::make_static_view( data );
}

template < std::size_t N, typename Iterator >
std::optional< static_view< N, Iterator > > try_static_view( Iterator begin, Iterator end )
{
        return static_view< N, Iterator >::try_static_view( begin, end );
}

template <
    std::size_t N,
    typename Container,
    typename Iterator = decltype( std::declval< Container >().begin() ) >
std::optional< static_view< N, Iterator > > try_static_view( Container&& cont )
{
        return static_view< N, Iterator >::try_static_view( cont );
}

}  // namespace schpin
