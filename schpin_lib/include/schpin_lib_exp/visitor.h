// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/algorithm.h>

#pragma once

namespace schpin
{

template < typename Function, typename... Tags >
struct visitor_callback
{
        using tag_tuple = std::tuple< Tags... >;

        Function f;
};

template < typename... Callbacks >
class visitor_driver
{
        using callbacks_tuple = std::tuple< Callbacks... >;

        callbacks_tuple callbacks_;

public:
        visitor_driver( Callbacks... cbs )
          : callbacks_( std::move( cbs )... )
        {
        }

        template < typename Tag, typename... Args >
        decltype( auto ) call( Args&&... args )
        {
                em::for_each( callbacks_, [&]< typename Callback >( Callback& cb ) {
                        if constexpr ( em::tuple_has_type_v< Tag, typename Callback::tag_tuple > ) {
                                cb.f( std::forward< Args >( args )... );
                        }
                } );
        }
};

}  // namespace schpin
