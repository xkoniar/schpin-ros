// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/algorithm.h>
#include <visualization_msgs/msg/marker.hpp>
#include <visualization_msgs/msg/marker_array.hpp>

#pragma once

namespace schpin
{

class marker_manager
{
        std::vector< std::pair< std::string, int > > robot_active_markers_{};
        rclcpp::Publisher< visualization_msgs::msg::MarkerArray >::SharedPtr marker_array_pub_;

public:
        marker_manager( rclcpp::Node& rcl_node )
          : marker_array_pub_( rcl_node.create_publisher< visualization_msgs::msg::MarkerArray >(
                "/visualization_marker_array",
                10 ) )
        {
        }

        void publish( const visualization_msgs::msg::MarkerArray& msg )
        {
                marker_array_pub_->publish( msg );
                for ( const visualization_msgs::msg::Marker& submsg : msg.markers ) {
                        robot_active_markers_.emplace_back( submsg.ns, submsg.id );
                }
        }

        void clear()
        {

                visualization_msgs::msg::MarkerArray arr;
                arr.markers =
                    map_f_to_v( robot_active_markers_, [&]( std::tuple< std::string, int > pack ) {
                            auto [ns, id] = pack;
                            visualization_msgs::msg::Marker msg;
                            msg.ns     = std::move( ns );
                            msg.id     = id;
                            msg.action = visualization_msgs::msg::Marker::DELETE;
                            return msg;
                    } );
                marker_array_pub_->publish( arr );
        }

        ~marker_manager()
        {
                clear();
        }
};

inline visualization_msgs::msg::Marker
create_marker( const std::string& ns, unsigned id, const std::string& frame )
{
        visualization_msgs::msg::Marker res;

        res.action          = visualization_msgs::msg::Marker::ADD;
        res.frame_locked    = 1u;
        res.header.frame_id = frame;
        // res.header.stamp    = ros::Time(); TODO: is this needed?
        res.ns = ns;
        res.id = int( id );

        res.pose.position.x    = 0;
        res.pose.position.y    = 0;
        res.pose.position.z    = 0;
        res.pose.orientation.x = 0.0;
        res.pose.orientation.y = 0.0;
        res.pose.orientation.z = 0.0;
        res.pose.orientation.w = 1.0;

        res.color.a = 1.f;

        res.lifetime = rclcpp::Duration( 0 );

        return res;
}

}  // namespace schpin
