
#pragma once

namespace schpin::stringify
{
template < typename... Args >
inline std::ostream& operator<<( std::ostream& os, const std::variant< Args... >& var );

template < typename... Args >
inline std::ostream& operator<<( std::ostream& os, const std::tuple< Args... >& var )
{
        os << "{";
        char del = ' ';
        em::for_each( var, [&]( const auto& item ) {
                os << del << item;
                del = ',';
        } );
        os << " }";
        return os;
}

template < typename... Args >
inline std::ostream& operator<<( std::ostream& os, const std::variant< Args... >& var )
{
        os << "[ ";
        em::visit(
            [&]( const auto& item ) {
                    os << item;
            },
            var );
        os << "]";
        return os;
}
}  // namespace schpin::stringify
