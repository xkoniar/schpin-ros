// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <emlabcpp/protocol/json.h>
#include <emlabcpp/quantity.h>

#pragma once

namespace schpin
{

template < typename Tag, typename T, typename Scale >
class scaled : public em::quantity< scaled< Tag, T, Scale >, T >
{
public:
        using scale = Scale;
        using em::quantity< scaled< Tag, T, Scale >, T >::quantity;

        static constexpr scaled from_double( double v )
        {
                return scaled{ static_cast< T >(
                    static_cast< double >( scale::den ) * v /
                    static_cast< double >( scale::num ) ) };
        }

        static constexpr scaled from_float( float v )
        {
                return scaled{ static_cast< T >(
                    static_cast< float >( scale::den ) * v / static_cast< float >( scale::num ) ) };
        }

        float as_float() const
        {
                return { scale::num * static_cast< float >( *this ) / scale::den };
        }

        static std::string get_unit()
        {
                return "";
        }
};

template < typename Tag, typename T, typename Scale >
inline std::ostream& operator<<( std::ostream& os, scaled< Tag, T, Scale > sc )
{
        return os << *sc << "(" << sc.as_float() << ")";
}

#ifdef SCHPIN_USE_NLOHMANN_JSON

template < typename Tag, typename T, typename Scale >
inline void from_json( const nlohmann::json& j, scaled< Tag, T, Scale >& s )
{
        s = scaled< Tag, T, Scale >::from_float( j.get< float >() );
}

template < typename Tag, typename T, typename Scale >
inline void to_json( nlohmann::json& j, const scaled< Tag, T, Scale >& s )
{
        j = s.as_float();
}

#endif

}  // namespace schpin

#ifdef EMLABCPP_USE_NLOHMANN_JSON
namespace emlabcpp
{

template < typename Tag, typename T, typename Scale >
struct protocol_json_serializer< schpin::scaled< Tag, T, Scale > > : protocol_json_serializer_base
{
        static constexpr std::string_view type_name = "scaled";

        static std::string get_name()
        {
                return protocol_json_serializer< T >::get_name();
        }

        static void add_extra( nlohmann::json& j )
        {
                j["sub_type"] = protocol_decl< T >{};
                j["num"]      = Scale::num;
                j["den"]      = Scale::den;
        }
};
}  // namespace emlabcpp
#endif
