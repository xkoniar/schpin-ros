// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/algorithm.h"
#include "schpin_lib/bounded.h"
#include "schpin_lib/error.h"
#include "schpin_lib/print.h"
#include "schpin_lib_exp/algorithm.h"
#include "schpin_lib_exp/scaled.h"
#include "schpin_lib_exp/sized_array.h"

#include <emlabcpp/physical_quantity.h>
#include <filesystem>
#include <magic_enum.hpp>
#include <optional>
#include <rclcpp/rclcpp.hpp>

#pragma once

namespace schpin
{

using config_error = error< struct config_errorTag >;

inline std::ostream& operator<<( std::ostream& os, const config_error& e )
{
        return os << simplified_error{ e };
}

template < typename T >
struct ros_param_serializer
{
        static em::either< T, config_error > load( rclcpp::Node& node, const std::string& prefix )
        {
                auto arg_tuple = T::ros_param_get();

                return std::apply(
                    [&]( auto... item ) {
                            return em::assemble_left_collect_right( item.load( node, prefix )... )
                                .convert_left( [&]( auto value_tpl ) {
                                        return std::apply(
                                            [&]( auto... values ) {
                                                    return T{ values... };
                                            },
                                            value_tpl );
                                } )
                                .convert_right( [&]( const auto& errors ) {
                                        return E( config_error ).group_attach( errors );
                                } );
                    },
                    arg_tuple );
        }
};

template < em::quantity_derived T >
struct ros_param_serializer< T >
{
        static em::either< T, config_error > load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< typename T::value_type >::load( node, prefix )
                    .template construct_left< T >();
        }
};

template < vec_point_derived T >
struct ros_param_serializer< T >
{
        static em::either< T, config_error > load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< typename T::container >::load( node, prefix )
                    .template construct_left< T >();
        }
};

template < typename T, T FromVal, T ToVal >
struct ros_param_serializer< em::bounded< T, FromVal, ToVal > >
{
        static em::either< em::bounded< T, FromVal, ToVal >, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< T >::load( node, prefix )
                    .bind_left(
                        [&]( T val )
                            -> em::either< em::bounded< T, FromVal, ToVal >, config_error > {
                                auto opt_val = em::bounded< T, FromVal, ToVal >::make( val );
                                if ( opt_val ) {
                                        return { *opt_val };
                                }
                                return { E( config_error ) << "Bounded value out of range" };
                        } );
        }
};

template < typename T >
struct ros_param_serializer< std::map< std::string, T > >
{
        static em::either< std::map< std::string, T >, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                auto params_and_prefixes = node.list_parameters( { prefix }, 20 );
                auto eithers =
                    map_f_to_v( params_and_prefixes.prefixes, [&]( const std::string& name ) {
                            return ros_param_serializer< T >::load( node, name )
                                .convert_left( [&]( T item ) {
                                        std::string used_name = name.substr( prefix.length() + 1 );
                                        return std::make_pair( used_name, item );
                                } );
                    } );

                auto parted = partition_eithers( eithers );
                if ( !parted.right.empty() ) {
                        return {
                            E( config_error ).group_attach( parted.right ) << "Failed to load "
                                                                              "map" };
                }

                std::map< std::string, T > map;
                for ( auto pair : parted.left ) {
                        map.insert( pair );
                }

                return { map };
        }
};

template < typename OutputType, typename SerializationType = OutputType >
struct ros_param_serializer_basic
{
        static em::either< OutputType, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                auto param = node.get_parameter( prefix );
                if ( param.get_type() == rclcpp::PARAMETER_NOT_SET ) {
                        return { E( config_error ) << "Parameter " << prefix << " is missing" };
                }
                return {
                    static_cast< OutputType >( param.template get_value< SerializationType >() ) };
        }
};

template < typename T >
concept native_ros_parameter_type = requires( rclcpp::ParameterValue pv )
{
        pv.get< T >();
};

template < native_ros_parameter_type T >
struct ros_param_serializer< T > : ros_param_serializer_basic< T >
{
};

template <>
struct ros_param_serializer< uint8_t > : ros_param_serializer_basic< uint8_t, int >
{
};
template <>
struct ros_param_serializer< int8_t > : ros_param_serializer_basic< int8_t, int >
{
};
template <>
struct ros_param_serializer< uint16_t > : ros_param_serializer_basic< uint16_t, int >
{
};
template <>
struct ros_param_serializer< int16_t > : ros_param_serializer_basic< int16_t, int >
{
};
template <>
struct ros_param_serializer< uint32_t > : ros_param_serializer_basic< uint32_t, int >
{
};
template <>
struct ros_param_serializer< int32_t > : ros_param_serializer_basic< int32_t, int >
{
};

// TODO: vector handling is mess
template < typename To, typename From >
struct ros_param_serializer_vec
{
        static em::either< std::vector< To >, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< std::vector< From > >::load( node, prefix )
                    .convert_left( [&]( auto v ) {
                            return map_f_to_v( v, [&]( From val ) {
                                    return static_cast< To >( val );
                            } );
                    } );
        }
};

template <>
struct ros_param_serializer< std::vector< float > > : ros_param_serializer_vec< float, double >
{
};

template <>
struct ros_param_serializer< std::vector< uint16_t > >
  : ros_param_serializer_vec< uint16_t, int64_t >
{
};

template < typename T, typename SizeType >
struct ros_param_serializer< sized_array< T, SizeType > >
{
        static em::either< sized_array< T, SizeType >, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< typename sized_array< T, SizeType >::container >::load(
                           node, prefix )
                    .template construct_left< sized_array< T, SizeType > >();
        }
};

template < typename T, std::size_t N >
struct ros_param_serializer< std::array< T, N > >
{
        static em::either< std::array< T, N >, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< std::vector< T > >::load( node, prefix )
                    .bind_left(
                        [&](
                            std::vector< T > v ) -> em::either< std::array< T, N >, config_error > {
                                if ( v.size() != N ) {
                                        return E( config_error )
                                               << "Data is of wrong size, should be " << N;
                                }
                                std::array< T, N > res;
                                std::copy( v.begin(), v.end(), res.begin() );
                                return res;
                        } );
        }
};

template < typename Tag, typename T, typename Scale >
struct ros_param_serializer< scaled< Tag, T, Scale > >
{
        static em::either< scaled< Tag, T, Scale >, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< float >::load( node, prefix )
                    .convert_left( [&]( float val ) {
                            return scaled< Tag, T, Scale >::from_float( val );
                    } );
        }
};

template < typename Tag, typename T, typename Scale >
struct ros_param_serializer< std::vector< scaled< Tag, T, Scale > > >
{
        static em::either< std::vector< scaled< Tag, T, Scale > >, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< std::vector< float > >::load( node, prefix )
                    .convert_left( [&]( auto v ) {
                            return map_f_to_v( v, [&]( float val ) {
                                    return scaled< Tag, T, Scale >::from_float( val );
                            } );
                    } );
        }
};

template <>
struct ros_param_serializer< std::filesystem::path >
{
        static em::either< std::filesystem::path, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< std::string >::load( node, prefix )
                    .template construct_left< std::filesystem::path >();
        }
};

template < typename T >
struct ros_param_serializer< std::optional< T > >
{
        static em::either< std::optional< T >, config_error >
        load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< T >::load( node, prefix )
                    .convert_left( [&]( T val ) {
                            return std::make_optional< T >( val );
                    } )
                    .bind_right( [&]( auto ) -> em::either< std::optional< T >, config_error > {
                            return { std::optional< T >{} };
                    } );
        }
};

template < typename T >
requires std::is_enum_v< T >
struct ros_param_serializer< T >
{
        static em::either< T, config_error > load( rclcpp::Node& node, const std::string& prefix )
        {
                return ros_param_serializer< std::string >::load( node, prefix )
                    .bind_left( [&]( std::string val ) -> em::either< T, config_error > {
                            auto eval = magic_enum::enum_cast< T >( val );
                            if ( !eval.has_value() ) {
                                    return E( config_error )
                                               .group_attach( magic_enum::enum_names< T >() )
                                           << "String " << val
                                           << " is not valid enum value, see list for valid values:";
                            }
                            return eval.value();
                    } );
        }
};

template < typename T >
class ros_param
{
        std::string parameter_name_;
        std::string description_;

public:
        ros_param( std::string parameter_name, std::string description )
          : parameter_name_( std::move( parameter_name ) )
          , description_( std::move( description ) )
        {
        }

        em::either< T, config_error > load( rclcpp::Node& node, const std::string& prefix = "" )
        {
                std::string sub_prefix = prefix;
                if ( sub_prefix != "" ) {
                        sub_prefix += ".";
                }
                return ros_param_serializer< T >::load( node, sub_prefix + parameter_name_ )
                    .convert_right( [&]( config_error e ) -> config_error {
                            return E( config_error ).attach( e )
                                   << "Failed to get config parameter: " << term_color::MAGENTA
                                   << sub_prefix + parameter_name_ << term_color::RESET
                                   << " desc: " << description_;
                    } );
        }
};

// TODO diry hack given that ROS2 can't have optional parameters...
template < typename T >
struct opt_ros_param_avoid_default : ros_param< std::optional< T > >
{
        T def_val_;

public:
        opt_ros_param_avoid_default( std::string name, std::string desc, T def_val )
          : ros_param< std::optional< T > >( name, desc )
          , def_val_( def_val )
        {
        }

        em::either< std::optional< T >, config_error >
        load( rclcpp::Node& node, const std::string& prefix = "" )
        {
                return ros_param< std::optional< T > >::load( node, prefix )
                    .convert_left( [&]( std::optional< T > val ) {
                            if ( !val ) {
                                    return val;
                            }
                            if ( *val == def_val_ ) {
                                    val.reset();
                            }
                            return val;
                    } );
        }
};

template < typename T >
class ros_opt_param
{
        std::string name_;
        std::string description_;
        T           default_;

public:
        ros_opt_param( std::string name, std::string description, T def )
          : name_( std::move( name ) )
          , description_( std::move( description ) )
          , default_( def )
        {
        }

        em::either< T, config_error > load( rclcpp::Node& node, const std::string& prefix = "" )
        {
                std::string sub_prefix = prefix;
                if ( sub_prefix != "" ) {
                        sub_prefix += ".";
                }
                return ros_param_serializer< T >::load( node, sub_prefix + name_ )
                    .bind_right( [&]( const config_error& e ) -> em::either< T, config_error > {
                            SCHPIN_INFO_LOG(
                                "config",
                                "Parameter " << name_ << " was not loaded due to: " << e );
                            return default_;
                    } );
        }
};

template < typename Config >
inline em::either< Config, config_error >
get_config( rclcpp::Node& node, const std::string& prefix = "" )
{
        return ros_param_serializer< Config >::load( node, prefix );
}

}  // namespace schpin
