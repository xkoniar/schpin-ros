// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/algorithm.h"

#include <functional>
#include <iostream>
#include <queue>
#include <tuple>
#include <variant>

#pragma once

namespace schpin
{

enum class event_priority
{
        CRITICAL = 0,
        HIGH     = 1,
        DEFAULT  = 2,
        LOW      = 3
};
template < typename Tag, typename... Events >
struct reactor_event_group
{
        using variant = std::variant< Events... >;

        variant        sub_cmd;
        event_priority priority;

        reactor_event_group( variant v )
          : sub_cmd( v )
          , priority( std::visit(
                [&]( auto& sub ) {
                        return sub.priority;
                },
                v ) )
        {
        }
};

template < typename... Events >
class event_reactor
{
public:
        static constexpr std::size_t priority_cardinality = 4;

        using event_variant = std::variant< Events... >;

private:
        std::array< std::queue< event_variant >, priority_cardinality > queues_;

public:
        template < typename ActualEvent >
        void insert( ActualEvent e )
        {
                queues_[static_cast< unsigned >( e.priority )].push(
                    event_variant{ std::forward< ActualEvent >( e ) } );
        }

        std::optional< event_variant > pop()
        {
                auto qiter = em::find_if( queues_, [&]( const auto& queue ) {
                        return !queue.empty();
                } );

                if ( qiter == queues_.end() ) {
                        return {};
                }

                std::optional< event_variant > res{ qiter->front() };
                qiter->pop();
                return res;
        }
};
}  // namespace schpin
