// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/xml.h"
#include "schpin_lib_exp/algorithm.h"

#pragma once

namespace schpin
{

struct srdf_leg
{
        std::string name;
        std::string root_joint;
};

struct srdf_information
{
        std::vector< srdf_leg > legs_info;
        std::string             body_link;
};

inline em::either< srdf_information, xml_storage_error > load_srdf_info( const std::string& srdf )
{
        std::stringstream srdf_istream{ srdf };
        return safe_load_xml< srdf_information >(
            srdf_istream,
            [&]( const pt::ptree& tree ) -> em::either< srdf_information, xml_storage_error > {
                    auto leg_group_iter = em::find_if(
                        tree.get_child( "robot" ),
                        [&]( std::pair< std::string, const pt::ptree > pack ) {
                                return pack.second.get< std::string >( "<xmlattr>.name", "" ) ==
                                       "legs";
                        } );

                    if ( leg_group_iter == tree.get_child( "robot" ).end() ) {
                            return {
                                E( xml_storage_error ) << "SRDF XML file does not have group "
                                                          "with name 'legs'" };
                    }

                    std::vector< srdf_leg > legs_info = filter_optionals(
                        leg_group_iter->second,
                        [&]( std::pair< std::string, pt::ptree > pack )
                            -> std::optional< srdf_leg > {
                                const auto& [key, child] = pack;
                                if ( key != "leg" ) {
                                        return {};
                                }
                                return { srdf_leg{
                                    child.get< std::string >( "<xmlattr>.name" ),
                                    child.get< std::string >( "<xmlattr>.root_joint" ) } };
                        } );
                    // TODO: body link name should be get from the srdf not hardcoded...
                    return { srdf_information{ legs_info, "base_link" } };
            } );
}

}  // namespace schpin
