
#pragma once

namespace schpin
{

template < typename >
struct tuple_variant_serializer;

template < typename... Ts >
struct tuple_variant_serializer< std::variant< Ts... > >
{
        template < typename T >
        struct tag
        {
                using value_type = T;
        };

        static void to_json( nlohmann::json& j, const std::variant< Ts... >& var )
        {
                j = nlohmann::json::array();
                std::visit(
                    [&]( const auto& tpl ) {
                            // TODO: this has to exists because fuckin nlohmann serializes to
                            // std::tuple of size 2 intoobjeck with single key/val instance ;(
                            em::for_each( tpl, [&]( const auto& subitems ) {
                                    j.push_back( subitems );
                            } );
                    },
                    var );
        }

        static std::variant< Ts... > from_json( const nlohmann::json& j )
        {
                std::optional< std::variant< Ts... > > opt_res;

                auto f = [&]< typename Tag >( Tag ) {
                        using T = typename Tag::value_type;
                        try {
                                opt_res = std::variant< Ts... >{ j.get< T >() };
                                return true;
                        }
                        catch ( ... ) {
                                return false;
                        }
                };

                ( f( tag< Ts >{} ) || ... || false );
                if ( !opt_res ) {
                        throw nlohmann::json::parse_error::create(
                            -1, 0, "no variant alternative can be parsed" );
                }
                return *opt_res;
        }
};

}  // namespace schpin
