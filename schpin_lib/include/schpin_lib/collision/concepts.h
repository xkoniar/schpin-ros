#include "schpin_lib/geom/point.h"

#pragma once

namespace schpin
{

template < typename Q >
concept sat_collision_query = requires( const Q& q )
{
        { q.points() };
        { q.separation_axes() };
        { q.intersection_axes() };
};

template < typename Q >
concept bs_collision_query = requires( Q q )
{
        {
                q.min_sphere_radius()
                } -> std::same_as< em::radius >;
        {
                q.min_sphere_position()
                } -> std::same_as< const point< 3, typename Q::tag >& >;
};

template < typename Q >
concept tree_collision_query = requires( Q q )
{
        {
                q.is_leaf()
                } -> std::same_as< bool >;
        { q.children() };
        {
                q.descent_priority()
                } -> std::same_as< float >;
};

}  // namespace schpin
