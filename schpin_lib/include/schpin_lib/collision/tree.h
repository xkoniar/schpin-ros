#include "schpin_lib/collision/concepts.h"

#pragma once

namespace schpin
{
// TODO: I think I use different comment styles accross entire codebase ...
/* Meta function to check collision between queries that represent trees.
 * Query objects represent trees and are traversed to find a pair of leafs
 * from each tree that are in collision. The traversion is in DFS-like manner.
 *
 * lh,rh represents the query objects for trees
 * col_f is a function that checks the collision
 * leaf_f is a function that checks the collision for leafs
 *
 * This function should not be used directly, it's ment as a tool to implement
 * specific functions for tree collision detection.
 */
template <
    tree_collision_query LH,
    tree_collision_query RH,
    typename CollisionFunction,
    typename LeafFunction >
[[gnu::hot]] constexpr bool trees_collides(
    const LH&           lh,
    const RH&           rh,
    CollisionFunction&& col_f,
    LeafFunction&& leaf_f ) noexcept( noexcept( col_f( lh, rh ) ) && noexcept( leaf_f( lh, rh ) ) )
{
        if ( !col_f( lh, rh ) ) {
                return false;
        }

        bool step_down_right = false;

        if ( lh.is_leaf() && rh.is_leaf() ) {
                return leaf_f( lh, rh );
        } else if ( lh.is_leaf() ) {
                step_down_right = true;
        } else if ( rh.is_leaf() ) {
                step_down_right = false;
        } else {
                step_down_right = lh.descent_priority() < rh.descent_priority();
        }

        if ( step_down_right ) {
                return em::any_of( rh.children(), [&]( const auto& child ) {
                        return trees_collides( lh, child, col_f, leaf_f );
                } );
        } else {
                return em::any_of( lh.children(), [&]( const auto& child ) {
                        return trees_collides( child, rh, col_f, leaf_f );
                } );
        }
}
}  // namespace schpin
