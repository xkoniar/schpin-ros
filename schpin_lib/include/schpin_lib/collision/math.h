#include "schpin_lib/collision/concepts.h"

#pragma once

namespace schpin
{
/* Calculates the smallest and biggest distance of all points from 'T' projected on axis. These are
 * all necessary points for Separation Axis Test. The distances are between the projected points and
 * [0,0,0] coordinate on that axis. The axis is defined only by it's direction.
 *
 * O(n), where 'n' is number of points
 */

template < sat_collision_query T, std::size_t N >
[[gnu::hot]] constexpr em::min_max< em::distance >
projection_range( const T& t, const vec< N >& axis_direction ) noexcept
{
        return em::min_max_elem( t.points(), [&]( const auto& point ) {
                return axis_projection_distance( point, axis_direction );
        } );
}

/* Checks if the provided axis detects an separation between 'LH' and 'RH' objects. The axis is
 * defined only by it's direction. All points of each object are projected on same axis and in case
 * they do not overlap, the two objects are not in collision - the separation was detected.
 *
 * O(n+k), where 'n' us number of points for 'LH' and 'k' is number of points for 'RH'.
 */
template < bool CanIntersect, sat_collision_query LH, sat_collision_query RH, std::size_t N >
[[gnu::hot]] constexpr bool
axis_separates( const LH& lh, const RH& rh, const vec< N >& axis_direction ) noexcept
{
        em::min_max< em::distance > lh_range   = projection_range( lh, axis_direction );
        em::min_max< em::distance > rh_range   = projection_range( rh, axis_direction );
        bool                        no_overlap = true;
        if constexpr ( CanIntersect ) {
                no_overlap = lh_range.min >= rh_range.max || rh_range.min >= lh_range.max;
        } else {
                no_overlap = lh_range.min > rh_range.max || rh_range.min > lh_range.max;
        }
        // https://stackoverflow.com/questions/3269434/whats-the-most-efficient-way-to-test-two-integer-ranges-for-overlap

        return no_overlap;
}

/* Checks collision between 'LH' and 'RH' objects. The Separation Axis Test is used and only axes of
 * the first object are considered. For each axis, the axisSeparates function is used. The method
 * returns true in case that not a single axis separates the objects. In case at least one axis
 * separates the objects, false is returned.
 *
 * O(a*n), where 'a' is number of axis of 'LH' and 'n' is the sum of points provided by 'LH' and
 * 'RH'
 */
template < sat_collision_query LH, sat_collision_query RH >
[[gnu::hot]] constexpr bool collides_single_axes( const LH& lh, const RH& rh ) noexcept
{
        return em::none_of(
                   lh.separation_axes(),
                   [&]< std::size_t N >( const vec< N >& axis_direction ) {
                           return axis_separates< false >( lh, rh, axis_direction );
                   } ) &&
               em::none_of(
                   lh.intersection_axes(), [&]< std::size_t N >( const vec< N >& axis_direction ) {
                           return axis_separates< true >( lh, rh, axis_direction );
                   } );
}
}  // namespace schpin
