// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/util.h"

#include <boost/integer.hpp>
#include <nlohmann/json.hpp>

#pragma once

namespace schpin
{
/** Class designed to create a identificator data-type for data containers.
 *
 *
 * The class derived than stores the unsigned value passed during the creation and allows for
 * modification by suffix and preffix operators --/++. There is also a full set of operators
 * overloads to compare identificators of same type. The actual value can em::either by accessed
 * with the operator*() method or the object can explicitly be converted into std::size_t. This is
 * considered enough for identificator of container.
 *
 * Various parts of the class have asserts for integer overflow or underflow.
 *
 */
template < typename Tag, unsigned Bits >
class identificator
{

public:
        static constexpr unsigned bits = Bits;
        using value_type      = typename boost::uint_t< static_cast< int >( bits ) >::least;
        using difference_type = typename boost::int_t< static_cast< int >( bits + 1 ) >::least;

private:
        value_type value_;

public:
        /** Constructors, the default should always be 0 */
        constexpr identificator()
          : value_( 0 )
        {
        }

        /** Primary constructor for usage, this is able to use maximal size of unsigned integer and
         * use an assert to make sure that it is within range of actually used data type. */
        constexpr identificator( uintmax_t val )
          : value_( value_type( val ) )
        {
        }

        /** Manual conversion of the object into std::size_t */
        constexpr explicit operator std::size_t() const
        {
                return std::size_t( value_ );
        }

        /** Accessor for the internal value, only const version is available */
        constexpr const value_type& operator*() const
        {
                return value_;
        }

        /** increment/decrement operators */
        // -----------------------------------------------------------------
        constexpr identificator& operator++()
        {
                value_ += 1;
                return *this;
        }
        constexpr identificator& operator--()
        {
                value_ -= 1;
                return *this;
        }

        constexpr identificator operator++( int )
        {
                identificator tmp( *this );

                operator++();

                return tmp;
        }

        constexpr identificator operator--( int )
        {
                identificator tmp( *this );

                operator--();

                return tmp;
        }
        //-------------------------------------------------------------------
};

template < typename Tag, unsigned Bits >
void to_json( nlohmann::json& j, const identificator< Tag, Bits >& id )
{
        j = *id;
}

template < typename Tag, unsigned Bits >
void from_json( const nlohmann::json& j, identificator< Tag, Bits >& id )
{
        id = identificator< Tag, Bits >{ j.get< unsigned long >() };
}

// Definition of operators for the identificaotrs
// ------------------------------------------------------------------
// First set defines basic operators operators

template < typename Tag, unsigned Bits >
inline std::ostream& operator<<( std::ostream& os, const identificator< Tag, Bits >& id )
{
        return os << std::to_string( *id );
}

template < typename Tag, unsigned Bits >
constexpr typename identificator< Tag, Bits >::difference_type
operator-( const identificator< Tag, Bits >& lhs, const identificator< Tag, Bits >& rhs )
{
        using difference_type = typename identificator< Tag, Bits >::difference_type;
        return difference_type( *lhs ) - difference_type( *rhs );
}

template < typename Tag, unsigned Bits >
constexpr bool
operator<( const identificator< Tag, Bits >& lhs, const identificator< Tag, Bits >& rhs )
{
        return *lhs < *rhs;
}

template < typename Tag, unsigned Bits >
constexpr bool
operator==( const identificator< Tag, Bits >& lhs, const identificator< Tag, Bits >& rhs )
{
        return *lhs == *rhs;
}

// Second set is defined solely based on the firt set
template < typename Tag, unsigned Bits >
constexpr bool
operator!=( const identificator< Tag, Bits >& lhs, const identificator< Tag, Bits >& rhs )
{
        return !( lhs == rhs );
}

template < typename Tag, unsigned Bits >
constexpr bool
operator>( const identificator< Tag, Bits >& lhs, const identificator< Tag, Bits >& rhs )
{
        return rhs < lhs;
}

template < typename Tag, unsigned Bits >
constexpr bool
operator<=( const identificator< Tag, Bits >& lhs, const identificator< Tag, Bits >& rhs )
{
        return !( lhs > rhs );
}

template < typename Tag, unsigned Bits >
constexpr bool
operator>=( const identificator< Tag, Bits >& lhs, const identificator< Tag, Bits >& rhs )
{
        return !( lhs < rhs );
}
}  // namespace schpin

template < typename Tag, unsigned Bits >
struct std::hash< schpin::identificator< Tag, Bits > >
{
        std::size_t operator()( const schpin::identificator< Tag, Bits >& id ) const
        {
                return std::hash< std::size_t >()( *id );
        }
};
