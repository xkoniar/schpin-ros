// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <array>
#include <cmath>

#pragma once

namespace schpin
{

constexpr float fac( float n )
{
        if ( n == 1 ) {
                return n;
        }

        return n * fac( n - 1 );
}

template < unsigned N >
struct detcalc
{
        static constexpr float determinant( const std::array< float, N * N >& matrix )
        {
                std::array< float, ( N - 1 ) * ( N - 1 ) > submatrix{};
                float                                      res = 0;
                for ( unsigned i = 0; i < N; ++i ) {
                        for ( unsigned x = 0; x < N - 1; ++x ) {
                                for ( unsigned y = 1; y < N; ++y ) {
                                        if ( x < i ) {
                                                submatrix.at( x + ( y - 1 ) * ( N - 1 ) ) =
                                                    matrix.at( x + y * N );
                                        } else {
                                                submatrix.at( x + ( y - 1 ) * ( N - 1 ) ) =
                                                    matrix.at( 1 + x + y * N );
                                        }
                                }
                        }
                        res += ( i % 2 == 0 ? 1 : -1 ) * matrix[i] *
                               detcalc< N - 1 >::determinant( submatrix );
                }
                return res;
        }
};

template <>
struct detcalc< 2 >
{
        static constexpr float determinant( const std::array< float, 2 * 2 >& matrix )
        {
                return matrix[0] * matrix[3] - matrix[1] * matrix[2];
        }
};

template < unsigned N >
constexpr float determinant( const std::array< float, N * N >& matrix )
{
        return detcalc< N >::determinant( matrix );
}

}  // namespace schpin
