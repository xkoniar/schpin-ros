// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/algorithm.h"

#include <algorithm>
#include <nlohmann/json.hpp>
#include <optional>
#ifdef SCHPIN_USE_STREAMS
#include <ostream>
#endif
#include <vector>

#pragma once

namespace em = emlabcpp;

namespace schpin
{

// Chain view is the value type of iterator iterated over the chain container.
// It provides references to the PLP segment of the chain - source_pin | link | target_pin.
template < typename Link, typename Pin >
struct chain_view
{
        Pin&  source_pin;
        Link& link;
        Pin&  target_pin;

        chain_view( Pin& source_pin, Link& link, Pin& target_pin )
          : source_pin( source_pin )
          , link( link )
          , target_pin( target_pin )
        {
        }

        // This makes it possible to convert non-const view to const!
        template < typename OtherLink, typename OtherPin >
        chain_view( const chain_view< OtherLink, OtherPin >& other )
          : source_pin( other.source_pin )
          , link( other.link )
          , target_pin( other.target_pin )
        {
                static_assert( std::is_same_v< std::decay_t< Link >, std::decay_t< OtherLink > > );
                static_assert( std::is_same_v< std::decay_t< Pin >, std::decay_t< OtherPin > > );
        }

        // operator->() is overloaded for cases when one wants to access the attributes of view via
        // iterator: iter->source_pin.
        chain_view* operator->()
        {
                return this;
        }

        const chain_view* operator->() const
        {
                return this;
        }
};

// Container used internally by the chain for links.
template < typename Link >
using chain_link_container = std::vector< Link >;

// Container used internally by the chain for pins.
template < typename Pin >
using chain_pin_container = std::vector< Pin >;

// Iterator over chain PLPLPLPLPLPLP where iterator provides view over one PLP section. Behaves like
// random access iterator
template < typename LinkIterator, typename PinIterator >
class chain_iterator
{
        using link_iterator = LinkIterator;
        using pin_iterator  = PinIterator;

public:
        using link =
            std::remove_reference_t< typename link_iterator::reference >;  // this preserves
                                                                           // constness of iterator
                                                                           // value
        using pin = std::remove_reference_t< typename pin_iterator::reference >;  // this preserves
                                                                                  // constness of
                                                                                  // iterator value
        using value_type        = chain_view< link, pin >;
        using difference_type   = typename link_iterator::difference_type;
        using pointer           = value_type*;
        using reference         = value_type&;
        using iterator_category = std::random_access_iterator_tag;

private:
        link_iterator link_iter_;
        pin_iterator  pin_iter_;

        // This is not correct
        template < typename OtherLinkIterator, typename OtherPinIterator >
        friend class chain_iterator;

public:
        constexpr chain_iterator() = default;

        // standard constructor for two pairs of iterators
        constexpr chain_iterator( link_iterator link_iter, pin_iterator pin_iter )
          : link_iter_( std::move( link_iter ) )
          , pin_iter_( std::move( pin_iter ) )
        {
        }

        // allow for conversion from non-const version to const verson
        template < typename OtherLinkIterator, typename OtherPinIterator >
        constexpr chain_iterator(
            const chain_iterator< OtherLinkIterator, OtherPinIterator >& other )
          : link_iter_( other.link_iter_ )
          , pin_iter_( other.pin_iter_ )
        {
        }

        constexpr chain_iterator( const chain_iterator& )     = default;
        constexpr chain_iterator( chain_iterator&& ) noexcept = default;
        constexpr chain_iterator& operator=( const chain_iterator& ) = default;
        constexpr chain_iterator& operator=( chain_iterator&& ) noexcept = default;

        // reference to internal value returns view over the PLP segment
        constexpr chain_view< link, pin > operator*()
        {
                return chain_view< link, pin >{ *pin_iter_, *link_iter_, *std::next( pin_iter_ ) };
        }
        // const reference to internal value returns const view over the PLP segment
        constexpr chain_view< const link, const pin > operator*() const
        {
                return chain_view< const link, const pin >{
                    *pin_iter_, *link_iter_, *std::next( pin_iter_ ) };
        }
        constexpr chain_view< link, pin > operator->()
        {
                return **this;
        }
        constexpr chain_view< const link, const pin > operator->() const
        {
                return **this;
        }
        // Advances iterator by 'off' forward
        constexpr chain_iterator& operator+=( difference_type off )
        {
                link_iter_ += off;
                pin_iter_ += off;
                return *this;
        }
        // Advances iterator from |PLP|LP segment to PL|PLP| segment
        constexpr chain_iterator& operator++()
        {
                ++link_iter_;
                ++pin_iter_;
                return *this;
        }
        // Advances iterator from |PLP|LP segment to PL|PLP| segment
        constexpr chain_iterator operator++( int )
        {
                chain_iterator copy( *this );
                ++( *this );
                return copy;
        }
        constexpr chain_iterator& operator-=( difference_type off )
        {
                link_iter_ -= off;
                pin_iter_ -= off;
                return *this;
        }
        constexpr chain_iterator& operator--()
        {
                --link_iter_;
                --pin_iter_;
                return *this;
        }
        constexpr chain_iterator operator--( int )
        {
                chain_iterator copy( *this );
                --( *this );
                return copy;
        }

        constexpr const link_iterator& get_link_iter() const
        {
                return link_iter_;
        }

        constexpr const pin_iterator& get_pin_iter() const
        {
                return pin_iter_;
        }

        ~chain_iterator() = default;
};

template < typename Link, typename Pin >
constexpr auto
operator-( const chain_iterator< Link, Pin >& lh, const chain_iterator< Link, Pin >& rh )
{
        return lh.get_link_iter() - rh.get_link_iter();
}

template < typename Link, typename Pin >
constexpr bool
operator==( const chain_iterator< Link, Pin >& lh, const chain_iterator< Link, Pin >& rh )
{
        return lh.get_link_iter() == rh.get_link_iter();
}

template < typename Link, typename Pin >
constexpr bool
operator!=( const chain_iterator< Link, Pin >& lh, const chain_iterator< Link, Pin >& rh )
{
        return !( lh == rh );
}

// Chain data structure allows a storage of two different data types - Link and Pin.
// These represent a sequence of Pins, where between each pin pair exists link P(LP)* (P, PLP,
// PLPLP...).
//
// Prime example of such a data set are paths in graphs, each pin represents a voxel and between
// each voxel on the path exists and ede - link. This makes it easier to work with such a data, than
// returning em::either two vectors (of edges and vertices) or returning just one set and gettin
// access to the other via some api (usually by returning a set of edges in case of path). Using
// this data structure for the task reduces a lot of code at place of wokring with the data.
//
// The iterator does not have standard value, rather it has a structure of three references as
// value_type - chain_view. The chain view provides reference to Pin|Link|Pin segment. Let's say we
// have P0,L0,P1,L1,P2,L3,....,Pn chain, the begin iterator provides reference to P0,L0,P1 and after
// advance of one step it provides references to P1,L0,P2 .. -> last pin at one step is first pin at
// another step.
//
// Invariant: For more than one pin, there is always exactly one less link than pins
template < typename Link, typename Pin >
class chain
{
        chain_link_container< Link > links_;
        chain_pin_container< Pin >   pins_;

        chain( chain_link_container< Link > links, chain_pin_container< Pin > pins )
          : links_( std::move( links ) )
          , pins_( std::move( pins ) )
        {
        }

        template < typename LinkT, typename PinT, typename LinkFunction, typename PinFunction >
        friend inline auto
        map_f_to_ch( const chain< LinkT, PinT >&&, LinkFunction&&, PinFunction&& );

public:
        using link           = Link;
        using pin            = Pin;
        using const_view     = chain_view< const link, const pin >;
        using const_iterator = chain_iterator<
            typename chain_link_container< link >::const_iterator,
            typename chain_link_container< pin >::const_iterator >;
        using iterator = chain_iterator<
            typename chain_link_container< link >::iterator,
            typename chain_link_container< pin >::iterator >;
        using value_type = chain_view< link, pin >;

        // Optimal way to create a chain from existing data, but returns an optional type in case
        // the number of links does not propertly correspond to the number of pins (links.size() + 1
        // == pins.size() )
        static std::optional< chain >
        make( chain_link_container< link > links, chain_pin_container< pin > pins )
        {
                if ( links.size() + 1 == pins.size() ) {
                        return { chain{ std::move( links ), std::move( pins ) } };
                }
                return {};
        }

        chain() = default;

        // uses std::reverse_iterator for iteration over chain in reverse order.
        constexpr std::reverse_iterator< const_iterator > rbegin() const
        {
                return std::make_reverse_iterator( end() );
        }
        // uses std::reverse_iterator for iteration over chain in reverse order.
        constexpr std::reverse_iterator< const_iterator > rend() const
        {
                return std::make_reverse_iterator( begin() );
        }
        // chain is empty in case there are no pins.
        constexpr bool empty() const
        {
                return pins_.empty();
        }

        // returns view over last triple of PLP
        constexpr chain_view< link, pin > back()
        {
                return { *std::next( pins_.rbegin() ), *links_.rbegin(), *pins_.rbegin() };
        }
        // returns view over first triple of PLP
        constexpr chain_view< link, pin > front()
        {
                return { pins_.front(), links_.front(), *std::next( pins_.begin() ) };
        }

        // returns view over last triple of PLP
        constexpr const_view back() const
        {
                return { *std::next( pins_.rbegin() ), links_.end(), pins_.end() };
        }
        // returns view over first triple of PLP
        constexpr const_view front() const
        {
                return { pins_.front(), links_.front(), pins_[1] };
        }
        // iterator to the first LPL triple
        constexpr const_iterator begin() const
        {
                if ( links_.empty() ) {
                        return end();
                }
                return { links_.begin(), pins_.begin() };
        }

        // iterator to past the end triple.
        constexpr const_iterator end() const
        {
                return { links_.end(), std::prev( pins_.end() ) };
        }

        // iterator to the first LPL triple
        constexpr iterator begin()
        {
                if ( links_.empty() ) {
                        return end();
                }
                return { links_.begin(), pins_.begin() };
        }
        // iterator to the past the end triple.
        constexpr iterator end()
        {
                return { links_.end(), pins_.end() };
        }

        // creates a view over PiLiPi+1 triple
        constexpr chain_view< link, pin > operator[]( std::size_t i )
        {
                return { pins_[i], links_[i], pins_[i + 1] };
        }

        // creates a view over PiLiPi+1 triple
        constexpr const_view operator[]( std::size_t i ) const
        {
                return { pins_[i], links_[i], pins_[i + 1] };
        }

        // Reverse the internal storage of links and pins
        void reverse()
        {
                std::reverse( pins_.begin(), pins_.end() );
                std::reverse( links_.begin(), links_.end() );
        }

        // Appends one pin to the chain, creating link of default value in case there is at least
        // another pin.
        void push_back( const pin& pin )
        {
                if ( !pins_.empty() ) {
                        links_.emplace_back();
                }
                pins_.push_back( pin );
        }
        // Removes last pin and link in case present
        void pop_back()
        {
                if ( !links_.empty() ) {
                        links_.pop_back();
                }
                pins_.pop_back();
        }

        // Inserts container of pins after the iter iterator, creates appropiate number of links.
        template < typename Container >
        iterator insert( const const_iterator& iter, Container pins )
        {
                static_assert(
                    std::is_same_v< std::decay_t< typename Container::value_type >, Pin >,
                    "Container has to contain the Pin datatype of the chain" );
                std::size_t count = pins.size();
                if ( pins_.empty() && count > 0 ) {
                        count -= 1;
                }
                auto link_iter = links_.insert( iter.get_link_iter(), count, Link() );
                auto pin_iter  = pins_.insert( iter.get_pin_iter(), pins.begin(), pins.end() );
                return { link_iter, pin_iter };
        }

        // Inserts one pin after the 'iter' iteator and creates link if appropiate.
        iterator insert( const const_iterator& iter, const pin& pin )
        {
                if ( !pins_.empty() ) {
                        auto link_iter = links_.insert( iter.get_link_iter(), Link() );
                        auto pin_iter  = pins_.insert( iter.get_pin_iter(), pin );
                        return { link_iter, pin_iter };
                }

                pins_.insert( iter.get_pin_iter(), pin );
                return end();
        }

        // Erases a range of elements from the chain deined by the view.
        template < typename View >
        iterator erase( const View& view )
        {
                auto link_iter =
                    links_.erase( view.begin().get_link_iter(), view.end().get_link_iter() );
                auto pin_iter =
                    pins_.erase( view.begin().get_pin_iter(), view.end().get_pin_iter() );

                if ( link_iter == links_.end() ) {
                        return end();
                }
                return { link_iter, pin_iter };
        }

        // Erases source_pin and lin of iter.
        iterator erase( const iterator& iter )
        {
                auto link_iter = links_.erase( iter.get_link_iter() );
                auto pin_iter  = pins_.erase( iter.get_pin_iter() );

                if ( link_iter == links_.end() ) {
                        return end();
                }
                return { link_iter, pin_iter };
        }
        // Returns the number of links.
        constexpr std::size_t size() const
        {
                return links_.size();
        }
        // clears all data
        void clear()
        {
                links_.clear();
                pins_.clear();
        }

        // Access to internal pin container
        constexpr const chain_pin_container< pin >& get_pins() const
        {
                return pins_;
        }

        // Access to internal link container
        constexpr const chain_link_container< link >& get_links() const
        {
                return links_;
        }
};

// Two chains are equal if all their links and pins are equal.
template < typename LinkT, typename PinT >
constexpr bool operator==( const chain< LinkT, PinT >& lh, const chain< LinkT, PinT >& rh )
{
        return em::equal( lh.get_pins(), rh.get_pins() ) &&
               em::equal( lh.get_links(), rh.get_links() );
}

// Two chains are not equal in case their equality is false.
template < typename LinkT, typename PinT >
constexpr bool operator!=( const chain< LinkT, PinT >& lh, const chain< LinkT, PinT >& rh )
{
        return !( lh == rh );
}

// Prints chain to the stream with ' P -L-> P -L-> P '.... syntax.
template < typename LinkT, typename PinT >
inline std::ostream& operator<<( std::ostream& os, const chain< LinkT, PinT >& ch )
{
        if ( ch.empty() ) {
                return os;
        }
        os << ch.get_pins().front();
        for ( auto chview : ch ) {
                os << " -(" << chview.link << ")-> " << chview.target_pin;
        }
        return os;
}

template < typename LinkT, typename PinT >
inline void to_json( nlohmann::json& j, const chain< LinkT, PinT >& ch )
{
        for ( auto ch_view : ch ) {
                j.push_back( ch_view.source_pin );
                j.push_back( ch_view.link );
        }
        if ( !ch.empty() ) {
                j.push_back( ch.get_pins().back() );
        }
}

template < typename LinkT, typename PinT, typename LinkFunction, typename PinFunction >
inline auto
map_f_to_ch( const chain< LinkT, PinT >&& ch, LinkFunction&& link_f, PinFunction&& pin_f )
{
        return chain{ map_f_to_v( ch.get_links(), link_f ), map_f_to_v( ch.get_pins(), pin_f ) };
}

}  // namespace schpin
