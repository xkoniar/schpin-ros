// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/either.h"

#ifdef SCHPIN_USE_STREAMS
#include <ostream>
#endif
#include <vector>

#pragma once

namespace schpin
{

namespace em = emlabcpp;

// Standard stream operator of em::either directly prints em::either left or right item.
template < typename LH, typename RH >
inline std::ostream& operator<<( std::ostream& os, const em::either< LH, RH >& env )
{
        env.match(
            [&]( const LH& lh ) {  //
                    os << lh;
            },
            [&]( const RH& rh ) {  //
                    os << rh;
            } );
        return os;
}

// Structure containing content of em::eithers stored in left/right vectors
template < typename LH, typename RH >
struct partitioned_eithers
{
        std::vector< LH > left;
        std::vector< RH > right;
};

// Partitions a collection of em::eithers into vector of left values and vector of right values,
// this implies that all em::eithers have same type.
template < typename LH, typename RH >
partitioned_eithers< LH, RH > partition_eithers( std::vector< em::either< LH, RH > > data )
{
        partitioned_eithers< LH, RH > res;
        for ( em::either< LH, RH >& item : data ) {
                std::move( item ).match(
                    [&]( LH&& lh_item ) {
                            res.left.push_back( std::move( lh_item ) );
                    },
                    [&]( RH&& rh_item ) {
                            res.right.push_back( std::move( rh_item ) );
                    } );
        }
        return res;
}
}  // namespace schpin
