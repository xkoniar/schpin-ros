// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <list>
#include <nlohmann/json.hpp>
#include <string>

#pragma once

namespace schpin
{

// Container is thin overlay over provided storage data container, that restricts index-based access
// to the storage with 'Indetificator' type.
//
// This means, that you can use only Idnetifiactor to the operator[] of the container and nothing
// else. This creates the possibillity to use type system for prevention of errors like 'accessing
// the std::vector index with different value than expected'
template < typename Item, typename Identificator, template < typename... > class Storage >
class container
{
public:
        using storage_type    = Storage< Item >;
        using iterator        = typename storage_type::iterator;
        using const_iterator  = typename storage_type::const_iterator;
        using value_type      = Item;
        using reference       = value_type&;
        using const_reference = const value_type&;
        using size_type       = Identificator;

        using reverse_iterator       = typename storage_type::reverse_iterator;
        using const_reverse_iterator = typename storage_type::const_reverse_iterator;

private:
        storage_type storage_;

public:
        container()
          : storage_()
        {
        }
        container( storage_type storage )
          : storage_( std::move( storage ) )
        {
        }
        container( Identificator size )
          : storage_( *size )
        {
        }

        void clear()
        {
                storage_.clear();
        }

        const storage_type& operator*() const
        {
                return storage_;
        }

        const storage_type* operator->() const
        {
                return storage_;
        }

        const_reference operator[]( const Identificator& id ) const
        {
                return storage_[*id];
        }
        reference operator[]( const Identificator& id )
        {
                return storage_[*id];
        }

        iterator begin()
        {
                return storage_.begin();
        }

        iterator end()
        {
                return storage_.end();
        }

        const_iterator begin() const
        {
                return storage_.cbegin();
        }

        const_iterator end() const
        {
                return storage_.cend();
        }

        reverse_iterator rbegin()
        {
                return storage_.rbegin();
        }

        reverse_iterator rend()
        {
                return storage_.rend();
        }

        const_reverse_iterator rbegin() const
        {
                return storage_.rbegin();
        }

        const_reverse_iterator rend() const
        {
                return storage_.rend();
        }

        bool empty() const
        {
                return storage_.empty();
        }

        size_type size() const
        {
                return storage_.size();
        }

        size_type capacity() const
        {
                return storage_.capacity();
        }

        bool has_id( const Identificator& id )
        {
                return *id >= 0 && *id < storage_.size();
        }

        void resize( const Identificator& id )
        {
                storage_.resize( *id );
        }

        void reserve( const Identificator& id )
        {
                storage_.reserve( *id );
        }

        void shrink_to_fit()
        {
                storage_.shrink_to_fit();
        }

        template < typename Container >
        void insert( const_iterator iter, Container& cont )
        {
                storage_.insert( iter, cont.begin(), cont.end() );
        }

        template < typename... Args >
        const_iterator emplace( const_iterator iter, Args... args )
        {
                return storage_.emplace( iter, args... );
        }

        void pop_back()
        {
                storage_.pop_back();
        }

        template < typename... Args >
        Identificator emplace_back( Args... args )
        {
                Identificator res( size() );

                storage_.emplace_back( args... );

                return res;
        }

        value_type& front()
        {
                return storage_.front();
        }

        value_type& back()
        {
                return storage_.back();
        }

        value_type& at( const Identificator& id )
        {
                return storage_.at( *id );
        }

        const value_type& at( const Identificator& id ) const
        {
                return storage_.at( *id );
        }

        void erase( const const_iterator& iter )
        {
                storage_.erase( iter );
        }
};

template < typename Item, typename Identificator, template < typename... > class Storage >
void to_json( nlohmann::json& json, const container< Item, Identificator, Storage >& con )
{
        json = nlohmann::json{ *con };
}

template < typename Item, typename Identificator, template < typename... > class Storage >
void from_json( const nlohmann::json& json, container< Item, Identificator, Storage >& con )
{
        con = container< Item, Identificator, Storage >( json.get< Storage< Item > >() );
}

}  // namespace schpin
