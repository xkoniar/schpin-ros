// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/logging.h"
#include "schpin_lib_exp/sized_array.h"

#include <Eigen/Core>
#include <Eigen/LU>
#include <Eigen/QR>
#include <type_traits>

#pragma once

namespace schpin
{

template < typename Derived >
class matrix_base
{
};

template < typename Derived >
constexpr bool is_matrix_like = std::is_base_of_v< matrix_base< Derived >, Derived >;

template < typename RowSize, typename ColSize >
class matrix : matrix_base< matrix< RowSize, ColSize > >
{
public:
        using row_size = RowSize;
        using col_size = ColSize;

private:
        static constexpr int eigen_row_size = std::is_same_v< RowSize, dynamic_size > ?
                                                  static_cast< int >( Eigen::Dynamic ) :
                                                  static_cast< int >( RowSize::value );
        static constexpr int eigen_col_size = std::is_same_v< ColSize, dynamic_size > ?
                                                  static_cast< int >( Eigen::Dynamic ) :
                                                  static_cast< int >( ColSize::value );

        using eigen_matrix = Eigen::Matrix< float, eigen_row_size, eigen_col_size >;

        eigen_matrix em_;

        using index = Eigen::Index;

        matrix( eigen_matrix inner_m )
          : em_( inner_m )
        {
        }

        friend matrix< ColSize, RowSize >;

public:
        using row_token = typename RowSize::token;
        using col_token = typename ColSize::token;

        matrix( row_token rows, col_token cols )
          : em_( rows.value, cols.value )
        {
        }

        matrix( row_token rows, col_token cols, std::initializer_list< float > values )
          : em_( rows.value, cols.value )
        {
                SCHPIN_ASSERT( rows.value * cols.value == values.size() );
                for ( std::size_t i : em::range( rows.value ) ) {
                        for ( std::size_t j : em::range( cols.value ) ) {
                                em_( static_cast< index >( i ), static_cast< index >( j ) ) =
                                    *( values.begin() + cols.value * i + j );
                        }
                }
        }

        row_token get_row_token() const
        {
                return row_token{ static_cast< std::size_t >( em_.rows() ) };
        }

        col_token get_col_token() const
        {
                return col_token{ static_cast< std::size_t >( em_.cols() ) };
        }

        template < typename T >
        float at( T row, T col ) const
        {
                return em_( static_cast< index >( row ), static_cast< index >( col ) );
        }

        template < typename T >
        float& at( T row, T col )
        {
                return em_( static_cast< index >( row ), static_cast< index >( col ) );
        }

        matrix< ColSize, RowSize > pseudo_inverse() const
        {
                Eigen::Matrix< float, eigen_col_size, eigen_row_size > res =
                    em_.completeOrthogonalDecomposition().pseudoInverse();
                return { res };
        }

        matrix< ColSize, RowSize > inverse() const
        {
                Eigen::Matrix< float, eigen_col_size, eigen_row_size > res = em_.inverse();
                return { res };
        }

        bool operator==( const matrix& other ) const
        {
                return em_ == other.em_;
        }

        template < typename T, std::enable_if_t< std::is_arithmetic_v< T > >* = nullptr >
        matrix& operator*=( T v )
        {
                em_ *= v;
                return *this;
        }
        template < typename T, std::enable_if_t< std::is_arithmetic_v< T > >* = nullptr >
        matrix& operator/=( T v )
        {
                em_ /= v;
                return *this;
        }

        friend std::ostream& operator<<( std::ostream& os, const matrix& m )
        {
                return os << m.em_;
        }
};  // namespace schpin

using dyn_matrix = matrix< dynamic_size, dynamic_size >;

template < typename RowSize, typename ColSize >
class matrix_transpose : matrix_base< matrix_transpose< RowSize, ColSize > >
{
        const matrix< RowSize, ColSize >& matrix_ref_;

        using index = Eigen::Index;

public:
        using row_size = ColSize;
        using col_size = RowSize;

        using row_token = typename row_size::token;
        using col_token = typename col_size::token;

        matrix_transpose( const matrix< RowSize, ColSize >& m )
          : matrix_ref_( m )
        {
        }

        float at( index row, index col ) const
        {
                return matrix_ref_.at( col, row );
        }

        row_token get_row_token() const
        {
                return matrix_ref_.get_col_token();
        }

        col_token get_col_token() const
        {
                return matrix_ref_.get_row_token();
        }

        operator matrix< ColSize, RowSize >()
        {
                matrix< ColSize, RowSize > res{
                    matrix_ref_.get_col_token(), matrix_ref_.get_row_token() };
                for ( index i : em::range( matrix_ref_.get_row_token().value ) ) {
                        for ( index j : em::range( matrix_ref_.get_col_token().value ) ) {
                                res.at( i, j ) = matrix_ref_.at( j, i );
                        }
                }
                return res;
        }
};

template < typename RowSize, typename ColSize >
inline matrix_transpose< RowSize, ColSize > transpose( const matrix< RowSize, ColSize >& m )
{
        return matrix_transpose( m );
}

template < typename RowSize, typename ColSize >
inline matrix< RowSize, ColSize >
identity_matrix( typename RowSize::token rows, typename ColSize::token cols )
{
        matrix< RowSize, ColSize > res{ rows, cols };
        for ( Eigen::Index i : em::range( rows.value ) ) {
                for ( Eigen::Index j : em::range( cols.value ) ) {
                        res.at( i, j ) = i == j ? 1 : 0;
                }
        }
        return res;
}

template <
    typename Derived,
    std::size_t N,
    std::enable_if_t< is_matrix_like< Derived > >* = nullptr >
inline sized_array< float, typename Derived::row_size >
operator*( const Derived& m, const vec< N >& vec )
{
        SCHPIN_ASSERT( m.get_col_token().value == N );

        sized_array< float, typename Derived::row_size > res{ m.get_row_token() };

        for ( std::size_t i : em::range( m.get_row_token().value ) ) {
                res[i] = 0;
                for ( std::size_t j : em::range( N ) ) {
                        res[i] += m.at( i, j ) * vec[j];
                }
        }

        return res;
}

template <
    typename LhDerived,
    typename RhDerived,
    std::enable_if_t< is_matrix_like< LhDerived > >* = nullptr,
    std::enable_if_t< is_matrix_like< RhDerived > >* = nullptr >
inline matrix< typename LhDerived::row_size, typename RhDerived::col_size >
operator*( const LhDerived& lh, const RhDerived& rh )
{
        // TODO: this is bad :/
        matrix< typename LhDerived::row_size, typename RhDerived::col_size > res{
            lh.get_row_token(), rh.get_col_token() };
        SCHPIN_ASSERT( lh.get_col_token().value == rh.get_row_token().value );

        for ( std::size_t i : em::range( lh.get_row_token().value ) ) {
                for ( std::size_t j : em::range( rh.get_col_token().value ) ) {
                        res.at( i, j ) = 0;
                        for ( std::size_t k : em::range( lh.get_col_token().value ) ) {
                                res.at( i, j ) += lh.at( i, k ) * rh.at( k, j );
                        }
                }
        }

        return res;
}

template < typename Derived, std::enable_if_t< is_matrix_like< Derived > >* = nullptr >
inline Derived operator*( Derived lh, float v )
{
        for ( std::size_t i : em::range( lh.get_row_token().value ) ) {
                for ( std::size_t j : em::range( lh.get_col_token().value ) ) {
                        lh.at( i, j ) *= v;
                }
        }
        return lh;
}

template < typename Derived, std::enable_if_t< is_matrix_like< Derived > >* = nullptr >
inline Derived operator*( float v, Derived lh )
{
        return lh * v;
}

template < typename Derived, std::enable_if_t< is_matrix_like< Derived > >* = nullptr >
inline Derived operator+( Derived lh, const Derived& rh )
{
        for ( std::size_t i : em::range( lh.get_row_token().value ) ) {
                for ( std::size_t j : em::range( lh.get_col_token().value ) ) {
                        lh.at( i, j ) += rh.at( i, j );
                }
        }
        return lh;
}

}  // namespace schpin
