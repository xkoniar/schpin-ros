// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/color.h"
#include "schpin_lib/geom/triangle.h"
#include "schpin_lib/parser.h"
#include "schpin_lib/urdf.h"

#include <urdf/model.h>
#include <urdf_model/joint.h>
#include <urdf_model/link.h>
#include <vector>

#pragma once

namespace schpin
{

template < typename Tag >
class structure_model
{
        // this is pointlessly class... could be const struct instead :/
        std::vector< triangle< 3, Tag > > surface_;
        color                             color_;
        point< 3, Tag >                   center_;
        em::radius                        mini_ball_radius_;

public:
        explicit structure_model( std::vector< triangle< 3, Tag > > surface, color c )
          : surface_( std::move( surface ) )
          , color_( c )
        {
                center_ = cast_to_point< Tag >(
                    em::sum( surface_, [&]( const triangle< 3, Tag >& t ) -> vec< 3 > {  //
                            return cast_to_vec( center_of( t ) ) / surface_.size();
                    } ) );

                mini_ball_radius_ = em::max_elem( surface_, [&]( const triangle< 3, Tag >& t ) {  //
                        return em::max_elem( t, [&]( const point< 3, Tag >& p ) {                 //
                                return distance_of( p, center_ );
                        } );
                } );
        }

        structure_model() = default;

        const point< 3, Tag >& get_center() const
        {
                return center_;
        }

        em::radius get_minball_radius() const
        {
                return mini_ball_radius_;
        }

        const color& get_color() const
        {
                return color_;
        }

        const std::vector< triangle< 3, Tag > >& get_surface() const
        {
                return surface_;
        }

        auto get_convex_hull() const
        {
                return em::view{ surface_ };
        }
};

template < typename Tag >
inline void to_json( nlohmann::json& j, const structure_model< Tag >& model )
{
        j["surface"] = model.get_surface();
        j["color"]   = model.get_color();
}

template < typename Tag >
inline void from_json( const nlohmann::json& j, structure_model< Tag >& model )
{
        model = structure_model{
            j.at( "surface" ).get< std::vector< triangle< 3, Tag > > >(),
            j.at( "color" ).get< color >() };
}

using urdf_parse_error = error< struct UrdfParseTag >;

template < typename Tag >
em::either< std::vector< triangle< 3, Tag > >, urdf_parse_error >
extract_trinagles_from_urdf( const urdf::Pose& offset, const urdf::Geometry& gem )
{
        switch ( gem.type ) {
                case urdf::Geometry::SPHERE:
                case urdf::Geometry::BOX:
                case urdf::Geometry::CYLINDER:
                        return { E( urdf_parse_error ) << "Unsupported geometry type" };
                        break;
                case urdf::Geometry::MESH:
                        const auto* mesh_ptr = dynamic_cast< const urdf::Mesh* >( &gem );

                        pose< Tag > transform_pose = convert_urdf_pose< Tag >( offset );
                        return parse_triangles< Tag >( mesh_ptr->filename )
                            .convert_left( [&]( std::vector< triangle< 3, Tag > > triangles ) {
                                    for ( triangle< 3, Tag >& t : triangles ) {
                                            t = transform(
                                                scale(
                                                    t,
                                                    convert_urdf_point< Tag >( mesh_ptr->scale ) ),
                                                transform_pose );
                                    }

                                    return triangles;
                            } )
                            .convert_right( [&]( const parse_error& e ) {
                                    return E( urdf_parse_error ).attach( e ) << "Failed to parse "
                                                                                "URDF file";
                            } );
        }
        return { E( urdf_parse_error ) << "Unknown geometry type" };
}

template < typename Tag >
em::either< structure_model< Tag >, urdf_parse_error >
extract_model( const urdf::Link& link, bool can_be_empty = false )
{
        std::vector< std::pair< const urdf::Pose*, const urdf::Geometry* > > data;
        std::vector< urdf_parse_error >                                      errors;
        color color{ 0.0f, 0.0f, 0.0f };

        if ( !link.collision_array.empty() ) {
                for ( const urdf::CollisionSharedPtr& col_ptr : link.collision_array ) {
                        if ( !col_ptr->geometry ) {
                                errors.push_back(
                                    E( urdf_parse_error ) << "The collision element "
                                                             "is missing "
                                                             "geometry" );
                        }
                        data.emplace_back( &col_ptr->origin, &*col_ptr->geometry );
                }
        } else if ( link.collision ) {
                if ( !link.collision->geometry ) {
                        errors.push_back(
                            E( urdf_parse_error ) << "The collision element is "
                                                     "missing geometry" );
                }
                data.emplace_back( &link.collision->origin, &*link.collision->geometry );
        } else {  // link->visual_array
                for ( const urdf::VisualSharedPtr& vis_ptr : link.visual_array ) {
                        if ( !vis_ptr->geometry ) {
                                errors.push_back(
                                    E( urdf_parse_error ) << "The visual element is "
                                                             "missing geometry" );
                        }
                        data.emplace_back( &vis_ptr->origin, &*vis_ptr->geometry );

                        // yeah, just take color of the last visual...
                        if ( vis_ptr->material ) {
                                color = convert_urdf_color( vis_ptr->material->color );
                        }
                }
        }

        if ( !can_be_empty && data.empty() ) {
                return {
                    E( urdf_parse_error )
                    << "Link " << link.name << " does not have any 3D model." };
        }

        std::vector< triangle< 3, Tag > > surface;

        for ( auto [pose_ptr, geom_ptr] : data ) {
                extract_trinagles_from_urdf< Tag >( *pose_ptr, *geom_ptr )
                    .match(
                        [&]( const std::vector< triangle< 3, Tag > >& triangles ) {
                                surface.insert( surface.end(), triangles.begin(), triangles.end() );
                        },
                        [&]( const urdf_parse_error& error ) {  //
                                errors.push_back( error );
                        } );
        }

        if ( !errors.empty() ) {
                return {
                    E( urdf_parse_error ).group_attach( errors )
                    << "There were errors during creation of model from link " << link.name };
        }

        return { structure_model< Tag >( surface, color ) };
}

}  // namespace schpin

template < typename Tag >
struct std::hash< schpin::structure_model< Tag > >
{
        std::size_t operator()( const schpin::structure_model< Tag >& model )
        {
                std::size_t res = 0;
                for ( const schpin::triangle< 3, Tag >& t : model.get_surface() ) {
                        res ^= std::hash< schpin::triangle< 3, Tag > >()( t );
                }

                return res ^ std::hash< schpin::color >()( model.get_color() );
        }
};
