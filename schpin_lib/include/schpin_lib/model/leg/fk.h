// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/model/leg/revolute_joint_dataset.h"

#pragma once

namespace schpin
{

template < typename SizeType >
class fk_solver
{
        revolute_joints_dataset< SizeType > dataset_;
        template < typename T >
        using container = sized_array< T, SizeType >;

public:
        fk_solver( pose< robot_frame > first_offset, container< const joint* > mapping )
          : dataset_( first_offset, std::move( mapping ) )
        {
        }

        template < typename BinaryFunction >
        void for_each_joint( const container< em::angle >& angles, BinaryFunction&& f ) const
        {
                SCHPIN_ASSERT( angles.size() == dataset_.joints.size() );
                pose< robot_frame > offset;
                for ( std::size_t i : em::range( angles.size() ) ) {
                        pose< robot_frame > rot_pose{
                            quaternion< robot_frame >{ dataset_.axis[i], angles[i] } };

                        offset = transform( transform( rot_pose, dataset_.offsets[i] ), offset );

                        f( *dataset_.joints[i], offset );
                        for_each_static_joint( dataset_.joints[i]->sub_link.child_ptr, offset, f );
                }
        }

        point< 3, robot_frame > get_tip_position( const container< em::angle >& angles ) const
        {
                point< 3, robot_frame > tip;
                for_each_joint( angles, [&]( const joint& j, const pose< robot_frame >& offset ) {
                        if ( j.sub_link.child_ptr ) {
                                return;
                        }
                        tip = offset.position;
                } );
                return tip;
        }

private:
        template < typename BinaryFunction >
        void for_each_static_joint(
            const std::unique_ptr< joint >& j_ptr,
            const pose< robot_frame >&      offset,
            BinaryFunction&&                f ) const
        {
                if ( !j_ptr ) {
                        return;
                }
                if ( std::holds_alternative< revolute_joint >( j_ptr->jclass ) ) {
                        return;
                }
                auto actual_offset = transform( j_ptr->offset, offset );
                f( *j_ptr, actual_offset );
                for_each_static_joint( j_ptr->sub_link.child_ptr, actual_offset, f );
        }
};

}  // namespace schpin
