// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point/serialization.h"
#include "schpin_lib/model/leg.h"

#include <emlabcpp/match.h>
#include <nlohmann/json.hpp>
#include <urdf/model.h>
#include <urdf_model/joint.h>

#pragma once

namespace schpin
{
inline em::either< joint, urdf_parse_error >
parse_joint_rec( const urdf::Model& model, const urdf::JointConstSharedPtr& joint_ptr )
{
        urdf::LinkConstSharedPtr link = model.getLink( joint_ptr->child_link_name );

        if ( !link ) {
                return {
                    E( urdf_parse_error )
                    << "Link " << joint_ptr->child_link_name << " is missing" };
        }
        joint j;
        j.name   = joint_ptr->name;
        j.offset = convert_urdf_pose< robot_frame >( joint_ptr->parent_to_joint_origin_transform );
        j.sub_link.name = joint_ptr->child_link_name;

        if ( joint_ptr->type == urdf::Joint::REVOLUTE ) {
                if ( !joint_ptr->limits ) {
                        return {
                            E( urdf_parse_error )
                            << "Joint " << joint_ptr->name << " is mising limit element" };
                }

                revolute_joint rj;

                // TODO: meh
                rj.axis      = cast_to_vec( convert_urdf_point< robot_frame >( joint_ptr->axis ) );
                rj.min_angle = em::angle{ float( joint_ptr->limits->lower ) };
                rj.max_angle = em::angle{ float( joint_ptr->limits->upper ) };
                rj.state_space_steps = 24;  // TODO(squirrel): ??? how to get steps? -> SRDF
                rj.max_velocity      = em::angular_velocity{ float( joint_ptr->limits->velocity ) };

                if ( length_of( rj.axis ) != em::length{ 1.f } ) {
                        return {
                            E( urdf_parse_error )
                            << "Axis " << rj.axis << " does not have length 1 " };
                }

                j.jclass = rj;
        } else if ( joint_ptr->type == urdf::Joint::FIXED ) {
                j.jclass = fixed_joint{};
        } else {
                return {
                    E( urdf_parse_error ) << "Joint has to be of type revolute or fixed, "
                                          << joint_ptr->name << " is not. " };
        }

        auto childs_eithers =
            map_f_to_v( link->child_joints, [&]( const urdf::JointConstSharedPtr& subjoint ) {
                    return parse_joint_rec( model, subjoint );
            } );

        partitioned_eithers< joint, urdf_parse_error > parted =
            partition_eithers( std::move( childs_eithers ) );

        if ( !parted.right.empty() ) {
                return {
                    E( urdf_parse_error ).group_attach( parted.right )
                    << "Failed to parse child nodes in " << link->name };
        }
        if ( parted.left.size() > 1 ) {
                return {
                    E( urdf_parse_error )
                    << "Failed to parse leg, link " << link->name << " has too many childs " };
        }
        if ( parted.left.size() == 1 ) {
                j.sub_link.child_ptr =
                    std::make_unique< joint >( std::move( parted.left.front() ) );
        }

        bool can_be_empty_model = true;  // TODO: this may not be a good idea
        return extract_model< robot_frame >( *link, can_be_empty_model )
            .convert_left( [&]( const structure_model< robot_frame >& smodel ) {
                    j.sub_link.model = smodel;
                    return std::move( j );
            } );
}

inline em::either< pose< robot_frame >, urdf_parse_error > urdf_tree_offset(
    const urdf::Model&               model,
    const urdf::LinkConstSharedPtr&  model_root,
    const urdf::JointConstSharedPtr& joint_ptr )
{
        auto actual =
            convert_urdf_pose< robot_frame >( joint_ptr->parent_to_joint_origin_transform );
        if ( model_root->name == joint_ptr->parent_link_name ) {
                return actual;
        }

        const urdf::LinkConstSharedPtr& parent_link = model.getLink( joint_ptr->parent_link_name );
        if ( !parent_link ) {
                return {
                    E( urdf_parse_error ) << "Failed to extract transformation between "
                                          << model_root->name << " and " << joint_ptr->name };
        }
        return urdf_tree_offset( model, model_root, parent_link->parent_joint )
            .convert_left( [&]( const pose< robot_frame >& p ) {  //
                    return transform( actual, p );
            } );
}

inline std::size_t count_revolute_joints( const joint& j )
{
        std::size_t c = std::holds_alternative< revolute_joint >( j.jclass ) ? 1 : 0;
        if ( j.sub_link.child_ptr ) {
                c += count_revolute_joints( *j.sub_link.child_ptr );
        }
        return c;
}

inline void to_json( nlohmann::json& j, const revolute_joint& rj )
{
        j["axis"]         = rj.axis;
        j["min_angle"]    = rj.min_angle;
        j["max_angle"]    = rj.max_angle;
        j["steps"]        = rj.state_space_steps;
        j["max_velocity"] = rj.max_velocity;
}
inline void from_json( const nlohmann::json& j, revolute_joint& rj )
{
        rj.axis              = j.at( "axis" ).get< vec< 3 > >();
        rj.min_angle         = j.at( "min_angle" ).get< em::angle >();
        rj.max_angle         = j.at( "max_angle" ).get< em::angle >();
        rj.state_space_steps = j.at( "steps" ).get< unsigned >();
        rj.max_velocity      = j.at( "max_velocity" ).get< em::angular_velocity >();
}

inline void to_json( nlohmann::json& j, const link& l );
inline void from_json( const nlohmann::json& j, link& l );

inline void to_json( nlohmann::json& j, const joint& joint )
{
        j["name"]   = joint.name;
        j["offset"] = joint.offset;
        em::match(
            joint.jclass,
            [&]( const fixed_joint& ) {
                    j["class"] = "fixed";
            },
            [&]( const revolute_joint& rj ) {
                    j["class"] = "revolute";
                    to_json( j, rj );
            } );
        j["link"] = joint.sub_link;
}
inline void from_json( const nlohmann::json& j, joint& joint )
{
        joint.name      = j.at( "name" ).get< std::string >();
        std::string cls = j.at( "class" ).get< std::string >();
        if ( cls == "revolute" ) {
                revolute_joint rj;
                from_json( j, rj );
                joint.jclass = rj;
        } else if ( cls == "fixed" ) {
                fixed_joint fj;
                joint.jclass = fj;
        } else {
                SCHPIN_ASSERT( false );
        }
        joint.offset   = j.at( "offset" ).get< pose< robot_frame > >();
        joint.sub_link = j.at( "link" ).get< link >();
}

inline void to_json( nlohmann::json& j, const link& l )
{
        j["name"]  = l.name;
        j["model"] = l.model;
        if ( l.child_ptr ) {
                j["child"] = *l.child_ptr;
        }
}

inline void from_json( const nlohmann::json& j, link& l )
{
        l.name  = j.at( "name" ).get< std::string >();
        l.model = j.at( "model" ).get< structure_model< robot_frame > >();
        if ( j.find( "child" ) != j.end() ) {
                l.child_ptr = std::make_unique< joint >( j.at( "child" ).get< joint >() );
        }
}

}  // namespace schpin
