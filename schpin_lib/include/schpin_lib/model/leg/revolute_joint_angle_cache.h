// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/model/leg.h"

#include <emlabcpp/match.h>

#pragma once

namespace schpin
{

template < unsigned N >
class revolute_joint_angle_cache
{
        std::array< em::angle, N > base_;
        std::array< em::angle, N > step_;

public:
        explicit revolute_joint_angle_cache( const std::array< const joint*, N >& mapping )
        {
                for ( std::size_t i : em::range( N ) ) {
                        em::match(
                            mapping[i]->jclass,
                            [&]( const revolute_joint& rj ) {
                                    base_[i] = rj.min_angle;
                                    step_[i] = ( rj.max_angle - rj.min_angle ) /
                                               float( rj.state_space_steps - 1 );
                            },
                            [&]( const fixed_joint& ) {
                                    SCHPIN_ASSERT( false );
                            } );
                }
        }

        constexpr std::array< em::angle, N >
        discrete_to_angles( const std::array< uint8_t, N >& angles ) const
        {
                return em::map_f_to_a< N >( em::range( N ), [&]( std::size_t i ) {
                        return base_[i] + step_[i] * static_cast< float >( angles[i] );
                } );
        }
};

}  // namespace schpin
