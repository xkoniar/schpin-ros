// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib_exp/sized_array.h"
#include <emlabcpp/match.h>

#pragma once

namespace schpin
{

template < typename SizeType >
struct revolute_joints_dataset
{

        template < typename T >
        using container = sized_array< T, SizeType >;

        container< const joint* >        joints;   // mapping angle -> joint
        container< pose< robot_frame > > offsets;  // offset of revolute joint from it's first
        container< vec< 3 > >            axis;     // axis for convenience

        revolute_joints_dataset(
            pose< robot_frame >              first_offset,
            const container< const joint* >& mapping )
          : joints( mapping )
          , offsets( joints.get_size_token() )
          , axis( joints.get_size_token() )
        {
                offsets[0] = first_offset;
                for ( std::size_t i : em::range( joints.size() ) ) {
                        find_revolute_children(
                            mapping[i], [&]( const joint*, const pose< robot_frame >& offset ) {
                                    offsets[i + 1] = offset;
                            } );

                        // TODO: this throws exception, deal with that
                        axis[i] = std::get< revolute_joint >( joints[i]->jclass ).axis;
                }
        }

private:
        template < typename OnChildFunction >
        void find_revolute_children(
            const joint*        j,
            OnChildFunction     f,
            pose< robot_frame > parent_offset = {} ) const
        {
                const std::unique_ptr< joint >& child = j->sub_link.child_ptr;
                if ( !child ) {
                        return;
                }
                em::match(
                    child->jclass,
                    [&]( const revolute_joint& ) {
                            f( &*child, transform( child->offset, parent_offset ) );
                    },
                    [&]( const fixed_joint& ) {  //
                            find_revolute_children(
                                &*child, f, transform( child->offset, parent_offset ) );
                    } );
        }
};

}  // namespace schpin
