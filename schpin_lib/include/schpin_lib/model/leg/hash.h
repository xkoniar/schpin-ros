// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/model/leg.h>

#pragma once

template <>
struct std::hash< schpin::joint >
{
        inline std::size_t operator()( const schpin::joint& ) const;
};

template <>
struct std::hash< schpin::link >
{
        std::size_t operator()( const schpin::link& link ) const
        {
                std::size_t base =
                    std::hash< schpin::structure_model< schpin::robot_frame > >()( link.model ) ^
                    std::hash< std::string >()( link.name );

                if ( link.child_ptr ) {
                        base ^= std::hash< schpin::joint >()( *link.child_ptr );
                }

                return base;
        }
};
template <>
struct std::hash< schpin::fixed_joint >
{
        std::size_t operator()( const schpin::fixed_joint& ) const
        {
                return 0;
        };
};
template <>
struct std::hash< schpin::revolute_joint >
{
        std::size_t operator()( const schpin::revolute_joint& joint ) const
        {
                // TODO: this seems overly ugly
                std::stringstream ss;
                ss << joint.axis << joint.min_angle << joint.max_angle << joint.state_space_steps
                   << joint.max_velocity;
                return std::hash< std::string >()( ss.str() );
        };
};

inline std::size_t std::hash< schpin::joint >::operator()( const schpin::joint& joint ) const
{
        // TODO: this seems overly ugly
        std::stringstream ss;
        ss << joint.name << joint.offset;
        return std::hash< std::string >()( ss.str() ) ^
               std::hash< schpin::link >()( joint.sub_link ) ^
               std::hash< schpin::joint_class_variant >()( joint.jclass );
}
