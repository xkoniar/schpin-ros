// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"
#include "schpin_lib/geom/point/serialization.h"
#include "schpin_lib/geom/pose/serialization.h"
#include "schpin_lib/model/structure.h"

#include <emlabcpp/physical_quantity.h>
#include <variant>

#pragma once

namespace schpin
{

struct fixed_joint
{
};

struct revolute_joint
{
        vec< 3 >             axis;
        em::angle            min_angle{ 0 };
        em::angle            max_angle{ 0 };
        unsigned             state_space_steps{};
        em::angular_velocity max_velocity;
};

struct joint;

struct link
{
        std::string                    name;
        structure_model< robot_frame > model;
        std::unique_ptr< joint >       child_ptr;

        link copy() const;
};

using joint_class_variant = std::variant< fixed_joint, revolute_joint >;

struct joint
{
        std::string         name;
        joint_class_variant jclass;
        pose< robot_frame > offset;
        link                sub_link;

        joint copy() const
        {
                return joint{ name, jclass, offset, sub_link.copy() };
        }
};

inline link link::copy() const
{
        return link{
            name,
            model,
            child_ptr ? std::make_unique< joint >( child_ptr->copy() ) :
                        std::unique_ptr< joint >{} };
}

inline std::ostream& operator<<( std::ostream& os, const joint_class_variant& var )
{
        if ( std::holds_alternative< fixed_joint >( var ) ) {
                return os << "fixed";
        }
        return os << "revolute";
}

inline std::ostream& operator<<( std::ostream& os, const joint& root )
{
        std::function< void( const joint&, std::string ) > rec_printer =
            [&]( const joint& j, const std::string& indent ) {
                    os << indent << j.name << " - " << j.jclass << "\n"
                       << indent << "    off: " << j.offset << "\n"
                       << indent << "    link: " << j.sub_link.name << "\n";
                    if ( j.sub_link.child_ptr ) {
                            rec_printer( *j.sub_link.child_ptr, indent + "    " );
                    }
            };
        rec_printer( root, "" );
        return os;
}

}  // namespace schpin
