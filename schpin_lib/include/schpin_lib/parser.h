// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/error.h"
#include "schpin_lib/geom/point.h"
#include "schpin_lib/geom/triangle.h"
#include "schpin_lib/util.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <filesystem>

#pragma once

namespace schpin
{

using parse_error = error< struct parse_error_tag >;

template < typename Tag >
inline point< 3, Tag > ai_vec_to_point( const aiVector3D& vec )
{
        return point< 3, Tag >( vec.x, vec.y, vec.z );
}

template < typename Tag >
inline em::either< std::vector< triangle< 3, Tag > >, parse_error >
parse_triangles( std::filesystem::path filename )
{
        filename = get_resolved_path( filename );
        Assimp::Importer importer;

        SCHPIN_INFO_LOG( "parser", "Loading file: " << filename );

        std::ifstream file{ filename, std::ios::binary | std::ios::ate };

        if ( !file ) {
                return { E( parse_error ) << "Failed to open file: " << filename };
        }

        std::streamsize size = file.tellg();
        file.seekg( 0, std::ios::beg );

        std::vector< char > buffer( static_cast< uint64_t >( size ) );
        if ( !file.read( buffer.data(), size ) ) {
                return { E( parse_error ) << "Failed to load a file: " << filename };
        }

        std::filesystem::path file_t = filename.extension();
        if ( file_t == "stlb" ) {
                file_t = "stl";
        }

        const aiScene* scene = importer.ReadFileFromMemory(
            static_cast< void* >( buffer.data() ),
            buffer.size(),
            aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_JoinIdenticalVertices |
                aiProcess_SortByPType | aiProcess_ImproveCacheLocality,
            file_t.c_str() );

        if ( scene == nullptr ) {
                return {
                    E( parse_error )
                    << "Assimp failed to parse the file: " << importer.GetErrorString() };
        }

        std::vector< triangle< 3, Tag > > res;

        for ( const aiMesh* mesh : em::view_n( scene->mMeshes, scene->mNumMeshes ) ) {
                for ( const aiFace& face : em::view_n( mesh->mFaces, mesh->mNumFaces ) ) {
                        SCHPIN_ASSERT( face.mNumIndices == 3 );

                        std::array< point< 3, Tag >, 3 > points = em::map_f_to_a< 3 >(
                            em::view_n( face.mIndices, face.mNumIndices ),
                            [&]( unsigned int indice ) -> point< 3, Tag > {
                                    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
                                    return ai_vec_to_point< Tag >( mesh->mVertices[indice] );
                            } );

                        res.emplace_back( points[0], points[1], points[2] );
                }
        }

        return { res };
}

}  // namespace schpin
