// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/types.h"

#include <array>
#include <tuple>
#include <type_traits>

#pragma once

namespace em = emlabcpp;

namespace schpin
{

template < typename T >
struct has_reserve
{
        template <
            typename U,
            typename = decltype( std::declval< U >().reserve( std::size_t{ 1 } ) ) >
        static std::true_type test( int );

        template < typename >
        static std::false_type test( ... );

        static constexpr bool value = decltype( test< T >( 0 ) )::value;
};

template < typename T >
// NOLINTNEXTLINE(readability-identifier-naming)
constexpr bool has_reserve_v = has_reserve< std::decay_t< T > >::value;

// from here:
// https://stackoverflow.com/questions/22758291/how-can-i-detect-if-a-type-can-be-streamed-to-an-stdostream
template < typename S, typename T, typename = void >
struct is_to_stream_writable : std::false_type
{
};

template < typename S, typename T >
struct is_to_stream_writable<
    S,
    T,
    std::void_t< decltype( std::declval< S& >() << std::declval< T >() ) > > : std::true_type
{
};

template < typename S, typename T >
// NOLINTNEXTLINE(readability-identifier-naming)
constexpr bool is_to_stream_writable_v = is_to_stream_writable< S, T >::value;

}  // namespace schpin
