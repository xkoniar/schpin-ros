#include "schpin_lib/geom/point/serialization.h"
#include "schpin_lib/octree/base.h"

#pragma once

namespace schpin
{

struct oc_node_context
{
        unsigned             depth;
        unsigned             max_depth;
        em::length           edge_length;
        point< 3, oc_frame > center_position;

        friend constexpr auto
        operator<=>( const oc_node_context&, const oc_node_context& ) = default;
};

std::ostream& operator<<( std::ostream&, const oc_node_context& );

inline oc_node_context oc_upper_context( const oc_node_context& c, oc_child_index i )
{
        return oc_node_context{
            .depth           = c.depth == 0 ? 0 : c.depth - 1,
            .max_depth       = c.max_depth + ( c.depth == 0 ? 1 : 0 ),
            .edge_length     = c.edge_length * 2.f,
            .center_position = c.center_position - get_oc_child_offset( i, c.edge_length * 2.f ) };
}

inline oc_node_context oc_lower_context( const oc_node_context& c, oc_child_index i )
{
        return oc_node_context{
            .depth           = c.depth + 1,
            .max_depth       = c.max_depth,
            .edge_length     = c.edge_length / 2.f,
            .center_position = c.center_position + get_oc_child_offset( i, c.edge_length ) };
}

}  // namespace schpin

template <>
struct std::hash< schpin::oc_node_context >
{
        std::size_t operator()( const schpin::oc_node_context& node )
        {
                std::array< std::size_t, 4 > vals = {
                    std::hash< unsigned >()( node.depth ),
                    std::hash< unsigned >()( node.max_depth ),
                    std::hash< em::length >()( node.edge_length ),
                    std::hash< schpin::point< 3, schpin::oc_frame > >()( node.center_position ) };
                std::size_t res = 0;
                for ( std::size_t h : vals ) {
                        res = std::rotl( res, 8 ) ^ h;
                }
                return res;
        }
};
