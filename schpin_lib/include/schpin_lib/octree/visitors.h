#include "schpin_lib/collision.h"
#include "schpin_lib/collision/concepts.h"
#include "schpin_lib/octree/node_view.h"
#include "schpin_lib/octree/query.h"

#include <emlabcpp/static_vector.h>

#pragma once

namespace schpin
{

template < typename Visitor >
concept oc_visitor = requires(
    Visitor                                            vis,
    const oc_node_view< typename Visitor::node_type >& node,
    typename Visitor::info_type                        info,
    typename Visitor::content_type                     cont )
{
        {
                std::as_const( vis ).on_node( node, info )
                } -> std::same_as< std::pair< oc_descent_control, typename Visitor::info_type > >;
        {
                std::as_const( vis ).on_locked_leaf( node, info )
                } -> std::same_as< std::pair< oc_leaf_control, typename Visitor::info_type > >;
        { std::as_const( vis ).on_leaf( node, info, cont ) };
};

template < typename Checker >
concept oc_checker = requires(
    const Checker& c,
    const sat_oc_node_collision_query< const oc_node< typename Checker::content_type >, oc_frame >&
        q )
{
        {
                c.node_collision( q )
                } -> std::same_as< bool >;
        {
                c.leaf_collision( q )
                } -> std::same_as< bool >;
};

template < sat_collision_query SATQuery, bs_collision_query BSQuery, oc_node_derived Node >
class oc_collision_checker
{
        static_assert( std::same_as< typename SATQuery::item_type, typename BSQuery::item_type > );

public:
        using content_type = typename Node::content_type;
        using item_type    = typename SATQuery::item_type;

private:
        const SATQuery item_sat_query_;
        const BSQuery  item_bs_query_;

public:
        oc_collision_checker( const item_type& val )
          : item_sat_query_( val )
          , item_bs_query_( val )
        {
        }

        constexpr bool node_collision(
            const sat_oc_node_collision_query< const Node, oc_frame >& node_query ) const
        {
                bs_oc_node_collision_query< const Node, oc_frame > bs_q{ node_query.node_view() };
                return bs_collides( item_bs_query_, bs_q );
        }

        constexpr bool leaf_collision(
            const sat_oc_node_collision_query< const Node, oc_frame >& node_query ) const
        {
                return sat_collides( item_sat_query_, node_query );
        }
};

template < oc_checker Checker, std::size_t BatchSize >
class oc_collision_visitor
{
public:
        using info_type    = std::bitset< BatchSize >;
        using item_type    = typename Checker::item_type;
        using content_type = typename Checker::content_type;
        using token        = typename content_type::token;
        using node_type    = const oc_node< content_type >;
        using tag          = oc_frame;
        using sat_query    = sat_oc_node_collision_query< node_type, tag >;

private:
        em::static_vector< Checker, BatchSize > checkers_{};
        token                                   token_;
        info_type                               basic_selection_{ 0 };

public:
        template < typename T >
        oc_collision_visitor( T&& batch, token tok )
          : checkers_()
          , token_( tok )
        {
                for ( std::size_t i : em::range( batch.size() ) ) {
                        checkers_.emplace_back( batch[i] );
                        basic_selection_[i] = 1;
                }
        }

        info_type default_info() const
        {
                info_type res;
                res.set();
                return res;
        }

        constexpr std::pair< oc_descent_control, info_type >
        on_node( const oc_node_view< node_type >& node, const info_type& parent_info ) const
        {
                info_type info = parent_info;
                sat_query node_query{ node };

                for ( std::size_t i : em::range( BatchSize ) ) {
                        if ( !info[i] ) {
                                continue;
                        }
                        if ( !checkers_[i].node_collision( node_query ) ) {
                                info[i] = false;
                        }
                }

                if ( info.any() ) {
                        return { oc_descent_control::FOLLOW_CHILDREN, info };
                }

                return { oc_descent_control::END, info };
        }

        constexpr std::pair< oc_leaf_control, info_type >
        on_locked_leaf( const oc_node_view< node_type >& node, const info_type& parent_info ) const
        {
                auto [ctl, info] = on_node( node, parent_info );
                if ( ctl == oc_descent_control::FOLLOW_CHILDREN ) {
                        return { oc_leaf_control::EXPAND_CHILDREN, info };
                }
                return { oc_leaf_control::END, info };
        }

        void on_leaf(
            const oc_node_view< node_type >& node,
            const info_type&                 parent_info,
            content_type&                    content ) const
        {
                sat_query node_query{ node };

                for ( std::size_t i : em::range( BatchSize ) ) {
                        if ( parent_info[i] && checkers_[i].leaf_collision( node_query ) ) {
                                content.update( token_ );
                        }
                }
        }
};

}  // namespace schpin
