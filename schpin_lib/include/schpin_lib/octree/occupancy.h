#include "schpin_lib/occupancy.h"
#include "schpin_lib/octree/node.h"
#include "schpin_lib/octree/node_view.h"

#pragma once

namespace schpin
{
constexpr occupancy OC_DEFAULT_OCCUPANCY{ 0.5f };
constexpr occupancy OC_PRUNNING_DIFF_OCCUPANCY{ 0.5f };
constexpr occupancy OC_MIN_OCCUPANCY{ 0.5f };
constexpr occupancy OC_MAX_OCCUPANCY{ 0.5f };

enum class occupancy_token
{
        FULL
};

class occupancy_content
{
        occupancy oc_ = OC_DEFAULT_OCCUPANCY;

public:
        using token = occupancy_token;

        occupancy_content() = default;
        occupancy_content( occupancy oc )
          : oc_( oc )
        {
        }

        constexpr occupancy occ() const
        {
                return oc_;
        }

        constexpr std::size_t memory_usage() const
        {
                return sizeof( occupancy_content );
        }

        constexpr bool prunnable( const oc_node< occupancy_content >& ) const
        {
                return abs( OC_MAX_OCCUPANCY - oc_ ) < OC_PRUNNING_DIFF_OCCUPANCY ||
                       abs( OC_MIN_OCCUPANCY - oc_ ) < OC_PRUNNING_DIFF_OCCUPANCY;
        }

        void on_child_update( const oc_node< occupancy_content >& node )
        {
                oc_ = em::sum( node, [&]( const oc_node< occupancy_content >& child ) {
                        return child->occ();
                } );
                oc_ /= static_cast< float >( node.size() );
        }

        void set_prunned( const oc_node< occupancy_content >& )
        {
                bool maxxed = oc_ > ( OC_MAX_OCCUPANCY + OC_MIN_OCCUPANCY ) / 2.f;
                oc_         = maxxed ? OC_MAX_OCCUPANCY : OC_MIN_OCCUPANCY;
        }

        void update( occupancy_token )
        {
                oc_ = OC_MAX_OCCUPANCY;
        }

        friend constexpr auto
        operator<=>( const occupancy_content&, const occupancy_content& ) = default;
};

struct occupancy_treshold_predicate
{
        constexpr bool operator()( const oc_node< occupancy_content >& node )
        {
                return node->occ() > OC_DEFAULT_OCCUPANCY;
        }

        constexpr bool operator()( const oc_node_view< const oc_node< occupancy_content > >& node )
        {
                return this->operator()( *node );
        }
};

inline void to_json( nlohmann::json& j, const occupancy_content& c )
{
        j = *c.occ();
}

inline void from_json( const nlohmann::json& j, occupancy_content& c )
{
        c = occupancy_content{ occupancy{ j.get< float >() } };
}

}  // namespace schpin

template <>
struct std::hash< schpin::occupancy_content >
{
        std::size_t operator()( const schpin::occupancy_content& c )
        {
                return std::hash< schpin::occupancy >{}( c.occ() );
        }
};
