#include "schpin_lib/geom/point.h"
#include "schpin_lib/geom/vec.h"

#include <bitset>
#include <concepts>
#include <cstdint>

#pragma once

namespace schpin
{

struct oc_frame
{
};

using oc_child_index = uint8_t;
enum class oc_descent_control
{
        FOLLOW_CHILDREN,
        END
};
enum class oc_leaf_control
{
        EXPAND_CHILDREN,
        END
};

template < typename Tag >
inline oc_child_index
get_oc_child_index( const point< 3, Tag >& parent, const point< 3, Tag >& child )
{
        std::bitset< 3 > bits;
        for ( std::size_t i : { 0u, 1u, 2u } ) {
                bits[i] = parent[i] > child[i] ? 0 : 1;
        }
        return static_cast< oc_child_index >( bits.to_ulong() );
}

inline vec< 3 > get_oc_child_direction( oc_child_index index )
{
        std::bitset< 3 > bits{ index };
        return vec< 3 >{ //
                         bits[0] ? 1 : -1,
                         bits[1] ? 1 : -1,
                         bits[2] ? 1 : -1 };
}

inline vec< 3 > get_oc_child_offset( oc_child_index index, em::length parent_edge )
{
        vec< 3 > dir  = get_oc_child_direction( index );
        float    step = *parent_edge / 4;
        return dir * step;
}

template < typename C >
concept oc_content = requires( C cont )
{
        typename C::token;
}
&&requires( const C& cont )
{
        {
                cont.memory_usage()
                } -> std::same_as< std::size_t >;
}
&&requires( C cont, typename C::token token )
{
        //{ cont.on_child_update( node ) };
        //{ cont.set_prunned( node ) };
        //{
        //       cont.prunnable(node)
        //     } -> std::same_as< bool >;
        { cont.update( token ) };
};

template < typename T, typename C >
concept has_content = std::same_as< typename std::decay_t< T >::content_type, C >;

}  // namespace schpin
