#include "schpin_lib/geom/pose.h"
#include "schpin_lib/octree/node.h"
#include "schpin_lib/octree/node_context.h"

#pragma once

namespace schpin
{

template < oc_node_derived OcNode >
class oc_node_view
{
public:
        static constexpr bool is_const = std::is_const_v< OcNode >;

        oc_node_view( OcNode& node, const oc_node_context& context )
          : node_( node )
          , context_( context )
        {
        }

        constexpr OcNode& operator*()
        {
                return node_;
        };

        constexpr const OcNode& operator*() const
        {
                return node_;
        };

        constexpr OcNode* operator->()
        {
                return &node_;
        };

        constexpr const OcNode* operator->() const
        {
                return &node_;
        };

        constexpr std::size_t size() const
        {
                return node_.size();
        }

        constexpr bool empty() const
        {
                return node_.empty();
        }

        constexpr bool is_leaf() const
        {
                return node_.is_leaf();
        }

        constexpr unsigned max_depth() const
        {
                return context_.max_depth;
        }

        constexpr unsigned depth() const
        {
                return context_.depth;
        }

        constexpr em::length edge_length() const
        {
                return context_.edge_length;
        }

        constexpr point< 3, oc_frame > center_position() const
        {
                return context_.center_position;
        }

        constexpr bool has_max_depth() const
        {
                return context_.depth == context_.max_depth;
        }

        constexpr em::min_max< point< 3, oc_frame > > make_corners() const
        {
                const auto& c = center_position();
                float       l = *edge_length() / 2.f;
                return {
                    point< 3, oc_frame >{ c[0] - l, c[1] - l, c[2] - l },
                    point< 3, oc_frame >{ c[0] + l, c[1] + l, c[2] + l } };
        }

        constexpr std::array< oc_node_view, 8 > generate_children() const
        {
                return map_f_to_a< 8 >( em::range( 8u ), [&]( std::size_t i ) {
                        return oc_node_view{
                            node_[i],
                            oc_lower_context( context_, static_cast< oc_child_index >( i ) ) };
                } );
        }

        constexpr bool overlaps( const point< 3, oc_frame >& p ) const
        {
                auto c = make_corners();

                return em::all_of( em::range( 3u ), [&]( std::size_t i ) {
                        return c.min[i] <= p[i] && p[i] <= c.max[i];
                } );
        }

        constexpr std::optional< oc_node_view >
        find_overlapping_leaf( const point< 3, oc_frame >& p )
        {
                if ( !overlaps( p ) ) {
                        return {};
                }
                if ( is_leaf() ) {
                        return *this;
                }
                auto children   = generate_children();
                auto child_iter = em::find_if( children, [&]( const oc_node_view& child ) {
                        return child.overlaps( p );
                } );
                return child_iter->find_overlapping_leaf( p );
        }

        template < typename FilterFunction >
        std::optional< em::min_max< point< 3, oc_frame > > >
        recursive_matched_area( FilterFunction&& f ) const
        {
                if ( f( *this ) ) {
                        auto corners = make_corners();
                        return dimensional_min_max_elem( std::array{ corners.min, corners.max } );
                }

                if ( is_leaf() ) {
                        return {};
                }

                std::vector< point< 3, oc_frame > > res;
                for ( const oc_node_view& child : generate_children() ) {
                        auto opt_subres = child.recursive_matched_area( f );
                        if ( !opt_subres ) {
                                continue;
                        }
                        res.push_back( opt_subres->min );
                        res.push_back( opt_subres->min );
                }
                if ( res.empty() ) {
                        return {};
                }
                return dimensional_min_max_elem( res );
        }

        constexpr const oc_node_context& context() const
        {
                return context_;
        }

private:
        OcNode&               node_;
        const oc_node_context context_;
};

template < typename OcNode >
inline std::ostream& operator<<( std::ostream& os, const oc_node_view< OcNode >& node )
{
        return os << " node depth: " << node.center_position() << "/" << node.max_depth()
                  << " edge length: " << node.edge_length()
                  << " position: " << node.center_position() << " size: " << node.size();
}

constexpr std::array< point< 3, oc_frame >, 8 > CUBE_CORNERS{
    point< 3, oc_frame >{ 1, 1, 1 },
    point< 3, oc_frame >{ -1, 1, 1 },
    point< 3, oc_frame >{ 1, -1, 1 },
    point< 3, oc_frame >{ -1, -1, 1 },
    point< 3, oc_frame >{ 1, 1, -1 },
    point< 3, oc_frame >{ -1, 1, -1 },
    point< 3, oc_frame >{ 1, -1, -1 },
    point< 3, oc_frame >{ -1, -1, -1 } };

constexpr std::array< vec< 3 >, 3 > CUBE_NORMALS{
    vec< 3 >{ 0, 0, 1 },
    vec< 3 >{ 0, 1, 0 },
    vec< 3 >{ 1, 0, 0 },
};

template < typename Node >
constexpr std::array< point< 3, oc_frame >, 8 >
get_node_corners( const oc_node_view< Node >& nview )
{
        return em::map_f_to_a( CUBE_CORNERS, [&]( const point< 3, oc_frame >& p ) {
                return p * *nview.edge_length() / 2.f + cast_to_vec( nview.center_position() );
        } );
}

}  // namespace schpin
