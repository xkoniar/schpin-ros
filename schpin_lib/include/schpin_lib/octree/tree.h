#include "schpin_lib/octree/node.h"
#include "schpin_lib/octree/visitors.h"

#include <schpin_lib/logging.h>

#pragma once

namespace schpin
{

template < typename Content >
class oc_tree
{
public:
        using node_content    = Content;
        using node            = oc_node< Content >;
        using const_node      = const oc_node< Content >;
        using node_view       = oc_node_view< node >;
        using const_node_view = oc_node_view< const_node >;

        oc_tree() = default;
        oc_tree( em::length root_length, Content default_content = {} )
          : root_( default_content )
          , root_context_{ 0u, 0u, root_length, point< 3, oc_frame >{ 0, 0, 0 } }
          , default_content_( std::move( default_content ) )
        {
        }

        oc_tree( oc_tree&& ) = default;
        oc_tree& operator=( oc_tree&& ) = default;

        oc_tree( const oc_tree& ) = delete;
        oc_tree& operator=( const oc_tree& ) = delete;

        oc_tree copy() const
        {
                return oc_tree( root_.copy(), root_context_, default_content_ );
        }

        const_node_view root_view() const
        {
                return { root_, root_context_ };
        }

        const node& bare_root() const
        {
                return root_;
        }

        template < em::range_container Container >
        requires( std::same_as< typename Container::value_type::tag, oc_frame > ) void expand(
            const Container& cont )
        {
                for ( const auto& p : cont ) {
                        expand( p );
                }
        }

        void expand( const point< 3, oc_frame >& p )
        {
                auto aroot = root_view();
                if ( aroot.overlaps( p ) ) {
                        return;
                }

                oc_child_index child_index = get_oc_child_index( p, aroot.center_position() );

                indent_root( child_index );

                root_context_ = oc_upper_context( root_context_, child_index );

                expand( p );
        }

        const oc_node_context& root_context() const
        {
                return root_context_;
        }

        const Content& default_content() const
        {
                return default_content_;
        }

        template < oc_visitor Visitor >
        void modify( const Visitor& visitor )
        {
                modify( visitor, visitor.default_info() );
        }

        template < oc_visitor Visitor >
        void modify( const Visitor& visitor, const typename Visitor::info_type& info )
        {
                node_view node{ root_, root_context_ };
                modify_node( node, visitor, info );
        }

        // TODO: is here because of tests, remove
        oc_tree( oc_node< Content > root, oc_node_context context, Content content )
          : root_( std::move( root ) )
          , root_context_( context )
          , default_content_( std::move( content ) )
        {
        }

private:
        oc_node< Content > root_;
        oc_node_context    root_context_;
        Content            default_content_;

        friend ::nlohmann::adl_serializer< schpin::oc_tree< Content > >;

        void indent_root( std::size_t i )
        {
                std::unique_ptr new_layer_ptr =
                    oc_node< Content >::make_container_ptr( default_content_ );
                std::swap( ( *new_layer_ptr )[i], root_ );

                root_ = oc_node< Content >{ default_content_, std::move( new_layer_ptr ) };
                root_.force_update();
        }

        template < typename Visitor >
        void modify_node(
            node_view&                         nview,
            const Visitor&                     vis,
            const typename Visitor::info_type& parent_info )
        {
                SCHPIN_ASSERT( nview.edge_length() != em::length{ 0.f } );
                const const_node_view&      cnview{ *nview, nview.context() };
                typename Visitor::info_type new_info;

                if ( nview.has_max_depth() ) {
                        vis.on_leaf( cnview, parent_info, **nview );
                        return;
                }

                if ( cnview.empty() ) {
                        auto [ctl, info] = vis.on_locked_leaf( cnview, parent_info );

                        if ( ctl == oc_leaf_control::END ) {
                                return;
                        }

                        auto new_children_ptr = node::make_container_ptr( **nview );
                        *nview   = node{ std::move( **nview ), std::move( new_children_ptr ) };
                        new_info = std::move( info );
                } else {
                        auto [ctl, info] = vis.on_node( cnview, parent_info );
                        if ( ctl == oc_descent_control::END ) {
                                return;
                        }
                        new_info = std::move( info );
                }

                if ( !cnview.empty() ) {
                        for ( node_view& child : nview.generate_children() ) {
                                modify_node( child, vis, new_info );
                        }
                }

                ( *nview )->on_child_update( *cnview );
                nview->try_prunning();
        }
};

}  // namespace schpin

template < typename Content >
struct std::hash< schpin::oc_tree< Content > >
{
        std::size_t operator()( const schpin::oc_tree< Content >& tree )
        {
                return std::hash< schpin::oc_node< Content > >()( tree.bare_root() ) ^
                       std::hash< schpin::oc_node_context >()( tree.root_context() );
        }
};
