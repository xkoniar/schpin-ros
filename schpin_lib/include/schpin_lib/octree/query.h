#include "schpin_lib/octree/node_view.h"

#pragma once

namespace schpin
{

template < oc_node_derived Node, typename Tag >
class sat_oc_node_collision_query
{
public:
        using points_container = std::array< point< 3, Tag >, 8 >;
        using axes_container   = std::array< vec< 3 >, 3 >;
        using tag              = Tag;

private:
        oc_node_view< Node > nview_;
        points_container     points_;
        axes_container       intersection_axes_;

public:
        sat_oc_node_collision_query( const oc_node_view< Node >& nview, const pose< Tag >& offset )
          : nview_( nview )
        {
                points_ = em::map_f_to_a(
                    get_node_corners( nview ), [&]( const point< 3, oc_frame >& p ) {
                            return transform( p, offset );
                    } );

                intersection_axes_ = em::map_f_to_a( CUBE_NORMALS, [&]( const vec< 3 >& v ) {
                        return transform( v, offset );
                } );
        }

        template < typename U = Tag >
        requires( std::same_as< U, oc_frame > )
            sat_oc_node_collision_query( const oc_node_view< Node >& nview )
          : nview_( nview )
        {
                points_            = get_node_corners( nview );
                intersection_axes_ = CUBE_NORMALS;
        }

        const oc_node_view< Node >& node_view() const
        {
                return nview_;
        }

        constexpr const points_container& points() const
        {
                return points_;
        }

        constexpr const em::view< const vec< 3 >* > separation_axes() const
        {
                return { nullptr, nullptr };
        }

        constexpr const axes_container& intersection_axes() const
        {
                return intersection_axes_;
        }
};

template < oc_node_derived Node >
sat_oc_node_collision_query( const oc_node_view< Node >& )
    -> sat_oc_node_collision_query< Node, oc_frame >;

template < oc_node_derived Node, typename Tag >
class bs_oc_node_collision_query
{
public:
        using tag = Tag;

private:
        em::radius           rad_;
        point< 3, Tag >      pos_;
        oc_node_view< Node > view_;
        pose< Tag >          offset_{};

public:
        bs_oc_node_collision_query( const oc_node_view< Node >& nview, const pose< Tag >& offset )
          : bs_oc_node_collision_query(
                sqrt( 3.f * pow< 2 >( nview.edge_length() ) ),
                transform( nview.center_position(), offset ),
                nview,
                offset )
        {
        }

        template < typename U = Tag >
        requires( std::same_as< U, oc_frame > )
            bs_oc_node_collision_query( const oc_node_view< Node >& nview )
          : bs_oc_node_collision_query(
                sqrt( 3.f * pow< 2 >( nview.edge_length() ) ),
                nview.center_position(),
                nview )
        {
        }

        bs_oc_node_collision_query(
            em::radius             rad,
            const point< 3, Tag >& pos,
            oc_node_view< Node >   nview,
            pose< Tag >            off = {} )
          : rad_( rad )
          , pos_( pos )
          , view_( nview )
          , offset_( off )
        {
        }

        const oc_node_view< Node >& node_view() const
        {
                return view_;
        }

        constexpr em::radius min_sphere_radius() const
        {
                return rad_;
        }

        constexpr const point< 3, Tag >& min_sphere_position() const
        {
                return pos_;
        }

        constexpr bool is_leaf() const
        {
                return view_.is_leaf();
        }

        constexpr float descent_priority() const
        {
                return *rad_;
        }

        constexpr std::array< bs_oc_node_collision_query, 8 > children() const
        {

                return em::map_f_to_a(
                    view_.generate_children(), [&]( oc_node_view< Node > child ) {
                            return bs_oc_node_collision_query{
                                rad_ / 2.f,
                                transform( child.center_position(), offset_ ),
                                child,
                                offset_ };
                    } );
        }

        constexpr sat_oc_node_collision_query< Node, Tag > sat() const
        {
                return sat_oc_node_collision_query{ view_, offset_ };
        }
};

template < oc_node_derived Node >
bs_oc_node_collision_query( const oc_node_view< Node >& )
    -> bs_oc_node_collision_query< Node, oc_frame >;
}  // namespace schpin
