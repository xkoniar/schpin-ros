#include "schpin_lib/octree/base.h"

#pragma once

namespace schpin
{

template < typename Content >
class oc_node
{
        static_assert( oc_content< Content > );

public:
        using content_type   = Content;
        using container      = std::array< oc_node, 8 >;
        using iterator       = oc_node*;
        using const_iterator = const oc_node*;

        static std::unique_ptr< container > make_container_ptr( const Content& content )
        {
                return std::make_unique< container >(
                    map_f_to_a< 8 >( em::range( 8u ), [&]( std::size_t ) {
                            return oc_node{ content };
                    } ) );
        }

        explicit oc_node( Content content, std::unique_ptr< container > children )
          : children_ptr_( std::move( children ) )
          , content_( std::move( content ) )
        {
        }

        explicit oc_node( const Content& content )
          : content_( content )
        {
        }

        explicit oc_node( Content&& content = {} )
          : content_( std::move( content ) )
        {
        }

        oc_node( oc_node&& ) = default;
        oc_node& operator=( oc_node&& ) = default;

        oc_node( const oc_node& ) = delete;
        oc_node& operator=( const oc_node& ) = delete;

        oc_node copy() const
        {
                std::unique_ptr< container > ptr;
                if ( children_ptr_ ) {
                        ptr = std::make_unique< container >(
                            em::map_f_to_a( *children_ptr_, []( const oc_node& n ) {
                                    return n.copy();
                            } ) );
                }

                return oc_node{ content_, std::move( ptr ) };
        }

        constexpr std::size_t size() const
        {
                if ( children_ptr_ ) {
                        return children_ptr_->size();
                }
                return 0;
        }

        constexpr oc_node& operator[]( std::size_t i )
        {
                return ( *children_ptr_ )[i];
        }

        constexpr const oc_node& operator[]( std::size_t i ) const
        {
                return ( *children_ptr_ )[i];
        }

        constexpr bool empty() const
        {
                return !children_ptr_;
        }

        constexpr bool is_leaf() const
        {
                return empty();
        }

        constexpr iterator begin()
        {
                if ( children_ptr_ ) {
                        return children_ptr_->begin();
                }
                return nullptr;
        }
        constexpr const_iterator begin() const
        {
                if ( children_ptr_ ) {
                        return children_ptr_->begin();
                }
                return nullptr;
        }
        constexpr iterator end()
        {
                if ( children_ptr_ ) {
                        return children_ptr_->end();
                }
                return nullptr;
        }
        constexpr const_iterator end() const
        {
                if ( children_ptr_ ) {
                        return children_ptr_->end();
                }
                return nullptr;
        }

        constexpr Content& operator*()
        {
                return content_;
        }

        constexpr Content* operator->()
        {
                return &content_;
        }
        constexpr const Content& operator*() const
        {
                return content_;
        }
        constexpr const Content* operator->() const
        {
                return &content_;
        }

        friend bool
        operator<=>( const oc_node< Content >& lh, const oc_node< Content >& rh ) = default;

        void force_update()
        {
                content_.on_child_update( *this );
        }

        void try_prunning()
        {
                if ( empty() ) {
                        return;
                }

                bool has_grandchildren = em::any_of( *this, [&]( const oc_node< Content >& node ) {
                        return !node.empty();
                } );
                if ( has_grandchildren ) {
                        return;
                }

                const oc_node< Content >& cref = *this;
                if ( content_.prunnable( cref ) ) {
                        content_.set_prunned( cref );
                        children_ptr_.reset();
                }
        }

        constexpr std::size_t recursive_count() const
        {
                if ( is_leaf() ) {
                        return 1;
                }

                return 1 + em::sum( *children_ptr_, []( const oc_node& child ) {
                               return child.recursive_count();
                       } );
        }

        constexpr std::size_t recursive_memory_usage() const
        {
                std::size_t res = content_.memory_usage + sizeof( children_ptr_ );

                if ( !is_leaf() ) {
                        res += em::sum( *children_ptr_, []( const oc_node& child ) {
                                return child.recursive_memory_usage();
                        } );
                }

                return res;
        }

private:
        std::unique_ptr< container > children_ptr_;
        Content                      content_;
};

namespace detail
{
        template < typename Content >
        constexpr bool oc_node_derived_test( const oc_node< Content >& )
        {
                return true;
        }
}  // namespace detail

template < typename T >
concept oc_node_derived = requires( T val )
{
        detail::oc_node_derived_test( val );
};

}  // namespace schpin

template < typename Content >
struct std::hash< schpin::oc_node< Content > >
{
        std::size_t operator()( const schpin::oc_node< Content >& node )
        {
                return em::accumulate(
                    node,
                    std::hash< Content >()( *node ),
                    [&]( std::size_t h, const schpin::oc_node< Content >& child ) {
                            return std::rotl( h, 4 ) ^
                                   std::hash< schpin::oc_node< Content > >()( child );
                    } );
                ;
        }
};
