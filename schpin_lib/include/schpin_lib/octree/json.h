#include "schpin_lib/octree/node.h"
#include "schpin_lib/octree/tree.h"

#pragma once

template < typename Content >
struct nlohmann::adl_serializer< schpin::oc_node< Content > >
{
        static schpin::oc_node< Content > from_json( const nlohmann::json& j )
        {
                nlohmann::json j_children = j.at( "children" );

                auto content = j.at( "content" ).get< Content >();

                if ( j_children.empty() ) {
                        return schpin::oc_node< Content >( content );
                }

                using cont = typename schpin::oc_node< Content >::container;

                auto vec = j_children.get< std::vector< schpin::oc_node< Content > > >();
                if ( vec.size() != 8 ) {
                        throw nlohmann::json::parse_error::create(
                            -1, 0, "More than 8 octree children" );
                }
                return schpin::oc_node< Content >(
                    content,
                    std::make_unique< cont >( emlabcpp::map_f_to_a< 8 >( std::move( vec ) ) ) );
        }

        static void to_json( nlohmann::json& j, const schpin::oc_node< Content >& node )
        {
                j["content"]  = *node;
                j["children"] = em::view{ node };
        }
};

template <>
struct nlohmann::adl_serializer< schpin::oc_node_context >
{
        static schpin::oc_node_context from_json( const nlohmann::json& j )
        {
                return {
                    j.at( "depth" ).get< unsigned >(),
                    j.at( "max_depth" ).get< unsigned >(),
                    j.at( "length" ).get< emlabcpp::length >(),
                    j.at( "position" ).get< schpin::point< 3, schpin::oc_frame > >() };
        }

        static void to_json( nlohmann::json& j, const schpin::oc_node_context& context )
        {
                j["depth"]     = context.depth;
                j["max_depth"] = context.max_depth;
                j["length"]    = context.edge_length;
                j["position"]  = context.center_position;
        }
};

template < typename Content >
struct nlohmann::adl_serializer< schpin::oc_tree< Content > >
{
        static schpin::oc_tree< Content > from_json( const nlohmann::json& j )
        {
                auto root    = j.at( "root" ).get< schpin::oc_node< Content > >();
                auto context = j.get< schpin::oc_node_context >();

                schpin::oc_tree< Content > tree{
                    std::move( root ),
                    std::move( context ),
                    j.at( "default_content" ).get< Content >(),
                };

                return tree;
        }

        static void to_json( nlohmann::json& j, const schpin::oc_tree< Content >& tree )
        {
                j = tree.root_context();

                j["root"]            = tree.bare_root();
                j["default_content"] = tree.default_content();
        }
};
