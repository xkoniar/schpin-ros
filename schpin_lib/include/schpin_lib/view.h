// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/iterators/access.h"
#include "emlabcpp/view.h"
#include "schpin_lib/types.h"

#ifdef SCHPIN_USE_NLOHMANN_JSON
#include <nlohmann/json.hpp>
#endif

#pragma once

namespace em = emlabcpp;

namespace schpin
{

#ifdef SCHPIN_USE_NLOHMANN_JSON
// Serialization for the existing view of data using nlohmann's json library
template < typename Iter >
inline void to_json( nlohmann::json& j, const em::view< Iter >& v )
{
        for ( const auto& item : v ) {
                j.push_back( item );
        }
}
#endif

}  // namespace schpin
