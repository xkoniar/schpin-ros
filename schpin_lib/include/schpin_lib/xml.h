// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/error.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <filesystem>

#pragma once

namespace schpin
{

namespace pt = ::boost::property_tree;

using xml_storage_error = error< struct xml_storage_error_tag >;

template < typename T, typename UnaryFunction >
em::either< T, xml_storage_error > safe_load_xml( std::istream& sstream, UnaryFunction f )
{
        try {
                pt::ptree tree;
                pt::read_xml( sstream, tree );
                return { f( tree ) };
        }
        catch ( const pt::xml_parser::xml_parser_error& e ) {
                return { E( xml_storage_error ) << "Failed to parse the XML file:" << e.what() };
        }
        catch ( const pt::ptree_error& e ) {
                return { E( xml_storage_error ) << "Failed to extract XML data:" << e.what() };
        }
}

}  // namespace schpin
