// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/logging.h"

#include <ament_index_cpp/get_package_share_directory.hpp>
#include <bitset>
#include <regex>
#include <string>

#pragma once

namespace schpin
{

template < typename T >
std::string to_string( const T& val )
{
        std::stringstream ss;
        ss << val;
        return ss.str();
}

// rotates the bites of bitset by 'n'
template < std::size_t N >
inline std::bitset< N > rotated( const std::bitset< N >& b, std::size_t m )
{
        m = m % N;
        return b << m | b >> ( N - m );
}

// resolves the path to file via ros mechanics if present - universal
inline std::string get_resolved_path( std::string filename )
{
        std::regex package_regex(
            "package://([a-zA-Z0-9_]+)/([a-zA-Z0-9_/.]*)", std::regex_constants::ECMAScript );
        std::smatch match;
        if ( std::regex_match( filename, match, package_regex ) ) {
                SCHPIN_ASSERT( match.size() == 3 );
                return ament_index_cpp::get_package_share_directory( match[1].str() ) + "/" +
                       match[2].str();
        }
        return filename;
}

}  // namespace schpin
