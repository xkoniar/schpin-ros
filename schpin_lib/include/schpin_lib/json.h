// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "schpin_lib/error.h"

#include <fstream>
#include <iomanip>
#include <nlohmann/json.hpp>

namespace schpin
{

using json_storage_error = error< struct json_storage_error_tag >;

// The call of `f` is guarded against json exceptions which are catched and stored into
// json_storage_error.
template < typename UnaryFunction >
inline auto json_guard( UnaryFunction f ) -> opt_error< json_storage_error >
{
        try {
                f();
                return {};
        }
        catch ( const nlohmann::json::exception& e ) {
                return { E( json_storage_error ) << "Failed to parse the JSON: " << e.what() };
        }
}

// Loads json from 'input' stream and passes it to function 'f', which should return type T -
// expected output, the call of function 'f' is guarded against expected exception calls and
// converts them to json_storage_error
template < typename T, typename UnaryFunction >
inline em::either< T, json_storage_error > safe_load_json( std::istream& input, UnaryFunction f )
{

        if ( !input ) {
                return { E( json_storage_error ) << "The input json file is missing" };
        }

        nlohmann::json j;

        try {
                input >> j;
                std::optional< T > val;
                return json_guard( [&]() {
                               val = f( std::move( j ) );
                       } )
                    .to_either( std::move( *val ) );
        }
        catch ( const std::ios_base::failure& e ) {
                return { E( json_storage_error ) << " Failed to read the file: " << e.what() };
        }
}

// Safely extracts instance of tpye T serialized as json out of input stream, where safely means
// that errors are reported via json_storage_error
template < typename T >
inline em::either< T, json_storage_error > safe_load_json( std::istream& input )
{
        return safe_load_json< T >( input, []( const nlohmann::json& j ) {
                return j.get< T >();
        } );
}

// Stores the result of call to the function 'f' into the 'output' stream provided as argument.
// The function 'f' is expected to do the conversion to json, as it is guarded againts json-related
// exceptions which are stored into optional json_storage_error as result.
template < typename Function, typename = std::enable_if_t< std::is_invocable_v< Function > > >
inline opt_error< json_storage_error > safe_store_json( std::ostream& output, Function f )
{

        try {
                output << f() << std::endl;
        }
        catch ( const nlohmann::json::exception& e ) {
                return {
                    E( json_storage_error )
                    << "JSON Exception raised when tried to store json: " << e.what() };
        }
        catch ( const std::ios_base::failure& e ) {
                return { E( json_storage_error ) << "Failed to write to json file: " << e.what() };
        }

        return {};
}

// Stores the 'item' of type 'T' as a json to the output stream
template < typename T, typename = std::enable_if_t< !std::is_invocable_v< T > > >
inline opt_error< json_storage_error > safe_store_json( std::ostream& output, const T& item )
{
        return safe_store_json( output, [&] {
                return nlohmann::json( item );
        } );
}

}  // namespace schpin
