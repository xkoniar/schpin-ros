// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/iterators/numeric.h"
#include "schpin_lib/algorithm.h"

#include <iomanip>
#include <ostream>

#pragma once

namespace schpin
{

// Enum of standard color codes used in the terminal
enum class term_color
{
        RESET   = 0,
        RED     = 31,
        BLUE    = 34,
        YELLOW  = 33,
        CYAN    = 36,
        MAGENTA = 35,
        GRAY    = 90
};

inline std::string colorize( const std::string& str, term_color c )
{
        return "\033[0;" + std::to_string( int( c ) ) + "m" + str + "\033[0;0m";
}

// Changes the color of the terminal to the specified color
inline std::ostream& operator<<( std::ostream& os, term_color c )
{
        os << "\033[0;" << int( c ) << "m";
        return os;
}

// Data structure to store two-dimensional table of strings, which can be printed on the stream
template < unsigned N >
struct print_table
{
        std::vector< std::array< std::string, N > > rows;

        print_table( std::array< std::string, N > header )
        {
                rows.push_back( header );
        }

        void add_row( std::array< std::string, N > row )
        {
                rows.push_back( row );
        }
};

template < unsigned N >
inline std::ostream& operator<<( std::ostream& os, const print_table< N >& table )
{
        std::array< unsigned, N > col_widths =
            em::map_f_to_a< N >( em::range( N ), [&]( const std::size_t i ) {
                    return max( table.rows, [&]( const std::array< std::string, N >& arr ) {  //
                            return arr[i].size();
                    } );
            } );

        for ( const std::array< std::string, N >& row : table.rows ) {
                for ( std::size_t i : em::range( N ) ) {
                        if ( i > 0 ) {
                                os << " | ";
                        }
                        os << std::setw( col_widths[i] ) << row[i];
                }
                os << std::endl;
        }
        return os;
}

}  // namespace schpin
