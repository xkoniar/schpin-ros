// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/algorithm.h"
#include "schpin_lib/types.h"
#include "schpin_lib/view.h"

#ifdef SCHPIN_USE_STREAMS
#include <ostream>
#endif

#include <cmath>
#include <cstdlib>
#include <tuple>
#include <vector>

#pragma once

namespace em = emlabcpp;

namespace schpin
{

#ifdef SCHPIN_USE_STREAMS
template < typename T >
inline std::ostream& operator<<( std::ostream& os, const em::min_max< T >& mm )
{
        return os << "Min: " << mm.min << " Max: " << mm.max;
}
#endif

// calss function f(x) for each item 'x' in container 'cont', the result are pushed to the and of
// result vector. The type of vector is decide based on return value of f(x)
template <
    typename Container,
    typename UnaryFunction = std::identity,
    typename T             = std::decay_t< em::mapped_t< Container, UnaryFunction > > >
[[nodiscard]] inline std::vector< T >
map_f_to_v( Container&& cont, UnaryFunction&& f = std::identity() )
{
        std::vector< T > res;
        res.reserve( em::cont_size( cont ) );
        em::for_each( cont, [&]< typename Item >( Item&& item ) {
                res.push_back( f( std::forward< Item >( item ) ) );
        } );
        return res;
}

// returns vector containg all items from LhContainer and RhContainer
template <
    typename LhContainer,
    typename RhContainer,
    typename T = typename std::decay_t< LhContainer >::value_type >
[[nodiscard]] inline std::vector< T > merge( LhContainer&& lh, RhContainer&& rh )
{
        std::vector< T > res;
        res.reserve( size( lh ) + size( rh ) );
        using namespace std;
        res.insert( res.end(), begin( lh ), end( lh ) );
        res.insert( res.end(), begin( rh ), end( rh ) );
        return res;
}

namespace detail
{
        template < typename OutF, typename Arg, typename... Args >
        void combine_impl( OutF&& f, const Arg& a, const Args&... args )
        {
                for ( auto i : a ) {
                        if constexpr ( sizeof...( Args ) == 0 ) {
                                f( i );
                        } else {
                                combine_impl(
                                    [&]( typename Args::value_type... subargs ) {  //
                                            f( i, subargs... );
                                    },
                                    args... );
                        }
                }
        }
}  // namespace detail

template < typename... Args >
auto combine( const Args&... args )
{
        using result_t = std::vector< std::tuple< typename Args::value_type... > >;

        result_t res;
        detail::combine_impl(
            [&]( typename Args::value_type... subargs ) {  //
                    res.emplace_back( subargs... );
            },
            args... );
        return res;
}

}  // namespace schpin
