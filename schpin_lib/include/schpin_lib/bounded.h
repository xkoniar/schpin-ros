
#include <emlabcpp/bounded.h>
#ifdef SCHPIN_USE_NLOHMANN_JSON
#include <nlohmann/json.hpp>
#endif

#pragma once

#ifdef SCHPIN_USE_NLOHMANN_JSON

namespace emlabcpp
{
template < typename T, T MinVal, T MaxVal >
inline void from_json( const nlohmann::json& j, bounded< T, MinVal, MaxVal >& b )
{
        auto opt_val = bounded< T, MinVal, MaxVal >::make( j.get< T >() );
        if ( !opt_val ) {
                throw nlohmann::json::out_of_range::create(
                    -1, "Bounded variable from json is out of range" );
        }
        b = *opt_val;
}

template < typename T, T MinVal, T MaxVal >
inline void to_json( nlohmann::json& j, const bounded< T, MinVal, MaxVal >& b )
{
        j = *b;
}
}  // namespace emlabcpp

#endif
