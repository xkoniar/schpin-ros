// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/color.h"
#include "schpin_lib/geom/pose.h"

#include <urdf/model.h>
#include <urdf_model/joint.h>
#include <urdf_model/link.h>
#pragma once

namespace schpin
{

inline color convert_urdf_color( urdf::Color c )
{
        return color{ c.r, c.g, c.b };
}

template < typename Tag >
inline point< 3, Tag > convert_urdf_point( urdf::Vector3 v )
{
        return point< 3, Tag >{ float( v.x ), float( v.y ), float( v.z ) };
}

template < typename Tag >
inline quaternion< Tag > convert_urdf_quaternion( urdf::Rotation r )
{
        return quaternion< Tag >{ float( r.x ), float( r.y ), float( r.z ), float( r.w ) };
}

template < typename Tag >
inline pose< Tag > convert_urdf_pose( urdf::Pose p )
{
        return pose< Tag >{
            convert_urdf_point< Tag >( p.position ), convert_urdf_quaternion< Tag >( p.rotation ) };
}
}  // namespace schpin
