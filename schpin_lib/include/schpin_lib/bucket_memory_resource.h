#include <bitset>
#include <emlabcpp/iterators/numeric.h>
#include <memory_resource>

#pragma once

// this is stupid and should be burned...
namespace em = emlabcpp;

namespace schpin
{
template < typename T >
struct bucket_memory_resource_deleter
{
        std::pmr::memory_resource* res;

        void operator()( T* item )
        {
                std::destroy_at( item );
                res->deallocate( item, sizeof( T ), alignof( T ) );
        }
};

template < std::size_t BucketCount, std::size_t BucketSize >
class bucket_memory_resource : public std::pmr::memory_resource
{
        using bucket = std::byte[BucketSize];

        bucket                     buckets_[BucketCount];
        std::bitset< BucketCount > taken_ = 0;

public:
        static constexpr std::size_t bucket_count = BucketCount;
        static constexpr std::size_t bucket_size  = BucketSize;

        std::size_t used() const
        {
                return taken_.count();
        }

protected:
        virtual void* do_allocate( std::size_t bytes, std::size_t alignment ) override
        {
                // TODO: maybe improve this?
                if ( taken_.all() ) {
#ifdef __EXCEPTIONS
                        throw std::bad_alloc{};
#else
                        std::abort();
#endif
                }
                std::size_t spot_i = 0;
                for ( std::size_t i : em::range( taken_.size() ) ) {
                        if ( !taken_[i] ) {
                                spot_i    = i;
                                taken_[i] = true;
                                break;
                        }
                }
                std::size_t space = BucketSize;
                void*       mem   = buckets_[spot_i];
                return std::align( alignment, bytes, mem, space );
        }

        virtual void do_deallocate( void* p, std::size_t, std::size_t )
        {
                // TODO: we can assert that 'p' is from this
                std::size_t buffer_i = BucketCount;
                for ( std::size_t i : em::range( BucketCount ) ) {
                        if ( buckets_[BucketCount - 1 - i] <= p ) {
                                buffer_i = BucketCount - 1 - i;
                                break;
                        }
                }

                if ( buffer_i == BucketCount ) {
#ifdef __EXCEPTIONS
                        throw std::bad_alloc{};
#else
                        std::abort();
#endif
                }

                taken_[buffer_i] = 0;
        }

        virtual bool do_is_equal( const std::pmr::memory_resource& other ) const noexcept
        {
                return this == &other;
        }
};

}  // namespace schpin
