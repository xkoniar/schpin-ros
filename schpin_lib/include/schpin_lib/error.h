// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/algorithm.h"
#include "schpin_lib/either.h"
#include "schpin_lib/logging.h"
#include "schpin_lib/print.h"

#include <optional>
#ifdef SCHPIN_USE_STREAMS
#include <sstream>
#endif
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#pragma once

namespace schpin
{

// Macro to construct errors, needed because of __PRETTY_FUNCTION__ macro
#define E( error_t ) error_t( #error_t, __PRETTY_FUNCTION__ )

// Internal data structure stored by the error
struct error_data
{
        std::vector< error_data >  suberrors_{};
        std::vector< std::string > collection_{};
        std::string                msg_{};
        std::string                function_name_{};
        std::string                error_name_{};
};

// The guts of error printing mechanism. Recursivelly traverses the error_data and prints them.
inline void print_error(
    const error_data& data,
    std::ostream&     os,
    bool              print_function_name,
    std::string       prefix  = "",
    bool              is_last = true )
{
        os << prefix << "+-" << term_color::RED << data.error_name_ << term_color::RESET << "\n";

        prefix += is_last ? "  " : "| ";

        if ( print_function_name ) {
                os << prefix << "+-" << data.function_name_ << "\n";
        }

        if ( !data.msg_.empty() ) {
                os << prefix << "+-" << data.msg_ << "\n";
        }
        for ( const std::string& msg : data.collection_ ) {
                os << prefix << "+-" << msg << "\n";
        }
        for ( std::size_t i : em::range( data.suberrors_.size() ) ) {
                print_error(
                    data.suberrors_[i],
                    os,
                    print_function_name,
                    prefix,
                    i == data.suberrors_.size() - 1 );
        }
}

// Error is fancy datatype that is instantiated by custom phantom type - Tag.
// This gives as specific error which when created - stores function where it was created, gives
// abillity to append human readable description, subdata and compose the errors into a tree.
//
// This is designed as human-readable errors and is not optimal performance-wise.
template < typename Tag >
class error
{
        error_data data_;

public:
        explicit error( std::string error_name, std::string function_name )
        {
                data_.error_name_    = std::move( error_name );
                data_.function_name_ = std::move( function_name );
        }

        template < typename OtherTag >
        error( const error< OtherTag >& other )
        {
                *this << "Error converted from:";
                attach( other );
        }

        error( error&& other ) noexcept = default;
        error( const error& other )     = default;
        error& operator=( error&& other ) noexcept = default;
        error& operator=( const error& other ) = default;

        const error_data& operator*() const
        {
                return data_;
        }
        const error_data* operator->() const
        {
                return &data_;
        }

        // Appends data to the error message
        template < typename T >
        error& operator<<( const T& item )
        {
                std::stringstream ss;
                ss << item;
                data_.msg_ += ss.str();
                return *this;
        }

        // Appends sub-data to the error message
        template < typename T >
        error& attach( const T& t )
        {
                std::stringstream ss;
                ss << t;
                data_.collection_.emplace_back( ss.str() );
                return *this;
        }

        // Appends error as suberrors.
        template < typename OtherTag >
        error& attach( const error< OtherTag >& other )
        {
                data_.suberrors_.emplace_back( *other );
                return *this;
        }

        template < typename Container >
        error& group_attach( const Container& data )
        {
                em::for_each( data, [&]( const auto& other_error ) {  //
                        this->attach( other_error );
                } );
                return *this;
        }

        ~error() = default;
};

// Prints the error
template < typename Tag >
std::ostream& operator<<( std::ostream& os, const error< Tag >& error )
{
        print_error( *error, os, true );
        return os;
}

template < typename Error >
struct simplified_error
{
        const Error& e;

        simplified_error( const Error& ee )
          : e( ee )
        {
        }
};

template < typename Error >
std::ostream& operator<<( std::ostream& os, const simplified_error< Error >& error )
{
        print_error( *error.e, os, false );
        return os;
}

// Our custom std::optional with better handling.
template < typename Error >
class [[nodiscard]] opt_error
{
        std::optional< Error > error_;

public:
        using value_type = Error;

        opt_error( Error err )
          : error_( err )
        {
        }

        opt_error() = default;

        const Error& operator*() const
        {
                return *error_;
        }

        template < typename T >
        em::either< T, Error > to_either( T val )
        {
                if ( error_ ) {
                        return { *error_ };
                }
                return { std::move( val ) };
        }

        template < typename UnaryFunction >
        auto convert( UnaryFunction f ) -> opt_error< decltype( f( *error_ ) ) >
        {
                if ( error_ ) {
                        return { f( *error_ ) };
                }
                return {};
        }

        template < typename UnaryFunction >
        void optionally( UnaryFunction f )
        {
                if ( error_ ) {
                        f( *error_ );
                }
        }

        void optionally_log()
        {
                if ( error_ ) {
                        SCHPIN_ERROR_LOG( "error", *error_ );
                }
        }

        operator bool() const
        {
                return bool( error_ );
        }
};
}  // namespace schpin
