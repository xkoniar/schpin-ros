// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/chain.h"
#include "schpin_lib_exp/rviz.h"

#pragma once

namespace schpin
{

// Creates a basic marker for visuaization of links
inline visualization_msgs::msg::Marker
create_chain_link_marker( unsigned id, const std::string& frame, const std::string& ns )
{
        visualization_msgs::msg::Marker res = create_marker( ns + "-link", id, frame );

        res.type    = visualization_msgs::msg::Marker::LINE_LIST;
        res.scale.x = 0.01;
        res.scale.y = 0.01;
        res.scale.z = 0.01;

        res.color.r = 0.f;
        res.color.g = 1.f;
        res.color.b = 0.f;

        return res;
}
// Creates a basic marker for visualization of pins
inline visualization_msgs::msg::Marker
create_chain_pin_marker( unsigned id, const std::string& frame, const std::string& ns )
{
        visualization_msgs::msg::Marker res = create_marker( ns + "-pin", id, frame );

        res.type    = visualization_msgs::msg::Marker::SPHERE_LIST;
        res.scale.x = 0.01;
        res.scale.y = 0.01;
        res.scale.z = 0.01;

        res.color.r = 0.f;
        res.color.g = 1.f;
        res.color.b = 0.f;

        return res;
}

// Creates an array of visualizations as a line_list (for links) and sphere_list ( or pins). This
// needs a pin_to_point conversion function that returns a point in 3D for each pin of the chain.
template < typename Chain, typename PinToPointFunction >
visualization_msgs::msg::MarkerArray create_chain_visualization(
    const Chain&         ch,
    const std::string&   frame,
    const std::string&   ns,
    PinToPointFunction&& pin_to_point )
{
        visualization_msgs::msg::Marker link_msg = create_chain_link_marker( 0, frame, ns );
        visualization_msgs::msg::Marker pins_msg = create_chain_pin_marker( 1, frame, ns );

        auto f = [&]( const auto& pin ) -> geometry_msgs::msg::Point {
                return point_to_msg( pin_to_point( pin ) );
        };

        if ( ch.begin() != ch.end() ) {
                pins_msg.points.push_back( f( ch.begin()->source_pin ) );
        }
        for ( const auto& view : ch ) {
                link_msg.points.push_back( f( view.source_pin ) );
                link_msg.points.push_back( f( view.target_pin ) );
                pins_msg.points.push_back( f( view.target_pin ) );
        }

        visualization_msgs::msg::MarkerArray res;
        res.markers = { link_msg, pins_msg };
        return res;
}

}  // namespace schpin
