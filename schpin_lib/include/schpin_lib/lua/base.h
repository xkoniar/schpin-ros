// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/error.h>
#include <schpin_lib/geom/pose.h>
#include <variant>

#pragma once

namespace schpin
{

enum class lua_res
{
        FINISHED,
        YIELDED
};
using slua_error = error< struct lua_error_tag >;
using slua_pose_input_var =
    std::variant< point< 3, world_frame >, quaternion< world_frame >, pose< world_frame > >;
}  // namespace schpin
