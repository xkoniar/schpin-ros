// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/lua/def.h>

#pragma once

namespace schpin
{

struct slua_print_event
{
        std::string msg;
};

struct slua_print_collect_args
{
        using value_type = slua_print_event;

        std::string get_desc()
        {
                return "All arguments are collected into string";
        }

        em::either< slua_print_event, slua_error > extract( lua_State* lua_ptr, int ) const
        {
                std::string res;
                int         n = lua_gettop( lua_ptr );
                std::string sep;

                for ( int i : em::range( 1, n + 1 ) ) {
                        std::size_t str_len = 0;
                        const char* str     = luaL_tolstring( lua_ptr, i, &str_len );
                        if ( str == NULL ) {
                                return E( slua_error ) << "Failed to convert " << i
                                                       << "th print argument to string";
                        }
                        res += sep + str;
                        lua_pop( lua_ptr, 1 );
                        sep = "\t";
                }

                return { slua_print_event{ res } };
        }
};

struct slua_print_bind
{
        static constexpr std::string_view name = "print";
        static constexpr std::string_view desc =
            "Prints data, output channel of this command depends on context.";

        inline static const auto           arg = slua_print_collect_args{};
        inline static const no_return_type res{};
};

inline void to_json( nlohmann::json& j, const slua_print_collect_args& )
{
        nlohmann::json args;
        args["keys"] = { "..." };
        args["desc"] = "Any arguments provided";
        args["type"] = "convertible by luaL_tolstring";

        j["event_type"] = boost::typeindex::type_id< slua_print_event >().pretty_name();
        j["args"]       = { args };
        j["example"]    = "( \"fooo: \", 123 )";
}

}  // namespace schpin
