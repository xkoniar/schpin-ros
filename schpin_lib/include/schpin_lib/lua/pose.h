// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/lua/def.h"

#include <emlabcpp/match.h>
#include <schpin_lib/geom/pose.h>

#pragma once

namespace schpin
{

inline pose< world_frame >
slua_get_real_pose( slua_pose_input_var var, pose< world_frame > actual_pose )
{
        em::match(
            var,
            [&]( point< 3, world_frame > p ) {
                    actual_pose.position = p;
            },
            [&]( quaternion< world_frame > q ) {
                    actual_pose.orientation = q;
            },
            [&]( pose< world_frame > p ) {
                    actual_pose = p;
            } );
        return actual_pose;
}

struct slua_pose_args
{
        using value_type = slua_pose_input_var;
        std::string point_key;
        std::string quat_key;
        std::string description;

        slua_pose_args( std::string pkey, std::string qkey, const std::string& desc )
          : point_key( std::move( pkey ) )
          , quat_key( std::move( qkey ) )
          , description(
                desc + " (you either specifiy both parts, or one of them. The missing one "
                       "is taken from actual pose.)" )
          , point_arg_{ point_key, "" }
          , quat_arg_{ quat_key, "" }
        {
        }

        em::either< value_type, slua_error >
        table_extract( lua_State* lua_ptr, int tbl_index ) const
        {
                auto peither = point_arg_.table_extract( lua_ptr, tbl_index );
                auto qeither = quat_arg_.table_extract( lua_ptr, tbl_index );

                if ( peither.is_left() && !qeither.is_left() ) {
                        return peither.construct_left< slua_pose_input_var >();
                }

                if ( qeither.is_left() && !peither.is_left() ) {
                        return qeither.convert_left( [&]( auto p ) {  //
                                return slua_pose_input_var{ p };
                        } );
                }

                return assemble_left_collect_right( peither, qeither )
                    .convert_left( [&]( auto pack ) {
                            auto [point, quat] = pack;
                            return slua_pose_input_var{ pose< world_frame >{ point, quat } };
                    } )
                    .convert_right( [&]( auto errs ) {
                            return E( slua_error ).group_attach( errs ) << "Failed to parse goal "
                                                                           "pose";
                    } );
        }

private:
        lua_key_arg< point< 3, world_frame > >   point_arg_;
        lua_key_arg< quaternion< world_frame > > quat_arg_;
};

inline void to_json( nlohmann::json& j, const slua_pose_args& item )
{
        j["keys"] = { item.point_key, item.quat_key };
        j["desc"] = item.description;
        j["type"] = lua_serializer< point< 3, world_frame > >::desc_name() + ", " +
                    lua_serializer< quaternion< world_frame > >::desc_name();
        j["example"] = item.point_key + "=" +
                       lua_serializer< point< 3, world_frame > >::desc_example() + ", " +
                       item.quat_key + "=" +
                       lua_serializer< quaternion< world_frame > >::desc_example();
}

}  // namespace schpin
