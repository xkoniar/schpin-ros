// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace schpin
{

struct slua_rosrequire : slua_item
{
        static constexpr const char* searcher = R"EOF(
        function __ros_searcher(modulename)
                pkg, module = string.match(modulename, "^(%S-)%.(%S+)")
                ros_path = "package://"..pkg.."/"
                resolved = __resolve_ros_package_path(ros_path)
                if resolved == ros_path then
                        return nil
                end
                path = resolved.."?.lua;"..resolved.."?/init.lua;"..resolved.."lua/?.lua;"..resolved.."lua/?/init.lua"
                file, error = package.searchpath(module, path)
                if file then
                        return assert(loadfile(file))
                end
                return error
        end

        table.insert(package.searchers, __ros_searcher)
        )EOF";

        std::string name = "";

        static int resolve_ros_package_path( lua_State* l_ptr )
        {
                const char* inpt = luaL_checkstring( l_ptr, 1 );

                std::string resolved = get_resolved_path( std::string{ inpt } );

                lua_pushstring( l_ptr, resolved.c_str() );
                return 1;
        }

        opt_error< slua_error > bind( lua_State* lua_ptr )
        {
                lua_pushcfunction( lua_ptr, resolve_ros_package_path );
                lua_setglobal( lua_ptr, "__resolve_ros_package_path" );
                if ( luaL_dostring( lua_ptr, searcher ) != 0 ) {
                        return { E( slua_error ) << lua_tostring( lua_ptr, -1 ) };
                }

                return {};
        }
};

inline void to_json( nlohmann::json& j, const slua_rosrequire& )
{
        j["type"] = "require handler";
        j["name"] = "ROS require handler";
        j["desc"] =
            "handler for luas require function that searches ROS packages, the first word "
            "before . is used as package name, and the rest is used as path in the package. "
            "So `requres schpin_koke.sequences.wololo` searches for file: "
            "package://schpin_koke/sequences/wololo.lua";
}

}  // namespace schpin
