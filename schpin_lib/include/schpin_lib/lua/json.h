// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <emlabcpp/zip.h>
#include <lauxlib.h>
#include <lualib.h>
#include <nlohmann/json.hpp>
#include <schpin_lib/lua/base.h>
#include <schpin_lib/lua/util.h>

#pragma once

namespace schpin
{

template < typename Function, typename T = std::invoke_result_t< Function > >
inline em::either< T, slua_error > safelly_lua_json( lua_State* lua_ptr, Function f )
{
        std::optional< em::either< T, slua_error > > res;

        auto lcall = []( lua_State* lua_ptr ) -> int {
                void* raw_f_ptr   = lua_touserdata( lua_ptr, -2 );
                auto  f_ptr       = static_cast< Function* >( raw_f_ptr );
                void* raw_res_ptr = lua_touserdata( lua_ptr, -1 );
                auto  res_ptr =
                    static_cast< std::optional< em::either< T, slua_error > >* >( raw_res_ptr );

                try {
                        ( *res_ptr ) = { ( *f_ptr )() };
                }
                catch ( const nlohmann::json::exception& e ) {
                        ( *res_ptr ) = {
                            E( slua_error )
                            << "json exception during lua/json interaction: " << e.what() };
                }
                return 0;
        };

        lua_pushcfunction( lua_ptr, lcall );
        lua_pushlightuserdata( lua_ptr, static_cast< void* >( &f ) );
        lua_pushlightuserdata( lua_ptr, static_cast< void* >( &res ) );
        int lua_res = lua_pcall( lua_ptr, 2, 0, 0 );
        if ( lua_res != LUA_OK ) {
                return {
                    E( slua_error ) << "failed due to lua error: " << lua_tostring( lua_ptr, -1 ) };
        }
        return *res;
}

inline void slua_json_encode( lua_State* lua_ptr, const nlohmann::json& j )
{
        switch ( j.type() ) {
                case nlohmann::json::value_t::null:
                        lua_pushnil( lua_ptr );
                        break;
                case nlohmann::json::value_t::boolean:
                        lua_pushboolean( lua_ptr, static_cast< int >( j.get< bool >() ) );
                        break;
                case nlohmann::json::value_t::string:
                        lua_pushstring( lua_ptr, j.get< std::string >().c_str() );
                        break;
                case nlohmann::json::value_t::number_integer:
                case nlohmann::json::value_t::number_unsigned:
                case nlohmann::json::value_t::number_float:
                        lua_pushnumber( lua_ptr, j.get< lua_Number >() );
                        break;
                case nlohmann::json::value_t::object:
                        lua_createtable( lua_ptr, 0, static_cast< int >( j.size() ) );
                        for ( const auto& [key, value] : j.items() ) {
                                slua_json_encode( lua_ptr, key );
                                slua_json_encode( lua_ptr, value );
                                lua_settable( lua_ptr, -3 );
                        }
                        break;
                case nlohmann::json::value_t::array:
                        lua_createtable( lua_ptr, static_cast< int >( j.size() ), 0 );
                        for ( const auto& [i, value] : em::enumerate( j ) ) {
                                slua_json_encode( lua_ptr, value );
                                lua_rawseti( lua_ptr, -2, static_cast< lua_Integer >( i + 1 ) );
                        }
                        break;
                default:
                        throw nlohmann::json::parse_error::create(
                            -1, 0, "Failed to encode to lua" );
                        break;
        }
}

inline nlohmann::json slua_json_decode( lua_State* lua_ptr, int i )
{
        // TODO: error handling, including errors raised from lua or exceptions from json...
        nlohmann::json res;
        switch ( lua_type( lua_ptr, i ) ) {
                case LUA_TBOOLEAN:
                        res = static_cast< bool >( lua_toboolean( lua_ptr, i ) );
                        break;
                case LUA_TNIL:
                        res = NULL;
                        break;
                case LUA_TNUMBER:
                        res = luaL_checknumber( lua_ptr, i );
                        break;
                case LUA_TSTRING:
                        res = luaL_checkstring( lua_ptr, i );
                        break;
                case LUA_TTABLE:
                        for ( lua_pushnil( lua_ptr ); lua_next( lua_ptr, i ) != 0;
                              lua_pop( lua_ptr, 1 ) ) {
                                int top_i = lua_gettop( lua_ptr );
                                int key_i = top_i - 1;
                                int val_i = top_i - 0;

                                nlohmann::json value = slua_json_decode( lua_ptr, val_i );
                                if ( lua_type( lua_ptr, key_i ) == LUA_TSTRING ) {
                                        std::string skey = luaL_checkstring( lua_ptr, key_i );
                                        res[skey]        = value;
                                } else {
                                        SCHPIN_ASSERT_MSG(
                                            lua_type( lua_ptr, key_i ) == LUA_TNUMBER,
                                            luaL_typename( lua_ptr, key_i ) );
                                        res.push_back( value );
                                }
                        }
                        break;
                default:
                        lua_error_stream(
                            lua_ptr, "Failed to handle lua type: ", lua_type( lua_ptr, i ) );
                        break;
        }
        return res;
}

}  // namespace schpin
