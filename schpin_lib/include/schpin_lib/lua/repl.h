// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <mutex>
#include <queue>
#include <readline/history.h>
#include <readline/readline.h>
#include <thread>

#pragma once

namespace schpin
{

class slua_repl
{
public:
        slua_repl( const slua_repl& ) = delete;
        slua_repl( slua_repl&& )      = delete;
        slua_repl& operator=( const slua_repl& ) = delete;
        slua_repl& operator=( slua_repl&& ) = delete;

        slua_repl() = default;

        std::string prompt()
        {
                std::unique_ptr< char > opt_chars{ readline( ">" ) };
                if ( !opt_chars ) {
                        return "";
                }
                if ( strlen( opt_chars.get() ) > 0 ) {
                        add_history( opt_chars.get() );
                }
                return { opt_chars.get() };
        }
};

}  // namespace schpin
