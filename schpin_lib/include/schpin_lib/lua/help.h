
#pragma once

namespace schpin
{

inline void print_header( std::ostream& os, const nlohmann::json& j )
{
        os << colorize( j["name"].get< std::string >(), term_color::CYAN );
        if ( j["type"] != "global variable" ) {
                os << colorize( "{", term_color::CYAN );
        }
        if ( j.contains( "arg" ) && j["arg"].contains( "args" ) ) {
                std::string delim = " ";
                for ( const nlohmann::json& args : j["arg"]["args"] ) {
                        for ( const nlohmann::json& key : args["keys"] ) {
                                os << delim << key.get< std::string >();
                                delim = ", ";
                        }
                }
                os << " ";
        }
        if ( j["type"] != "global variable" ) {
                os << colorize( "}", term_color::CYAN );
        }

        os << " " << colorize( "(" + j["type"].get< std::string >() + ")", term_color::GRAY )
           << "\n";
}

inline std::string rjust( std::string inpt, std::size_t val )
{
        while ( inpt.size() < val ) {
                inpt += " ";
        }
        return inpt;
}

inline void
wrap_print( std::ostream& os, const std::string& s, std::size_t len, const std::string& prefix )
{
        std::size_t i = s.rfind( ' ', len );
        if ( i == std::string::npos || s.size() < len ) {
                os << prefix << s << "\n";
                return;
        }

        os << prefix << s.substr( 0, i ) << "\n";
        wrap_print( os, s.substr( i + 1 ), len, prefix );
}

inline void print_example( std::ostream& os, const nlohmann::json& j )
{
        if ( !j.contains( "example" ) ) {
                return;
        }
        os << "    " << colorize( "example ", term_color::BLUE ) << "\n";
        os << "        " << j["example"].get< std::string >() << "\n";
}

inline void print_args( std::ostream& os, const nlohmann::json& j )
{
        if ( j["type"] == "global variable" ) {
                return;
        }
        if ( !j.contains( "arg" ) || !j["arg"].contains( "args" ) ) {
                return;
        }

        for ( const nlohmann::json& arg : j["arg"]["args"] ) {
                os << "    ";
                std::string delim;
                for ( const nlohmann::json& key : arg["keys"] ) {
                        os << delim << colorize( key.get< std::string >(), term_color::MAGENTA );
                        delim = ", ";
                }
                os << " "
                   << colorize( "(" + arg["type"].get< std::string >() + ")", term_color::GRAY )
                   << "\n";
                wrap_print( os, arg["desc"].get< std::string >(), 71, "        " );
        }
}

inline void print_res( std::ostream& os, const nlohmann::json& j )
{
        if ( !j.contains( "res" ) ) {
                return;
        }
        os << "    " << colorize( "returns", term_color::YELLOW ) << " "
           << colorize( "(" + j["res"]["type"].get< std::string >() + ")", term_color::GRAY )
           << "\n";
        wrap_print( os, j["res"]["desc"].get< std::string >(), 71, "        " );
}

inline void print_desc( std::ostream& os, const nlohmann::json& j )
{
        if ( j["type"] == "global variable" ) {
                os << "    " << colorize( "value", term_color::YELLOW ) << " "
                   << colorize( "(" + j["val_type"].get< std::string >() + ")", term_color::GRAY )
                   << "\n        " << j["val"].get< std::string >() << "\n";
        }
        os << "\n";
        wrap_print( os, j["desc"].get< std::string >(), 75, "    " );
        os << "\n";
}

inline void print_help( std::ostream& os, const nlohmann::json& j )
{
        for ( const nlohmann::json& item : j ) {
                os << "\n";
                print_header( os, item );
                print_args( os, item );
                print_res( os, item );
                print_desc( os, item );
                print_example( os, item );
                os << "\n";
        }
}

}  // namespace schpin
