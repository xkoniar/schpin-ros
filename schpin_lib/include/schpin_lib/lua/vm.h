// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <emlabcpp/types.h>
#include <filesystem>
#include <lauxlib.h>
#include <lua.h>
#include <schpin_lib/logging.h>
#include <schpin_lib/lua/def.h>
#include <schpin_lib_exp/algorithm.h>
#include <tuple>
#include <variant>

#pragma once

namespace schpin
{

template < typename Def >
class slua_vm
{
        using content          = typename Def::def;
        content    cont_       = content{};
        lua_State* L_ptr_      = nullptr;
        lua_State* thread_ptr_ = nullptr;

        opt_error< slua_error > bind()
        {
                auto err_vec = filter_optionals( cont_, [&]( auto& item ) {
                        return item.bind( L_ptr_ );
                } );

                if ( err_vec.empty() ) {
                        return {};
                }

                return {
                    E( slua_error ).group_attach( err_vec ) << "Failed to bind items into lua "
                                                               "VM" };
        }

public:
        slua_vm()
        {
                L_ptr_ = luaL_newstate();
                luaL_openlibs( L_ptr_ );
                thread_ptr_ = lua_newthread( L_ptr_ );

                bind().optionally_log();
        }
        slua_vm( const slua_vm& ) = delete;
        slua_vm( slua_vm&& )      = delete;
        slua_vm& operator=( const slua_vm& ) = delete;
        slua_vm& operator=( slua_vm&& ) = delete;

        opt_error< slua_error > load( const std::filesystem::path& filename )
        {
                int error = luaL_loadfile( thread_ptr_, filename.c_str() );
                if ( error != LUA_OK ) {
                        return { E( slua_error ).attach( lua_tostring( thread_ptr_, -1 ) ) };
                }
                return {};
        }

        opt_error< slua_error > load( const std::string& code )
        {
                int error = luaL_loadbuffer( thread_ptr_, code.c_str(), code.size(), "vm" );
                if ( error != LUA_OK ) {
                        return { E( slua_error ).attach( lua_tostring( thread_ptr_, -1 ) ) };
                }
                return {};
        }

        bool load_finished()
        {
                return lua_status( thread_ptr_ ) == LUA_OK ||
                       lua_status( thread_ptr_ ) == LUA_ERRRUN;
        }

        template < typename Visitor >
        opt_error< slua_error > full_exec( Visitor&& vis )
        {
                lua_res                 state = lua_res::YIELDED;
                opt_error< slua_error > res;
                while ( state == lua_res::YIELDED ) {
                        auto var = exec( vis );
                        em::match(
                            var,
                            [&]( lua_res r ) {
                                    state = r;
                            },
                            [&]( slua_error e ) {
                                    state = lua_res::FINISHED;
                                    res   = std::move( e );
                            } );
                }
                return res;
        }

        template < typename Visitor >
        em::either< std::optional< nlohmann::json >, slua_error >
        exec_bind( std::string name, const nlohmann::json& j, Visitor&& vis )
        {
                em::for_each( cont_, [&]( auto& item ) {
                        item.bind_visitor( vis );
                } );

                em::either< std::optional< nlohmann::json >, slua_error > res = {
                    std::optional< nlohmann::json >{} };

                bool found = false;

                lua_State* ptr = lua_newthread( L_ptr_ );

                em::for_each( cont_, [&]( auto& item ) {
                        if ( item.name != name ) {
                                return;
                        }
                        SCHPIN_INFO_LOG(
                            "wololo", "Executing the bind: " << name << ", with args: " << j );
                        res   = item.exec_bind( ptr, j );
                        found = true;
                } );
                if ( !found ) {
                        return { E( slua_error ) << "Failed to find bind function: " << name };
                }
                return res;
        }

        template < typename Visitor >
        std::variant< lua_res, slua_error > exec( Visitor&& vis )
        {
                em::for_each( cont_, [&]( auto& item ) {
                        item.bind_visitor( vis );
                } );

                auto [res, opt_error] = slua_resume( thread_ptr_, 0 );
                if ( res == LUA_OK ) {
                        return { lua_res::FINISHED };
                }
                if ( res == LUA_YIELD ) {
                        return { lua_res::YIELDED };
                }
                thread_ptr_ = lua_newthread( L_ptr_ );
                SCHPIN_ASSERT_MSG(
                    opt_error, "There is no error with slua_resume result: " << res );
                return { *opt_error };
        }

        template < auto ID, typename Visitor, typename... Args >
        opt_error< slua_error > lua_func_call( Visitor&& vis, Args... args )
        {
                em::for_each( cont_, [&]( auto& item ) {
                        item.bind_visitor( vis );
                } );

                auto err_vec = filter_optionals( cont_, [&]( auto& item ) {
                        return item.template fcall< ID >( L_ptr_, args... );
                } );

                if ( err_vec.empty() ) {
                        return {};
                }
                return {
                    E( slua_error ).group_attach( err_vec ) << "Error during call of "
                                                               "function" };
        }

        virtual ~slua_vm()
        {
                lua_close( L_ptr_ );
        }
};

}  // namespace schpin
