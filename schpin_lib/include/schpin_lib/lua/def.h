// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <boost/type_index.hpp>
#include <lauxlib.h>
#include <lualib.h>
#include <schpin_lib/logging.h>
#include <schpin_lib/lua/json.h>
#include <schpin_lib/lua/ser.h>
#include <schpin_lib/print.h>
#include <schpin_lib/util.h>
#include <variant>

#pragma once

namespace schpin
{
inline em::either< std::optional< nlohmann::json >, slua_error >
slua_jsonized_exec( const std::string& name, lua_State* lua_ptr, const nlohmann::json& j )
{
        return safelly_lua_json( lua_ptr, [&]() -> std::optional< nlohmann::json > {
                lua_getglobal( lua_ptr, name.c_str() );
                slua_json_encode( lua_ptr, j );
                lua_pcall( lua_ptr, 1, 1, 0 );
                if ( lua_isnil( lua_ptr, -1 ) ) {
                        return {};
                }
                return { slua_json_decode( lua_ptr, -1 ) };
        } );
}

inline std::tuple< int, opt_error< slua_error > > slua_resume( lua_State* lua_ptr, int n )
{
        int res = lua_resume( lua_ptr, NULL, n );
        if ( res == LUA_OK || res == LUA_YIELD ) {
                return { res, {} };
        }
        if ( lua_isstring( lua_ptr, -1 ) != 0 ) {
                return {
                    res,
                    E( slua_error ).attach( lua_tostring( lua_ptr, -1 ) )  //
                        << "Lua error happend during executin of the lua code: " };
        }
        void* raw_err_ptr = lua_touserdata( lua_ptr, -1 );

        if ( raw_err_ptr == nullptr ) {
                return { res, {} };
        }

        std::unique_ptr< slua_error > err_ptr{ reinterpret_cast< slua_error* >( raw_err_ptr ) };

        return {
            res,
            E( slua_error ).attach( *err_ptr ) << "Lua binding erro happend during "
                                                  "execution" };
}

struct slua_item
{

        opt_error< slua_error > bind( lua_State* )
        {
                return {};
        }

        template < typename Visitor >
        void bind_visitor( Visitor )
        {
        }

        template < auto ID, typename... Args >
        opt_error< slua_error > fcall( lua_State*, Args... )
        {
                return {};
        }

        em::either< std::optional< nlohmann::json >, slua_error >
        exec_bind( lua_State*, const nlohmann::json& )
        {
                return { E( slua_error ) << " exec not implemented for this bind!" };
        }
};

struct no_return_type
{
        using return_type = void;
};

template < typename T >
struct slua_callback : slua_item
{

        using res_type = typename decltype( T::res )::return_type;
        using arg_type = decltype( T::arg );

        static constexpr bool is_procedure = std::is_same_v< res_type, void >;

        std::function< res_type( lua_State*, typename arg_type::value_type ) > cb;

        std::string name = std::string{ T::name };

        opt_error< slua_error > bind( lua_State* lua_ptr )
        {
                lua_pushlightuserdata( lua_ptr, this );
                lua_pushcclosure( lua_ptr, bind_callback, 1 );
                lua_setglobal( lua_ptr, name.c_str() );

                return {};
        }

        em::either< std::optional< nlohmann::json >, slua_error >
        exec_bind( lua_State* lua_ptr, const nlohmann::json& j )
        {
                return slua_jsonized_exec( name, lua_ptr, j );
        }

        template < typename Visitor >
        void bind_visitor( Visitor&& vis )
        {
                cb = std::forward< Visitor >( vis );
        }

private:
        static int bind_callback( lua_State* lua_ptr )
        {
                slua_callback* obj_ptr = reinterpret_cast< slua_callback* >(
                    lua_touserdata( lua_ptr, lua_upvalueindex( 1 ) ) );
                obj_ptr->handle_bind( lua_ptr );
                if constexpr ( is_procedure ) {
                        return 0;
                } else {
                        return 1;
                }
        }

        void handle_bind( lua_State* lua_ptr )
        {
                T::arg.extract( lua_ptr, 1 )
                    .match(
                        [&]( typename arg_type::value_type val ) {
                                if constexpr ( is_procedure ) {
                                        cb( lua_ptr, val );
                                } else {
                                        res_type res = cb( lua_ptr, val );
                                        lua_serializer< res_type >::encode( lua_ptr, res );
                                }
                        },
                        [&]( slua_error e ) {
                                auto* e_ptr = new slua_error{ std::move( e ) };
                                luaL_traceback( lua_ptr, lua_ptr, NULL, 1 );
                                e_ptr->attach( lua_tostring( lua_ptr, -1 ) );
                                lua_pop( lua_ptr, 1 );
                                lua_pushlightuserdata( lua_ptr, e_ptr );
                                lua_error( lua_ptr );
                        } );
        }
};

template < typename T >
inline void to_json( nlohmann::json& j, const slua_callback< T >& item )
{
        j["type"]    = "callback";
        j["name"]    = T::name;
        j["desc"]    = T::desc;
        j["arg"]     = T::arg;
        j["example"] = item.name + j["arg"]["example"].get< std::string >();
        if constexpr ( !slua_callback< T >::is_procedure ) {
                j["res"] = T::res;
        }
}

template < typename T >
struct slua_glob_var : slua_item
{

        // TODO this should not be necessary
        std::string name = std::string{ T::name };

        opt_error< slua_error > bind( lua_State* lua_ptr )
        {
                lua_serializer< std::decay_t< decltype( T::var ) > >::encode( lua_ptr, T::var );
                lua_setglobal( lua_ptr, name.c_str() );
                return {};
        };

        em::either< std::optional< nlohmann::json >, slua_error >
        exec_bind( lua_State*, const nlohmann::json& )
        {
                return { std::optional< nlohmann::json >{} };
        }
};

template < typename T >
inline void to_json( nlohmann::json& j, const slua_glob_var< T >& )
{
        auto var = T::var;

        j["type"]     = "global variable";
        j["name"]     = T::name;
        j["desc"]     = T::desc;
        j["val_type"] = lua_serializer< decltype( var ) >::desc_name();

        lua_State* lua_state = luaL_newstate();
        lua_serializer< decltype( var ) >::encode( lua_state, var );
        std::size_t len;
        const char* lua_val = luaL_tolstring( lua_state, -1, &len );

        j["val"] = lua_val;
}

template < typename T >
struct slua_native_proc : slua_item
{
        std::string name = std::string{ T::name };

        opt_error< slua_error > bind( lua_State* lua_ptr )
        {
                std::stringstream ss;
                ss << "function " << name << " (arg)";
                ss << T::code;
                ss << "end";
                int error = luaL_dostring( lua_ptr, ss.str().c_str() );
                if ( error != LUA_OK ) {
                        return {
                            E( slua_error ).attach( lua_tostring( lua_ptr, -1 ) )
                            << "Error in function " << name };
                }
                return {};
        }

        em::either< std::optional< nlohmann::json >, slua_error >
        exec_bind( lua_State* lua_ptr, const nlohmann::json& j )
        {
                return slua_jsonized_exec( name, lua_ptr, j );
        }
};

template < typename T >
void to_json( nlohmann::json& j, const slua_native_proc< T >& item )
{
        j["type"]    = "lua procedure";
        j["name"]    = T::name;
        j["desc"]    = T::desc;
        j["arg"]     = T::arg;
        j["code"]    = T::code;
        j["example"] = std::string{ T::name } + j["arg"]["example"].get< std::string >();
}

template < typename T >
struct slua_native_proc_call : slua_item
{
        std::string name = std::string{ T::name };

        slua_native_proc< T > sub_native;

        opt_error< slua_error > bind( lua_State* lua_ptr )
        {
                return sub_native.bind( lua_ptr );
        }

        em::either< std::optional< nlohmann::json >, slua_error >
        exec_bind( lua_State*, const nlohmann::json& )
        {
                return { std::optional< nlohmann::json >{} };
        }

        template < auto ID, typename... Args >
        opt_error< slua_error > fcall( lua_State* lua_ptr, Args... args )
        {
                if ( ID != T::call_id ) {
                        return {};
                }

                int type = lua_getglobal( lua_ptr, name.c_str() );

                if ( type != LUA_TFUNCTION ) {
                        return {
                            E( slua_error ) << "The symbol " << name << " in VM is not function" };
                }

                // TODO: rename lua_serializer to slua_serializer
                ( lua_serializer< Args >::encode( lua_ptr, args ), ... );

                int count = sizeof...( args );
                int res   = LUA_YIELD;

                opt_error< slua_error > opt_e;
                while ( res == LUA_YIELD ) {
                        std::tie( res, opt_e ) = slua_resume( lua_ptr, count );
                        count                  = 0;
                }
                return opt_e;
        }
};

template < typename T >
void to_json( nlohmann::json& j, const slua_native_proc_call< T >& item )
{
        j["type"] = "called lua procedure";
        j["name"] = T::name;
        j["desc"] = T::desc;
        j["arg"]  = T::arg;
        j["code"] = T::code;
}

template < typename T >
struct slua_native_class : slua_item
{
        std::string name = std::string{ T::name };

        opt_error< slua_error > bind( lua_State* lua_ptr )
        {
                lua_newtable( lua_ptr );
                lua_setglobal( lua_ptr, name.c_str() );

                auto vec = filter_optionals( typename T::methods{}, [&]( auto item ) {
                        return item.bind( lua_ptr );
                } );
                if ( vec.empty() ) {
                        return {};
                }
                return {
                    E( slua_error ).group_attach( vec ) << "Failed at binding an class " << name };
        }

        em::either< std::optional< nlohmann::json >, slua_error >
        exec_bind( lua_State* lua_ptr, const nlohmann::json& j )
        {
                return slua_jsonized_exec( name, lua_ptr, j );
        }
};

template < typename T >
void to_json( nlohmann::json& j, const slua_native_class< T >& item )
{
        j["type"]    = "class";
        j["name"]    = T::name;
        j["desc"]    = T::desc;
        j["example"] = "chain:new(function () ... end):exec()";
        // TODO: methods !
}

template < typename T >
struct slua_result
{
        using return_type = T;
        std::string description;
};

template < typename T >
void to_json( nlohmann::json& j, const slua_result< T >& item )
{
        j["type"] = lua_serializer< T >::desc_name();
        j["desc"] = item.description;
}

template < typename T, typename... Args >
struct slua_struct_impl
{
        using value_type = T;

        std::tuple< Args... > args_tpl;

        em::either< value_type, slua_error > extract( lua_State* lua_ptr, int pos ) const
        {
                return std::apply(
                    [&]( Args... args ) -> em::either< value_type, slua_error > {
                            if constexpr ( sizeof...( args ) != 0 ) {
                                    return extract_impl< Args... >( lua_ptr, pos, args... );
                            } else {
                                    return { T{} };
                            }
                    },
                    args_tpl );
        }

private:
        template < typename... Args2 >
        auto extract_impl( lua_State* lua_ptr, int pos, Args2... args ) const
        {
                return em::assemble_left_collect_right( args.table_extract( lua_ptr, pos )... )
                    .convert_left( [&]( auto pack ) {
                            return std::apply(
                                []( auto... arg_vals ) {
                                        return T{ arg_vals... };
                                },
                                pack );
                    } )
                    .convert_right( [&]( auto errs ) {
                            return E( slua_error )
                                       .group_attach( errs )
                                       .attach( slua_json_decode( lua_ptr, pos ) )
                                   << "Failed to extract arguments for struct "
                                   << typeid( T ).name();
                    } );
        }
};

inline std::string slua_get_table_args_example( const nlohmann::json& args )
{
        auto keys = map_f_to_v( args, [&]( const auto& sub ) -> std::string {
                return sub["example"].template get< std::string >();
        } );
        if ( keys.empty() ) {
                return "{}";
        }
        std::string res = "{ ";
        std::string delim;
        for ( const std::string& k : keys ) {
                if ( k.empty() ) {
                        continue;
                }
                res += delim + k;
                delim = ", ";
        }
        return res + " }";
}

template < typename T, typename... Args >
void to_json( nlohmann::json& j, const slua_struct_impl< T, Args... >& item )
{
        j["event_type"] = boost::typeindex::type_id< T >().pretty_name();
        em::for_each( item.args_tpl, [&]( auto item ) {
                j["args"].push_back( item );
        } );
        j["example"] = slua_get_table_args_example( j["args"] );
}

template < typename T, typename... Args >
auto slua_struct( Args... args )
{
        return slua_struct_impl< T, Args... >{ std::make_tuple( args... ) };
}

template < typename... Args >
struct slua_native_args_impl
{
        std::tuple< Args... > args_tpl;
};

template < typename... Args >
void to_json( nlohmann::json& j, const slua_native_args_impl< Args... >& item )
{
        em::for_each( item.args_tpl, [&]( auto arg ) {
                j["args"].push_back( arg );
        } );
        j["example"] = slua_get_table_args_example( j["args"] );
}

template < typename... Args >
auto slua_native_args( Args... args )
{
        return slua_native_args_impl< Args... >{ std::make_tuple( args... ) };
}

template < typename T >
struct lua_key_arg
{
        using value_type = T;
        std::string key;
        std::string description;

        em::either< value_type, slua_error >
        table_extract( lua_State* lua_ptr, int tbl_index ) const
        {
                lua_pushstring( lua_ptr, key.c_str() );
                lua_gettable( lua_ptr, tbl_index );

                return lua_serializer< T >::decode( lua_ptr, lua_gettop( lua_ptr ) )
                    .convert_right( [&]( const slua_error& e ) {
                            return E( slua_error ).attach( e )
                                   << "Failed parsing of key argument: " << key;
                    } );
        }
};

template < typename T >
inline void to_json( nlohmann::json& j, const lua_key_arg< T >& item )
{
        j["keys"]    = { item.key };
        j["type"]    = lua_serializer< T >::desc_name();
        j["desc"]    = item.description;
        j["example"] = item.key + "=" + lua_serializer< T >::desc_example();
}

template < typename T >
struct lua_arg
{
        using value_type = T;
        std::string description;

        std::string get_desc()
        {
                return description;
        }

        void insert( lua_State* lua_ptr, T val )
        {
                lua_serializer< T >::encode( lua_ptr, val );
        }
};

template < typename T >
inline void to_json( nlohmann::json& j, const lua_arg< T >& item )
{
        j["type"]    = lua_serializer< T >::desc_name();
        j["desc"]    = item.description;
        j["example"] = lua_serializer< T >::desc_example();
}

template < typename T >
struct lua_opt_key_arg
{
        using value_type = T;
        std::string key;
        std::string description;
        T           default_value;

        em::either< value_type, slua_error >
        table_extract( lua_State* lua_ptr, int tbl_index ) const
        {
                lua_pushstring( lua_ptr, key.c_str() );
                lua_gettable( lua_ptr, tbl_index );
                if ( lua_isnil( lua_ptr, -1 ) ) {
                        lua_pop( lua_ptr, 1 );
                        return { default_value };
                }
                return lua_serializer< T >::decode( lua_ptr, lua_gettop( lua_ptr ) )
                    .convert_right( [&]( const slua_error& e ) {
                            return E( slua_error ) << "Failed at parsing optional key argument "
                                                      "(which is present): "
                                                   << e;
                    } );
        }
};

template < typename T >
inline void to_json( nlohmann::json& j, const lua_opt_key_arg< T >& item )
{
        j["keys"]          = { "[" + item.key + "]" };
        j["desc"]          = item.description;
        j["type"]          = lua_serializer< T >::desc_name();
        j["default_value"] = item.default_value;
        j["example"]       = "";
}

template < typename T >
struct lua_multi_key_arg
{
        using value_type = std::vector< T >;
        std::string key_prefix;
        std::string description;

        em::either< value_type, slua_error >
        table_extract( lua_State* lua_ptr, int tbl_index ) const
        {

                lua_pushstring( lua_ptr, key_prefix.c_str() );
                lua_gettable( lua_ptr, tbl_index );
                if ( !lua_isnil( lua_ptr, -1 ) ) {
                        return lua_serializer< T >::decode( lua_ptr, -1 )
                            .convert_left( [&]( T val ) {
                                    std::vector< T > res;
                                    res.push_back( val );
                                    return res;
                            } )
                            .convert_right( [&]( const slua_error& e ) {
                                    return E( slua_error ).attach( e )
                                           << "Failed at parsing argument with key " << key_prefix;
                            } );
                }

                lua_pop( lua_ptr, 1 );
                std::string k = key_prefix + "s";
                lua_pushstring( lua_ptr, k.c_str() );
                lua_gettable( lua_ptr, tbl_index );
                if ( !lua_istable( lua_ptr, -1 ) ) {
                        return {
                            E( slua_error )
                            << "Argument " + key_prefix + " or " + key_prefix + "s is missing" };
                }

                lua_Integer size    = luaL_len( lua_ptr, -1 );
                auto        eithers = map_f_to_v(
                    em::range< lua_Integer >( 1, size + 1 ),
                    [&]( lua_Integer i ) -> em::either< T, slua_error > {
                            lua_rawgeti( lua_ptr, -1, i );
                            return lua_serializer< T >::decode( lua_ptr, -1 );
                    } );
                lua_pop( lua_ptr, 1 );
                auto split = partition_eithers( eithers );
                if ( split.right.empty() ) {
                        return { split.left };
                }
                return {
                    E( slua_error ).group_attach( split.right )
                    << "There were errors in parsing of items of arg: " << key_prefix << "s" };
        }
};

template < typename T >
inline void to_json( nlohmann::json& j, const lua_multi_key_arg< T >& item )
{
        j["keys"]    = { item.key_prefix, item.key_prefix + "s" };
        j["type"]    = lua_serializer< std::vector< T > >::desc_name();
        j["desc"]    = item.description;
        j["example"] = item.key_prefix + "s=" + lua_serializer< std::vector< T > >::desc_example();
}

}  // namespace schpin
