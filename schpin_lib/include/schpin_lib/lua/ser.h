// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <boost/type_index.hpp>
#include <emlabcpp/zip.h>
#include <lauxlib.h>
#include <lualib.h>
#include <magic_enum.hpp>
#include <schpin_lib/bounded.h>
#include <schpin_lib/geom/point.h>
#include <schpin_lib/geom/quaternion.h>
#include <schpin_lib/lua/base.h>
#include <schpin_lib/lua/json.h>
#include <schpin_lib/util.h>
#include <schpin_lib_exp/scaled.h>
#include <schpin_lib_exp/sized_array.h>
#include <variant>

#pragma once

namespace schpin
{
template < typename >
struct lua_serializer;

template < typename... Ts >
struct lua_serializer< std::variant< Ts... > >
{
        static void encode( lua_State* lua_ptr, const std::variant< Ts... >& var )
        {
                std::visit(
                    [&]< typename Item >( Item item ) {
                            lua_serializer< Item >::encode( lua_ptr, item );
                    },
                    var );
        }

        static std::string desc_name()
        {
                return em::joined(
                    std::array{ lua_serializer< Ts >::desc_name()... }, std::string{ " || " } );
        }

        static std::string desc_example()
        {
                return lua_serializer<
                    std::variant_alternative_t< 0, std::variant< Ts... > > >::desc_example();
        }
};

template < typename T, T MinVal, T MaxVal >
struct lua_serializer< em::bounded< T, MinVal, MaxVal > >
{
        static void encode( lua_State* lua_ptr, em::bounded< T, MinVal, MaxVal > val )
        {
                lua_serializer< T >::encode( lua_ptr, *val );
        };

        static em::either< em::bounded< T, MinVal, MaxVal >, slua_error >
        decode( lua_State* lua_ptr, int index )
        {
                return lua_serializer< T >::decode( lua_ptr, index )
                    .bind_left(
                        [&]( T val ) -> em::either< em::bounded< T, MinVal, MaxVal >, slua_error > {
                                auto res = em::bounded< T, MinVal, MaxVal >::make( val );
                                if ( res ) {
                                        return { *res };
                                }

                                return { E( slua_error ) << "Value " << val << " out of range" };
                        } );
        }

        static std::string desc_name()
        {
                return lua_serializer< T >::desc_name() + " such that: " + to_string( MinVal ) +
                       " < value < " + to_string( MaxVal );
        }
};

template < typename T >
struct lua_serializer< std::optional< T > >
{
        static void encode( lua_State* lua_ptr, std::optional< T > opt_val )
        {
                if ( opt_val ) {
                        lua_serializer< T >::encode( lua_ptr, *opt_val );
                }
        }
        static em::either< std::optional< T >, slua_error > decode( lua_State* lua_ptr, int index )
        {
                // TODO this returns empty optional on aprsing error, not only on missing!
                return lua_serializer< T >::decode( lua_ptr, index )
                    .template construct_left< std::optional< T > >()
                    .bind_right( [&]( auto ) -> em::either< std::optional< T >, slua_error > {
                            return std::optional< T >{};
                    } );
        }
        static std::string desc_name()
        {
                return "[" + lua_serializer< T >::desc_name() + "]";
        }
        static std::string desc_example()
        {
                return lua_serializer< T >::desc_example();
        }
};

template <>
struct lua_serializer< std::string >
{
        static em::either< std::string, slua_error > decode( lua_State* lua_ptr, int index )
        {
                size_t      len;
                const char* string_ptr = lua_tolstring( lua_ptr, index, &len );
                if ( string_ptr == NULL ) {
                        return { E( slua_error ) << "Failed to serialize to string" };
                }
                std::string res{ string_ptr, len };
                lua_pop( lua_ptr, 1 );
                return { res };
        }

        static void encode( lua_State* lua_ptr, const std::string& val )
        {
                lua_pushstring( lua_ptr, val.c_str() );
        }
        static std::string desc_name()
        {
                return "string";
        }
        static std::string desc_example()
        {
                return "\"abc\"";
        }
};

template < typename T >
requires( std::is_enum_v< T > ) struct lua_serializer< T >
{
        static void encode( lua_State* lua_ptr, T val )
        {
                lua_serializer< std::string >::encode(
                    lua_ptr, std::string{ magic_enum::enum_name( val ) } );
        }

        static em::either< T, slua_error > decode( lua_State* lua_ptr, int index )
        {
                return lua_serializer< std::string >::decode( lua_ptr, index )
                    .bind_left( [&]( std::string kword ) -> em::either< T, slua_error > {
                            auto res = magic_enum::enum_cast< T >( kword );
                            if ( res.has_value() ) {
                                    return res.value();
                            }
                            return E( slua_error ) << kword << " is not valid value";
                    } );
        }

        static std::string desc_name()
        {
                return "one of: " + em::joined(
                                        em::map_f_to_a(
                                            magic_enum::enum_entries< T >(),
                                            [&]( auto item ) {
                                                    return "\"" + std::string{ item.second } + "\"";
                                            } ),
                                        std::string{ " " } );
        }
};
template < typename T, typename Iterator >
opt_error< slua_error >
lua_decode_iterable( lua_State* lua_ptr, int index, Iterator output_iter, std::size_t output_n )
{

        if ( !lua_istable( lua_ptr, index ) ) {
                return {
                    E( slua_error ) << "Data for container should be a table, not: "
                                    << luaL_typename( lua_ptr, index ) };
        }
        lua_Integer n = luaL_len( lua_ptr, index );
        if ( n != static_cast< lua_Integer >( output_n ) ) {
                return { E( slua_error ) << "Wrong table size" };
        }

        opt_error< slua_error > res;
        for ( std::size_t i : em::range< std::size_t >( 1, output_n + 1 ) ) {
                lua_rawgeti( lua_ptr, index, static_cast< lua_Integer >( i ) );
                lua_serializer< T >::decode( lua_ptr, -1 )
                    .match(
                        [&]( T val ) {
                                *output_iter = val;
                        },
                        [&]( const slua_error& e ) {
                                res = opt_error{
                                    E( slua_error ).attach( e )
                                    << "Failed to decode " << i << "th item of container" };
                        } );
                if ( res ) {
                        return res;
                }
                output_iter += 1;
        }

        lua_pop( lua_ptr, 1 );
        return res;
}

template < typename Container >
void lua_encode_iterable( lua_State* lua_ptr, const Container& cont )
{
        lua_newtable( lua_ptr );

        for ( const auto& [i, item] : em::enumerate( cont ) ) {
                lua_pushinteger( lua_ptr, static_cast< lua_Integer >( i + 1 ) );
                lua_serializer< std::decay_t< decltype( item ) > >::encode( lua_ptr, item );
                lua_settable( lua_ptr, -3 );
        }
}

template < typename T, std::size_t N >
struct lua_serializer< std::array< T, N > >
{
        static em::either< std::array< T, N >, slua_error > decode( lua_State* lua_ptr, int index )
        {
                std::array< T, N > res;
                auto opt_e = lua_decode_iterable< T >( lua_ptr, index, res.begin(), N );
                if ( opt_e ) {
                        return { *opt_e };
                }
                return { res };
        }

        static void encode( lua_State* lua_ptr, const std::array< T, N >& val )
        {
                lua_encode_iterable( lua_ptr, val );
        }

        static std::string desc_name()
        {
                return lua_serializer< T >::desc_name() + " array with " + std::to_string( N ) +
                       " items";
        }

        static std::string desc_example()
        {
                return "{" +
                       em::joined(
                           map_f_to_v(
                               em::range( N ),
                               [&]( auto ) {
                                       return lua_serializer< T >::desc_example();
                               } ),
                           std::string{ "," } ) +
                       "}";
        }
};

template < typename T >
struct lua_serializer< std::vector< T > >
{
        static em::either< std::vector< T >, slua_error > decode( lua_State* lua_ptr, int index )
        {
                std::vector< T > res{ lua_rawlen( lua_ptr, index ) };
                auto opt_e = lua_decode_iterable< T >( lua_ptr, index, res.begin(), res.size() );
                if ( opt_e ) {
                        return { *opt_e };
                }
                return { res };
        }

        static void encode( lua_State* lua_ptr, const std::vector< T >& val )
        {
                lua_encode_iterable( lua_ptr, val );
        }

        static std::string desc_name()
        {
                return lua_serializer< T >::desc_name() + " array";
        }

        static std::string desc_example()
        {
                return "{" + lua_serializer< T >::desc_example() + ", ...}";
        }
};

template < typename T, typename SizeType >
struct lua_serializer< sized_array< T, SizeType > >
{
        using value_type = sized_array< T, SizeType >;

        static em::either< value_type, slua_error > decode( lua_State* lua_ptr, int index )
        {
                return lua_serializer< typename value_type::container >::decode( lua_ptr, index )
                    .template construct_left< value_type >();
        }

        static void encode( lua_State* lua_ptr, const value_type& val )
        {
                lua_encode_iterable( lua_ptr, val );
        }

        static std::string desc_name()
        {
                return lua_serializer< typename value_type::container >::desc_name();
        }

        static std::string desc_example()
        {
                return lua_serializer< typename value_type::container >::desc_example();
        }
};

template < typename T >
struct lua_serializer_number
{
        static em::either< T, slua_error > decode( lua_State* lua_ptr, int index )
        {
                int isnum;
                T   val = static_cast< T >( lua_tonumberx( lua_ptr, index, &isnum ) );
                lua_pop( lua_ptr, 1 );
                if ( isnum == 0 ) {
                        return {
                            E( slua_error )
                            << "Can't serialize " << index << " th item to " << typeid( T ).name()
                            << " actualy is: " << luaL_typename( lua_ptr, index ) };
                }
                return { val };
        }

        static void encode( lua_State* lua_ptr, T val )
        {
                lua_pushnumber( lua_ptr, static_cast< lua_Number >( val ) );
        }

        static std::string desc_name()
        {
                if constexpr ( std::is_unsigned_v< T > ) {
                        return "unsigned";
                }
                if constexpr ( std::is_integral_v< T > ) {
                        return "integer";
                }
                return "number";
        }

        static std::string desc_example()
        {
                if constexpr ( std::is_unsigned_v< T > ) {
                        return "1";
                }
                if constexpr ( std::is_integral_v< T > ) {
                        return "-1";
                }
                return "1.";
        }
};

template <>
struct lua_serializer< float > : lua_serializer_number< float >
{
};
template <>
struct lua_serializer< double > : lua_serializer_number< double >
{
};
template <>
struct lua_serializer< uint64_t > : lua_serializer_number< uint64_t >
{
};
template <>
struct lua_serializer< uint32_t > : lua_serializer_number< uint32_t >
{
};
template <>
struct lua_serializer< uint16_t > : lua_serializer_number< uint16_t >
{
};
template <>
struct lua_serializer< char > : lua_serializer_number< char >
{
};
template <>
struct lua_serializer< uint8_t > : lua_serializer_number< uint8_t >
{
};
template <>
struct lua_serializer< int64_t > : lua_serializer_number< int64_t >
{
};
template <>
struct lua_serializer< int32_t > : lua_serializer_number< int32_t >
{
};
template <>
struct lua_serializer< int16_t > : lua_serializer_number< int16_t >
{
};
template <>
struct lua_serializer< int8_t > : lua_serializer_number< int8_t >
{
};

template < int Len, int Mass, int Time, int Current, int Temp, int Mol, int Li, int Angle, int Byte >
struct lua_serializer<
    em::physical_quantity< Len, Mass, Time, Current, Temp, Mol, Li, Angle, Byte > >
{
        using value_type =
            em::physical_quantity< Len, Mass, Time, Current, Temp, Mol, Li, Angle, Byte >;

        static em::either< value_type, slua_error > decode( lua_State* lua_ptr, int index )
        {
                return lua_serializer< float >::decode( lua_ptr, index )
                    .template construct_left< value_type >();
        }

        static void encode( lua_State* lua_ptr, const value_type& val )
        {
                lua_serializer< float >::encode( lua_ptr, *val );
        }

        static std::string desc_name()
        {
                return "number " + value_type::get_unit();
        }

        static std::string desc_example()
        {
                return lua_serializer< float >::desc_example();
        }
};

template < typename Tag, typename T, typename Ratio >
struct lua_serializer< scaled< Tag, T, Ratio > >
{
        static em::either< scaled< Tag, T, Ratio >, slua_error >
        decode( lua_State* lua_ptr, int index )
        {
                return lua_serializer< float >::decode( lua_ptr, index )
                    .convert_left( [&]( float val ) {  //
                            return scaled< Tag, T, Ratio >::from_float( val );
                    } );
        }

        static void encode( lua_State* lua_ptr, scaled< Tag, T, Ratio > val )
        {
                lua_serializer< float >::encode( lua_ptr, val.as_float() );
        }

        static std::string desc_name()
        {
                return lua_serializer< float >::desc_name();
        }

        static std::string desc_example()
        {
                return lua_serializer< float >::desc_example();
        }
};

template < typename... Ts >
struct lua_serializer< std::tuple< Ts... > >
{

        static void encode( lua_State* lua_ptr, const std::tuple< Ts... >& tpl )
        {

                lua_createtable( lua_ptr, sizeof...( Ts ), 0 );
                std::size_t i = 0;
                em::for_each( tpl, [&]< typename Val >( Val val ) {
                        lua_serializer< Val >::encode( lua_ptr, val );
                        lua_rawseti( lua_ptr, -2, static_cast< lua_Integer >( i ) );
                        i += 1;
                } );
        }

        static std::string desc_name()
        {
                std::vector< std::string > desc{ lua_serializer< Ts >::desc_name()... };
                return "{" + to_string( em::view{ desc } ) + "}";
        }

        static std::string desc_example()
        {
                std::vector< std::string > desc{ lua_serializer< Ts >::desc_example()... };
                return "{" + to_string( em::view{ desc } ) + "}";
        }
};

template <>
struct lua_serializer< std::function< void() > >
{
        static std::string desc_name()
        {
                return "nullary callback";
        }

        static std::string desc_example()
        {
                return "function() ... end";
        }
};

template < typename Derived, std::size_t N >
struct lua_serializer< vec_point_base< Derived, N > >
{
        static em::either< Derived, slua_error > decode( lua_State* lua_ptr, int index )
        {
                Derived p;
                auto    opt_error =
                    lua_decode_iterable< float >( lua_ptr, index, p.begin(), p.size() );
                if ( opt_error ) {
                        return { *opt_error };
                }
                return { p };
        }

        static void encode( lua_State* lua_ptr, const Derived& val )
        {
                lua_encode_iterable( lua_ptr, val );
        }

        static std::string desc_example()
        {
                return lua_serializer< std::array< float, N > >::desc_example();
        }
};

template < std::size_t N, typename Tag >
struct lua_serializer< point< N, Tag > > : lua_serializer< vec_point_base< point< N, Tag >, N > >
{
        static std::string desc_name()
        {
                return to_string( N ) + "D point in " + to_string( Tag{} ) + " (number array) ";
        }
};

template < std::size_t N >
struct lua_serializer< vec< N > > : lua_serializer< vec_point_base< vec< N >, N > >
{
        static std::string desc_name()
        {
                return to_string( N ) + "D vector (number array)";
        }
};

template < typename Tag >
struct lua_serializer< quaternion< Tag > >
{
        static em::either< quaternion< Tag >, slua_error > decode( lua_State* lua_ptr, int index )
        {
                if ( !lua_istable( lua_ptr, index ) ) {
                        return {
                            E( slua_error ) << "quaternion should be a table of axis and "
                                               "angle" };
                }
                lua_Integer n = luaL_len( lua_ptr, index );
                if ( n != 2 ) {
                        return {
                            E( slua_error ) << "quaternion should be made of two elemnets: "
                                               "axis and angle" };
                }
                lua_rawgeti( lua_ptr, index, 1 );  // this is axis
                auto axis_res = lua_serializer< vec< 3 > >::decode( lua_ptr, -1 );

                lua_rawgeti( lua_ptr, index, 2 );  // angle
                auto angle_res = lua_serializer< double >::decode( lua_ptr, -1 );
                lua_pop( lua_ptr, 1 );

                return em::assemble_left_collect_right( axis_res, angle_res )
                    .convert_left( [&]( std::tuple< vec< 3 >, double > pack ) {
                            auto [axis, alpha] = pack;
                            return quaternion< Tag >{
                                axis, em::angle{ static_cast< float >( alpha ) } };
                    } )
                    .convert_right( [&]( auto errs ) {
                            return E( slua_error ).group_attach( errs ) << "errors in parsing "
                                                                           "quaternion";
                    } );
        }

        static std::string desc_name()
        {
                return "quaternion in " + to_string( Tag{} ) + " ({axis, angle})";
        }

        static std::string desc_example()
        {
                return "{0.,0.,0.,1.}";
        }
};

template <>
struct lua_serializer< nlohmann::json >
{
        static em::either< nlohmann::json, slua_error > decode( lua_State* lua_ptr, int index )
        {
                return { slua_json_decode( lua_ptr, index ) };
        }

        static void encode( lua_State* lua_ptr, const nlohmann::json& j )
        {
                slua_json_encode( lua_ptr, j );
        }

        static std::string desc_name()
        {
                return "any lua data convertible to json";
        }

        static std::string desc_example()
        {
                return "{0.}";
        }
};

}  // namespace schpin
