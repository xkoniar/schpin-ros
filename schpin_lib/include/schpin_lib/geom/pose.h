// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"
#include "schpin_lib/geom/quaternion.h"

#include <utility>

#pragma once

namespace schpin
{

// distance between two poses in space, represented as 'space distance' and 'angular distance'
template < typename Tag >
struct pose_distance
{
        em::distance dist;
        em::angle    angle_dist;
};

// returns steps necessary for linear interpolation of distance between poses 'dis', such that:
//  - the number of steps is minimized
//  - space distance between interpolated poses is smaller than dist_step
//  - angle distance between interpolated poses is smaller than angle_step

template < typename Tag >
constexpr std::size_t
steps( pose_distance< Tag > dist, em::distance dist_step, em::angle angle_step )
{
        auto d_steps = std::size_t( 1.f + *dist.dist / *dist_step );
        auto a_steps = std::size_t( 1.f + *dist.angle_dist / *angle_step );
        return std::max( d_steps, a_steps );
}

// represents orientation and position in 3D space
template < typename Tag >
struct pose
{

        point< 3, Tag >   position;
        quaternion< Tag > orientation;

        constexpr pose()
          : position( 0, 0, 0 )
          , orientation( tag_cast< Tag >( neutral_quat ) )
        {
        }

        explicit constexpr pose( point< 3, Tag > position )
          : position( std::move( position ) )
          , orientation( tag_cast< Tag >( neutral_quat ) )
        {
        }

        explicit constexpr pose( quaternion< Tag > orientation )
          : position( 0, 0, 0 )
          , orientation( std::move( orientation ) )
        {
        }

        constexpr pose( point< 3, Tag > position, quaternion< Tag > orientation )
          : position( std::move( position ) )
          , orientation( std::move( orientation ) )
        {
        }
};

template < typename OtherTag, typename Tag >
constexpr pose< OtherTag > tag_cast( const pose< Tag >& tag )
{
        return pose< OtherTag >{
            tag_cast< OtherTag >( tag.position ), tag_cast< OtherTag >( tag.orientation ) };
}

template < typename Tag >
constexpr pose< Tag > operator-( const pose< Tag >& pos )
{
        auto reverse_rot = pose< Tag >{ -pos.orientation };
        auto reverse_pos = pose< Tag >{ -pos.position };
        return reverse_rot + reverse_pos;
}

template < typename Tag >
constexpr bool operator<( const pose< Tag >& x, const pose< Tag >& y )
{
        if ( x.position == y.position ) {
                return x.orientation < y.orientation;
        }
        return x.position < y.position;
}

// compares poses on their position and orientation
template < typename Tag >
constexpr bool operator==( const pose< Tag >& x, const pose< Tag >& y )
{
        return x.position == y.position && x.orientation == y.orientation;
}

// negation of operator== between poses
template < typename Tag >
constexpr bool operator!=( const pose< Tag >& x, const pose< Tag >& y )
{
        return !( x == y );
}

// returns PoseDistance between provided poses
template < typename Tag >
constexpr pose_distance< Tag > distance_of( const pose< Tag >& x, const pose< Tag >& y )
{
        return {
            distance_of( x.position, y.position ),
            angle_shortest_path( x.orientation, y.orientation ) };
}

// two poses are almost equal when their orientations and positions are almost equal
template < typename Tag >
constexpr bool
almost_equal( const pose< Tag >& x, const pose< Tag >& y, float eps = em::default_epsilon )
{
        return almost_equal( x.orientation, y.orientation, eps ) &&
               almost_equal( x.position, y.position, eps );
}

// linear interpolation between base se and goal pose, with factor 0 'base' is returned, with factor
// 1 'goal' is returned. With factor 0.5, pose between 'base' and 'goal' pose is returned
template < typename Tag >
constexpr pose< Tag > lin_interp( const pose< Tag >& from, const pose< Tag >& goal, float factor )
{
        return pose< Tag >{
            lin_interp( from.position, goal.position, factor ),
            slerp( from.orientation, goal.orientation, factor ) };
}

template < typename Tag >
inline std::vector< pose< Tag > > lineary_interpolate_path(
    const std::vector< pose< Tag > >& ipath,
    em::distance                      d_step,
    em::angle                         a_step )
{
        std::vector< pose< Tag > > res;
        if ( ipath.empty() ) {
                return res;
        }
        for ( std::size_t i : em::range( ipath.size() - 1 ) ) {
                const pose< Tag >& from      = ipath[i];
                const pose< Tag >& to        = ipath[i + 1];
                std::size_t        seg_steps = steps( distance_of( to, from ), d_step, a_step );
                for ( std::size_t j : em::range( seg_steps ) ) {
                        res.push_back( lin_interp( from, to, float( j ) / float( seg_steps ) ) );
                }
        }
        res.push_back( ipath.back() );
        return res;
}

// Point A is rotated based on 'transformation' orientation and than moved based on 'transformation'
// position
template < typename SourceTag, typename GoalTag >
constexpr point< 3, GoalTag >
transform( const point< 3, SourceTag >& a, const pose< GoalTag >& transformation )
{
        return rotate( tag_cast< GoalTag >( a ), transformation.orientation ) +
               cast_to_vec( transformation.position );
}
template < typename Tag >
constexpr vec< 3 > transform( const vec< 3 >& v, const pose< Tag >& transformation )
{
        return rotate( v, transformation.orientation ) + cast_to_vec( transformation.position );
}

// Pose X is rotated based on 'transformation' orientation and than moved based on 'transformation'
// position
template < typename SourceTag, typename GoalTag >
constexpr pose< GoalTag >
transform( const pose< SourceTag >& x, const pose< GoalTag >& transformation )
{
        return pose< GoalTag >{
            transform( tag_cast< GoalTag >( x.position ), transformation ),
            transformation.orientation * tag_cast< GoalTag >( x.orientation ) };
}

template < typename T, typename GoalTag >
constexpr auto transform( const em::min_max< T >& mm, const pose< GoalTag >& transformation )
{
        return em::min_max{
            transform( mm.min, transformation ), transform( mm.max, transformation ) };
}

template < typename Tag >
constexpr pose< Tag > inverse( const pose< Tag >& x )
{
        return pose{ -x.position, inverse( x.orientation ) };
}

template < typename SourceTag, typename T, typename GoalTag >
constexpr auto inverse_transform( const T& item, const pose< GoalTag >& transformation )
{
        return transform( item, tag_cast< SourceTag >( inverse( transformation ) ) );
}

// Pose X is rotated based on quaternion 'quad'
template < typename Tag >
constexpr pose< Tag > rotate( const pose< Tag >& x, const quaternion< Tag >& quat )
{
        return pose{ rotate( x.position, quat ), quat * x.orientation };
}

// sum X + Y is same as Y transformed by X via transform(Y,X) function

}  // namespace schpin
