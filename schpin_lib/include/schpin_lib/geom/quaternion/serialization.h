// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/quaternion.h"

#include <geometry_msgs/msg/quaternion.hpp>
#include <nlohmann/json.hpp>

#pragma once

namespace schpin
{

template < typename Tag >
constexpr quaternion< Tag > msg_to_quaternion( const geometry_msgs::msg::Quaternion& msg )
{
        return quaternion< Tag >{ float( msg.x ), float( msg.y ), float( msg.z ), float( msg.w ) };
}

template < typename Tag >
inline geometry_msgs::msg::Quaternion quaternion_to_msg( const quaternion< Tag >& q )
{
        geometry_msgs::msg::Quaternion msg;
        msg.x = static_cast< double >( q[0] );
        msg.y = static_cast< double >( q[1] );
        msg.z = static_cast< double >( q[2] );
        msg.w = static_cast< double >( q[3] );
        return msg;
}

template < typename Tag >
inline void to_json( nlohmann::json& j, const quaternion< Tag >& quat )
{
        j = { quat[0], quat[1], quat[2], quat[3] };
}

template < typename Tag >
inline void from_json( const nlohmann::json& j, quaternion< Tag >& quat )
{
        quat = quaternion< Tag >{
            j.at( 0 ).get< float >(),
            j.at( 1 ).get< float >(),
            j.at( 2 ).get< float >(),
            j.at( 3 ).get< float >()  //
        };
}
template < typename Tag >
inline std::ostream& operator<<( std::ostream& os, const quaternion< Tag >& q )
{
        os << std::fixed << std::setprecision( 6 );
        return os << "[" << q[0] << "," << q[1] << "," << q[2] << "," << q[3] << "]";
}

}  // namespace schpin

template < typename Tag >
struct std::hash< schpin::quaternion< Tag > >
{
        std::size_t operator()( const schpin::quaternion< Tag >& quat ) const
        {
                std::stringstream ss;
                ss << quat;
                return std::hash< std::string >()( ss.str() );
        }
};
