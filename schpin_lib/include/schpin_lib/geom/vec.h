// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/vec_point_base.h"

#include <iomanip>
#ifdef SCHPIN_USE_STREAMS
#include <ostream>
#endif

#ifdef SCHPIN_USE_NLOHMANN_JSON
#include <nlohmann/json.hpp>
#endif

#pragma once

namespace schpin
{

template < std::size_t N >
class vec : public vec_point_base< vec< N >, N >
{
public:
        using vec_point_base< vec, N >::vec_point_base;

        constexpr vec< N >& operator+=( const vec< N >& other )
        {
                for ( std::size_t i : em::range( N ) ) {
                        ( *this )[i] += other[i];
                }
                return *this;
        }

        constexpr vec< N >& operator-=( const vec< N >& other )
        {
                for ( std::size_t i : em::range( N ) ) {
                        ( *this )[i] -= other[i];
                }
                return *this;
        }
};

/** instances of constants in the code for X/Y/Z axis
 */
constexpr vec< 3 > x_axis{ 1, 0, 0 };
constexpr vec< 3 > y_axis{ 0, 1, 0 };
constexpr vec< 3 > z_axis{ 0, 0, 1 };

template < std::size_t N >
inline vec< N > operator+( vec< N > lh, const vec< N >& rh )
{
        return lh += rh;
}

/** Calculates cross product between points A and B
 */
inline constexpr vec< 3 > cross_product( const vec< 3 >& a, const vec< 3 >& b )
{
        return vec< 3 >(
            a[1] * b[2] - a[2] * b[1],  //
            a[2] * b[0] - a[0] * b[2],
            a[0] * b[1] - a[1] * b[0] );
}

/** Returns a normal to a point A in two dimensions
 */
inline constexpr vec< 2 > normal_of( const vec< 2 >& a )
{
        return vec< 2 >( a[1], -a[0] );
}

template < std::size_t N >
constexpr em::angle vec_angle( const vec< N >& a, const vec< N >& b )
{
        auto s = sqrt( length2_of( a ) * length2_of( b ) );
        return em::angle{ acosf( float( dot( a, b ) ) / *s ) };
}

template < std::size_t N >
inline std::ostream& operator<<( std::ostream& os, const vec< N >& v )
{
        char del = '[';
        os << std::fixed << std::setprecision( 6 );
        for ( float val : v ) {
                os << del << val;
                del = ',';
        }
        return os << "]";
}

#ifdef SCHPIN_USE_NLOHMANN_JSON
// Conversion from json to point<N>
template < std::size_t N >
inline void from_json( const nlohmann::json& j, vec< N >& v )
{
        using container = typename vec< N >::container;

        v = vec< N >( j.get< container >() );
}

// Conversion from point<N> to json
template < std::size_t N >
inline void to_json( nlohmann::json& j, const vec< N >& v )
{
        j = em::view{ v };
}
#endif

}  // namespace schpin

template < std::size_t N >
struct std::numeric_limits< schpin::vec< N > >
  : std::numeric_limits< schpin::vec_point_base< schpin::vec< N >, N > >
{
};
