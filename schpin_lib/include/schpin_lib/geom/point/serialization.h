// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"

#include <geometry_msgs/msg/point.hpp>
#include <geometry_msgs/msg/vector3.hpp>
#include <nlohmann/json.hpp>

#pragma once

namespace schpin
{

template < std::size_t N, typename Tag >
inline std::ostream& operator<<( std::ostream& os, const point< N, Tag >& a )
{
        char del = '(';
        os << std::fixed << std::setprecision( 6 );
        for ( float val : a ) {
                os << del << val;
                del = ',';
        }
        return os << ")";
}

template < typename Tag >
inline geometry_msgs::msg::Point point_to_msg( const point< 3, Tag >& x )
{
        geometry_msgs::msg::Point msg;
        msg.x = static_cast< double >( x[0] );
        msg.y = static_cast< double >( x[1] );
        msg.z = static_cast< double >( x[2] );
        return msg;
}

template < typename Tag >
inline geometry_msgs::msg::Vector3 vector_to_msg( const point< 3, Tag >& x )
{
        geometry_msgs::msg::Vector3 msg;
        msg.x = static_cast< double >( x[0] );
        msg.y = static_cast< double >( x[1] );
        msg.z = static_cast< double >( x[2] );
        return msg;
}

template < typename Tag >
constexpr point< 3, Tag > msg_to_point( const geometry_msgs::msg::Point& msg )
{
        return point< 3, Tag >{ msg.x, msg.y, msg.z };
}

// Conversion from json to point<N>
template < std::size_t N, typename Tag >
inline void from_json( const nlohmann::json& j, point< N, Tag >& p )
{
        using container = typename point< N, Tag >::container;

        p = point< N, Tag >( j.get< container >() );
}

// Conversion from point<N> to json
template < std::size_t N, typename Tag >
inline void to_json( nlohmann::json& j, const point< N, Tag >& p )
{
        j = em::view{ p };
}

}  // namespace schpin

// Instance of std::hash for point< N, Tag >, uses serialization to string to calculate the hash
template < std::size_t N, typename Tag >
struct std::hash< schpin::point< N, Tag > >
{
        std::size_t operator()( const schpin::point< N, Tag >& a ) const
        {
                std::stringstream ss;
                ss << a;
                return std::hash< std::string >()( ss.str() );
        }
};
