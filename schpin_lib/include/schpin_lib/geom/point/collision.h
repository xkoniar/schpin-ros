// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <schpin_lib/geom/point.h>

#pragma once

namespace schpin
{

// Query for the collision detection system for point A
template < std::size_t N, typename TagT >
class sat_point_collision_query
{
public:
        using tag       = TagT;
        using container = std::array< point< N, tag >, 1 >;

private:
        container point_;

public:
        explicit sat_point_collision_query( const point< N, tag >& a )
          : point_( { a } )
        {
        }

        using item_type                = point< N, tag >;
        using point_iterator           = const point< N, tag >*;
        using separation_axis_iterator = const vec< N >*;

        const container& points() const
        {
                return point_;
        }

        em::view< separation_axis_iterator > separation_axes() const
        {
                return { nullptr, nullptr };
        }

        em::view< separation_axis_iterator > intersection_axes() const
        {
                return { nullptr, nullptr };
        }
};

// Query for the collision detection system for point A
template < std::size_t N, typename TagT >
class bs_point_collision_query
{
public:
        using tag       = TagT;
        using sat_type  = sat_point_collision_query< N, TagT >;
        using item_type = point< N, tag >;

private:
        point< N, tag > point_;

public:
        explicit bs_point_collision_query( const point< N, tag >& a )
          : point_( a )
        {
        }

        constexpr const point< N, tag >& min_sphere_position() const
        {
                return point_;
        }

        constexpr em::radius min_sphere_radius() const
        {
                return em::radius{ 0.f };
        }

        constexpr bool is_leaf() const
        {
                return true;
        }

        constexpr float descent_priority() const
        {
                return 0.f;
        }

        constexpr std::vector< bs_point_collision_query > children() const
        {
                return {};
        }

        constexpr sat_point_collision_query< N, tag > sat() const
        {
                return sat_point_collision_query{ point_ };
        }
};
}  // namespace schpin
