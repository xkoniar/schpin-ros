// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <random>
#include <schpin_lib/geom/point.h>

#pragma once

namespace schpin
{

template < std::size_t N, typename Tag >
class point_generator
{
        std::mt19937                      gen_{};
        std::normal_distribution< float > normal_d_{ 0, 1 };

public:
        point< N, Tag > get( vec< 3 > v = vec< 3 >::make_filled_with( 1.f ) )
        {
                point< N, Tag > res{};
                for ( std::size_t i : em::range( N ) ) {
                        res[i] = normal_d_( gen_ ) * v[i];
                }
                return res;
        }
};

}  // namespace schpin
