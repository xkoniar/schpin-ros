// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/iterators/numeric.h"
#include "emlabcpp/physical_quantity.h"
#include "schpin_lib/algorithm.h"
#include "schpin_lib/geom/frame.h"
#include "schpin_lib/geom/vec.h"
#include "schpin_lib/geom/vec_point_base.h"

#pragma once

namespace schpin
{

/** Class implementing multidimensional point in coordinate system of dimension N
 */
template < std::size_t N, typename Tag >
class point : public vec_point_base< point< N, Tag >, N >
{
public:
        using tag = Tag;

        using vec_point_base< point, N >::vec_point_base;

        /** += operator adds value of 'i'th coordinate from 'other' to 'this', for all 0 <= i < N
         */
        constexpr point< N, tag >& operator+=( const vec< N >& other )
        {
                for ( std::size_t i : em::range( N ) ) {
                        ( *this )[i] += other[i];
                }
                return *this;
        }

        /** -= operator subtracts value of 'i'th coordinate of 'other' from 'this', for all 0 <= i <
         * N
         */
        constexpr point< N, tag >& operator-=( const vec< N >& other )
        {
                for ( std::size_t i : em::range( N ) ) {
                        ( *this )[i] -= other[i];
                }
                return *this;
        }

        ~point() = default;
};

template < typename Tag, std::size_t N >
point< N, Tag > cast_to_point( const vec< N >& v )
{
        return point< N, Tag >{ *v };
}

template < typename Tag, std::size_t N >
vec< N > cast_to_vec( point< N, Tag > p )
{
        return vec< N >{ *p };
}

template < typename OtherTag, typename Tag, std::size_t N >
constexpr point< N, OtherTag > tag_cast( point< N, Tag > a )
{
        return point< N, OtherTag >{ *a };
}

// TODO: this may not be mathematically correct
/** Multiplication of points multiplies each coordinate of A by coordinate of B on same dimension
 */
template < std::size_t N, typename Tag >
[[deprecated]] constexpr point< N, Tag > operator*( point< N, Tag > a, const point< N, Tag >& b )
{
        for ( std::size_t i : em::range( N ) ) {
                a[i] *= b[i];
        }
        return a;
}

/** Returns a result of subtraction of A from B, viz -= operator
 */
template < std::size_t N, typename Tag >
constexpr vec< N > operator-( point< N, Tag > a, const point< N, Tag >& b )
{
        a -= cast_to_vec( b );
        return vec< N >{ *a };
}

/** Returns a result of addition a to b, viz += operator
 */
template < std::size_t N, typename Tag >
constexpr point< N, Tag > operator+( point< N, Tag > a, const vec< N >& b )
{
        a += b;
        return a;
}

/** Returns a result of subtraction a to b, viz += operator
 */
template < std::size_t N, typename Tag >
constexpr point< N, Tag > operator-( point< N, Tag > a, const vec< N >& b )
{
        a -= b;
        return a;
}

/** Returns euclidian distance of point A from point B
 */
template < std::size_t N, typename Tag >
constexpr em::distance distance_of( const point< N, Tag >& a, const point< N, Tag >& b )
{
        auto tmp = em::sum( em::range( N ), [&]( std::size_t i ) {  //
                return std::pow( a[i] - b[i], 2 );
        } );
        return em::distance( std::sqrt( float( tmp ) ) );
}

template < std::size_t N, typename Tag >
constexpr em::angle point_angle( const point< N, Tag >& a, const point< N, Tag >& b )
{
        return vec_angle( cast_to_vec( a ), cast_to_vec( b ) );
}

template < std::size_t N, typename Tag >
inline std::vector< point< N, Tag > >
lineary_interpolate_path( const std::vector< point< N, Tag > >& ipath, em::distance d_step )
{
        std::vector< point< N, Tag > > res;
        if ( ipath.empty() ) {
                return res;
        }
        for ( std::size_t i : em::range( ipath.size() - 1 ) ) {
                const point< N, Tag >& from = ipath[i];
                const point< N, Tag >& to   = ipath[i + 1];

                std::size_t seg_steps = std::size_t{ distance_of( from, to ) / d_step };
                for ( std::size_t j : em::range( seg_steps ) ) {
                        res.push_back( lin_interp( from, to, float( j ) / float( seg_steps ) ) );
                }
        }
        res.push_back( ipath.back() );
        return res;
}

/** Function to calculate distance of projection of point A. That point is projected on axis defined
 * only by it's direction - 'axis_direction'. The distance of that projection from the [0,0,0]
 * coordinate is returned.
 */
template < typename Tag, std::size_t N >
constexpr em::distance
axis_projection_distance( const point< N, Tag >& a, const vec< N >& axis_direction )
{
        return em::distance( float( dot( cast_to_vec( a ), axis_direction ) ) );
}
}  // namespace schpin

template < std::size_t N, typename Tag >
struct std::numeric_limits< schpin::point< N, Tag > >
  : std::numeric_limits< schpin::vec_point_base< schpin::point< N, Tag >, N > >
{
};

