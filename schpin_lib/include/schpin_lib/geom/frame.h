// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

namespace schpin
{

/** Frames of reference for the codebase
 */
struct no_frame
{
};

struct world_frame
{
};

struct robot_frame
{
};

#ifdef SCHPIN_USE_STREAMS
inline std::ostream& operator<<( std::ostream& os, no_frame )
{
        return os << "no frame";
}

inline std::ostream& operator<<( std::ostream& os, world_frame )
{
        return os << "world frame";
}
inline std::ostream& operator<<( std::ostream& os, robot_frame )
{
        return os << "robot frame";
}
#endif

}  // namespace schpin
