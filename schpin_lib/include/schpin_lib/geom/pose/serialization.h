// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point/serialization.h"
#include "schpin_lib/geom/pose.h"
#include "schpin_lib/geom/quaternion/serialization.h"

#include <geometry_msgs/msg/pose.hpp>
#include <nlohmann/json.hpp>

#pragma once

namespace schpin
{

// Output operator for pose simply prints position and orientation
template < typename Tag >
inline std::ostream& operator<<( std::ostream& os, const pose< Tag >& pose )
{
        return os << pose.position << "-" << pose.orientation;
}

template < typename Tag >
constexpr pose< Tag > msg_to_pose( const geometry_msgs::msg::Pose& msg )
{
        return pose< Tag >{
            msg_to_point< Tag >( msg.position ), msg_to_quaternion< Tag >( msg.orientation ) };
}

template < typename Tag >
inline geometry_msgs::msg::Pose pose_to_msg( const pose< Tag >& p )
{
        geometry_msgs::msg::Pose res;
        res.position    = point_to_msg( p.position );
        res.orientation = quaternion_to_msg( p.orientation );
        return res;
}

// converts Pose to nlohmann::json
template < typename Tag >
inline void to_json( nlohmann::json& j, const pose< Tag >& p )
{
        j["position"]    = p.position;
        j["orientation"] = p.orientation;
}

// converts nlohhmann::json to Pose
template < typename Tag >
inline void from_json( const nlohmann::json& j, pose< Tag >& p )
{
        p = pose{
            j.at( "position" ).get< point< 3, Tag > >(),
            j.at( "orientation" ).get< quaternion< Tag > >() };
}

}  // namespace schpin

// calculates hash of Pose, which is a XOR of hashes of it's position and orientation
template < typename Tag >
struct std::hash< schpin::pose< Tag > >
{
        std::size_t operator()( const schpin::pose< Tag >& pose ) const
        {
                return std::hash< schpin::point< 3, Tag > >()( pose.position ) ^
                       std::hash< schpin::quaternion< Tag > >()( pose.orientation );
        }
};
