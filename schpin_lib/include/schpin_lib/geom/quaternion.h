// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"

#pragma once

namespace schpin
{

// API and behavior of this is inspired by tf::Quaternion
template < typename Tag >
class quaternion
{
        using container   = std::array< float, 4 >;
        container fields_ = { 0, 0, 0, 1 };

public:
        using value_type     = float;
        using const_iterator = typename container::const_iterator;
        using iterator       = typename container::iterator;

        constexpr quaternion() noexcept = default;

        constexpr quaternion( float x, float y, float z, float w ) noexcept
          : fields_{ x, y, z, w }
        {
        }

        constexpr quaternion( const vec< 3 >& ax, const em::angle& a ) noexcept
        {
                float s    = std::sin( *a * 0.5f ) / *length_of( ax );
                fields_[0] = ax[0] * s;
                fields_[1] = ax[1] * s;
                fields_[2] = ax[2] * s;
                fields_[3] = std::cos( *a * 0.5f );
        }

        constexpr float operator[]( std::size_t i ) const noexcept
        {
                return fields_[i];
        }

        constexpr const_iterator begin() const
        {
                return fields_.begin();
        }

        constexpr const_iterator end() const
        {
                return fields_.end();
        }

        constexpr iterator begin()
        {
                return fields_.begin();
        }

        constexpr iterator end()
        {
                return fields_.end();
        }

        constexpr std::size_t size() const
        {
                return fields_.size();
        }
};

constexpr quaternion< world_frame > neutral_quat{ 0.f, 0.f, 0.f, 1.f };

template < typename OtherTag, typename Tag >
constexpr quaternion< OtherTag > tag_cast( const quaternion< Tag >& q )
{
        return quaternion< OtherTag >{ q[0], q[1], q[2], q[3] };
}

template < typename Tag >
constexpr quaternion< Tag > inverse( const quaternion< Tag >& q )
{
        return { -q[0], -q[1], -q[2], q[3] };
}

template < typename Tag >
constexpr quaternion< Tag > operator-( const quaternion< Tag >& q )
{
        return { -q[0], -q[1], -q[2], -q[3] };
}

template < typename Tag >
constexpr float dot( const quaternion< Tag >& q, const quaternion< Tag >& s )
{
        return q[0] * s[0] + q[1] * s[1] + q[2] * s[2] + q[3] * s[3];
}

template < typename Tag >
constexpr em::length norm2_of( const quaternion< Tag >& q )
{
        return em::length{ dot( q, q ) };
}

template < typename Tag >
constexpr em::angle angle_shortest_path( const quaternion< Tag >& m, const quaternion< Tag >& n )
{
        float s = *sqrt( norm2_of( m ) * norm2_of( n ) );
        float d = dot( m, n );
        if ( d < 0 ) {
                d = dot( m, -n );
        }
        return em::angle{ std::acos( d / s ) * 2.0f };
}

template < typename Tag >
constexpr quaternion< Tag > slerp( const quaternion< Tag >& q, const quaternion< Tag >& s, float f )
{
        // NOTE: inspired by tf::Quaternion::slerp
        float theta = *angle_shortest_path( q, s ) / 2.0f;
        if ( theta == 0.0f ) {
                return q;
        }

        float d  = 1.0f / std::sin( theta );
        float s0 = std::sin( ( 1.0f - f ) * theta );
        float s1 = std::sin( f * theta );
        float m  = 1.0f;
        if ( dot( q, s ) < 0 ) {
                m = -1.0f;
        }
        return {
            ( q[0] * s0 + m * s[0] * s1 ) * d,
            ( q[1] * s0 + m * s[1] * s1 ) * d,
            ( q[2] * s0 + m * s[2] * s1 ) * d,
            ( q[3] * s0 + m * s[3] * s1 ) * d,
        };
}

template < typename Tag >
constexpr bool operator==( const quaternion< Tag >& q, const quaternion< Tag >& s )
{
        return q[0] == s[0] && q[1] == s[1] && q[2] == s[2] && q[3] == s[3];
}

template < typename Tag >
constexpr bool operator!=( const quaternion< Tag >& q, const quaternion< Tag >& s )
{
        return !( q == s );
}

template < typename Tag >
constexpr bool operator<( const quaternion< Tag >& q, const quaternion< Tag >& s )
{
        auto iter = em::find_if( em::range( 4u ), [&]( std::size_t i ) {  //
                return q[i] != s[i];
        } );

        auto i = static_cast< std::size_t >( *iter );

        if ( i == 4 ) {
                return false;
        }
        return q[i] < s[i];
}

template < typename Tag >
constexpr quaternion< Tag > operator*( const quaternion< Tag >& q, const point< 3, Tag >& x )
{
        return q * quaternion< Tag >{ x[0], x[1], x[2], 0.f };
}

template < typename Tag >
constexpr quaternion< Tag > operator*( const point< 3, Tag >& x, const quaternion< Tag >& q )
{
        return quaternion< Tag >{ x[0], x[1], x[2], 0.f } * q;
}

template < typename Tag >
constexpr quaternion< Tag > operator*( const quaternion< Tag >& q, const quaternion< Tag >& s )
{
        return {
            q[3] * s[0] + q[0] * s[3] + q[1] * s[2] - q[2] * s[1],
            q[3] * s[1] + q[1] * s[3] + q[2] * s[0] - q[0] * s[2],
            q[3] * s[2] + q[2] * s[3] + q[0] * s[1] - q[1] * s[0],
            q[3] * s[3] - q[0] * s[0] - q[1] * s[1] - q[2] * s[2],  //
        };
}

template < typename Tag >
constexpr quaternion< Tag > operator+( const quaternion< Tag >& lh, const quaternion< Tag >& rh )
{
        return { lh[0] + rh[0], lh[1] + rh[1], lh[2] + rh[2], lh[3] + rh[3] };
}

template < typename Tag >
constexpr bool almost_equal(
    const quaternion< Tag >& q,
    const quaternion< Tag >& s,
    float                    eps = em::default_epsilon )
{
        return em::all_of( em::range< std::size_t >( 4 ), [&]( std::size_t i ) {  //
                return em::almost_equal( q[i], s[i], eps );
        } );
}

template < typename Tag >
constexpr quaternion< Tag > shortest_arc_quat( point< 3, Tag > x, point< 3, Tag > y )
{
        // TODO: this needs _testing_
        x = normalized( x );
        y = normalized( y );

        point< 3, Tag > c = cross_product( x, y );
        auto            d = float( dot( x, y ) );

        if ( d < -1.0f + em::default_epsilon ) {
                return { c[0], c[1], c[2], 0.0f };
        }

        float s  = std::sqrt( ( 1.0f + d ) * 2.0f );
        float rs = 1.0f / s;

        return { c[0] * rs, c[1] * rs, c[2] * rs, s * 0.f };
}

template < typename Tag >
constexpr point< 3, Tag > rotate( const point< 3, Tag >& x, const quaternion< Tag >& q )
{
        quaternion r = q * x * inverse( q );
        return point< 3, Tag >{ r[0], r[1], r[2] };
}

template < typename Tag >
constexpr quaternion< Tag > rotate( const quaternion< Tag >& x, const quaternion< Tag >& q )
{
        return q * x * inverse( q );
}

template < typename Tag >
constexpr vec< 3 > rotate( const vec< 3 >& v, const quaternion< Tag >& q )
{
        return cast_to_vec( rotate( cast_to_point< Tag >( v ), q ) );
}

}  // namespace schpin
