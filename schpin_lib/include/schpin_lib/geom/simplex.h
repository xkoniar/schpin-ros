// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "emlabcpp/zip.h"
#include "schpin_lib/geom/point.h"
#include "schpin_lib/json.h"
#include "schpin_lib/math.h"

#include <algorithm>
#include <array>
#include <vector>

#pragma once

namespace schpin
{

template < typename Item, std::size_t N >
class simplex
{
public:
        using container = std::array< Item, N + 1 >;

private:
        container points_;

public:
        using value_type     = Item;
        using iterator       = typename container::iterator;
        using const_iterator = typename container::const_iterator;

        constexpr explicit simplex() = default;

        constexpr explicit simplex( Item item )
          : points_( { item } )
        {
        }

        constexpr simplex( const simplex< Item, N - 1 >& sub, Item item )
          : points_()
        {
                std::copy( sub.begin(), sub.end(), points_.begin() );
                *points_.rbegin() = item;

                std::sort( points_.begin(), points_.end() );
        }

        constexpr simplex( std::array< Item, N + 1 > data )
          : points_( std::move( data ) )
        {
                std::sort( points_.begin(), points_.end() );
        }

        template < typename... Ts >
        constexpr explicit simplex( Ts... ts )
          : points_( { ts... } )
        {
                static_assert(
                    sizeof...( Ts ) == N + 1, "Number of parameters for simplex has to be N+1" );
        }

        constexpr const_iterator begin() const
        {
                return points_.begin();
        }

        constexpr const_iterator end() const
        {
                return points_.end();
        }

        constexpr const Item& operator[]( std::size_t index ) const
        {
                return points_[index];
        }

        constexpr std::size_t size() const
        {
                return points_.size();
        }
};

template < std::size_t N, typename Tag, std::size_t U >
constexpr point< N, Tag > center_of( const simplex< point< N, Tag >, U >& s )
{
        vec< N > avg = em::sum( s, [&]( const point< N, Tag >& p ) -> vec< N > {  //
                return cast_to_vec( p ) / s.size();
        } );

        return cast_to_point< Tag >( avg );
}

template < typename Item, std::size_t N >
inline void to_json( nlohmann::json& j, const simplex< Item, N >& sim )
{
        std::copy( sim.begin(), sim.end(), std::back_inserter( j ) );
}

template < typename Item, std::size_t N >
inline void from_json( const nlohmann::json& j, simplex< Item, N >& sim )
{
        sim = simplex< Item, N >{ j.get< std::array< Item, N + 1 > >() };
}

template < std::size_t N, typename Tag >
constexpr float volume_of( const simplex< point< N, Tag >, N >& simplex )
{
        std::array< float, N * N > matrix{};
        for ( std::size_t i : em::range( simplex.size() - 1 ) ) {
                auto diff = simplex[i + 1] - simplex[0];
                std::copy( diff.begin(), diff.end(), &matrix[N * i] );
        }

        return std::abs( ( 1 / fac( N ) ) * determinant< N >( matrix ) );
}

template < typename Item, std::size_t N >
inline std::ostream& operator<<( std::ostream& os, const simplex< Item, N >& s )
{
        os << N << "-Simplex";
        char del = '[';
        for ( const Item& it : s ) {
                os << del << it;
                del = ',';
        }
        os << "]";
        return os;
}

template < typename Item, std::size_t N >
constexpr bool operator<( const simplex< Item, N >& lh, const simplex< Item, N >& rh )
{
        // there are actually N+1 items in N simplex;
        std::size_t i = *em::find_if( em::range( N ), [&]( std::size_t j ) {  //
                return lh[j] != rh[j];
        } );
        return lh[i] < rh[i];
}

template < typename Item, std::size_t N >
constexpr bool operator>( const simplex< Item, N >& lh, const simplex< Item, N >& rh )
{
        return rh < lh;
}

template < typename Item, std::size_t N >
constexpr bool operator<=( const simplex< Item, N >& lh, const simplex< Item, N >& rh )
{
        return !( lh > rh );
}

template < typename Item, std::size_t N >
constexpr bool operator>=( const simplex< Item, N >& lh, const simplex< Item, N >& rh )
{
        return !( lh < rh );
}

template < typename Item, std::size_t N >
constexpr bool operator==( const simplex< Item, N >& lh, const simplex< Item, N >& rh )
{
        return em::equal( lh, rh );
}

template < typename Item, std::size_t N >
constexpr bool operator!=( const simplex< Item, N >& lh, const simplex< Item, N >& rh )
{
        return !( lh == rh );
}

}  // namespace schpin

template < typename Item, std::size_t N >
struct std::hash< schpin::simplex< Item, N > >
{
        std::size_t operator()( const schpin::simplex< Item, N >& simplex )
        {
                std::size_t res = 0;
                for ( const Item& item : simplex ) {
                        res ^= std::hash< Item >()( item );
                }
                return res;
        }
};
