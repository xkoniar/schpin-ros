// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"

#include <array>

#pragma once

namespace schpin
{

// represents an set of two points, type alies of std::array because nothing more specific is
// required
template < std::size_t N, typename Tag >
using line = std::array< point< N, Tag >, 2 >;

// calculate shortest distance between any point on line and point 'p'
template < std::size_t N, typename Tag >
constexpr em::distance distance_of( const line< N, Tag >& l, const point< N, Tag >& p )
{
        auto direction      = l[1] - l[0];
        auto length_squared = *length2_of( direction );
        if ( length_squared == 0.f ) {
                return distance_of( p, l[0] );
        }
        auto projection_dist = float( dot( p - l[0], direction ) );
        projection_dist /= length_squared;
        projection_dist = std::clamp( projection_dist, 0.f, 1.f );

        point< N, Tag > closest_p = l[0] + projection_dist * direction;

        return distance_of( closest_p, p );
}

template < std::size_t N, typename Tag >
constexpr em::distance distance_of( const point< N, Tag >& p, const line< N, Tag >& line )
{
        return distance_of( line, p );
}

}  // namespace schpin
