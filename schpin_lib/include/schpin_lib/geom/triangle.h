// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "schpin_lib/geom/point.h"
#include "schpin_lib/geom/pose.h"
#include "schpin_lib/geom/simplex.h"

#pragma once

namespace schpin
{

template < std::size_t N, typename Tag >
using triangle = simplex< point< N, Tag >, 2 >;

template < typename GoalTag, std::size_t N, typename SourceTag >
inline triangle< N, GoalTag > tag_cast( const triangle< N, SourceTag >& t )
{
        return triangle< N, GoalTag >{
            tag_cast< GoalTag >( t[0] ), tag_cast< GoalTag >( t[1] ), tag_cast< GoalTag >( t[2] ) };
}

template < typename Tag >
inline point< 3, Tag > get_triangle_sphere_center( const triangle< 3, Tag >& tri )
{
        vec< 3 > ab     = tri[0] - tri[1];
        vec< 3 > ac     = tri[0] - tri[2];
        vec< 3 > normal = normalized( cross_product( ab, ac ) );
        vec< 3 > p_ab   = cross_product( normal, ab );
        vec< 3 > p_ac   = cross_product( normal, ac );
        vec< 3 > c_ac   = ( cast_to_vec( tri[0] ) + cast_to_vec( tri[2] ) ) / 2;
        vec< 3 > c_ab   = ( cast_to_vec( tri[0] ) + cast_to_vec( tri[1] ) ) / 2;

        float k;

        k = p_ab[0] * ( c_ab[1] - c_ac[1] ) + p_ab[1] * ( c_ac[0] - c_ab[0] );
        //-------------------------------------------------------------------
        k /= ( p_ab[0] * p_ac[1] - p_ac[0] * p_ab[1] );

        return cast_to_point< Tag >( c_ac + k * p_ac );
}

template < typename Tag >
constexpr vec< 3 > normal_of( const triangle< 3, Tag >& tri )
{
        return cross_product( tri[0] - tri[1], tri[0] - tri[2] );
}

template < std::size_t N, typename Tag >
inline std::ostream& operator<<( std::ostream& os, const triangle< N, Tag >& t )
{
        return os << t[0] << "," << t[1] << "," << t[2];
}

template < typename SourceTag, typename GoalTag >
constexpr triangle< 3, GoalTag >
transform( const triangle< 3, SourceTag >& t, const pose< GoalTag >& transformation )
{
        return triangle< 3, GoalTag >{
            transform( t[0], transformation ),
            transform( t[1], transformation ),
            transform( t[2], transformation ) };
}

template < std::size_t N, typename Tag >
constexpr triangle< N, Tag > scale( const triangle< N, Tag >& t, const point< N, Tag >& scales )
{
        return triangle< N, Tag >{ t[0] * scales, t[1] * scales, t[2] * scales };
}

}  // namespace schpin

template < std::size_t N, typename Tag >
struct std::hash< schpin::triangle< N, Tag > >
{
        std::size_t operator()( const schpin::triangle< N, Tag >& t ) const
        {
                std::stringstream ss;
                ss << t;
                return std::hash< std::string >()( ss.str() );
        }
};
