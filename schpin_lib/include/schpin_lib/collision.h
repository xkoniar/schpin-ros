#include "schpin_lib/collision/concepts.h"
#include "schpin_lib/collision/math.h"
#include "schpin_lib/collision/tree.h"

#pragma once

namespace schpin
{

/* Checks collision between provided objects. The Separation Axis Test is used. This simply calls
 * the collidesSingleAxes(A,B) on the axes of 'RH' object and than on the axes of 'RH' object.
 *
 * O( (x+z)*(k+n) ), where x,z are the number of axes for the LH and RH objects and k,n are the
 * number of points for LH and RH.
 */
template < sat_collision_query LH, sat_collision_query RH >
[[gnu::hot]] constexpr bool sat_collides( const LH& lh, const RH& rh ) noexcept
{
        static_assert( std::is_same_v< typename LH::tag, typename RH::tag > );
        return collides_single_axes< LH, RH >( lh, rh ) && collides_single_axes< RH, LH >( rh, lh );
}

/* Fast check of collision between provided objects. This uses min-spheres for each object. The min
 * sphere is the smallest sphere containing adequate object. The collision check between two spheres
 * is easy - simply compare the distance between their centers with their radiuses.
 *
 * O(1)
 */
template < bs_collision_query LH, bs_collision_query RH >
[[gnu::hot]] constexpr bool bs_collides( const LH& lh, const RH& rh ) noexcept
{
        static_assert( std::is_same_v< typename LH::tag, typename RH::tag > );

        em::distance dist{ distance_of( lh.min_sphere_position(), rh.min_sphere_position() ) };

        return dist < ( lh.min_sphere_radius() + rh.min_sphere_radius() );
}

/* Tree collision detection function that uses bs_collides() on nodes
 * and sat_collides on leafs.
 */
template < bs_collision_query LH, bs_collision_query RH, typename LeafFunction >
requires( tree_collision_query< LH >&& tree_collision_query< RH > )
    [[gnu::hot]] constexpr bool bs_trees_collides( const LH& lh, const RH& rh, LeafFunction&& f )
{
        return trees_collides(
            lh,
            rh,
            [&]( const auto& lh, const auto& rh ) {
                    return bs_collides( lh, rh );
            },
            [&]( const auto& lh, const auto& rh ) {
                    return sat_collides( lh.sat(), rh.sat() ) && f( lh, rh );
            } );
}

/* Function that calls `collect_f` for each leaf nodes from `lh` and `rh` that are in collision.
 */
template <
    bs_collision_query LH,
    bs_collision_query RH,
    typename CollisionFunction,
    typename CollectFunction >
constexpr void bs_find_collision_nodes(
    const LH&           lh,
    const RH&           rh,
    CollisionFunction&& collision_f,
    CollectFunction&&   collect_f )
{
        bs_trees_collides( lh, rh, [&]( const auto& lh_node, const auto& rh_node ) {
                if ( !collision_f( lh_node, rh_node ) ) {
                        return false;
                }
                collect_f( lh_node, rh_node );
                return false;
        } );
}

}  // namespace schpin
