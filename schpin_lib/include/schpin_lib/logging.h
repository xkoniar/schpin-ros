// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#ifdef SCHPIN_DISABLE_LOGGING

#define SCHPIN_DEBUG_LOG( name, msg )

#define SCHPIN_INFO_LOG( name, msg )

#define SCHPIN_ERROR_LOG( name, msg )

#define SCHPIN_WARN_LOG( name, msg )

#define SCHPIN_ASSERT( cond )

#define SCHPIN_ASSERT_MSG( cond, args )

#endif

#ifndef SCHPIN_DISABLE_LOGGING

#include <iostream>
#include <rclcpp/logging.hpp>

#define SCHPIN_DEBUG_LOG( name, msg )                                   \
        {                                                               \
                RCLCPP_DEBUG_STREAM( rclcpp::get_logger( name ), msg ); \
        }

#define SCHPIN_INFO_LOG( name, msg )                                   \
        {                                                              \
                RCLCPP_INFO_STREAM( rclcpp::get_logger( name ), msg ); \
        }

#define SCHPIN_ERROR_LOG( name, msg )                                   \
        {                                                               \
                RCLCPP_ERROR_STREAM( rclcpp::get_logger( name ), msg ); \
        }

#define SCHPIN_WARN_LOG( name, msg )                                   \
        {                                                              \
                RCLCPP_WARN_STREAM( rclcpp::get_logger( name ), msg ); \
        }

#define SCHPIN_ASSERT( cond )   \
        {                       \
                assert( cond ); \
        }

#ifdef NDEBUG
#define SCHPIN_ASSERT_MSG( cond, args ) (void) ( sizeof( std::stringstream{} << msg ) )
#else
#define SCHPIN_ASSERT_MSG( cond, args )                         \
        {                                                       \
                if ( !( cond ) ) {                              \
                        std::cerr << "ASSERT FAILED: " << args; \
                        std::abort();                           \
                }                                               \
        }
#endif

#endif
