// Copyright 2021 Schpin
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <functional>
#include <nlohmann/json.hpp>

#pragma once

namespace schpin
{

// Color in range of 0-1.f
struct color
{
        float r = 0.f;
        float g = 0.f;
        float b = 0.f;
};

constexpr bool operator!=( const color& lh, const color& rh )
{
        return lh.r != rh.r && lh.g != rh.g && lh.b != rh.b;
}

inline void to_json( nlohmann::json& j, const color& c )
{
        j["r"] = c.r;
        j["g"] = c.g;
        j["b"] = c.b;
}

inline void from_json( const nlohmann::json& j, color& c )
{
        c.r = j.at( "r" ).get< float >();
        c.g = j.at( "g" ).get< float >();
        c.b = j.at( "b" ).get< float >();
}

}  // namespace schpin

template <>
struct std::hash< schpin::color >
{
        std::size_t operator()( const schpin::color& c )
        {
                std::hash< float > f{};
                return f( c.r ) ^ f( c.g ) ^ f( c.b );
        }
};
