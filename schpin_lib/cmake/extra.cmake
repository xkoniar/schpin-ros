# Copyright 2021 Schpin
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

function(schpin_compile_options target)
  set_target_properties(${target} PROPERTIES CXX_STANDARD 20 CXX_EXTENSIONS OFF)
  target_compile_options(
    ${target}
    PRIVATE -Wall
            -Werror
            -Wextra
            -Wpedantic
            -Wnon-virtual-dtor
            -Wold-style-cast
            -Wcast-align
            -Wunused
            -Woverloaded-virtual
            -Wnull-dereference
            -Wformat=2
            # -Wduplicated-cond -Wlogical-op -Wuseless-cast -Wlifetime
            -Wunreachable-code
            -Wsign-conversion
            -Wconversion
            -Wdouble-promotion)
endfunction()

macro(schpin_add_gtest target)
  cmake_parse_arguments(ARG "" "WORKING_DIRECTORY" "" ${ARGN})
  _ament_cmake_gtest_find_gtest()

  if(ARG_WORKING_DIRECTORY)
    set(ARG_WORKING_DIRECTORY "WORKING_DIRECTORY" "${ARG_WORKING_DIRECTORY}")
  endif()
  add_executable(${target} ${ARG_UNPARSED_ARGUMENTS})

  target_include_directories(${target} SYSTEM PUBLIC ${GTEST_INCLUDE_DIRS})
  target_link_libraries(${target} PUBLIC ${GTEST_MAIN_LIBRARIES}
                                         ${GTEST_LIBRARIES})

  set(_argn_test "")
  ament_add_gtest_test(${target} ${_argn_test} ${ARG_WORKING_DIRECTORY})

endmacro()
