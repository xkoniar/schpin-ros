# Copyright 2021 Schpin
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program.  If not, see <https://www.gnu.org/licenses/>.

include(FetchContent)

FetchContent_Declare(
  lua_repo
  GIT_REPOSITORY https://github.com/lua/lua.git
  GIT_TAG v5.3.5
  GIT_PROGRESS TRUE)

FetchContent_GetProperties(lua_repo)
if(NOT lua_repo_POPULATED)
  FetchContent_Populate(lua_repo)
endif()

file(GLOB lua_sources CONFIGURE DEPENDS ${lua_repo_SOURCE_DIR}/*.c)
set_source_files_properties(${lua_sources} PROPERTIES LANGUAGE CXX)
add_library(lua STATIC ${lua_sources})
target_include_directories(lua SYSTEM PUBLIC ${lua_repo_SOURCE_DIR})
target_compile_options(lua PRIVATE -std=c++17 -DLUA_USE_POSIX)

set_target_properties(lua PROPERTIES CXX_CLANG_TIDY "")
